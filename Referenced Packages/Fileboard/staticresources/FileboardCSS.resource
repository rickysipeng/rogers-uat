/* css reset */
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, font, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td {
  margin: 0;
  padding: 0;
  border: 0;
  outline: 0;
  font-weight: inherit;
  font-style: inherit;
  font-size: 100%;
  font-family: inherit;
  vertical-align: baseline; }

/* remember to define focus styles! */
:focus {
  outline: 0; }

body {
  line-height: 1;
  color: black;
  background: white; }

ol, ul {
  list-style: none; }

/* tables still need 'cellspacing="0"' in the markup */
table {
  border-collapse: separate;
  border-spacing: 0; }

caption, th, td {
  text-align: left;
  font-weight: normal; }

blockquote:before, blockquote:after,
q:before, q:after {
  content: ""; }

blockquote, q {
  quotes: "" ""; }

/* fileboard stuff */
body {
  margin: 0px;
  padding: 0px;
  background: #252a2f;
  background-image: url(https://app.fileboard.com/assets/dark_tire.png);
  color: #333;
  font-family: 'Helvetica Neue', Helvetica, Arial;
  font-size: 12px; }

a {
  color: #828282;
  margin-right: 5px;
  text-decoration: none;
  cursor: pointer; }
  a:hover {
    color: #015ba7; }

strong {
  font-weight: bold; }

p {
  margin-bottom: 5px; }

h1 {
  font-size: 54px;
  font-weight: bold; }
  @media (max-width: 800px) {
    h1 {
      font-size: 28px; } }

h3 {
  font-size: 22px;
  font-weight: bold;
  margin-bottom: 10px; }
  @media (max-width: 800px) {
    h3 {
      font-size: 16px; } }

iframe {
  border: 0px;
  width: 100%;
  height: 100%; }

textarea, input {
  margin-bottom: 10px;
  font-size: 12px; }

hr {
  border-top: 1px solid #eee;
  margin-top: 15px;
  margin-bottom: 15px;
  opacity: 0.6; }

.slide-panel {
  overflow-y: auto;
  display: none;
  position: absolute;
  background-color: white;
  width: 670px;
  height: 100%;
  right: 0px;
  box-shadow: -2px 0px 2px 1px #444; }
  .slide-panel .panel-content {
    padding: 10px; }
  .slide-panel#settings-panel, .slide-panel#viewers-panel, .slide-panel#end-meeting-panel {
    width: 510px;
    font-size: 14px; }
    .slide-panel#settings-panel a, .slide-panel#viewers-panel a, .slide-panel#end-meeting-panel a {
      text-decoration: underline; }
  .slide-panel#settings-panel table td:first-child {
    font-weight: bold; }
  .slide-panel#settings-panel table td:last-child {
    text-align: right; }
  .slide-panel#end-meeting-panel textarea {
    width: 100%;
    height: 150px; }
  @media (max-width: 700px) {
    .slide-panel {
      left: 0% !important;
      width: 100% !important; } }

table {
  width: 100%;
  margin-bottom: 10px; }
  table tr {
    height: 20px; }
  table td {
    vertical-align: top; }
  table.attendees-table td:first-child {
    width: 45px; }
  table.attendees-table td:last-child {
    width: 20px;
    vertical-align: middle; }
  table.attendees-table tr {
    height: 40px; }
  table.attendees-table .light {
    font-size: smaller; }

.slides-list {
  clear: both; }
  .slides-list .slide {
    float: left;
    margin: 5px; }
    .slides-list .slide img {
      border: 1px solid #ddd;
      border-radius: 3px; }
    .slides-list .slide .time {
      text-align: center;
      margin-top: 4px;
      color: #888; }

.hide {
  display: none !important; }

.collapsed {
  visibility: hidden !important; }

.close {
  font-size: 13px;
  text-decoration: none !important;
  position: absolute;
  right: 10px;
  margin-top: 3px; }

.blue {
  color: #3696ee; }

.light {
  color: #888; }

.bar, .status-bar, .control-bar {
  display: block;
  position: fixed;
  width: 100%;
  background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#ececec), to(#a8a8a8));
  background-image: -webkit-linear-gradient(#ececec, #a8a8a8);
  background-image: -moz-linear-gradient(#ececec, #a8a8a8);
  background-image: -o-linear-gradient(#ececec, #a8a8a8);
  background-image: linear-gradient(#ececec, #a8a8a8);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorStr='#ececec',EndColorStr='#a8a8a8');
  -ms-filter: "progid:DXImageTransform.Microsoft.gradient(startColorStr='#ececec', EndColorStr='#a8a8a8')";
  border-bottom: 1px solid #888;
  border-top: 1px solid #ccc; }

.status-bar {
  top: 0%;
  padding: 4px;
  height: 20px; }
  .status-bar a {
    font-size: 12px;
    color: #333; }
    @media (max-width: 700px) {
      .status-bar a span {
        display: none; } }
  .status-bar .status {
    position: absolute;
    top: 5px;
    left: 5px; }
  .status-bar .left {
    position: relative;
    margin-left: 23px;
    margin-top: 4px; }
  .status-bar .center {
    position: relative;
    text-align: center;
    margin: 0 auto;
    margin-top: -18px;
    width: 250px; }
    .status-bar .center select {
      width: 100%; }
    @media (max-width: 400px) {
      .status-bar .center {
        width: 160px; } }
  .status-bar .right {
    float: right;
    position: relative;
    margin-top: -15px;
    margin-right: 8px; }

.control-bar {
  bottom: 0%;
  height: 38px; }
  .control-bar a {
    font-size: 15px; }
  .control-bar img:first-child {
    margin-left: 10px;
    margin-top: 6px;
    float: left; }
  .control-bar .center {
    position: relative;
    margin: 0 auto;
    margin-top: 3px;
    width: 250px;
    text-align: center; }
  .control-bar .right {
    float: right;
    position: relative;
    margin-top: -25px;
    margin-right: 5px; }

#current-slide-container {
  position: absolute;
  top: 30px;
  width: 100%;
  height: calc(100% - 70px);
  text-align: center; }
  #current-slide-container img {
    display: none;
    height: 100%; }
    @media (max-width: 640px) {
      #current-slide-container img {
        width: 100%;
        height: auto; } }
  #current-slide-container .status {
    display: none;
    padding-top: 20%; }
  #current-slide-container .hotspots {
    position: absolute; }
    #current-slide-container .hotspots a {
      display: block;
      position: absolute; }

.empty {
  width: 100%;
  text-align: center; }
  .empty .content i {
    display: block;
    font-size: 150px;
    margin: 40px 0px; }
    @media (max-height: 600px) {
      .empty .content i {
        font-size: 75px; } }
  .empty .invite {
    color: #666;
    font-size: 16px;
    margin-top: 40px; }

.resizer, .previous-slide, .next-slide {
  position: absolute;
  top: 30px;
  height: calc(100% - 70px);
  width: 50px; }

.previous-slide {
  cursor: w-resize; }

.next-slide {
  right: 0px;
  cursor: e-resize; }

.slide-background {
  display: none;
  width: 100%;
  height: 100%;
  position: absolute;
  background: black;
  overflow: hidden;
  -webkit-filter: blur(20px);
  -moz-filter: blur(20px);
  -o-filter: blur(20px);
  -ms-filter: blur(20px);
  filter: blur(20px);
  opacity: 0.4; }

.divider {
  border-left: 1px solid silver;
  border-right: 1px solid #16222c;
  height: 100%;
  margin: 0px 8px 0px 5px; }

a.button {
  display: inline-block;
  outline: none;
  cursor: pointer;
  text-align: center;
  text-decoration: none !important;
  font: 15px/100% Arial, Helvetica, sans-serif;
  padding: 8px;
  margin-right: 2px;
  text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
  -webkit-border-radius: 5px;
  -moz-border-radius: 5px;
  border-radius: 5px;
  -webkit-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  -moz-box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
  box-shadow: 0 1px 2px rgba(0, 0, 0, 0.2); }
  a.button:hover {
    color: white;
    text-decoration: none; }
  a.button:active {
    position: relative;
    top: 1px; }
a.gray {
  -webkit-border-radius: 10px;
  -moz-border-radius: 10px;
  border-radius: 10px;
  color: #e9e9e9;
  border: solid 1px #999;
  background: #6e6e6e;
  background: -webkit-gradient(linear, left top, left bottom, from(#888888), to(#575757));
  background: -moz-linear-gradient(top, #888888, #575757);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#888888', endColorstr='#575757'); }
  a.gray:hover {
    background: #616161;
    background: -webkit-gradient(linear, left top, left bottom, from(#757575), to(#4b4b4b));
    background: -moz-linear-gradient(top, #757575, #4b4b4b);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#757575', endColorstr='#4b4b4b'); }
  a.gray:active {
    color: #afafaf;
    background: -webkit-gradient(linear, left top, left bottom, from(#575757), to(#888888));
    background: -moz-linear-gradient(top, #575757, #888888);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#575757', endColorstr='#888888'); }
a.blue {
  color: #d9eef7;
  border: solid 1px #0076a3;
  background: #0095cd;
  background: -webkit-gradient(linear, left top, left bottom, from(#00adee), to(#0078a5));
  background: -moz-linear-gradient(top, #00adee, #0078a5);
  filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#00adee', endColorstr='#0078a5'); }
  a.blue:hover {
    background: #007ead;
    background: -webkit-gradient(linear, left top, left bottom, from(#0095cc), to(#00678e));
    background: -moz-linear-gradient(top, #0095cc, #00678e);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0095cc', endColorstr='#00678e'); }
  a.blue:active {
    color: #80bed6;
    background: -webkit-gradient(linear, left top, left bottom, from(#0078a5), to(#00adee));
    background: -moz-linear-gradient(top, #0078a5, #00adee);
    filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#0078a5', endColorstr='#00adee'); }
a.big {
  font-size: 22px;
  padding: 15px; }