<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Will_They_Buy_Mover</fullName>
        <description>Moves the numeric value from the Will They Buy field to the Will They Buy Numeric field.</description>
        <field>Will_They_Buy_Numeric__c</field>
        <formula>Will_They_Buy__c</formula>
        <name>Will They Buy Mover</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Will_They_Buy_Ours_Mover</fullName>
        <description>Moves the numeric value from the Will They Buy Ours field to the Will They Buy Ours Numeric field.</description>
        <field>Will_They_Buy_Ours_Numeric__c</field>
        <formula>Will_They_Buy_Ours__c</formula>
        <name>Will They Buy Ours Mover</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>SPI Mover</fullName>
        <actions>
            <name>Will_They_Buy_Mover</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Will_They_Buy_Ours_Mover</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>SPI__c.Will_They_Buy__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>SPI__c.Will_They_Buy_Ours__c</field>
            <operation>greaterOrEqual</operation>
            <value>0</value>
        </criteriaItems>
        <description>Moves Will They Buy and Will They Buy Ours values to placeholder numeric fields in order to calculate the Probability Score.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
