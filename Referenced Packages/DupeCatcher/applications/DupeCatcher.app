<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>Scenario__c</defaultLandingTab>
    <description>DupeCatcher is a real time duplicate lead, contact and account blocker. It can provide duplicate warning records and associate tasks with them.</description>
    <label>DupeCatcher</label>
    <logo>DupeCatcher_Media/DupeCatcher_Logo_55x300_transparent_background.jpg</logo>
    <tab>Scenario__c</tab>
    <tab>Duplicate_Warning__c</tab>
    <tab>DC_DupeCatcher_Application_Settings</tab>
    <tab>DC_DupeCatcher_Getting_Started</tab>
    <tab>NHL_Service__c</tab>
    <tab>Account_Provisioning_Form__c</tab>
    <tab>RFP_Pre_Qualification__c</tab>
</CustomApplication>
