<apex:page controller="n2de.TagAssignController" sidebar="false"  action="{!init}" tabStyle="Distribution_Tags__tab">
	
	<script type="text/javascript" src="{!URLFOR($Resource.DEResources, 'scripts/jquery-1.7.1.min.js')}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.DEResources, 'scripts/jquery-ui-1.8.21.min.js')}"></script>
	<script type="text/javascript" src="{!URLFOR($Resource.DEResources, 'scripts/jquery.multiselect.min.js')}"></script>
	<link rel="stylesheet" href="{!URLFOR($Resource.DEResources, 'css/jquery-ui-1.8.21.css')}" type="text/css" />
	<link rel="stylesheet" href="{!URLFOR($Resource.DEResources, 'css/jquery.multiselect.css')}" type="text/css" />
	<link href="//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css" rel="stylesheet" />
	
	<c:CommonMask />
	<c:CommonHelp help_page="distributor-tags"/>
	<style>
	.tag_values .tag{
		border: 1px solid #56aa1c;
		background-color: #56aa1c !important;
		color: white;
		margin-top: 3px;
		padding: 5px;
		float: left;
		margin: 5px;
		border-radius: 5px;
		cursor: move;
		font-size: 10pt;
	 }
	.panel{
		height: 400px;
		width: 100%;
		overflow: auto;
	}
	.panel table{
		height: 100%;
	}
	.search_box{
		float: right;
	}
	.search_box input{
		width: 150px;
		margin-left: 10px;
	}
	.drag_hover_over div{
		background-color: yellowgreen;
	}
	.delete_hover_over{
		background: url('{!URLFOR($Resource.DEResources, 'images/cross32.png')}') right center no-repeat;
		background-size: 20px;
		padding: 10px;
	}
	.available{
	}
	.user_tag_val input{
		display: none;
	}
	.highlight{
		border: 1px solid lightgray !important;
		color: yellow !important;
	}
	.tag_collection{
		float: left;
		border: 1px solid lightgray;
		border-radius: 5px;
		clear: both;
		margin: 3px;
		padding: 3px;
		width: 95%;
		background-color: #fafafa;
	}
	.tag_collection span{
		float: left;
		color: gray;
		font-family: FontAwesome;
		margin-left: 5px;
	}
	.tag_collection .tag:first-of-type{
		clear: both;
	}
	.user_profile{
		border: 1px solid lightgray;
		border-radius: 5px;
		padding: 10px;
		border-radius: 5px;
		color: gray;
		font-weight: bold;
		margin: 3px;
		display: block;
		height: 97%;
		text-align: center;
		background-color: #fafafa;
		vertical-align: middle;
	}
	.user_profile div{
		margin-top: 5px;
	}
	.user_table td:first-of-type{
		width: 150px;
	}
	.user_table td{
		padding: 4px 2px 4px 5px;
		color: #333;
		border: none !important;
		border-bottom: 1px solid lightgray !important;
	}
	</style>
	<script>
	var tagScrollPosition;
	var userScrollPosition;
	
	//Show mask while page loads
	showMask();
	
	function fidoCallback(status) { }
	 
	// Update to jquery :selector, make case insensitive
	jQuery.expr[':'].contains = function(a, i, m) {
	  return jQuery(a).text().toUpperCase()
	      .indexOf(m[3].toUpperCase()) >= 0;
	};
	
	/**
	* On document ready
	**/
	$(document).ready(function() {
		init();
	});
	
	function init(){
		initFilters();
		initDragDrop();
		hideMask();
	}
	
	function postTagInit(){
		filterUsers();
		filterTags();
		initDragDrop();
	}
	
	/**
	* Tag and team filters and searches
	**/
	function initFilters(){
		//Add jquery multiselect control
		$('.team_filter').multiselect({
			minWidth: 400,
			noneSelectedText: 'All teams',
			selectedList: 2
		});
		$('.tag_filter').multiselect({
			minWidth: 400,
			noneSelectedText: 'All tags',
			selectedList: 2
		});
		$(".team_filter").change(function(event, ui){
			var teamFilterArray = $(".team_filter").val();
			if(teamFilterArray != null){
				var userFilter = $('#user_search_box input').val();
				filterUsersByTeam(teamFilterArray, userFilter);
			}
		});
		$('#user_search_box input').keyup(function( event ) {
			filterUsers();
		});
		$('#tag_search_box input').keyup(function( event ) {
			filterTags();
		});
	}
	
	
	/**
	* Drag and drop add / remove tags
	**/
	function initDragDrop(){
		$( ".user_tag_val" ).draggable({ 
			revert: true, 
			helper: "clone",
			drag: function( event, ui ) {
			}	
		});
		$( ".tagPanel .tag" ).draggable({ 
			revert: true, 
			helper: "clone",
			drag: function( event, ui ) {
			}	
		});
		$( ".droppable").droppable({
	       accept: ".tagPanel .tag",
	       over: function( event, ui ) {
	          $(this).addClass('drag_hover_over');
	       },
	       out: function( event, ui ) {
	          $(this).removeClass('drag_hover_over');
	       },
	       drop: function( event, ui ) {
	       	  $(this).removeClass('drag_hover_over');
	          var tagId = ui.draggable.context.id;
	          var userId = $(this).attr("id"); //event.target.id;
	          showMask();
	          tagScrollPosition = $(".tag_panel").scrollTop();
	    	  userScrollPosition = $(".user_panel").scrollTop();
	          tagUserAF(userId, tagId);
	          
	       }
	    });
	    $( ".tagPanel").droppable({
	       accept: ".user_tag_val",
	       over: function( event, ui ) {
	          $(this).addClass('drag_hover_over');
	       },
	       out: function( event, ui ) {
	          $(this).removeClass('drag_hover_over');
	       },
	       drop: function( event, ui ) {
	       	  $(this).removeClass('drag_hover_over');
	          var tagValueId = ui.draggable.context.id;
	          $('#' + tagValueId + ' input').prop('checked', true);
	          showMask();
	          tagScrollPosition = $(".tag_panel").scrollTop();
	    	  userScrollPosition = $(".user_panel").scrollTop();
	          deleteTagAF();
	       }
	    });
		initHover();
		setScrollbars();
	    hideMask();
	}
	
	function setScrollbars(){
		$(".tag_panel").scrollTop(tagScrollPosition);
	    $(".user_panel").scrollTop(userScrollPosition);
	}
	
	function filterUsers(){
		var userName = $('#user_search_box input').val();
		if($('.team_filter').val() == null){
		    userName = userName.toUpperCase();
			$(".user_row").hide();
			$(".user_row td:contains('" + userName + "')").each(function(){
				$(this).parent().show();
			});  
		}else{
			filterUsersByTeam($('.team_filter').val(), userName);
		}
	}
	function filterUsersByTeam(teamIds, userFilter){
		$(".user_row").hide();
		$(".user_row").each(function(){
			var matchesTeamFilter = false;
			var thisUserTeamIds = $(this).attr("team_ids");
			var thisUserTeamIdArray = thisUserTeamIds.split(',');
			teamIds.forEach(function(thisTeamId){
				thisUserTeamIdArray.forEach(function(aTeamId){
					if(aTeamId == thisTeamId){
						matchesTeamFilter = true;
					}
				});
			});
			if(matchesTeamFilter == true){
				if(userFilter.length > 0){
					userFilter = userFilter.toUpperCase();
					if($(this).find("td:contains('" + userFilter + "')").size() > 0){
						$(this).show();
					}
				}else{
					$(this).show();
				}
			}
		});
	}
	function filterTags(){
		var tagName = $('#tag_search_box input').val();
	    tagName = tagName.toUpperCase();
		$(".tag").hide();
		$(".tag:contains('" + tagName + "')").each(function(){
			$(this).show();
		});  
	}
	function initHover(){
		$(".tag").hover(
			function() {
		    	$( this ).addClass('highlight');
		  	}, function() {
		    	$( this ).removeClass('highlight');
		  	}
		);
	}
	</script>	
	<apex:form >
		
		<apex:actionFunction name="tagUserAF" action="{!tagUser}" reRender="pageMessages, userBlock" oncomplete="postTagInit()">
			<apex:param name="userId" value="" />
			<apex:param name="tagId" value="" />
		</apex:actionFunction>
		
		<apex:actionFunction name="deleteTagAF" action="{!deleteTags}" reRender="pageMessages, userBlock" oncomplete="postTagInit()" />
		
		<apex:sectionHeader title="Tag Collections" subtitle="Tag Users"/>
		<apex:pageMessages id="pageMessages"/>
		
		<apex:pageBlock title="Tag Assignment">
			<apex:pageBlockButtons >
				<apex:commandButton value="Save" action="{!save}" />
				<apex:commandButton value="Cancel" action="{!cancel}" />
			</apex:pageBlockButtons>
			<apex:pageBlockSection columns="2">
				<apex:pageBlockSectionItem >
				<apex:outputPanel >
					<apex:selectList value="{!tagFilter}" multiselect="true" size="5" styleClass="tag_filter">
						<apex:selectOptions value="{!tagOptionList}" />
						<apex:actionSupport event="onchange" action="{!doUpdate}" onsubmit="showMask()" oncomplete="initDragDrop()" reRender="tag_block, pageMessages"/> 
					</apex:selectList>
					<div id="tag_search_box" class="search_box">Tag <input type="text"/></div>
				</apex:outputPanel>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
				<apex:outputPanel >
					<apex:selectList value="{!teamFilter}" multiselect="true" size="5" styleClass="team_filter">
						<apex:selectOptions value="{!teamOptionList}" />
					</apex:selectList>
					<div id="user_search_box" class="search_box">User <input type="text"/></div>
				</apex:outputPanel>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem >
					<apex:outputPanel id="tag_block" styleClass="tagPanel panel" layout="block">
						<table class="list team_table" border="0" cellpadding="0" cellspacing="0">
							<tr>
								<td>
									<div class="tag_values">
									<apex:repeat value="{!tagList}" var="tag">
										<div class="tag_collection">
											<span>{!tag.tagCategorySObject.Name}</span>
											<div style="clear:both"/>
											<apex:repeat value="{!tag.tagCategorySObject.Distributor_Tag_Values__r}" var="val">
												<div id="{!val.Id}" class="tag fa fa-tag fa-lg ">&nbsp;&nbsp;{!val.Name}</div>
											</apex:repeat>
										</div>
									</apex:repeat>
									</div>
								</td>
							</tr>
						</table>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
				<apex:pageBlockSectionItem id="userPanel">
					<apex:outputPanel styleClass="panel user_panel" layout="block" id="userBlock">
						<table class="list user_table" border="0" cellpadding="0" cellspacing="0">
							<apex:repeat value="{!userList}" var="u">
							<tr id="{!u.userRecord.Id}" class="user_row droppable" team_ids="{!u.teamIds}">
								<td id="{!u.userRecord.Id}" >
									<div class="user_profile">
										<apex:image value="{!u.photoURL}" styleClass="profile_image" rendered="{!LEN(u.photoUrl) > 0}"/>
										<apex:image value="{!URLFOR($Resource.n2de__DEResources, 'images/teammember32.png')}" styleClass="profile_image" rendered="{!LEN(u.photoUrl) == 0}" />
										<div>{!u.userRecord.name}</div>
									</div>
								</td>
								<td id="{!u.userRecord.Id}">
									<div class="tag_values">
									<apex:repeat value="{!u.tags}" var="collection">
										<div class="tag_collection">
										<span>{!collection.name}</span>
										<div style="clear:both"/>
										<apex:repeat value="{!collection.tagValues}" var="tagval">
											<div class="user_tag_val tag fa fa-tag fa-lg" id="{!u.userRecord.Id + '_' + collection.id + '_' + tagval.tagRecord.id}">
												<apex:inputcheckbox value="{!tagval.selected}"/>
												&nbsp;&nbsp;{!tagval.tagRecord.Name}</div>
										</apex:repeat>
										</div>
									</apex:repeat>
									</div>
								</td>
							</tr>
							</apex:repeat>
						</table>
					</apex:outputPanel>
				</apex:pageBlockSectionItem>
			</apex:pageBlockSection>
		</apex:pageBlock>
	</apex:form>
	
</apex:page>