<?xml version="1.0" encoding="UTF-8"?>
<CustomApplication xmlns="http://soap.sforce.com/2006/04/metadata">
    <defaultLandingTab>standard-home</defaultLandingTab>
    <label>Distribution Engine</label>
    <logo>Distribution_engine/Logo.png</logo>
    <tab>Console</tab>
    <tab>Team__c</tab>
    <tab>Distribution_Tags</tab>
    <tab>Manage_Holidays</tab>
    <tab>Out_of_office</tab>
    <tab>Logs</tab>
    <tab>Distribution_Analytics</tab>
    <tab>Settings</tab>
    <tab>Account_Provisioning_Form__c</tab>
    <tab>RFP_Pre_Qualification__c</tab>
</CustomApplication>
