<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Notification_to_Owner</fullName>
        <description>Notification to Owner</description>
        <protected>false</protected>
        <recipients>
            <field>SPR_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Restquested_By_spr</fullName>
        <description>Notification to Restquested By (spr)</description>
        <protected>false</protected>
        <recipients>
            <field>SPR_Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Ack_Recipient_on_Note_Change</fullName>
        <description>Notify Ack Recipient on Note Change</description>
        <protected>false</protected>
        <recipients>
            <field>Recipient_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/Ack_Recipient_Note_Change</template>
    </alerts>
    <alerts>
        <fullName>Notify_Global_Recipients</fullName>
        <description>Notify Global Recipients</description>
        <protected>false</protected>
        <recipients>
            <field>Global_Recipient_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_3_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_4_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_5_Email__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Recipient_1</fullName>
        <description>Notify Recipient 1</description>
        <protected>false</protected>
        <recipients>
            <field>Recipient_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Recipients</fullName>
        <description>Notify Recipients</description>
        <protected>false</protected>
        <recipients>
            <field>Recipient_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Recipients_2_5</fullName>
        <description>Notify Recipients 2-5</description>
        <protected>false</protected>
        <recipients>
            <field>Recipient_2__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_3__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_4__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_5__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SPR_Notification</template>
    </alerts>
    <alerts>
        <fullName>Notify_Recipients_an_SPR_Note_Created_or_Edited</fullName>
        <description>Notify Recipients an SPR Note Created or Edited</description>
        <protected>false</protected>
        <recipients>
            <field>Global_Recipient_1_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Global_Recipient_2_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>SPR_Requestor_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <field>Recipient_1__c</field>
            <type>userLookup</type>
        </recipients>
        <recipients>
            <field>Recipient_2__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/SALESFollowUpSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Notify_new_Ack_Recipient</fullName>
        <description>Notify new Ack Recipient</description>
        <protected>false</protected>
        <recipients>
            <field>Recipient_1__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RDC/Ack_Recipient</template>
    </alerts>
    <fieldUpdates>
        <fullName>Ack_Already_notified</fullName>
        <description>Mark Ack_Already_Notified = True</description>
        <field>Ack_Already_Notified__c</field>
        <literalValue>1</literalValue>
        <name>Ack Already notified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Append_Ack_Recipient_Info_on_Body</fullName>
        <field>Body__c</field>
        <formula>Body__c  + BR() + &quot;(Acknowledged by &quot; +  Recipient_1__r.FirstName + &quot; &quot; +  Recipient_1__r.LastName + &quot; on &quot; +  left(text(NOW() -0.208333), 19) + &quot;)&quot;</formula>
        <name>Append Ack Recipient Info on Body</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Recipient_1_Email_on_Note</fullName>
        <field>Global_Recipient_1_Email__c</field>
        <formula>SPR__r.Global_Recipient_1__r.Email</formula>
        <name>Update Global Recipient 1 Email on Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Recipient_2_Email_on_Note</fullName>
        <field>Global_Recipient_2_Email__c</field>
        <formula>SPR__r.Global_Recipient_2__r.Email</formula>
        <name>Update Global Recipient 2 Email on Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Recipient_3_Email_on_Note</fullName>
        <field>Global_Recipient_3_Email__c</field>
        <formula>SPR__r.Global_Recipient_3__r.Email</formula>
        <name>Update Global Recipient 3 Email on Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Recipient_4_Email_on_Note</fullName>
        <field>Global_Recipient_4_Email__c</field>
        <formula>SPR__r.Global_Recipient_4__r.Email</formula>
        <name>Update Global Recipient 4 Email on Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Global_Recipient_5_Email_on_Note</fullName>
        <field>Global_Recipient_5_Email__c</field>
        <formula>SPR__r.Global_Recipient_5__r.Email</formula>
        <name>Update Global Recipient 5 Email on Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Requestor_Email_on_SPR_Note</fullName>
        <field>SPR_Requestor_Email__c</field>
        <formula>SPR__r.Requested_By__r.Email</formula>
        <name>Update Requestor Email on SPR Note</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Append Ack Recipient Info on Note Body upon Acknolwedgement</fullName>
        <actions>
            <name>Append_Ack_Recipient_Info_on_Body</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>When an ACK Recipient acknowledges a note, it will time stamp the body.
The current user ack-ing must be the ack recipient 1.</description>
        <formula>AND (   NOT (Isblank(Recipient_1__c )) ,    AND ( ischanged(Request_ACK__c ), Request_ACK__c  == false) ,   Recipient_1__r.Id ==  $User.Id )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Closed SPR with new note Created</fullName>
        <active>false</active>
        <formula>ISPICKVAL( SPR__r.Status__c  , &quot;Closed&quot;)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>New SPR Note Created</fullName>
        <actions>
            <name>Update_Global_Recipient_1_Email_on_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Recipient_2_Email_on_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Recipient_3_Email_on_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Recipient_4_Email_on_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Global_Recipient_5_Email_on_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Requestor_Email_on_SPR_Note</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <formula>True</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR ACK notification to Recipient 1 - Just Assigned</fullName>
        <actions>
            <name>Notify_new_Ack_Recipient</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Ack_Already_notified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Notify Recipient 1 on SPR NOTE upon being assigned as Ack recipient</description>
        <formula>OR(  AND ( Private__c == false,  OR(ISCHANGED( Recipient_1__c ), ISCHANGED(  Request_ACK__c ), ISNEW()),  Request_ACK__c == true) ,  AND ( Private__c == false, ischanged(Private__c),  OR(ISCHANGED( Recipient_1__c ), ISCHANGED(  Request_ACK__c ), Not (ISNEW())),  Request_ACK__c == true, NOT  (Ack_Already_Notified__c )) ,  AND (Private__c == true,  ischanged(Recipient_1_Shared__c),  Request_ACK__c == true )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR ACK notification to Recipient 1 on update</fullName>
        <actions>
            <name>Notify_Ack_Recipient_on_Note_Change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Recipient 1 on SPR NOTE upon note changed, and that the recipient is an ACK person.
Want to filter out the case where it&apos;s a new assignment, including IsNew, Recipient 1 changed, or Request Ack changed.</description>
        <formula>AND (  Private__c == false,  ISCHANGED(  Body__c  ),  Request_ACK__c == true,    Not(ISCHANGED( Recipient_1__c ) ),   Not(ISCHANGED(  Request_ACK__c  ) ),   Not(ISNEW() ) )  ||  AND (  Private__c == true,  isChanged(Recipient_1_Shared__c) ,  ISCHANGED(  Body__c  ),  Request_ACK__c == true,    Not(ISCHANGED( Recipient_1__c ) ),   Not(ISCHANGED(  Request_ACK__c  ) ),   Not(ISNEW() ) )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR Note Email to Global Recipient</fullName>
        <actions>
            <name>Notify_Global_Recipients</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(Body__c ) || ISNEW() )  &amp;&amp; ( Not Private__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR Notification to Owner</fullName>
        <actions>
            <name>Notification_to_Owner</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(Body__c ) || ISNEW()  )  &amp;&amp;  Notify_Owner__c  &amp;&amp;  ( Not Private__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR Notification to Requested By</fullName>
        <actions>
            <name>Notification_to_Restquested_By_spr</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(Body__c ) || ISNEW() )  &amp;&amp;  Notify_Requestor__c  &amp;&amp; ( Not Private__c )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR notification to Recipient 1</fullName>
        <actions>
            <name>Notify_Recipient_1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Recipient 1 on SPR NOTE upon updates of the note.
But only if ACK is not on. (If ACK is on, there is another email for it)</description>
        <formula>AND ( OR(ISCHANGED(Body__c), ISNEW()),   Request_ACK__c == false)</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR notification to Recipients</fullName>
        <actions>
            <name>Notify_Recipients</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <formula>(ISCHANGED(Body__c ) || ISNEW() )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>SPR notification to Recipients 2-5</fullName>
        <actions>
            <name>Notify_Recipients_2_5</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <description>Notify Recipient 2-5 on SPR NOTE upon updates of the note.</description>
        <formula>OR(ISCHANGED(Body__c), ISNEW())</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
