<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Sales_Empowerment_Rejected</fullName>
        <description>Sales Empowerment Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/Sales_Empowerment_Request_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Sales_Empowerment_Request_Approved</fullName>
        <description>Sales Empowerment Request Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/Sales_Empowerment_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Start_Contract</fullName>
        <description>Start Contract</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Prime__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/PER_Email_to_Start_Contract</template>
    </alerts>
    <alerts>
        <fullName>Terminate_Contract</fullName>
        <description>Terminate Contract</description>
        <protected>false</protected>
        <recipients>
            <field>Contracts_Prime__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Various_Templates/PER_Email_to_Terminate_Contract</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approved_Status</fullName>
        <field>Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Approved Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PER_Last_Approval_Date_Time_Stamp</fullName>
        <field>Date_Time_of_Last_Approval__c</field>
        <formula>NOW()</formula>
        <name>PER - Last Approval Date/Time Stamp</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PER_Previous_Approver</fullName>
        <field>Previous_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>PER - Previous Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PER_Submitter</fullName>
        <field>Previous_Approver__c</field>
        <formula>$User.FirstName &amp; &quot; &quot; &amp; $User.LastName</formula>
        <name>PER - Submitter</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Approved_PER</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Approved_Pricing_Enablement_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Approved PER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_In_Approval_PER</fullName>
        <field>RecordTypeId</field>
        <lookupValue>In_Approval_Pricing_Enablement_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to In-Approval PER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Unapproved_PER</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Unapproved_Pricing_Enablement_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Unapproved PER</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_Status</fullName>
        <field>Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Rejected Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_for_Approval_Status</fullName>
        <description>Update the status of PER to Submitted for Approval</description>
        <field>Status__c</field>
        <literalValue>Submitted for Approval</literalValue>
        <name>Submitted for Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Monthly_Recurring_Add_Ons</fullName>
        <description>Updates the Monthly Recurring Add-Ons (pre-tax) custom field on Pricing Enablement Request custom object</description>
        <field>Monthly_Recurring_Add_Ons_pre_tax_WF__c</field>
        <formula>Monthly_Recurring_Indv_Add_Ons__c +  Monthly_Recurring_Intl_Shared_Add_Ons__c +  Monthly_Recurring_US_Shared_Add_Ons__c</formula>
        <name>Update Monthly Recurring Add-Ons</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Sales Empowerment Request is edited</fullName>
        <actions>
            <name>Update_Monthly_Recurring_Add_Ons</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Triggers every time a record is modified</description>
        <formula>ISCHANGED(LastModifiedDate )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
