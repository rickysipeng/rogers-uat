<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>QR3P_A_new_3rd_party_quote_request_is_closed</fullName>
        <description>QR3P: Quote Request is Closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Quote_Request/QR3P_Quote_Request_Status_Closed</template>
    </alerts>
    <alerts>
        <fullName>QR3P_A_new_3rd_party_quote_request_is_submitted</fullName>
        <description>QR3P: A new 3rd party quote request is submitted</description>
        <protected>false</protected>
        <recipients>
            <recipient>Quote_Desk</recipient>
            <type>group</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Quote_Request/QR3P_Quote_Request_Status_Update</template>
    </alerts>
    <fieldUpdates>
        <fullName>QR3P_Assign_to_Quote_Desk</fullName>
        <description>assign the ownership of a Quote Request to the Quote Desk queue</description>
        <field>OwnerId</field>
        <lookupValue>Quote_Desk</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>QR3P Assign to Quote Desk</name>
        <notifyAssignee>true</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QR3P_Stage_is_Rejected_by_Quote_Desk</fullName>
        <field>Quoting_Stage__c</field>
        <literalValue>Rejected by Quote Desk</literalValue>
        <name>QR3P Stage is Rejected by Quote Desk</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QR3P_Stage_is_Submitted_by_SE</fullName>
        <field>Quoting_Stage__c</field>
        <literalValue>Submitted by SE</literalValue>
        <name>QR3P Stage is Submitted by SE</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QR_Approval_Status_Update_Approved</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>QR Approval Status Update - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>QR_Approval_Status_Update_Declined</fullName>
        <field>Approval_Status__c</field>
        <literalValue>Declined</literalValue>
        <name>QR Approval Status Update - Declined</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Request_Closed</fullName>
        <field>Quoting_Stage__c</field>
        <literalValue>Closed by Quote Desk</literalValue>
        <name>Quote Request - Closed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Request_Completed</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Completed_Process</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quote Request - Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Request_In_Process</fullName>
        <field>RecordTypeId</field>
        <lookupValue>In_Process_SE</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quote Request - In Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Request_Out_of_Process</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Control_SE</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Quote Request - Out of Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Quote_Request_Recalled</fullName>
        <field>Quoting_Stage__c</field>
        <literalValue>Pre-submission</literalValue>
        <name>Quote Request - Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>QR3P Closed by QD</fullName>
        <actions>
            <name>QR3P_A_new_3rd_party_quote_request_is_closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote_Request__c.Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approved,Rejected</value>
        </criteriaItems>
        <description>Quote Request for 3rd party is submitted by QD return to SE.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>QR3P Submitted by SE</fullName>
        <actions>
            <name>QR3P_A_new_3rd_party_quote_request_is_submitted</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>QR3P_Assign_to_Quote_Desk</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>QR3P_Stage_is_Submitted_by_SE</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote_Request__c.Submit_to_Quote_Desk__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote_Request__c.Number_of_Location_A__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <description>Quote Request for 3rd party is submitted by SE to QD</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
