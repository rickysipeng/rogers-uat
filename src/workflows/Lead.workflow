<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Auto_response</fullName>
        <description>Auto response</description>
        <protected>false</protected>
        <recipients>
            <field>Agent_Email_Address__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Lead_Related_Templates/RS4B_Web2Lead_Auto_response</template>
    </alerts>
    <alerts>
        <fullName>Notify_Owner</fullName>
        <description>Notify Owner</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>DefaultWorkflowUser</senderType>
        <template>unfiled$public/LeadsNewassignmentnotificationSAMPLE</template>
    </alerts>
    <fieldUpdates>
        <fullName>Change_Record_Type_for_Lead_to</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Qualified_Inbound_Call_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change Record Type for Lead to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Opt_In_Flip_1_1</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>0</literalValue>
        <name>Email Opt In Flip 1.1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Email_Opt_In_Flip_2_1</fullName>
        <field>HasOptedOutOfEmail</field>
        <literalValue>1</literalValue>
        <name>Email Opt In Flip 2.1</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HAWT_Flow_Large</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>HAWT Lead Large</literalValue>
        <name>HAWT Flow Large</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HAWT_Flow_Medium</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>HAWT Lead Medium</literalValue>
        <name>HAWT Flow Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>HAWT_Flow_Small</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>HAWT Lead Small</literalValue>
        <name>HAWT Flow Small</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Type_Dealer</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>Dealer to Internal</literalValue>
        <name>Lead Type - Dealer</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Type_HawtFlow</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>HAWT Lead</literalValue>
        <name>Lead Type - HawtFlow</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Type_New_Lead</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>New Lead</literalValue>
        <name>Lead Type - New Lead</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Type_RS4B</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>RS4B Lead</literalValue>
        <name>Lead Type - RS4B</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Lead_Type_Rogers_com</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>Rogers.Com Small</literalValue>
        <name>Lead Type - Rogers.com Small</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RS4B_Lead_Source</fullName>
        <field>LeadSource</field>
        <literalValue>RS4B Lead</literalValue>
        <name>RS4B Lead Source</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RS4B_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>RS4B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>RS4B Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RS4B_Scoring_Rule</fullName>
        <field>Lead_Scoring__c</field>
        <literalValue>Hot</literalValue>
        <name>RS4B Scoring Rule</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RS4B_Scoring_Rule_Cold</fullName>
        <field>Lead_Scoring__c</field>
        <literalValue>Cold</literalValue>
        <name>RS4B Scoring Rule - Cold</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RS4B_Scoring_Rule_warm</fullName>
        <field>Lead_Scoring__c</field>
        <literalValue>Warm</literalValue>
        <name>RS4B Scoring Rule - warm</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Referral_Program_Field</fullName>
        <description>Update the field with a predefine value called &quot;Employee Referral Program&quot;</description>
        <field>Referral_Program_Name__c</field>
        <formula>&quot;Employee Referral Program&quot;</formula>
        <name>Referral Program Field</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rogers_com_Large</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>Rogers.Com Large</literalValue>
        <name>Rogers.com Large</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rogers_com_Medium</fullName>
        <field>arxxusleadassig__Type__c</field>
        <literalValue>Rogers.Com Medium</literalValue>
        <name>Rogers.com Medium</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Qualified</fullName>
        <description>Update record type from New Lead to Qualified Lead</description>
        <field>RecordTypeId</field>
        <lookupValue>Qualified_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Qualified</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type_to_Wireless_New_Lea</fullName>
        <field>RecordTypeId</field>
        <lookupValue>New_Lead</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type to Wireless – New Lea</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Change Lead Record Type for Qualified Inbound Call Leads</fullName>
        <actions>
            <name>Change_Record_Type_for_Lead_to</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Inbound Call Lead</value>
        </criteriaItems>
        <description>This is For Qualified Inbound Call,Change the record type of the lead once the status is changed to Qualified. Phone and email fields are made mandatory. Convert button is provided.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Lead Record Type for Qualified Leads</fullName>
        <actions>
            <name>Update_Record_Type_to_Qualified</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Lead</value>
        </criteriaItems>
        <description>Change the record type of the lead once the status is changed to Qualified. Phone and email fields are made mandatory. Convert button is provided.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Data Centre %E2%80%93 New</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Data Centre %E2%80%93 Qualified</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Data Centre</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Wireless %E2%80%93 New</fullName>
        <actions>
            <name>Update_Record_Type_to_Wireless_New_Lea</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Wireless</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Wireline %E2%80%93 New</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>notEqual</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Change Record Type to Wireline %E2%80%93 Qualified</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Lead.Status</field>
            <operation>equals</operation>
            <value>Qualified</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Lead_Type__c</field>
            <operation>equals</operation>
            <value>Wireline</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt In Flip 1</fullName>
        <actions>
            <name>Email_Opt_In_Flip_1_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.E_mail_Opt_in__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Email Opt In Flip 2</fullName>
        <actions>
            <name>Email_Opt_In_Flip_2_1</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.E_mail_Opt_in__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - Dealer</fullName>
        <actions>
            <name>Lead_Type_Dealer</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Dealer to Internal</value>
        </criteriaItems>
        <description>Change Lead Type to Dealer Lead on New Dealer Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - HawtFlow</fullName>
        <actions>
            <name>Lead_Type_HawtFlow</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>HAWT Lead</value>
        </criteriaItems>
        <description>Change Lead Type to Hawt flow on New Hawt Lead Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - HawtFlow Large</fullName>
        <actions>
            <name>HAWT_Flow_Large</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>HAWT Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_Of_Employees2__c</field>
            <operation>equals</operation>
            <value>500+</value>
        </criteriaItems>
        <description>Change Lead Type to Hawt flow on New Hawt Lead Record 1-20</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - HawtFlow Medium</fullName>
        <actions>
            <name>HAWT_Flow_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>HAWT Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_Of_Employees2__c</field>
            <operation>equals</operation>
            <value>20-99,100-499</value>
        </criteriaItems>
        <description>Change Lead Type to Hawt flow on New Hawt Lead Record 1-20</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - HawtFlow Small</fullName>
        <actions>
            <name>HAWT_Flow_Small</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>HAWT Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_Of_Employees2__c</field>
            <operation>equals</operation>
            <value>1-19</value>
        </criteriaItems>
        <description>Change Lead Type to Hawt flow on New Hawt Lead Record 1-20</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - New Lead</fullName>
        <actions>
            <name>Lead_Type_New_Lead</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Lead</value>
        </criteriaItems>
        <description>Change Lead Type to New Lead on New New Lead Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - RS4B</fullName>
        <actions>
            <name>Lead_Type_RS4B</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>RS4B Lead</value>
        </criteriaItems>
        <description>Change Lead Type to RS4B Lead on New RS4B Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - Rogers%2Ecom Large</fullName>
        <actions>
            <name>Rogers_com_Large</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers.Com Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_Of_Employees2__c</field>
            <operation>equals</operation>
            <value>500+</value>
        </criteriaItems>
        <description>Change Lead Type to Rogers.com on New Rogers.com Lead Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - Rogers%2Ecom Medium</fullName>
        <actions>
            <name>Rogers_com_Medium</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers.Com Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_Of_Employees2__c</field>
            <operation>equals</operation>
            <value>20-99,100-499</value>
        </criteriaItems>
        <description>Change Lead Type to Rogers.com on New Rogers.com Lead Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Lead Type - Rogers%2Ecom Small</fullName>
        <actions>
            <name>Lead_Type_Rogers_com</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>Rogers.Com Lead</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.Number_Of_Employees2__c</field>
            <operation>equals</operation>
            <value>1-19</value>
        </criteriaItems>
        <description>Change Lead Type to Rogers.com on New Rogers.com Lead Record</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RS4B Lead Source</fullName>
        <actions>
            <name>Auto_response</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>RS4B_Lead_Source</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.RecordTypeId</field>
            <operation>equals</operation>
            <value>RS4B Lead</value>
        </criteriaItems>
        <description>Auto populates a new RS4B to have the Lead source of RS4B</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>RS4B Scoring Rule - Cold</fullName>
        <actions>
            <name>RS4B_Scoring_Rule_Cold</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Did_the_customer_agree_to_be_contacted__c</field>
            <operation>equals</operation>
            <value>In a month</value>
        </criteriaItems>
        <description>Sets the Lead to Cold on an RS4B Lead</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RS4B Scoring Rule - Hot</fullName>
        <actions>
            <name>RS4B_Scoring_Rule</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Did_the_customer_agree_to_be_contacted__c</field>
            <operation>equals</operation>
            <value>Within 48 hours</value>
        </criteriaItems>
        <description>Sets the Lead to Hot on an RS4B Lead</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>RS4B Scoring Rule - Warm</fullName>
        <actions>
            <name>RS4B_Scoring_Rule_warm</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Lead.Did_the_customer_agree_to_be_contacted__c</field>
            <operation>equals</operation>
            <value>Within 2 weeks</value>
        </criteriaItems>
        <description>Sets the Lead to Warm on an RS4B Lead</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Referral Program Field</fullName>
        <actions>
            <name>Referral_Program_Field</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <booleanFilter>1 OR 2</booleanFilter>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>ReferralPage Site Guest User</value>
        </criteriaItems>
        <criteriaItems>
            <field>Lead.CreatedById</field>
            <operation>equals</operation>
            <value>Referral Page French Site Guest User</value>
        </criteriaItems>
        <description>Update the referral program field from the website, via identifying the user</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
</Workflow>
