<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Contact_Rogers_Dealer_switched_to_Inactive</fullName>
        <ccEmails>sfdc.support@rci.rogers.com</ccEmails>
        <description>Contact - Rogers Dealer switched to Inactive</description>
        <protected>false</protected>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Various_Templates/Rogers_Dealer_Contact_switched_to_Inactive</template>
    </alerts>
    <alerts>
        <fullName>Rogers_Dealer_Contact_switched_to_Inactive</fullName>
        <ccEmails>sfdc.support@rci.rogers.com</ccEmails>
        <ccEmails>sfdcdealer.support@rci.rogers.com</ccEmails>
        <description>Rogers Dealer Contact switched to Inactive</description>
        <protected>false</protected>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Various_Templates/Rogers_Dealer_Contact_switched_to_Inactive</template>
    </alerts>
    <fieldUpdates>
        <fullName>Switch_to_Active_Contact</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Contact_Active</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Switch to Active Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Switch_to_Inactive_Contact</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Contact_Inactive</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Switch to Inactive Contact</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Rogers Dealer Contact switched to Inactive</fullName>
        <actions>
            <name>Rogers_Dealer_Contact_switched_to_Inactive</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <criteriaItems>
            <field>Contact.Contact_Type__c</field>
            <operation>equals</operation>
            <value>Rogers Dealer</value>
        </criteriaItems>
        <criteriaItems>
            <field>User.LastModifiedById</field>
            <operation>notEqual</operation>
            <value>Shim Bindra,Andy Ward</value>
        </criteriaItems>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Switch to Active Contact</fullName>
        <actions>
            <name>Switch_to_Active_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Active__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Switch to Inactive Contact</fullName>
        <actions>
            <name>Switch_to_Inactive_Contact</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Contact.Active__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
