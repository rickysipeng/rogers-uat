<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Account_Assignment_Description_populat</fullName>
        <description>Account Assignment - Description populate from Service Group</description>
        <field>Description__c</field>
        <formula>Service_Group__r.Description__c</formula>
        <name>Account Assignment - Description populat</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Account Assignment - Description populate</fullName>
        <actions>
            <name>Account_Assignment_Description_populat</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Assignment__c.Inactive__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Populates the Description</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
