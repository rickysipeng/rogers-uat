<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Provisioning_Form_Email_Closed</fullName>
        <description>Provisioning Form Email Closed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Provisioning_Email_Closed</template>
    </alerts>
    <alerts>
        <fullName>Provisioning_Form_Email_Out</fullName>
        <ccEmails>ESA365@rci.rogers.com</ccEmails>
        <description>Provisioning Form Email Out</description>
        <protected>false</protected>
        <recipients>
            <recipient>Account_Provisioning_Group</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Provisioning_Email</template>
    </alerts>
    <fieldUpdates>
        <fullName>APF_Record_Type_Flip</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Provisioning_Form_2</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>APF Record Type Flip</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>APF - Save As Draft</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Account_Provisioning_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Provisioning Form %22Closed%22</fullName>
        <actions>
            <name>Provisioning_Form_Email_Closed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Provisioning_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Provisioning Form %22In Progress%22</fullName>
        <actions>
            <name>Provisioning_Form_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>APF_Record_Type_Flip</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Account_Provisioning_Form__c.Status__c</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account_Provisioning_Form__c.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Provisioning Form 1</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
