<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <fieldUpdates>
        <fullName>Update_Account_DUNS_on_MSD</fullName>
        <field>Account_DUNS__c</field>
        <formula>Account__r.Internal_Duns__c</formula>
        <name>Update Account DUNS on MSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Account_Name_on_MSD</fullName>
        <field>Account_Name__c</field>
        <formula>Account__r.Name</formula>
        <name>Update Account Name on MSD</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <rules>
        <fullName>Add Account Name to MSD Code for search</fullName>
        <actions>
            <name>Update_Account_DUNS_on_MSD</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Account_Name_on_MSD</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <description>Workflow to add Account Name to search MSD Codes.</description>
        <formula>OR(  ISNEW(),  ISCHANGED(  Account__c   )  )</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
