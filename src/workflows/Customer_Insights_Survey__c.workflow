<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Business_Assessment_Tool_Completed</fullName>
        <description>Business Assessment Tool - Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Manager_DBM__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Business_Assessment_Tool_Notification</template>
    </alerts>
    <alerts>
        <fullName>Ruby_Notification</fullName>
        <description>Ruby Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>unfiled$public/Ruby_Notification</template>
    </alerts>
    <fieldUpdates>
        <fullName>Business_Assessment_Tool_Business</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Business_Assessment_Tool_Business</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Business Assessment Tool - Business</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Business_Assessment_Tool_IT</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Business_Assessment_Tool_IT</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Business Assessment Tool - IT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>BAT Notification Email</fullName>
        <actions>
            <name>Business_Assessment_Tool_Completed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Insights_Survey__c.Status__c</field>
            <operation>equals</operation>
            <value>Completed</value>
        </criteriaItems>
        <description>Sent once status is changed to complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Business Assessment Tool - Business</fullName>
        <actions>
            <name>Business_Assessment_Tool_Business</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Insights_Survey__c.Department__c</field>
            <operation>equals</operation>
            <value>Business</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Business Assessment Tool - IT</fullName>
        <actions>
            <name>Business_Assessment_Tool_IT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Customer_Insights_Survey__c.Department__c</field>
            <operation>equals</operation>
            <value>IT</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Ruby Notification</fullName>
        <actions>
            <name>Ruby_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <formula>Or( ISCHANGED( Ruby_was_offered_to_this_client__c  ),  ISPICKVAL(Ruby_was_offered_to_this_client__c, &apos;Not Interested&apos;),  ISPICKVAL(Ruby_was_offered_to_this_client__c, &apos;Accepted&apos;),  ISPICKVAL(Ruby_was_offered_to_this_client__c, &apos;In Progress&apos;),  ISPICKVAL(Ruby_was_offered_to_this_client__c, &apos;Request More Info&apos;))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
</Workflow>
