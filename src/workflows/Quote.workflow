<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>Approval_Notification_Carrier</fullName>
        <description>Approval Notification for Carrier</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Notification_Carrier</template>
    </alerts>
    <alerts>
        <fullName>Approval_Notification_for_Finance_Team</fullName>
        <description>Approval Notification for Finance Team</description>
        <protected>false</protected>
        <recipients>
            <field>Finance_Email_Box__c</field>
            <type>email</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Template_Notification_Finance</template>
    </alerts>
    <alerts>
        <fullName>Approval_Notification_from_Finance</fullName>
        <description>Approval Notification from Finance</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Notification_Finance</template>
    </alerts>
    <alerts>
        <fullName>Approval_Notification_from_Product_Managers</fullName>
        <description>Approval Notification from Product Managers</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Notification_PM</template>
    </alerts>
    <alerts>
        <fullName>Approval_notification_Director</fullName>
        <description>Approval Notification from Director</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Notification_Director</template>
    </alerts>
    <alerts>
        <fullName>Approval_notification_Sales_Managers</fullName>
        <description>Approval Notification from Sales Managers</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Notification_SM</template>
    </alerts>
    <alerts>
        <fullName>Approval_notification_VP</fullName>
        <description>Approval Notification from VP</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Approval_Notification_CP</template>
    </alerts>
    <alerts>
        <fullName>Discount_Rejection_Email</fullName>
        <description>Discount Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Discount_Rejection_Email_VF</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_PM_rejection</fullName>
        <description>Email Notification PM rejection</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Discount_Rejection_Email</template>
    </alerts>
    <alerts>
        <fullName>Email_rejection_Notification</fullName>
        <description>Email rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Discount_Rejection_Email</template>
    </alerts>
    <alerts>
        <fullName>Expiry_Approved_Email</fullName>
        <description>Expiry Approved Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Expirary_Extension_Approved</template>
    </alerts>
    <alerts>
        <fullName>Expiry_Rejection_Email</fullName>
        <description>Expiry Rejection Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Expiry_Extension_Declined</template>
    </alerts>
    <alerts>
        <fullName>NSA_Approved_Quote</fullName>
        <description>NSA Approved Quote</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/NSA_Approved</template>
    </alerts>
    <alerts>
        <fullName>NSA_Declined_Quote</fullName>
        <description>NSA Declined Quote</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/NSA_Declined</template>
    </alerts>
    <alerts>
        <fullName>Quote_Expiry_date_Notification1</fullName>
        <description>Quote Expiry date Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Quote_expiration_Notification</template>
    </alerts>
    <alerts>
        <fullName>Reminder_Email_Alert</fullName>
        <description>Reminder Email Alert</description>
        <protected>false</protected>
        <recipients>
            <recipient>jose.camacho@rci.rogers.com.prod</recipient>
            <type>user</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>RBS/Reminder_Approval_Template</template>
    </alerts>
    <fieldUpdates>
        <fullName>Approval_Completed</fullName>
        <field>Approval_Process_Started__c</field>
        <literalValue>0</literalValue>
        <name>Approval Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Started</fullName>
        <field>Approval_Process_Started__c</field>
        <literalValue>1</literalValue>
        <name>Approval Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Tracking_Completed_Approved</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Final Approved</literalValue>
        <name>Approval Tracking Completed - Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Tracking_Completed_Recalled</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Recalled</literalValue>
        <name>Approval Tracking Completed - Recalled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Tracking_Sales_Approved</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Sales Manager Approved</literalValue>
        <name>Approval Tracking - Sales Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Tracking_Started</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Approval Started</literalValue>
        <name>Approval Tracking Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Approval_Tracking_Updated_Rejected</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Final Rejection</literalValue>
        <name>Approval Tracking Completed - Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Auto_populate_Custom_Quote_Number</fullName>
        <field>Quote_Number__c</field>
        <formula>QuoteNumber</formula>
        <name>Auto-populate Custom Quote Number</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Awaiting_Discount_Approval_Status</fullName>
        <description>After NSA Approval this field is set to Awaiting Discount Approval Submission</description>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Awaiting Discount Approval Submission</literalValue>
        <name>Awaiting Discount Approval Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Cancel_Approval_Process</fullName>
        <field>Approval_Process_Started__c</field>
        <literalValue>0</literalValue>
        <name>Cancel Approval Process</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Complete_Enterprise_Approval</fullName>
        <description>Enterprise 2 stage (3 step) approval process is completed.  NSA and Discount.</description>
        <field>In_Enterprise_Approval__c</field>
        <literalValue>0</literalValue>
        <name>Complete Enterprise Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Director_Approved</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Director Approved</literalValue>
        <name>Director Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Escalation_Completed</fullName>
        <field>Approval_Escalation_Started__c</field>
        <literalValue>0</literalValue>
        <name>Escalation Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expiry_Approver</fullName>
        <description>Expiry Extension was Approved</description>
        <field>Expiry_Extension_Status__c</field>
        <literalValue>Approved</literalValue>
        <name>Expiry Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expiry_Pending</fullName>
        <description>Sets Expiry Extension Status to Pending</description>
        <field>Expiry_Extension_Status__c</field>
        <literalValue>Pending</literalValue>
        <name>Expiry Pending</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expiry_Rejected</fullName>
        <description>Sets Expiry Extension Status to Rejected</description>
        <field>Expiry_Extension_Status__c</field>
        <literalValue>Rejected</literalValue>
        <name>Expiry Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Expiry_Reset</fullName>
        <description>Expiry Extension Reset to N/A</description>
        <field>Expiry_Extension_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>Expiry Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Large_Discount_Completed</fullName>
        <description>Used in the triggering of the time based WF for the reminder email to the PM.</description>
        <field>Large_Discount_In_Progress__c</field>
        <literalValue>0</literalValue>
        <name>Large Discount Completed</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Large_Discount_Started</fullName>
        <description>This is used to indicate if we should trigger the time-based large discount reminder.</description>
        <field>Large_Discount_In_Progress__c</field>
        <literalValue>1</literalValue>
        <name>Large Discount Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Medium_Discount_Finished</fullName>
        <field>Medium_Discount_In_Progress__c</field>
        <literalValue>0</literalValue>
        <name>Medium Discount Finished</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Medium_Discount_Started</fullName>
        <field>Medium_Discount_In_Progress__c</field>
        <literalValue>1</literalValue>
        <name>Medium Discount Started</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NSA_Approval_Complete</fullName>
        <description>NSA is Complete</description>
        <field>NSA_Approval_Complete__c</field>
        <literalValue>1</literalValue>
        <name>NSA Approval Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NSA_Approved</fullName>
        <description>The NSA Approved the Quote</description>
        <field>NSA_Approved__c</field>
        <literalValue>1</literalValue>
        <name>NSA Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NSA_Not_Approved</fullName>
        <description>The NSA did not complete the Quote yet.</description>
        <field>NSA_Approved__c</field>
        <literalValue>0</literalValue>
        <name>NSA Not Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Escalation_Complete</fullName>
        <field>Approval_Escalation_Started_Product__c</field>
        <literalValue>0</literalValue>
        <name>Product Escalation Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Product_Escalation_Update</fullName>
        <field>Approval_Escalation_Started_Product__c</field>
        <literalValue>1</literalValue>
        <name>Product Escalation Update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Rejected_By_NSA</fullName>
        <description>Rejected by the NSA in the Approval Process</description>
        <field>Discount_Approval_Status__c</field>
        <literalValue>NSA Rejected</literalValue>
        <name>Rejected By NSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Expiration_Date</fullName>
        <description>When a quote is approved for discount the expiration date will be reset to a specified number of days from today&apos;s date.</description>
        <field>ExpirationDate</field>
        <formula>today() + 60</formula>
        <name>Reset Expiration Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_NSA</fullName>
        <description>Used to reassign Approval to correct Manager</description>
        <field>NSA_Approval_Complete__c</field>
        <literalValue>0</literalValue>
        <name>Reset NSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reset_Request_for_Extension</fullName>
        <description>Resets the Request for Extension to False.</description>
        <field>requestExtension__c</field>
        <literalValue>0</literalValue>
        <name>Reset Request for Extension</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SM_Discount_Reminder</fullName>
        <description>Used to indicate that a Sales Discount Reminder needs to be sent.</description>
        <field>Sales_Discount_Reminder__c</field>
        <literalValue>1</literalValue>
        <name>SM Discount Reminder</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>SM_Notification_Reset</fullName>
        <field>Send_SM_Approval_Notification__c</field>
        <literalValue>0</literalValue>
        <name>SM Notification Reset</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Sales_Escalation_Complete</fullName>
        <field>Approval_Escalation_Started_Sales__c</field>
        <literalValue>0</literalValue>
        <name>Sales Escalation Complete</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_Director_Notification</fullName>
        <field>Send_Director_Approval_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Send Director Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Send_VP_Notification</fullName>
        <field>Send_VP_Approval_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Send VP Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Start_Enterprise_Approval</fullName>
        <description>The two stage (3 step) approval process has begun</description>
        <field>In_Enterprise_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Start Enterprise Approval</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Start_Expiry_Extension</fullName>
        <description>Begin the Expiry Extension from the Approval Process.</description>
        <field>proceedWithRequestForExtension__c</field>
        <literalValue>1</literalValue>
        <name>Start Expiry Extension</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Start_SM_Notification</fullName>
        <description>Used to trigger the SM Notification for the Approval Process as we need the Approval Notes.</description>
        <field>Send_SM_Approval_Notification__c</field>
        <literalValue>1</literalValue>
        <name>Start SM Notification</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
        <reevaluateOnChange>true</reevaluateOnChange>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Stop_Expiry_Extension</fullName>
        <description>Expiry Extension is Completed.</description>
        <field>proceedWithRequestForExtension__c</field>
        <literalValue>0</literalValue>
        <name>Stop Expiry Extension</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Submitted_to_NSA</fullName>
        <description>Submitted to the NSA in the Approval prociess</description>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Submitted to NSA</literalValue>
        <name>Submitted to NSA</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Approval_Escalation</fullName>
        <field>Approval_Escalation_Started__c</field>
        <literalValue>1</literalValue>
        <name>Update Approval Escalation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Discount_Approved</fullName>
        <field>Discount_Approval__c</field>
        <literalValue>1</literalValue>
        <name>Update Discount Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Email_Information_Quote</fullName>
        <description>Update the email information in the quote pagelayout with the email from the contact record.</description>
        <field>Email</field>
        <formula>Contact.Email</formula>
        <name>Update Email Information Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_SM_Status</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Escalated - Sales Manager</literalValue>
        <name>Update SM Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Sales_Escalation</fullName>
        <field>Approval_Escalation_Started_Sales__c</field>
        <literalValue>1</literalValue>
        <name>Update Sales Escalation</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Status</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>Escalated - Product Manager</literalValue>
        <name>Update Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_fax_information_Quote</fullName>
        <description>Update the email information in the quote pagelayout with the email from the contact record.</description>
        <field>Fax</field>
        <formula>Contact.Fax</formula>
        <name>Update fax information Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>VP_Approved</fullName>
        <field>Discount_Approval_Status__c</field>
        <literalValue>VP Approved</literalValue>
        <name>VP Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>account_name_update</fullName>
        <field>Account_email_alert__c</field>
        <formula>Opportunity.Account.Name</formula>
        <name>account name update</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>discountProcessUpdate</fullName>
        <field>discountProcessStarted__c</field>
        <literalValue>1</literalValue>
        <name>discountProcessUpdate</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>update_phone_info_Quote</fullName>
        <description>Update the phone information in the quote pagelayout with the email from the contact record.</description>
        <field>Phone</field>
        <formula>Contact.Phone</formula>
        <name>update phone info Quote</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>Approval Notification for Finance Team</fullName>
        <actions>
            <name>Approval_Notification_for_Finance_Team</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Sales Manager Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.RecordTypeId</field>
            <operation>equals</operation>
            <value>Wireline - New Enterprise Sale,Wireline - Enterprise Move,Wireline - Enterprise Contract Renewal</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Competitor_Match__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Contains_ON_NET_Only__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Sends a notification to the general mailbox for the finance team.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Auto-populate Custom Quote Number</fullName>
        <actions>
            <name>Auto_populate_Custom_Quote_Number</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>true</formula>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Carrier SM Quote Approval Reminder</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Pre_Approved_Pricing__c</field>
            <operation>equals</operation>
            <value>false</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Approval_Process_Started__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Small_Discount__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_In_Progress__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approval Started</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Account_Record_Type__c</field>
            <operation>contains</operation>
            <value>Carrier</value>
        </criteriaItems>
        <description>Sends email reminder for Quote approval. Carrier Sales Managers</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SM_Discount_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Director Notification Started</fullName>
        <actions>
            <name>Approval_notification_Director</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Director Approved</value>
        </criteriaItems>
        <description>Used to trigger the Director Approval Notification to include the Approve Notes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Enterprise SM Quote Approval Reminder</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2 AND 3 AND 4 AND 5 AND 6</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Pre_Approved_Pricing__c</field>
            <operation>equals</operation>
            <value>false</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Approval_Process_Started__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Discount</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_In_Progress__c</field>
            <operation>notEqual</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Approval Started</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.Account_Record_Type__c</field>
            <operation>notContain</operation>
            <value>Carrier</value>
        </criteriaItems>
        <description>Sends email reminder for Quote approval. Enterprise and Channel Sales Managers</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>SM_Discount_Reminder</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Expiry Approved - Notification</fullName>
        <actions>
            <name>Expiry_Approved_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Expiry_Extension_Status__c</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been approved for an expiry extension</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Expiry Rejected - Notification</fullName>
        <actions>
            <name>Expiry_Rejection_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Expiry_Extension_Status__c</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been rejected for an expiry extension</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Alert</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Quote.TotalPrice</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.IsSyncing</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Opportunity.StageName</field>
            <operation>notEqual</operation>
            <value>Closed Won,Closed Lost</value>
        </criteriaItems>
        <description>It sends and email to the opportunity and account owner when is less than 10 days from the expiry date.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Quote_Expiry_date_Notification1</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Quote.ExpirationDate</offsetFromField>
            <timeLength>-10</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Quote Approval Escalation - Product Manager Step</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Pre_Approved_Pricing__c</field>
            <operation>equals</operation>
            <value>false</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Approval_Process_Started__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_In_Progress__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Cancel stale approval for escalation using Trigger.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cancel_Approval_Process</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Product_Escalation_Update</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Approval_Escalation</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Quote Approval Escalation - Sales Step</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Opportunity.Pre_Approved_Pricing__c</field>
            <operation>equals</operation>
            <value>false</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Approval_Process_Started__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_In_Progress__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Cancel stale approval for escalation using Trigger.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Cancel_Approval_Process</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Approval_Escalation</name>
                <type>FieldUpdate</type>
            </actions>
            <actions>
                <name>Update_Sales_Escalation</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Quote Approval Reminder - Large Disc%2E</fullName>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Opportunity.Pre_Approved_Pricing__c</field>
            <operation>equals</operation>
            <value>false</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_In_Progress__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Sends email reminder for Large Discount Quote approval.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Reminder_Email_Alert</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Quote Approved - NSA Notification</fullName>
        <actions>
            <name>NSA_Approved_Quote</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Awaiting Discount Approval Submission</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.NSA_Approved__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Email Alert - Used so that we can display the notes from the Approval Process</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Approved - Notification</fullName>
        <actions>
            <name>Approval_Notification_from_Product_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4) AND 5</booleanFilter>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Final Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Empty_List_Prices_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Inconsistent_Pricing_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Competitor_Match__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Approved - Notification Carrier</fullName>
        <actions>
            <name>Approval_Notification_Carrier</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 2</booleanFilter>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Final Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.AccountRecordType__c</field>
            <operation>contains</operation>
            <value>Carrier</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been approved for Carrier</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Approved - Notification Finance</fullName>
        <actions>
            <name>Approval_Notification_from_Finance</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2 OR 3 OR 4) AND (5 AND 6)</booleanFilter>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Final Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Empty_List_Prices_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Inconsistent_Pricing_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Competitor_Match__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Contains_ON_NET_Only__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been approved finance.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Approved - Notification SM</fullName>
        <actions>
            <name>Approval_notification_Sales_Managers</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND (2  OR (3 AND 4)) AND 5</booleanFilter>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Final Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Small_Discount_Services__c</field>
            <operation>greaterThan</operation>
            <value>0</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Contains_ON_NET_Only__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Competitor_Match__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.AccountRecordType__c</field>
            <operation>notContain</operation>
            <value>Carrier</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Approved - SM Notification</fullName>
        <actions>
            <name>Approval_notification_Sales_Managers</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Medium_Discount_Finished</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 AND 3 AND (2 OR 4) AND NOT (2 OR 4)</booleanFilter>
        <criteriaItems>
            <field>Quote.Send_SM_Approval_Notification__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Medium_Discount_In_Progress__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Sales Manager Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Large_Discount_In_Progress__c</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <description>Quote was Approved by the Sales Manager - includes approval notes.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Declined - NSA Notification</fullName>
        <actions>
            <name>NSA_Declined_Quote</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>NSA Rejected</value>
        </criteriaItems>
        <description>WF Rule to prepare Email Notification when the NSA Rejects.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Quote Declined - Notification</fullName>
        <actions>
            <name>Discount_Rejection_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Final Rejection</value>
        </criteriaItems>
        <description>Email Notification when the Quote has been declined</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>SM Notification Started</fullName>
        <actions>
            <name>Start_SM_Notification</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>Sales Manager Approved</value>
        </criteriaItems>
        <description>Used to trigger the SM Approval Notification to include the Approve Notes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Start Discount Process</fullName>
        <actions>
            <name>discountProcessUpdate</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.discountProcessStarted__c</field>
            <operation>equals</operation>
            <value>False</value>
        </criteriaItems>
        <description>This controls the processing of the discount on the MRC since we want to update the discount only after the quotelineitems have been commited.</description>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Email Phone Fax info quote</fullName>
        <actions>
            <name>Update_Email_Information_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_fax_information_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>update_phone_info_Quote</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <booleanFilter>1 OR 2 OR 3</booleanFilter>
        <criteriaItems>
            <field>Quote.Email</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Phone</field>
            <operation>equals</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Quote.Fax</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Automatically update the email, fax, and phone number in the quote template, being those fields mandatory.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>VP Notification Started</fullName>
        <actions>
            <name>Approval_notification_VP</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount_Approval_Status__c</field>
            <operation>equals</operation>
            <value>VP Approved</value>
        </criteriaItems>
        <description>Used to trigger the VP Approval Notification to include the Approve Notes</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>copy account name</fullName>
        <actions>
            <name>account_name_update</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Quote.Discount</field>
            <operation>greaterThan</operation>
            <value>10</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
</Workflow>
