<?xml version="1.0" encoding="UTF-8"?>
<Workflow xmlns="http://soap.sforce.com/2006/04/metadata">
    <alerts>
        <fullName>AM_TM_Notification_T2G</fullName>
        <description>AM &gt; TM Notification</description>
        <protected>false</protected>
        <recipients>
            <recipient>Trainer_Manager_T2G</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/T2G_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>Account_Alert_Escalation_Email</fullName>
        <description>Account Alert Escalation Email</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Manager__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>unfiled$public/SUPPORTCaseescalationnotificationSAMPLE</template>
    </alerts>
    <alerts>
        <fullName>Account_Alert_Escalation_Email24</fullName>
        <description>Account Alert Escalation Email 24</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Account_Alert_Escalation24</template>
    </alerts>
    <alerts>
        <fullName>Account_Alert_Escalation_Email_48</fullName>
        <description>Account Alert Escalation Email 48</description>
        <protected>false</protected>
        <recipients>
            <field>Account_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Account_Alert_Escalation</template>
    </alerts>
    <alerts>
        <fullName>CIE</fullName>
        <ccEmails>RogersCIE@proserveit.com</ccEmails>
        <description>CIE</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/CIE_Case_Created</template>
    </alerts>
    <alerts>
        <fullName>CIE_Case_Completed</fullName>
        <description>CIE Case Completed</description>
        <protected>false</protected>
        <recipients>
            <field>Rep_Name__c</field>
            <type>userLookup</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/CIE_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Campaign_Request_Rejected_Notification</fullName>
        <description>Campaign Request Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Campaign_Request_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_Closed_Notification</fullName>
        <description>Case Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>Case_for_New_Account_Merge_Invalid</fullName>
        <description>Email Notification- New Account Request Delined</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Account_Declined_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_for_New_Account_Merged</fullName>
        <description>Notification Email - New Account Merged</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Account_Merged_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Case_for_New_Account_is_Approved</fullName>
        <description>Case for New Account is Approved .</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Account_Approved_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Comp_Case_More_Info_Required_Email</fullName>
        <description>Comp Case - More Info Required Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/Comp_Case_More_Info_Required</template>
    </alerts>
    <alerts>
        <fullName>Comp_Case_Reminder</fullName>
        <description>Comp Case - Reminder</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/Comp_Reminder_24</template>
    </alerts>
    <alerts>
        <fullName>DUNS_Update_Declined_Notification</fullName>
        <description>DUNS Update Declined Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/DUNS_Update_Declined_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>D_B_DUNS_Update_Approved_Notification</fullName>
        <description>D&amp;B DUNS Update Approved Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/DUNS_Update_Approved_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Customer_Billed</fullName>
        <description>Demo Case - Customer Billed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Demo_Case_Customer_Billed</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Rule_1</fullName>
        <description>Demo Case - Rule 1</description>
        <protected>false</protected>
        <recipients>
            <recipient>Demo_Case_Nextivity</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Demo_Case</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Rule_2</fullName>
        <description>Demo Case - Rule 2</description>
        <protected>false</protected>
        <recipients>
            <recipient>Demo_Device_Nextivity_excluding_Federal</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Demo_Case</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Rule_3</fullName>
        <description>Demo Case - Rule 3</description>
        <protected>false</protected>
        <recipients>
            <recipient>Demo_Case_Nextivity_and_Federal</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Demo_Case</template>
    </alerts>
    <alerts>
        <fullName>Demo_Case_Send_to_Billing</fullName>
        <description>Demo Case - Send to Billing</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>Business_Care</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Demo_Case_Send_to_Billing</template>
    </alerts>
    <alerts>
        <fullName>Diamond_Reciprocal_Initiative_Case</fullName>
        <description>Diamond Reciprocal Initiative Case</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/New_Diamond_Reciprocal_Initiative_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Email_Notification_When_Closed</fullName>
        <description>Email Notification When Closed</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Email_To_AM_Closed</template>
    </alerts>
    <alerts>
        <fullName>Email_Out_LCC</fullName>
        <description>Email Out</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/New_Line_Commitment_Compliance_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Email_out</fullName>
        <description>Email out</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Case_Acknowledge_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Email_out2</fullName>
        <ccEmails>sfdc.support@rci.rogers.com</ccEmails>
        <description>Email out</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_On_Boarding_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>Email_out_on_Complete</fullName>
        <description>Email out on Complete</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PRT_Complete_Response</template>
    </alerts>
    <alerts>
        <fullName>Email_out_on_Reject</fullName>
        <description>Email out on Reject</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PRT_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>Information_to_Case_Owner_Informing_about_the_Status_change</fullName>
        <description>Information to Case Owner Informing about the Status change</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Case_Status_Change_Acknowledgement</template>
    </alerts>
    <alerts>
        <fullName>Internal_Inquiry_Closed_Notification</fullName>
        <description>Internal Inquiry Closed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Internal_Inquiry_Closed_Notification</template>
    </alerts>
    <alerts>
        <fullName>Internal_Inquiry_Rejected_Notification</fullName>
        <description>Internal Inquiry Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Internal_Inquiry_Rejected_Notification</template>
    </alerts>
    <alerts>
        <fullName>Internal_Inquiry_SFDC_Closed_Support_Requestor</fullName>
        <description>Internal Inquiry SFDC Closed Support Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Internal_Inquiry_Closed_Notification</template>
    </alerts>
    <alerts>
        <fullName>Internal_Inquiry_SFDC_Support_Rejected</fullName>
        <description>Internal Inquiry SFDC Support Rejected</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Internal_Inquiry_Rejected_Notification</template>
    </alerts>
    <alerts>
        <fullName>Internal_Inquiry_SFDC_Support_Requestor</fullName>
        <description>Internal Inquiry SFDC Support Requestor</description>
        <protected>false</protected>
        <recipients>
            <field>Requester__c</field>
            <type>userLookup</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_SFDC_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>NPC_Request_Completed_Notification</fullName>
        <description>NPC Request Completed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/NPC_Complete_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Alert_case_Creation_Notification</fullName>
        <description>New Alert case Creation Notification</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Account_Alert_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Case_has_been_Assigned_with_status_New</fullName>
        <description>New Case has been Assigned with status New</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/New_Case_Acknowledge_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Engagement_Request_Accepted_Notification</fullName>
        <description>New Engagement Request Accepted Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PM_Engagement_Accepted_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Engagement_Request_Completed_Notification</fullName>
        <description>New Engagement Request Completed Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PM_Engagement_Status_Change_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Engagement_Request_Created_Notification</fullName>
        <ccEmails>Wireless.Deployments@rci.rogers.com</ccEmails>
        <description>New Engagement Request Created Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Engagement_Form_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Engagement_Request_Rejected_Notification</fullName>
        <description>New Engagement Request Rejected Notification</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PM_Engagement_Rejected_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_NPC_case_Created_Information_Mail</fullName>
        <description>New NPC case Created Notification Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <recipient>NPC_Team</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_NPC_Form_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>New_Nextivity_Email_Out</fullName>
        <description>New Nextivity Email Out</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Nextivity_Case</template>
    </alerts>
    <alerts>
        <fullName>New_Sales_Tools_On_Boarding_Request</fullName>
        <ccEmails>sfdc.support@rci.rogers.com</ccEmails>
        <description>New Sales Tools On-Boarding Request</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Case_Acknowledge_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>Notification_to_Case_Owner_Informing_about_Writer_Assigned</fullName>
        <description>Notification to Case Owner Informing about Writer Assigned</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/NPC_Writer_Assigned_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>On_Boarding_Off_Boarding_Case_Closed</fullName>
        <description>On-Boarding/Off-Boarding Case Closed</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/On_Boarding_Off_Boarding_Case_Closed</template>
    </alerts>
    <alerts>
        <fullName>PRTAR1_to_PRTRAR2_Email</fullName>
        <description>PPAR1 to PPAR2 Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Case_Acknowledge_Email_Template</template>
    </alerts>
    <alerts>
        <fullName>PRT_Case_Approved</fullName>
        <description>PRT Case Approved</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PRT_Complete_Response</template>
    </alerts>
    <alerts>
        <fullName>PRT_Case_Cancelled</fullName>
        <description>PRT Case Cancelled</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PRT_Case_Cancelled</template>
    </alerts>
    <alerts>
        <fullName>PRT_Case_Closed_Email</fullName>
        <description>PRT Case Closed Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Generic_Case_Closed_Email</template>
    </alerts>
    <alerts>
        <fullName>PRT_Case_Rejected</fullName>
        <description>PRT Case Rejected</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PRT_Approval_Rejected</template>
    </alerts>
    <alerts>
        <fullName>PRT_Group</fullName>
        <description>Price Plan Group</description>
        <protected>false</protected>
        <recipients>
            <recipient>PRT_Analysis</recipient>
            <type>group</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Case_Assigned</template>
    </alerts>
    <alerts>
        <fullName>PRT_More_Info_Required_Email</fullName>
        <description>PRT More Info Required Email</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/PRT_More_Info_Required</template>
    </alerts>
    <alerts>
        <fullName>Request_Approved_for_Critical_Fields</fullName>
        <description>Request Approved for Critical Fields</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Account_Update_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Request_Approved_for_Non_Critical_Fields_By_Others</fullName>
        <description>Request Approved for Non-Critical Fields By Others</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Account_Update_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Request_Approved_for_Non_Critical_Fields_By_Owner</fullName>
        <description>Request Approved for Non-Critical Fields By Owner</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Account_Update_Request_Approved</template>
    </alerts>
    <alerts>
        <fullName>Request_Rejected_by_Account_Owner_For_NoCrirical_by_Other</fullName>
        <description>Request Rejected by Account Owner For NoCrirical by Other</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Approval_Reject_by_Account_Owner</template>
    </alerts>
    <alerts>
        <fullName>Request_Rejected_by_Account_Owner_For_NoCrirical_by_Support</fullName>
        <description>Request Rejected by Account Owner For NoCrirical by Support</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Approval_Reject_by_Support_Team</template>
    </alerts>
    <alerts>
        <fullName>Request_Rejected_by_Account_Owner_Manager</fullName>
        <description>Request Rejected by Account Owner Manager</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Approval_Reject_by_Account_Owner_Manager</template>
    </alerts>
    <alerts>
        <fullName>Request_Rejected_by_Support_Team</fullName>
        <description>Request Rejected by Support Team</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Approval_Reject_by_Support_Team</template>
    </alerts>
    <alerts>
        <fullName>Request_Rejected_by_Support_Team_NonCritical</fullName>
        <description>Request Rejected by Support Team NonCritical By Others</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Approval_Reject_by_Support_Team</template>
    </alerts>
    <alerts>
        <fullName>TM_AM_Notifications_T2G</fullName>
        <description>TM &gt; AM Notifications</description>
        <protected>false</protected>
        <recipients>
            <type>creator</type>
        </recipients>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/T2G_More_Info_Required</template>
    </alerts>
    <alerts>
        <fullName>Update_In_Build_Email_Out</fullName>
        <description>Update In-Build Email Out</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Updated_Nextivity_Case</template>
    </alerts>
    <alerts>
        <fullName>Update_Nextivity_Email_Out</fullName>
        <description>Update Nextivity Email Out</description>
        <protected>false</protected>
        <recipients>
            <type>accountOwner</type>
        </recipients>
        <recipients>
            <type>creator</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/Updated_Nextivity_Case</template>
    </alerts>
    <alerts>
        <fullName>Wireline_Customer_Status_Update_Email_notification_to_Case_Owner_Manager</fullName>
        <description>Wireline Customer Status Update Email notification to Case Owner Manager</description>
        <protected>false</protected>
        <recipients>
            <field>Case_Owner_Manager_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/Wireline_Customer_Status_Managers_Manager</template>
    </alerts>
    <alerts>
        <fullName>Wireline_Customer_Status_Update_Rejection_Notification</fullName>
        <description>Wireline Customer Status Update Rejection Notification</description>
        <protected>false</protected>
        <recipients>
            <field>Opportunity_Owner_Email__c</field>
            <type>email</type>
        </recipients>
        <senderAddress>sfdc.support@rci.rogers.com</senderAddress>
        <senderType>OrgWideEmailAddress</senderType>
        <template>Case_Related_Emails/Wireline_Customer_Status_Update_Rejection_Notification_Template</template>
    </alerts>
    <alerts>
        <fullName>test_1</fullName>
        <description>test 1</description>
        <protected>false</protected>
        <recipients>
            <type>owner</type>
        </recipients>
        <senderType>CurrentUser</senderType>
        <template>Case_Related_Emails/New_Case_Assigned</template>
    </alerts>
    <fieldUpdates>
        <fullName>AO_Manager_Email_populator</fullName>
        <field>Account_Owner_Manager_Email__c</field>
        <formula>AO_Manager_Email__c</formula>
        <name>AO Manager Email populator</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Account_Alert_Escalation_Status_Change</fullName>
        <field>Status</field>
        <literalValue>Escalated</literalValue>
        <name>Account Alert Escalation Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Assign_to_PRT_Queue</fullName>
        <description>PRT Request</description>
        <field>OwnerId</field>
        <lookupValue>PRT_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Assign to PRT Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>CIE_Assigned</fullName>
        <field>RecordTypeId</field>
        <lookupValue>CIE_Request_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>CIE Assigned</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>PRT_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXTIBPRT1 &gt; NXTIBPRT2 Case Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_to_In_Progress1</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Status to In-Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Status_to_in_progress</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Case Status to in progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Case_Subject_Change_for_Campaign_Case</fullName>
        <field>Subject</field>
        <formula>&quot;Campaign Request:&quot;+Campaign_Name__c</formula>
        <name>Case Subject Change for Campaign Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Case_Status</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Change Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_Subject</fullName>
        <field>Subject</field>
        <formula>&quot;PRT Request for Account&quot; +&quot; &quot;+ Account.Name</formula>
        <name>Change Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_record_type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Request_Saved</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change record type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Change_to_Demo_Prime</fullName>
        <description>Demo Case</description>
        <field>RecordTypeId</field>
        <lookupValue>Demo_Device_Prime</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Change to Demo Prime</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Close_Case</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Close Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Case_Review_More_Info</fullName>
        <description>Comp Case - Initial reviewer needs more info - change record type</description>
        <field>RecordTypeId</field>
        <lookupValue>Compensation_Inquiry_Review_More_Info</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Comp Case - Review - More Info</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Case_Review_Status_Change</fullName>
        <description>Comp case - change status to show reviewer that the information has been provided</description>
        <field>Status</field>
        <literalValue>Updates Provided for Initial Review</literalValue>
        <name>Comp Case - Review - Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Case_Review_Updates_Provided</fullName>
        <description>Comp Case - send back to initial reviewer</description>
        <field>RecordTypeId</field>
        <lookupValue>Compensation_Inquiry_SM</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Comp Case - Review - Updates Provided</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Case_Submitted</fullName>
        <description>Comp Case</description>
        <field>RecordTypeId</field>
        <lookupValue>Compensation_Inquiry_SM</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Comp Case - Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Case_Submitted_Date</fullName>
        <field>Comp_Case_Compensation_Inquiry_Submitted__c</field>
        <formula>Today()</formula>
        <name>Comp Case - Submitted Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Case_Submitted_Status</fullName>
        <description>Comp Case - submission status change</description>
        <field>Status</field>
        <literalValue>Initial Review In Progress</literalValue>
        <name>Comp Case - Submitted - Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Appendix_Check</fullName>
        <field>Comp_4_2_Appendix__c</field>
        <literalValue>1</literalValue>
        <name>Comp Inquiry - Appendix Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Appendix_Uncheck</fullName>
        <field>Comp_4_2_Appendix__c</field>
        <literalValue>0</literalValue>
        <name>Comp Inquiry - Appendix Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Change_to_In_Progress</fullName>
        <description>Compensation Inquiry</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Comp Inquiry - Change to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Date_Initial_Review_Cmplt</fullName>
        <field>Comp_Initial_Review_Completed__c</field>
        <formula>Today()</formula>
        <name>Comp Inquiry - Date Initial Review Cmplt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Escalated_Date</fullName>
        <field>Comp_Case_Compensation_Inquiry_Escalated__c</field>
        <formula>Today()</formula>
        <name>Comp Inquiry - Escalated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Guarantee</fullName>
        <field>Comp_4_2_Guarantee__c</field>
        <literalValue>1</literalValue>
        <name>Comp Inquiry - Guarantee</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Guarantee_Uncheck</fullName>
        <field>Comp_4_2_Guarantee__c</field>
        <literalValue>0</literalValue>
        <name>Comp Inquiry - Guarantee Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Handbook_Check</fullName>
        <field>Comp_4_2_Handbook__c</field>
        <literalValue>1</literalValue>
        <name>Comp Inquiry - Handbook Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Handbook_Uncheck</fullName>
        <field>Comp_4_2_Handbook__c</field>
        <literalValue>0</literalValue>
        <name>Comp Inquiry - Handbook Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Owner</fullName>
        <description>Compensation Case</description>
        <field>OwnerId</field>
        <lookupValue>Internal_Compensation_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Comp Inquiry - Owner</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Quota_Letter_Check</fullName>
        <field>Comp_4_2_Quota_Letter__c</field>
        <literalValue>1</literalValue>
        <name>Comp Inquiry - Quota Letter Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Quota_Letter_Uncheck</fullName>
        <field>Comp_4_2_Quota_Letter__c</field>
        <literalValue>0</literalValue>
        <name>Comp Inquiry - Quota Letter Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Record_Change_updated</fullName>
        <description>Compensation Case</description>
        <field>RecordTypeId</field>
        <lookupValue>Compensation_Inquiry_SM</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Comp Inquiry - Record Change (updated)</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Term_Sheet_Check</fullName>
        <field>Comp_4_2_Term_Sheet__c</field>
        <literalValue>1</literalValue>
        <name>Comp Inquiry - Term Sheet Check</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Inquiry_Term_Sheet_Uncheck</fullName>
        <field>Comp_4_2_Term_Sheet__c</field>
        <literalValue>0</literalValue>
        <name>Comp Inquiry - Term Sheet Uncheck</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_More_Info_Change_Record_Type</fullName>
        <description>Compensation Case</description>
        <field>RecordTypeId</field>
        <lookupValue>Compensation_Inquiry_More_Info</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Comp More Info - Change Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Comp_Subject_Change</fullName>
        <description>Compensation Case</description>
        <field>Subject</field>
        <formula>&quot;Compensation Inquiry&quot;</formula>
        <name>Comp Subject Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Date_Time_moved_to_In_Progress</fullName>
        <field>Date_Time_moved_to_In_Progress__c</field>
        <formula>NOW()</formula>
        <name>Date/Time moved to In Progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Deal_Desk_Contract_Generated_Date</fullName>
        <description>Deal Desk Contract Generated Date</description>
        <field>PRT_Contract_Generated_Date__c</field>
        <formula>Today()</formula>
        <name>Deal Desk Contract Generated Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Demo_Subject_Change</fullName>
        <description>Demo Case</description>
        <field>Subject</field>
        <formula>&quot;Demo Device Request&quot;</formula>
        <name>Demo Subject Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB1_to_NIB9</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_9</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>INB1 to NIB9</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB2A_to_NIB10</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_10</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>INB2A to NIB10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB2A_to_NIB10_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>ABS_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>INB2A to NIB10 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB2B_to_NIB11</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_11</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>INB2B to NIB11</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB2B_to_NIB11_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>B2B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>INB2B to NIB11 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB4_to_NIB12</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_12</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>INB4 to NIB12</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB5_to_NIB13</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_13</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>INB5 to NIB13</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>INB5_to_NIB13_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>B2B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>INB5 to NIB13 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>MAl_Updated</fullName>
        <field>MAL_Updated__c</field>
        <literalValue>1</literalValue>
        <name>MAl Updated</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NIB13_to_NIB3</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_3</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NIB13 to NIB3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NIB8_to_NIB10</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_10</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NIB8 to NIB10</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NIB8_to_NIB11</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_11</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NIB8 to NIB11</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT1_to_NIB2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_2</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT1 to NIB2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT1_to_NIB2_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>B2B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXT1 to NIB2 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT2A_to_NIB3</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_3</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT2A to NIB3</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT2B_to_NIB8</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_8</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT2B to NIB8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT2B_to_NIB8_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>ABS_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXT2B to NIB8 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT3A_to_NIB4</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_4</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT3A to NIB4</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT3A_to_NIB4_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>B2B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXT3A to NIB4 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT3B_to_NIB5</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_5</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT3B to NIB5</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT3B_to_NIB5_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>B2B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXT3B to NIB5 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT5_to_NIB6</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_6</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT5 to NIB6</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT6B_to_NIB2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_3</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT6B to NIB2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT6_to_NIB7</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_7</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT6 to NIB7</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT6_to_NIB7_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>B2B_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXT6 to NIB7 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT7_to_NIB8</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_Request_8</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXT7 to NIB8</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXT7_to_NIB8_Assignment</fullName>
        <field>OwnerId</field>
        <lookupValue>ABS_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>NXT7 to NIB8 Assignment</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>NXTIBPRT1_NXTIBPRT2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Nextivity_In_Build_PRT_Request_02</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>NXTIBPRT1 to NXTIBPRT2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>New_PRT_Request_Created</fullName>
        <description>PRT Request</description>
        <field>Subject</field>
        <formula>&quot;Deal Desk Request&quot;</formula>
        <name>New PRT Request Created</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>On_Off_Boarding_Subject</fullName>
        <description>Change the subject of the On-Boarding or Off-Boarding Sales Tools Case</description>
        <field>Subject</field>
        <formula>&quot;On-Boarding/Off-Boarding Sales Tools Case&quot;</formula>
        <name>On/Off-Boarding Subject</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRTAR1_to_PRTRAR2</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Analysis_Request_02</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PPAR1 to PPAR2</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRTAR1_to_PRTRAR2_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>PRT_Analysis_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>PPAR1 to PPAR2 Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Approval_record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Request_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRT Approval record Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_Approval_Level</fullName>
        <description>PRT Case - Clone All - Change Approval Level to blank</description>
        <field>Approval_Level_PRT__c</field>
        <name>PRT Case - Clone All - Approval Level</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_L2_5_Date</fullName>
        <description>PRT Case - Clone All - Change L2.5 date to blank</description>
        <field>L2_5_PRT__c</field>
        <name>PRT Case - Clone All - L2.5 Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_L2_Date</fullName>
        <description>PRT Case - Clone All - Change L2 date to blank</description>
        <field>L2_PRT__c</field>
        <name>PRT Case - Clone All - L2 Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_L3_Date</fullName>
        <description>PRT Case - Clone All - Change L3 Date to Blank</description>
        <field>L3_PRT__c</field>
        <name>PRT Case - Clone All - L3 Date</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_PRT</fullName>
        <description>PRT Case - Clone All - Clear PRT # when cloning</description>
        <field>PRT_Number_PRT__c</field>
        <name>PRT Case - Clone All - PRT #</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_PRT_Internal_Com</fullName>
        <description>PRT Case - Clone All - Clear Internal Comments</description>
        <field>PRT_Internal_Comments_PRT__c</field>
        <name>PRT Case - Clone All - PRT Internal Com</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Null</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Case_Clone_All_Status</fullName>
        <description>PRT Case - Clone All change status to New</description>
        <field>Status</field>
        <literalValue>New</literalValue>
        <name>PRT Case - Clone All - Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Clone_All_Record_Type</fullName>
        <description>PRT Case - Clone all record type field update</description>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Request</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRT - Clone All - Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Financial_Model_Record_Type_Change</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Request_Financial_Modelling</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRT Financial Model Record Type Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Populate_Finance_Modelled</fullName>
        <field>PRT_Finance_Modelled__c</field>
        <literalValue>1</literalValue>
        <name>PRT Populate Finance Modelled</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Record_Change_More_Info</fullName>
        <description>PRT Request</description>
        <field>RecordTypeId</field>
        <lookupValue>PRT_More_Info_Required</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRT Record Change More Info</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Record_Type_to_Approver</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Approver</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRT Record Type to Approver</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Return_to_Approver_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Approver</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>PRT Return to Approver Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Status_Change_to_Provided</fullName>
        <field>Status</field>
        <literalValue>Finance Package Provided</literalValue>
        <name>PRT Status Change to Provided</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Subject_Change</fullName>
        <field>Subject</field>
        <formula>&quot;Price Plan Analysis Request for&quot;+ &quot; &quot;+ Account.Name</formula>
        <name>Price Plan Subject Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Update_More_Info_Amount</fullName>
        <description>Solution to provide the ability to view the amount of times  the Additional Information Requested button has been used by Deal Desk to request for more information</description>
        <field>PRT_of_Times_Request_Sent_for_More_Info__c</field>
        <formula>PRT_of_Times_Request_Sent_for_More_Info__c + 1</formula>
        <name>PRT Update More Info Amount</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>PRT_Update_Original_Requester</fullName>
        <description>PRT Case</description>
        <field>PRT_Original_Requester__c</field>
        <formula>CreatedById</formula>
        <name>PRT Update Original Requester</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RBATT_Status_Change</fullName>
        <description>Field update to update the RBATT Status field if the user being added is not an account manger or sales coordinator</description>
        <field>RBATT_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>RBATT Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>RCT_Status_Change</fullName>
        <description>Change the RCT status to N/A if the new user is not an account manager or sales coordinator</description>
        <field>RCT_Status__c</field>
        <literalValue>N/A</literalValue>
        <name>RCT Status Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Reassign_to_PRT</fullName>
        <description>PRT Request</description>
        <field>RecordTypeId</field>
        <lookupValue>PRT_Approver</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Reassign to PRT</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Record_Type_to_Account_Update_Submitted</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Update_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Record Type to Account Update Submitted</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Set Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Set_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Set Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Subject_Change</fullName>
        <field>Subject</field>
        <formula>&quot;Nextivity/In-build Case for &quot;+Account.Name</formula>
        <name>Subject Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>T2G_Subject_Change</fullName>
        <description>T2G Case</description>
        <field>Subject</field>
        <formula>&quot;Trainer-to-Go Request for  &quot; +  Opportunity__r.Account.Name</formula>
        <name>T2G Subject Change</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>T2G_TM</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Trainer_2_Go_Coordinator</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>AM &gt; TM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>TM_AM_T2G</fullName>
        <description>Trainer 2 Go</description>
        <field>RecordTypeId</field>
        <lookupValue>Trainer_2_Go</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>TM &gt; AM</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Field_for_Eng_Form_Record</fullName>
        <description>This field is updated when record type is set to Engagement form form (insert subject = Engagement Form: &lt;Opportunity Name&gt;)</description>
        <field>Subject</field>
        <formula>&apos;Engagement Form: &apos;+  Opportunity__r.Name</formula>
        <name>Update Case Field for Eng. Form Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Field_for_NPC_Form_Record</fullName>
        <description>This field is updated when record type is set to NPC form (insert subject = RFP Name)</description>
        <field>Subject</field>
        <formula>RFP_Name__c</formula>
        <name>Update Case Field for NPC Form Record</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_CRM_Support_Queue1</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to CRM Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_CRM_Support_Queue2</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to CRM Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Owner_to_CRM_Support_Queue4</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Case Owner to CRM Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Case Status</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Approved</fullName>
        <field>Status</field>
        <literalValue>Approved</literalValue>
        <name>Update Case Status to Approved</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_In_progress</fullName>
        <description>Update case status from new to inprogress</description>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Case Status to In-progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_In_progress2</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Case Status to In-progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_In_progress4</fullName>
        <field>Status</field>
        <literalValue>In Progress</literalValue>
        <name>Update Case Status to In-progress</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Case_Status_to_Rejected</fullName>
        <field>Status</field>
        <literalValue>Rejected</literalValue>
        <name>Update Case Status to Rejected</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Literal</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Date_Time_First_Responded</fullName>
        <field>First_Responded_Date__c</field>
        <formula>NOW()</formula>
        <name>Update Date/Time First Responded</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to CRM Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_CRM_Support_Queue</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to CRM Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_CRM_Support_Queue1</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to CRM Support Queue</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Owner_to_CRM_Support_Queue_Case</fullName>
        <field>OwnerId</field>
        <lookupValue>CRM_Support_Queue</lookupValue>
        <lookupValueType>Queue</lookupValueType>
        <name>Update Owner to CRM Support Queue Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecType_to_Account_Update_Submit1</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Update_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecType to Account Update-Submitt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecType_to_Account_Update_Submit3</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Update_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecType to Account Update-Submitt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecType_to_Account_Update_Submit5</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Update_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecType to Account Update-Submitt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_RecType_to_Account_Update_Submitt</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Update_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update RecType to Account Update-Submitt</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Record_Type</fullName>
        <field>RecordTypeId</field>
        <lookupValue>Account_Update_Submitted</lookupValue>
        <lookupValueType>RecordType</lookupValueType>
        <name>Update Record Type</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>LookupValue</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_field_for_Account_Alert</fullName>
        <field>Subject</field>
        <formula>&quot;Account Alert: &quot;+Account.Name</formula>
        <name>Update Subject field for Account Alert</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_field_for_Case</fullName>
        <field>Subject</field>
        <formula>&quot;Account Update Request:&quot;+Account.Name</formula>
        <name>Update Subject field for Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_field_for_Case_D_B_DUNs</fullName>
        <field>Subject</field>
        <formula>&quot;D&amp;B DUNs Update:&quot;+Account.Name</formula>
        <name>Update Subject field for Case D&amp;B DUNs</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_field_for_Int_Inquiry</fullName>
        <description>Prefix the subject of Internal Inquiry with &quot;Internal Inquiry: &quot;</description>
        <field>Subject</field>
        <formula>&quot;Internal Inquiry: &quot; +  Subject</formula>
        <name>Update Subject field for Int Inquiry</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <fieldUpdates>
        <fullName>Update_Subject_for_Account_Update_Case</fullName>
        <description>Set the subject of Account Update case to Account Update Request  + Account Name</description>
        <field>Subject</field>
        <formula>&quot;Account Update Request:&quot;+Account.Name</formula>
        <name>Update Subject for Account Update Case</name>
        <notifyAssignee>false</notifyAssignee>
        <operation>Formula</operation>
        <protected>false</protected>
    </fieldUpdates>
    <rules>
        <fullName>AM %3E TM</fullName>
        <actions>
            <name>AM_TM_Notification_T2G</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>T2G_Subject_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>T2G_TM</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Trainer 2 Go (Rep),Trainer 2 Go (Coordinator)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Trainer 2 Go</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Account Alert Escalation</fullName>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Alert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Email Notification on Account Alert Case Escalation</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Alert_Escalation_Email</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Account_Alert_Escalation_Status_Change</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>48</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account Alert Escalation In Progress</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Alert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Alert_Escalation_Email24</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Alert_Escalation_Email_48</name>
                <type>Alert</type>
            </actions>
            <offsetFromField>Case.Date__c</offsetFromField>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Account Alert Escalation New</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Alert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New,Escalated</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Alert_Escalation_Email24</name>
                <type>Alert</type>
            </actions>
            <timeLength>1</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
        <workflowTimeTriggers>
            <actions>
                <name>Account_Alert_Escalation_Email_48</name>
                <type>Alert</type>
            </actions>
            <actions>
                <name>Account_Alert_Escalation_Status_Change</name>
                <type>FieldUpdate</type>
            </actions>
            <timeLength>2</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Assign Back to PRT</fullName>
        <actions>
            <name>Reassign_to_PRT</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT More Info Required</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Updates Provided</value>
        </criteriaItems>
        <description>PRT Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CIE Assigned</fullName>
        <actions>
            <name>CIE</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>CIE_Assigned</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CIE Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>CIE Case Closed</fullName>
        <actions>
            <name>CIE_Case_Completed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>CIE Request Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Campaign Rejected Notification</fullName>
        <actions>
            <name>Campaign_Request_Rejected_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Campaign Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Email Notification on Campaign Request Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Status Change</fullName>
        <actions>
            <name>Information_to_Case_Owner_Informing_about_the_Status_change</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Alert</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated,In Progress,Closed</value>
        </criteriaItems>
        <description>Email Alert for Account Alert Status Change</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Case Subject Change on Eng%2E Form</fullName>
        <actions>
            <name>Update_Case_Field_for_Eng_Form_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Engagement Form</value>
        </criteriaItems>
        <description>Changes Subject on Engagement Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Case Subject Change on NPC Form</fullName>
        <actions>
            <name>Update_Case_Field_for_NPC_Form_Record</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>NPC Form</value>
        </criteriaItems>
        <description>Changes Subject on NPC Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Change Case Record Type - Account Update</fullName>
        <actions>
            <name>Record_Type_to_Account_Update_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Change Case Record Type to Account Update - Submitted when a Account Update case status is changed to In Progress</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Escalated</fullName>
        <actions>
            <name>Comp_Inquiry_Escalated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry - SM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Escalated</value>
        </criteriaItems>
        <description>Set status to In Progress once Initial Review Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Initial Review Complete</fullName>
        <actions>
            <name>Comp_Inquiry_Change_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Date_Initial_Review_Cmplt</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry - SM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Initial Review Completed</value>
        </criteriaItems>
        <description>Set status to In Progress once Initial Review Completed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Initial Subject Change</fullName>
        <actions>
            <name>Comp_Subject_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry</value>
        </criteriaItems>
        <description>Compensation Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - More Info</fullName>
        <actions>
            <name>Comp_Case_More_Info_Required_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Comp_More_Info_Change_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry - SM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Inquiry Additional Info Required</value>
        </criteriaItems>
        <description>Compensation Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - New Employee Check</fullName>
        <actions>
            <name>Comp_Inquiry_Appendix_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Guarantee</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Handbook_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Quota_Letter_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Term_Sheet_Check</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Comp_HR_Type__c</field>
            <operation>equals</operation>
            <value>New Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry,Compensation Inquiry - More Info,Compensation Inquiry - Review - More Info,Compensation Inquiry - SM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Comp_Reason_for_Claim__c</field>
            <operation>equals</operation>
            <value>New Employee</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - New Employee Uncheck</fullName>
        <actions>
            <name>Comp_Inquiry_Appendix_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Guarantee_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Handbook_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Quota_Letter_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Inquiry_Term_Sheet_Uncheck</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Comp_HR_Type__c</field>
            <operation>notEqual</operation>
            <value>New Employee</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry,Compensation Inquiry - More Info,Compensation Inquiry - Review - More Info,Compensation Inquiry - SM</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>New</value>
        </criteriaItems>
        <description>Email Notification on Compensation Inquiry submission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Comp_Case_Reminder</name>
                <type>Alert</type>
            </actions>
            <timeLength>24</timeLength>
            <workflowTimeTriggerUnit>Hours</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Review - More Info</fullName>
        <actions>
            <name>Comp_Case_More_Info_Required_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Comp_Case_Review_More_Info</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry - SM</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Initial Review Missing Info</value>
        </criteriaItems>
        <description>Compensation Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Review - Updates Provided</fullName>
        <actions>
            <name>Comp_Case_Review_Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Case_Review_Updates_Provided</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry - Review - More Info</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Updates Provided</value>
        </criteriaItems>
        <description>Compensation Inquiry - send back to initial reviewer</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Submitted</fullName>
        <actions>
            <name>Comp_Case_Submitted</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Case_Submitted_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Comp_Case_Submitted_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submit for Initial Review</value>
        </criteriaItems>
        <description>Comp Case Submission</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Comp Inquiry - Updates Provided</fullName>
        <actions>
            <name>Comp_Inquiry_Record_Change_updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Compensation Inquiry - More Info</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Updates Provided</value>
        </criteriaItems>
        <description>Compensation Inquiry</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Create Task for Restricted Account Update</fullName>
        <actions>
            <name>Restricted_Account_Update</name>
            <type>Task</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Restricted_Status_Mapping__c</field>
            <operation>equals</operation>
            <value>1</value>
        </criteriaItems>
        <criteriaItems>
            <field>Account.Account_Status__c</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <description>Creates task to notify Admins to Zero out Opportunity on Restricted Account</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>D%26B DUNS Update Approved Notification</fullName>
        <actions>
            <name>D_B_DUNS_Update_Approved_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>D&amp;B DUNS Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Email Notification on D&amp;B DUNS Update Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>DUNS Update Declined</fullName>
        <actions>
            <name>DUNS_Update_Declined_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>D&amp;B DUNS Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Declined - Invalid</value>
        </criteriaItems>
        <description>Email Notification on D&amp;B DUNS Update Declined - Invalid</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Deal Desk Contract Generated Date</fullName>
        <actions>
            <name>Deal_Desk_Contract_Generated_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Contract Generated</value>
        </criteriaItems>
        <description>Deal Desk</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Demo - Case Submit</fullName>
        <actions>
            <name>Change_to_Demo_Prime</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted to Demo Prime</value>
        </criteriaItems>
        <description>Demo Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Demo - Customer Billed</fullName>
        <actions>
            <name>Demo_Case_Customer_Billed</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Customer Billed</value>
        </criteriaItems>
        <description>Demo Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Demo - Send to Billing</fullName>
        <actions>
            <name>Demo_Case_Send_to_Billing</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Send to Billing</value>
        </criteriaItems>
        <description>Demo Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Demo - Subject Change</fullName>
        <actions>
            <name>Demo_Subject_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Demo Case - Rule 1</fullName>
        <actions>
            <name>Demo_Case_Rule_1</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted to Demo Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Nextivity_Request__c</field>
            <operation>equals</operation>
            <value>No</value>
        </criteriaItems>
        <description>Demo Case + Nextivity = False</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Demo Case - Rule 2</fullName>
        <actions>
            <name>Demo_Case_Rule_2</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted to Demo Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Nextivity_Request__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Nextivity_Federal_Request__c</field>
            <operation>notEqual</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Demo Case + Nextivity = True + Nextivity Federal = False</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Demo Case - Rule 3</fullName>
        <actions>
            <name>Demo_Case_Rule_3</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Demo Device Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted to Demo Prime</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Nextivity_Request__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Nextivity_Federal_Request__c</field>
            <operation>equals</operation>
            <value>Yes</value>
        </criteriaItems>
        <description>Demo Case + Nextivity = True + Nextivity Federal = True</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Diamond Reciprocal Initiative Case Created</fullName>
        <actions>
            <name>Diamond_Reciprocal_Initiative_Case</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Diamond Reciprocal Initiative</value>
        </criteriaItems>
        <description>Email Notification on Diamond Reciprocal Initiative Created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>Edit Rule Internal Inquiry Rejected Notification</fullName>
        <actions>
            <name>Internal_Inquiry_Rejected_Notification</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Edit Rule Internal Inquiry Rejected Notification - SFDC Support</fullName>
        <actions>
            <name>Internal_Inquiry_SFDC_Support_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Inquiry</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Engagement Case Accepted Notification</fullName>
        <actions>
            <name>New_Engagement_Request_Accepted_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Engagement Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Accepted</value>
        </criteriaItems>
        <description>Email Notification on Engagement Form Accepted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Engagement Case Completed Notification</fullName>
        <actions>
            <name>New_Engagement_Request_Completed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Engagement Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Email Notification on Engagement Form Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Engagement Case Rejected Notification</fullName>
        <actions>
            <name>New_Engagement_Request_Rejected_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Engagement Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <description>Email Notification on Engagement Form Rejected</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INB1 %3E NIB9</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>INB1_to_NIB9</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 08</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting IB - AM/CX Funding Decision</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INB2A %3E NIB10</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>INB2A_to_NIB10</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>INB2A_to_NIB10_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 09</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>IB - Client Paying</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INB2B %3E NIB11</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>INB2B_to_NIB11</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>INB2B_to_NIB11_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 09</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>IB - Funding Required/Awaiting Estimate</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INB4 %3E NIB12</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>INB4_to_NIB12</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 11</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting AM IB Documentation</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>INB5 %3E NIB13</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>INB5_to_NIB13</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>INB5_to_NIB13_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 09,Nextivity/In-Build Request 12</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>IB - Documentation Provided</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Internal Inquiry Closed Notification</fullName>
        <actions>
            <name>Internal_Inquiry_Closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Email Notification on Internal Inquiry Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Internal Inquiry Closed Notification - SFDC Support</fullName>
        <actions>
            <name>Internal_Inquiry_SFDC_Closed_Support_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email Notification on Internal Inquiry Closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Line Commitment Compliance</fullName>
        <actions>
            <name>Email_Out_LCC</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Line Commitment Compliance</value>
        </criteriaItems>
        <description>Line Commitment Compliance Case notifies account owner of case, updates subject</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>NIB13%3ENIB3</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NIB13_to_NIB3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 13</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Discussion With CX</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NIB8%3ENIB10</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NIB8_to_NIB10</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 08</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>IB - Client Paying</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NIB8%3ENIB11</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NIB8_to_NIB11</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 08</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>IB - Funding Required/Awaiting Estimate</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NPC Completed</fullName>
        <actions>
            <name>NPC_Request_Completed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>NPC Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Email Notification on NPC Case Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NPC Writer Assigned Notification</fullName>
        <actions>
            <name>Notification_to_Case_Owner_Informing_about_Writer_Assigned</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>NPC Form</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Assigned</value>
        </criteriaItems>
        <description>Email Notification on NPC Case Assigned</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT1 %3E NIB2</fullName>
        <actions>
            <name>New_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT1_to_NIB2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXT1_to_NIB2_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Subject_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Sent To B2B</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT2A %3E NIB3</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT2A_to_NIB3</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 02</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Nextivity Box Shipped</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT2B %3E NIB8</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT2B_to_NIB8</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXT2B_to_NIB8_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 02</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In-Build Required</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT3A %3E NIB4</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT3A_to_NIB4</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXT3A_to_NIB4_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 03</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>&quot;Box Ordered In RD, Replacement To B2B&quot;</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT3B %3E NIB5</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT3B_to_NIB5</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXT3B_to_NIB5_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 03</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Nextivity PRT Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT5 %3E NIB6</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT5_to_NIB6</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 03,Nextivity/In-Build Request 05</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Nextivity PRT Declined</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT6 %3E NIB7</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT6_to_NIB7</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXT6_to_NIB7_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 06,Nextivity/In-Build Request 03</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Nexitivity Unit Arranged For B2B</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT6B %3E NIB3</fullName>
        <actions>
            <name>Update_In_Build_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT6B_to_NIB2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 06</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Discussion With CX</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXT7 %3E NIB8</fullName>
        <actions>
            <name>Update_Nextivity_Email_Out</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>NXT7_to_NIB8</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXT7_to_NIB8_Assignment</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build Request 07</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In-Build Required</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>NXTIBPRT1 %3E NXTIBPRT2</fullName>
        <actions>
            <name>Case_Owner</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>NXTIBPRT1_NXTIBPRT2</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Nextivity/In-Build PRT Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Pending Nextivity PRT Approval,Pending IB PRT Approval</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account Approved Notification</fullName>
        <actions>
            <name>Case_for_New_Account_is_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved</value>
        </criteriaItems>
        <description>Email notification on New Account Approved</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account Case Notification</fullName>
        <actions>
            <name>New_Case_has_been_Assigned_with_status_New</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Account</value>
        </criteriaItems>
        <description>Email Notification on New Account Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Account Invalid</fullName>
        <actions>
            <name>Case_for_New_Account_Merge_Invalid</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Declined - Invalid</value>
        </criteriaItems>
        <description>Email notification on New Account Invalid</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account Merged</fullName>
        <actions>
            <name>Case_for_New_Account_Merged</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>New Account</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Declined - Merge</value>
        </criteriaItems>
        <description>Email notification on New Account Merged</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Account Update Case Notification</fullName>
        <actions>
            <name>New_Case_has_been_Assigned_with_status_New</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>In Progress</value>
        </criteriaItems>
        <description>Email Notification on Account Update Case In Progress</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>New Alert Case Creation  Notification</fullName>
        <actions>
            <name>New_Alert_case_Creation_Notification</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>AO_Manager_Email_populator</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Update_Subject_field_for_Account_Alert</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Alert</value>
        </criteriaItems>
        <description>Email Notification on Account Alert Created, Subject Change and Account Owner Manager established</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Campaign Case Notification</fullName>
        <actions>
            <name>New_Case_has_been_Assigned_with_status_New</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Case_Subject_Change_for_Campaign_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Campaign Request</value>
        </criteriaItems>
        <description>Email Notification on Campaign Request created, Subject Change</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New D%26B DUNs Case Notification</fullName>
        <actions>
            <name>New_Case_has_been_Assigned_with_status_New</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Subject_field_for_Case_D_B_DUNs</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>D&amp;B DUNS Update</value>
        </criteriaItems>
        <description>Email Notification on D&amp;B DUNS Update Case created, Subject Change</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Engagement Case Notification</fullName>
        <actions>
            <name>New_Engagement_Request_Created_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Engagement Form</value>
        </criteriaItems>
        <description>Email Notification on Engagement Form Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Internal Inquiry Case Notification</fullName>
        <actions>
            <name>New_Case_has_been_Assigned_with_status_New</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>Update_Subject_field_for_Int_Inquiry</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester__c</field>
            <operation>equals</operation>
        </criteriaItems>
        <description>Email Notification on Internal Inquiry Case created, Subject Change</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Internal Inquiry Case Notification - SFDC Support</fullName>
        <actions>
            <name>Internal_Inquiry_SFDC_Support_Requestor</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Internal Inquiry</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Requester__c</field>
            <operation>notEqual</operation>
        </criteriaItems>
        <description>Email Notification on Internal Inquiry Case created, Subject Change</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New NPC Case Notification</fullName>
        <actions>
            <name>New_NPC_case_Created_Information_Mail</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>NPC Form</value>
        </criteriaItems>
        <description>Email Notification on NPC Case Created</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New PRT Request Created</fullName>
        <actions>
            <name>New_PRT_Request_Created</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request</value>
        </criteriaItems>
        <description>PRT Request</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Price Plan Request Case Notification</fullName>
        <actions>
            <name>New_Case_has_been_Assigned_with_status_New</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Price Plan Analysis Request</value>
        </criteriaItems>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>New Sales Tools On-Boarding Request</fullName>
        <actions>
            <name>Email_out2</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>On_Off_Boarding_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Tools On-Boarding,Sales Tools On-Boarding,Sales Tools Off-Boarding</value>
        </criteriaItems>
        <description>Email Notification on Sales Tools On-Boarding,Sales Tools Off-Boarding Case created, Subject Change</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>On-Board Sales Role</fullName>
        <actions>
            <name>RBATT_Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>RCT_Status_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Tools On-Boarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Expected_Role__c</field>
            <operation>notEqual</operation>
            <value>Sales Rep,DBM (Dealer Business Manager),Sales Coordinator,Sales Leader</value>
        </criteriaItems>
        <description>Field Status Change Sales Tools On-Boarding Case (RBATT. RCT)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>On-Board%2FOff-Board Case Close</fullName>
        <actions>
            <name>On_Boarding_Off_Boarding_Case_Closed</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Sales Tools On-Boarding,Sales Tools Off-Boarding</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>Send email to Case Creator that the case has been closed</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Analysis Case Closed</fullName>
        <actions>
            <name>PRT_Case_Closed_Email</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Analysis Request 02</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.IsClosed</field>
            <operation>equals</operation>
            <value>True</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Case - Clone All</fullName>
        <actions>
            <name>PRT_Update_Original_Requester</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request,PRT More Info Required,PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Case_Requester_Profile__c</field>
            <operation>notEqual</operation>
            <value>Rogers Marketing</value>
        </criteriaItems>
        <description>PRT Case</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PRT Case - Clone All - Record Type</fullName>
        <actions>
            <name>PRT_Case_Clone_All_Approval_Level</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Case_Clone_All_L2_5_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Case_Clone_All_L2_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Case_Clone_All_L3_Date</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Case_Clone_All_PRT</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Case_Clone_All_PRT_Internal_Com</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Case_Clone_All_Status</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Clone_All_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request,PRT More Info Required,PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>notEqual</operation>
            <value>New</value>
        </criteriaItems>
        <description>PRT Case - when the clone all button is clicked, the record type needs to change back to the original record type with a New status</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PRT Case Approved</fullName>
        <actions>
            <name>PRT_Case_Approved</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Approved,Approved by L3 Forum,Approved by Sales VP,Approved w/ changes</value>
        </criteriaItems>
        <description>PRT Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Case Cancelled</fullName>
        <actions>
            <name>PRT_Case_Cancelled</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT More Info Required,PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>PRT Request Cancelled</value>
        </criteriaItems>
        <description>PRT Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Case Rejected</fullName>
        <actions>
            <name>PRT_Case_Rejected</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Declined</value>
        </criteriaItems>
        <description>PRT Case</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Change RT 1</fullName>
        <actions>
            <name>Change_Subject</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>Change_record_type</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request</value>
        </criteriaItems>
        <description>PRT change Record type to PRT Saved</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>PRT Change RT 2</fullName>
        <actions>
            <name>PRT_Approval_record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request Saved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Submitted__c</field>
            <operation>equals</operation>
            <value>yes</value>
        </criteriaItems>
        <description>PRT change Record type to PRT Submitted</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Financial Modelling</fullName>
        <actions>
            <name>PRT_Financial_Model_Record_Type_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Finance Model Requested</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Return from Financial Modelling</fullName>
        <actions>
            <name>PRT_Populate_Finance_Modelled</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Return_to_Approver_Record_Type</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Status_Change_to_Provided</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request - Financial Modelling</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Finance Package sent to Deal Desk</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Send Email to Creator</fullName>
        <actions>
            <name>Email_out</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request Saved</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Owner__c</field>
            <operation>notEqual</operation>
            <value>CRM Support Queue</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Submitted__c</field>
            <operation>equals</operation>
            <value>yes</value>
        </criteriaItems>
        <description>PRT Send Email to Creator</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Send Email to Creator on Complete</fullName>
        <actions>
            <name>Email_out_on_Complete</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Complete</value>
        </criteriaItems>
        <description>PRT Send Email to Creator on Complete</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Send Email to Creator on Rejected</fullName>
        <actions>
            <name>Email_out_on_Reject</name>
            <type>Alert</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request Submitted</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT Submit</fullName>
        <actions>
            <name>PRT_Record_Type_to_Approver</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Submitted to Deal Desk</value>
        </criteriaItems>
        <description>PRT Request.</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRTAR1 %3E PRTRAR2</fullName>
        <actions>
            <name>PRTAR1_to_PRTRAR2_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRT_Group</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRTAR1_to_PRTRAR2</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRTAR1_to_PRTRAR2_Queue</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Subject_Change</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Price Plan Analysis Request</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Price Plan Analysis Required</value>
        </criteriaItems>
        <description>Email Notification on Price Plan Analysis Request staus change, Subject Change, Record Type</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>PRT_More_Info</fullName>
        <actions>
            <name>PRT_More_Info_Required_Email</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>PRT_Record_Change_More_Info</name>
            <type>FieldUpdate</type>
        </actions>
        <actions>
            <name>PRT_Update_More_Info_Amount</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>PRT Request - Financial Modelling,PRT Approver</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Additional Information Required</value>
        </criteriaItems>
        <description>PRT Request</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Price Plan Request Case Closed Notification</fullName>
        <actions>
            <name>Case_Closed_Notification</name>
            <type>Alert</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Price Plan Analysis Request,Price Plan Analysis Request 02</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Closed</value>
        </criteriaItems>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Rejected Case</fullName>
        <actions>
            <name>MAl_Updated</name>
            <type>FieldUpdate</type>
        </actions>
        <active>false</active>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Rejected</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Update</value>
        </criteriaItems>
        <description>Field Status Change on rejected Account Update Case (MAL Update)</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Set Case Subject for Account Update</fullName>
        <actions>
            <name>Update_Subject_for_Account_Update_Case</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Account Update</value>
        </criteriaItems>
        <description>Set the Subject for Account Update case when the case is crated</description>
        <triggerType>onCreateOnly</triggerType>
    </rules>
    <rules>
        <fullName>TM %3E AM</fullName>
        <actions>
            <name>TM_AM_Notifications_T2G</name>
            <type>Alert</type>
        </actions>
        <actions>
            <name>TM_AM_T2G</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Trainer 2 Go (Coordinator)</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Additional Information Required</value>
        </criteriaItems>
        <description>Trainer 2 Go</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Update Time Stamp for First Response</fullName>
        <actions>
            <name>Update_Date_Time_First_Responded</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND( OR( ISPICKVAL(Status, &quot;Approved&quot;), ISPICKVAL(Status, &quot;Rejected&quot;), ISPICKVAL(Status, &quot;More Information Required&quot;), ISPICKVAL(Status, &quot;Awaiting Approval&quot;), ISPICKVAL(Status, &quot;Complete&quot;)), (ISBLANK(First_Responded_Date__c)), AND( OR( RecordType.Id = &quot;012i0000000CGrX&quot;, RecordType.Id = &quot;012i00000019zyt&quot;,   RecordType.Id = &quot;012i00000019zyu&quot;, RecordType.Id = &quot;012i0000000AxLD&quot;, RecordType.Id = &quot;012i0000000Ay1M&quot; )))</formula>
        <triggerType>onAllChanges</triggerType>
    </rules>
    <rules>
        <fullName>Update Time Stamp for Status Change</fullName>
        <actions>
            <name>Date_Time_moved_to_In_Progress</name>
            <type>FieldUpdate</type>
        </actions>
        <active>true</active>
        <formula>AND (ISPICKVAL(Status, &quot;In Progress&quot;), (ISBLANK(Date_Time_moved_to_In_Progress__c)), AND( OR( RecordType.Id = &quot;012i0000000ATPJ&quot;, RecordType.Id = &quot;012i00000019zyt&quot;,  RecordType.Id = &quot;012i00000019zyu&quot;,  RecordType.Id = &quot;012i0000000AxLD&quot;,  RecordType.Id = &quot;012i0000000Ay1M&quot;  ) ) )</formula>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
    </rules>
    <rules>
        <fullName>Wireline Customer Status Update - 7 day reminder</fullName>
        <active>true</active>
        <criteriaItems>
            <field>Case.RecordTypeId</field>
            <operation>equals</operation>
            <value>Wireline Customer Status Update</value>
        </criteriaItems>
        <criteriaItems>
            <field>Case.Status</field>
            <operation>equals</operation>
            <value>Awaiting Approval</value>
        </criteriaItems>
        <description>If Sales Manager has not yet approved the Case 7 days after case creation, email alert will be sent to the manager’s manager (Case Owner Manager email field).</description>
        <triggerType>onCreateOrTriggeringUpdate</triggerType>
        <workflowTimeTriggers>
            <actions>
                <name>Wireline_Customer_Status_Update_Email_notification_to_Case_Owner_Manager</name>
                <type>Alert</type>
            </actions>
            <timeLength>7</timeLength>
            <workflowTimeTriggerUnit>Days</workflowTimeTriggerUnit>
        </workflowTimeTriggers>
    </rules>
    <tasks>
        <fullName>Restricted_Account_Update</fullName>
        <assignedToType>role</assignedToType>
        <description>• Export all records in the following objects:
- Opportunities
- Opportunity Product
- Opportunity Product Schedule 
- MSD Snapshot
• Backup the extracts
• Zero out all revenue related fields in the records, &amp; upload back to SalesForce</description>
        <dueDateOffset>1</dueDateOffset>
        <notifyAssignee>true</notifyAssignee>
        <offsetFromField>Case.ClosedDate</offsetFromField>
        <priority>High</priority>
        <protected>false</protected>
        <status>Not Started</status>
        <subject>Restricted Account Update</subject>
    </tasks>
</Workflow>
