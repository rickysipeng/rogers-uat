public class SSOMobileAutoRedirectController {

    public PageReference redirect() {
        PageReference pgRef = new PageReference('https://esf.rci.rogers.com/affwebservices/public/saml2sso?SPID=https://rogersb2bmobile.my.salesforce.com&RelayState=' + ApexPages.currentPage().getParameters().get('RelayState'));
        return pgRef;
    }

}