/*
===============================================================================
 Class Name   : Test_TAG_AccountMSDOwnerTransferCont
===============================================================================
PURPOSE:    This class a controller class for TAG_AccountMSDOwnerTransferController.
              
Developer: Deepika Rawat
Date: 02/05/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
02/05/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_AccountMSDOwnerTransferCont{
    private static Assignment_Request__c assignReq = new Assignment_Request__c();
    private static Assignment_Request_Item__c reqItem;
    private static Account ParentAcc;
    private static Account ParentAcc2;
    private static Account ParentAcc3;
    private static Account ChildAcc1;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static User userRec2;
    private static User userRec3;
    private static  MSD_Code__c msdCode; 
    private static  MSD_Code__c msdCode2; 
    private static  MSD_Code__c msdCode3; 
    private static Shared_MSD_Code__c sharedMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD2;
    private static Shared_MSD_Accounts__c sharedAccMSD3;
    private static District__c newDist;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
    private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.User_Location__c = 'Central,Eastern,National,Western';
        TAG_CS.User_Region__c = 'Alberta,Atlantic,British Columbia,National,Ontario,Prairies,Quebec';
        TAG_CS.User_Channel_1__c = 'ABS,BIS,Business Customer Enablement,Business Customer Loyalty,Business Customer Relations,Business National Support,Business Segment,Commercial Service Delivery,Dealer,Enterprise,Enterprise Service Assurance';
        TAG_CS.User_Channel_2__c = 'Enterprise,Service Delivery,Federal,Field Sales Cable,Large,M2M Reseller,Medium,,MSA,NIS,Public Sector,RBS,RCI,Sales Operations,Small,VAR';  
        insert TAG_CS;
        staticData_PageSize.Name = 'AccountTransfer_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='AccountTransfer_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;
        
        system.debug('pEmp***'+pEmp);
        
        User testu = new User(LastName = 'Mark O’Roger1', Alias = 'er112', Email='test@Rogertest.com', Username='test@Rogertest12.com', CommunityNickname = 'nick23Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert testu;
         User testu1 = new User(LastName = 'Mark O’Rogerdsfd1', ManagerId = testu.id, Alias = 'ty112', Email='test@Rogertest.com', Username='test@Trseegertest12.com', CommunityNickname = 'nicw3Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Assignment_Approver__c = testu.id );
        insert testu1;

        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c=testu1.id);
        insert userRec;

        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment',Owner_Type__c='District');
        insert userRec2;

         userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment', Owner_Type__c='Account');
        insert userRec3;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.BillingPostalCode= 'A1A 1A1';
        insert ChildAcc1;
        //Account 2
        ParentAcc2= new Account();
        ParentAcc2.Name = 'ParentAcc2';
        ParentAcc2.Account_Status__c = 'Assigned';
        ParentAcc2.ParentID= null;
        ParentAcc2.OwnerId= userRec3.id;
        ParentAcc2.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc2;
        //Account 3
        ParentAcc3= new Account();
        ParentAcc3.Name = 'ParentAcc3';
        ParentAcc3.Account_Status__c = 'Assigned';
        ParentAcc3.ParentID= null;
        ParentAcc3.OwnerId= userRec3.id;
        ParentAcc3.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc3;

        msdCode = new MSD_Code__c();
        msdCode.OwnerId = userRec.id ;
        msdCode.Account__c = ParentAcc.id ;
        msdCode.MSD_Code_External__c = '2009';
        insert msdCode;

        msdCode2 = new MSD_Code__c();
        msdCode2.OwnerId = userRec.id ;
        msdCode2.Account__c = ParentAcc2.id ;
        msdCode2.MSD_Code_External__c = '7891';
        insert msdCode2 ;

        msdCode3 = new MSD_Code__c();
        msdCode3.OwnerId = userRec.id ;
        msdCode3.Account__c = ParentAcc3.id ;
        msdCode3.MSD_Code_External__c = '7892';
        insert msdCode3;

        Shared_MSD_Code__c sharedMSD2 = new Shared_MSD_Code__c();
        sharedMSD2.OwnerId = userRec.id ;
        sharedMSD2.Name = 'SharedMSD2';
        sharedMSD2.MSD_Code_External__c = '9876';
        insert sharedMSD2;

        sharedAccMSD3 = new Shared_MSD_Accounts__c();
        sharedAccMSD3.Account__c=ParentAcc2.id;
        sharedAccMSD3.Shared_MSD_Code__c=sharedMSD2.id;
        insert sharedAccMSD3;

        sharedMSD = new Shared_MSD_Code__c();
        sharedMSD.OwnerId = userRec.id ;
        sharedMSD.Name = 'SharedMSD1';
        sharedMSD.MSD_Code_External__c = '12345';
        insert sharedMSD;

        sharedAccMSD = new Shared_MSD_Accounts__c();
        sharedAccMSD.Account__c=ParentAcc.id;
        sharedAccMSD.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD;

        sharedAccMSD2 = new Shared_MSD_Accounts__c();
        sharedAccMSD2.Account__c=ParentAcc2.id;
        sharedAccMSD2.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD2;

        newDist = new District__c();
        newDist.Name='New Distrct';
        newDist.District_Owner__c=userRec2.id;
        insert newDist;

    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testSearchAccount(){
        setUpData(); 
        test.startTest();
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.AccountName='ParentAcc';
            obj.search();
            system.assert(obj.showSection2 =True);
            system.debug(obj.lstAllResultAccWrap.size()>0);
            obj.next();
        //    obj.previous();
            obj.backtosearch();
        test.stopTest(); 
    }
      /*******************************************************************************************************
    * @description: This is a negative test method with invalid search criteria.
    *******************************************************************************************************/
    static testmethod void testSearchAccountNegative(){
        setUpData(); 
        test.startTest();
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='Acc';
            obj.searchFilterData.Channel='BIS';
            obj.searchFilterData.Region='National';
            obj.searchFilterData.Location='National';
            obj.searchFilterData.DUNS='5656';
            obj.searchFilterData.OwnerName='Acc';
            obj.searchFilterData.Segment='Large';
            obj.searchFilterData.MSDCode='6767';
            obj.searchFilterData.MSDName='Acc';
            obj.search();
            
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name and Transfer Owner.
    *******************************************************************************************************/
    static testmethod void testSearchAccountTransferOwner(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='ParentAcc';
            obj.search();
            system.assert(obj.showSection2 =True);
            obj.lstSetController[0].isSelected =true;
            obj.dolstSelectedRecords();
            obj.openAccountTransfer();
            obj.closeAccountTransfer();
            obj.openAccountTransfer();
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.accUserSelect.TAG_New_Owner__c =userRec3.id;
            obj.transferOwnership();
            obj.ProcessNext();
            obj.finalSelectList.add(obj.lstSetController[0]);
            obj.finalSelectList[0].removed=true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.backtoStep2();
             obj.lstSetController[0].isSelected =true;
            obj.dolstSelectedRecords();
            obj.openAccountTransfer();
            obj.closeAccountTransfer();
            obj.openAccountTransfer();
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.accUserSelect.TAG_New_Owner__c =userRec3.id;
            obj.transferOwnership();
            obj.ProcessNext();
            obj.finalSelectList.add(obj.lstSetController[0]);
            obj.finalSelectList[0].removed=true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.createAssigmentRecs();
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name and Transfer District.
    *******************************************************************************************************/
    static testmethod void testSearchAccountTransferDistrict(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='ParentAcc';
            obj.search();
            system.assert(obj.showSection2 =True);
            obj.lstSetController[0].isSelected =true;
            obj.dolstSelectedRecords();
            obj.openDistrict();
            obj.closeDistrict();
            obj.openDistrict();
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.districtVal.District_Active__c =newDist.id;
            obj.ProcessNext();
            obj.changeDistrict();
            obj.createAssigmentRecs();
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD .
    *******************************************************************************************************/
    static testmethod void testSearchMSDTransferOwner(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='ParentAcc3';
            obj.search();
            system.assert(obj.showSection2 =True);
            system.debug('obj.lstSetController****'+obj.lstSetController);
            obj.lstSetController[0].lstMSDCodeWrap[0].isSelected =true;
            obj.dolstSelectedRecords();
            obj.openAccountTransfer();
            obj.closeAccountTransfer();
            obj.openAccountTransfer();
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.accUserSelect.TAG_New_Owner__c=userRec3.id;
            obj.ProcessNext();
            obj.transferOwnership();
            obj.finalSelectList.add(obj.lstSetController[0]);
            obj.finalSelectList[0].lstMSDCodeWrap[0].removed=true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.createAssigmentRecs();
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD .
    *******************************************************************************************************/
    static testmethod void testSearchMSDTransferDistrict(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2 =True);
            system.debug('obj.lstSetController****'+obj.lstSetController);
            obj.lstSetController[0].lstMSDCodeWrap[0].isSelected =true;
             obj.dolstSelectedRecords();
            obj.openDistrict();
            obj.closeDistrict();
            obj.openDistrict();
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.districtVal.District_Active__c =newDist.id;
            obj.changeDistrict();
            obj.ProcessNext();
            obj.finalSelectList[0].lstMSDCodeWrap[0].removed=true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.createAssigmentRecs();
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Shared MSD .
    *******************************************************************************************************/
    static testmethod void testSearch_SharedMSDTransferOwner(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2 =True);
            system.debug('obj.lstSetController****'+obj.lstSetController);
            obj.lstSetController[0].lstSharedMSDCodeWrap[0].isSelected =true;
            obj.dolstSelectedRecords();
            obj.openAccountTransfer();
            obj.closeAccountTransfer();
            obj.openAccountTransfer();
            
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.accUserSelect.TAG_New_Owner__c =userRec3.id;
            obj.ProcessNext();
            obj.transferOwnership();
            obj.finalSelectList.add(obj.lstSetController[0]);
            obj.finalSelectList[0].lstSharedMSDCodeWrap[0].removed=true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.createAssigmentRecs();
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as SharedMSD .
    *******************************************************************************************************/
    static testmethod void testSearch_SharedMSDTransferDistrict(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
            obj.getreasonCodevalue();
            obj.getAccFields();
            obj.searchFilterData.AccountName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2 =True);
            system.debug('obj.lstSetController****'+obj.lstSetController);
            obj.lstSetController[0].lstSharedMSDCodeWrap[0].isSelected =true;
             obj.dolstSelectedRecords();
            obj.openDistrict();
            obj.closeDistrict();
            obj.openDistrict();
            obj.businessCare='Test Case';
            obj.reasonCode='Test Reason';
            obj.districtVal.District_Active__c =newDist.id;
            obj.ProcessNext();
            obj.changeDistrict();
            obj.finalSelectList[0].lstSharedMSDCodeWrap[0].removed=true;
            obj.undoRemoveItems();
            obj.removeItems();
            obj.createAssigmentRecs();
        }
        test.stopTest(); 
    }
     static testmethod void testExixtingReqItem(){
     setUpData(); 
        Test.startTest();
        system.runAs(userRec){
        assignReq = new Assignment_Request__c();
        assignReq.Request_Type__c = 'Mixed';
        assignReq.Status__c = 'Processed';
        assignReq.Approval_Override__c=false;
        insert assignReq;

        reqItem = new Assignment_Request_Item__c();
        reqItem.Assignment_Request__c = assignReq.id;
        reqItem.Account__c = ParentAcc.id;
        reqItem.Approval_Override__c = assignReq.Approval_Override__c;
        reqItem.Business_Case__c = 'Test Business Case';
        reqItem.Current_Approver__c =userRec.Id;
        reqItem.Current_Channel__c = 'Large';
        reqItem.Current_Owner__c= userRec2.id;       
        reqItem.Reason_Code__c = '1 - Channel Alignment to Small';
        reqItem.New_Owner__c= userRec3.id; 
        reqItem.New_Approver__c=userRec2.Id;
        reqItem.New_Channel__c= 'BIS';
        reqItem.Requested_Item_Type__c='Account Owner';
        insert reqItem;
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(reqItem.id);
        Approval.ProcessResult result = Approval.process(req1);

        TAG_AccountMSDOwnerTransferController obj = new TAG_AccountMSDOwnerTransferController();
        obj.getreasonCodevalue();
        obj.getAccFields();
        obj.searchFilterData.AccountName='ParentAcc2';
        obj.search();

        }
    Test.stopTest();
    }
}