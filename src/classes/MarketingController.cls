/*
Feb 24, 2015 Replace Opportunity.Actual_Sign_Date__c with Opportunity.CloseDate
*/
global without sharing class MarketingController {
    public String scrollingText {get; private set;}   
    public String HTMLpageText {get; set;}
    public MarketingController() {
        try{
        scrollingText = ([SELECT id, Value__c FROM Custom_Property__c WHERE Name = 'Marketing VF Marquee' LIMIT 1]).Value__c;
        }catch(Exception ex){
            
        }
        
        if (Test.IsRunningTest())
       {    // PageReference dashboard = new PageReference('/01Z30000000iYZt');
            PageReference dashboard = new PageReference('/01Z30000000jA4L');
            
            HTMLpageText ='UNIT.TEST';
       }
       else
       {
            // PageReference dashboard = new PageReference('/01Z30000000iYZt');
        PageReference dashboard = new PageReference('/01Z30000000jA4L');
        HTMLpageText = dashboard.getContent().toString();
       }
        
     
                
     }
     
     
    public String updatePage(){
        return null;
    }
    @RemoteAction 
    global static String updateMarquee(){
        return ([SELECT id, Value__c FROM Custom_Property__c WHERE Name = 'Marketing VF Marquee' LIMIT 1]).Value__c;
    }
    
    @RemoteAction 
    global static List<SalesTVComponents__c> getDashboardComponentProperties(){
        return ([SELECT id, Title__c, Footer__c, Source__c, Height__c FROM SalesTVComponents__c]);
    }
    
    @RemoteAction 
    global static String getNumberOfWonOppsForMonth(){
        return '' + ([SELECT count() FROM Opportunity WHERE (NOT(Account_Record_Type__c LIKE 'Carrier%')) AND CloseDate >= THIS_MONTH AND CloseDate <= NEXT_MONTH and StageName= 'Contracted' AND Net_Change_MRR__c >= 0]);
    }
    

  

}