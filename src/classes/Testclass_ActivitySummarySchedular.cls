/*Class Name :Testclass_ActivitySummarySchedular.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :20/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = False)
private class Testclass_ActivitySummarySchedular{
    static testmethod void vanillaTestCreate(){
        Test.StartTest();
        BatchProcess_Admin__c BA= new BatchProcess_Admin__c();
        BA.Name='SysAdm1';
        BA.Email__c='sfdc.support@rci.rogers.com';
        Upsert BA;
        
        Activity_Log__c aL= new Activity_Log__c();
        aL.RPC_Call__c=true;
        aL.Activity_Type__c='Test@ActivitySummarySchedular';
        aL.Activity_Date__c=System.today();
        aL.Sales_Rep__c=UserInfo.getUserid();
        insert aL;
        
        Activity_Log__c aL1= new Activity_Log__c();
        aL1.RPC_Call__c=False;
        aL1.Activity_Type__c='Test@ActivitySummarySchedular';
        aL1.Activity_Date__c=System.today();
        aL1.Sales_Rep__c=UserInfo.getUserid();
        insert aL1;
        
        
        Static_Data_Utilities__c ssu= new Static_Data_Utilities__c();
        ssu.Name='Activity Update Checking Period';
        ssu.Value__c='1';
        insert ssu;
        
        Static_Data_Utilities__c ssu1= new Static_Data_Utilities__c();
        ssu1.Name='Activity Summary Cleaned';
        ssu1.Value__c='True';
        insert ssu1;
        
        
        ActivitySummarySchedular obj= new ActivitySummarySchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('ActivitySummarySchedular', sch, obj);
        System.assert(true);
        Test.stopTest();  
        
    }
    static testmethod void vanillaTestDelete(){
        Test.StartTest();
        BatchProcess_Admin__c BA= new BatchProcess_Admin__c();
        BA.Name='SysAdm1';
        BA.Email__c='sfdc.support@rci.rogers.com';
        Upsert BA;
        
        Activity_Log__c aL= new Activity_Log__c();
        aL.RPC_Call__c=true;
        aL.Activity_Type__c='TEST@ACTS';
        aL.Activity_Date__c=System.today();
        aL.Sales_Rep__c=UserInfo.getUserid();
        insert aL;
        
        Activity_Log__c AL1=[select id,Name,OwnerMonthType__c,Activity_Date__c,Activity_Type__c from Activity_Log__c where id=:aL.id];
        
        Activity_Summary__c newAccSummary= new Activity_Summary__c();
        newAccSummary.Activity_Month__c=Decimal.ValueOf(String.valueof(AL1.Activity_Date__c).SubString(0,4)+String.valueof(AL1.Activity_Date__c).SubString(5,7));
        newAccSummary.Start_of_Activity_Month__c=Date.Valueof(string.valueof(AL1.Activity_Date__c).SubString(0,4)+'-'+string.valueof(AL1.Activity_Date__c).SubString(5,7)+'-01 00:00:00');
        newAccSummary.Activity_Type__c=AL1.Activity_Type__c;
        newAccSummary.OwnerMonthType__c=AL1.OwnerMonthType__c;
        newAccSummary.Ownerid=UserInfo.getUserid();
        newAccSummary.Activity_Target__c=1;
        newAccSummary.Total_Number_of_Activity__c= 1;
        newAccSummary.RPC_Call__c=1;

        
        Static_Data_Utilities__c ssu= new Static_Data_Utilities__c();
        ssu.Name='Activity Update Checking Period';
        ssu.Value__c='1';
        insert ssu;
        
        Static_Data_Utilities__c ssu1= new Static_Data_Utilities__c();
        ssu1.Name='Activity Summary Cleaned';
        ssu1.Value__c='False';
        insert ssu1;
        
        
        ActivitySummarySchedular obj1= new ActivitySummarySchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('ActivitySummarySchedular', sch, obj1);
        System.assert(true);
        Test.stopTest();   

    }
}