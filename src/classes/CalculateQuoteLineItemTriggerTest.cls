@isTest(seeAllData=true) 
private class CalculateQuoteLineItemTriggerTest {
        
    private static Opportunity testOpp;
    private static Quote__c testQuote;
    private static Quote_Line_Item__c testQuoteLineItem;
    private static Product2 testProd2;
    private static list<PricebookEntry> thePBEList;
    
    private static void initData() {
        
        // get all aggregated products
        /*thePBEList = new list<PricebookEntry>([
            select Id, Product2.Family from PricebookEntry 
                where Product2.Aggregated_Product__c = true and Product2.Family != null and Product2.IsActive = true]);*/
        thePBEList = new list<PricebookEntry>([
           select Id, Name, Product2.Family from PricebookEntry 
            Where Name IN ('Business Resumption Total', 'Facilities Management Total','Connectivity Total','Cloud Total','Managed Services Total','Colocation Total') 
            AND PriceBook2.Name='Data Centre Price Book' AND Product2.Aggregated_Product__c = true 
            and Product2.Family != null and Product2.IsActive = true LIMIT 100]);
        system.debug('==============>thePBEList'+thePBEList);
        
        testOpp = new Opportunity(Name = 'testOpp',
                                StageName = 'testOpp',
                                CloseDate = Date.today().addDays(2));
        insert testOpp;
         // create new OLI from PBE list
        list<OpportunityLineItem> theNewOLIList = new list<OpportunityLineItem>();
        map<Id, string> thePBEFamilyMap = new map<Id, string>(); // we need this to reference later as we are only setting the pbe.Id and that doesn't populate the family
        for (PricebookEntry pbe : thePBEList) {
            OpportunityLineItem oli = new OpportunityLineItem();
            oli.UnitPrice = 0;
            oli.Quantity = 1;
            oli.PricebookEntryId = pbe.Id;
            oli.Line_Item_Product_Family__c = pbe.Product2.Family;
            
            thePBEFamilyMap.put(pbe.Id, pbe.Product2.Family);
             oli.OpportunityId = testOpp.Id; // will do this later on
            
            theNewOLIList.add(oli);
        }
        system.debug(theNewOLIList);
        
        
    
        testQuote = new Quote__c(Opportunity_Name__c = testOpp.Id);
        insert testQuote;
        
        testProd2 = new Product2(Name = 'testName');
        insert testProd2;
        
        testQuoteLineItem = new Quote_Line_Item__c(
                                Name = 'test',
                                Price__c = 1.0, 
                                Type__c = 'Custom',
                                Quote__c = testQuote.Id,
                                Commission_Type__c = 'New Revenue',
                                Product__c = testProd2.Id,
                                RollupType__c = '123',
                                RollupValue__c = 123,
                                Service_Class__c = '123',
                                Sub_Class__c = '123',
                                Service_Group__c = '123',
                                Family__c = '123');
        insert testQuoteLineItem;
        
        ApexPages.currentPage().getParameters().put('qid', String.valueOf(testQuoteLineItem.Id));
    }
    
    private static testMethod void testSimpleInsertUpdate() {
        initData();
        
        Test.startTest();
        testQuoteLineItem.Quantity__c = 1;
        testQuoteLineItem.Price__c = 0;
        update testQuoteLineItem;
        
        testQuoteLineItem.Price__c = 1.0;
        testQuoteLineItem.Discount__c = null;
        update testQuoteLineItem;
        
        delete testQuoteLineItem;
        Test.stopTest();
        
   //     System.assertEquals(thePBEList.size(), [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testWithDifferenetRevenueTypes() {
        initData();
        
        Test.startTest();
        testProd2.Revenue_Type__c = thePBEList.get(2).Product2.Family;
        update testProd2;
        
        testQuoteLineItem.Commission_Type__c = 'Service Upgrade';
        update testQuoteLineItem;
        
        testProd2.Revenue_Type__c = thePBEList.get(1).Product2.Family;
        update testProd2;
        
        update testQuoteLineItem;
        Test.stopTest();
        
   //     System.assertEquals(6, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testProductPeriod(){
        initData();
        
        Test.startTest();
        testProd2.Period__c = '1';
        update testProd2;
        
        update testQuoteLineItem;
        
        testQuoteLineItem.Quantity__c = 1;
        testQuoteLineItem.Price__c = 0;
        update testQuoteLineItem;
        Test.stopTest();
        
   //     System.assertEquals(6, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testCommissionAndRevenue(){
        initData();
        
        Test.startTest();
        testQuoteLineItem.Commission_Type__c = null;
        update testQuoteLineItem;
        
        testProd2.Revenue_Type__c = thePBEList.get(2).Product2.Family;
        update testProd2;
        
        update testQuoteLineItem;
        
        testProd2.Revenue_Type__c = thePBEList.get(1).Product2.Family;
        update testProd2;
        
        update testQuoteLineItem;
        Test.stopTest();
        
  //      System.assertEquals(6, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testDiscountAndQuantity(){
        initData();
        
        Test.startTest();
        testQuoteLineItem.Discount__c = 5;
        update testQuoteLineItem;
        
        testQuoteLineItem.Quantity__c = null;
        update testQuoteLineItem;
        Test.stopTest();
        
   //     System.assertEquals(6, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testPriceDiscountQuantity(){
        initData();
        
        Test.startTest();
        testQuoteLineItem.Price__c = 1.0;
        testQuoteLineItem.Discount__c = null;
        update testQuoteLineItem;
        
        testQuoteLineItem.Discount__c = 5;
        update testQuoteLineItem;
        
        testQuoteLineItem.Quantity__c = null;
        update testQuoteLineItem;
        Test.stopTest();
        
    //    System.assertEquals(6, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testWithoutCreationOppProducts(){
        initData();
        
        Test.startTest();
        Quote_Line_Item__c childQLI = new Quote_Line_Item__c(
                                Name = 'child test',
                                Price__c = 1.0, 
                                Type__c = 'Standard',
                                Quote__c = testQuote.Id,
                                Commission_Type__c = 'New Revenue',
                                Product__c = testProd2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id);
        insert childQLI;
        Test.stopTest();
        
   //     System.assertEquals(6, [SELECT Id FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id].size()); // 2.0 opportunity will always have a set of products
    }
    
    private static testMethod void testWithCreationOppProducts(){
        initData();
        
        Test.startTest();
        testProd2.Period__c = '1';
        testProd2.Revenue_Type__c = thePBEList.get(0).Product2.Family;
        update testProd2;
        
        testQuoteLineItem.Type__c = 'Standard';
        testQuoteLineItem.Quantity__c = 1;
        update testQuoteLineItem;
        
        testQuote.Syncing__c = TRUE;
        update testQuote;
        
        Quote_Line_Item__c newQLI = new Quote_Line_Item__c(
                                Name = 'test',
                                Price__c = 1000.0,
                                Quantity__c = 1,
                                Type__c = 'Standard',
                                Quote__c = testQuote.Id,
                                Commission_Type__c = 'New Revenue',
                                Product__c = testProd2.Id);
        insert newQLI;
        Test.stopTest();
        
        // expect one line with price of 1001
        //system.assertEquals(1001, [SELECT UnitPrice FROM OpportunityLineItem WHERE OpportunityId = :testOpp.Id and UnitPrice > 0].get(0).UnitPrice);
    }
    
    private static testMethod void testUnitPriceChanges(){
        initData();
        
        testQuote.Syncing__c = TRUE;
        update testQuote;
        
        testProd2.Revenue_Type__c = thePBEList.get(1).Product2.Family;
        testProd2.Family = thePBEList.get(1).Product2.Family;
        testProd2.Period__c = '1';
        update testProd2;
        
        Test.startTest();
        Quote_Line_Item__c newQLI = new Quote_Line_Item__c(
                Name = 'new test',
                Type__c = 'Standard',
                Commission_Type__c = 'New Revenue',
                Price__c = 1000,
                Quantity__c = 1,
                Discount__c = 0,
                Line__c = 1,
                Period__c = '1',
                Quote__c = testQuote.Id,
                Product__c = testProd2.Id);
        
        insert newQLI;
        //Decimal voiceInsertEMRR = [SELECT UnitPrice FROM OpportunityLineItem WHERE Line_Item_Product_Family__c =: thePBEList.get(1).Product2.Family limit 1].UnitPrice;
        
        newQLI.Price__c = 1500;
        update newQLI;
        //Decimal voiceUpdateEMRR = [SELECT UnitPrice FROM OpportunityLineItem WHERE Line_Item_Product_Family__c =: thePBEList.get(1).Product2.Family limit 1].UnitPrice;
        
        newQLI.Quantity__c = 0;
        update newQLI;
        
        newQLI.Quantity__c = 1;
        newQLI.Price__c = 0;
        update newQLI;
        
        newQLI.Price__c = 1000;
        newQLI.Discount__c = NULL;
        update newQLI;
        
        delete newQLI;
        List<OpportunityLineItem> lineItem = [SELECT UnitPrice FROM OpportunityLineItem WHERE Line_Item_Product_Family__c =: thePBEList.get(1).Product2.Family];
        Test.stopTest();
        
        //System.assertEquals(1000, voiceInsertEMRR);
        //System.assertEquals(1500, voiceUpdateEMRR);
        //System.assertEquals(0, lineItem.size());
    }
    
    private static testMethod void testServiceUpgradeCommission(){
        initData();
        
        testQuote.Syncing__c = TRUE;
        update testQuote;
        
        testProd2.Revenue_Type__c = thePBEList.get(1).Product2.Family;
        testProd2.Family = thePBEList.get(1).Product2.Family;
        testProd2.Period__c = '1';
        update testProd2;
        
        Test.startTest();
        Quote_Line_Item__c newQLI = new Quote_Line_Item__c(
                Name = 'new test',
                Type__c = 'Standard',
                Commission_Type__c = 'Service Upgrade',
                Upgrade_Amount__c = 1000,
                Quote__c = testQuote.Id,
                Product__c = testProd2.Id);
        
        insert newQLI;
        //Decimal voiceInsertEMRR = [SELECT UnitPrice FROM OpportunityLineItem WHERE Line_Item_Product_Family__c =: thePBEList.get(1).Product2.Family limit 1].UnitPrice;
        
        newQLI.Upgrade_Amount__c = 1500;
        update newQLI;
        //Decimal voiceUpdateEMRR = [SELECT UnitPrice FROM OpportunityLineItem WHERE Line_Item_Product_Family__c =: thePBEList.get(1).Product2.Family limit 1].UnitPrice;
        
        testProd2.Revenue_Type__c = thePBEList.get(2).Product2.Family;
        update testProd2;
        
        update newQLI;
        
        newQLI.Commission_Type__c = NULL;
        update newQLI;
        
        Decimal qliEMRR = [SELECT EMRR__c FROM Quote_Line_Item__c WHERE Id = :newQLI.Id].EMRR__c;
        Test.stopTest();
        
        //System.assertEquals(1000, voiceInsertEMRR);
        //System.assertEquals(1500, voiceUpdateEMRR);
        //System.assertEquals(0, qliEMRR);
    }
}