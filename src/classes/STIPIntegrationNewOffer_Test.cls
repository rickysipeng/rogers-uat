@isTest
private class STIPIntegrationNewOffer_Test{
    static Account acc= new Account();
    static Opportunity opp= new Opportunity();
    static offers__c offer= new offers__c();
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  
        
    private static void testdata(){
    TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
    
        acc.Name='TestAcc';
        acc.Account_Status__c='Assigned';
        acc.BillingCountry= 'CA';
        acc.BillingPostalCode = 'A9A 9A9';
        acc.BillingState = 'MA';
        acc.BillingCity='City';
        acc.BillingStreet='Street';
        insert acc;  
        opp.Name='TestOpp'; 
        opp.AccountId= acc.id;
        opp.StageName='Identify';
        opp.CloseDate=system.today();
        insert opp;
        offer.Name = 'TestOffer';
        offer.Opportunity__c = opp.id;
        insert offer;
    }
    static testMethod void testMethod1(){
       testdata();        
        test.startTest();
        Apexpages.currentPage().getParameters().put('oppid',opp.id);
        //ApexPages.StandardController controller = new ApexPages.StandardController(opp); 
        //STIPIntegrationNewOffer cls = new STIPIntegrationNewOffer(controller ); 
        test.stopTest();   
    }
     static testMethod void testMethod2(){
        testdata();   
        test.startTest();
        ApexPages.StandardController controller = new ApexPages.StandardController(offer);
        Apexpages.currentPage().getParameters().put('offerid',offer.id); 
        STIPIntegrationNewOffer cls = new STIPIntegrationNewOffer(controller ); 
        test.stopTest();    
    }
    static testMethod void testMethod3(){
        testdata();        
        test.startTest();
        Apexpages.currentPage().getParameters().put('id',offer.id); 
        ApexPages.StandardController controller = new ApexPages.StandardController(offer);
        STIPIntegrationNewOffer cls = new STIPIntegrationNewOffer(controller ); 
        test.stopTest();  
    }
}