@isTest (seeAllData=true) 
private class TestContactTrigger_AU {
    private static Account a ;
    private static Contact testContact;
    private static Quote testQuote;
    private static Opportunity oppTestObj;
    private static List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
    private static Product2 p2;
    private static Pricebook2 stdpb;
    private static PricebookEntry pbe;

      private static void setUpData(){
        //Account
        a = new Account();
        a.name= 'A45cc56ou67ntTe57804st';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;
        
        //Opprtunity
         oppTestObj = new Opportunity(
                                Name = 'oppTestObj',
                                StageName = 'oppTestObj',
                                CloseDate = Date.today().addDays(2),
                                AccountID=a.id);
                                
                                
        insert oppTestObj;
      
        //Contact
        testContact         = new Contact();
        testContact.FirstName       = 'ContactFirstName';
        testContact.LastName        ='ContactLastName';
        testContact.Email           = 'Adress2@Adress.com';
        testContact.Title           = 'ContactTitile';
        testContact.MailingStreet   ='456 garrik st';
        testContact.MailingCity    = 'testCity'; 
        testContact.MailingState    = 'AB';
        testContact.mailingPostalCode  ='A9A 9A9';
        testContact.MailingCountry  ='CA';
        testContact.phone='123';
        testContact.Accountid=a.id;
        testContact.Contact_Type__c = 'other';
        insert testContact;
        
        //Quote
         testQuote = new Quote();
         testQuote.Name='Test Quote';
         testQuote.ContactId=testContact.Id;
         testQuote.OpportunityId=oppTestObj.Id;
         insert testQuote;
      
      }
      
       static testmethod void TestMethod1(){
         test.startTest();
         setUpData();
         testContact.FirstName='TestNewFN';
         update testContact;
         test.stopTest();
         }


}