@isTest
private class HermesQueueReceivedHandlerTest {
    @isTest
    public static void testHandleFromOtherOrg() {
        HermesLead obj = new HermesLead();
        obj.company = 'company';
        obj.lastname = 'lastname';
        Hermes_Queue__c queue = new Hermes_Queue__c();
        queue.Class_Name__c = HermesLead.CLASS_NAME;
        queue.Payload__c = obj.toJson();

        List<Hermes_Queue__c> queues = new List<Hermes_Queue__c>();
        queues.add(queue);
        new HermesQueueReceivedHandler().handleFromOtherOrg(queues);

        List<Lead> lead = [SELECT Id, Company FROM Lead];
        System.assert(lead.size() == 1);
        System.assert(lead[0].company == obj.company);
    }
}