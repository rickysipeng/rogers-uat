public class PostToGetController {

    public String redirect {get;set;}


/*
    public PostToGetController() {
       
       //redirect = 'http://esflab3.rci.rogers.ca:380/affwebservices/public/saml2sso';
       redirect = 'http://rciesnesfdev22.rci.rogers.ca/affwebservices/public/saml2sso';
       Map<String, String> params = ApexPages.currentPage().getParameters();
       //if (params.containsKey('RelayState')) redirect = redirect + '&RelayState=' + params.get('RelayState');
       if (params.containsKey('RelayState')) redirect = redirect + '?SPID=https://rio--UAT.cs15.my.salesforce.com';
       //if (params.containsKey('RelayState')) redirect = redirect + '?SPID=https://rbs.my.salesforce.com';
       
    }
  */  
    public PostToGetController() {
       
       //redirect = 'http://esflab3.rci.rogers.ca:380/affwebservices/public/saml2sso';
       //redirect = 'http://rciesnesfdev22.rci.rogers.ca/affwebservices/public/saml2sso';
       redirect = 'https://esf.rci.rogers.com/affwebservices/public/saml2sso';


       Map<String, String> params = ApexPages.currentPage().getParameters();
       
       //if (params.containsKey('RelayState')) redirect = redirect + '&RelayState=' + params.get('RelayState');
       //if (params.containsKey('RelayState')) redirect = redirect + '?SPID=https://rio--UAT.cs15.my.salesforce.com';
       if (params.containsKey('RelayState')) redirect = redirect + '?SPID=https://rbs.my.salesforce.com';       
    }
    
}