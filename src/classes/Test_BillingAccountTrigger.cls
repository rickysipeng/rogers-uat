/*
===============================================================================
 Class Name   : Test_BillingAccountTrigger
===============================================================================
PURPOSE:    This is a Test class for BillingAccountTrigger.
              
Developer: Jayavardhan
Date: 03/09/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/09/2015           Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_BillingAccountTrigger{
     private static Account ParentAcc;
     private static Account childAcc;
     private static List<AccountTeamMember> parentAccTeam;
     private static List<Billing_Account__c> billingObjList;
     private static Profile pEmp = [Select Id from Profile where Name Like '%Rogers%' limit 1];
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static User userRec;
     private static User userRec2;
     //create Test Data
     private static void setUpData(){
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.Unassigned_User__c ='Unassigned User';
        insert TAG_CS;
        userRec = new User(LastName = 'Mark O’BrienRoger', Alias = 'alRoger', Email='test@Rogertest.com', Username='test666@Rogertest.com', CommunityNickname = 'nickRoger666', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’BrienRoger2', Alias = 'aRoger', Email='test@Rogertest.com', Username='test667@Rogertest2.com', CommunityNickname = 'nickRoger667', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
        
        ParentAcc= new Account(Name='ParentAcc');
        ParentAcc.Account_Status__c = 'Unassigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId = userRec.Id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc;
        
        parentAccTeam = new List<AccountTeamMember>();
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'Owner';
        newMem.AccountId = ParentAcc.Id;
        newmem.UserId = ParentAcc.OwnerId; 
        parentAccTeam.add(newMem);
        
        AccountTeamMember newMem2 = new AccountTeamMember();
        newMem2 .TeamMemberRole = 'SDC';
        newMem2 .AccountId = ParentAcc.Id;
        newMem2 .UserId = userRec.Id; 
        parentAccTeam.add(newMem2 );
        
        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'ISR - Large';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = userRec2.Id; 
        parentAccTeam.add(newMem3);
        
        insert parentAccTeam;   
        
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with Billing Account obj records created with account team members
    *******************************************************************************************************/
    static testmethod void testAccountTeamMemberAccess(){
        setUpData(); 
        billingObjList = new List<Billing_Account__c>();
        Billing_Account__c  bill1 = new Billing_Account__c();
        bill1.ownerId = userRec.Id;
        bill1.Account__c = ParentAcc.Id;
        bill1.Name = 'Bill 1';
        bill1.Account_Status__c = 'Active';
        
        billingObjList.add(bill1);
        
        Billing_Account__c  bill2 = new Billing_Account__c();
        bill2.ownerId = userRec2.Id;
        bill2.Account__c = ParentAcc.Id;
        bill2.Name = 'Bill 2';
        bill1.Account_Status__c = 'Active';
        
        billingObjList.add(bill2);
    }
}