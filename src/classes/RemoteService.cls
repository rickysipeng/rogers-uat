/* Feb 26 2015 Paul Saini
   As per clients request below, made changes.
   The CURRENT functionality in RDC PROD is to pull the account # from the parent OPP of the quote.
   The NEW functionality in EBU SF is to pull the account # from the billing account attached to the parent OPP of the quote.
   Replace Opportunity_Name__r.Account.Customer_Number__c with Opportunity_Name__r.Billing_Account__r.Billing_Number__c
*/
public with sharing class RemoteService {
    public static final String Endpoint = Config__c.getInstance('Config').RemoteService__c + '?c={0}';
    private Quote__c QuoteItem;
    public Quote_Line_Item__c[] QuoteLineItemsList{get;private set;}

    public class RemoteDataItem {
        public String service { get; set; }
        public Integer id { get; set; }
        public Integer quantity { get; set; }
        public Double discount { get; set; }
        public Double price { get; set; }

        public RemoteDataItem(String service, Integer quantity, Double discount, Double price) {
            this.service = service;
            this.id = id;
            this.quantity = quantity;
            this.discount = discount;
            this.price = price;
        }

    }

    public static List<RemoteDataItem> ActiveServices { get; set; }

    public RemoteService(ApexPages.StandardController stdController) {
        this.QuoteItem = (Quote__c)stdController.getRecord();
        if(this.QuoteItem!= null){
            this.QuoteItem =[Select Id, Name, Opportunity_Name__r.Account.Id, Opportunity_Name__r.Billing_Account__r.Billing_Number__c from Quote__c where ID=:this.QuoteItem.Id][0];
        }
        
    }

    public RemoteService() {
        String QuoteId = ApexPages.currentPage().getParameters().get('id');
    }

    public static List<RemoteDataItem> loadData(String customerId) {

        List<RemoteDataItem> actServ = new List<RemoteDataItem>();

        HttpRequest req = new Httprequest();

        String endp = String.format(Endpoint, new String[]{customerId});

        req.setEndpoint(endp);
        req.setMethod('GET');

        /*String username = 'myname';
        String password = 'mypwd';

        Blob headerValue = Blob.valueOf(username + ':' + password);
        String authorizationHeader = 'BASIC ' +
        EncodingUtil.base64Encode(headerValue);
        req.setHeader('Authorization', authorizationHeader);*/
        
        HTTPResponse resp;
        
        if (!Test.isRunningTest()) {
            resp = (new Http()).send(req);
        } else {
            resp = new Httpresponse();
            resp.setStatus('OK');
            resp.setBody('[{"service":"152010","quantity":"1","discount":"100.00","price":"9.95"},{"service":"400160","quantity":"1","discount":".00","price":"3.95"}]');
        }

        if (resp.getStatus() == 'OK') {
            try {
                actServ = parseResponce(resp.getBody());
            } catch (Exception e) {
                system.debug(e);
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.UnexpectedServiceResponse));
            }
        }
        
        return actServ;
    }

    public void loadDataAction() {
        System.debug('==== this.QuoteItem > ' + this.QuoteItem);
        //System.debug('TPS:EH1.1  QuoteItem=' + QuoteItem);
        //System.debug('TPS:EH1.1  QuoteItem.Opportunity_Name__c=' + QuoteItem.Opportunity_Name__c);
        //System.debug('TPS:EH1.1  QuoteItem.Opportunity_Name__r.Account=' + QuoteItem.Opportunity_Name__r.Account);        
        //System.debug('TPS:EH1.1  Opportunity_Name__r.Billing_Account__r.Billing_Number__c=' + QuoteItem.Opportunity_Name__r.Billing_Account__r.Billing_Number__c);
       
        if (this.QuoteItem == NULL || this.QuoteItem.Opportunity_Name__c == NULL
            || this.QuoteItem.Opportunity_Name__r.Account == NULL
            || this.QuoteItem.Opportunity_Name__r.Billing_Account__r.Billing_Number__c == NULL
        ) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, Label.CustomerNumberNotSpecified));
            return;
        }

        //ActiveServices = RemoteService.loadData(this.QuoteItem.Opportunity_Name__r.Account.Customer_Number__c);
       ActiveServices = RemoteService.loadData(this.QuoteItem.Opportunity_Name__r.Billing_Account__r.Billing_Number__c);

        if (ActiveServices.size() <= 0) {
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.NoCustomServices2Import));
            return;
        }

        List<String> productsCodesList = new List<String>();

        for (RemoteDataItem rdi: ActiveServices) {
            productsCodesList.add(rdi.service);
        }

        Product2[] products = [SELECT Id, ProductCode, Name, Period__c FROM Product2 WHERE ProductCode IN :productsCodesList];

        Map<String,Product2> code2Id  = new Map<String,Product2>();

        for (Product2 pr:products) {
            code2Id.put(pr.ProductCode,pr);
        }

        QuoteLineItemsList = new List<Quote_Line_Item__c>();

        // create QLI template
        Quote_Line_Item__c tmp = new Quote_Line_Item__c (
            Quote__c = QuoteItem.Id,
            Commission_Type__c = 'Contract Renewal (Not Commissionable)',
            Datafill_Commission_Type__c = 'Contract Renewal (Not Commissionable)',
            Type__c = 'Autobilling',
            From_Datafill__c = TRUE
        );
        
        // find out number of QLIs to respect the line numbering
        Integer theNumberOfQLIs = [SELECT COUNT() FROM Quote_Line_Item__c WHERE Quote__c =: QuoteItem.Id AND Parent_Quote_Line_Item__c = null];
        
        
        for (RemoteDataItem rdi: ActiveServices) {
            try {
                Quote_Line_Item__c qli = tmp.clone();
                theNumberOfQLIs++;
                                
                qli.Name = code2Id.get(rdi.service).Name;
                qli.Product__c = code2Id.get(rdi.service).Id;
                qli.Quantity__c = rdi.quantity;
                qli.Price__c = rdi.price;
                qli.Period__c = code2Id.get(rdi.service).Period__c;
                qli.Discount__c = rdi.discount;
                qli.Line__c = theNumberOfQLIs;
                qli.Autobill_Id__c = rdi.id;
                
                QuoteLineItemsList.add(qli);
            } catch(Exception exp) {
                ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Product_Code_NotValid + ': ' + rdi.service));
            }
        }

        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, Label.ImportedServicesList));

    }
    
    public pageReference insertQuoteLineItemsList() {
        Savepoint sp = Database.setSavepoint();
        if(QuoteLineItemsList != NULL && (!QuoteLineItemsList.isEmpty())) {
            try{
                insert QuoteLineItemsList;
            }
            catch (Exception e) {
                Database.rollback(sp);
                ApexPages.addMessages(e);
                return null;
            }
        }
        
        return new PageReference('/apex/QuoteImplementation?id=' + QuoteItem.Id);
    }

    @future(callout=true)
    public static void loadDataAsync(String customerId) {
        ActiveServices = loadData(customerId);
    }

    public static List<RemoteDataItem> parseResponce (String JSONString) {

        system.debug(JSONString);

        JSONParser parser = JSON.createParser(JSONString);
        List<RemoteDataItem> actServ = new List<RemoteDataItem>();

        while (parser.nextToken() != NULL) {
            if (parser.getCurrentToken() == JSONToken.START_ARRAY) {
                while (parser.nextToken() != NULL) {
                    if (parser.getCurrentToken() == JSONToken.START_OBJECT) {
                        RemoteDataItem rdi = (RemoteDataItem)parser.readValueAs(RemoteDataItem.class);
                        actServ.add(rdi);
                        parser.skipChildren();
                    }
                }
            }
        }
        return actServ;
    }
}