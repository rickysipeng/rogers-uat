global class SitesController implements ObjectPaginatorListener {
 
    public Opportunity opp {get; set;}
    private Id oppId;
    List<String> clliCodes;
    public boolean fromOpportunity {get; set;}
    public boolean duplicateCheck {get; set;}
    public boolean modifyServiceablLocation{get; set;}
    public String duplicateMessage {get; set;}
    public boolean displayDuplicateMessage {get; set;}
    public Integer defaultAccessTypeGroup {get; set; }
    public String defaultATG {get; set; }
    
    private static final String SERVICEABLE_LOCATION = 'Serviceable Location'; 
    global ObjectPaginator paginator {get;private set;}
 // init the controller and display some sample data when the page loads
    public SitesController(ApexPages.StandardController controller) {
        this.serviceableLocations = new List<ServiceableLocationWrapper>();
              
        oppId = ApexPages.currentPage().getParameters().get('retURL');
        String dupCheck = ApexPages.currentPage().getParameters().get('dupCheck');   
        String editServiceable = ApexPages.currentPage().getParameters().get('edit');   
        duplicateMessage = '';
        /* This will allow us to use the Serviceability functionality for duplicate Checking by setting this param in the URL */
        if (!Utils.isEmpty(dupCheck) && dupCheck.equals('1'))
            duplicateCheck = true;
        else
            duplicateCheck = false;
            
        /* This will allow us to use the Serviceability functionality for duplicate Checking by setting this param in the URL */
        if (!Utils.isEmpty(editServiceable) && editServiceable .equals('1'))
            modifyServiceablLocation= true;
        else
            modifyServiceablLocation= false;
        
        if (!Utils.isEmpty(oppId)){
            fromOpportunity = true;
            opp = [SELECT Id, Name, AccountId From Opportunity WHERE Id = :oppId];
        }else{
            fromOpportunity = false;
        }
        
        // Obtain the default Access Type Group
        
        Schema.DescribeFieldResult fieldResult = ServiceableLocation__c.Access_Type_Group__c.getDescribe();
        List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
        
        defaultATG = '';
        for(Schema.PicklistEntry f : ple){
            if (f.isDefaultValue())
                defaultATG = f.getLabel();
        }
        
        
        displayDuplicateMessage = false;
        clliCodes = new List<String>();
        soql = 'SELECT Name, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c, CLLI_Code__c, Postal_Code__c, City__c, Access_Type__c, Access_Type_Group__c, CNINNI__c, Network_Access_Type__c, Build_Date__c, Display_Name__c FROM serviceableLocation__c';
        if (!Utils.isEmpty(defaultATG))
            soql += ' WHERE Access_Type_Group__c INCLUDES (\''+defaultATG +'\')'; 
 
        runQuery();
        
        List<ServiceableLocationWrapper> all = new List<ServiceableLocationWrapper>();
        
        for (ServiceableLocationWrapper slw : serviceableLocations){
            all.add(slw);
        }
        
        paginator = new ObjectPaginator(20, this);
        paginator.setRecords(all);
    }
         
  // the soql without the order and limit
  private String soql {get;set;}
  
  // the collection of serviceablity to display
  global List<ServiceableLocationWrapper> serviceableLocations {get;private set;}
  
  /*
  global List<ServiceableLocationWrapper> getServiceableLocations(){
        return this.serviceableLocations;
    }
    */  
  // the current sort direction. defaults to asc
  public String sortDir {
    get  { if (sortDir == null) {  sortDir = 'asc'; } return sortDir;  }
    set;
  }
 
  // the current field to sort by. defaults to last name
  public String sortField {
    get  { if (sortField == null) {sortField = 'Name'; } return sortField;  }
    set;
  }
 
  // format the soql for display on the visualforce page
  public String debugSoql {
    get { return soql + ' order by ' + sortField + ' ' + sortDir + ' limit 20'; }
    set;
  }
 

  // toggles the sorting of query from asc<-->desc
  public void toggleSort() {
    // simply toggle the direction
    sortDir = sortDir.equals('asc') ? 'desc' : 'asc';
    // run the query again
    runQuery();
  }
 
  // runs the actual query
  public void runQuery() {
   Set<Id> currentlySelected = new Set<Id>();
     for (ServiceableLocationWrapper serviceableLocation : serviceableLocations){
                if (serviceableLocation.getIsSelected()){
                    currentlySelected.add(serviceableLocation.getServiceableLocation().Id);
                }
     }
    
    serviceableLocations.clear();
    try {
      for (ServiceableLocation__c serviceableLocation : Database.query(soql + ' order by ' + sortField + ' ' + sortDir + (sortDir=='asc'? ' NULLS FIRST ': ' NULLS LAST ') + ' limit 100')){
        ServiceableLocationWrapper temp = new ServiceableLocationWrapper(serviceableLocation);
        if (currentlySelected.contains(temp.getServiceableLocation().Id))
            temp.setIsSelected(true);
        serviceableLocations.add(temp);    
      }
      
    } catch (Exception e) {
      ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage()));
    }
 
  }
 
  // runs the search with parameters passed via Javascript
  public PageReference runSearch() {
 
    String serviceableLocationName = Apexpages.currentPage().getParameters().get('serviceableLocationName');
    String suite = Apexpages.currentPage().getParameters().get('suite');
    String streetNumber = Apexpages.currentPage().getParameters().get('streetNumber');
    String streetName = Apexpages.currentPage().getParameters().get('streetName');
    String streetType = Apexpages.currentPage().getParameters().get('streetType');
    String streetDirection = Apexpages.currentPage().getParameters().get('streetDirection');
    String city = Apexpages.currentPage().getParameters().get('city');
    String postalCode = Apexpages.currentPage().getParameters().get('postalCode');
    String cninni = Apexpages.currentPage().getParameters().get('cninni');
    String accessTypeGroup = Apexpages.currentPage().getParameters().get('accessTypeGroups');
    String networkAccessType = Apexpages.currentPage().getParameters().get('networkAccessTypes');
    
    cninni = (!Utils.isEmpty(cninni) && cninni=='true')?' AND (CNINNI__C = \'NNI\' OR CNINNI__C =\'CNI\')':'';
    
    clliCodes.clear();
    if (!Utils.isEmpty(postalCode)){
        String qryString = 'SELECT Id, Postal_Code__c, CLLI_Code__c FROM CLLIPostalCode__c where Postal_Code__c LIKE \'' + String.escapeSingleQuotes(postalCode)+ '%\' LIMIT 2500';
        
        try{    
            for (CLLIPostalCode__c c : Database.query(qryString)){
                if (!Utils.listContains(clliCodes, c.CLLI_Code__c))
                    clliCodes.add(c.CLLI_Code__c);
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
 
    soql = 'SELECT Name, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c,'
         + ' Postal_Code__c, CLLI_Code__c, City__c, Access_Type__c, Access_Type_Group__c, CNINNI__c, Network_Access_Type__c, Build_Date__c, Display_Name__c from serviceableLocation__c where Name!= null '
         + cninni;
         
    
    
    if (!serviceableLocationName.equals(''))
      soql += ' and Name LIKE \''+String.escapeSingleQuotes(serviceableLocationName)+'%\'';
    if (!suite.equals(''))
      soql += ' and Suite_Floor__c LIKE \''+String.escapeSingleQuotes(suite)+'%\'';
    if (!streetNumber.equals(''))
      soql += ' and Street_Number__c LIKE \''+String.escapeSingleQuotes(streetNumber)+'%\'';
    if (!streetName.equals(''))
      soql += ' and Street_Name__c LIKE \''+String.escapeSingleQuotes(streetName)+'%\'';    
   if (!streetType.equals(''))
      soql += ' and Street_Type__c LIKE \''+String.escapeSingleQuotes(streetType)+'%\'';
    if (!streetDirection.equals(''))
      soql += ' and Street_Direction__c LIKE \''+String.escapeSingleQuotes(streetDirection)+'%\'';
    if (!city.equals(''))
      soql += ' and City__c LIKE \''+String.escapeSingleQuotes(city)+'%\'';  
    if (!Utils.isEmpty(postalCode))
      soql += ' and CLLI_Code__c IN :clliCodes';
 
    if (!Utils.isEmpty(networkAccessType))
      soql += ' and Network_Access_Type__c = \''+String.escapeSingleQuotes(networkAccessType)+'\'';
     
    if (!accessTypeGroup.equals(''))
      soql += ' and Access_Type_Group__c INCLUDES (\''+String.escapeSingleQuotes(accessTypeGroup)+'\')';    
  
    // run the query again
    runQuery();
    
    List<ServiceableLocationWrapper> all = new List<ServiceableLocationWrapper>();
        
    for (ServiceableLocationWrapper slw : serviceableLocations){
        all.add(slw);
    }
        
    //paginator = new ObjectPaginator(20, this);
    
    paginator.setRecords(all);
    return null;
  }
  public PageReference addNewSite(){
   // Old Sandbox
   // PageReference pr= new PageReference('/a0C/e?CF00NQ0000000leek=' + opp.Name +'&CF00NQ0000000leek_lkid='+opp.Id+'&retURL=%2F'+opp.Id+'&Name=Please%20Ignore%20Field');

   // Production
    SitesController_Setting__c cs = SitesController_Setting__c.getInstance();
    
    String oppName = EncodingUtil.urlEncode(opp.Name, 'UTF-8');
    PageReference pr= new PageReference('/' + cs.Site__c + '/e?' + cs.Site_Opportunity__c + '=' + oppName +'&' + cs.Site_Opportunity_lkid__c + '='+opp.Id+'&retURL=%2F'+opp.Id+'&Name=Please%20Ignore%20Field');
   
    pr.setRedirect(true); 
    return pr;
  }
  
  /* Obtains the Access Type Groups for the Select List and uses the order and default value specifies.  
     The VF Page willl use jQuery to set the default value
  */
  public List<String> getAccessTypeGroups(){
    List<String> accessTypeGroupOptions = new List<String>();
    Integer i = 0;  
    Schema.DescribeFieldResult fieldResult = ServiceableLocation__c.Access_Type_Group__c.getDescribe();
    List<Schema.PicklistEntry> ple = fieldResult.getPicklistValues();
    defaultAccessTypeGroup = ple.size();    
    for(Schema.PicklistEntry f : ple){
        if (f.isDefaultValue())
            defaultAccessTypeGroup = i;
        i++;    
        accessTypeGroupOptions.add(f.getLabel());
    }       
   
    return accessTypeGroupOptions;
    
  }
  
    public List<String> getNetworkAccessTypes(){
        List<String> networkAccessTypeOptions = new List<String>();
        Schema.DescribeFieldResult fieldInfo = Schema.SObjectType.ServiceableLocation__c.fields.Network_Access_Type__c;
      
        List<Schema.PicklistEntry> picklistValues = fieldInfo.getPicklistValues();
      
        for (Schema.PicklistEntry picklistValue : picklistValues){
            networkAccessTypeOptions.add(picklistValue.getLabel());
        }
       
        List<String> options = new List<String>();
       
        for(String network : networkAccessTypeOptions){
            options.add(network);
        }
      
        return options;
  }
 
  
  
  
   public PageReference addSites(){
        String suiteMap = Apexpages.currentPage().getParameters().get('suiteMap');
        System.debug(suiteMap);
        
        Map<String, String> suiteIdMap = new Map<String, String>();
        if (!Utils.isEmpty(suiteMap)){
            String[] idSuite = suiteMap.split('~');
            
            for (String s : idSuite){
                
                if (s.split('@').size() == 3){
                    suiteIdMap.put(s.split('@')[0],s.split('@')[1] + '@' + s.split('@')[2]);
                }
            }
            
        }
    
        System.debug(suiteIdMap);
    
        Boolean error = false;
        String dupList = '';
        Integer counter = 0;
        for (ServiceableLocationWrapper serviceableLocation : serviceableLocations){
                
                if (serviceableLocation.getIsSelected()){
                    Site__c site = new Site__c(ServiceableLocation__c = serviceableLocation.getServiceableLocation().Id,
                                                Name = serviceableLocation.getServiceableLocation().Name,                                              
                                                Suite_Floor__c = serviceableLocation.getServiceableLocation().Suite_Floor__c,
                                                Street_Number__c = serviceableLocation.getServiceableLocation().Street_Number__c,
                                                Street_Name__c = serviceableLocation.getServiceableLocation().Street_Name__c,
                                                Street_Type__c = serviceableLocation.getServiceableLocation().Street_Type__c,
                                                Province_Code__c = serviceableLocation.getServiceableLocation().Province_Code__c,
                                                Postal_Code__c = serviceableLocation.getServiceableLocation().Postal_Code__c,
                                                CLLI_SWC__c = serviceableLocation.getServiceableLocation().CLLI_Code__c,
                                                Street_Direction__c = serviceableLocation.getServiceableLocation().Street_Direction__c,
                                                City__c = serviceableLocation.getServiceableLocation().City__c,
                                                Access_Type__c = serviceableLocation.getServiceableLocation().Access_Type__c,
                                                Access_Type_Group__c = serviceableLocation.getServiceableLocation().Access_Type_Group__c,
                                                Opportunity__c = opp.Id,
                                                Account__c = opp.AccountId);
                                                
                    System.debug('YYYYY: ' + Utils.get15CharId(serviceableLocation.getServiceableLocation().Id));                                                
                    System.debug('YYYYY: ' + suiteIdMap);
                    if (suiteIdMap.get(Utils.get15CharId(serviceableLocation.getServiceableLocation().Id)) != null){
                        String tempSuite = suiteIdMap.get(Utils.get15CharId(serviceableLocation.getServiceableLocation().Id));
                        if (tempSuite.split('@').size() == 2){
                            site.Suite_Floor__c = tempSuite.split('@')[0];
                            site.Suite_N_A__c = tempSuite.split('@')[1] == 'true';
                        }
                    }
                    try{
                        if (!Utils.isEmpty(serviceableLocation.getServiceableLocation().CNINNI__c))
                            site.Type__c = serviceableLocation.getServiceableLocation().CNINNI__c;
                        else
                            site.Type__c = SERVICEABLE_LOCATION;
                        INSERT site;
                        serviceableLocation.setIsSelected(false);
                    }catch(DMLException ex){
                        // dupList += (counter++>0?', ':'') + serviceableLocation.getServiceableLocation().Name;
                         serviceableLocation.setIsSelected(false);
                    //     error = true;
                    }
                    
                }       
            }
        
     //   if (error){
    //        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.INFO, 'You have chosen a site that is already a prospect site for this Opportunity. Site' + (counter>1?'s ':' ') + dupList + (counter>1?' were':' was') +' not readded.'));
     //   }
         
        return null;
   }
   
     public PageReference returnToOpportunity(){
        PageReference oppPage = new ApexPages.StandardController(opp).view();
        oppPage.setRedirect(true);
        return oppPage;
    }
  
    global void handlePageChange(List<Object> newPage){
        serviceableLocations.clear();
        if(newPage != null){
            for (Object serviceableLocation : newPage){
                serviceableLocations.add((ServiceableLocationWrapper)serviceableLocation);
            }
        }
    }
    
    public PageReference checkExistingQuote() {
        String siteId = Apexpages.currentPage().getParameters().get('siteId');
        String accountName = Apexpages.currentPage().getParameters().get('accountName');    
        
        if (Utils.isEmpty(siteId)){
            displayDuplicateMessage = false;
            duplicateMessage = '';
            return null;
        }
        
        List<Quote_Site__c> qSites = new List<Quote_Site__c>();
        
        try{
            qSites = [SELECT quote__c, site__c, Site__r.ServiceableLocation__r.Access_Type_Group__c, Site__r.Account__r.Name FROM Quote_Site__c WHERE site__r.ServiceableLocation__c = :siteId AND Quote__r.ExpirationDate >= today AND Site__r.Opportunity__r.OwnerId != :UserInfo.getUserId()];
        }catch(Exception ex){
            displayDuplicateMessage = true;
            duplicateMessage = 'This customer site has yet to be quoted for a Rogers service!!.';
        }
        
        if (qSites != null && qSites.size()>0){
            boolean hasDup = false;
            Set<String> matchingAccounts = new Set<String>();
            for (Quote_Site__c qSite : qSites){
                if(!Test.isRunningTest()){
                    if ((!Utils.isEmpty(qSite.site__r.ServiceableLocation__r.Access_Type_Group__c) && !qSite.Site__r.ServiceableLocation__r.Access_Type_Group__c.equalsIgnoreCase('fibre')) || (qSite.Site__r.ServiceableLocation__r.Access_Type_Group__c.toLowerCase().contains('fibre') && qSite.Site__r.Account__r.Name.toLowerCase().contains(accountName.toLowerCase()))){
                        displayDuplicateMessage = true;
                        hasDup = true;
                        matchingAccounts.add(qSite.Site__r.Account__r.Name);
                    }
                }
            }
            if (!hasDup){
                displayDuplicateMessage = true;
                duplicateMessage = 'This customer site has yet to be quoted for a Rogers service.';
            }else{
                displayDuplicateMessage = true;
                duplicateMessage = 'This customer site has recently been quoted for Rogers services. If you would like to escalate or find out more information please contact your direct sales manager.\n';
                duplicateMessage += '<b>Matching Accounts: </b>';
                for (String acct : matchingAccounts){
                    duplicateMessage += acct + '\n';
                }
                duplicateMessage = duplicateMessage.replace('\n', '<br/>');
            }
        }else{
            displayDuplicateMessage = true;
            duplicateMessage = 'This customer site has yet to be quoted for a Rogers service.';
        }
        ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.INFO, duplicateMessage);
        ApexPages.addMessage(myMsg);
        return null;
    }
 
    public PageReference deleteServiceableLocations(){
        String siteIds = Apexpages.currentPage().getParameters().get('SiteIds');
        
        String serviceableLocationName = Apexpages.currentPage().getParameters().get('serviceableLocationName');
        String suite = Apexpages.currentPage().getParameters().get('suite');
        String streetNumber = Apexpages.currentPage().getParameters().get('streetNumber');
        String streetName = Apexpages.currentPage().getParameters().get('streetName');
        String streetType = Apexpages.currentPage().getParameters().get('streetType');
        String streetDirection = Apexpages.currentPage().getParameters().get('streetDirection');
        String city = Apexpages.currentPage().getParameters().get('city');
        String postalCode = Apexpages.currentPage().getParameters().get('postalCode');
        String cninni = Apexpages.currentPage().getParameters().get('cninni');
        String accessTypeGroup = Apexpages.currentPage().getParameters().get('accessTypeGroups');
        String networkAccessType = Apexpages.currentPage().getParameters().get('networkAccessTypes');
        
        List<String> ids = siteIds.split('~');
        
        try{
            List<ServiceableLocation__c> sitesToDelete = [SELECT Id, Name FROM ServiceableLocation__c WHERE id IN :ids];
            DELETE sitesToDelete;
            
            //runSearch();
        }catch(DMLException ex){
            
        }
        
        cninni = (!Utils.isEmpty(cninni) && cninni=='true')?'(CNINNI__C = \'NNI\' OR CNINNI__C =\'CNI\')':'CNINNI__C = \'\'';
    

    
    clliCodes.clear();
    if (!Utils.isEmpty(postalCode)){
        String qryString = 'SELECT Id, Postal_Code__c, CLLI_Code__c FROM CLLIPostalCode__c where Postal_Code__c LIKE \'' + String.escapeSingleQuotes(postalCode)+ '%\' LIMIT 2500';
        
        try{    
            for (CLLIPostalCode__c c : Database.query(qryString)){
                if (!Utils.listContains(clliCodes, c.CLLI_Code__c))
                    clliCodes.add(c.CLLI_Code__c);
            }
        }catch(Exception ex){
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, ex.getMessage()));
        }
    }
 
    soql = 'SELECT Name, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c,'
         + ' Postal_Code__c, CLLI_Code__c, City__c, Access_Type__c, Access_Type_Group__c, CNINNI__c, Network_Access_Type__c, Build_Date__c, Display_Name__c from serviceableLocation__c where Name!= null '
         + ' and ' + cninni;
         
    
    if (!serviceableLocationName.equals(''))
      soql += ' and Name LIKE \''+String.escapeSingleQuotes(serviceableLocationName)+'%\'';
    if (!suite.equals(''))
      soql += ' and Suite_Floor__c LIKE \''+String.escapeSingleQuotes(suite)+'%\'';
    if (!streetNumber.equals(''))
      soql += ' and Street_Number__c LIKE \''+String.escapeSingleQuotes(streetNumber)+'%\'';
    if (!streetName.equals(''))
      soql += ' and Street_Name__c LIKE \''+String.escapeSingleQuotes(streetName)+'%\'';    
   if (!streetType.equals(''))
      soql += ' and Street_Type__c LIKE \''+String.escapeSingleQuotes(streetType)+'%\'';
    if (!streetDirection.equals(''))
      soql += ' and Street_Direction__c LIKE \''+String.escapeSingleQuotes(streetDirection)+'%\'';
    if (!city.equals(''))
      soql += ' and City__c LIKE \''+String.escapeSingleQuotes(city)+'%\'';  
    if (!Utils.isEmpty(postalCode))
      soql += ' and CLLI_Code__c IN :clliCodes';
 
    if (!Utils.isEmpty(networkAccessType))
      soql += ' and Network_Access_Type__c = \''+String.escapeSingleQuotes(networkAccessType)+'\'';
     
    if (!accessTypeGroup.equals(''))
      soql += ' and Access_Type_Group__c INCLUDES (\''+String.escapeSingleQuotes(accessTypeGroup)+'\')';    
  
    // run the query again
    runQuery();
    
    List<ServiceableLocationWrapper> all = new List<ServiceableLocationWrapper>();
        
    for (ServiceableLocationWrapper slw : serviceableLocations){
        all.add(slw);
    }
        
    //paginator = new ObjectPaginator(20, this);
    
    paginator.setRecords(all);
    return null;
     }   
 
 
     public PageReference editServiceableLocation(){
        String siteId = Apexpages.currentPage().getParameters().get('SiteId');
        String isNew = Apexpages.currentPage().getParameters().get('isNewSite');  
        SitesController_Setting__c cs = SitesController_Setting__c.getInstance(); 
        
        if (Utils.isEmpty(isNew) || (isNew.equals('false')&& Utils.isEmpty(siteId))){
            return null;
        }
        
        String urlString = '';
        
        if (isNew.equals('true'))
            urlString = '/' + cs.Quote_Request__c + '/e?Name=Please%20Ignore%20Field';//&retURL=%2Fapex%2FaddSite%3Fedit=1';
        else
            urlString = '/'+siteId+'/e?retURL='+siteId;//%2Fapex%2FaddSite%3Fedit=1';       
        
        PageReference pr= new PageReference(urlString);
   
        pr.setRedirect(true); 
        return pr;
     }   
}