/**
* Opportunity clone button controller
* //<TODO Need to clone only primary quote
*/
public class OpportunityCloning {
	private Opportunity opportunity { get; set; }
	private CloneUtil util = new CloneUtil();

	public OpportunityCloning(ApexPages.StandardController stdController) {
		this.opportunity = (Opportunity)stdController.getRecord();
	}

	public Pagereference cloneOpportunity() {
		Opportunity newOpp;
		try {
			newOpp = util.cloneOpportunity(opportunity.Id);

			List<Quote__c> clonedQuoteToUpdate = new List<Quote__c>();
			
			/*06/07/2012 Clone only Primary Quote*/
			for (Quote__c quote: [SELECT Id, Syncing__c FROM Quote__c WHERE Opportunity_Name__r.Id = :opportunity.id AND Syncing__c = TRUE]) {
				Quote__c clonedQuote = util.cloneQuote(quote.id);
				if(clonedQuote != NULL) {
					clonedQuote.Opportunity_Name__c = newOpp.Id;
					clonedQuote.Is_Cloned__c = FALSE;
					clonedQuoteToUpdate.add(clonedQuote);
				}
			}

			if(!clonedQuoteToUpdate.isEmpty()) {
				Database.update(clonedQuoteToUpdate);
			}
		} catch (Exception e) {
			ApexPages.addMessages(e);
			return null;
		}
		return new PageReference('/'+newOpp.Id+'/e?retURL=%2F'+newOpp.Id);
	}
}