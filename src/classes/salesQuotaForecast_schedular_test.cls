/*
===============================================================================
Class Name : salesQuotaForecast_schedular_test
===============================================================================
PURPOSE: This is a test class for salesQuotaForecast_schedular class

COMMENTS: 

Developer:Aakanksha Patel
Date: 3/04/2015


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
3/04/2015               Aakanksha               Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class salesQuotaForecast_schedular_test{

    private static Static_Data_Utilities__c CS= new Static_Data_Utilities__c();   
    private static BatchProcess_Admin__c CS1= new BatchProcess_Admin__c();   
    private static Static_Data_Utilities__c CS2 = new Static_Data_Utilities__c(); 
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
          
   static testmethod void testMethod1(){
       Sales_Quota__c quota = new Sales_Quota__c();
        quota.Forecast_Month__c =201310;
        quota.Ownerid =listUser[1].id;
        quota.Quantity__c= 2;
        quota.Quota_Family__c= 'ABS - Software';
        quota.Revenue__c = 100.00;
        insert quota; 
 
        CS.Name = 'Quota Update Checking Period';
        CS.Value__c = '1'; 
        insert CS;
        
        CS2.Name = 'Opportunity Update Checking Period';
        CS2.Value__c = '1'; 
        insert CS2;
        
        CS1.Name = 'SysAdm1';
        CS1.Email__c = 'sfdc.support@rci.rogers.com'; 
        insert CS1;
                       
        Test.StartTest();
        salesQuotaForecast_schedular obj= new salesQuotaForecast_schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('SalesQuotaForecastTesting', sch, obj);
        System.assert(true);
        
        Test.stopTest();    

    }
}