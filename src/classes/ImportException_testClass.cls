/**************************************************************************************
Apex Class Name     : ImportException_testClass
Version             : 1.0 
Created Date        : 4 Apr 2015
Function            : This is the Test Class for Import Exception and MileStone1 Exception.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel            3/04/2015              Original Version
*************************************************************************************/

@isTest(SeeAllData=true)
private class ImportException_testClass
{  
      static testmethod void TestImport()
      {
         test.StartTest();
         ImportException cont = new ImportException();  
         test.StopTest();
      
      }
      static testmethod void TestMileStone()
      {
          test.StartTest();
         Milestone1_Exception cont1 = new Milestone1_Exception();  
           test.StopTest();
      }
       static testmethod void TestObgPaginator()
      {
         test.StartTest();
         ObjectPaginatorListenerForTesting cont = new ObjectPaginatorListenerForTesting();  
         test.StopTest();
      
      }
       
   
   
}