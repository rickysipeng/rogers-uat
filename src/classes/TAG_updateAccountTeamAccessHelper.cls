/*
===============================================================================
Class Name   : TAG_updateAccountTeamAccessHelper
===============================================================================
PURPOSE:    This class is to give or reomve access from Updated AccountTeam.
            This class will be called from below triggers:
            1. TAG_WireLineCon_ATM_Access

Developer: Jayavardhan
Date: 03/03/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/03/2015           Jayavardhan               Original Version
===============================================================================
*/
public class TAG_updateAccountTeamAccessHelper{

    public Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();

    /*******************************************************************************************************
     * @description: This method is used give Access to AccountTeamMembers for the Contract Object records                     
     * @param: Map<ContractRecId,setUserIds>
     * @return type: void.
    ********************************************************************************************************/
    public void giveAccesstoContractTeam(Map<Id,Set<Id>> mapConUserIdsToAdd){
        List<Wireline_Contract__share> newWirelineshares = new List<Wireline_Contract__share>();
        
        for(Id wObj:mapConUserIdsToAdd.keyset()){
            Set<Id> teamMembers = mapConUserIdsToAdd.get(wObj);
            for(Id memId : teamMembers){
                Wireline_Contract__share share = new Wireline_Contract__share();
                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                share.ParentID=wObj;
                share.UserOrGroupId=memId;
                share.RowCause=Schema.Wireline_Contract__share.RowCause.AccountTeamMember__c;
                newWirelineshares.add(share);                     
            } 
        }       
        if(newWirelineshares!=null && newWirelineshares.size()>0)
        Database.insert(newWirelineshares);
        
    }
    
    /*******************************************************************************************************
     * @description: This method is used give Access to AccountTeamMembers for the BillingAccount Object records                     
     * @param: Map<BillingAccountRecId,setUserIds>
     * @return type: void.
    ********************************************************************************************************/
    public void giveAccesstoBillingAccTeam(Map<Id,Set<Id>> mapBillAccUserIdsToAdd){
        List<Billing_Account__share> newBillAccShares = new List<Billing_Account__share>();      
        
        for(Id wObj:mapBillAccUserIdsToAdd.keyset()){
            Set<Id> teamMembers = mapBillAccUserIdsToAdd.get(wObj);
            for(Id memId : teamMembers){
                Billing_Account__share share = new Billing_Account__share();
                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                share.ParentID=wObj;
                share.UserOrGroupId=memId;
                share.RowCause=Schema.Billing_Account__share.RowCause.AccountTeamMember__c;
                newBillAccShares.add(share);                     
            } 
        }       
        if(newBillAccShares!=null && newBillAccShares.size()>0)
        Database.insert(newBillAccShares);
        
    }
    
    /*******************************************************************************************************
     * @description: This method is used give Access to AccountTeamMembers for the MSDCode Object records                     
     * @param: Map<MSDCodeId,setUserIds>
     * @return type: void.
    ********************************************************************************************************/
    public void giveAccesstoMSDCodeTeam(Map<Id,Set<Id>> mapMSDCodeUserIdsToAdd){
        List<MSD_Code__share> newMSDCodeShares = new List<MSD_Code__share>();
        
        for(Id wObj:mapMSDCodeUserIdsToAdd.keyset()){
            Set<Id> teamMembers = mapMSDCodeUserIdsToAdd.get(wObj);
            for(Id memId : teamMembers){
                MSD_Code__share share = new MSD_Code__share();
                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                share.ParentID=wObj;
                share.UserOrGroupId=memId;
                share.RowCause=Schema.MSD_Code__share.RowCause.AccountTeamMember__c;
                newMSDCodeShares.add(share);                     
            } 
        }       
        if(newMSDCodeShares!=null && newMSDCodeShares.size()>0)
        Database.insert(newMSDCodeShares);
        
    }
    
    /*******************************************************************************************************
     * @description: This method is used give Access to AccountTeamMembers for the SharedMSDCode Object records                     
     * @param: Map<SharedMsdCodeId,setUserIds>
     * @return type: void.
    ********************************************************************************************************/
    public void giveAccesstoShrMSDCodeTeam(Map<Id,Set<Id>> mapShrMSDCodeUserIdsToAdd){
        List<Shared_MSD_Code__share> newShareMSDCodeShares = new List<Shared_MSD_Code__share>();
        
        for(Id wObj:mapShrMSDCodeUserIdsToAdd.keyset()){
            Set<Id> teamMembers = mapShrMSDCodeUserIdsToAdd.get(wObj);
            for(Id memId : teamMembers){
                Shared_MSD_Code__share share = new Shared_MSD_Code__share();
                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                share.ParentID=wObj;
                share.UserOrGroupId=memId;
                share.RowCause=Schema.Shared_MSD_Code__share.RowCause.AccountTeamMember__c;
                newShareMSDCodeShares.add(share);                     
            } 
        }       
        if(newShareMSDCodeShares!=null && newShareMSDCodeShares.size()>0)
        Database.insert(newShareMSDCodeShares);
        
    }
    
    /*******************************************************************************************************
     * @description: This method is used give Access to AccountTeamMembers for the SharedMSDAccount Object records                     
     * @param: Map<SharedMsdAccountId,setUserIds>
     * @return type: void.
    ********************************************************************************************************/
    public void giveAccesstoShrMSDAccTeam(Map<Id,Set<Id>> mapShrMSDAccUserIdsToAdd){
        List<Shared_MSD_Accounts__share> newShareMSDAccountShares = new List<Shared_MSD_Accounts__share>();
        
        for(Id wObj:mapShrMSDAccUserIdsToAdd.keyset()){
            Set<Id> teamMembers = mapShrMSDAccUserIdsToAdd.get(wObj);
            for(Id memId : teamMembers){
                Shared_MSD_Accounts__share share = new Shared_MSD_Accounts__share();
                share.AccessLevel =TAGsettings.Team_Custom_Object_Access__c;
                share.ParentID=wObj;
                share.UserOrGroupId=memId;
                share.RowCause=Schema.Shared_MSD_Accounts__share.RowCause.AccountTeamMember__c;
                newShareMSDAccountShares.add(share);                     
            } 
        }       
        if(newShareMSDAccountShares!=null && newShareMSDAccountShares.size()>0)
        Database.insert(newShareMSDAccountShares);
        
    }
}