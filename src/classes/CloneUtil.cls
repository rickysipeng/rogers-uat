/**
* Class is used for clonnig opportunities and quotes
*/
public class CloneUtil{
    private String userId = UserInfo.getUserId();
    private QueryString queryString = new QueryString();

    /**
    * Clones opportunities and opportunity line item
    */
    public Opportunity cloneOpportunity(String oppId) {
        Opportunity newOpp;
        List<Opportunity> oppList = Database.query(queryString.makeQueryString('opportunity', oppId ));
        if(oppList != NULL && oppList.size() > 0) {
            newOpp = oppList[0].clone(FALSE);
            if(newOpp.OwnerId != userId){
                newOpp.OwnerId = userId;
            }
            try{
                insert newOpp;
            } catch (DmlException ex) {
                system.debug(LoggingLevel.ERROR, ex.getMessage());
                throw ex;
            }
        }
        List<OpportunityLineItem> listItem = Database.query(queryString.makeQueryString('OLIs',oppId));
        if(listItem != NULL && listItem.size() > 0) {
            List<OpportunityLineItem> newListItems = new List<OpportunityLineItem>();
            for(OpportunityLineItem item: listItem) {
                OpportunityLineItem newLI = item.clone(FALSE);
                newLI.OpportunityId = newOpp.Id;
                newListItems.add(newLI);
            }
            try{
                insert newListItems;
            } catch (DmlException ex) {
                system.debug(LoggingLevel.ERROR, ex.getMessage());
                throw ex;
            }
        }
        return newOpp;
    }

    /**
    * Clones quotes and quote line item
    */
    public Quote__c cloneQuote(String quoteId) {
        Quote__c newQoute;
        List<Quote__c> quoteList = Database.query(queryString.makeQueryString( 'quote', quoteId ));
        if(quoteList != NULL && quoteList.size() > 0){
            newQoute = quoteList[0].clone(FALSE);
           /* if(newQoute.OwnerId != userId){
                //newQoute.OwnerId = userId;
            }*/
            try{
                newQoute.Is_Cloned__c = TRUE;
                insert newQoute;
            } catch (DmlException ex) {
                system.debug(LoggingLevel.ERROR, ex.getMessage());
                throw ex;
            }
        }
        List<Quote_Line_Item__c> listItem = Database.query(queryString.makeQueryString('QLIs', quoteId ));
        if(listItem != NULL && listItem.size() > 0) {
            List<Quote_Line_Item__c> newListItems = new List<Quote_Line_Item__c>();
            List<String> listOldId = new List<String>();
            for(Quote_Line_Item__c item: listItem) {
                listOldId.add(item.Id);
                Quote_Line_Item__c newLI = item.clone(FALSE);
                newLI.Quote__c = newQoute.Id;
                newLI.Is_Cloned__c = TRUE;
                newListItems.add(newLI);
            }
            try {
                insert newListItems;
                Map<String, String> mapId = new Map<String, String>();
                Integer i = 0;
                for(Quote_Line_Item__c item: newListItems){
                    mapId.put(listOldId.get(i), item.Id);
                    i++;
                }
                for(Quote_Line_Item__c qli: newListItems) {
                    String paretnQlisId = qli.Parent_Quote_Line_Item__c;
                    if(paretnQlisId != NULL && mapId.containsKey(paretnQlisId)) {
                        qli.Parent_Quote_Line_Item__c = mapId.get(paretnQlisId);
                    }
                }
                update newListItems;

                for(Quote_Line_Item__c qli : newListItems) {
                    qli.Is_Cloned__c = FALSE;
                }
                Database.update(newListItems);
            } catch (DmlException ex) {
                system.debug(LoggingLevel.ERROR, ex.getMessage());
                throw ex;
            }
        }
        return newQoute;
    }

    /**
    * Class is used to make query string without fields from custom settings
    */
    class QueryString{
        private Config__c settings = Config__c.getInstance('Config');
        private String option ='';
        private String id ='';

        private String prepareSelectQuery() {
            Map<String, Schema.SObjectField> fieldsMap;
            String settingsString    ='';
            String fromQueryParam   = '';
            String whereQueryParam  = ' WHERE Id =\'' + id+'\'';

            if( option.equals('opportunity')) {
                fieldsMap = Opportunity.getSObjectType().getDescribe().fields.getMap();
                fromQueryParam = ' FROM Opportunity';
                settingsString = settings.OpportunityExclusion__c;
            } else if( option.equals('OLIs')) {
                fieldsMap = OpportunityLineItem.getSObjectType().getDescribe().fields.getMap();
                fromQueryParam = ' FROM OpportunityLineItem';
                whereQueryParam = ' WHERE OpportunityId =\'' + id+'\'' +
                    ' AND Line_Item_Product_Family__c != \'DATA\'' +
                    ' AND Line_Item_Product_Family__c != \'VOICE\'' +
                    ' AND Line_Item_Product_Family__c != \'HPBX\'' +
                    ' AND Line_Item_Product_Family__c != \'OTC SPECIAL\'';
                settingsString = settings.OLIExclusion__c;
            } else if( option.equals('quote')) {
                fieldsMap = Quote__c.getSObjectType().getDescribe().fields.getMap();
                fromQueryParam = ' FROM Quote__c';
                settingsString = settings.QuoteExclusion__c;
            } else if( option.equals('QLIs')) {
                fieldsMap = Quote_Line_Item__c.getSObjectType().getDescribe().fields.getMap();
                fromQueryParam = ' FROM Quote_Line_Item__c';
                whereQueryParam = ' WHERE Quote__r.Id =\'' + id+'\'';
                settingsString = settings.QLIExclusion__c;
            }
            String queryString = '';
            for (String field : fieldsMap.keySet()) {
                queryString += field + ', ';
            }
            queryString = checkSettings(queryString,settingsString);
            queryString = queryString.substring(0, queryString.length()-2);
            return 'SELECT ' + queryString + fromQueryParam + whereQueryParam;
        }

        private String checkSettings(String str1, String str2) {
            if( str2 != NULL && str2 != ''){
                String[] fieldsArray = str2.split(',');
                for(String field: fieldsArray) {
                    field = ' ' + field.toLowerCase() + ',';
                    str1 = str1.replaceAll(field,'');
                }
            }
            return str1;
        }

        public String makeQueryString(String option,String id){
            this.id     = id;
            this.option = option;
            return prepareSelectQuery();
        }
    }
}