@isTest(seeAllData = true)
public class SalesReportManagerTest{
    
    static Quote q;
    static Opportunity o;
    static Account a;
    static Id priceBookEntryInstallId1;
    static Product2 pAccess, pInstall;
    static Id oppId;
    static OpportunityLineItem oliTestObj ;
    
    public static void setAllData(){
        
         List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where IsStandard = true limit 1];
        
        Account a2 = new Account();
        a2.name = 'Test Act';
        a2.Business_Segment__c = 'Alternate';
        //a2.RecordTypeId = mapRTa.get('New Account');
        a2.BillingStreet = 'Test';
        a2.BillingCity = 'Ontario';
        a2.BillingCountry = 'CA';
        a2.BillingPostalCode = 'A9A 9A9';
        a2.BillingState = 'ON';
        a2.Account_Status__c = 'Assigned';
        insert a2;       
                
        Opportunity o2 = new Opportunity();
        o2.Estimated_MRR__c = 500;
        o2.Name = 'Test Opp';
        o2.StageName = 'Suspect - Qualified';
        o2.Product_Category__c = 'Local';
        o2.Network__c = 'Cable';
        o2.Estimated_One_Time_Charge__c = 500;
        o2.New_Term_Months__c = 5;
        o2.AccountId = a2.id;
        o2.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o2.CloseDate = date.newInstance(2013,1,10);
        Recursion_blocker.flag = false;
        insert o2;
        
        q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', Pricebook2Id = sp.id, OpportunityId = o2.id);
        
        insert q;
        
       /* a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        //a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Test';
        a.BillingCity = 'Ontario';
        a.BillingCountry = 'CA';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingState = 'ON';
        a.Account_Status__c = 'Assigned';
        insert a;       */
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a2.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.newInstance(2013,1,10);
        o.SyncedQuoteId = q.id;
        o.Pricebook2Id = sp.Id;
        //Recursion_blocker.flag = false;
        insert o;
        oppId = o.id;        
        
        /*ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl.id;
        s1.Opportunity__c = oppId;
        insert s1;*/       
        
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '12-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
                        
        PricebookEntry pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 50;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
        priceBookEntryInstallId1 = pbeNonStandardInstall.Id;
        
        //QuoteLineItem qliInstall1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=s1.id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
        QuoteLineItem qliInstall1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
        insert qliInstall1;
        
        oliTestObj = new OpportunityLineItem ();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        oliTestObj.Product_Family__c = 'ABS Connectivity';
        oliTestObj.Line_Grouping__c = 'TV';
        oliTestObj.PricebookEntryId=pbeNonStandardInstall.id;
        oliTestObj.OpportunityId = o.id;
        oliTestObj.TotalPrice=0;
        Recursion_blocker.flag = false;
        insert oliTestObj;
        
        q.Discount_Approval__c = true;
        update q;
        
        o.StageName = 'Closed Won';
        o.Win_Reason__c = 'Price';
        o.CloseDate = System.TODAY()-20;
        //Recursion_blocker.flag = false;
        update o;
    }
    
    static testMethod Void TestMethod1(){
        setAllData();
        SalesReportManager.updateSalesReport();
    }
}