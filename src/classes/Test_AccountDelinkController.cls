/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest (seeAllData=true) 
private class Test_AccountDelinkController{
    private static Account ParentAcc;
    private static Account AccNoCas;
    private static Account PrntAccNoCas;
    private static Account ChildAcc;
    private static Account ChildAccNoCase;
    private static Account ChildAccNoCase2;
    private static Case caseTestObj;
    private static Case caseTestObj2;
    private static Mass_Update_Details__c mObj;
    static ID caseDelinkRecordTypeId = [select Id from recordType where name='Account Delink' and sObjecttype ='Case'].id;
    static List<QueueSobject> lstQueues = [SELECT Id,queue.Name, QueueId FROM QueueSobject WHERE SobjectType = 'Case'];
    static User userRec = [select id, ManagerId  from User where isActive = true limit 1];
    static User userAdmn = [select id, ManagerId  from User where isActive = true limit 1];
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static void setUpData(){
        
        ParentAcc= new Account(Name='testAcc');
        ParentAcc.BillingCountry= 'CA';
        ParentAcc.BillingPostalCode = 'A9A 9A9';
        ParentAcc.BillingState = 'MA';
        ParentAcc.BillingCity='City';
        ParentAcc.BillingStreet='Street';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.OwnerId = userRec.id;
        ParentAcc.OwnerId = userRec.id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc;
        ChildAcc= new Account(Name='testAcc1');
        ChildAcc.ParentId = ParentAcc.id;
        ChildAcc.BillingPostalCode= 'A1A 1A1';
        insert ChildAcc;
        caseTestObj = new Case();
        caseTestObj.AccountID =ParentAcc.id;
        caseTestObj.Mass_Update_Parent_Account__c = ParentAcc.id;
        caseTestObj.RecordTypeId = caseDelinkRecordTypeId;

        Schema.DescribeFieldResult resultProgressReason= Case.In_Progress_Reason__c.getDescribe();        
        caseTestObj.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
        caseTestObj.Date__c = Date.today();
        /**/        
        System.debug('***In Progress Reason : ' + caseTestObj.In_Progress_Reason__c);
        System.debug('***Date (custom): ' + caseTestObj.Date__c);
        /**/        
                
        insert caseTestObj;    
        caseTestObj2 = new Case();
        PrntAccNoCas= new Account(Name='testAccNoCase');
        PrntAccNoCas.Account_Status__c = 'Assigned';
        PrntAccNoCas.OwnerId = userRec.id;
        PrntAccNoCas.BillingPostalCode= 'A1A 1A1';
        insert PrntAccNoCas;
        caseTestObj2.AccountID =PrntAccNoCas.id;
        caseTestObj2.Mass_Update_Parent_Account__c = PrntAccNoCas.id;
        caseTestObj2.RecordTypeId = caseDelinkRecordTypeId;

        caseTestObj2.In_Progress_Reason__c = (String)(TestingUtil.getPicklistDefaultValue(resultProgressReason));
        caseTestObj2.Date__c = Date.today();
        /**/        
        System.debug('***In Progress Reason 2: ' + caseTestObj2.In_Progress_Reason__c);
        System.debug('***Date (custom) 2: ' + caseTestObj2.Date__c);
        /**/        
                
        insert caseTestObj2;    
        
        ChildAccNoCase= new Account(Name='testAccNoCase1');
        ChildAccNoCase.ParentId = PrntAccNoCas.id;
        ChildAccNoCase.BillingPostalCode= 'A1A 1A1';
        insert ChildAccNoCase;
        ChildAccNoCase2= new Account(Name='testAccNoCase1');
        ChildAccNoCase2.ParentId = PrntAccNoCas.id;
        ChildAccNoCase2.BillingPostalCode= 'A1A 1A1';
        insert ChildAccNoCase2;
        mObj = new Mass_Update_Details__c();
        mObj.Case__c = caseTestObj.id;
        mObj.Child_Account__c = ChildAcc.id;
        insert mObj;
        
    }
     static testmethod void testCaseInsert(){
        setUpData(); 
        Account PrntAccNoCas2= new Account(Name='testAccNoCase');
        PrntAccNoCas2.Account_Status__c = 'Assigned';
        PrntAccNoCas2.OwnerId = userRec.id;
        PrntAccNoCas2.BillingPostalCode= 'A1A 1A1';
        insert PrntAccNoCas2;
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('OnEdit','false');
        Apexpages.currentPage().getParameters().put('OnView','false');
        Apexpages.currentPage().getParameters().put('parentAcc',PrntAccNoCas2.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountDelinkController cls = new AccountDelinkController(controller ); 
        cls.lstAccWrprNoCaseDisplay = cls.lstAccWrprDispaly;
        AccountDelinkController.AccountWrapper awObj = new AccountDelinkController.AccountWrapper();
        awObj.aObj = ChildAccNoCase;
        awObj.selected = true;
        cls.lstAccWrprNoCaseDisplay.add(awObj);
        cls.setSortDirection('ASC');
        cls.getSortDirection();
        cls.doSelectedAccounts();
        cls.saveCase();
        cls.first();
        cls.last();
        cls.previous();
        cls.next();
        cls.cancelpage();
        Boolean b = cls.hasNext;
        b = cls.hasPrevious;
        integer i = cls.pageNumber;
        test.stopTest(); 
     }
     //Pending or New Account error
     static testmethod void testCaseInsert2(){
        setUpData();        
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('OnEdit','false');
        Apexpages.currentPage().getParameters().put('OnView','false');
        Apexpages.currentPage().getParameters().put('parentAcc',ChildAcc.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountDelinkController cls = new AccountDelinkController(controller );
        cls.submitforApproval();
        test.stopTest();   
     }
      static testmethod void testCaseViewApprove(){
        setUpData();  
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(caseTestObj.id);
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);      
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('OnEdit','false');
        Apexpages.currentPage().getParameters().put('OnView','True');
        Apexpages.currentPage().getParameters().put('parentAcc',ParentAcc.id);
        Apexpages.currentPage().getParameters().put('id',caseTestObj.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountDelinkController cls = new AccountDelinkController(controller ); 
        cls.ApproveRequest();
        ApexPages.StandardController controller2 = new ApexPages.StandardController(caseTestObj); 
        AccountDelinkController cls2 = new AccountDelinkController(controller2 ); 
        test.stopTest();    
     }
     static testmethod void testCaseViewReject(){
        setUpData();  
        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(caseTestObj.id);
        // Submit the approval request for the account
        Approval.ProcessResult result = Approval.process(req1);      
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('OnEdit','false');
        Apexpages.currentPage().getParameters().put('OnView','True');
        Apexpages.currentPage().getParameters().put('parentAcc',ParentAcc.id);
        Apexpages.currentPage().getParameters().put('id',caseTestObj.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj); 
        AccountDelinkController cls = new AccountDelinkController(controller ); 
        cls.RejectRequest();
        cls.listCaseObj.add(caseTestObj);
        cls.saveCase();
        test.stopTest();   
         
     }
      static testmethod void testCaseEdit(){
        setUpData(); 
        Mass_Update_Details__c mObj1 = new Mass_Update_Details__c();
        mObj1.Case__c = caseTestObj2.id;
        mObj1.Child_Account__c = ChildAccNoCase2.id;
        insert mObj1;       
        test.startTest();
        Apexpages.currentPage().getParameters().put('loc','500');
        Apexpages.currentPage().getParameters().put('OnEdit','true');
        Apexpages.currentPage().getParameters().put('OnView','false');
        Apexpages.currentPage().getParameters().put('parentAcc',PrntAccNoCas.id);
        Apexpages.currentPage().getParameters().put('id',caseTestObj2.id);
        ApexPages.StandardController controller = new ApexPages.StandardController(caseTestObj2); 
        AccountDelinkController cls = new AccountDelinkController(controller ); 
        cls.setSortDirection('ASC');
        cls.getSortDirection();
        cls.lstAccWrprNoCaseDisplay=cls.lstAccNoCaseDispaly;
        AccountDelinkController.AccountWrapper awObj = new AccountDelinkController.AccountWrapper();
        awObj.aObj = ChildAccNoCase2;
        awObj.selected = true;
        cls.lstAccselected.add(awObj);
        cls.remove();
        cls.lstAccWrprNoCaseDisplay.add(awObj);
        cls.doSelectedAccounts();
        cls.add();
        cls.onEdit();
        cls.cancel();
        cls.selectedCaseID = caseTestObj2.id;
        cls.selectedAccountID=PrntAccNoCas.id;
        cls.openCase();
        cls.openAccount();
        cls.listCaseObj.add(caseTestObj2);
        cls.submitforApproval();
        test.stopTest();    
     }
}