/*
    @Name: GenerateCIFExtension
    @Description: 
    @Version: 1.1.0 
    
    ===VERSION HISTORY === 
    | Version Number | Author               | Description
    | 1.0.0          | Unknown              |  Initial
    | 1.1.0          | ShiftCRM - RM        |  Include SOI and SOI requested Date fields into the generated PDF
    | 1.1.1          | Jose Camacho - RM    |  Include MAL ID field into the generated PDF
    
*/
/*
=====================================================================================================
Modified : Aakanksha Patel (March 2015) : for TAG

=====================================================================================================
*/

public without sharing class GenerateCIFExtension {
    
    public Id idCIF {get; set;}
    public String isMove {get; set;}
    public String sCIF {get; set;}
    public CIF__c aCIF {get; set;}
    public List<CIF_Site__c> cifSites {get; set;}
    public Map<Id, List<QuoteLineItem>> services {get; set;}
    List<QuoteLineItem> tempQLI = new List<QuoteLineItem>();
    
    public String showTable {get; set;}
         
    public GenerateCIFExtension(ApexPages.StandardController controller) {
        sCIF = ApexPages.currentPage().getParameters().get('cifid');
        
        if (!Utils.isEmpty(sCIF)){
            idCIF = sCIF.substring(0);
            
            System.debug('\n\n\nidCIF: ' + sCIF );
            
            aCIF = [SELECT Id, Account_Name1__c,Name, Comments__c, Contact_Email__c, Contact_Mobile__c, Contact_Name__c, Contact_Phone__c, Opp_Owner__c,
                      Contact_Title__c, Billing_Contact_Email__c, Billing_Contact_Mobile__c, Billing_Contact_Name__c, Billing_Contact_Phone__c,
                      Billing_Contact_Title__c, Credit_Approval__c, Credit_Approval_Date__c, Customer_Address__c,
                     
                      Opportunity_Name1__c, Opportunity_Number__c, Opportunity__c, COP_Owner__c, COP_Owner__r.Name, Provisioning_ID__c, Quote_Name__c, Quote_Number__c, 
                     Service_Date_Implementation__c, TSC_Assigned__c, TSC_Name__c,
                      Technical_Contact_Name__c, Technical_Contact_Name1__c, Technical_Contact_Email__c, Technical_Contact_Title__c, Technical_Contact_Mobile__c, Order_Tracker__c,
                      Technical_Contact_Phone__c, OSS__c, Oracle_CA__c, Billing_Contact_Name1__c, Contact_Name1__c, Contact_Email_Alternate__c, Contact_Email_Interconnection__c, Contact_Mobile_Alternate__c, Contact_Mobile_Interconnection__c,
                      Contact_Name_Alternate1_c__c, Contact_Name1_Interconnection__c, Contact_Alternate_Phone__c, Contact_Phone_Interconnection__c, Contact_Title_Alternate__c, Contact_Title_Interconnection__c,
                      Account_Owner__r.Name, AC_ID__c, AC_Name__c, AM_ID__c, Billing_Address__c, Program__c,
                      
                      eBilling_Admin_Contact_Name__c, eBilling_Admin_Contact_Phone__c, eBilling_Admin_Contact_Mobile__c, eBilling_Admin_Contact_Email__c,
                      eBilling_Admin_Contact__c, eBilling_Admin_Contact_Job_Title__c, Opportunity_Name__r.Type, Decision_Maker_Address__c, Billing_Entity_Code__c,Billing_Entity_Code_EoC__c, SPS_Assigned__c,
                      Customer_Order_Number__c, Customer_PO__c, Carrier_Type__c, MAL_ID__c, Manager_of_Account_Owner__c 
                      
                      FROM CIF__c where id = :idCIF];
                     
            CIFSites = [SELECT Id, Building_Entry_Address_FreeText__c, Building_Entry_Contact_FreeText__c, Building_Entry_Contact_Email_FreeText__c, Building_Entry_Contact_JobTitle_FreeText__c,
                      Building_Entry_Contact_Mobile_FreeText__c, Building_Entry_Contact_Phone_FreeText__c, CIF_Number__c, Name, Circuit_Type__c, Floor__c, Interface_Type__c,
                      Property_Mgmt_Address_FreeText__c, Property_Mgmt_Contact_FreeText__c, Property_Mgmt_Email_FreeText__c, Property_Mgmt_Job_Title_FreeText__c,
                      Property_Mgmt_Mobile_FreeText__c, Property_Mgmt_Phone_FreeText__c, Quote__c, Rack__c, Room__c, Router__c, Service_Type__c, Site__c, Site__r.Display_Name__c, Quote__r.Id, Notes__c, Order_Type__c, Schedule_A_List__c, Oracle_CA__c,
                      Target_Turn_Up__c, Hours_of_Operation__c, Confirmed_Display_Name__c, EPOM__c, INSIS__c, Project_Amount__c, MSAP_Type__c, Site__r.Z_Sites__c, PHUB_POP__c, Suite_Floor__c, Demarcation__c, 
                      Customer_Requested_SOIDate__c, SOI_Requested_Date__c, SOI_Date__c //V1.1 RM
                      FROM CIF_Site__c where CIF_Number__c = :idCIF Order By Site__c] ;
            
            Set<Id> siteIds = new Set<Id>();
            services = new Map<Id, List<QuoteLineItem>>();
            
            for(CIF_Site__c cifSite : CIFSites){
                siteIds.add(cifSite.Site__r.Id);
            }
                                  
            List<QuoteLineItem> lineItems = new List<QuoteLineItem>();
            
            try{
                lineItems = [SELECT Id, PricebookEntry.ProductCode, Site__r.id, Quote.Id, Service_Type__c, Quantity, Discount__c, DiscountWizard__c, TotalPrice, Type__c FROM QuoteLineItem WHERE Site__r.id IN :siteIds AND Quote.Id = :CIFSites[0].Quote__r.Id];
            }catch(Exception ex){
                System.debug('No Quote Line Items.  No COP Sites.');
            }
                                
            for (Id siteId : siteIds){
                tempQLI = new List<QuoteLineItem>();
                for(QuoteLineItem li : lineItems){
                    if (li.Site__r.Id == siteId){
                        tempQLI.add(li);
                    }
                }
                services.put(siteId, tempQLI);
            }        
        }      
    }
    
    
    public void previewPDF(){

        String sTable;
        List<String> lLocationTable = new List<String>();
         
         System.debug('\n\n\nCIF Sites: ' + sCIF ); 
          String strZSite;
        //Create Table
        for(CIF_Site__c cifSite:CIFSites){   
            sTable = '';
            String strSite = Utils.isEmpty(cifSite.Site__r.Display_Name__c)?'':cifSite.Site__r.Display_Name__c;
            if(cifSite.Site__c!=null)
            strZSite = (cifSite.Site__r!=null && Utils.isEmpty(cifSite.Site__r.Z_Sites__c))?'':cifSite.Site__r.Z_Sites__c.replace('\n', ', ');
            String soiDate = (cifSite.SOI_Date__c==null)?'':string.valueOf(cifSite.SOI_Date__c); //V1.1 RM
            String soiRequestedDate = (cifSite.SOI_Requested_Date__c==null)?'':string.valueOf(cifSite.SOI_Requested_Date__c); //V1.1 RM
        
            sTable = '<br/><span style="padding-left:5px;" class="font7"><b>' + label.site_A + ' : ' + strSite.toUpperCase();
            sTable += (!Utils.isEmpty(cifSite.Order_Type__c))?' ('+cifSite.Order_Type__c+')':'';
            sTable += '</b></span>'; 
            sTable += '<br/><span style="padding-left:5px;" class="font7"><b>' + label.site_Z + ' : ' + ((Utils.isEmpty(strZSite))?'':strZSite.toUpperCase());
            sTable += '</b></span><br/>';                   
            sTable += '<table  width="100%" border="0" cellpadding = "0" cellspacing = "0">';         
            
            sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="6" class="font7b"><b>'+Label.Oracle+':&nbsp;</b>' + (Utils.isEmpty(cifSite.Oracle_CA__c)?'':cifSite.Oracle_CA__c);
            sTable += '<br/><b>'+Label.Project_Amount+':&nbsp;</b>' + ((cifSite.Project_Amount__c!=null)?''+cifSite.Project_Amount__c:'');
            sTable += '<br/><b>'+Label.EPOM+':&nbsp;</b>' + (Utils.isEmpty(cifSite.EPOM__c)?'':cifSite.EPOM__c);
            sTable += '<br/><b>'+Label.INSIS+':&nbsp;</b>' + (Utils.isEmpty(cifSite.INSIS__c)?'':cifSite.INSIS__c);
            sTable += '<br/><b>'+Label.PHUB_POP+':&nbsp;</b>' + (Utils.isEmpty(cifSite.PHUB_POP__c)?'':cifSite.PHUB_POP__c);
            sTable += '</td></tr>';    
            sTable += '<tr><td style="padding:5px 5px 0px 5px;width:15%;" colspan="1" class="font7b"><b>Quote Site:&nbsp;</b></td><td colspan="5" style="padding:5px 5px 0px 5px;" class="font7b">' + (Utils.isEmpty(cifSite.Site__r.Display_Name__c)?'':cifSite.Site__r.Display_Name__c)+'</td><tr/>';
            sTable += '<tr><td style="padding:0px 5px 5px 5px;width:15%;" colspan="1" class="font7b"><b>Suite/Floor:&nbsp;</b></td><td colspan="5" style="padding:0px 5px 5px 5px;" class="font7b">' + (Utils.isEmpty(cifSite.Suite_Floor__c)?'':cifSite.Suite_Floor__c)+'</td></tr>';
            sTable += '<tr><td style="padding:0px 5px 5px 5px;width:15%;" colspan="1" class="font7b"><b>SOI Date:&nbsp;</b></td><td colspan="5" style="padding:0px 5px 5px 5px;" class="font7b">' + soiDate +'</td></tr>'; //V1.1 RM
            sTable += '<tr><td style="padding:0px 5px 5px 5px;width:15%;" colspan="1" class="font7b"><b>Customer Requested Date:&nbsp;</b></td><td colspan="5" style="padding:0px 5px 5px 5px;" class="font7b">' + soiRequestedDate +'</td></tr>'; //V1.1 RM
            sTable += '<tr>';
            sTable += '<td colspan="3" style="padding:5px 5px 5px 5px;width:50%; vertical-align:text-top;" class="font7b"><br/><b>Building Entry Contact Information</b><br/>';
            sTable += 'Name:&nbsp;' + (Utils.isEmpty(cifSite.Building_Entry_Contact_FreeText__c)?'':cifSite.Building_Entry_Contact_FreeText__c) + '<br/>' + 'Job Title:&nbsp;' + (Utils.isEmpty(cifSite.Building_Entry_Contact_JobTitle_FreeText__c)?'':cifSite.Building_Entry_Contact_JobTitle_FreeText__c);
            sTable += '<br/>Address:&nbsp;' + (Utils.isEmpty(cifSite.Building_Entry_Address_FreeText__c)?'':cifSite.Building_Entry_Address_FreeText__c);
            sTable += '<br/>Phone:&nbsp;' + (Utils.isEmpty(cifSite.Building_Entry_Contact_Phone_FreeText__c)?'':cifSite.Building_Entry_Contact_Phone_FreeText__c) + '<br/>' + 'Mobile:&nbsp;' + (Utils.isEmpty(cifSite.Building_Entry_Contact_Mobile_FreeText__c)?'':cifSite.Building_Entry_Contact_Mobile_FreeText__c);
            sTable += '<br/>Email:&nbsp;' + (Utils.isEmpty(cifSite.Building_Entry_Contact_Email_FreeText__c)?'':cifSite.Building_Entry_Contact_Email_FreeText__c); 
            sTable += '</td>';
            
            sTable += '<td colspan="3" style="padding:5px 5px 5px 5px;width:50%; vertical-align:text-top;" class="font7b"><br/><b>Property Management Contact Information</b><br/>';
            sTable += 'Name:&nbsp;' + (Utils.isEmpty(cifSite.Property_Mgmt_Contact_FreeText__c)?'':cifSite.Property_Mgmt_Contact_FreeText__c) + '<br/>' + 'Job Title:&nbsp;' + (Utils.isEmpty(cifSite.Property_Mgmt_Job_Title_FreeText__c)?'':cifSite.Property_Mgmt_Job_Title_FreeText__c);
            sTable += '<br/>Address:&nbsp;' + (Utils.isEmpty(cifSite.Property_Mgmt_Address_FreeText__c)?'':cifSite.Property_Mgmt_Address_FreeText__c);
            sTable += '<br/>Phone:&nbsp;' + (Utils.isEmpty(cifSite.Property_Mgmt_Phone_FreeText__c )?'':cifSite.Property_Mgmt_Phone_FreeText__c)+ '<br/>'+'Mobile:&nbsp;' + (Utils.isEmpty(cifSite.Property_Mgmt_Mobile_FreeText__c)?'':cifSite.Property_Mgmt_Mobile_FreeText__c);
            sTable += '<br/>Email:&nbsp;' + (Utils.isEmpty(cifSite.Property_Mgmt_Email_FreeText__c)?'':cifSite.Property_Mgmt_Email_FreeText__c); 
            sTable += '</td>';
            sTable += '</tr>';
            
            sTable += '<tr>';
            sTable += '<td colspan="3" style="padding:5px 5px 5px 5px;width:50%; vertical-align:text-top;" class="font7b"><b>Demark Location</b><br/>';
        //    sTable += '<td style="padding:5px 5px 5px 5px;width:50%; vertical-align:text-top;" class="font7b"><b>Demark Location</b><br/>';
            sTable += 'Floor:&nbsp;' + (Utils.isEmpty(cifSite.Floor__c)?'':cifSite.Floor__c) + '<br/>' + 'Room:&nbsp;' + (Utils.isEmpty(cifSite.Room__c)?'':cifSite.Room__c) + '<br/>' + 'Rack:&nbsp;' + (Utils.isEmpty(cifSite.Rack__c)?'':cifSite.Rack__c) + '<br/>' + 'Target Turn Up:&nbsp;' + (cifSite.Target_Turn_Up__c == null?'':cifSite.Target_Turn_Up__c.format('MMM dd, yyyy hh:mm a')) + '<br/>' + 'Hours of Operation:&nbsp;' + (Utils.isEmpty(cifSite.Hours_of_Operation__c)?'':cifSite.Hours_of_Operation__c);
            sTable += '<br/>' + Label.MSAP_Type + ':&nbsp;' + (Utils.isEmpty(cifSite.MSAP_Type__c)?'':cifSite.MSAP_Type__c);
            sTable += '<br/>Demarc from Mandatory Fields:&nbsp;' + (Utils.isEmpty(cifSite.Demarcation__c)?'':cifSite.Demarcation__c);
            sTable += '</td>';
            
            sTable += '<td colspan="3" style="padding:5px 5px 5px 5px;width:50%; vertical-align:text-top;" class="font7b"><b>Product/Technical Details</b><br/>';
        //    sTable += '<td style="padding:5px 5px 5px 5px;width:50%; vertical-align:text-top;" class="font7b"><b>Product/Technical Details</b><br/>';
            sTable += 'Circuit Type:&nbsp;' + (Utils.isEmpty(cifSite.Circuit_Type__c)?'':cifSite.Circuit_Type__c) + '<br/>' + 'Router:&nbsp;' + cifSite.Router__c + '<br/> ' + 'Interface Type:&nbsp;' + cifSite.Interface_Type__c;
            sTable += '</td>';
            sTable += '</tr>';           

             sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="6" class="font7b"><b>Services:&nbsp;</b><br/>';
             
           //  sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="2" class="font7b"><b>Services:&nbsp;</b><br/>';
             List<QuoteLineItem> quoteLineItems = services.get(cifSite.Site__r.Id);
             
             System.debug('\n\n\n\n'+ quoteLineItems);
                
                  
               // Line Item type removed - did not make it to prod in Dec 2013 Release       
             for(QuoteLineItem qli : quoteLineItems){
                sTable += Utils.isEmpty(qli.Service_Type__c) ? '' : (qli.Service_Type__c + (Utils.isEmpty(qli.Type__c)?'':' - ' + (qli.Type__c.startsWith('N')?'New':qli.Type__c.startsWith('R')?'Renewal':'Upsell'))) + ' - (';
                sTable += Utils.isEmpty(qli.PricebookEntry.ProductCode)?'Product Code: --':'Product Code: ' + qli.PricebookEntry.ProductCode; 
                sTable += ' / Total Price: $' + qli.TotalPrice;
                sTable += ' / Quantity: ' + qli.Quantity;
                sTable += ((qli.DiscountWizard__c!=null)?' / Discount (%): ' + qli.DiscountWizard__c: '') + ((qli.Discount__c!=null)?' / Discount ($): ' + qli.Discount__c: '');
                sTable += ')<br/>';
             }
             
             
             /*
            for(QuoteLineItem qli : quoteLineItems){
                sTable += (Utils.isEmpty(qli.Service_Type__c)?'':qli.Service_Type__c) + ' - (';
                sTable += Utils.isEmpty(qli.PricebookEntry.ProductCode)?'Product Code: --':'Product Code: ' + qli.PricebookEntry.ProductCode; 
                sTable += ' / Total Price: $' + qli.TotalPrice;
                sTable += ' / Quantity: ' + qli.Quantity;
                sTable += ((qli.DiscountWizard__c!=null)?' / Discount (%): ' + qli.DiscountWizard__c: '') + ((qli.Discount__c!=null)?' / Discount ($): ' + qli.Discount__c: '');
                sTable += ')<br/>';
             }
         */
             
             sTable += '</td></tr>';
           
             
            if (!Utils.isEmpty(cifSite.Schedule_A_List__c))
                sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="6" class="font7b"><b>Schedule A</b><br/>'+Utils.convertToHTML(cifSite.Schedule_A_List__c)+'</td></tr>';                        
            sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="6" class="font7b"><b>Additional Notes</b><br/>'+(Utils.isEmpty(cifSite.Notes__c)?'':cifSite.Notes__c)+'</td></tr>';
            
           //            sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="2" class="font7b"><b>Schedule A</b><br/>'+Utils.convertToHTML(cifSite.Schedule_A_List__c)+'</td></tr>';                        
        //    sTable += '<tr><td style="padding:5px 5px 5px 5px;" colspan="2" class="font7b"><b>Additional Notes</b><br/>'+(Utils.isEmpty(cifSite.Notes__c)?'':cifSite.Notes__c)+'</td></tr>';
            
            sTable += '</table> <br/>';
            
            // Add sTable to List String
            lLocationTable.add(sTable);
        }
                
        showTable = '<table width="100%" border="0" cellpadding = "0" cellspacing = "0">';
        Integer i = 1;
        for(String s1:lLocationTable){   
                //Wrap Table in side Table to avoid page break inside table 
                showTable += '<tr><td><table  style="page-break-inside: avoid;"  width="100%" border="1" cellpadding = "0" cellspacing = "0">';
                showTable += '<tr>';
                showTable += '<td>' + s1 + '</td>' ;
                showTable += '</tr>';
                showTable += '</table></td></tr><tr><td>&nbsp;</td></tr>';
                
                i++;
        }
            
        showTable += '</table>';
    }
    
    public PageReference savePdf() {
        List<Attachment> listCIFAttachments = new List<Attachment>();
        Attachment cifAttachment = new Attachment();
        try{
            CIF__c cif = [SELECT Id, Name FROM CIF__c WHERE Id = :idCIF LIMIT 1];
        
            PageReference pdf = Page.CIFDocument;
          
            // Add parent id to the parameters for standardcontroller
            pdf.getParameters().put('cifid',idCIF);
              
            // The contents of the attachment from the pdf
            Blob body;
         
            try {
                // returns the output of the page as a PDF
                body = pdf.getContent();
         
            // need to pass unit test -- current bug    
            } catch (VisualforceException e) {
                body = Blob.valueOf('Some Text');
            }
         
         
            cifAttachment.Body = body;
            cifAttachment.ParentId = idCIF;
            cifAttachment.Name = cif.Name + '.pdf';
            
            insert cifAttachment;
    
            
            listCIFAttachments = [SELECT id FROM Attachment WHERE id != :cifAttachment.id and parentId = :idCIF AND Name = :cifAttachment.Name];
            if (listCIFAttachments.size() > 0){
                delete listCIFAttachments;
            }
           
    
            aCIF.Attachment_ID__c = cifAttachment.Id;
            UPDATE aCIF;
        }catch(Exception ex){
            return null;
        }
        return new PageReference('/'+idCIF);
    }
    

    
    static testMethod void generateCIFUnitTest() {
        Id carrierOppId;
        
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto)
        {
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act 23';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Street'; a_carrier.Account_Status__c = 'Assigned';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        a_carrier.ParentId = null;
        insert a_carrier;       
        
        Opportunity o_carrier = new Opportunity();
        o_carrier.Estimated_MRR__c = 500;
        o_carrier.Name = 'Test Opp';
        o_carrier.StageName = 'Suspect - Qualified';
        o_carrier.Product_Category__c = 'Local';
        o_carrier.Network__c = 'Cable';
        o_carrier.Estimated_One_Time_Charge__c = 500;
        o_carrier.New_Term_Months__c = 5;
        o_carrier.AccountId = a_carrier.id;
        o_carrier.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o_carrier.CloseDate = date.today();
        insert o_carrier;
        carrierOppId = o_carrier.id;
        
        Pricebook2 carrier_sp = new Pricebook2();
        carrier_sp = [select id from Pricebook2 where Name = :'Carrier PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        Quote quote = new Quote(Name='q', Term__c='3-Years', OpportunityId=carrierOppId, Pricebook2Id = carrier_sp.id);
        insert quote;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
            
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
                        
        Site__c s4 = new Site__c();
        s4.Suite_Floor__c = '11a';
        s4.Street_Name__c = 'Somewhere';
        s4.Street_Number__c  = '7';
        s4.City__c = 'Coty';
        s4.Postal_Code__c = 'A3A1A1';
        s4.Province_Code__c = 'ON';
        s4.CLLI_SWC__c = '123113';
        s4.ServiceableLocation__c = sl.Id;
        s4.Opportunity__c = carrierOppId;
        s4.Is_a_Z_Site__c = true; 
        insert s4;
       
        
        Site__c s5 = new Site__c();
        s5.Suite_Floor__c = '11b';
        s5.Street_Name__c = 'Somewhere';
        s5.Street_Number__c  = '8';
        s5.City__c = 'Coty';
        s5.Postal_Code__c = 'A3A1A1';
        s5.Province_Code__c = 'ON';
        s5.CLLI_SWC__c = '123113';
        s5.ServiceableLocation__c = sl2.Id;
        s5.Opportunity__c = carrierOppId; 
        s5.Z_Site__c = s4.Id;
        insert s5;
        
        
        Site__c s6 = new Site__c();
        s6.Suite_Floor__c = '11c';
        s6.Street_Name__c = 'Somewhere';
        s6.Street_Number__c  = '9';
        s6.City__c = 'Coty';
        s6.Postal_Code__c = 'A3A1A1';
        s6.Province_Code__c = 'ON';
        s6.CLLI_SWC__c = '123113';
        s6.ServiceableLocation__c = sl2.Id;
        s6.Opportunity__c = carrierOppId; 
        insert s6;
       
        
        Site__c s7 = new Site__c();
        s7.Suite_Floor__c = '11a';
        s7.Street_Name__c = 'Somewhere';
        s7.Street_Number__c  = '10';
        s7.City__c = 'Coty';
        s7.Postal_Code__c = 'A3A1A1';
        s7.Province_Code__c = 'ON';
        s7.CLLI_SWC__c = '123113';
        s7.ServiceableLocation__c = sl2.Id;
        s7.Opportunity__c = carrierOppId; 
        s7.Is_a_Z_Site__c = true;
        insert s7;
        
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = s4.Id;
        qs.Quote__c = quote.Id;
        insert qs;
        
               
        qs = new Quote_Site__c();
        qs.Site__c = s5.Id;
        qs.Quote__c = quote.Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = s6.Id;
        qs.Quote__c = quote.Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = s7.Id;
        qs.Quote__c = quote.Id;
        insert qs;
        
        // Carrier 
        Product2 carrierProduct  = new Product2();
        carrierProduct.IsActive = true;
        carrierProduct.Name = 'Fibre';
        carrierProduct.Charge_Type__c = 'NRC';
        carrierProduct.Access_Type__c = 'Ethernet EON';
        carrierProduct.Access_Type_Group__c = 'Fibre';
        carrierProduct.Category__c = 'Access';
        carrierProduct.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        carrierProduct.RecordTypeId = mapRTp.get('Carrier Wholesale Products');
        carrierProduct.Service_Term__c = '3-Years';
        carrierProduct.Mark_Up_Factor__c = 1.0;
        carrierProduct.Start_Date__c = Date.today();
        carrierProduct.End_Date__c = Date.today()+1;
        insert carrierProduct;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Pricebook2.IsStandard = true];
        
        PricebookEntry carrier_peStandard = new PricebookEntry();
        carrier_peStandard.Pricebook2Id = sp1.id;
        carrier_peStandard.UnitPrice = 60;
        carrier_peStandard.Product2Id = carrierProduct.id;
        carrier_peStandard.IsActive = true;
        carrier_peStandard.UseStandardPrice = false;
        
        insert carrier_peStandard;
        
        PricebookEntry carrier_pe1 = new PricebookEntry();
        carrier_pe1.Pricebook2Id = carrier_sp.id;
        carrier_pe1.UnitPrice = 50;
        carrier_pe1.Product2Id = carrierProduct.id;
        carrier_pe1.IsActive = true;
        carrier_pe1.UseStandardPrice = false;
        
        insert carrier_pe1;
        
        //Product 2
        Product2 carrier_p1  = new Product2();
        carrier_p1.IsActive = true;
        carrier_p1.Name = 'ProductMe1';
        carrier_p1.Charge_Type__c = 'MRC';
        carrier_p1.Mark_Up_Factor__c = 1.0;
        carrier_p1.Start_Date__c = Date.today();
        carrier_p1.End_Date__c = Date.today()+1;
        insert carrier_p1;
        
        PricebookEntry carrier_pe21Standard = new PricebookEntry();
        carrier_pe21Standard.Pricebook2Id = sp1.id;
        carrier_pe21Standard.UnitPrice = 60;
        carrier_pe21Standard.Product2Id = carrier_p1.id;
        carrier_pe21Standard.IsActive = true;
        carrier_pe21Standard.UseStandardPrice = false;
        
        insert carrier_pe21Standard;
        
        PricebookEntry carrier_pe21 = new PricebookEntry();
        carrier_pe21.Pricebook2Id = carrier_sp.id;
        carrier_pe21.UnitPrice = 60;
        carrier_pe21.Product2Id = carrier_p1.id;
        carrier_pe21.IsActive = true;
        carrier_pe21.UseStandardPrice = false;
        
        insert carrier_pe21;
        
        //Line Item Product 1
        QuoteLineItem qli = new QuoteLineItem();
        qli.PricebookEntryId = carrier_pe1.id;
        qli.Site__c = s4.Id;
        qli.Quantity = 3;
        qli.QuoteId = quote.id;
        qli.UnitPrice = 50;
        qli.Visible_Quote_Line_Item__c = true;
        insert qli;
        
        //Line Item Product 2
        QuoteLineItem qli2 = new QuoteLineItem();
        qli2.PricebookEntryId = carrier_pe21.id;
        qli2.Site__c = s4.id;
        qli2.Quantity = 1;
        qli2.QuoteId = quote.id;
        qli2.UnitPrice = 60;
        qli2.Visible_Quote_Line_Item__c = true;
        insert qli2;
        
        //Line Item Product 1
        QuoteLineItem qli3 = new QuoteLineItem();
        qli3.PricebookEntryId = carrier_pe21.id;
        qli3.Site__c = s5.id;
        qli3.Quantity = 2;
        qli3.QuoteId = quote.id;
        qli3.UnitPrice = 60;
        qli3.Visible_Quote_Line_Item__c = true;
        insert qli3;
        
        //Line Item Product 1
        QuoteLineItem qli4 = new QuoteLineItem();
        qli4.PricebookEntryId = carrier_pe21.id;
        qli4.Site__c = s6.id;
        qli4.Quantity = 2;
        qli4.QuoteId = quote.id;
        qli4.UnitPrice = 60;
        qli4.Visible_Quote_Line_Item__c = true;
        insert qli4;
        
        //Line Item Product 1
        QuoteLineItem qli5 = new QuoteLineItem();
        qli5.PricebookEntryId = carrier_pe21.id;
        qli5.Site__c = s7.id;
        qli5.Quantity = 2;
        qli5.QuoteId = quote.id;
        qli5.UnitPrice = 60;
        qli5.Visible_Quote_Line_Item__c = true;
        insert qli5;
        
        
        Test.startTest();
        
        User user1 = [SELECT Id, Name From User LIMIT 1];
        
        o_carrier.SyncedQuoteId = quote.Id;
        UPDATE o_carrier;   
        
        Id cifId = CIFWebservices.createCIF('' + o_carrier.Id);
        
        CIF__c cif = [SELECT Id FROM CIF__C WHERE Id = :cifId];
        
        /*
        CIF__c cif = new CIF__c();
        cif.Opportunity__c = user1.Id; 
        
        cif.Opportunity_Name__c = o_carrier.Id; 
        
        
        insert cif;
        */
        
        
        //Use the PageReference Apex class to instantiate a page
        PageReference pageRef = null;
        
        //https://cs4.salesforce.com/a0BP00000007FBN/e?retURL=%2F001P0000004O65uIAC
        pageRef = New PageReference('/?cifid=' + String.valueOf(cifId)); 
        //In this case, the Visualforce page named 'jobpage' is the starting point of this test method.
        Test.setCurrentPage(pageRef);
          
        
        ApexPages.StandardController sc = new ApexPages.standardController(cif);
        GenerateCIFExtension cif_controller = new GenerateCIFExtension(sc) ;
        
        cif_controller.previewPDF();
        cif_controller.savePdf(); 

        Test.stopTest();
       

    }
   
}