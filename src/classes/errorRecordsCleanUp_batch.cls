/*
===============================================================================
 Class Name   : errorRecordsCleanUp_batch
===============================================================================
PURPOSE:    This batch class deletes all the Error records that are older than
            one week.

Author   : Jacob Klay, Rogers Communications
Date     : 24-June-2o14

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/

global class errorRecordsCleanUp_batch implements Database.Batchable<SObject>, Database.Stateful{
    
    //New week starts 12AM on Sunday. Delete everything older than two Sundays back
    global String query = 'SELECT id, account_name__c, userId__c, user_name__c, Error_Message__c, statusCode__c FROM ErrorRecords__c WHERE isDeleted = false AND CreatedDate < LAST_WEEK LIMIT 9999';
    //global String testQuery = 'SELECT id, account_name__c, userId__c, user_name__c, Error_Message__c, statusCode__c FROM ErrorRecords__c WHERE isDeleted = false LIMIT 9999'
    static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];

    static OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;
    
    Integer totalRecords = 0;

    global Database.QueryLocator start(Database.BatchableContext BC){        
        if(Test.isRunningTest())
        query = 'SELECT id, account_name__c, userId__c, user_name__c, Error_Message__c, statusCode__c FROM ErrorRecords__c WHERE isDeleted = false AND CreatedDate < LAST_WEEK LIMIT 2';
    
        return Database.getQueryLocator(query);
    }

    global void execute(Database.BatchableContext BC, List<ErrorRecords__c> scope) {
        List<ErrorRecords__c> errorList = new List<ErrorRecords__c>();
        for(ErrorRecords__c element:scope){
            errorList.add(element);
        }
        try {
            totalRecords += errorList.size();
            System.debug('####totalRecords size: ' + totalRecords);
            delete errorList;
            }catch(DmlException e) {
            //Send an email to the Apex job's submitter on failure.
            Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
            String[] toAddresses = new String[] {admin.email__c};
            System.debug('####toAddresses: ' + toAddresses);
            mail.setToAddresses(toAddresses); 
            mail.setSubject('Batch process to delete error records from ErrorRecords__c table exception');
            mail.setPlainTextBody('Batch process to delete error records from ErrorRecords__c table threw the following exception: ' + e.getMessage());
            Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
            }
   }

    global void finish(Database.BatchableContext BC){
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {admin.email__c};
        System.debug('####toAddresses: ' + toAddresses);
        String body = 'Hello,<br/><br/>Batch process to delete errors records from ErrorRecords__c table has been completed. <br/><br/>' + 'Total deleted records: ' + String.valueof(totalRecords); 
        body += '<br/> <br/>Best Regards, <br/>Rogers IT Team'; 
        email.setToAddresses(toAddresses); 
        email.setSubject('Batch Completion Report: ErrorRecords');        
        email.setSaveAsActivity(false);  
        email.setHtmlBody(body);
        email.setOrgWideEmailAddressId(orgEmailId.id);
        
        //email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}