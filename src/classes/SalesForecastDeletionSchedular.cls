/*Class Name :SalesForecastDeletionSchedular.
 *Description : .
 *Created By : Deepika Rawat.
 *Created Date :05/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class SalesForecastDeletionSchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
        date dT1=system.Today().addDays(-SFCheckPoint);
        SalesForecastDeletionBatch salesForecastObj = new SalesForecastDeletionBatch();
        if(!Test.isRunningTest()){
        salesForecastObj.Query = 'select id , Start_of_Deployment_Month__c from Sales_Forecast__c where Start_of_Deployment_Month__c>=:dT1';
        }
        else{
        salesForecastObj.Query = 'select id , Start_of_Deployment_Month__c from Sales_Forecast__c where Start_of_Deployment_Month__c>=:dT1 limit 2';
        
        }
        ID salesMeasurementBatchProcessId = Database.executeBatch(salesForecastObj,200);
    }
}