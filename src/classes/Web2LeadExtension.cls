/* 
* Name          : Web2LeadExtension
* Author        : Shift CRM
* Description   : Extension to web to lead
*
* Maintenance History: 
* Date ------------  Name  ----  Version ---  Remarks 
* 03/06/2014       Hermes      1.0         CR522: added Referral_Program_Name to mapping
*/

public class Web2LeadExtension {
    private final Lead weblead;
    private final Boolean isFrench;
    
    //public String salutation1 {get; set;}
    //public String salutation2 {get; set;}
    //public String employeeSegment {get; set;}

    public Web2LeadExtension(ApexPages.StandardController stdController) {
       weblead = (Lead)stdController.getRecord();
       isFrench = ApexPages.currentPage().getUrl().toLowerCase().contains('french');
    }

     public PageReference saveLead() {
       try {
             
        webLead.webLead__c = true;
               
        Database.DMLOptions dmo = new Database.DMLOptions();
        dmo.assignmentRuleHeader.useDefaultRule= true;
        webLead.setOptions(dmo);
        
        if (webLead.Same_As_Head_Office__c){
            webLead.Physical_City__c = webLead.City;
            webLead.Physical_Country__c = webLead.Country;
            webLead.Physical_Postal_Code__c = webLead.PostalCode;
            webLead.Physical_State_Province__c = webLead.State;
            webLead.Physical_Street__c = webLead.Street;
        }
        
        if (isFrench){
          //  webLead.Salutation = salutation1;
            //webLead.Referral_Salutation__c = salutation2;
            if (webLead.Lead_Rep_Type__c == 'Solutions d’affaires Rogers')
              webLead.Lead_Rep_Type__c = 'Rogers Business Solutions (RBS)';
            else if (webLead.Lead_Rep_Type__c == 'Sans-fil')
              webLead.Lead_Rep_Type__c = 'Wireless / MDU / CMA';
             else if (webLead.Lead_Rep_Type__c == 'MDU – Unité résidentielle multiple')
              webLead.Lead_Rep_Type__c = 'Wireless / MDU / CMA';
             else if (webLead.Lead_Rep_Type__c == 'CMA – Comptes commerciaux majeurs')
              webLead.Lead_Rep_Type__c = 'Wireless / MDU / CMA';
             else if (webLead.Lead_Rep_Type__c == 'Câble')
              webLead.Lead_Rep_Type__c = 'Cable';
             else if (webLead.Lead_Rep_Type__c == 'Techniciens sur le terrain')
              webLead.Lead_Rep_Type__c = 'Field Technicians';
             else if (webLead.Lead_Rep_Type__c == 'Autre')
              webLead.Lead_Rep_Type__c = 'Other';
            
        }
        
        webLead.leadSource = 'Employee Referral Program';
        webLead.Referral_Program_Name__c = 'Lead Source';  //1.0 Added hidden mapping
        webLead.Lead_Type__c='Wireline';
        insert(weblead);
       }

       catch(System.DMLException e) {
           ApexPages.addMessages(e);
           return null;
       }

       PageReference p;
      
       p = (isFrench)?Page.WebToLeadConfirmationFrench:Page.WebToLeadConfirmation;
       p.setRedirect(true);
       return p;
     }
     
     public List<SelectOption> getFrenchSalutations(){
        List<SelectOption> options = new List<SelectOption>();
       
        options.add(new SelectOption('Monsieur', 'Monsieur'));
        options.add(new SelectOption('Madame', 'Madame'));       
      
        return options;
    }
    
    public List<SelectOption> getFrenchEmployeeSegments(){
        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '-- None --'));
        options.add(new SelectOption('Solutions d’affaires Rogers', 'Solutions d’affaires Rogers'));
        options.add(new SelectOption('Sans-fil', 'Sans-fil'));    
        options.add(new SelectOption('MDU – Unité résidentielle multiple', 'MDU – Unité résidentielle multiple'));
        options.add(new SelectOption('CMA – Comptes commerciaux majeurs', 'CMA – Comptes commerciaux majeurs')); 
        options.add(new SelectOption('Câble', 'Câble'));
        options.add(new SelectOption('Techniciens sur le terrain', 'Techniciens sur le terrain'));    
        options.add(new SelectOption('Autre', 'Autre'));
      
        return options;
    }
    
    //Test Coverage Migrated to Shift_Web2LeadExtension_Test
    
     /*
     static testMethod void Web2LeadTest() {
        Lead lead1 = new Lead(Company='Test Company', LastName = 'Test1', Phone='416-555-1212', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='Ontario', Street='Sesame Street', Same_As_Head_Office__c = true);
        PageReference pageRef = New PageReference('/apex/WebToLead2'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench1() {
        Lead lead1 = new Lead(Company='Test Company', LastName = 'Test1', Phone='416-555-1212', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Solutions d’affaires Rogers');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench2() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='Ontario', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Sans-fil');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench3() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Autre');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench4() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'MDU – Unité résidentielle multiple');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench5() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'CMA – Comptes commerciaux majeurs');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench6() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Câble');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }
     
     static testMethod void Web2LeadTestFrench7() {
        Lead lead1 = new Lead(Company='Test Company', Phone='416-555-1212', LastName = 'Test1', Referral_Agent_ID__c='123456', City='city', Country='country', PostalCode='M1C3E2', State='ON', Street='Sesame Street', Same_As_Head_Office__c = true, Lead_Rep_Type__c = 'Techniciens sur le terrain');
        PageReference pageRef = New PageReference('/apex/WebToLeadFrench'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(lead1);
        Web2LeadExtension controller = new Web2LeadExtension(sc);
        controller.webLead.Same_As_Head_Office__c = true;
        controller.saveLead();
        controller.getFrenchEmployeeSegments();
        controller.getFrenchSalutations();
     }*/
}