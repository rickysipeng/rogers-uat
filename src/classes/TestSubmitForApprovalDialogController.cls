@isTest (seeAllData=true) 
private class TestSubmitForApprovalDialogController {

        private static Account a ;
        private static Contact testContact;
        private static Quote testQuote;
        private static Opportunity oppTestObj;
        private static List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];

        static void setUpData(){
        //Account
        a = new Account();
        a.name= 'A45cc56ou67ntTe57804st';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';
        a.BillingStreet ='Test';
        a.BillingCity = 'Ontario';
        a.BillingState = 'ON';
        a.BillingPostalCode = 'A9A 9A9';
        a.BillingCountry = 'CA'; 

        insert a;
        
        //Opportunity
         oppTestObj = new Opportunity(
                                Name = 'oppTestObj',
                                StageName = 'oppTestObj',
                                CloseDate = Date.today().addDays(2),
                                AccountID=a.id);
                                
                                
        insert oppTestObj;
        
        //Quote
         testQuote = new Quote();
         testQuote.Name='Test Quote';
         testQuote.OpportunityId=oppTestObj.Id;
         insert testQuote;
      
      }

       static testmethod void TestMethod1(){
         test.startTest();
         setUpData();
         ApexPages.StandardController sc = new ApexPages.standardController(testQuote);
         SubmitForApprovalDialogController sfadcont= new SubmitForApprovalDialogController(sc);
         ApexPages.currentPage().getParameters().put('quoteId',testQuote.Id);
         sfadcont.executeLogic();
         test.stopTest();
         }


}