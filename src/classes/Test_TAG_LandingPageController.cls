/**************************************************************************************
Apex Class Name     : Test_TAG_LandingPageController
Version             : 1.0 
Created Date        : 8 Apr 2015
Function            : This is the Test Class for TAG_LandingPageController.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             8/04/2015              Original Version
*************************************************************************************/
@isTest(SeeAllData = false)
private class Test_TAG_LandingPageController
{     
    private static  User UserRec1;
    private static  User UserRec2;
    private Static Profile pEmp = [Select Id from Profile where name like '%Rogers%' limit 1];
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();         
   
    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
    */
    private static void setUpData()
    {       
        TAG_CS.Allowed_Bulk_Access__c ='Bulk_Request_Permission';
        insert TAG_CS;

        userRec1 = new User(LastName = 'Mark O’BrienRoger',Owner_Type__c = 'District', Alias = 'alRoger1', Email='test1@Rogertest.com', Username='test1@Rogertest.com', CommunityNickname = 'nickRoger2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec1;
    }
    
   
    /*
     Description : This is is a method to test the User with Permission set
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_user_wid_permission()
    {
        
        setUpData();
        System.RunAs(userRec1){
        Test.startTest();
        TAG_LandingPageController landingPgCont = new TAG_LandingPageController();   
           
        Test.stopTest();     
        }
            
    }
  
}