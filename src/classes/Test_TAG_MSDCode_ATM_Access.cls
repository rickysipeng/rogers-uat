/*
===============================================================================
 Class Name   : Test_TAG_MSDCode_ATM_Access
===============================================================================
PURPOSE:    This is a Test class for TAG_MSDCode_ATM_Access Trigger.
              
Developer: Jayavardhan
Date: 03/09/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/09/2015           Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_MSDCode_ATM_Access{
     private static Account ParentAcc;
     private static Account ParentAcc1;
     private static List<AccountTeamMember> parentAccTeam;
     private static List<MSD_Code__c> msdcodeObjList;
     private static Profile pEmp = [Select Id from Profile where Name Like '%Rogers%' limit 1];
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static User userRec;
     private static User userRec2;
     //create Test Data
     private static void setUpData(){
        TAG_CS.Team_Custom_Object_Access__c='Read';
        TAG_CS.Unassigned_User__c ='Unassigned User';
        insert TAG_CS;
        userRec = new User(LastName = 'Mark O’BrienRoger', Alias = 'alRoger', Email='test@Rogertest.com', Username='test777@Rogertest.com', CommunityNickname = 'nickRoger777', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’BrienRoger2', Alias = 'aRoger', Email='test@Rogertest.com', Username='test778@Rogertest2.com', CommunityNickname = 'nickRoger778', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
        
        ParentAcc= new Account(Name='ParentAcc');
        ParentAcc.Account_Status__c = 'Unassigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc;
        
        parentAccTeam = new List<AccountTeamMember>();        
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'Owner';
        newMem.AccountId = ParentAcc.Id;
        newmem.UserId = ParentAcc.OwnerId; 
        parentAccTeam.add(newMem);
        
        AccountTeamMember newMem2 = new AccountTeamMember();
        newMem2 .TeamMemberRole = 'SDC';
        newMem2 .AccountId = ParentAcc.Id;
        newMem2 .UserId = userRec.Id; 
        parentAccTeam.add(newMem2 );
        
        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'ISR - Large';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = userRec2.Id; 
        parentAccTeam.add(newMem3);
        
        insert parentAccTeam;   
        
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with MSD Code obj records created with account team members
    *******************************************************************************************************/
    static testmethod void testAccountTeamMemberAccess(){
        setUpData(); 
        msdcodeObjList = new  List<MSD_Code__c>();
        MSD_Code__c  msd1 = new MSD_Code__c();
        msd1.ownerId = userRec.Id;
        msd1.Account__c = ParentAcc.Id;
        
        msdcodeObjList.add(msd1);
        
        MSD_Code__c  msd2 = new MSD_Code__c();
        msd2.ownerId = userRec2.Id;
        msd2.Account__c = ParentAcc.Id;
        
        msdcodeObjList.add(msd2);
        
        //system.assert(cont2.Account_Name__r.=True);

    }
    
    static testmethod void testMSDOwner(){
        setUpData(); 
        
        ParentAcc1= new Account(Name='ParentAcc');
        ParentAcc1.Account_Status__c = 'Unassigned';
        ParentAcc1.ParentID= ParentAcc.Id;
        ParentAcc1.OwnerId= userRec.id;
        ParentAcc1.BillingPostalCode = 'A1A 1A1'; 
        insert ParentAcc1;
        
        MSD_Code__c msdnew = new MSD_Code__c();
        msdnew.Account__c = ParentAcc.Id;
        
        MSD_Code__c msdnew1 = new MSD_Code__c();
        msdnew1.Account__c = ParentAcc1.Id;
        
        insert msdnew;
        insert msdnew1;
    }
    
    
    
    
}