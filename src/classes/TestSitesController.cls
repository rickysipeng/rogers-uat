/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestSitesController {

    static Id oppId, sl3Id, sl4Id, sl5Id;
    static Boolean withAssertions = true;
    static Opportunity opp;
    Static Profile pEmp = [Select Id from Profile where name like '%Rogers%' limit 1];
    static user userRec1;

    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        userRec1 = new User(LastName = 'Mark O’BrienRoger',Owner_Type__c = 'District', Alias = 'alRoger1', Email='test1@Rogertest.com', Username='test1@Rogertest.com', CommunityNickname = 'nickRoger2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec1;
        
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.ParentId = null;
        a.account_Status__c = 'Assigned';
        insert a;       
        
        opp = new Opportunity();
        opp.Estimated_MRR__c = 500;
        opp.Name = 'Test Opp';
        opp.StageName = 'Suspect - Qualified';
        opp.Product_Category__c = 'Local';
        opp.Network__c = 'Cable';
        opp.Estimated_One_Time_Charge__c = 500;
        opp.New_Term_Months__c = 5;
        opp.AccountId = a.id;
        opp.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp.CloseDate = date.today();
        opp.OwnerId = userRec1.Id;
        insert opp;
        oppId = opp.id;
        
        CLLIPostalCode__c pcToCLLI1 = new CLLIPostalCode__c(Postal_Code__c = 'A1A1A1', CLLI_Code__c = '123113');
        insert pcToCLLI1;   
        CLLIPostalCode__c pcToCLLI2 = new CLLIPostalCode__c(Postal_Code__c = 'A1A1A2', CLLI_Code__c = '223113');
        insert pcToCLLI2;
        
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '1Somewhere1AveCityON123113';
        sl1.Street_Name__c = 'Somewhere1';
        sl1.Street_Number__c  = '1';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '2Someplace1AveCityON223113';
        sl2.Street_Name__c = 'Someplace1';
        sl2.Street_Number__c  = '2';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '223113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        
        ServiceableLocation__c sl3 = new ServiceableLocation__c();
        sl3.Name = '3Someplace1AveCityON223113';
        sl3.Street_Name__c = 'Someplace1';
        sl3.Street_Number__c  = '3';
        sl3.Street_Type__c  = 'Ave';
        sl3.City__c = 'City';
        sl3.Province_Code__c = 'ON';
        sl3.CLLI_Code__c = '223113';
        sl3.Access_Type_Group__c = 'CNI';
        insert sl3;
        sl3Id = sl3.Id;
        
        ServiceableLocation__c sl4 = new ServiceableLocation__c();
        sl4.Name = 'A4Someplace1AveECityON223113';
        sl4.Street_Name__c = 'Someplace1';
        sl4.Suite_Floor__c  = 'A';
        sl4.Street_Number__c  = '4';
        sl4.Street_Type__c  = 'Ave';
        sl4.Street_Direction__c  = 'E';
        sl4.City__c = 'City';
        sl4.Province_Code__c = 'ON';
        sl4.CLLI_Code__c = '223113';
        sl4.Access_Type_Group__c = 'Fibre';
        sl4.Network_Access_Type__c = 'ROGERS';
        insert sl4;
        sl4Id = sl4.Id;
        
        ServiceableLocation__c sl5 = new ServiceableLocation__c();
        sl5.Name = 'A5Someplace1AveECityON223113';
        sl5.Street_Name__c = 'Someplace1';
        sl5.Suite_Floor__c  = 'A';
        sl5.Street_Number__c  = '5';
        sl5.Street_Type__c  = 'Ave';
        sl5.Street_Direction__c  = 'E';
        sl5.City__c = 'City';
        sl5.Province_Code__c = 'ON';
        sl5.CLLI_Code__c = '223113';
        sl5.Access_Type_Group__c = 'Cable';
        insert sl5;
        sl5Id = sl5.Id;
        
        Site__c s4 = new Site__c();
        s4.Street_Name__c = 'Somewhere1';
        s4.Suite_Floor__c = '11a';
        s4.Street_Number__c  = '4';
        s4.City__c = 'City';
        s4.Province_Code__c = 'ON';
        s4.CLLI_SWC__c = '223113';
        s4.ServiceableLocation__c = sl4Id;
        s4.Opportunity__c = oppId;
        s4.Is_a_Z_Site__c = true; 
        insert s4;
        
        Site__c s5 = new Site__c();
        s5.Street_Name__c = 'Somewhere1';
        s5.Suite_Floor__c = '11a';
        s5.Street_Number__c  = '5';
        s5.City__c = 'City';
        s5.Province_Code__c = 'ON';
        s5.CLLI_SWC__c = '223113';
        s5.ServiceableLocation__c = sl5Id;
        s5.Opportunity__c = oppId; 
        insert s5;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Quote q = new Quote(Name='q1', Term__c='3-Years', OpportunityId=oppId, Pricebook2Id = sp.id, ExpirationDate = Date.today() + 1);
        insert q;      
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = s4.Id;
        qs.Quote__c = q.Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = s5.Id;
        qs.Quote__c = q.Id;
        insert qs;
    }
    
    static testMethod void defaultBlankTest() {
        PageReference pageRef = New PageReference('/apex/addSites?retURL=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        SitesController controller = new SitesController(sc);
        
        List<ServiceableLocation__c> locations1 = [SELECT Id FROM ServiceableLocation__c LIMIT 20];
        List<ServiceableLocation__c> locations2 = [SELECT Id FROM ServiceableLocation__c where CNINNI__c != '' LIMIT 20];
        
        Apexpages.currentPage().getParameters().put('serviceableLocationName', '');
        Apexpages.currentPage().getParameters().put('suite', '');
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('streetType', '');
        Apexpages.currentPage().getParameters().put('streetDirection', '');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('postalCode', '');
        Apexpages.currentPage().getParameters().put('cninni', 'false');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
        controller.runSearch();
        
        System.debug('\n\n\n\n\n\n\n:' + controller.debugSoql);
        
        if (withAssertions){
            System.assertEquals(controller.serviceableLocations.size(), locations1.size());
        //  System.assertEquals(controller.debugSoql, 'SELECT Name, Suite_Floor__c, Street_Number__c, Street_Name__c, Street_Type__c, Street_Direction__c, Province_Code__c, CLLI_Code__c, Postal_Code__c, City__c, Access_Type__c, Access_Type_Group__c, CNINNI__c FROM serviceableLocation__c where CNINNI__C = \'\' order by Name asc limit 20');
        }
        
        ApexPages.currentPage().getParameters().put('cninni', 'true');
        controller.runSearch();
        
        
        if (withAssertions)
            System.assertEquals(controller.serviceableLocations.size(), locations2.size());
       
        
        controller.toggleSort();    
      
      
           
    }
    
    static testMethod void myexactMatchTest() {
        PageReference pageRef = New PageReference('/apex/addSites?retURL=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        SitesController controller = new SitesController(sc);
        
    //    List<ServiceableLocation__c> locations1 = [SELECT Id FROM ServiceableLocation__c where CNINNI__c = '' LIMIT 20];
    //    List<ServiceableLocation__c> locations2 = [SELECT Id FROM ServiceableLocation__c where CNINNI__c != '' LIMIT 20];
                
        Apexpages.currentPage().getParameters().put('serviceableLocationName', 'A4Someplace1AveECityON223113');
        Apexpages.currentPage().getParameters().put('suite', 'A');
        Apexpages.currentPage().getParameters().put('streetNumber', '4');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace1');
        Apexpages.currentPage().getParameters().put('streetType', 'Ave');
        Apexpages.currentPage().getParameters().put('streetDirection', 'E');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('postalCode', 'A1A1A2');
        Apexpages.currentPage().getParameters().put('cninni', 'false');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
        
        controller.runSearch();
        
        System.debug(controller.serviceableLocations);
        
        if (withAssertions){
            boolean found = false;
            for (ServiceableLocationWrapper sl : controller.serviceableLocations){
                if (sl.getServiceableLocation().Id == sl4Id)
                    found = true;
            }
            System.assertEquals(found, true);
        }
    }
    
    static testMethod void miscTest() {
        
        ModifyServiceableLocation controllerA = new ModifyServiceableLocation();
        controllerA.addSite();
        
        ApexPages.StandardController sc1 = new ApexPages.standardController(new ServiceableLocation__c());
        controllerA = new ModifyServiceableLocation(sc1);
        controllerA.addSite();
        
  //    EnterpriseSalesToolBoxController controllerB = new EnterpriseSalesToolBoxController();
  //    String test1 = controllerB.baseURL;
        
        PageReference pageRef = New PageReference('/apex/addSites?edit=1&retURL=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        SitesController controller = new SitesController(sc);
        Apexpages.currentPage().getParameters().put('isNewSite', 'true');
        controller.editServiceableLocation();
        
        Apexpages.currentPage().getParameters().put('isNewSite', 'false');
        
        Apexpages.currentPage().getParameters().put('serviceableLocationName', 'A4Someplace1AveECityON223113');
        Apexpages.currentPage().getParameters().put('suite', 'A');
        Apexpages.currentPage().getParameters().put('streetNumber', '4');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace1');
        Apexpages.currentPage().getParameters().put('streetType', 'Ave');
        Apexpages.currentPage().getParameters().put('streetDirection', 'E');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('postalCode', 'A1A1A2');
        Apexpages.currentPage().getParameters().put('cninni', 'false');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
        
        controller.runSearch();
        
        ServiceableLocationWrapper slw1 = controller.serviceableLocations[0];
        Apexpages.currentPage().getParameters().put('SiteId', slw1.getServiceableLocation().id);
        controller.editServiceableLocation();
        
        Apexpages.currentPage().getParameters().put('SiteId', '');
        Apexpages.currentPage().getParameters().put('isNew', 'false');
        controller.editServiceableLocation();
        
        Apexpages.currentPage().getParameters().put('SiteId', '');
        controller.checkExistingQuote();
                
        controller.addNewSite();
    }
    
     static testMethod void checkDuplicateAccountTest() {
        PageReference pageRef = New PageReference('/apex/addSites?edit=1&retURL=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        SitesController controller = new SitesController(sc);
        
        Apexpages.currentPage().getParameters().put('serviceableLocationName', 'A4Someplace1AveECityON223113');
        Apexpages.currentPage().getParameters().put('suite', 'A');
        Apexpages.currentPage().getParameters().put('streetNumber', '4');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace1');
        Apexpages.currentPage().getParameters().put('streetType', 'Ave');
        Apexpages.currentPage().getParameters().put('streetDirection', 'E');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('postalCode', 'A1A1A2');
        Apexpages.currentPage().getParameters().put('cninni', 'false');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
        
        controller.runSearch();
        
        ServiceableLocationWrapper slw1 = controller.serviceableLocations[0];
        Apexpages.currentPage().getParameters().put('SiteId', slw1.getServiceableLocation().Id);
        Apexpages.currentPage().getParameters().put('accountName', 'Test Account');
        controller.checkExistingQuote();
        
         Apexpages.currentPage().getParameters().put('serviceableLocationName', 'A4Someplace1AveECityON223113');
        Apexpages.currentPage().getParameters().put('suite', 'A');
        Apexpages.currentPage().getParameters().put('streetNumber', '4');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace1');
        Apexpages.currentPage().getParameters().put('streetType', 'Ave');
        Apexpages.currentPage().getParameters().put('streetDirection', 'E');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('postalCode', 'A1A1A2');
        Apexpages.currentPage().getParameters().put('cninni', 'false');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', '');
        
        controller.runSearch();
        
        ServiceableLocationWrapper slw2 = controller.serviceableLocations[0];
        Apexpages.currentPage().getParameters().put('SiteId', slw2.getServiceableLocation().Id);
        Apexpages.currentPage().getParameters().put('accountName', 'Test Act');
        controller.checkExistingQuote();
        
        Apexpages.currentPage().getParameters().put('SiteId', slw2.getServiceableLocation().Id);
        Apexpages.currentPage().getParameters().put('accountName', 'Test Account');
        controller.checkExistingQuote();
        
        Apexpages.currentPage().getParameters().put('SiteId', sl3Id);
        Apexpages.currentPage().getParameters().put('accountName', 'Test Act');
        controller.checkExistingQuote();
        
     }
   
   static testMethod void deleteSelectedSitesTest() {
        PageReference pageRef = New PageReference('/apex/addSites?edit=1&retURL=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        SitesController controller = new SitesController(sc);
        
        Apexpages.currentPage().getParameters().put('serviceableLocationName', 'A4Someplace1AveECityON223113');
        Apexpages.currentPage().getParameters().put('suite', 'A');
        Apexpages.currentPage().getParameters().put('streetNumber', '4');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace1');
        Apexpages.currentPage().getParameters().put('streetType', 'Ave');
        Apexpages.currentPage().getParameters().put('streetDirection', 'E');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('postalCode', 'A1A1A2');
        Apexpages.currentPage().getParameters().put('cninni', 'false');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', 'Fibre');
        Apexpages.currentPage().getParameters().put('networkAccessTypes', 'ROGERS');
        
        controller.runSearch();
        
        ServiceableLocationWrapper slw1 = controller.serviceableLocations[0];
        
        
        List<Id> ids = new List<Id>();
        
        ids.add(slw1.getServiceableLocation().Id);
        
        Apexpages.currentPage().getParameters().put('SiteIds', slw1.getServiceableLocation().Id);
        controller.deleteServiceableLocations();
        
        Integer result = [SELECT count() FROM ServiceableLocation__c WHERE id IN :ids];
        System.assertEquals(result, 0);
        
        
   }
    
   static testMethod void addSelectedSitesTest() {
        PageReference pageRef = New PageReference('/apex/addSites?retURL=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        SitesController controller = new SitesController(sc);
        List<Site__c> sitesBefore = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Someplace1'];
        Integer sitesBeforeSize = 0;
        
        if (sitesBefore!= null)
            sitesBeforeSize = sitesBefore.size();
        
        
        Apexpages.currentPage().getParameters().put('serviceableLocationName', '');
        Apexpages.currentPage().getParameters().put('suite', '');
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace1');
        Apexpages.currentPage().getParameters().put('streetType', '');
        Apexpages.currentPage().getParameters().put('streetDirection', '');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('postalCode', '');
        Apexpages.currentPage().getParameters().put('cninni', 'true');
        Apexpages.currentPage().getParameters().put('accessTypeGroups', '');

        controller.runSearch();
                        
        Integer i = 0, j = 0;
        for (ServiceableLocationWrapper slw : controller.serviceableLocations){
                slw.setIsSelected(true);
                j++;
        }
                
        controller.addSites();
        List<Site__c> sitesAfter = [Select Id, Name, Street_Name__c FROM Site__c where Street_Name__c = 'Someplace1'];
        
        System.assertEquals(sitesAfter.size() - sitesBeforeSize, j);
        
        controller.getAccessTypeGroups();
        controller.getNetworkAccessTypes();
        String debug = controller.debugSoql;
        controller.returnToOpportunity();
        
    }
    
}