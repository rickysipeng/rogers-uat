/*
===============================================================================
 Class Name   : Test_TAG_ApproverViewController
===============================================================================
PURPOSE:    This class a controller class for TAG_ApproverViewController.
              
Developer: Deepika Rawat
Date: 02/05/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
02/05/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_ApproverViewController{
     private static Assignment_Request__c assignReq = new Assignment_Request__c();
     private static Account ParentAcc;
     private static Account ChildAcc1;
     private static List<Assignment_Request_Item__c> reqItems = new List<Assignment_Request_Item__c>();
     private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
     private static User userRec;
     private static User userRec2;
     private static User userRec3;
     private static Assignment_Request_Item__c reqItem;
     private static Assignment_Request_Item__c reqItem2;
     private static Assignment_Request_Item__c reqItem3;
     private static Assignment_Request_Item__c reqItem4;
     private static List<AccountTeamMember> parentAccTeam;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
     private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.TAG_Ownership_Requests__c = 'Account Owner,MSD Owner,Shared MSD Owner,Account District,MSD District,Shared MSD District';
        TAG_CS.TAG_Membership_Requests__c = 'Account Team,MSD Member,Shared MSD Member';
        insert TAG_CS;
        staticData_PageSize.Name = 'ApproverView_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='AccountTransfer_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;

        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment');
        insert userRec2;

        userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Large');
        insert userRec3;

       System.RunAs(userRec2)
       {
            //Account 1
            ParentAcc= new Account();
            ParentAcc.Name = 'ParentAcc';
            ParentAcc.Account_Status__c = 'Assigned';
            ParentAcc.ParentID= null;
            ParentAcc.OwnerId= userRec2.id;
            ParentAcc.BillingPostalCode= 'A1A 1A1';
            insert ParentAcc;

            //Create Assignment request
            assignReq = new Assignment_Request__c();
            assignReq.Request_Type__c = 'Mixed';
            assignReq.Status__c = 'Processed';
            assignReq.Approval_Override__c=false;
            insert assignReq;

            reqItem = new Assignment_Request_Item__c();
            reqItem.Assignment_Request__c = assignReq.id;
            reqItem.Account__c = ParentAcc.id;
            reqItem.Approval_Override__c = assignReq.Approval_Override__c;
            reqItem.Business_Case__c = 'Test Business Case';
            reqItem.Current_Approver__c =userRec.Id;
            reqItem.Current_Channel__c = 'Large';
            reqItem.Current_Owner__c= userRec2.id;       
            reqItem.Reason_Code__c = '1 - Channel Alignment to Small';
            reqItem.New_Owner__c= userRec3.id; 
            reqItem.New_Approver__c=userRec2.Id;
            reqItem.New_Channel__c= 'BIS';
            reqItem.Requested_Item_Type__c='Account Owner';
            reqItems.add(reqItem);
            
            reqItem3 = new Assignment_Request_Item__c();
            reqItem3.Assignment_Request__c = assignReq.id;
            reqItem3.Account__c = ParentAcc.id;
            reqItem3.Business_Case__c = 'Test 2 Business Case';
            reqItem3.Current_Approver__c =userRec.Id;
            reqItem3.Current_Channel__c = 'Large';
            reqItem3.Current_Owner__c= userRec2.id;       
            reqItem3.Reason_Code__c = '1 - Channel Alignment to Small';
            reqItem3.New_Owner__c= userRec3.id; 
            reqItem3.New_Approver__c=userRec2.Id;
            reqItem3.New_Channel__c= 'BIS';
            reqItem3.Requested_Item_Type__c='Account Owner';
            reqItems.add(reqItem3);

            reqItem2 = new Assignment_Request_Item__c();
            reqItem2.Assignment_Request__c = assignReq.id;
            reqItem2.Account__c = ParentAcc.id;
            reqItem2.Approval_Override__c = assignReq.Approval_Override__c;
            reqItem2.Business_Case__c = 'Test 2 Business Case';
            reqItem2.Reason_Code__c = 'Test 2 Reson code';
            reqItem2.Action__c = 'Remove';
            reqItem2.Requested_Item_Type__c='Account Team';
            reqItem2.Role__C = 'Owner';
            reqItem2.Member__c = userRec2.id;
            reqItem2.Member_Approver__c = userRec2.Assignment_Approver__r.Id;
            reqItem2.Member_Channel__c = userRec2.Channel__c;
            reqItem2.Current_Approver__c=userRec3.Assignment_Approver__r.Id;     
            reqItem2.Current_Channel__c=  userRec3.Channel__c;
            reqItem2.Current_Owner__c= userRec3.id;  
            reqItem2.Status__c='Submitted';
            reqItem2.Approval_Override__c =false;
            reqItems.add(reqItem2);     

            reqItem4 = new Assignment_Request_Item__c();
            reqItem4.Assignment_Request__c = assignReq.id;
            reqItem4.Account__c = ParentAcc.id;
            reqItem4.Approval_Override__c = assignReq.Approval_Override__c;
            reqItem4.Business_Case__c = 'Test 2 Business Case';
            reqItem4.Reason_Code__c = 'Test 2 Reson code';
            reqItem4.Action__c = 'Remove';
            reqItem4.Requested_Item_Type__c='Account Team';
            reqItem4.Role__C = 'Owner';
            reqItem4.Member__c = userRec2.id;
            reqItem4.Member_Approver__c = userRec2.Assignment_Approver__r.Id;
            reqItem4.Member_Channel__c = userRec2.Channel__c;
            reqItem4.Current_Approver__c=userRec3.Assignment_Approver__r.Id;     
            reqItem4.Current_Channel__c=  userRec3.Channel__c;
            reqItem4.Current_Owner__c= userRec3.id; 
            reqItem4.Status__c='Submitted';
            reqItem4.Approval_Override__c =false;
            reqItems.add(reqItem4);
            insert reqItems;
            
            Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
            req1.setComments('Submitting request for approval.');
            req1.setObjectId(reqItems[0].id);
            Approval.ProcessResult result = Approval.process(req1);

            Approval.ProcessSubmitRequest req2 = new Approval.ProcessSubmitRequest();
            req2.setComments('Submitting request for approval.');
            req2.setObjectId(reqItems[1].id);
            Approval.ProcessResult result2 = Approval.process(req2);

            Approval.ProcessSubmitRequest req3 = new Approval.ProcessSubmitRequest();
            req3.setComments('Submitting request for approval.');
            req3.setObjectId(reqItems[2].id);
            Approval.ProcessResult result3 = Approval.process(req3);

            Approval.ProcessSubmitRequest req4 = new Approval.ProcessSubmitRequest();
            req4.setComments('Submitting request for approval.');
            req4.setObjectId(reqItems[3].id);
            Approval.ProcessResult result4 = Approval.process(req4);
        }
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Request Type.
    *******************************************************************************************************/
    static testmethod void testSearchPendingReqItems(){
        setUpData(); 
        test.startTest();
           TAG_ApproverViewController obj = new TAG_ApproverViewController();
           obj.searchRequestType='Account Owner';
           obj.createOwnerRequests();
           obj.searchRequestTypeMem='Account Team';
           obj.createMemberRequests();
           system.assert(obj.listOwnershipRequests.size()>0);
           obj.toggleSort();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as invalid data for Owner request.
    *******************************************************************************************************/
    static testmethod void testSearchOwnerReqItemsNegative(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            obj.searchSubmittedDate='3/9/2015';
            obj.searchSubmittedBy='Rog';
            obj.searchRequestType='Account Owner';
            obj.searchName='Acc';
            obj.searchDUNS='12345';
            obj.searchMSDCode='23456';
            obj.searchCurrentOwner='Rog';
            obj.searchNewOwner='User2';
            obj.searchNewDistrict='District test';
            obj.searchReasonCode='Reason Test';
            obj.searchBusinessCase='Business Case Test';
            obj.createOwnerRequests();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as invalid data for Member request.
    *******************************************************************************************************/
    static testmethod void testSearchMemberReqItemsNegative(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            obj.searchSubmittedDateMem='3/9/2015';
            obj.searchSubmittedByMem ='Test';
            obj.searchRequestTypeMem='Account Team';
            obj.searchNameMem='Acc';
            obj.searchDUNSMem='12345';
            obj.searchMSDCodeMem='23456';
            obj.searchAction='Remove';  
            obj.searchMember='Test Member';
            obj.searchRole='SDC';
            obj.searchGeoType='Test Geo';
            obj.searchReasonCodeMem='Reason Test';
            obj.searchBusinessCaseMem='Business Case Test';
            obj.createMemberRequests();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Member request.
                    Approve the selected Request Item
    *******************************************************************************************************/
    static testmethod void testSearchMemberReqItemsApprove(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listMembershipRequests.size()>0);
            obj.searchRequestTypeMem='Account Team';
            obj.createMemberRequests();
            obj.lstSetController2[0].isSelected=true;
            obj.dolstSelectedMemberss();
            obj.displayMemberApprovePopUp();
            obj.closeMemberApprovePopUp();
            obj.displayMemberApprovePopUp();
            obj.memberApproveComments='Approved test Class';
            obj.approveMembershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Member request.
                    Approve All Request Item
    *******************************************************************************************************/
    static testmethod void testSearchMemberReqItemsApproveAll(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listMembershipRequests.size()>0);
            obj.searchRequestTypeMem='Account Team';
            obj.createMemberRequests();
            obj.lstSetController2[0].isSelected=true;
            obj.dolstSelectedMemberss();
            obj.displayMemberApproveAllPopUp();
            obj.memberApproveComments='Approved test Class';
            obj.approveMembershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Member request.
                    Reject the selected Request Item
    *******************************************************************************************************/
    static testmethod void testSearchMemberReqItemsReject(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listMembershipRequests.size()>0);
            obj.searchRequestTypeMem='Account Team';
            obj.createMemberRequests();
            obj.lstSetController2[0].isSelected=true;
            obj.dolstSelectedMemberss();
            obj.displayMemberRejectPopUp();
            obj.closeMemberRejectPopUp();
            obj.displayMemberRejectPopUp();
            obj.memberRejectComments ='Rejected Test Class';
            obj.rejectMembershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Member request.
                    Reject All Request Item
    *******************************************************************************************************/
    static testmethod void testSearchMemberReqItemsRejectAll(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listMembershipRequests.size()>0);
            obj.searchRequestTypeMem='Account Team';
            obj.createMemberRequests();
            obj.lstSetController2[0].isSelected=true;
            obj.dolstSelectedMemberss();
            obj.displayMemberRejectAllPopUp();
            obj.memberRejectComments ='Rejected Test Class';
            obj.rejectMembershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as valid data for Member request.
                    Reject/Approve without selecting any record.
    *******************************************************************************************************/
    static testmethod void testSearchMemberReqItemsNegative2(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listMembershipRequests.size()>0);
            obj.searchRequestTypeMem='Account Team';
            obj.createMemberRequests();
            obj.displayMemberRejectPopUp();
            obj.displayMemberApprovePopUp();
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Owner request.
                    Approve the selected Request Item
    *******************************************************************************************************/
    static testmethod void testSearchOwnerReqItemsApprove(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listOwnershipRequests.size()>0);
            obj.lstSetController[0].isSelected=true;
            obj.dolstSelectedOwnerss();
            obj.displayOwnerApprovePopUp();
            obj.closeOwnerApprovePopUp();
            obj.displayOwnerApprovePopUp();
            obj.ownerApproveComments='Approved test Class';
            obj.approveOwnershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Owner request.
                    Approve All Request Item
    *******************************************************************************************************/
    static testmethod void testSearchOwnerReqItemsApproveAll(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listOwnershipRequests.size()>0);
            obj.dolstSelectedOwnerss();
            obj.displayOwnerApproveAllPopUp();
            obj.ownerApproveComments='Approved test Class';
            obj.approveOwnershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Owner request.
                    Reject the selected Request Item
    *******************************************************************************************************/
    static testmethod void testSearchOwnerReqItemsReject(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listOwnershipRequests.size()>0);
            obj.lstSetController[0].isSelected=true;
            obj.dolstSelectedOwnerss();
            obj.displayOwnerRejectPopUp();
            obj.closeOwnerRejectPopUp();
            obj.displayOwnerRejectPopUp();
            obj.ownerRejectComments='Rejected test Class';
            obj.rejectOwnershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as valid data for Owner request.
                    Reject All Request Item
    *******************************************************************************************************/
    static testmethod void testSearchOwnerReqItemsRejectAll(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listOwnershipRequests.size()>0);
            obj.dolstSelectedOwnerss();
            obj.displayOwnerRejectAllPopUp();
            obj.ownerRejectComments='Rejected test Class';
            obj.rejectOwnershipItems();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as valid data for Owner request.
                    Reject/Approve without selecting any record.
    *******************************************************************************************************/
    static testmethod void testSearchOwnerReqItemsNegative2(){
        setUpData(); 
        test.startTest();
            TAG_ApproverViewController obj = new TAG_ApproverViewController();
            system.assert(obj.listOwnershipRequests.size()>0);
            obj.displayOwnerRejectPopUp();
            obj.displayOwnerApprovePopUp();
        test.stopTest(); 
    }
}