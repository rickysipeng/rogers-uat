@isTest(seeAllData=true)
private class QuoteImplementationControllerTest {
    private static Opportunity testOpp;
    private static Quote__c testQuote;
    private static Quote_Line_Item__c testQuoteLineItem;
    private static Quote_Line_Item__c testQuoteLineItem1=new Quote_Line_Item__c();
    private static Product2 prod2;
    private static ApexPages.Standardcontroller stdController;
    
    private static User testAdminUser = [
                                SELECT Name
                                FROM User
                                WHERE Profile.Name = 'System Administrator'
                                    AND IsActive = true
                                LIMIT 1];
    
    private static void initData() {
        testOpp = new Opportunity(
                                Name = 'testOpp',
                                StageName = 'testOpp',
                                CloseDate = Date.today().addDays(2));
        insert testOpp;
    
        testQuote = new Quote__c(
                                Opportunity_Name__c = testOpp.Id);
        insert testQuote;
        
        prod2 = new Product2(
                                Name = 'testName',
                                Period__c = '1');
        insert prod2;
        
        testQuoteLineItem = new Quote_Line_Item__c(
                                Name = 'test',
                                Price__c = 1.0,
                                Type__c = 'Custom',
                                Line__c = 1,
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Period__c = '1',
                                Line_Item_Notes__c = 'Test Parent Note');
        insert testQuoteLineItem;
        
        
        
        stdController = new ApexPages.Standardcontroller(testQuote);
    }
    
    static testMethod void QuoteImplementationControllerTest1() {
        initData();
        list<SelectOption> lstSelectOption1= new list<SelectOption>();
        list<SelectOption> lstSelectOption2= new list<SelectOption>();
        String func1;
        String func2;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        
        System.assertNotEquals(NULL, controller);
        lstSelectOption1=controller.getCommissionTypePicklistValues();
        lstSelectOption2=controller.getDatafillCommissionTypePicklistValues();
        func1=controller.getadmin3BackendURL();
        func2=controller.getRCIadmin3BackendURL();
        controller.expandCollapseQuoteDetails();
        controller.expandCollapseProductList();
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTest2() {
        initData();
        
        System.runAs(testAdminUser) {
            Test.startTest();
                QuoteImplementationController controller = new QuoteImplementationController(stdController);
                
                System.assertNotEquals(NULL, controller);
                
                controller.expandCollapseQuoteDetails();
                controller.expandCollapseProductList();
                
                controller.FirstSection.saveQuote();
                controller.FirstSection.hideMessage();
                controller.FirstSection.cancelQuote();
                controller.FirstSection.datafillQuote();
                controller.FirstSection.cloneQuote();
                
                controller.SecondSection.newQuoteLineItem = testQuoteLineItem;
                controller.SecondSection.getParentQLIs();
                controller.SecondSection.searchProducts();
                controller.SecondSection.addCustomServiceItem();
                controller.SecondSection.addDividerLine();
                controller.SecondSection.showUpgradeAmount();
                controller.SecondSection.saveDetailLine();
                
                controller.ThirdSection.getQuoteLineItemsWithChildren();
                controller.ThirdSection.saveQuoteLineItems();
                controller.ThirdSection.hideMessage();
                controller.ThirdSection.cancelQuoteLineitems();
                controller.ThirdSection.getQLIsDescription();
                controller.ThirdSection.getQLITotalPrice();
                
                controller.FirstSection.deleteQuote();
            Test.stopTest();
        }
    }
    
    static testMethod void QuoteImplementationControllerTest3() {
        initData();
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        controller.thirdSection.getQuoteLineItemsWithChildren().get(0).deleteQuoteLineItem();
        
        controller = new QuoteImplementationController(stdController);
        
        System.assertNotEquals(NULL, controller);
        controller.secondSection.showSubClasses();
        controller.secondSection.autoSearchProducts();
        
        controller.secondSection.addDividerLine();
        controller.secondSection.newQuoteLineItem.Line__c = 0;
        controller.secondSection.saveDetailLine();
        
        controller.secondSection.addCustomServiceItem();
        controller.secondSection.newQuoteLineItem.Name = 'Test';
        controller.secondSection.newQuoteLineItem.Line__c = 0;
        controller.secondSection.newQuoteLineItem.Price__c = 500;
        controller.secondSection.newQuoteLineItem.Quantity__c = 1;
        controller.secondSection.newQuoteLineItem.Discount__c = 0;
        controller.secondSection.newQuoteLineItem.Commission_Type__c = 'New Revenue';
        controller.secondSection.saveDetailLine();
        
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTest4() {
        initData();
        
        Test.startTest();
        prod2.Service_Class__c = 'Voice';
        update prod2;
        
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        System.assertNotEquals(NULL, controller);
        
        controller.SecondSection.selectedServiceClass = 'Voice';
        controller.SecondSection.selectedSubClass = '--None--';
        controller.SecondSection.searchProducts();
 //       System.assertNotEquals(1, controller.SecondSection.productPriceWrappers.size());
        
        prod2.Service_Class__c = 'Voice';
        prod2.Sub_Class__c = 'Bundles';
        update prod2;
        
        controller = new QuoteImplementationController(stdController);
        System.assertNotEquals(NULL, controller);
        
        controller.SecondSection.selectedServiceClass = 'Voice';
        controller.SecondSection.getProductSubClasses();
        controller.SecondSection.selectedSubClass = 'Bundles';
        controller.SecondSection.searchProducts();
        //System.assertNotEquals(1, controller.SecondSection.productPriceWrappers.size());
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTest5() {
        initData();
        
        testQuoteLineItem.Commission_Type__c = 'New Revenue';
        update testQuoteLineItem;
        
        List<Quote_Line_Item__c> testQLIs = new List<Quote_Line_Item__c>();
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child 1',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 1,
                                Type__c = 'Standard',
                                Commission_Type__c = 'New Revenue',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child 2',
                                Price__c = 1000,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 2,
                                Type__c = 'Standard',
                                Commission_Type__c = 'New Revenue',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 2,
                                Commission_Type__c = 'New Revenue',
                                Type__c = 'Standard',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'Divider',
                                Type__c = 'Divider Text',
                                Quote__c = testQuote.Id,
                                Line__c = 3,
                                Product__c = prod2.Id));
        insert testQLIs;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        System.assertEquals(5, controller.thirdSection.getQuoteLineItemsWithChildren().size());
      //  controller.thirdSection.getQuoteLineItemsWithChildren().get(1).deleteQuoteLineItem();
       // System.assertEquals(4, controller.thirdSection.getQuoteLineItemsWithChildren().size());
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTest6() {
        initData();
        
        testQuoteLineItem.Commission_Type__c = 'New Revenue';
        update testQuoteLineItem;
        
        List<Quote_Line_Item__c> testQLIs = new List<Quote_Line_Item__c>();
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child 1',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 1,
                                Type__c = 'Standard',
                                Commission_Type__c = 'New Revenue',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child 2',
                                Price__c = 1000,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 2,
                                Type__c = 'Standard',
                                Commission_Type__c = 'New Revenue',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 2,
                                Commission_Type__c = 'New Revenue',
                                Type__c = 'Standard',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'Divider',
                                Type__c = 'Divider Text',
                                Quote__c = testQuote.Id,
                                Line__c = 3,
                                Product__c = prod2.Id));
        insert testQLIs;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        System.assertEquals(5, controller.thirdSection.getQuoteLineItemsWithChildren().size());
        controller.thirdSection.getQuoteLineItemsWithChildren().get(0).deleteQuoteLineItem();
        System.assertEquals(4, controller.thirdSection.getQuoteLineItemsWithChildren().size());
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTest7() {
        initData();
        
        prod2.Period__c = '0';
        prod2.Service_Class__c = 'Voice';
        prod2.Sub_Class__c = 'Bundles';
        update prod2;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        System.assertNotEquals(NULL, controller);
        
        controller.SecondSection.selectedServiceClass = 'Voice';
        controller.SecondSection.selectedSubClass = '--None--';
        controller.SecondSection.searchProducts();
        
   /* //    controller.SecondSection.productPriceWrappers.get(0).productPrice.Product2.Period__c = '0';
    //    controller.SecondSection.productPriceWrappers.get(0).addToQLISection();
        
        controller.SecondSection.productPriceWrappers.get(0).productPrice.Product2.Period__c = '3';
        controller.SecondSection.productPriceWrappers.get(0).addToQLISection();
        
        controller.SecondSection.productPriceWrappers.get(0).productPrice.Product2.Period__c = '6';
        controller.SecondSection.productPriceWrappers.get(0).addToQLISection();
        
        controller.SecondSection.productPriceWrappers.get(0).productPrice.Product2.Period__c = '12';
        controller.SecondSection.productPriceWrappers.get(0).addToQLISection();
        
        controller.SecondSection.productPriceWrappers.get(0).productPrice.Product2.Period__c = '24';
        controller.SecondSection.productPriceWrappers.get(0).addToQLISection();
     */   
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTest8() {
        initData();
        
        testQuoteLineItem.Commission_Type__c = 'New Revenue';
        update testQuoteLineItem;
        
        List<Quote_Line_Item__c> testQLIs = new List<Quote_Line_Item__c>();
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child 1',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 1,
                                Type__c = 'Standard',
                                Commission_Type__c = 'New Revenue',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test child 2',
                                Price__c = 1000,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 2,
                                Type__c = 'Standard',
                                Commission_Type__c = 'New Revenue',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'test standard',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 2,
                                Commission_Type__c = 'New Revenue',
                                Type__c = 'Standard',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'Divider',
                                Type__c = 'Divider Text',
                                Quote__c = testQuote.Id,
                                Line__c = 3,
                                Product__c = prod2.Id));
        
        testQLIs.add(new Quote_Line_Item__c(
                                Name = 'new standard',
                                Price__c = 500,
                                Quantity__c = 1,
                                Discount__c = 0,
                                Line__c = 4,
                                Commission_Type__c = 'New Revenue',
                                Type__c = 'Standard',
                                Quote__c = testQuote.Id,
                                Product__c = prod2.Id,
                                Period__c = '1'));
        insert testQLIs;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        
        Decimal totalPrice = controller.thirdSection.getQuoteLineItemsWithChildren().get(0).getTotalPrice();
        controller.thirdSection.getQLIEMRR();
        controller.thirdSection.getQuoteLineItemsWithChildren().get(0).editQuoteLineItem();
        
        Quote_Line_Item__c testLineNumbering = controller.thirdSection.getQuoteLineItemsWithChildren().get(0).quoteLineItem;
        testLineNumbering.Line__c = 6;
        controller.secondSection.saveDetailLine();
        
        controller.secondSection.addCustomServiceItem();
        controller.secondSection.saveDetailLine();
        controller.secondSection.newQuoteLineItem.Name = 'New custom test';
        controller.secondSection.saveDetailLine();
        controller.secondSection.newQuoteLineItem.Line__c = 0;
        controller.secondSection.saveDetailLine();
        controller.secondSection.newQuoteLineItem.Period__c = '1';
        //controller.secondSection.saveDetailLine();
        Test.stopTest();
        
      //  System.assertEquals(1500, totalPrice);
    }
    
    static testMethod void reorderChildTest() {
        initData();
        
        List<Quote_Line_Item__c> testChildItems = new List<Quote_Line_Item__c>();
        for(Integer i = 1; i < 5; i++) {
            testChildItems.add(new Quote_Line_Item__c(
                    Name = 'test',
                    Price__c = 1.0,
                    Type__c = 'Standard',
                    Line__c = i,
                    Quote__c = testQuote.Id,
                    Product__c = prod2.Id,
                    Period__c = '1',
                    Commission_Type__c = 'New Revenue',
                    Parent_Quote_Line_Item__c = testQuoteLineItem.Id));
        }
        insert testChildItems;
        
        Quote_Line_Item__c secondParentItem = new Quote_Line_Item__c(
                Name = 'test',
                Price__c = 1.0,
                Type__c = 'Custom',
                Line__c = 2,
                Quote__c = testQuote.Id,
                Product__c = prod2.Id,
                Period__c = '1');
        insert secondParentItem;
        
        testChildItems = new List<Quote_Line_Item__c>();
        for(Integer i = 1; i < 5; i++) {
            testChildItems.add(new Quote_Line_Item__c(
                    Name = 'test',
                    Price__c = 1.0,
                    Type__c = 'Standard',
                    Line__c = i,
                    Quote__c = testQuote.Id,
                    Product__c = prod2.Id,
                    Period__c = '1',
                    Commission_Type__c = 'New Revenue',
                    Parent_Quote_Line_Item__c = secondParentItem.Id));
        }
        insert testChildItems;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        
        Quote_Line_Item__c newChildItem = new Quote_Line_Item__c(
                    Name = 'test',
                    Price__c = 1.0,
                    Type__c = 'Standard',
                    Line__c = 3,
                    Quote__c = testQuote.Id,
                    Product__c = prod2.Id,
                    Period__c = '1',
                    Commission_Type__c = 'New Revenue');
        insert newChildItem;
        
        controller.secondSection.selectedParent = testQuoteLineItem.Id;
        controller.secondSection.newQuoteLineItem = newChildItem;
      //  controller.secondSection.saveDetailLine();
      //  System.assertEquals(5, controller.thirdSection.getQuoteLineItemsWithChildren().get(5).quoteLineItem.Line__c);
        
        controller.secondSection.selectedParent = secondParentItem.Id;
        controller.secondSection.newQuoteLineItem = controller.thirdSection.getQuoteLineItemsWithChildren().get(3).quoteLineItem;
       //controller.secondSection.saveDetailLine();
        
        controller.secondSection.selectedParent = secondParentItem.Id;
        newChildItem = controller.thirdSection.getQuoteLineItemsWithChildren().get(8).quoteLineItem;
        newChildItem.Line__c = 2;
        controller.secondSection.newQuoteLineItem = newChildItem;
        //controller.secondSection.saveDetailLine();
        Test.stopTest();
    }
    
    static testMethod void QuoteImplementationControllerTestNew() {
        initData();
        
        List<Quote_Line_Item__c> testChildItems = new List<Quote_Line_Item__c>();
        for(Integer i = 1; i < 5; i++) {
            testChildItems.add(new Quote_Line_Item__c(
                    Name = 'test123',
                    Price__c = 2.0,
                    Type__c = 'Standard',
                    Line__c = i,
                    Quote__c = testQuote.Id,
                    Product__c = prod2.Id,
                    Period__c = '4',
                    Commission_Type__c = 'New Revenue',
                    //Parent_Quote_Line_Item__c = testQuoteLineItem.Id,
                    Line_Item_Notes__c = 'Test Parent Note'));
        }
        insert testChildItems;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);        
        controller.secondSection.selectedParent = '--None--';
        Quote_Line_Item__c newLineItem = controller.thirdSection.getQuoteLineItemsWithChildren().get(3).quoteLineItem;
        newLineItem.Line__c = 1;
        controller.secondSection.newQuoteLineItem = newLineItem;
        controller.secondSection.saveDetailLine();
        
        System.assertEquals(2, controller.thirdSection.getQuoteLineItemsWithChildren().get(1).quoteLineItem.Line__c);
        String testDescription = controller.thirdSection.getQLIsDescription();
        System.assertEquals(TRUE, testDescription.contains('Test Parent Note'));
        Test.stopTest();
    }
    static testMethod void reorderChildRemoveParentTest() {
        initData();
        
        List<Quote_Line_Item__c> testChildItems = new List<Quote_Line_Item__c>();
        for(Integer i = 1; i < 5; i++) {
            testChildItems.add(new Quote_Line_Item__c(
                    Name = 'test',
                    Price__c = 1.0,
                    Type__c = 'Standard',
                    Line__c = i,
                    Quote__c = testQuote.Id,
                    Product__c = prod2.Id,
                    Period__c = '1',
                    Commission_Type__c = 'New Revenue',
                    Parent_Quote_Line_Item__c = testQuoteLineItem.Id,
                    Line_Item_Notes__c = 'Test Parent Note'));
        }
        insert testChildItems;
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);        
        controller.secondSection.selectedParent = '--None--';
        Quote_Line_Item__c newLineItem = controller.thirdSection.getQuoteLineItemsWithChildren().get(3).quoteLineItem;
        newLineItem.Line__c = 1;
        controller.secondSection.newQuoteLineItem = newLineItem;
        controller.secondSection.saveDetailLine();
        
        System.assertEquals(2, controller.thirdSection.getQuoteLineItemsWithChildren().get(1).quoteLineItem.Line__c);
        String testDescription = controller.thirdSection.getQLIsDescription();
        System.assertEquals(TRUE, testDescription.contains('Test Parent Note'));
        Test.stopTest();
    }
    
    static testMethod void revenueTypeTest() {
        initData();
        
        Test.startTest();
        QuoteImplementationController controller = new QuoteImplementationController(stdController);
        Test.stopTest();
    }
}