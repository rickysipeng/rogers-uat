/*
===============================================================================
Class Name : msdCodeSharing_schedular_Test
===============================================================================
PURPOSE: This is a test class for msdCodeSharing_schedular class

COMMENTS: 

Developer: JAcob Klay
Date: 10/10/2014


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
===============================================================================

*/
@isTest(SeeAllData = true)
private class msdCodeSharing_schedular_Test{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        msdCodeSharing_schedular obj= new msdCodeSharing_schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('msdCodeSharing', sch, obj);
        System.assert(true);
        Test.stopTest();    
    }
}