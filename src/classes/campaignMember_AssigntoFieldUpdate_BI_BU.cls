/*Class Name  :campaignMember_AssigntoFieldUpdate_BI_BU.
 *Description : This Class will support the logic for CampaignMember Trigger before Insert.
 *Created By  : Rajiv Gangra.
 *Created Date :29/08/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class campaignMember_AssigntoFieldUpdate_BI_BU {

    /*@ Method Name: updateAssignTo
      Description: This Method gets the list of cases and Assign the Assign to Field on the same Accordingly .
      Return Type: NA.
      Input Param: NA.   
    */
    public void updateAssignTo(LIST<CampaignMember> cList,String type) {
        list<Id> conIds= new list<Id>();
        list<Id> leadIds= new list<Id>();
        list<String> userIds= new list<String>();
        for( CampaignMember cm : cList){
            if(cm.Uploaded_Assigned_To_ID__c !=null && cm.Uploaded_Assigned_To_ID__c !=''){
                userIds.add(cm.Uploaded_Assigned_To_ID__c);
            }else if(cm.ContactID !=null){
                conIds.add(cm.ContactID);
            }else if(cm.leadID !=null){
                leadIds.add(cm.leadID);
            }
        }
        List<User> lstUser=[select id from User where ID IN:userIds];
        Map<Id,Id> UserIdMap= new Map<Id,Id>();
        for(User u:lstUser){
            UserIdMap.put(u.id,u.id);
        }
        
        List<Contact> lstCon=[select id,OwnerID,Account.ownerID from Contact where ID IN:conIds];
        Map<Id,Id> conIdOwnerMap= new Map<Id,Id>();
        for(Contact c:lstCon){
            conIdOwnerMap.put(c.id,c.Account.ownerID);
        }
        
        List<lead> lstLead=[select id,OwnerID from Lead where ID IN:leadIds];
        Map<Id,Id> leadIdOwnerMap= new Map<Id,Id>();
        for(lead l:lstLead){
            leadIdOwnerMap.put(l.id,l.OwnerID);
        }
        
        for( CampaignMember cm : cList){
            
            if(cm.Assigned_To__c ==null){
                if(cm.Uploaded_Assigned_To_ID__c !=null && cm.Uploaded_Assigned_To_ID__c !=''){
                    try{
                        cm.Uploaded_Assigned_To_ID__c=UserIdMap.get(cm.Uploaded_Assigned_To_ID__c);
                        cm.Assigned_To__c=cm.Uploaded_Assigned_To_ID__c;
                    }catch(exception e){
                        cm.Uploaded_ID_Error__c=true;
                    }
                    if(cm.Uploaded_Assigned_To_ID__c ==null && cm.Uploaded_Assigned_To_ID__c !=''){
                        cm.Uploaded_ID_Error__c=true;
                    }
                }else if(cm.contactID !=null){
                      cm.Uploaded_Assigned_To_ID__c=conIdOwnerMap.get(cm.contactID);
                      cm.Assigned_To__c=cm.Uploaded_Assigned_To_ID__c;
                }else if(cm.leadID !=null){
                      cm.Uploaded_Assigned_To_ID__c=leadIdOwnerMap.get(cm.leadID);
                      cm.Assigned_To__c=cm.Uploaded_Assigned_To_ID__c;
                }
            }else{
                cm.Uploaded_Assigned_To_ID__c=cm.Assigned_To__c;
            }/*else if(cm.Assigned_To__c ==null && type=='BU'){
                cm.Uploaded_Assigned_To_ID__c=cm.Assigned_To__c;
                cm.Uploaded_ID_Error__c=False;
            }else if(cm.Assigned_To__c !=null){
                cm.Uploaded_Assigned_To_ID__c=cm.Assigned_To__c;
                cm.Uploaded_ID_Error__c=False;
            }
            if((cm.Uploaded_Assigned_To_ID__c ==null || cm.Uploaded_Assigned_To_ID__c =='')&& (cm.Assigned_To__c==null || cm.Assigned_To__c=='')){
                if(cm.contactID !=null){
                      cm.Uploaded_Assigned_To_ID__c=conIdOwnerMap.get(cm.contactID);
                      cm.Assigned_To__c=cm.Uploaded_Assigned_To_ID__c;
                }else if(cm.leadID !=null){
                      cm.Uploaded_Assigned_To_ID__c=leadIdOwnerMap.get(cm.leadID);
                      cm.Assigned_To__c=cm.Uploaded_Assigned_To_ID__c;
                }
                cm.Uploaded_ID_Error__c=False;
            }*/
        }
     }

}