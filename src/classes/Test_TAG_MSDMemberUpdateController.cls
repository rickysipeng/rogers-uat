/*
===============================================================================
 Class Name   : Test_TAG_MSDMemberUpdateController
===============================================================================
PURPOSE:    This class a controller class for TAG_MSDMemberUpdate Page. 
              
Developer: Jayavardhan
Date: 03/09/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/09/2015           Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_MSDMemberUpdateController{
    private static Assignment_Request__c assignReq = new Assignment_Request__c();
    private static Assignment_Request_Item__c reqItem;
     private static Account ParentAcc;
     private static Account ParentAcc2;  
     private static Account ChildAcc1;
     private static MSD_Code__c ParentMSD1;
     private static MSD_Code__c ParentMSD2;
     private static MSD_Code__c ParentMSD3;     
     private static List<MSD_Member__c> MsdMemberList;
     private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
     private static User userRec;
     private static User userRec2;
     private static User userRec3;
     private static User userRec4;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
     private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';      
        TAG_CS.User_Location__c = 'Central,Eastern,National,Western';
        TAG_CS.User_Region__c = 'Alberta,Atlantic,British Columbia,National,Ontario,Prairies,Quebec';
        TAG_CS.User_Channel_1__c = 'ABS,BIS,Business Customer Enablement,Business Customer Loyalty,Business Customer Relations,Business National Support,Business Segment,Commercial Service Delivery,Dealer,Enterprise,Enterprise Service Assurance';
        TAG_CS.User_Channel_2__c = 'Enterprise,Service Delivery,Federal,Field Sales Cable,Large,M2M Reseller,Medium,MSA,NIS,Public Sector,RBS,RCI,Sales Operations,Small,VAR';
        TAG_CS.MSD_SharedMSD_Roles_Available__c = 'Account Development Rep,Account Development Rep - Dealer';
        insert TAG_CS;
        staticData_PageSize.Name = 'MemberUpdate_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='MemberUpdate_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;

       
        User testu = new User(LastName = 'Mark O’Roger1', Alias = 'er112', Email='test@Rogertest.com', Username='test@Rogertest12.com', CommunityNickname = 'nick23Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert testu;

         User testu1 = new User(LastName = 'Mark O’Rogerdsfd1', ManagerId = testu.id, Alias = 'ty112', Email='test@Rogertest.com', Username='test@Trseegertest12.com', CommunityNickname = 'nicw3Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Assignment_Approver__c = testu.id );
        insert testu1;

        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec2;
        userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3',ManagerId = testu1.id, Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='Large',Assignment_Approver__c = testu.id);
        insert userRec3;
        userRec4 = new User(LastName = 'Raj', FirstName='Jeev',Alias = 'aRoger4' ,ManagerId = testu1.id,Email='test@Rogertest.com', Username='test@Rogertest4.com', CommunityNickname = 'nickRog4', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Region__c = 'Alberta',Sub_Region__c ='Central',Channel__c  ='ABS',Assignment_Approver__c = testu.id);
        insert userRec4;
        
        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec2.id;
        ChildAcc1.BillingPostalCode = 'A1A 1A1';
        insert ChildAcc1;
        
        //Account 2
        ParentAcc2= new Account();
        ParentAcc2.Name = 'ParentAcc2';
        ParentAcc2.Account_Status__c = 'Assigned';
        ParentAcc2.ParentID= null;
        ParentAcc2.OwnerId= userRec3.id;
        ParentAcc2.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc2;

        //MSD Code 1
        ParentMSD1= new MSD_Code__c();
        ParentMSD1.Name = 'ParentMSD1';
        ParentMSD1.MSD_Code_External__c = '789';
        ParentMSD1.Account__c = ParentAcc.id ;
        ParentMSD1.OwnerId= userRec.id;
            
        
        insert ParentMSD1;

        //MSD Code 2
        ParentMSD2= new MSD_Code__c();
        ParentMSD2.Name = 'ParentMSD2';
        ParentMSD2.MSD_Code_External__c = '7891';
        ParentMSD2.Account__c = ParentAcc2.id ;
        ParentMSD2.OwnerId= userRec4.id;
        insert ParentMSD2;
        //MSD Code 2
        ParentMSD3= new MSD_Code__c();
        ParentMSD3.Name = 'ParentMSD3';
        ParentMSD3.MSD_Code_External__c = '7892';
        ParentMSD3.Account__c = ChildAcc1.id ;
        ParentMSD3.OwnerId= userRec3.id;
        insert ParentMSD3;
        //instantiate Shared MSD Member List
        MsdMemberList = new List<MSD_Member__c>();
        
        //Add team members for ParentMSD1
        MSD_Member__c Smem1 = new MSD_Member__c();       
         Smem1.MSD_Code__c = ParentMSD1.Id;         
         Smem1.User__c = UserRec.Id;         
        MsdMemberList.add(Smem1); 
        
       
        //Add team members for ParentMSD2
        MSD_Member__c Smem2 = new MSD_Member__c();
        Smem2.MSD_Code__c = ParentMSD2.Id;         
         Smem2.User__c = UserRec2.Id;        
         Smem2.Name = UserRec2.Name;        
         Smem2.Role__c = 'AE Helper';      
        MsdMemberList.add(Smem2); 
         //Add team members for ParentMSD3
        MSD_Member__c Smem3 = new MSD_Member__c();
         Smem3.MSD_Code__c = ParentMSD3.Id; 
         Smem3.User__c = UserRec2.Id;                
         Smem3.Name = UserRec2.Name;        
         Smem3.Role__c = 'AE Helper';              
        MsdMemberList.add(Smem3); 
        //insert SharedMsdMember List
        insert MsdMemberList;   
    
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as msd code name.
    *******************************************************************************************************/
    static testmethod void testSearchAccount(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.ownerName='Mark';
            obj.searchFilterData.Channel = 'ABS';
            obj.searchFilterData.Location = 'Alberta';
            obj.searchFilterData.Region = 'Central';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);

        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with search the as Child msd code
    *******************************************************************************************************/
    static testmethod void testSearchAccountNegative(){
        setUpData(); 
        test.startTest();
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue();
            obj.searchFilterData.ownerName='Roger';
            obj.searchFilterData.MSDCodeExternal='123432';
            obj.searchFilterData.MSDName='Test1234';
            obj.searchFilterData.Segment='Large';
            obj.searchFilterData.RoleName='Test';

            obj.search();
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with all the search criteria filled, with no record 
                    satisfying the filter criteria
    *******************************************************************************************************/
    static testmethod void testSearchAccountAllFilter(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue();            
            obj.searchFilterData.ownerName='Mark';
            obj.searchFilterData.userName='Acc';
            obj.searchFilterData.codeObj.Name='ABC';
            obj.searchFilterData.codeObj.MSD_Code_External__c='12345';            
            obj.searchFilterData.memberObj.Role__c='AE Helper';
            obj.searchFilterData.searchUser.Channel__c='Business Segment';
            obj.searchFilterData.searchUser.Region__c='National';
            obj.searchFilterData.searchUser.Sub_Region__c='National';            
            obj.search();
            obj.backtosearch();
            obj.search();
            obj.close();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap ==null);
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD Code name and Add members
    *******************************************************************************************************/
    static testmethod void testAddNewMember(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue();  
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            system.debug('==========>obj.listMSDCodeWrap'+obj.listMSDCodeWrap);
            //Add 2 new Members
            system.debug('==========>ParentMSD2.OwnerName'+ParentMSD2.Owner.Name);
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].member.User__c = userRec2.id;
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].member.User__c = userRec.id;
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.finalpage();
            obj.createRequest();

            obj.getreasonCodevalue();            
            obj.searchFilterData.ownerName='Mark';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as MSD Code name and Add members
    *******************************************************************************************************/
    static testmethod void testAddNewMemberNegative(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();          
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].reasonCode =''; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].BusinessCase= '';
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.finalpage();
        }
        test.stopTest(); 
    }
        /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD Code name and Add members and
                    remove a row from confirmation page
    *******************************************************************************************************/
    static testmethod void testAddNewMember3(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();            
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].member.User__c = userRec2.id;
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].member.User__c = userRec.id;
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.finalpage();
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            Apexpages.currentPage().getParameters().put('index','1');
            obj.showSection3=true;
            obj.removeRow();
            //obj.removeRowFromConfirmationPage();
            obj.createRequest();
        }
            
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD Code name, Add members and
                    remove the newly added member
    *******************************************************************************************************/
    static testmethod void testAddNewMember2(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();           
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            //obj.listMSDCodeWrap[0].listAddmemberWrap[0].member.TeamMemberRole= 'SDC';
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].member.User__c = userRec2.id;
            obj.listMSDCodeWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec.id;
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.addRow();
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            //obj.listMSDCodeWrap[0].listAddmemberWrap[1].member.TeamMemberRole= 'SDC';
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].member.User__c = userRec.id;
            obj.listMSDCodeWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            //Remove one of the added members
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            Apexpages.currentPage().getParameters().put('index','1');
            obj.removeRow();
            obj.finalpage();
            obj.backtoStep2();
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD Code name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMember(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();

            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();          
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.removeExistingMem();
            obj.listMSDCodeWrap[0].listmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listMSDCodeWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
            obj.createRequest();
            List<Assignment_Request_Item__c> aReqItems = [Select Id from Assignment_Request_Item__c where Shared_MSD__c =:ParentMSD2.id];
            //system.assert(aReqItems.size()>0);
            obj.getreasonCodevalue();            
            obj.searchFilterData.ownerName='Roger3';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as MSD Code name, remove the 
                    existing member and remove a selected row from confirmation page
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMember2(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();          
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.removeExistingMem();
            obj.listMSDCodeWrap[0].listmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listMSDCodeWrap[0].listmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listMSDCodeWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
            obj.listMSDCodeWrap[0].listmemberWrap[0].removed =true;
            obj.undoRemoveItems();
            obj.removeItems();
        }

        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as MSD Code name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMemberNegative(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
            obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();           
            obj.searchFilterData.MSDName='ParentMSD2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listMSDCodeWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('msdId',ParentMSD2.id);
            obj.removeExistingMem();
            obj.listMSDCodeWrap[0].listmemberWrap[0].reasonCode =''; 
            obj.listMSDCodeWrap[0].listmemberWrap[0].BusinessCase='';
            obj.listMSDCodeWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
        }
        test.stopTest(); 
    }
    static testmethod void testExixtingReqItem(){
         setUpData();
        test.startTest();
        system.runAs(userRec){
        
        assignReq = new Assignment_Request__c();
        assignReq.Request_Type__c = 'Mixed';
        assignReq.Status__c = 'Processed';
        assignReq.Approval_Override__c=false;
        insert assignReq;

        reqItem = new Assignment_Request_Item__c();
        reqItem = new Assignment_Request_Item__c();
        reqItem.Assignment_Request__c = assignReq.id;
        reqItem.MSD__c = ParentMSD1.id;
        reqItem.Approval_Override__c = assignReq.Approval_Override__c;
        reqItem.Business_Case__c ='Test Business Case';
        reqItem.Reason_Code__c = 'Test';
        reqItem.Action__c = 'Add';
        reqItem.Status__c = 'Submitted';
        reqItem.Requested_Item_Type__c='MSD Member';
        reqItem.Role__C = 'SDC';
        reqItem.NPA__c = '1';
        reqItem.Member__c = userRec3.Id;
        reqItem.Member_Channel__c = 'Large';
        insert reqItem;

        Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
        req1.setComments('Submitting request for approval.');
        req1.setObjectId(reqItem.id);
        Approval.ProcessResult result = Approval.process(req1);

        TAG_MSDMemberUpdateController obj = new TAG_MSDMemberUpdateController();
        obj.getreasonCodevalue();         
        obj.getreasonCodevalue(); 
            obj.getNPAvalue();
            obj.getCityvalue();
            obj.getRegionvalue();
            obj.getRoles();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
        obj.searchFilterData.MSDName='ParentMSD1';
        obj.search();
        }
        test.stopTest();
    }
    
    
    
}