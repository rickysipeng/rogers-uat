/*
===============================================================================
 Class Name   : msdSnapshotSharing_schedular
===============================================================================
PURPOSE:    This schedular class runs the msdSnapshotSharing_batch class with 
            a batch of 200 records. 

Developer: Deepika Rawat
Date: 09/03/2013

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
09/03/2013          Deepika                     Created
===============================================================================
*/
global class msdSnapshotSharing_schedular implements schedulable{

global void execute(SchedulableContext ctx) {

msdSnapshotSharing_batch sc = new msdSnapshotSharing_batch();
ID batchprocessid = Database.executeBatch(sc,200);
system.debug('sc*************' +sc);
}

}