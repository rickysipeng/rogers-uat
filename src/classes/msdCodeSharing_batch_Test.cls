/*
===============================================================================
Class Name : msdCodeSharing_batch_Test
===============================================================================
PURPOSE: This is a test class for msdCodeSharing_batch class

COMMENTS: 

Developer: Deepika Rawat
Date: 18/9/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
18/9/2013               Deepika               Created

March 2015             Aakanksha Patel        Modified(For TAG)
===============================================================================

*/
@isTest (SeeAllData = true)
private class msdCodeSharing_batch_Test{
    static Account acc;
    static MSD_Code__c msd; 
    static AccountTeamMember atm;
    static AccountTeamMember atm2;
    static AccountTeamMember atm3;
    static MSD_Code__share msdShare;
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  
 
    static testmethod void testMethod1(){
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        User u;
        System.runAs ( thisUser ) {
        Profile p = [select id from profile where name='System Administrator' ];
        u = new User(alias = 'TestUser', email='standarduser@testorg.com.rogers', 
            emailencodingkey='UTF-8', lastname='TestUser', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', 
            username='standarduser@testorg.com.Rogers');
        }  
         
        System.runAs(u) {
            acc = new Account();
            acc.Name='TestAcc';
            acc.OwnerId=listUser[0].id;
            acc.Account_Status__c='Assigned';
            acc.BillingCountry= 'CA';
            acc.BillingPostalCode = 'A9A 9A9';
            acc.BillingState = 'MA';
            acc.BillingCity='City';
            acc.BillingStreet='Street';
            acc.ParentId = null;
            insert acc; 

            atm= new AccountTeamMember();
            atm.accountId= acc.id;
            atm.userId= listUser[1].id;
            atm.TeamMemberRole='Member';
            insert atm;
            
            atm2= new AccountTeamMember();
            atm2.accountId= acc.id;
            atm2.userId= listUser [0].id;
            atm2.TeamMemberRole='Member';
            insert atm2;
            
            msd= new MSD_Code__c();
            msd.ownerId=listUser[1].id;
            msd.Account__c= acc.id;
            insert msd;
            
            msdShare= new MSD_Code__share();
            msdShare.UserOrGroupId= listUser[1].id;
            msdShare.ParentID =msd.id;
            msdShare.RowCause='AccountTeamMember__c';
            msdShare.AccessLevel ='Read';  
            insert msdShare;
        }

        //System.runAs ( thisUser ) {
        //    listUser[1].isActive = false;
        //    update listUser[1];
        //}
        System.runAs(u) {
        Database.BatchableContext scc;
        msdCodeSharing_batch sc = new msdCodeSharing_batch();           
        Test.StartTest();
        sc.Query ='select id, Account__c FROM MSD_Code__c WHERE id =\''+ msd.id+ '\' ';
        sc.start(scc);
        List<MSD_Code__c> scope = [select id, Account__c FROM MSD_Code__c WHERE id =: msd.id];
        sc.execute(scc,scope);
        sc.finish(scc);
        //ID batchprocessid = Database.executeBatch(sc);
        Test.StopTest();
        }
    }
}