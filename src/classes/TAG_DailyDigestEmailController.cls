/*
===============================================================================
 Class Name   : TAG_DailyDigestEmailController 
===============================================================================
PURPOSE:    This batch class sends a daily digest Email.
  

Developer: Prem Kumar Gupta
Date: 03/06/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/06/2015           Prem Kumar Gupta            Original Version
05/15/2015           Deepika Rawat               Modified code 
===============================================================================
*/
global class TAG_DailyDigestEmailController implements Database.Batchable<SObject>, Database.stateful{
    global String query;
    static OrgWideEmailAddress orgEmailId=[select id, Address, DisplayName from OrgWideEmailAddress limit 1] ;
    global Map<Id, List<ProcessInstanceWorkitem>> mapUserARItems = new Map<Id, List<ProcessInstanceWorkitem>>();
    global Map<Id, List<ProcessInstanceWorkitem>> mapUserOtherItems = new Map<Id, List<ProcessInstanceWorkitem>>();
    global Map<id,Assignment_Request_Item__c> mapAssignItems = new Map<id,Assignment_Request_Item__c>();
    
    
    global Database.QueryLocator start(Database.BatchableContext BC){
        DateTime currentDay = system.now().AddDays (-1);
        if(!Test.isRunningTest()){
            query = 'Select ProcessInstanceId,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name, ProcessInstance.TargetObject.type, OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById,CreatedBy.Name,  ActorId From ProcessInstanceWorkitem where  CreatedDate>=:currentDay order by ActorId';
            return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('Select ProcessInstanceId,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name, ProcessInstance.TargetObject.type, OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById,CreatedBy.Name, ActorId From ProcessInstanceWorkitem where CreatedDate>=:currentDay order by ActorId limit 10');
        }     
    }
    
    global void execute(Database.BatchableContext BC, List<ProcessInstanceWorkitem> scope){
        Map<Id, List<ProcessInstanceWorkitem>> mapGroupPendingItems = new Map<Id, List<ProcessInstanceWorkitem>>();
        Map<Id, Id> mapReqPendingItem = new Map<Id, Id>();
        Set<Id> setAssignReqIds = new Set<Id>();
        Set<Id> groupIds = new Set<Id>();
        Set<Id> userGroupIds = new Set<id>();
        for(ProcessInstanceWorkitem item :scope){
        system.debug('****->item '+ item);
            if(String.valueof(item.ActorId).startsWith('005')){
            system.debug('****->item(inside 005) '+ item);
                if(item.ProcessInstance.TargetObject.type == 'Assignment_Request_Item__c'){
                    mapReqPendingItem.put(item.ProcessInstance.TargetObjectId, item.id);
                    List<ProcessInstanceWorkitem> tempItemList = mapUserARItems.get(item.ActorId);
                    if(tempItemList==null)
                        tempItemList = new List<ProcessInstanceWorkitem>();
                    tempItemList.add(item);
                    mapUserARItems.put(item.ActorId, tempItemList);
                }
                else{
                    List<ProcessInstanceWorkitem> tempItemList2 = mapUserOtherItems.get(item.ActorId);
                    if(tempItemList2==null)
                        tempItemList2 = new List<ProcessInstanceWorkitem>();
                    tempItemList2.add(item);
                    mapUserOtherItems.put(item.ActorId, tempItemList2);                 
                }
            }           
        }
        List<Assignment_Request_Item__c> lstAssignReqItem = [Select Account__c,Account__r.Name, MSD__c ,MSD__r.Name, Shared_MSD__c ,Shared_MSD__r.Name, Requested_Item_Type__c, Action__c,Current_Owner__c ,Current_Owner__r.Name, New_Owner__c ,New_Owner__r.Name, Member__c , Member__r.Name, CreatedDate, CreatedById,CreatedBy.Name, Business_Case__c from  Assignment_Request_Item__c where id in:mapReqPendingItem.keyset()];

        for(Assignment_Request_Item__c reqItem:lstAssignReqItem){
            Id processItemId = mapReqPendingItem.get(reqItem.id);
            if(processItemId!=null){
                mapAssignItems.put(processItemId, reqItem);
            }

        }
        system.debug('mapAssignItems****'+mapAssignItems);
    }

    global void finish(Database.BatchableContext BC) {
        List<String> toAddresses;
        List <Messaging.SingleEmailMessage> lstSendMails=new  List <Messaging.SingleEmailMessage>();
        Messaging.SingleEmailMessage email;
        String eBody;
        
        if(mapUserARItems!=null && mapUserARItems.size()>0){
            Map<Id, User> mapUser = new Map<id, User>([Select id, Name,  email from User where id in :mapUserARItems.keyset()]);
            for(id usrId : mapUserARItems.keyset()){
                toAddresses = new List<String>() ;
                toAddresses.add(mapUser.get(usrId).email);
                List<ProcessInstanceWorkitem> lstARWorkItem = mapUserARItems.get(usrId);
                List<ProcessInstanceWorkitem> lstOtherWorkItem = mapUserOtherItems.get(usrId);
                Integer arSize = 0;
                Integer otherSize = 0;
                if(lstARWorkItem!=null && lstARWorkItem.size()>0)
                    arSize = lstARWorkItem.size();
                if(lstOtherWorkItem!=null && lstOtherWorkItem.size()>0)
                    otherSize = lstOtherWorkItem.size();
                eBody = '<font size="2"> Hi '+mapUser.get(usrId).Name+', <br/><br/>';
                eBody = eBody + 'You have '+ arSize +' new TAG pending requests and '+ otherSize+' other items from yesterday awaiting your approval:';
                eBody = eBody + '<table style="font-size:11px" border="1" cellspacing="3px" width="100%"><tr><th>Account Name</th><th>MSD/Shared MSD Code</th><th>Request Item Type</th><th>Action</th><th>Current Owner</th><th>New Owner</th><th>Member</th><th>Request Date</th><th>Requested By</th><th>Business Case</th></tr>';
                for(ProcessInstanceWorkitem wItem :lstARWorkItem){
                    Assignment_Request_Item__c assignItem = mapAssignItems.get(wItem.id);
                    eBody = eBody + '<tr><td>';
                    if(assignItem.Account__c!=null && assignItem.Account__r.Name!=null){
                        eBody = eBody+ assignItem.Account__r.Name;
                    }
                    eBody = eBody +'</td><td>';
                    if(assignItem.MSD__c!=null && assignItem.MSD__r.Name!=null ){
                        eBody = eBody +assignItem.MSD__r.Name;
                    }
                    if(assignItem.Shared_MSD__c!=null && assignItem.Shared_MSD__r.Name!=null){
                        eBody = eBody +assignItem.Shared_MSD__r.Name;
                    }
                    eBody = eBody+'</td><td>'+assignItem.Requested_Item_Type__c+'</td><td>';
                    if(assignItem.Action__c!=null){
                        eBody= eBody + assignItem.Action__c;
                    }
                    eBody = eBody+'</td><td>';
                    if(assignItem.Current_Owner__c!=null && assignItem.Current_Owner__r.Name!=null){
                        eBody= eBody + assignItem.Current_Owner__r.Name;
                    }
                    eBody = eBody+'</td><td>';
                    if(assignItem.New_Owner__c!=null && assignItem.New_Owner__r.Name!=null){
                        eBody = eBody +assignItem.New_Owner__r.Name; 
                    }
                    eBody = eBody +'</td><td>';
                    if(assignItem.Member__c!=null && assignItem.Member__r.Name!=null){
                        eBody = eBody +assignItem.Member__r.Name;
                    }
                    eBody = eBody +'</td><td>'+assignItem.CreatedDate+'</td><td>'+assignItem.CreatedBy.Name+'</td><td>'+assignItem.Business_Case__c+'</td></tr>';
                }
                eBody = eBody + '</table><br/><br/>';

                
                if(lstOtherWorkItem!=null && lstOtherWorkItem.size()>0){
                    eBody = eBody + '<table style="font-size:11px" border="1" cellspacing="3px" width="90%"><tr><th>Related To</th><th>Type</th><th>Date Submitted</th><th>Submitter</th></tr>';
                    for(ProcessInstanceWorkitem wItem2 : lstOtherWorkItem){
                        eBody = eBody + '<tr><td>'+wItem2.ProcessInstance.TargetObject.Name+ '</td><td>'+wItem2.ProcessInstance.TargetObject.type+'</td><td>'+wItem2.CreatedDate+'</td><td>'+wItem2.CreatedBy.Name+'</td></tr>';
                    }
                     eBody = eBody + '</table><br/><br/>';
                }
                              
                eBody = eBody + 'Please go the appropriate link to review your items:<br/><br/><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/apex/TAG_ApproverView">TAG Approver View</a> <br/> <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/04i">All other items</a><br/><br/>Thank you.<br/>Approval Request</font>';
                System.debug(eBody);
                email = new Messaging.SingleEmailMessage();
                email.setToAddresses(toAddresses);
                email.setSubject(label.Transfer_dailyMsg); 
                email.setSaveAsActivity(false);
                email.setOrgWideEmailAddressId(orgEmailId.id); 
                email.setHTMLBody(eBody); 
                lstSendMails.add(email);
            }
        }
        else if((mapUserARItems==null || mapUserARItems.size()==0) && (mapUserOtherItems!=null && mapUserOtherItems.size()>0) ){
             Map<Id, User> mapUser = new Map<id, User>([Select id, Name,  email from User where id in :mapUserOtherItems.keyset()]);
             for(id usrId : mapUserOtherItems.keyset()){
                 toAddresses = new List<String>() ;
                 toAddresses.add(mapUser.get(usrId).email);
                List<ProcessInstanceWorkitem> lstOtherWorkItem = mapUserOtherItems.get(usrId);
                Integer otherSize = 0;
                if(lstOtherWorkItem!=null && lstOtherWorkItem.size()>0)
                    otherSize = lstOtherWorkItem.size();
                 eBody = '<font size="2"> Hi '+mapUser.get(usrId).Name+', <br/><br/>';
                 eBody = eBody + 'You have 0 new TAG pending requests and '+ otherSize +' other items from yesterday awaiting your approval:';
                 eBody = eBody + '<table style="font-size:11px" border="1" cellspacing="3px" width="90%"><tr><th>Related To</th><th>Type</th><th>Date Submitted</th><th>Submitter</th></tr>';
                for(ProcessInstanceWorkitem wItem2 : lstOtherWorkItem){
                    eBody = eBody + '<tr><td>'+wItem2.ProcessInstance.TargetObject.Name+ '</td><td>'+wItem2.ProcessInstance.TargetObject.type+'</td><td>'+wItem2.CreatedDate+'</td><td>'+wItem2.CreatedBy.Name+'</td></tr>';
                }
                eBody = eBody + '</table><br/><br/>';               
                eBody = eBody + 'Please go the appropriate link to review your items:<br/><br/><a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/apex/TAG_ApproverView">TAG Approver View</a> <br/> <a href="'+URL.getSalesforceBaseUrl().toExternalForm()+'/04i">All other items</a><br/><br/>Thank you.<br/>Approval Request</font>';
                email = new Messaging.SingleEmailMessage();
                email.setToAddresses(toAddresses);
                email.setSubject(label.Transfer_dailyMsg); 
                email.setSaveAsActivity(false);
                email.setOrgWideEmailAddressId(orgEmailId.id); 
                email.setHTMLBody(eBody); 
                lstSendMails.add(email);
             }
            
        }
        if(!lstSendMails.IsEmpty()) {
                Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(lstSendMails);
             system.debug('===========Mail Sent===========');
            } 
    }
}