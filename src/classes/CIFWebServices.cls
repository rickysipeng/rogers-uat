/*
    @Name: CIFWebServices
    @Description: 
    @Version: 1.1.0 
    
    ===VERSION HISTORY === 
    | Version Number | Author               | Description
    | 1.0.0          | Unknown              |  Initial
    | 1.1.0          | ShiftCRM - RM        |  createCIF -> Added mapping for SOI fields from the "Mandatory Fields" object
    Jan 23, 2015. Paul Saini. Removed Contact.Ext__c as per requirements.
    As per comments from MP: The data in this field is planned to be concatenated with the standard Phone field. 
    The code should no longer reference the Ext__c field.
*/

global without sharing class CIFWebServices {
    
    webservice static Boolean updateBaseURL(String cifId){
        try{
            CIF__c cop = [SELECT Id, BaseURLEmailTemplate__c FROM CIF__c WHERE Id = :cifId];
            
            cop.BaseURLEmailTemplate__c = URL.getSalesforceBaseUrl().toExternalForm();
            UPDATE cop;
        } catch(Exception ex){
            return false;
        }
        
        return true;
    }
    /* No longer used now that sales can create the COP 
    webService static id createCustomerCIF(String oppId){
        
        Id cifId = createCIF(oppId);

        // Add the customer COP id to the opp.      
        Opportunity opp = [SELECT Id, Customer_Facing_COP__c FROM Opportunity WHERE Id = :oppId];
        if (opp!=null){
            opp.Customer_Facing_COP__c = cifId;
            UPDATE opp;
        }
        
        return cifId;
    }
    */
    webService static id createCIF(String oppId){
        Map<Id, List<QuoteLineItem>> services;
        List<QuoteLineItem> tempQLI = new List<QuoteLineItem>();        
        Opportunity opp = [SELECT Id, Name, AccountId, Type, SyncedQuoteId, OwnerId, Contact__c FROM Opportunity WHERE id = :oppId];
        
       
        Quote SyncedQuote;
        
        System.debug('\n\n\n\n\n\n' + opp.SyncedQuoteId);
        // Need to ensure that Quote is synched - we will send back the oppId if there is a problem
        if (opp.SyncedQuoteId == null){
            System.debug('\n\n\n\n\n\nBad Opp!!');
            return oppId;
        }
        
        /* Create a map of Record Types for the CIFSite so we can use it to decide which Record type to assign the CIF */
       
        List <RecordType> cifSiteRecordTypes = new List <RecordType> ([Select Id, recordtype.Name from RecordType where recordtype.SobjectType = 'CIF_Site__c']);
        Map <String, Id> mapRTcifSite = new Map <String, Id> ();
        
        for (RecordType rt : cifSiteRecordTypes){
            mapRTcifSite.put(rt.Name,rt.id);  
        }
       
        Account account = [SELECT Id, OwnerId, RecordType.Name, Name FROM Account WHERE id = :opp.AccountId]; 
        User oppOwner = [SELECT Id, Name FROM User WHERE Id = :opp.OwnerId];
        User accOwner = [SELECT Id, Name, AM_ID__c FROM User WHERE Id = :account.OwnerId];
        
        
        CIF__c cif = new CIF__c();
        cif.Opportunity_Name__c = opp.Id;
        cif.Contact_Name__c = opp.Contact__c;
        cif.Opportunity__c = oppOwner.Id;
        cif.Account_Owner__c = accOwner.Id;
        cif.AM_ID__c = accOwner.AM_ID__c;
        cif.COP_Owner__c = UserInfo.getUserId();
        
        INSERT cif;
                
        Id quoteId = opp.SyncedQuoteId;
        Set<Id> siteIds = new Set<Id>();
        services = new Map<Id, List<QuoteLineItem>>();   
        
        // Need to create the CIF Sites.
        List<CIF_Site__c> cifSites = new List<CIF_Site__c>();
        List<Quote_Site__c> quoteSites = [Select Quote__c, Quote__r.Id, Site__r.Id, Site__c from Quote_Site__c where Quote__r.Id = :quoteId];
        List<QuoteLineItem> lineItems = [SELECT Id, Site__r.id, Site__r.Display_Name__c,Site__r.ServiceableLocation__r.Network_Access_Type__c, Site__r.ServiceableLocation__r.Access_Type__c, Site__r.ServiceableLocation__r.Build_Date__c, Service_Type__c FROM QuoteLineItem WHERE QuoteId = :quoteId];
        
        
        Set<String> manSiteIds = new Set<String>();
        for(Quote_Site__c qs : quoteSites){
            siteIds.add(qs.Site__r.Id);
            manSiteIds.add(Utils.get15CharId(qs.Site__r.Id));
        }
        
        
        
        System.debug('Site Ids: ' + siteIds);
        // PS: removed Ext__c from query below.
        // Get all of the Mandatory Fields and create a map with the siteId as the Key and the Mandatory Field as the value 
        List<Mandatory_COP_Fields__c> mandatoryFields = [SELECT Id, 
                        Building_Entry_Contact__r.FirstName, Building_Entry_Contact__r.LastName, Building_Entry_Contact__r.Phone, Building_Entry_Contact__r.MobilePhone, Building_Entry_Contact__r.Email,
                        Building_Entry_Contact__r.Title, Building_Entry_Contact__r.MailingCity, Building_Entry_Contact__r.MailingCountry, Building_Entry_Contact__r.MailingState, Building_Entry_Contact__r.MailingStreet, Building_Entry_Contact__r.MailingPostalCode,
                        Building_Entry_Contact__c, Not_Applicable__c, Quote_Site__c, Roger_Account_Optional__c, Suite_Floor__c, Quote_Site__r.Site__r.Display_Name__c, Demarc__c, SiteId__c, 
                        Customer_Requested_SOIDate__c, SOI_Requested_Date__c, SOI_Date__c //V1.1 RM
                        FROM Mandatory_COP_Fields__c WHERE SiteId__c IN :manSiteIds];

        Map<Id, Mandatory_COP_Fields__c> mandatoryFieldMap = new Map<Id, Mandatory_COP_Fields__c>();
        
        System.debug('Man Fields: ' + mandatoryFields);
        
        if (mandatoryFields != null && mandatoryFields.size() > 0){
            for (Mandatory_COP_Fields__c m : mandatoryFields){
                mandatoryFieldMap.put(m.siteId__c, m);
            }
        }
        for (Id siteId : siteIds){
            tempQLI = new List<QuoteLineItem>();  
            for(QuoteLineItem li : lineItems){
                if (li.Site__r.Id == siteId){
                    tempQLI.add(li);
                }
            }
            if(tempQLI.size()>0)
                services.put(siteId, tempQLI); 
        }

        Set<Id> Ids = services.keySet();
        
        for (Id siteId : Ids){
            CIF_Site__c cifSite = new CIF_Site__c();
            cifSite.CIF_Number__c = cif.Id;
            cifSite.Quote__c = quoteId;
            cifSite.Site__c = siteId;
            
            if (mandatoryFields != null && mandatoryFields.size() > 0 && mandatoryFieldMap.get(siteId) != null){
                cifSite.Building_Entry_Contact__c = mandatoryFieldMap.get(siteId).Building_Entry_Contact__c;
                cifSite.Demarcation__c = mandatoryFieldMap.get(siteId).Demarc__c;
                cifSite.Suite_Floor__c = mandatoryFieldMap.get(siteId).Suite_Floor__c;
                cifSite.SOI_Date__c = mandatoryFieldMap.get(siteId).SOI_Date__c; //V1.1 RM
                cifSite.SOI_Requested_Date__c = mandatoryFieldMap.get(siteId).SOI_Requested_Date__c; //V1.1 RM
                cifSite.Customer_Requested_SOIDate__c = mandatoryFieldMap.get(siteId).Customer_Requested_SOIDate__c; //V1.1 RM
                cif.Rogers_Account_Optional__c = mandatoryFieldMap.get(siteId).Roger_Account_Optional__c;
            }
            
            try{
                ServiceableLocation__c servicableLocation = null;
                if (services.get(siteId)!=null && services.get(siteId).size() > 0)
                    servicableLocation  = services.get(siteId)[0].Site__r.ServiceableLocation__r;
                    
                if (servicableLocation != null/* && !Utils.isEmpty(servicableLocation.Network_Access_Type__c)*/){
                        cifSite.Order_Type__c = (!Utils.isEmpty(servicableLocation.Network_Access_Type__c)?servicableLocation.Network_Access_Type__c+ ' ':'')  + (!Utils.isEmpty(servicableLocation.Access_Type__c)?servicableLocation.Access_Type__c:'');
                }
            }catch(Exception ex){
                System.debug('CIFWebServices.createCIF: Problem setting the Order Type for the COP Site.');
            }
      
                
            if (account.RecordType.Name.startsWith('Carrier')){
                cifSite.RecordTypeId = mapRTcifSite.get('Carrier CIF Site');
            }else{
                cifSite.RecordTypeId = mapRTcifSite.get('Enterprise CIF Site');
            }
            
               
            
            cifSites.add(cifSite);
        }
        
        insert cifSites;
        
        if (!Utils.isEmpty(cif.Rogers_Account_Optional__c)){
            UPDATE CIF;
        }
        
         // Customer Facing COP - this is used to see if it has already been sent to the customer
        if (opp!=null){
            opp.Customer_Facing_COP__c = cif.Id;
            UPDATE opp;
        }
        return cif.Id;
    }
    /*
    webService static id createCIF_v2(String oppId){
        Map<Id, List<QuoteLineItem>> services;
        List<QuoteLineItem> tempQLI = new List<QuoteLineItem>();        
        Opportunity opp = [SELECT Id, Name, AccountId, Type, SyncedQuoteId, OwnerId, Contact__c FROM Opportunity WHERE id = :oppId];
        
       
        Quote SyncedQuote;
        
        System.debug('\n\n\n\n\n\n' + opp.SyncedQuoteId);
        // Need to ensure that Quote is synched - we will send back the oppId if there is a problem
        if (opp.SyncedQuoteId == null){
            System.debug('\n\n\n\n\n\nBad Opp!!');
            return oppId;
        }
        
        // Create a map of Record Types for the CIFSite so we can use it to decide which Record type to assign the CIF 
       
        List <RecordType> cifSiteRecordTypes = new List <RecordType> ([Select Id, recordtype.Name from RecordType where recordtype.SobjectType = 'CIF_Site__c']);
        Map <String, Id> mapRTcifSite = new Map <String, Id> ();
        
        for (RecordType rt : cifSiteRecordTypes){
            mapRTcifSite.put(rt.Name,rt.id);  
        }
       
        Account account = [SELECT Id, OwnerId, RecordType.Name, Name FROM Account WHERE id = :opp.AccountId]; 
        User oppOwner = [SELECT Id, Name FROM User WHERE Id = :opp.OwnerId];
        User accOwner = [SELECT Id, Name, AM_ID__c FROM User WHERE Id = :account.OwnerId];
        
        Id quoteId = opp.SyncedQuoteId;
        
        CIF__c cif = new CIF__c();
        cif.Opportunity_Name__c = opp.Id;
        cif.Contact_Name__c = opp.Contact__c;
        cif.Opportunity__c = oppOwner.Id;
        cif.Account_Owner__c = accOwner.Id;
        cif.AM_ID__c = accOwner.AM_ID__c;
        
        // Let's get the Mandatory Fields
        List<Mandatory_COP_Fields__c> mandatoryCOPFieldsList = new List<Mandatory_COP_Fields__c>();
        try{
            mandatoryCOPFieldsList = [SELECT Id, Building_Entry_Contact__c, Not_Applicable__c, Quote_Site__c, Roger_Account_Optional__c, Suite_Floor__c, Quote_Site__r.Site__r.Display_Name__c FROM Mandatory_COP_Fields__c WHERE Quote_Site__r.Quote__c = :quoteId Order By CreatedDate asc];
        }catch(Exception ex1){
            // The mandatory field objects do not exist.
            
        }
        if (mandatoryCOPFieldsList!= null && !mandatoryCOPFieldsList.isEmpty())
            cif.Rogers_Account_Optional__c = mandatoryCOPFieldsList[0].Roger_Account_Optional__c;
        
        INSERT cif;
                
        
        Set<Id> siteIds = new Set<Id>();
        services = new Map<Id, List<QuoteLineItem>>();   
        
        // Need to create the CIF Sites.
        List<CIF_Site__c> cifSites = new List<CIF_Site__c>();
        List<Quote_Site__c> quoteSites = [Select Quote__c, Quote__r.Id, Site__r.Id, Site__c from Quote_Site__c where Quote__r.Id = :quoteId];
        List<QuoteLineItem> lineItems = [SELECT Id, Site__r.id, Site__r.Display_Name__c,Site__r.ServiceableLocation__r.Network_Access_Type__c, Site__r.ServiceableLocation__r.Access_Type__c, Site__r.ServiceableLocation__r.Build_Date__c, Service_Type__c FROM QuoteLineItem WHERE QuoteId = :quoteId];
        
        
        

        // We need to place mandatoryCOPFieldsList in a map with the siteId as the key.
        Map<Id, Mandatory_COP_Fields__c> mandatoryFieldMap = new Map<Id, Mandatory_COP_Fields__c>();
         
        for (Mandatory_COP_Fields__c m : mandatoryCOPFieldsList){
            mandatoryFieldMap.put(m.Quote_Site__r.Site__c, m);
        }
        
        for(Quote_Site__c qs : quoteSites){
            siteIds.add(qs.Site__r.Id);
        }
        
        Map<Id, Site__c> siteMap = new Map<Id, Site__c>([SELECT Id, Suite_Floor__c FROM Site__c WHERE Id IN :siteIds]);
        
        for (Id siteId : siteIds){
            tempQLI = new List<QuoteLineItem>();  
            for(QuoteLineItem li : lineItems){
                if (li.Site__r.Id == siteId){
                    tempQLI.add(li);
                }
            }
            if(tempQLI.size()>0)
                services.put(siteId, tempQLI); 
        }

        Set<Id> Ids = services.keySet();
        List<Site__c> sites = new List<Site__c>(); 
        for (Id siteId : Ids){
            CIF_Site__c cifSite = new CIF_Site__c();
            cifSite.CIF_Number__c = cif.Id;
            cifSite.Quote__c = quoteId;
            cifSite.Site__c = siteId;
            
            if (mandatoryFieldMap.get(siteId)!= null){
                cifSite.Building_Entry_Contact__c = mandatoryFieldMap.get(siteId).Building_Entry_Contact__c;
                
                if (!mandatoryFieldMap.get(siteId).Not_Applicable__c){
                    siteMap.get(siteId).Suite_Floor__c = mandatoryFieldMap.get(siteId).Suite_Floor__c;
                    sites.add(siteMap.get(siteId));
                }
            }
                      
            try{
                ServiceableLocation__c servicableLocation = null;
                if (services.get(siteId)!=null && services.get(siteId).size() > 0)
                    servicableLocation  = services.get(siteId)[0].Site__r.ServiceableLocation__r;
                    
                if (servicableLocation != null){
                        cifSite.Order_Type__c = (!Utils.isEmpty(servicableLocation.Network_Access_Type__c)?servicableLocation.Network_Access_Type__c+ ' ':'')  + (!Utils.isEmpty(servicableLocation.Access_Type__c)?servicableLocation.Access_Type__c:'');
                }
            }catch(Exception ex){
                System.debug('CIFWebServices.createCIF: Problem setting the Order Type for the COP Site.');
            }
      
                
            if (account.RecordType.Name.startsWith('Carrier')){
                cifSite.RecordTypeId = mapRTcifSite.get('Carrier CIF Site');
            }else{
                cifSite.RecordTypeId = mapRTcifSite.get('Enterprise CIF Site');
            }
            
            
            cifSites.add(cifSite);
        }
        
        insert cifSites;
        UPDATE sites;
          
         // Customer Facing COP - this is used to see if it has already been sent to the customer
        if (opp!=null){
            opp.Customer_Facing_COP__c = cif.Id;
            UPDATE opp;
        }
        return cif.Id;
    }
   */
    webservice static void resetUpdatedCOP(String quoteId){
        
        if (Utils.isEmpty(quoteId))
            return;
        
        String syncedQuoteId = ((String)quoteId).substring(0,15);
        List<CIF__c> cifs = [SELECT Id, updatedCOP__c FROM CIF__C WHERE syncedQuoteId__c = :syncedQuoteId];
        
        for (CIF__c c : cifs){
            c.updatedCOP__c = false;
        }
        
        UPDATE cifs;
    }
    
    public static boolean updateCOP(List<String> quoteIds){
        // Create a map of Record Types for the CIFSite so we can use it to decide which Record type to assign the CIF 
        List <RecordType> cifSiteRecordTypes = new List <RecordType> ([Select Id, recordtype.Name from RecordType where recordtype.SobjectType = 'CIF_Site__c']);
        Map <String, Id> mapRTcifSite = new Map <String, Id> ();
        
        for (RecordType rt : cifSiteRecordTypes){
            mapRTcifSite.put(rt.Name,rt.id);  
        }
                
        System.debug('QuoteIds: ' + quoteIds);
        List<String> syncedQuoteIds = new List<String>();
        
        for(String q : quoteIds){
            syncedQuoteIds.add(((String)(q)).substring(0,15));
        }
        System.debug('Here we are with the quote ids: ' + quoteIds);
        
        List<CIF__c> cifs = [SELECT Id, syncedQuoteId__c, Opportunity_Name__r.Account.RecordType.Name, (SELECT Id, Site__c FROM CIF_Sites__r) FROM CIF__C WHERE syncedQuoteId__c IN :syncedQuoteIds];
        System.debug('Here we are with the cif ids: ' + cifs);
        
        
        // We have the list of COP Sites that exist.
        // The ones that do not exist need  to be created
        // The ones that do exist but has no sites any more need to be deleted
        List<Quote_Site__c> quoteSites = [Select Quote__c, Quote__r.Id, Site__r.Id, Site__c from Quote_Site__c where Quote__c IN :quoteIds];
        Map<Id, List<Quote_Site__c>> quoteSiteMap = new Map<Id, List<Quote_Site__c>>();
        
        // Prepare a map that links the quote sites for a particular quote.
        List<Quote_Site__c> tempQSList;
        for (Quote_Site__c qs : quoteSites){
            if (quoteSiteMap.get(qs.quote__c)==null)
                tempQSList = new List<Quote_Site__c>();
            tempQSList.add(qs);
            quoteSiteMap.put(qs.quote__c, tempQSList);
        }        
        
        List<QuoteLineItem> lineItems = [SELECT Id, Site__r.id, QuoteId, Site__r.Display_Name__c,Site__r.ServiceableLocation__r.Network_Access_Type__c, Site__r.ServiceableLocation__r.Access_Type__c, Site__r.ServiceableLocation__r.Build_Date__c, Service_Type__c FROM QuoteLineItem WHERE QuoteId IN :quoteIds];
        System.debug('Quote Line Items on Update COP:' + lineItems );    
            
        List<CIF_Site__c> cSiteToCreate = new List<CIF_Site__c>();
        List<CIF_Site__c> cSiteToDelete = new List<CIF_Site__c>();
        
        
        // These are all of the cifs that need to be updated. 
        if(cifs!=null && cifs.size()>0){
            for (CIF__c c : cifs){
                Map<Id, CIF_Site__c> cSiteMap = new Map<Id, CIF_Site__c>();
                List<QuoteLineItem> tempQLI = new List<QuoteLineItem>();
                Map<Id, List<QuoteLineItem>> services;
                Set<Id> siteIds = new Set<Id>();
                services = new Map<Id, List<QuoteLineItem>>();
                c.updatedCOP__c = true;

                if(c.CIF_Sites__r!=null && c.CIF_Sites__r.size()>0){
                    for (CIF_Site__c cSite : c.CIF_Sites__r){
                        cSiteMap.put(cSite.site__c, cSite);
                    }
                }

                if(c.syncedQuoteId__c!=null && quoteSiteMap.get(c.syncedQuoteId__c)!=null){
                    for(Quote_Site__c qs : quoteSiteMap.get(c.syncedQuoteId__c)){
                        siteIds.add(qs.Site__c);
                    }
                }
                
                if(siteIds!=null){
                    for (Id siteId : siteIds){
                        tempQLI = new List<QuoteLineItem>();  
                        for(QuoteLineItem li : lineItems){
                            if (li.Site__c == siteId){
                                tempQLI.add(li);
                            }
                        }
                        if(tempQLI.size()>0)
                            services.put(siteId, tempQLI); 
                    }
                }
        
                System.debug('These are the services in a map: ' + services);
        
                Set<Id> Ids = services.keySet();
                
                
                if(Ids!=null){
                    for (Id siteId : Ids){
                        System.debug('Site id we are checking: ' + siteId);
                        System.debug('Site id and cSiteMap: ' + cSiteMap.get(siteId));
                        if (cSiteMap.get(siteId) == null){
                            // Create the CIFSite
                            CIF_Site__c cifSite = new CIF_Site__c();
                            
                            cifSite.CIF_Number__c = c.Id;
                            cifSite.Quote__c = (services.get(siteId)).get(0).quoteId;
                            cifSite.Site__c = siteId;
                            try{
                                ServiceableLocation__c servicableLocation = null;
                                if (services.get(siteId)!=null && services.get(siteId).size() > 0)
                                    servicableLocation  = services.get(siteId)[0].Site__r.ServiceableLocation__r;
                                    
                                if (servicableLocation != null){
                                        cifSite.Order_Type__c = (!Utils.isEmpty(servicableLocation.Network_Access_Type__c)?servicableLocation.Network_Access_Type__c+ ' ':'')  + (!Utils.isEmpty(servicableLocation.Access_Type__c)?servicableLocation.Access_Type__c:'');
                                }
                            }catch(Exception ex){
                                System.debug('CIFWebServices.createCIF: Problem setting the Order Type for the COP Site.');
                            }
                      
                                
                            if (c.Opportunity_Name__r.Account.RecordType.Name.startsWith('Carrier')){
                                cifSite.RecordTypeId = mapRTcifSite.get('Carrier CIF Site');
                            }else{
                                cifSite.RecordTypeId = mapRTcifSite.get('Enterprise CIF Site');
                            }
                            
                            cSiteToCreate.add(cifSite);
                        }
                    }
                }
                if(cSiteMap.values()!=null){
                for (CIF_Site__c cSite : cSiteMap.values()){
                    if (services.get(cSite.site__c) == null){
                        // Delete the CIFSite
                        cSiteToDelete.add(cSiteMap.get(cSite.site__c));
                    }
                }
                }
            }
        }
        
        if (cSiteToCreate!=null && !cSiteToCreate.isEmpty())
            INSERT cSiteToCreate;
        if (cSiteToDelete!=null && !cSiteToDelete.isEmpty())
            DELETE cSiteToDelete;
        UPDATE cifs;
        return true;
    }
   
    webService static String validateCIFSites(String cifId){
        String cifName = '';
        CIF__c cif = [SELECT ID, Name FROM CIF__c WHERE id = :cifId];
        
        List<CIF_Site__c> CIFSites = [SELECT Id, Name, Circuit_Type__c, Interface_Type__c, Router__c, Floor__c, Room__c FROM CIF_Site__c where CIF_Number__c = :cif.Id AND (Circuit_Type__c = '' OR Interface_Type__c = '' OR Router__c = '' OR Floor__c = '' OR Room__c = '')] ;
        
        if (CIFSites != null && CIFSites.size() > 0){
            cifName = CIFSites[0].Name;
        }
        
        return cifName; 
    }
    
    
    webService static String encryptCOP(String COPId){
        String encryptedCOP = '';

        Blob key = Crypto.generateAesKey(128);

        // Generate the data to be encrypted. 
        Blob data = Blob.valueOf(COPId);
        
        // Generate an encrypted form of the data using base64 encoding     
        String b64Data = EncodingUtil.base64Encode(data);
        Blob encryptedData = Crypto.encryptWithManagedIV('AES128', key, data); 
        encryptedCOP = EncodingUtil.base64Encode(encryptedData);
        
        
        EncryptionKey__c eKey = new EncryptionKey__c(Name=encryptedCOP, key__c=EncodingUtil.base64Encode(key), keyEncrypted__c = EncodingUtil.base64Encode(key));
        INSERT eKey;

        encryptedCOP = EncodingUtil.urlEncode(encryptedCOP, 'UTF-8');
        CIF__c c = [SELECT Id, Name, Opportunity_Name__c FROM CIF__c WHERE Id = :COPId];
        Opportunity opp = [SELECT Id, Name, EncryptedCOPId__c FROM Opportunity WHERE Id = :c.Opportunity_Name__c];
        opp.EncryptedCOPId__c = encryptedCOP;
        
        UPDATE opp;
            
        return encryptedCOP;
    }
    
    webService static String sendEmail(String quoteId){
        return null;
    }
    
    
    webService static Integer checkForCIFAttachment(String cifID){
        Integer numAttachments = 0;
        CIF__c cif = [SELECT ID, Name FROM CIF__c WHERE id = :cifId];
        String tempName = cif.Name + '.pdf';
        
        try{
            numAttachments = [SELECT count() FROM Attachment where ParentId = :cifID and Name = :tempName];
        }catch(Exception ex){
            System.debug('Attachment has not been created!');
        }
        
        return (numAttachments); 
    }
    
    webService static String createMandatoryFields(String oppId){
        Opportunity opp = [SELECT Id, Name, AccountId, Type, SyncedQuoteId, Mandatory_COP_Fields_Status__c, OwnerId, Contact__c FROM Opportunity WHERE id = :oppId];
        Id syncedQuote;
        
        // Need to ensure that Quote is synched - we will send back the oppId if there is a problem
        if (opp.SyncedQuoteId == null){
            return oppId;
        }
        
        syncedQuote = opp.SyncedQuoteId; 
        
        // Get all of the Quote Sites
        List<Quote_Site__c> quoteSites = [SELECT Id, Site__c, Site__r.Suite_Floor__c FROM Quote_Site__c WHERE Quote__c = :syncedQuote];
        
        
        List<Mandatory_COP_Fields__c> mandatoryCOPFieldsList = [SELECT Id, Quote_Site__c, Suite_Floor__c FROM Mandatory_COP_Fields__c WHERE Quote_Site__r.Quote__c = :opp.SyncedQuoteId Order By CreatedDate asc];
        Set<Id> mandatoryFieldIds = new Set<Id>();
        for (Mandatory_COP_Fields__c m : mandatoryCOPFieldsList){
            mandatoryFieldIds.add(m.Quote_Site__c);
        }
        
         
        for (Quote_Site__c qs : quoteSites){
            if (!mandatoryFieldIds.contains(qs.Id)){
                Mandatory_COP_Fields__c mFields = new Mandatory_COP_Fields__c();
                mFields.Quote_Site__c = qs.Id;
                mFields.Suite_Floor__c = qs.Site__r.Suite_Floor__c;
                mandatoryCOPFieldsList.add(mFields);
            }   
        }
        
        try{
            
            UPSERT mandatoryCOPFieldsList;
            if (Utils.isEmpty(opp.Mandatory_COP_Fields_Status__c)){
                opp.Mandatory_COP_Fields_Status__c = 'Started';
                UPDATE opp;
            }
        }catch(Exception ex){
            return oppId;
        }
        
        return mandatoryCOPFieldsList[0].Id; 
        
    }
}