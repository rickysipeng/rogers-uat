/*
@Name:  ShiftInlineOLIView_Extension 
@Description: Visualforce extension for the ShiftInlineOLIView page to display the Opportunity Line Items
@Dependancies: 
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-14-2014  | 1.0.0          | Jocsan | Initial
*/
public class ShiftInlineOLIView_Extension {
    
    private Opportunity theOpportunity;
    public list<OpportunityLineItem> theOLIList {get; set;}
    
    public ShiftInlineOLIView_Extension(ApexPages.StandardController controller) {
        // get opportunity records
        theOpportunity = (Opportunity) controller.getRecord();
        
        // get olis
        theGetOLI();
    }

    private void theGetOLI() {
        // query opportunity line items
        theOLIList = new list<OpportunityLineItem>([select Id, UnitPrice, PricebookEntry.Name from OpportunityLineItem where OpportunityId =: theOpportunity.Id]);
        
           
   }
    
    public PageReference theSave() {
        // update olis
        update theOLIList;        
        return null;
    }
    
    public PageReference theCancel() {
        // query olis again as we are not saving anything
        theGetOLI();
        
        return null;
    }    
}