/**************************************************************************************
Apex Class Name     : Test_TAG_DailyDigestEmailController
Version             : 1.0 
Created Date        : 16 Mar 2015
Function            : This is the Test Class for TAG_DailyDigestEmailController.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             03/16/2015              Original Version
*************************************************************************************/
@isTest (seeAllData=false)
private class Test_TAG_DailyDigestEmailController
{
    private static Assignment_Request__c assignReq = new Assignment_Request__c();
    private static  Assignment_Request__c  AccountAR;
    private static  Assignment_Request_Item__c AccountARI;
    private static Assignment_Request_Item__c reqItem;
    private static Assignment_Request_Item__c reqItem2;
    private static Assignment_Request_Item__c reqItem3;
    private static Assignment_Request_Item__c reqItem4;
    private static  User userRec;
    private static  User userRec1;
    private static  User userRec2;
    private static  User userRec3;
    private static  Profile pEmp;
    private static  Account ParentAcc;
    private static  List<ProcessInstanceWorkItem> pwItem;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
    */
    private static void setUpData()
    {     
            List<String> userlist = new List<String>();
        
            //Data for Custom Settings
            TAG_CS.Unassigned_User__c ='Unassigned User';
            TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
            TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
            TAG_CS.TAG_Ownership_Requests__c = 'Account Owner,MSD Owner,Shared MSD Owner,Account District,MSD District,Shared MSD District';
            TAG_CS.TAG_Membership_Requests__c = 'Account Team,MSD Member,Shared MSD Member';
            insert TAG_CS;
          
          
            pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
            system.debug('pEmp--->'+pEmp);
           
            userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
            insert userRec;
            userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment');
            insert userRec2;
    
            userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment');
            insert userRec3;
            system.debug('===userRec1 --->'+userRec1 +'===userRec2 --->'+userRec2);
            
            userlist.add(userRec3.id);
            
            System.RunAs(userRec2)
            {
            
                DateTime currentDay = system.now().AddDays (-1);
                
                //Account 1
                ParentAcc= new Account();
                ParentAcc.Name = 'ParentAcc';
                ParentAcc.Account_Status__c = 'Assigned';
                ParentAcc.ParentID= null;
                ParentAcc.OwnerId= userRec2.id;
                ParentAcc.BillingPostalCode ='A1A 1A1';
                insert ParentAcc;
        
                //Create Assignment request
                assignReq = new Assignment_Request__c();
                assignReq.Request_Type__c = 'Mixed';
                assignReq.Status__c = 'Processed';
                assignReq.Approval_Override__c=false;
                insert assignReq;
        
                reqItem = new Assignment_Request_Item__c();
                reqItem.Assignment_Request__c = assignReq.id;
                reqItem.Account__c = ParentAcc.id;
                reqItem.Approval_Override__c = assignReq.Approval_Override__c;
                reqItem.Business_Case__c = 'Test Business Case';
                reqItem.Current_Approver__c =userRec.Id;
                reqItem.Current_Channel__c = 'Enterprise';
                reqItem.Current_Owner__c= userRec2.id;       
                reqItem.Reason_Code__c = '1 - Channel Alignment to Small';
                reqItem.New_Owner__c= userRec3.id; 
                reqItem.New_Approver__c=userRec2.Id;
                reqItem.New_Channel__c= 'BIS';
                reqItem.Requested_Item_Type__c='Account Owner';
                insert reqItem;
               
                Approval.ProcessSubmitRequest req1 = new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval.');
                req1.setObjectId(reqItem.id);
                Approval.ProcessResult result = Approval.process(req1);
                
                reqItem3 = new Assignment_Request_Item__c();
                reqItem3.Assignment_Request__c = assignReq.id;
                reqItem3.Account__c = ParentAcc.id;
                reqItem3.Business_Case__c = 'Test 2 Business Case';
                reqItem3.Current_Approver__c =userRec.Id;
                reqItem3.Current_Channel__c = 'Enterprise';
                reqItem3.Current_Owner__c= userRec2.id;       
                reqItem3.Reason_Code__c = '1 - Channel Alignment to Small';
                reqItem3.New_Owner__c= userRec3.id; 
                reqItem3.New_Approver__c=userRec2.Id;
                reqItem3.New_Channel__c= 'BIS';
                reqItem3.Requested_Item_Type__c='Account Owner';
                insert reqItem3;
             
                Approval.ProcessSubmitRequest req2 = new Approval.ProcessSubmitRequest();
                req2.setComments('Submitting request for approval.');
                req2.setObjectId(reqItem3.id);
                Approval.ProcessResult result2 = Approval.process(req2);
        
                reqItem2 = new Assignment_Request_Item__c();
                reqItem2.Assignment_Request__c = assignReq.id;
                reqItem2.Account__c = ParentAcc.id;
                reqItem2.Approval_Override__c = assignReq.Approval_Override__c;
                reqItem2.Business_Case__c = 'Test 2 Business Case';
                reqItem2.Reason_Code__c = 'Test 2 Reson code';
                reqItem2.Action__c = 'Remove';
                reqItem2.Requested_Item_Type__c='Account Team';
                reqItem2.Role__C = 'Owner';
                reqItem2.Member__c = userRec2.id;
                reqItem2.Member_Approver__c = userRec2.Assignment_Approver__r.Id;
                reqItem2.Member_Channel__c = userRec2.Channel__c;
                reqItem2.Current_Approver__c=userRec3.Assignment_Approver__r.Id;     
                reqItem2.Current_Channel__c=  userRec3.Channel__c;
                reqItem2.Current_Owner__c= userRec3.id;  
                insert reqItem2;
                Approval.ProcessSubmitRequest req3 = new Approval.ProcessSubmitRequest();
                req3.setComments('Submitting request for approval.');
                req3.setObjectId(reqItem2.id);
                Approval.ProcessResult result3 = Approval.process(req3);
        
                reqItem4 = new Assignment_Request_Item__c();
                reqItem4.Assignment_Request__c = assignReq.id;
                reqItem4.Account__c = ParentAcc.id;
                reqItem4.Approval_Override__c = assignReq.Approval_Override__c;
                reqItem4.Business_Case__c = 'Test 2 Business Case';
                reqItem4.Reason_Code__c = 'Test 2 Reson code';
                reqItem4.Action__c = 'Remove';
                reqItem4.Requested_Item_Type__c='Account Team';
                reqItem4.Role__C = 'Owner';
                reqItem4.Member__c = userRec2.id;
                reqItem4.Member_Approver__c = userRec2.Assignment_Approver__r.Id;
                reqItem4.Member_Channel__c = userRec2.Channel__c;
                reqItem4.Current_Approver__c=userRec3.Assignment_Approver__r.Id;     
                reqItem4.Current_Channel__c=  userRec3.Channel__c;
                reqItem4.Current_Owner__c= userRec3.id;  
                insert reqItem4;
                
                Approval.ProcessSubmitRequest req4 = new Approval.ProcessSubmitRequest();
                req4.setComments('Submitting request for approval.');
                req4.setObjectId(reqItem4.id);
                req4.setNextApproverIds(userlist);
                Approval.ProcessResult result4 = Approval.process(req4);
                
                list<ProcessInstanceWorkitem> newlistPending = new list<ProcessInstanceWorkitem>([Select ProcessInstanceId,ProcessInstance.TargetObjectId,ProcessInstance.TargetObject.Name, ProcessInstance.TargetObject.type, OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById,CreatedBy.Name, ActorId From ProcessInstanceWorkitem where CreatedDate>=:currentDay order by ActorId limit 1]);
                list<ProcessInstanceWorkitem> temp = new list<ProcessInstanceWorkitem>();
                for(ProcessInstanceWorkitem p: newlistPending)
                {
                    p.ActorId = userRec3.id;
                    temp.add(p);
                }
                if(temp.size()>0)
                update temp;
            }
            
    }
    
     private static testMethod void Test_DailyEmail()
    {
             
            Test.startTest();
            setUpData();  
            Database.BatchableContext scc;
            //TAG_DailyDigestEmailController sc = new TAG_DailyDigestEmailController();
            //Database.QueryLocator obj = sc.start(null);
            
            System.debug('pwItem^^^^^^^^^^  '+pwItem);
           
            TAG_DailyDigestEmailController scDaily = new TAG_DailyDigestEmailController();
            TAG_DailyDigestEmailController d = new TAG_DailyDigestEmailController();
            Database.executeBatch(d);
            
            
        /*  sc.query = 'Select ProcessInstanceId,ProcessInstance.TargetObjectId, OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById, ActorId From ProcessInstanceWorkitem where  CreatedDate>=:currentDay and  ProcessInstance.TargetObject.type =\'Assignment_Request_Item__c\' order by ActorId';
            sc.start(scc);
            List<sObjects> scope = ([Select ProcessInstanceId,ProcessInstance.TargetObjectId, OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById, ActorId From ProcessInstanceWorkitem where  CreatedDate>=:currentDay]);
            sc.execute(scc,scope);
            sc.finish(scc);
        
           sc.query ='SELECT Id,Account__c,Role__c,Member__c,Assignment_Request__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ AccountDistrictARI+ '\'  ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,Assignment_Request__c,Role__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
        */
            Test.stopTest();     
            
    }
   


}