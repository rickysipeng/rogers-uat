/*
===============================================================================
 Class Name   : Test_updateAccountTeamAccess
===============================================================================
PURPOSE:    This class a controller class for updateAccountTeamAccess.
              
Developer: Deepika Rawat
Date: 03/11/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/11/2015          Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_updateAccountTeamAccess{
    private static Account ParentAcc;
    private static Account ChildAcc1;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static User userRec2;
    private static User userRec3;
    private static MSD_Code__c msdCode; 
    private static Shared_MSD_Code__c sharedMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD;
    private static Wireline_Contract__c wContract;
    private static Billing_Account__c billingAcc;
    private static MSD__c msdSnapshot;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_Account_Access__c='Read';
        TAG_CS.Team_Custom_Object_Access__c='Read';
        insert TAG_CS;
        
        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment',Owner_Type__c='District');
        insert userRec2;
        userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment');
        insert userRec3;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.BillingPostalCode= 'A1A 1A1';
        insert ChildAcc1;

        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'MSD Member';
        newMem3.AccountId = ParentAcc.Id;
        newMem3.UserId = userRec.Id; 
        insert newMem3;
        
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'SDC';
        newMem.AccountId = ChildAcc1.Id;
        newMem.UserId = userRec.Id; 
        insert newMem;

        msdCode = new MSD_Code__c();
        msdCode.OwnerId = userRec.id ;
        msdCode.Account__c = ChildAcc1.id ;
        msdCode.MSD_Code_External__c = '789';
        insert msdCode;

        sharedMSD = new Shared_MSD_Code__c();
        sharedMSD.OwnerId = userRec.id ;
        sharedMSD.Name = 'SharedMSD1';
        sharedMSD.MSD_Code_External__c = '12345';
        insert sharedMSD;

        sharedAccMSD = new Shared_MSD_Accounts__c();
        sharedAccMSD.Account__c=ChildAcc1.id;
        sharedAccMSD.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD;

        wContract = new Wireline_Contract__c();
        wContract.Name='WCName';
        wContract.Account_Name__c =ChildAcc1.id;
        wContract.Status__c='Activated';
        wContract.Contract_Start_Date__c= system.today();
        wContract.Contract_Term_months__c = 5;
        insert wContract;
        
        billingAcc = new Billing_Account__c();
        billingAcc.Name = 'Test Billing Acc';
        billingAcc.Account__c = ChildAcc1.id;
        insert billingAcc;
        
        msdSnapshot = new MSD__c() ;
        msdSnapshot.AccountId__c = ChildAcc1.id;
        insert msdSnapshot;
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testAddandRemoveAccess(){
        setUpData(); 
        Map<Id, Set<Id>> mapAccountNewUserId = new Map<Id, Set<Id>>();
        Set<Id> userIds = new Set<Id>();
        userIds.add(userRec2.id);
        userIds.add(userRec3.id);
        mapAccountNewUserId.put(ChildAcc1.id,userIds);
        
        test.startTest();
          updateAccountTeamAccess obj = new updateAccountTeamAccess();
          obj.giveAccesstoTeam(mapAccountNewUserId);
          obj.removeAccessFromTeam(mapAccountNewUserId);
        test.stopTest(); 
    }

}