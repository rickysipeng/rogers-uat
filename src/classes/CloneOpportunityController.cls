/* CloneOpportunityController
 * Description  : cloning opportunity and its related prospect sites (Sites__r) list
 *              Called in replacement of the Clone Opportunity button
 *              
 * Objects Used : Opportunity
 *              , Site__c
 * Initial Version: 2012 05 09
 * Last update: 20120511
 * Previous updates: 
 *                  20120509: start
     Jan 23, 2015. Paul Saini. Replace Opportunity.Lost_Comments__c & Reason_Lost__c  with Opportunity.Win_Loss_Description__c.
     As per comments from MP: The code needs to instead reference the field [Win_Loss_Description__c]
     Feb 23, 2015 Removed Signed_Date__c as it is no longer required.
     Feb 24, 2015 Replace Opportunity.Actual_Sign_Date with Opportunity.CloseDate
 */
 

public with sharing class CloneOpportunityController {
    
    private Id sourceOppId; // source opportunity Id, required.
    public Opportunity sourceOpp {get; private set;} // source opportunity, to be cloned
    public Opportunity newOpp {get;set;} 
    public List<Site__c> newSites {get;set;}
    private List<Site__c> sourceSites; // Sites of sourceOpp
    public Boolean sourceOppIsValid {get; private set;}
    
    public CloneOpportunityController(ApexPages.StandardController controller) {
        sourceOppId = ApexPages.currentPage().getParameters().get('oppId'); // oppId as parameter is optional
        if (sourceOppId == null) {
            sourceOppId = controller.getId();
        }
        sourceOppIsValid = true;
        try {
            sourceOpp = [SELECT Id, Name
                , AccountId, Account.Total_MRR__c
                , Account_Record_Type__c
                //, Actual_Sign_Date__c
                , Amount
                , Assigned_Campaign__c
                , Assigned_Telemarketing__c
                , Build_Cost__c
                , CampaignId
                , Channel_Sales_Rep__c
                , CloseDate
                , Close_Month__c
                , Contact__c
                , Contract_Number__c
                , COR_Comments__c
                , COR__c
                //, CreatedById
                //, CreatedDate
            //  , Customer_Facing_COP__c
                , Customer_Order_Reference__c
                , Description
                , Description_Multiple_networks__c
                , EncryptedCOPId__c
                , Estimated_MRC1__c
                , Estimated_MRR__c
                , Estimated_One_Time_Charge1__c
                , Estimated_One_Time_Charge__c
                , Estimated_Total_Contract_Value_TCV1__c
                , Estimated_Total_Contract_Value_TCV__c
                , Existing_MRR__c
                , ExpectedRevenue
                , Expected_Billing_Date__c
                , First_Bill_Date__c
                , Fiscal
                , FiscalQuarter
                , FiscalYear
                , ForecastCategory
                , ForecastCategoryName
                , HasOpportunityLineItem
                //, IsClosed
                //, IsDeleted
                , IsPrivate
                //, IsWon
                //, LastActivityDate
                //, LastModifiedById
                //, LastModifiedDate
                , LeadSource
                , Win_Loss_Description__c
                , Network__c
                , Net_Change_MRC__c
                , Net_Change_MRR__c
                , New_MRR__c
                , New_Term_Months1__c
                , New_Term_Months__c
                , NextStep
                , No_of_Sites__c
                , Number_of_Circuits__c
                , One_Time_Charge1__c
                , One_Time_Charge__c
                , Opportunity_Number__c
                , Opp_Age__c
                , Order_Submission_Date__c
                , Order_Tracker__c
                , OwnerId
                , PartnerAccountId
                , Pre_Approved_Pricing__c
                , Pricebook2Id
                , Pricing__c
                , Probability
                , Product_Category__c
                , Product__c
                , Proposal__c
                , Provider__c
                , Qualify_by__c
                //, Win_Loss_Description__c
                , RecordTypeId
                , Renewal_Notification_Period__c
                , Renewal_TCV__c
                , Renewal_Type__c
                , Service_Requested_Date__c
               // , Signed_Date__c
                , Site_A_City__c
                , Site_A_Postal_Zip_Code__c
                , Site_A_Street_Address_2__c
                , Site_A_Street_Address__c
                , Site_Z_City__c
                , Site_Z_Postal_Zip_Code__c
                , Site_Z_Street_Address_2__c
                , Site_Z_Street_Address__c
                , StageName
    //          , SyncedQuoteId
                , SystemModstamp
                , Term_Months1__c
                , Term_Months__c
                , TotalOpportunityQuantity
                , Type
                , Upsell_Delta_MRC1__c
                , Upsell_Delta_MRR__c
                , Won_MRC1__c
                , Won_MRR__c
                , Won_Total_Contract_Value_TCV1__c
                , Won_Total_Contract_Value_TCV__c 
                , Parent_Opportunity__c
                , Lead_Rep_Type__c
                , Referral_Agent_ID__c
                , Referral_Agent_Email__c
                , Referral_Agent_First_Name__c
                , Referral_Agent_Last_Name__c
                , Referral_Phone__c
                , Referral_Salutation__c
                , ( SELECT Access_Class__c
                        , Access_Provider__c, Access_Type_Group__c, Access_Type__c
                        , Account__c
                        , Approved__c
                        , City__c
                        , CLLI_SWC__c
                        , Country__c
                        , CreatedById, CreatedDate
                        , Display_Name__c
                        , Id
                        //, IsDeleted
                        , Is_a_Z_Site__c
                        , LastModifiedById, LastModifiedDate
                        , LocationKey_del__c
                        , Name
                        //, Notes__c
                        , Opportunity__c
                        //, OwnerId
                        , Postal_Code__c
                        , Proposal_Display_Name__c
                        , Province_Code__c
                        , ServiceableLocation__c
                        , Serviceable__c
                        , Services__c
                        , Service_Address__c
                        , Street_Direction__c
                        , Street_Name__c, Street_Number__c, Street_Type__c
                        , Suite_Floor__c
                        , SystemModstamp
                        , Type__c
                        //, UniqueKey__c // should be calculated on insert
                        , Z_Site__c // this will not be used again
                        , Suite_N_A__c
                        
                    FROM Sites__r )
                FROM Opportunity WHERE Id = : sourceOppId];
             System.debug('-----------sourceOppId-------'+sourceOppId);   
            //System.debug('\n\n\n\n\n sourceOpp: ' + sourceOpp);
            newOpp = sourceOpp.clone(false, false); // opt_preserve_id = false, IsDeepClone = true
            newOpp.Name = newOpp.Name + '- Clone';
            newOpp.OwnerId = UserInfo.getUserId(); // owner should be the current user
            newOpp.StageName = 'Suspect';
            newOpp.Probability = 0;
            newOpp.Parent_Opportunity__c = sourceOpp.Parent_Opportunity__c;
            newOpp.Lead_Rep_Type__c = sourceOpp.Lead_Rep_Type__c; 
            newOpp.Referral_Agent_ID__c = sourceOpp.Referral_Agent_ID__c; 
            newOpp.Referral_Agent_Email__c = sourceOpp.Referral_Agent_Email__c; 
            newOpp.Referral_Agent_First_Name__c = sourceOpp.Referral_Agent_First_Name__c; 
            newOpp.Referral_Agent_Last_Name__c = sourceOpp.Referral_Agent_Last_Name__c; 
            newOpp.Referral_Phone__c = sourceOpp.Referral_Phone__c; 
            newOpp.Referral_Salutation__c = sourceOpp.Referral_Salutation__c; 
        } catch (Exception ex) {
            sourceOppIsValid = false;
            ApexPages.addMessages(ex);
            ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR
                , 'Please call this clone functionality from a valid opportunity.'));
            
        }
    }
    
    // 
    public PageReference returnToSourceOpportunity() {
        PageReference oppPage = new ApexPages.StandardController(sourceOpp).View();
        oppPage.setRedirect(true);
        return oppPage;
    }
    
    
    // Cloning this opportunity will insert a new opportunity record
    // and its new Prospect Site (Site__c)
    public PageReference cloneOpportunity() {
        Savepoint sp = Database.setSavepoint();
        
        try{
            insert newOpp;
            cloneSourceSites();
            PageReference oppPage = new ApexPages.StandardController(newOpp).View();
            oppPage.setRedirect(true);
            
            return oppPage;
        } catch (Exception ex) {
            Database.Rollback(sp); 
            ApexPages.addMessages(ex);
            return null;
        }
    }
    
    
    // clone Opportunity.Site__r, 
    // but prevent readding those Site__c that has been created during newOpp insertion
    private void cloneSourceSites()
    {
        // check currently site on the newOpp (the clone) which were created during opp insertion:
        //List<Site__c> existingSiteList = [select Id, Name, UniqueKey__c
        //  from Site__c where Opportunity__c = : newOpp.Id ];
        
        Map<Id, Site__c> existingIdToSiteMap = new Map<Id, Site__c>([
            select Id, Name, UniqueKey__c
            from Site__c where Opportunity__c = : newOpp.Id ]);
        System.debug('-----------newOpp-------'+newOpp.Id+'-----'+sourceOppId);
        //Set<String> existingUniqueKeys = new Set<String>();
        Set<String> existingSiteNames = new Set<String>();
        
        Map<String, Site__c> newSiteNameToObjectMap = new Map<String, Site__c>();
        for ( Site__c savedSite : existingIdToSiteMap.values() )
        {
            //existingUniqueKeys.add(savedSite.UniqueKey__c);
            existingSiteNames.add(savedSite.Name);
            newSiteNameToObjectMap.put(savedSite.Name, savedSite);
        }
        
        // sourceSiteIdToObjectMap will be used in 'cloning' the A_Z_Site records
        Map<Id, Site__c> sourceSiteIdToObjectMap = new Map<Id, Site__c>();
        
        sourceSites = sourceOpp.Sites__r;
        List<Site__c> newSites = new List<Site__c> ();
        
        // clone all non existing sites, A or Z will be put on the same list
        for (Site__c srcSite : sourceSites)
        {
            sourceSiteIdToObjectMap.put(srcSite.Id, srcSite);
            /*
            String thisUniqueKey = srcSite.Suite_Floor__c 
                + ' ' + srcSite.Street_Number__c + ' ' + srcSite.Street_Name__c 
                + ' ' + srcSite.Street_Type__c + ' ' + srcSite.Street_Direction__c 
                + ' ' + srcSIte.City__c + ' ' + srcSite.Province_Code__c 
                + ' ' + srcSite.Postal_Code__c + ' ' + srcSite.opportunity__c;
            // f (! existingUniqueKeys.contains(thisUniqueKey))
            */
            
            if (! existingSiteNames.contains(srcSite.Name))
            {   
                Site__c newSite = srcSite.clone(false, false);
                newSite.Opportunity__c = newOpp.Id;
                newSiteNameToObjectMap.put(newSite.Name, newSite);
                
                newSites.add(newSite);
            }
        }
        
        insert newSites;
        
        Set<Id> siteIds = new Set<Id>(); // existing & new Site__c.Id
        siteIds.addAll(existingIdToSiteMap.keySet());
        for (Site__c newSite : newSites) {
            siteIds.add(newSite.Id);
        }
        
        // get the A_Z Site Mapping
        List<A_Z_Site__c> existingAZSiteList = [select Id, Site_A__c, Site_Z__c
            from A_Z_Site__c 
            where Site_A__c in : siteIds 
                and IsDeleted = false
            ];
        Set<String> existingAZIdPairSet = new Set<String>();
        for (A_Z_Site__c existingAZ : existingAZSiteList){
            String existingPair = existingAZ.Site_A__c;
            existingPair += existingAZ.Site_Z__c;
            existingAZIdPairSet.add(existingPair);
        }
            
        List<A_Z_Site__c> sourceAZSiteList = [select Id, Site_A__c, Site_Z__c
            from A_Z_Site__c 
            where Site_A__c in : sourceSiteIdToObjectMap.keySet() 
                and IsDeleted = false
            ];
        
        
        List<A_Z_Site__c> newAZSiteList = new List<A_Z_Site__c>(); // new map of Site_A and Site_Z
    
        // create Z SIte
        for ( A_Z_Site__c azSite : sourceAZSiteList) {
            Id newASiteId = newSiteNameToObjectMap.get(sourceSiteIdToObjectMap.get(azSite.Site_A__c).Name).Id;
            //System.assertNotEquals( null, newASiteId);
            Id newZSiteId = newSiteNameToObjectMap.get(sourceSiteIdToObjectMap.get(azSite.Site_Z__c).Name).Id;
            
            //System.debug ('xxxx Map: ' 
            //  + newASiteId + ':' + sourceSiteIdToObjectMap.get(azSite.Site_A__c).Name
            //  + newZSiteId + ':' + sourceSiteIdToObjectMap.get(azSite.Site_Z__c).Name + '\n\n');
                
            String newPair = newASiteId;
            newPair += newZSiteId;
            if (!existingAZIdPairSet.contains(newPair))
            { 
                A_Z_Site__c newAZSite = new A_Z_Site__c ( Site_A__c = newASiteId
                                                    , Site_Z__c = newZSiteId );
                newAZSiteList.add(newAZSite);
            }
        }
        insert newAZSiteList;
        
    }
    
    
    // ==============================================================
    // basic test method and supporting methods are written below
    // ==============================================================
    
   /* private static testMethod void testMe()
    {
        Opportunity oppTestObj;
        Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();      
        //Add ( In SetupData or where Before Account is getting Created) :
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
        //insert AccountaccTestObj
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act 23';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Street'; a_carrier.Account_Status__c = 'Assigned';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        insert a_carrier;
        //insert Opportunity oppTestObj
        oppTestObj = new Opportunity(Name='testAcc');
        oppTestObj.StageName='Cloased Won';
        oppTestObj.CloseDate=system.today();
        oppTestObj.AccountID=a_carrier.id;
        oppTestObj.ownerid =listUser[0].id;
        insert oppTestObj;
        List<Site__c> listSite = new List<Site__c>();
        
        List<Site__c> sites1 = new List<Site__c>();
        for (Integer i=0; i<6; ++i){
            Site__c site = new Site__c();
            site.Suite_Floor__c = '11' + (i+1);
            site.Street_Name__c = 'Somewhere' + (i+1);
            site.Street_Number__c  = '' + (i+1);
            site.City__c = 'City';
            site.Postal_Code__c = 'A1A1A1';
            site.Province_Code__c = 'ON';
            site.CLLI_SWC__c = '123113';
            site.Opportunity__c = oppTestObj.id;
            sites1.add(site);
        }
        insert sites1;
    }*/
    
    private static testMethod void testMe1()
    {   
        Opportunity oppTestObj;
        Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();      
        //Add ( In SetupData or where Before Account is getting Created) :
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
        //insert AccountaccTestObj
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act 23';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Street'; a_carrier.Account_Status__c = 'Assigned';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        insert a_carrier;
        //insert Opportunity oppTestObj
        oppTestObj = new Opportunity(Name='testAcc');
        oppTestObj.StageName='Cloased Won';
        oppTestObj.CloseDate=system.today();
        oppTestObj.AccountID=a_carrier.id;
        oppTestObj.ownerid =listUser[0].id;
        insert oppTestObj;
        List<Site__c> listSite = new List<Site__c>();
        List<Site__c> sites1 = new List<Site__c>();
        for (Integer i=0; i<6; ++i){
            Site__c site = new Site__c();
            site.Suite_Floor__c = '11' + (i+1);
            site.Street_Name__c = 'Somewhere' + (i+1);
            site.Street_Number__c  = '' + (i+1);
            site.City__c = 'City';
            site.Postal_Code__c = 'A1A1A1';
            site.Province_Code__c = 'ON';
            site.CLLI_SWC__c = '123113';
            site.Opportunity__c = oppTestObj.id;
            sites1.add(site);
        }
        insert sites1;
        ApexPages.StandardController sc = new ApexPages.standardController(oppTestObj);
        CloneOpportunityController cloneController = new CloneOpportunityController(sc);
        PageReference pageRef = Page.CloneOpportunity_RDC;
        ApexPages.currentPage().getParameters().put('oppId',oppTestObj.Id);
        Test.setCurrentPage(pageRef);
        //System.assertEquals(false, cloneController.sourceOppIsValid);
        pageRef = cloneController.cloneOpportunity();
        pageRef = cloneController.returnToSourceOpportunity();
        List<Site__c> sites2 = new List<Site__c>();
        for (Integer i=0; i<5; ++i){
            Site__c site = new Site__c();
            site.Suite_Floor__c = '11' + (i+1);
            site.Street_Name__c = 'Somewhere' + (i+1);
            site.Street_Number__c  = '' + (i+1);
            site.City__c = 'City';
            site.Postal_Code__c = 'A1A1A1';
            site.Province_Code__c = 'ON';
            site.CLLI_SWC__c = '123113';
            site.Opportunity__c = cloneController.newOpp.id;
            sites2.add(site);
        }
        pageRef = cloneController.cloneOpportunity();
        pageRef = cloneController.returnToSourceOpportunity();
    }
}