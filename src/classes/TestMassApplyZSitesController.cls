/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestMassApplyZSitesController {

    static Id oppId, opp2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, sl1Id, sl2Id, sl3Id, sl4Id, qRequestId, accountId, quoteId;
    static Boolean withAssertions = true;

    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        Account a = new Account();
        a.name = 'Mass Apply Z Sites Test Act';
        a.Business_Segment__c = 'Alternate';
        //a.RecordTypeId = mapRTa.get('New Account');
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.parentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        accountId = a.Id;
        
        Opportunity opp = new Opportunity();
        opp.Estimated_MRR__c = 500;
        opp.Name = 'Mass Apply Z Sites Test Opp';
        opp.StageName = 'Suspect - Qualified';
        opp.Product_Category__c = 'Local';
        opp.Network__c = 'Cable';
        opp.Estimated_One_Time_Charge__c = 500;
        opp.New_Term_Months__c = 5;
        opp.AccountId = a.id;
        opp.Probability = 10;
        //opp.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp.CloseDate = date.today();
        insert opp;
        oppId = opp.id;
        
        Opportunity opp2 = new Opportunity();
        opp2.Estimated_MRR__c = 500;
        opp2.Name = 'Mass Apply Z Sites Test Opp2';
        opp2.StageName = 'Suspect - Qualified';
        opp2.Product_Category__c = 'Local';
        opp2.Network__c = 'Cable';
        opp2.Estimated_One_Time_Charge__c = 500;
        opp2.New_Term_Months__c = 5;
        opp2.AccountId = a.id;
        opp2.Probability = 10;
        //opp2.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp2.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        opp2.CloseDate = date.today();
        insert opp2;
        opp2Id = opp2.id;
        
        Quote q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId);
        insert q;
        quoteId = q.Id;
        
        ServiceableLocation__c sl1 = new ServiceableLocation__c();
        sl1.Name = '1Somewhere1AveCityON123113';
        sl1.Street_Name__c = 'Somewhere1';
        sl1.Street_Number__c  = '1';
        sl1.Street_Type__c  = 'Ave';
        sl1.City__c = 'City';
        sl1.Province_Code__c = 'ON';
        sl1.CLLI_Code__c = '123113';
        sl1.Access_Type_Group__c = 'NNI';
        insert sl1;
        sl1Id = sl1.id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Name = '2Someplace1AveCityON223113';
        sl2.Street_Name__c = 'Someplace1';
        sl2.Street_Number__c  = '2';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'City';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '223113';
        sl2.Access_Type_Group__c = 'NNI';
        insert sl2;
        sl2Id = sl2.id;
        
        ServiceableLocation__c sl3 = new ServiceableLocation__c();
        sl3.Name = '3Someplace1AveCityON223113';
        sl3.Street_Name__c = 'Someplace1';
        sl3.Street_Number__c  = '3';
        sl3.Street_Type__c  = 'Ave';
        sl3.City__c = 'City';
        sl3.Province_Code__c = 'ON';
        sl3.CLLI_Code__c = '223114';
        sl3.Access_Type_Group__c = 'CNI';
        insert sl3;
        sl3Id = sl3.id;
        
        ServiceableLocation__c sl4 = new ServiceableLocation__c();
        sl4.Name = '4Someplace1AveCityON223113';
        sl4.Street_Name__c = 'Someplace1';
        sl4.Suite_Floor__c  = 'A';
        sl4.Street_Number__c  = '4';
        sl4.Street_Type__c  = 'Ave';
        sl4.Street_Direction__c  = 'E';
        sl4.City__c = 'City';
        sl4.Province_Code__c = 'ON';
        sl4.CLLI_Code__c = '223115';
        sl4.Access_Type_Group__c = 'Fibre';
        insert sl4;
        sl4Id = sl4.id;
    }   
    
    private static void setupSites(){
        
        Site__c s1 = new Site__c();
        s1.Suite_Floor__c = '11a';
        s1.Street_Name__c = 'Somewhere1';
        s1.Street_Number__c  = '4';
        s1.City__c = 'City';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        s1.Is_a_Z_Site__c = true;
        insert s1;
        site1Id = s1.id;
                
        Site__c s2 = new Site__c();
        s2.Suite_Floor__c = '11a';
        s2.Street_Name__c = 'Someplace2';
        s2.Street_Number__c  = '5';
        s2.City__c = 'City';
        s2.Postal_Code__c = 'A1A1A2';
        s2.Province_Code__c = 'ON';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.id;
        
        Site__c s3 = new Site__c();
        s3.Suite_Floor__c = '11a';
        s3.Street_Name__c = 'Somewhere3';
        s3.Street_Number__c  = '5';
        s3.City__c = 'City';
        s3.Postal_Code__c = 'A1A1A3';
        s3.Province_Code__c = 'ON';
        s3.ServiceableLocation__c = sl3Id;
        s3.Opportunity__c = oppId;
        s3.Is_a_Z_Site__c = true;
        insert s3;
        site3Id = s3.id;
                
        Site__c s4 = new Site__c();
        s4.Suite_Floor__c = '11a';
        s4.Street_Name__c = 'Someplace4';
        s4.Street_Number__c  = '5';
        s4.City__c = 'City';
        s4.Postal_Code__c = 'A1A1A4';
        s4.Province_Code__c = 'ON';
        s4.ServiceableLocation__c = sl4Id;
        s4.Opportunity__c = oppId;
        insert s4;
        site4Id = s4.id;
        
        Site__c s5 = new Site__c();
        s5.Suite_Floor__c = '11a';
        s5.Street_Name__c = 'Somewhere5';
        s5.Street_Number__c  = '5';
        s5.City__c = 'City';
        s5.Postal_Code__c = 'A1A1A5';
        s5.Province_Code__c = 'AB';
        s5.ServiceableLocation__c = null;
        s5.Opportunity__c = oppId;
        s5.Is_a_Z_Site__c = true;
        insert s5;
        site5Id = s5.id;
                
        Site__c s6 = new Site__c();
        s6.Suite_Floor__c = '11a';
        s6.Street_Name__c = 'Someplace6';
        s6.Street_Number__c  = '6';
        s6.City__c = 'City6';
        s6.Postal_Code__c = 'A1A1A6';
        s6.Province_Code__c = 'AB';
        s6.ServiceableLocation__c = null;
        s6.Opportunity__c = oppId;
        insert s6;
        site6Id = s6.id;
          
    }

    private static void createQuoteRequest(){
        
        Quote_Request__c qRequest = new Quote_Request__c();
        qRequest.Account__c = accountId;
        qRequest.Opportunity__c = oppId;
        qRequest.Quote__c = quoteId;
        
        INSERT qRequest;
        qRequestId = qRequest.Id;
       
    }

    static testMethod void testQuoteRequest(){
        Test.StartTest();
        setupSites();
        createQuoteRequest();
       
        PageReference pageRef = new PageReference('/apex/massApplyZSite?retUrl=' + String.valueOf(oppId)+'&oppId=' + String.valueOf(oppId)+ '&qRequestId='+String.valueOf(qRequestId));
        
        Test.setCurrentPage(pageRef);
       
        MassApplyZSitesController controller = new MassApplyZSitesController();
       
        // 1, 3 and 5 are z Sites
        // 2, 4 and 6 are not z Sites.
        for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == site2Id) || (s.Site.Id == site4Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        
        for (SiteWrapper s : controller.zSites){
            if (s.Site.Id == site1Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  

       controller.updateZSites();
    
    
       for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == site2Id) || (s.Site.Id == site4Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        
        for (SiteWrapper s : controller.zSites){
            if (s.Site.Id == site1Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  


       controller.addSites();
       controller.loadQRequestsLocations();
       
       for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == site2Id) || (s.Site.Id == site4Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        
        for (SiteWrapper s : controller.zSites){
            if (s.Site.Id == site1Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  


       controller.removeSites();
       
       controller.returnToOpportunity();
       controller.returnToQuoteRequest();
       Test.StopTest();
    }


    static testMethod void SearchTest() {
        Test.StartTest();
       setupSites();
       createQuoteRequest();
       
       PageReference pageRef = new PageReference('/apex/massApplyZSite?retUrl=' + String.valueOf(oppId)+'&oppId=' + String.valueOf(oppId));
        
       Test.setCurrentPage(pageRef);
       
       MassApplyZSitesController controller = new MassApplyZSitesController();
       
       if (withAssertions){
        System.assertEquals(controller.zSites.size(), 3);
        System.assertEquals(controller.getProspectSites().size(), 3);
       }
       
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('provinceCode', 'AB');
    
        controller.runSearch();
        
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            
            System.assertEquals(test.size(), 1);
            System.assertEquals(test.contains(site6Id), true);
        }
        
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', 'Someplace');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('provinceCode', 'ON');
    
        controller.runSearch();
        
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 2);
            System.assertEquals(test.contains(site2Id), true);
            System.assertEquals(test.contains(site4Id), true);
        }
        
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
    
        controller.runSearch();
        
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 3);
        }
        
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', 'City6');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
    
        controller.runSearch();
        
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 1);
            System.assertEquals(test.contains(site6Id), true);
        }
        
        Apexpages.currentPage().getParameters().put('streetNumber', '5');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', '');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
    
        controller.runSearch();
        
        if (withAssertions){
            Set<Id> test = new Set<Id>();
            boolean found = false;
            for (SiteWrapper s : controller.getProspectSites()){
                test.add(s.Site.Id);
            }
            System.assertEquals(test.size(), 2);
            System.assertEquals(test.contains(site2Id), true);  
            System.assertEquals(test.contains(site4Id), true);
        }
        
        controller.returnToOpportunity();
       Test.StopTest();
    }
    
    static testMethod void addZSitesTest() {
        Test.StartTest();
        setupSites();
       PageReference pageRef = new PageReference('/apex/massApplyZSite?retURL=' + String.valueOf(oppId)+'&oppId=' + String.valueOf(oppId));
       Test.setCurrentPage(pageRef);
       
       MassApplyZSitesController controller = new MassApplyZSitesController();
       
        Apexpages.currentPage().getParameters().put('streetNumber', '');
        Apexpages.currentPage().getParameters().put('streetName', '');
        Apexpages.currentPage().getParameters().put('city', 'City');
        Apexpages.currentPage().getParameters().put('provinceCode', '');
       
        if (withAssertions){
            System.assertEquals(controller.zSites.size(), 3);
            System.assertEquals(controller.getProspectSites().size(), 3);
        }
       
        for (SiteWrapper s : controller.getProspectSites()){
            if ((s.Site.Id == site4Id) || (s.Site.Id == site6Id)){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        }  
        
 
        // Check what happens if no z sites are selected
        controller.runSearch();
        controller.updateZSites();
        
        Map<Id, Id> test1 = new Map<Id, Id>();
        for (SiteWrapper sw : controller.getProspectSites()){
            if (sw.Site.Z_Site__c != null){
                test1.put(sw.Site.Id, sw.Site.Z_Site__c);
            }
        }
        
        System.assertEquals(test1.size(), 0);
        
        // Now we have a z site selected.
        for (SiteWrapper z : controller.zSites){
            if (z.Site.Id == site1Id){
                z.setIsSelected(true);
            }else{
                z.setIsSelected(false);
            }
        } 
        
        controller.updateZSites();
        
        test1 = new Map<Id, Id>();
        for (SiteWrapper sw : controller.getProspectSites()){
            if (sw.Site.Z_Site__c != null){
                test1.put(sw.Site.Id, sw.Site.Z_Site__c);
            }
        }
        
        
        // Clear the ZSites for all but site4
        for (SiteWrapper s : controller.getProspectSites()){
            if (s.Site.Id != site4Id){
                s.setIsSelected(true);
            }else{
                s.setIsSelected(false);
            }
        } 
        
        controller.removeZSites();
        
        test1 = new Map<Id, Id>();
        for (SiteWrapper sw : controller.getProspectSites()){
            if (sw.Site.Z_Site__c != null){
                test1.put(sw.Site.Id, sw.Site.Z_Site__c);
            }
        }
        Test.StopTest();
    }
    
    static testMethod void noSitesTest() {
        Test.StartTest();
       PageReference pageRef = New PageReference('/apex/massApplyZSite?retURL=' + String.valueOf(opp2Id)+'&oppId=' + String.valueOf(opp2Id)); 
       Test.setCurrentPage(pageRef);
       
       MassApplyZSitesController controller = new MassApplyZSitesController();
       
       if (withAssertions){
        System.assertEquals(controller.zSites.size(), 0);
        System.assertEquals(controller.getProspectSites().size(), 0);
       }
       Test.StopTest();
    }
    
    
    
    
    
}