public with sharing class shift_qli_replication_extension {

    /*
    Quote page displays a 'Replicate' button that, when clicked, will prompt user to enter a new quote #.
    When quote # is entered, a new quote is created with all member values of the source quote copied over.
    System performs a page redirection to the new quote.
    25/10/2012 by Jocsan Diaz
    */

    public Quote__c theQuote {set; get;} // source quote
    public list<Quote_Line_Item__c> theSourceQLIList;  // source quote line items
    private Quote__c theTargetQuote; // target quote
    public string theTargetQuoteName {set; get;} // target quote name field

    public shift_qli_replication_extension(ApexPages.StandardController controller) {
        theQuote = (Quote__c) controller.getRecord();
        
        // get source line items
        map<String, Schema.Sobjectfield> QLIDescribe = new map<String, Schema.Sobjectfield>();
        QLIDescribe = Quote_Line_Item__c.sObjectType.getDescribe().fields.getMap();
        string theQuery = 'select ';
        
        // build query string
        for(String s : QLIDescribe.keyset()) {
            if (s == 'Id') continue;
            
            Schema.DescribeFieldResult dfr = QLIDescribe.get(s).getDescribe();
            if (dfr.isCreateable()) theQuery += dfr.getName() + ', ';
        }        
        
        Id theQuoteId = theQuote.Id;
        theQuery += ' Id from Quote_Line_Item__c where Quote__c =: theQuoteId order by Line__c asc';
        system.debug(theQuery);
        
        // query line items
        theSourceQLIList = Database.query(theQuery);        
    }

    public list<Quote_Line_Item__c> gettheSourceQLIList() {
        return theSourceQLIList;
    }
    
    public PageReference theReplicate() {
        // validate quote name entered
        if ((theTargetQuoteName == null) || (theTargetQuoteName == '')) {
            showErrorMessage('Please enter a valid Quote Name.');
            return null;
        }
        
        // validate again querying Salesforce
        try {
            theTargetQuote = [select Id, Opportunity_Name__r.Account.Customer_Number__c from Quote__c where Name =: theTargetQuoteName];
        } catch (Exception e) {
            showErrorMessage('No target Quote can be found based on the name entered.');
            return null;
        }    
        
        // get source quote customer number
        theQuote = [select Id, Opportunity_Name__r.Account.Customer_Number__c from Quote__c where Id =: theQuote.Id];
        
        // get latest line number from target quote
        list<Quote_Line_Item__c> theTargetQLIList = new list<Quote_Line_Item__c>([select Id from Quote_Line_Item__c where Quote__c =: theTargetQuote.Id and Parent_Quote_Line_Item__c = null]);
        integer theLastLineNumber = theTargetQLIList.size();
                       
        // clone line items from quote
        map<Id, Quote_Line_Item__c> theCLoneQLIMap = new map<Id, Quote_Line_Item__c>();
        system.debug(theSourceQLIList);
        for (Quote_Line_Item__c qli : theSourceQLIList) {
            if ((qli.From_Datafill__c) && (theQuote.Opportunity_Name__r.Account.Customer_Number__c != theTargetQuote.Opportunity_Name__r.Account.Customer_Number__c)) continue;
            theCLoneQLIMap.put(qli.Id, qli.clone());
        }
        system.debug(theCLoneQLIMap.values());
        system.debug(theLastLineNumber);
                
        // reassign line items to target quote
        list<Quote_Line_Item__c> theParentList = new list<Quote_Line_Item__c>();
        list<Quote_Line_Item__c> theChildrenList = new list<Quote_Line_Item__c>();        
        for (Quote_Line_Item__c qli : theCLoneQLIMap.values()) {
            qli.Quote__c = theTargetQuote.Id;
            system.debug(qli.Line__c);
            if (qli.Parent_Quote_Line_Item__c == null) {
                system.debug(qli.Line__c);
                system.debug(theLastLineNumber);                
                qli.Line__c = qli.Line__c + theLastLineNumber; // add line items to the end of the list
                //theLastLineNumber++; // increment one for the next parent line item
                
                theParentList.add(qli);
            } else {
                theChildrenList.add(qli);
            }   
        }
        
        // insert clone line items
        try {
            insert theParentList;
        } catch (Exception e) {
            showErrorMessage('An error has occured replicating line items. Error: ' + e.getMessage());
            return null;
        }
        
        // reparent line items
        for (Quote_Line_Item__c qli :  theChildrenList) {
            if (qli.Parent_Quote_Line_Item__c != null) qli.Parent_Quote_Line_Item__c = theCLoneQLIMap.get(qli.Parent_Quote_Line_Item__c).Id;
        }
        
        // update lines with parent information
        try {
            insert theChildrenList;
        }  catch (Exception e) {
            showErrorMessage('An error has occured replicating line items. Error: ' + e.getMessage());
            return null;
        }
        
        // get to target quote
        PageReference pageRef = new PageReference('/' + theTargetQuote.Id);
        return pageRef;
    }
    
    private void showErrorMessage(String errorMessage) {      
        ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.ERROR, errorMessage));
    }  
    
    @isTest(seeAllData=true)
    static void shift_qli_replication_extensionTest(){
        
        // create dummy account
        Account a = new Account(Name='Shift Account', BillingPostalCode = 'A1A 1A1');
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='Shift Opportunity', Has_quote__c = false, AccountId = a.Id, CloseDate = Date.today(), Amount = 500, StageName = 'Dormant');
        insert o;
        
        // create test quotes without lineitems
        Quote__c q1 = new Quote__c(Opportunity_Name__c = o.id);
        insert q1;
        q1 = [select Name from Quote__c where Id =: q1.Id];

        Quote__c q2 = new Quote__c(Opportunity_Name__c = o.id);
        insert q2;
        q2 = [select Name from Quote__c where Id =: q2.Id];
        
        // create controller plus extension
        ApexPages.Standardcontroller con = new ApexPages.Standardcontroller(q1);
        shift_qli_replication_extension ext = new shift_qli_replication_extension(con);
        
        // assert no line items found
        system.assert(ext.gettheSourceQLIList().size() == 0);
        system.debug(ext.theReplicate() == null);
        
        // add a product
        Product2 p1 = new Product2(Name = 'Test product',   Period__c = '1');
        insert p1;
        
        // add line items to q1
        Quote_Line_Item__c qli1 = new Quote_Line_Item__c(Name = 'test', Quantity__c = 1, Price__c = 1.0, Type__c = 'Custom', Line__c = 1, Quote__c = q1.Id, Product__c = p1.Id, Period__c = '1');
        insert qli1;
        
        // create controller plus extension
        con = new ApexPages.Standardcontroller(q1);
        ext = new shift_qli_replication_extension(con);     
        
        // assert line items found
        system.assertEquals(ext.gettheSourceQLIList().size(), 1);
        system.assert(ext.theReplicate() == null); // no target quote defined   
        
        // set a target quote name
        ext.theTargetQuoteName =  '123';    
        system.assert(ext.theReplicate() == null); // target quote defined but invalid
        
        // set a valid target quote name
        system.assertEquals(0, [select count() from Quote_Line_Item__c where quote__c =: q2.Id]);
        ext.theTargetQuoteName = q2.Name;   
        system.assert(ext.theReplicate() != null); // target quote defined and invalid  
        system.assertEquals(1, [select count() from Quote_Line_Item__c where quote__c =: q2.Id]);   
    }       
}