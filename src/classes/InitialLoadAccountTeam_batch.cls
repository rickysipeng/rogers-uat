/*
===============================================================================
 Class Name   : InitialLoadAccountTeam_batch
===============================================================================
PURPOSE:    This batch class will run after initial Account load. This is to 
            create AccountTeamMembers on Child accounts as in Parent Account.
  

Developer: Deepika Rawat
Date: 02/24/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
02/24/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class InitialLoadAccountTeam_batch implements Database.Batchable<SObject>{
    global String query = 'select id, ParentId, Account_Team_to_MAL__c,OwnerId, District__c FROM Account where ParentId=null';
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('select id, ParentId, Account_Team_to_MAL__c,OwnerId, District__c, Parent.District__c, Parent.OwnerId FROM Account where ParentId=null limit 10');
        }     
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Set<Id> ParentAccIds = new Set<Id>();
        List<Account> childAccToUpdate = new List<Account>();
        for(Account parentAcc :scope){
           ParentAccIds.add(parentAcc.Id);
        }
        List<Account> lstChildAccounts = [Select id, ParentId,OwnerId,District__c,Parent.District__c,Account_Owner_to_MAL__c, Account_Team_to_MAL__c, Parent.OwnerId from Account where ParentId in : ParentAccIds];
        for(Account childAcc :lstChildAccounts){
        childAcc.Account_Team_to_MAL__c=true;
            if(childAcc.Parent.District__c!=null)
               childAcc.District__c = childAcc.Parent.District__c;
            if(childAcc.Parent.OwnerId!=null){
               childAcc.OwnerId= childAcc.Parent.OwnerId; 
               childAcc.Account_Owner_to_MAL__c=true;
            }
            childAccToUpdate.add(childAcc);
        }
        
        
        /*Call syncAccountTeamMembers to create Team Members on Child accounts. 
        Note* This method will first delete AccountTeamMembers from child, if any and then create new team as in Parent Account*/
        TAG_SyncAccountTeamMembers obj = new TAG_SyncAccountTeamMembers();
        obj.addTeamMembersTraversingFromParent(ParentAccIds);
        
        if(childAccToUpdate.size()>0){
            update childAccToUpdate;
        }
        
    }
    global void finish(Database.BatchableContext BC){
    }
}