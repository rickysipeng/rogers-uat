/*
This schedular class runs the sharedMsdCodeSharing_batch class with a batch of 200 records.
            
Written by: Jacob Klay, Rogers Communications
Date: 10-Mar-2014 09:00 EST

***Revisions***
Date        |Name               |Description

*/
global class sharedMsdCodeSharing_schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
       
        sharedMsdCodeSharing_batch sc = new sharedMsdCodeSharing_batch();
        ID batchprocessid = Database.executeBatch(sc,150);
        system.debug('sc*************' +sc);
    }

}