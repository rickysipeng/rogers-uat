/**************************************************************************************
Apex Class Name     : Test_InitialLoadAccountTeam_batch
Version             : 1.0 
Created Date        : 19 Mar 2015
Function            : This is the Test Class for InitialLoadAccountTeam_batch.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             03/19/2015              Original Version
*************************************************************************************/
@isTest
private class Test_InitialLoadAccountTeam_batch
{  
    private static  User UserRec1;
    private static  Account AccountParent;
    private static  Account AccountChild;
    private Static Profile pEmp = [Select Id from Profile where name like '%Rogers%' limit 1];
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  

    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
    */
    private static void setUpData()
    {       
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;        
        
        userRec1 = new User(LastName = 'Mark O’BrienRoger',Owner_Type__c = 'District', Alias = 'alRoger2', Email='test1@Rogertest.com', Username='test1@Rogertest.com', CommunityNickname = 'nickRoger2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec1;
        
        AccountParent = new Account();
        AccountParent.Name = 'Test_account_parent_with_child';
        AccountParent.Account_Status__c = 'Assigned';
        AccountParent.ParentId = null;
        AccountParent.Ownerid = userRec1.id;
        insert AccountParent; 
        
        AccountChild = new Account();
        AccountChild.Name = 'Test_only_child';
        AccountChild.ParentId = AccountParent.id;
        AccountChild.Account_Team_to_MAL__c=true;
        AccountChild.Ownerid = userRec1.id;
        AccountChild.Account_Status__c = 'Assigned';
        insert AccountChild;
    }
   
     /*
     Description : This is is a method to test the Batch for MSD District and MSD Owner Type
     Parameters  : None
     Return Type : void
     */
   
    private static testMethod void Test_MSDDistrictOwner()
    {
        setUpData();
        Test.startTest();
         Database.BatchableContext scc;
            InitialLoadAccountTeam_batch sc = new InitialLoadAccountTeam_batch();
            
            sc.query ='select id, ParentId, Account_Team_to_MAL__c,OwnerId, District__c, Parent.District__c, Parent.OwnerId FROM Account where ParentId=null limit 10';
            sc.start(scc);
            List<Account> scope = ([select id, ParentId, Account_Team_to_MAL__c,OwnerId, District__c, Parent.District__c, Parent.OwnerId FROM Account where ParentId=null limit 10]);
            sc.execute(scc,scope);
            sc.finish(scc);
         
        Test.stopTest();     
        
       
    }



}