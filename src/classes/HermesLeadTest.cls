@isTest
private class HermesLeadTest {
   
    @isTest
    public static void testParse() {
        HermesLead obj1 = new HermesLead();
        obj1.company = 'Test Co.';
        String json = System.JSON.serialize(obj1);
        HermesLead obj2 = HermesLead.parseJson(json);
        System.assertEquals(obj1.company, obj2.company);
    }
}