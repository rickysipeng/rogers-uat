/*
===============================================================================
Class Name   : Test_TAG_UpdateDistirctOwner
===============================================================================
PURPOSE:    This is test class for trigger TAG_UpdateDistirctOwner
            
Developer: Jayavardhan
Date: 03/18/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/18/2015          Jayavardhan               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_UpdateDistirctOwner{
    private static Account ParentAcc;
    private static Account ParentAcc2;
    private static Account ParentAcc3;
    private static Account ChildAcc1;
    private static  Assignment_Request__c  AccountAR;
    private static  Assignment_Request_Item__c AccountARI;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static User userRec2;
    private static User userRec3; 
    private static User userRec4; 
    private static  MSD_Code__c msdCode; 
    private static  MSD_Code__c msdCode2; 
    private static  MSD_Code__c msdCode3; 
    private static Shared_MSD_Code__c sharedMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD;
    private static Shared_MSD_Accounts__c sharedAccMSD2;
    private static Shared_MSD_Accounts__c sharedAccMSD3;
    private static District__c newDist;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();

    //create Test Data
    private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.DistrictOwnerUpdateBatch_size__c = '10';
        insert TAG_CS;
        
        system.debug('pEmp***'+pEmp);

        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Owner_Type__c = 'District');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment',Owner_Type__c='District');
        insert userRec2;

        userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment',Owner_Type__c = 'District');
        insert userRec3;
        
        userRec4 = new User(LastName = 'Raj', FirstName='Jeev',Alias = 'aRoger4', Email='test@Rogertest.com', Username='test@Rogertest4.com', CommunityNickname = 'nickRog4', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Owner_Type__c = 'District');
        insert userRec4;
        
        newDist = new District__c();
        newDist.Name='New Distrct';
        newDist.District_Owner__c=userRec3.id;
        newDist.Inactive__c = FALSE;
        insert newDist;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.District__c = newDist.Id;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.BillingPostalCode = 'A1A 1A1';
        insert ChildAcc1;
        //Account 2
        ParentAcc2= new Account();
        ParentAcc2.Name = 'ParentAcc2';
        ParentAcc2.Account_Status__c = 'Assigned';
        ParentAcc2.ParentID= null;
        ParentAcc2.OwnerId= userRec3.id;
        ParentAcc2.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc2;
        //Account 3
        ParentAcc3= new Account();
        ParentAcc3.Name = 'ParentAcc3';
        ParentAcc3.Account_Status__c = 'Assigned';
        ParentAcc3.ParentID= null;
        ParentAcc3.OwnerId= userRec3.id;
        ParentAcc3.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc3;

        msdCode = new MSD_Code__c();
        msdCode.OwnerId = userRec.id ;
        msdCode.Account__c = ParentAcc.id ;
        msdCode.District__c = newDist.Id;
        msdCode.MSD_Code_External__c = '2009';
        insert msdCode;

        msdCode2 = new MSD_Code__c();
        msdCode2.OwnerId = userRec.id ;
        msdCode2.Account__c = ParentAcc2.id ;
        msdCode2.MSD_Code_External__c = '7891';
        insert msdCode2 ;

        msdCode3 = new MSD_Code__c();
        msdCode3.OwnerId = userRec.id ;
        msdCode3.Account__c = ParentAcc3.id ;
        msdCode3.MSD_Code_External__c = '7892';
        insert msdCode3;

        Shared_MSD_Code__c sharedMSD2 = new Shared_MSD_Code__c();
        sharedMSD2.OwnerId = userRec.id ;
        sharedMSD2.Name = 'SharedMSD2';
        sharedMSD2.District__c = newDist.Id;
        sharedMSD2.MSD_Code_External__c = '9876';
        insert sharedMSD2;

        sharedAccMSD3 = new Shared_MSD_Accounts__c();
        sharedAccMSD3.Account__c=ParentAcc2.id;
        sharedAccMSD3.Shared_MSD_Code__c=sharedMSD2.id;
        insert sharedAccMSD3;

        sharedMSD = new Shared_MSD_Code__c();
        sharedMSD.OwnerId = userRec.id ;
        sharedMSD.Name = 'SharedMSD1';
        sharedMSD.MSD_Code_External__c = '12345';
        insert sharedMSD;

        sharedAccMSD = new Shared_MSD_Accounts__c();
        sharedAccMSD.Account__c=ParentAcc.id;
        sharedAccMSD.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD;

        sharedAccMSD2 = new Shared_MSD_Accounts__c();
        sharedAccMSD2.Account__c=ParentAcc2.id;
        sharedAccMSD2.Shared_MSD_Code__c=sharedMSD.id;
        insert sharedAccMSD2;
        
        AccountAR = new Assignment_Request__c();
        AccountAR.OwnerID = userRec2.id;
        insert AccountAR ;
        
        AccountARI= new Assignment_Request_Item__c();
        AccountARI.Account__c = ParentAcc.Id;
        AccountARI.New_Owner__c = userRec2.id;
        AccountARI.Business_Case__c = 'Business_Trigger';
        AccountARI.Reason_code__c = '4 - New Account Creation';
        AccountARI.Assignment_Request__c = AccountAR.Id;
        AccountARI.New_Approver__c = userRec2.Assignment_Approver__c;
        AccountARI.New_Channel__c = 'Channel_test';
        AccountARI.Requested_Item_Type__c='Account Owner';
        insert AccountARI;

        

    }
    /*******************************************************************************************************
    * @description: This is a positive test method with District Owner updated with User type as 'District'.
    *******************************************************************************************************/
    static testmethod void testUpdateOwnerTypeDistrict(){
        test.startTest();
        setUpData();        
        newDist.District_Owner__c = userRec2.id;
        update newDist;       
        test.stopTest(); 
    }
    
    /*******************************************************************************************************
    * @description: This is a positive test method with District Owner updated with User type as 'Account'.
    *******************************************************************************************************/
    static testmethod void testUpdateOwnerTypeAccount(){
        test.startTest();
        setUpData();        
        newDist.District_Owner__c = userRec3.id;
        update newDist;
        test.stopTest(); 
    }
}