/*  Jan 23, 2015. Paul Saini. Removed Contact.Ext__c as per requirements.
    As per comments from MP: The data in this field is planned to be concatenated with the standard Phone field. 
    The code should no longer reference the Ext__c field.                                             Pre-populate SOI Date from Serviceable location's region (timeframe define in custom setting)
    
*/

public with sharing class MandatoryFieldsPDFController {

  public Opportunity opp {get; set;}
  public String showTable {get; set;}
  public Quote q {get; set;}
  public List<Mandatory_COP_Fields__c> theMandatoryFields {get; set;}

  public MandatoryFieldsPDFController (ApexPages.StandardController stdController) {
    String oppId = ApexPages.currentPage().getParameters().get('oppId');
    
    if (Utils.isEmpty(oppId)){
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Missing Opportunity Id in URL. Please ensure to enter this page via the button on the Opportunity Detail Page after you have WON an opportunity and the quote is synced.'));
      return;
    }
    
    try{
      // Get the Opportunity
      opp = [SELECT Id, SyncedQuoteId, Account.BillingCountry, Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingStreet, Contact__r.Name, Account.Name FROM Opportunity WHERE Id = :oppId];
      
      // Get the Quote
      if (opp != null && opp.SyncedQuoteId != null){
        q = [SELECT id, Name, QuoteNumber__c FROM Quote WHERE Id = :opp.SyncedQuoteId];
      }
      
    }catch(Exception ex){
      if (opp == null){
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the Opportunity from the given Id.  Please ensure that the Opportunity Id is valid.'));
        return;
      } 
    }
    
      // PS: removed Ext__c from query below
      try{
      theMandatoryFields = [SELECT Id, 
                  Building_Entry_Contact__r.FirstName, Building_Entry_Contact__r.LastName, Building_Entry_Contact__r.Phone, Building_Entry_Contact__r.MobilePhone, Building_Entry_Contact__r.Email,
            Building_Entry_Contact__r.Title, Building_Entry_Contact__r.MailingCity, Building_Entry_Contact__r.MailingCountry, Building_Entry_Contact__r.MailingState, Building_Entry_Contact__r.MailingStreet, Building_Entry_Contact__r.MailingPostalCode,
            Building_Entry_Contact__c, Not_Applicable__c, Quote_Site__c, Roger_Account_Optional__c, Suite_Floor__c, Quote_Site__r.Site__r.Display_Name__c, Demarc__c FROM Mandatory_COP_Fields__c WHERE Quote_Site__r.Quote__c = :opp.SyncedQuoteId Order By CreatedDate asc];
    }catch(Exception ex1){
      ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the quote sites for this opportunity.  Please ensure there is at least one site on this quote before proceeding.'));
      return;
    }
      
  }
  
  public void previewPDF(){
    // Need to update the showTable with the site information
    
    String sTable;  // a table for one of the sites
    
        List<String> lLocationTable = new List<String>();  // This will store all of the tables and then we can grab each and place in the showTable
        
        //Create Table
        for(Mandatory_COP_Fields__c manField : theMandatoryFields){   
            sTable = '';
             if(  manField.Quote_Site__r !=null && manField.Quote_Site__r.Site__r!=null && manField.Quote_Site__r.Site__r.Display_Name__c!=null ){
	            sTable = '<span style="padding-left:5px;" class="font7"><b>'+Label.Site_Address_MF+': '  + ' : ' + manField.Quote_Site__r.Site__r.Display_Name__c.toUpperCase();
	            sTable += '</b></span><br/>'; 
             }
            sTable += '<table  width="100%" border="0" cellpadding = "0" cellspacing = "0">';
            sTable += '<tr><th colspan="2" style="padding-left:5px;padding-top:5px;">'+Label.Site_Information_MF+'</th></tr>';
            sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">'+Label.Suite_Floor_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Suite_Floor__c)?'':manField.Suite_Floor__c) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b">'+Label.Site_does_not_have_a_Suite_Floor_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + manField.Not_Applicable__c +'</td>';
      sTable += '</tr>';
       sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;" align="left" class="font7b">'+Label.Demarc_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Demarc__c)?'':manField.Demarc__c) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b"></td>';
      sTable += '<td width="30%" align="left" class="font7b"></td>';
      sTable += '</tr>';
      sTable += '</table>';
      
      sTable += '<br/><table  width="100%" border="0" cellpadding = "0" cellspacing = "0">';
            sTable += '<tr><th colspan="2" style="padding-left:5px;padding-top:5px;padding-bottom:5px;">'+Label.Building_Entry_Contact_MF+'</th></tr>';
            sTable += '<tr>';
      
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.First_Name_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.FirstName)?'':manField.Building_Entry_Contact__r.FirstName) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b">'+Label.Mailing_Street_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.MailingStreet)?'':manField.Building_Entry_Contact__r.MailingStreet) +'</td>';
      sTable += '</tr>';
      sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.Last_Name_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.LastName)?'':manField.Building_Entry_Contact__r.LastName) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b">'+Label.Mailing_City_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.MailingCity)?'':manField.Building_Entry_Contact__r.MailingCity) +'</td>';
      sTable += '</tr>';
      sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.Title_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.Title)?'':manField.Building_Entry_Contact__r.Title) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b">'+Label.Mailing_State_Province_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.MailingState)?'':manField.Building_Entry_Contact__r.MailingState) +'</td>';
      sTable += '</tr>';
      sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.Phone_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.Phone)?'':manField.Building_Entry_Contact__r.Phone) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b">'+Label.Mailing_Zip_Postal_Code_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.MailingPostalCode)?'':manField.Building_Entry_Contact__r.MailingPostalCode) +'</td>';
      sTable += '</tr>';
      sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.Extension_MF+': </td>';
     //PS: removed Ext__c sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.Ext__c)?'':manField.Building_Entry_Contact__r.Ext__c) + '</td>';
      sTable += '<td width="30%" align="left" class="font7b"></td>';
     
      sTable += '<td width="20%" align="left" class="font7b">'+Label.Mailing_Country_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.MailingCountry)?'':manField.Building_Entry_Contact__r.MailingCountry) +'</td>';
      sTable += '</tr>';
      sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.Mobile_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.MobilePhone)?'':manField.Building_Entry_Contact__r.MobilePhone) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b"></td>';
      sTable += '<td width="30%" align="left" class="font7b"></td>';
      sTable += '</tr>';
      sTable += '<tr>';
      sTable += '<td width="20%" style="padding-left:5px;padding-bottom:5px;" align="left" class="font7b">'+Label.Email_MF+': </td>';
      sTable += '<td width="30%" align="left" class="font7b">' + (Utils.isEmpty(manField.Building_Entry_Contact__r.Email)?'':manField.Building_Entry_Contact__r.Email) + '</td>';
      sTable += '<td width="20%" align="left" class="font7b"></td>';
      sTable += '<td width="30%" align="left" class="font7b"></td>';
      sTable += '</tr>';
      sTable += '</table>';
            
            
            
            lLocationTable.add(sTable);
        }
    
    
    showTable = '<table width="100%" border="0" cellpadding = "0" cellspacing = "0">';
        Integer i = 1;
        for(String s1:lLocationTable){   
                //Wrap Table in side Table to avoid page break inside table 
                showTable += '<tr><td><table  style="margin-left:5px;margin-right:5px;page-break-inside: avoid;"  width="98%" border="1" cellpadding = "0" cellspacing = "0">';
                showTable += '<tr>';
                showTable += '<td>' + s1 + '</td>' ;
                showTable += '</tr>';
                showTable += '</table></td></tr><tr><td>&nbsp;</td></tr>';
                
                i++;
        }
            
        showTable += '</table>';
        
  }
    
}