/*
===============================================================================
 Class Name   : Test_TAG_DeleteAREntrieserrorBatch
===============================================================================
PURPOSE:    This class a controller class for TAG_BulkRequestProcessBatch.
              
Developer: Deepika Rawat
Date: 4/23/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
4/23/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_DeleteAREntrieserrorBatch{
    private static List<Assignment_Request__c> lstAssignReq = new List<Assignment_Request__c>();
    private static List<Assignment_Request_Entry__c> lstAREntries = new List<Assignment_Request_Entry__c>();
    private static List<Assignment_Request_Error__c> lstARErrors= new List<Assignment_Request_Error__c>();
    private static List<Account> lstaccount = new List<Account>();
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    
    private static void setUpData(){
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Purge_AREntry__c = '0';
        insert TAG_CS;
        User user1 = new User(LastName = 'Mark O’Roger1', Alias = 'er112', Email='test@Rogertest.com', Username='test@Rogertest12.com', CommunityNickname = 'nick23Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Employee_Id__c='Te1Te2Te3', Owner_Type__c='Account', Assignment_Approver__c = UserInfo.getUserId(), Channel__c = 'ABS');
        insert user1;
        User userAccountOwner = new User(LastName = 'Mark O’Rogerdsfd1', ManagerId = user1.id, Alias = 'ty112', Email='test@Rogertest.com', Username='test@Trseegertest12.com', CommunityNickname = 'nicw3Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Assignment_Approver__c = user1.id,Employee_Id__c='Te1Te2Te4', Owner_Type__c='Account',Channel__c = 'ABS' );
        insert userAccountOwner;

        Account ParentAcc = new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= user1.id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        lstaccount.add(ParentAcc);
        insert lstaccount;
        
        Assignment_Request__c assignOwner = new Assignment_Request__c();
        assignOwner.Approval_Override__c = false;
        assignOwner.Upload_via_Data_Loader__c= true;
        assignOwner.Request_Type__c='Owner';
        assignOwner.Status__c='In-Progress';
        lstAssignReq.add(assignOwner);
        insert lstAssignReq;
        
        //Account Owner Entry
        Assignment_Request_Entry__c arEntry = new Assignment_Request_Entry__c();
        arEntry.Account__c = lstaccount[0].id;
        arEntry.User__c = userAccountOwner.id;
        arEntry.Assignment_Request__c = lstAssignReq[0].id;
        arEntry.Business_Case__c = 'Test Business Case';
        arEntry.Reason_Code__c = '1';
        lstAREntries.add(arEntry);
        insert lstAREntries;
        lstAREntries[0].Status__c='Processed';
        update lstAREntries;

        Assignment_Request_Error__c errorRec = new Assignment_Request_Error__c();
        errorRec.Assignment_Request__c = lstAssignReq[0].id;
        insert errorRec;
    }
     /*******************************************************************************************************
     * @description: This is a positive test method
    *******************************************************************************************************/
    static testmethod void testBatch(){
        setUpData(); 
        test.startTest();
        TAG_DeleteAREntriesBatch sc = new TAG_DeleteAREntriesBatch();
        ID batchprocessid = Database.executeBatch(sc,200);
        test.stopTest();
    }
}