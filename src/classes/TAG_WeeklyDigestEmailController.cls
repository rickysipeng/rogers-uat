/**
This class sends a Weekly  digest Email
**/

global class TAG_WeeklyDigestEmailController implements Database.Batchable<sObject>, Database.stateful {
    
    
     global List<ProcessInstanceWorkitem> listTotal = new List<ProcessInstanceWorkitem>();
     global List<String> listIds = new List<String>();
     global Map<ID,Assignment_Request_Item__c>  MaptotalAssgn= new Map<ID,Assignment_Request_Item__c>();
     global List<Assignment_Request_Item__c>  ListtotalAssgn = new List<Assignment_Request_Item__c>();
     global List<Assignment_Request_Item__c> ListtotalAssgnError= new List<Assignment_Request_Item__c>();
     global List<ProcessInstanceStep> listProcessInsSteps  = new List<ProcessInstanceStep>();
     global Map<ID,ProcessInstanceStep> MapProcessInsSteps = new Map<ID,ProcessInstanceStep>();
     global Map<String,ProcessInstanceStep> Maptarget  = new Map<String,ProcessInstanceStep>();
     global Map<String,String>  MaptargetUserId  = new Map<String,String>() ;
     global List<ProcessInstance> listProcessInsRej = new List<ProcessInstance>();
     global List<String> listActorId = null;
     global List<User> Listuser  = null;
     global Map<Id,List<String>> mapUser  = null;
     global String StringSub = null;
     global List<String> listProcessIns  = new List<String>();
     String allRejStr = '';
     global String concatStr='';
     global Map<String,String>  mapError = new Map<String,String>();
     global Map<ID,Assignment_Request_Item__c> mapAssgReqItem;
     global Map<Id,User> mapUserId = new Map<Id,User>();
     global String query='';
     global Assignment_Request_Item__c testObj= new Assignment_Request_Item__c();
   
    global TAG_WeeklyDigestEmailController () {
  
    }
    
    global TAG_WeeklyDigestEmailController(String query) {
        this.query = query;
    }
    
    global Database.QueryLocator start(Database.BatchableContext BC){
    
        string ApprovalUrl1 = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/TAG_ApproverView';
       
    query = 'Select ProcessInstanceId,ProcessInstance.TargetObjectId,ProcessInstance.Status ,ProcessInstance.CompletedDate, OriginalActorId, IsDeleted, Id, CreatedDate, CreatedById, ActorId From ProcessInstanceWorkitem where  ProcessInstance.TargetObject.type =\'Assignment_Request_Item__c\' and CreatedDate >=LAST_N_DAYS:7    order by ActorId';
      
        
        
         return Database.getQueryLocator(query);
    }
     
    
    
    Set<ID> setProcessWorkItems;
    Map<ID,ID> mapTargetUserIdnew;
    
    global void execute(Database.BatchableContext BC,List<sObject> scope){
        //put logic
        List<ProcessInstanceWorkitem> listTotal =(List<ProcessInstanceWorkitem>)scope;
        String strActorId= '';
        String tempActor='';
        allRejStr = '';
        setProcessWorkItems= new Set<ID>();
        
        Assignment_Request_Item__c assignList;
        String strActor ;
        List<String> ls;
        for(ProcessInstanceWorkitem totalProcess:listTotal){
            assignList= new Assignment_Request_Item__c();
            if(totalProcess.ProcessInstance.TargetObjectId!=null){
                assignList.id = totalProcess.ProcessInstance.TargetObjectId;
                StringSub = (String)assignList.id;
                strActor = (String)totalProcess.ActorId;
                //   005=a1-a2,005a=b1-b2,005c=c1-c2
                if(strActor.startsWith('005')){
                    if(tempActor.equalsIgnoreCase(totalProcess.ActorId)){
                        concatStr =concatStr+'-'+ totalProcess.ProcessInstance.TargetObjectId;
                    }else{
                        tempActor=totalProcess.ActorId;
                        concatStr=concatStr + ',' + totalProcess.ActorId+'='+totalProcess.ProcessInstance.TargetObjectId;
                    }
               }
                if(strActor.startsWith('005')){
                    listIds.add(assignList.id);
                    
                    if(!strActorId.contains(totalProcess.ActorId) &&(strActor.startsWith('005')) ){
                        strActorId+= ',' + totalProcess.ActorId;
                        setProcessWorkItems.add(totalProcess.ActorId);
                    }
                }   
               
            }
        }
       
        if(strActorId!=''){
            strActorId= strActorId.substring(1,strActorId.length());
            listActorId = strActorId.split(',');
            Listuser= [select id,name,email from User where id =:listActorId];
            mapUser = new Map<Id,List<String>>();
            ls= new List<String>();
            for(User u:Listuser){
                ls= new List<String>();
                ls.add(u.email);
                ls.add(u.name);
                mapUser.put(u.id,ls);
            }
        }
        
       
        List<String> allList = new List<String> ();
         List<String> Liststr = new List<String>();
        if(Test.isRunningTest()){
            concatStr = ',005e0000002WYAhAAO=a2we0000000GUtnAAG-a2we0000000GUtoAAG';
        }
        if(concatStr.startsWith(','))
            concatStr =concatStr.substring(1,concatStr.length());
        for(Integer i=0;i<= concatStr.split(',').size()-1;i++){
           Liststr = concatStr.split(',')[i].split('=')[1].split('-');
            allList.addAll(Liststr);
        }
       
       ListtotalAssgn = [select name, Account__r.Name ,MSD__r.MSD_Code_External__c ,Shared_MSD__r.MSD_Code_External__c ,Action__c ,Current_Owner__r.name ,New_Owner__r.name ,Member__r.name ,CreatedDate ,CreatedBy.name,CreatedById,Business_Case__c , Requested_Item_Type__c from Assignment_Request_Item__c where id=:allList order by id];
        
       MaptotalAssgn = new Map<ID,Assignment_Request_Item__c>([select name, Account__r.Name ,MSD__r.MSD_Code_External__c ,Shared_MSD__r.MSD_Code_External__c ,Action__c ,New_District__r.name,Current_Owner__r.name ,New_Owner__r.name ,Member__r.name ,CreatedDate ,CreatedBy.name,CreatedById,Business_Case__c , Requested_Item_Type__c from Assignment_Request_Item__c where id=:allList order by id]);

        date myDate = Date.today().addDays(-7);
        listProcessInsRej=[Select CompletedDate,Status,TargetObjectId,LastActorId,(select OriginalActorId from StepsAndWorkItems where StepStatus=:Label.Transfer_Rejected order by ProcessInstance.CompletedDate) From ProcessInstance where CompletedDate>=:myDate and ProcessInstance.TargetObject.type ='Assignment_Request_Item__c' and  Status =:Label.Transfer_Rejected];
        List<String> allRejList = new List<String>();
        mapTargetUserIdnew= new Map<ID,ID>();
        
        List<ProcessInstanceHistory> tempProcessSteps = new List<ProcessInstanceHistory>();
        String originalActorId;
        for(Integer i=0;i<listProcessInsRej.size();i++){
            tempProcessSteps =listProcessInsRej[i].StepsAndWorkItems;
           for(ProcessInstanceHistory PIS : tempProcessSteps){
                originalActorId=PIS.OriginalActorId;
                if(originalActorId.startswith('005')){
                    allRejList.add(listProcessInsRej[i].TargetObjectId);
                    allRejStr = allRejStr+','+listProcessInsRej[i].LastActorId +'='+listProcessInsRej[i].TargetObjectId;
                    mapTargetUserIdnew.put(listProcessInsRej[i].TargetObjectId,listProcessInsRej[i].LastActorId);
                }
            }
        }
        if(allRejStr!=null && allRejStr!='')
          allRejStr =allRejStr.substring(1,allRejStr.length());
        
        ListtotalAssgnError= [select name, Account__r.Name ,MSD__r.MSD_Code_External__c ,Shared_MSD__r.MSD_Code_External__c ,Action__c ,Current_Owner__r.name ,New_Owner__r.name ,New_District__r.name,Member__r.name ,CreatedDate ,CreatedBy.name,CreatedById,Business_Case__c , Requested_Item_Type__c,New_Channel__c,Override_Reason__c,Current_Approver__c,Current_Approver__r.Name from Assignment_Request_Item__c where id=:allRejList and Current_Approver__r.Name!=null  order by id];
        
        listProcessInsSteps=[Select ID,ProcessInstanceId,ProcessInstance.TargetObjectId,OriginalActorId,Comments,ProcessInstance.CompletedDate From ProcessInstanceStep where ProcessInstance.CompletedDate!=null and ProcessInstance.status=:Label.Transfer_Rejected and ProcessInstance.CompletedDate>=:myDate and StepStatus=:Label.Transfer_Rejected order by ProcessInstance.CompletedDate];
        
        MapProcessInsSteps=new Map<ID,ProcessInstanceStep>([Select ID,ProcessInstanceId,ProcessInstance.TargetObjectId,ActorId,OriginalActorId,Comments,ProcessInstance.CompletedDate From ProcessInstanceStep where  ProcessInstance.CompletedDate!=null and ProcessInstance.status=:Label.Transfer_Rejected and ProcessInstance.CompletedDate>=:myDate and StepStatus=:Label.Transfer_Rejected order by ProcessInstanceId]);
        Integer j=0;
        
        Maptarget = new Map<String,ProcessInstanceStep>();
        MaptargetUserId = new Map<String,String>();
        String strIDArray='';
        Integer count1;
        ID currentID;
        String orginalIdStr;
        String strID;
        for(ProcessInstanceStep PIS:listProcessInsSteps){
              currentID = PIS.ID;
               count1=0;
            if(MapProcessInsSteps.containsKey(currentID)){
                strID=MapProcessInsSteps.get(currentID).ProcessInstance.TargetObjectId;
                if(!strIDArray.contains(strID)){
                    strIDArray = strIDArray + ',' + strID;
                
                    orginalIdStr=MapProcessInsSteps.get(currentID).OriginalActorId;
                    if(orginalIdStr.startsWith('005'))
                        Maptarget.put(strID,MapProcessInsSteps.get(currentID));
                    MaptargetUserId.put(strID,MapProcessInsSteps.get(currentID).ActorId);    // not in use
                    if(count1==0)
                        concatStr = concatStr+','+ MapProcessInsSteps.get(currentID).ActorId+'='+strID.substring(0,15);
                    else
                        concatStr = concatStr+'-'+ strID.substring(0,15);
                }
                
            }   
        }
        
        mapError = new Map<String,String>();
        
        if(listProcessInsSteps.size()>0){
        mapUserId= new   Map<Id,User>([select id, name from user]);
        String currApp;
        //String strID;
            for(Assignment_Request_Item__c items:ListtotalAssgnError){
                currApp=items.Current_Approver__c;
                if(currApp!=null)
                if(currApp.startsWith('005')){
                    strID='';
                    strID=items.id;
                    //strID = strID.substring(0,15);
                    if(Maptarget.get(strID)!=null){
                        mapError.put(strID,Maptarget.get(strID).OriginalActorId+'=='+Maptarget.get(strID).Comments);
                    }
                }
            }
        }
        
         listProcessInsSteps=[Select ID,ProcessInstanceId,ProcessInstance.TargetObjectId,OriginalActorId,Comments,ProcessInstance.CompletedDate From ProcessInstanceStep where ProcessInstance.CompletedDate!=null and ProcessInstance.status=:Label.Transfer_Rejected and ProcessInstance.CompletedDate>=:myDate order by ProcessInstance.CompletedDate];
         
         

       List<String>  assgReqItemList=new List<String>();
       for(ProcessInstanceStep objPIStep:listProcessInsSteps){
            if(objPIStep.ProcessInstance.TargetObjectId!=null)
                assgReqItemList.add(objPIStep.ProcessInstance.TargetObjectId);
       }
      mapAssgReqItem = new Map<ID,Assignment_Request_Item__c>([select name, Account__r.Name ,MSD__r.MSD_Code_External__c ,Shared_MSD__r.MSD_Code_External__c ,Action__c ,Current_Owner__r.name ,New_Owner__r.name ,Member__r.name ,CreatedDate ,CreatedBy.name,CreatedById,Business_Case__c , Requested_Item_Type__c,New_Channel__c,Override_Reason__c,Current_Approver__r.Name from Assignment_Request_Item__c where id=:assgReqItemList  order by id]);
        
    }    
    
    global void finish(Database.BatchableContext BC) {
        
           string ApprovalUrl1 = URL.getSalesforceBaseUrl().toExternalForm()+'/apex/TAG_ApproverView';
         
            Messaging.singleEmailMessage mail;
            List <Messaging.SingleEmailMessage> lstSendMails=new  List <Messaging.SingleEmailMessage>();
            List<OrgWideEmailAddress> lstOWE = [SELECT Address, DisplayName, Id FROM OrgWideEmailAddress where DisplayName =:label.Transfer_Rogers_Admin];
            List<String> listResult= concatStr.split(',');
            Integer counter1=0;
                
            List<String> listAct = new List<String>();
            List<String> ListnewStrArr;
            List<Static_Data_Utilities__c> ListapproverEmailsss = new List<Static_Data_Utilities__c>();
            List<String> ListcurrentItemTotal = new List<String>();
            String strEmailBody1;
            Integer noOfRecord;
            String approverName;
            Integer noRecordCount;
            if(listActorId!=null){
                String strEmailBody=null;
                for(String singleActor:listActorId){
                    listAct=mapUser.get(singleActor);
                    if(listAct != null && listAct.get(0)!=null && listAct.get(1)!=null)
                    {
                    approverName = listAct.get(1);
                    }
           
                    mail = new Messaging.singleEmailMessage();
                   ListnewStrArr =new List<String>();
                   ListapproverEmailsss = [Select Value__c from Static_Data_Utilities__c where name='WeeklyApproverEmailId'];
                    if(ListapproverEmailsss.isEmpty()!=true && ListapproverEmailsss[0].Value__c!='NOVALUE'){
                    //if(ListapproverEmailsss.IsEmpty()!=true){
                        Static_Data_Utilities__c approverEmail=ListapproverEmailsss[0];
                        ListnewStrArr.add(approverEmail.Value__c);
                    }else
                        ListnewStrArr.add(listAct.get(0));
                    
                     mail.setToAddresses(ListnewStrArr);
                    
                    //code added to get acurate number of records needed for approval
                    ListcurrentItemTotal = listResult[counter1].split('=')[1].split('-');
                    noOfRecord=0;
                    Integer innerCounter;
                    String resultString;
                    List<String> arrStr;
                    List<String> currentItem;
                    Assignment_Request_Item__c result;
                    for(String eachItem:ListcurrentItemTotal){
                        result = MaptotalAssgn.get(eachItem);
                        if(Test.isRunningTest()){
                            result = testObj;
                        }
                        if(result.Account__r.Name==null && result.MSD__r.MSD_Code_External__c==null && result.Shared_MSD__r.MSD_Code_External__c==null){
                       }else{
                            noOfRecord++;
                       }
                    }
                     
                    strEmailBody = ' <html>';
                    strEmailBody = strEmailBody +  ' <body>';
                    strEmailBody = strEmailBody +  ' Hi <b>'+listAct.get(1)+',</b>';
                    strEmailBody = strEmailBody +  ' <p>You have <b>'+ noOfRecord+'</b> new pending requests for a week awaiting your approval:</p>';
                 //  strEmailBody = strEmailBody +  ' </br></br>';
                    strEmailBody = strEmailBody +  ' </br></br> <table> <tr><td> <b>Pending Requests</b> </td></tr></table> <table border="1"> <tr><th>Account Name</th><th>MSD Code</th>  <th>Shared MSD Code</th> <th>Request Item Type</th> <th>Action</th> <th>Current Owner</th> <th>New Owner</th> <th>Member</th> <th>Request Date</th> <th>Requested By</th> <th>Business Case</th> </tr>';
          
                    innerCounter=0;
                    arrStr=new List<String>();
                    for(Integer i=0;i<= listResult.size()-1;i++){
                        arrStr.addAll(listResult[i].split('=')[1].split('-'));
                    }
                    resultString='';
                    for(String s:arrStr){
                        resultString = resultString +','+s;
                    }
                    
                    currentItem= new List<String>();
                    currentItem = listResult[counter1].split('=')[1].split('-');
                    result = new Assignment_Request_Item__c();
                    for(String id:currentItem){
                        
                        result = MaptotalAssgn.get(id);
                        if(Test.isRunningTest()){
                            result = testObj;
                        }
                        if(result.Account__r.Name==null && result.MSD__r.MSD_Code_External__c==null && result.Shared_MSD__r.MSD_Code_External__c==null){
                        
                        }else{
                             strEmailBody = strEmailBody +  ' <tr>';
                            if(result.Account__r.Name!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.Account__r.Name+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.MSD__r.MSD_Code_External__c!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.MSD__r.MSD_Code_External__c+'</td>   ';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.Shared_MSD__r.MSD_Code_External__c!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.Shared_MSD__r.MSD_Code_External__c+'</td>   ';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.Requested_Item_Type__c!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.Requested_Item_Type__c+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.Action__c!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.Action__c+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.Current_Owner__r.name!=null && !result.Requested_Item_Type__c.equalsIgnoreCase('Account Team') && !result.Requested_Item_Type__c.equalsIgnoreCase('MSD Member') && !result.Requested_Item_Type__c.equalsIgnoreCase('Shared MSD Member'))
                                strEmailBody = strEmailBody +  ' <td>'+result.Current_Owner__r.name+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.New_Owner__r.name!=null && result.Requested_Item_Type__c.contains('District') && result.New_District__r.name!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.New_District__r.name +' ('+result.New_Owner__r.name+')'+'</td>';
                            else if(result.New_Owner__r.name!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.New_Owner__r.name+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.Member__r.name!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.Member__r.name+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.CreatedDate!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.CreatedDate+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.CreatedBy.name!=null)
                                strEmailBody = strEmailBody +  ' <td>'+result.CreatedBy.name+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            if(result.Business_Case__c!=null) 
                                strEmailBody = strEmailBody +  ' <td>'+result.Business_Case__c+'</td>';
                            else    
                                strEmailBody = strEmailBody +  ' <td></td>';
                            strEmailBody = strEmailBody +  ' </tr>';
                        }
                    }
                    strEmailBody = strEmailBody +  ' </table>';
                    system.debug('In strEmailBody creating');
                    strEmailBody1='';
                    strEmailBody1 = strEmailBody1 +  ' </br>';
                    strEmailBody1 = strEmailBody1 +  ' <table>';
                    strEmailBody1 = strEmailBody1 +  ' <tr>';
                    strEmailBody1 = strEmailBody1 +  ' <td>Please <a href='+ApprovalUrl1+'>Click Here</a> To review them</td>';
                    strEmailBody1 = strEmailBody1 +  ' </tr>';
                    strEmailBody1 = strEmailBody1 +  ' </table>';
                    strEmailBody1 = strEmailBody1 +  ' </br>';
                    strEmailBody1 = strEmailBody1 +  ' <table>';
                   
                    noRecordCount=0;
                   
                  // result = new Assignment_Request_Item__c();
                    for(Assignment_Request_Item__c result1:ListtotalAssgnError){
                        result1 = mapAssgReqItem.get(result1.id);
                        if(result1!=null && result1.Current_Approver__r!=null)
                            if(result1.Current_Approver__r.Name.equalsIgnoreCase(listAct.get(1))){
                                if(result1.Account__r.Name==null && result1.MSD__r.MSD_Code_External__c==null && result1.Shared_MSD__r.MSD_Code_External__c==null){
                        
                                }else{
                                    noRecordCount++;
                                }
                            }
                    }   
                    
                   if(noRecordCount<=0){
                        strEmailBody1 = strEmailBody1 +  ' <tr>';
                        strEmailBody1 = strEmailBody1 +  ' <td> <b>No Expired Requests</b> </td>';
                        strEmailBody1 = strEmailBody1 +  ' </tr>';
                        
                   }else{
                     //   strEmailBody1 = strEmailBody1 +  ' <tr>';
                        strEmailBody1 = strEmailBody1 +  '<tr> <td> <b>The  following records that you previously reviewed have been rejected: </b> </td> </tr> </table><table border="1"> <tr><th>Account Name</th> <th>MSD Code</th><th>Shared MSD Code</th><th>Request Item Type</th><th>Action</th><th>Current Owner</th><th>New Owner</th><th>Member</th><th>Request Date</th><th>Requested By</th> <th>Last Approver</th><th>Reason</th> </tr>';
               
                        for(Assignment_Request_Item__c result2:ListtotalAssgnError){
                        result2 = mapAssgReqItem.get(result2.id);
                        if(result2!=null && result2.Current_Approver__r!=null){
                        if(result2.Current_Approver__r.Name.equalsIgnoreCase(listAct.get(1))){
                        if(result2.Account__r.Name==null && result2.MSD__r.MSD_Code_External__c==null && result2.Shared_MSD__r.MSD_Code_External__c==null){
                        
                        }else{
                             strEmailBody1 = strEmailBody1 +  ' <tr>';
                                if(result2.Account__r.Name!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.Account__r.Name+'</td>';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.MSD__r.MSD_Code_External__c!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.MSD__r.MSD_Code_External__c+'</td>   ';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.Shared_MSD__r.MSD_Code_External__c!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.Shared_MSD__r.MSD_Code_External__c+'</td>   ';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.Requested_Item_Type__c!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.Requested_Item_Type__c+'</td>';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.Action__c!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.Action__c+'</td>';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.Current_Owner__r.name!=null && !result2.Requested_Item_Type__c.equalsIgnoreCase('Account Team') && !result2.Requested_Item_Type__c.equalsIgnoreCase('MSD Member') && !result2.Requested_Item_Type__c.equalsIgnoreCase('Shared MSD Member')){
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.Current_Owner__r.name+'</td>';
                                }else {   
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                }
                                 
                                if(result2.New_Owner__r.name!=null && result2.New_Owner__r.name!='' && result2.Requested_Item_Type__c.contains('District') && result2.New_District__r.name!=null && result2.New_District__r.name!=''){
                                     strEmailBody1 = strEmailBody1 +  ' <td>'+result2.New_District__r.name +' ('+result2.New_Owner__r.name+')'+'</td>';
                                }else if(result2.New_Owner__r.name!=null){
                                    strEmailBody1 = strEmailBody1+  ' <td>'+result2.New_Owner__r.name+'</td>';
                                }else{    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                }if(result2.Member__r.name!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.Member__r.name+'</td>';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.CreatedDate!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.CreatedDate+'</td>';
                                else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(result2.CreatedBy.name!=null)
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+result2.CreatedBy.name+'</td>';
                               else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                
                                 
                                if(mapError.get(result2.id)!=null && mapUserId.get(mapError.get(result2.id).split('==')[0]).name!=null)  { 
                                    strEmailBody1 = strEmailBody1 +  ' <td>'+mapUserId.get(mapError.get(result2.id).split('==')[0]).name+'</td>';
                                }else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';
                                if(mapError.get(result2.id)!=null && mapError.get(result2.id).split('==')[1]!=null && mapError.get(result2.id).split('==')[1]!='null' )   {
                                   strEmailBody1 = strEmailBody1 +  ' <td>'+mapError.get(result2.id).split('==')[1]+'</td>';
                               } else    
                                    strEmailBody1 = strEmailBody1 +  ' <td></td>';  
                                    
                                strEmailBody1 = strEmailBody1 +  ' </tr>';
                            }
                            system.debug('strEmailBody1--->'+strEmailBody1);
                        }
                      } 
                   }
                }
                   

                    strEmailBody1 = strEmailBody1 +  ' </table>';
                    strEmailBody1 = strEmailBody1 +  ' </br>';
                    strEmailBody1 = strEmailBody1 +  ' <table>';
                    strEmailBody1 = strEmailBody1 +  ' <tr>';
                    strEmailBody1 = strEmailBody1 +  ' <td>Thanks</td>';
                    strEmailBody1 = strEmailBody1 +  ' </tr>';
                    strEmailBody1 = strEmailBody1 +  ' </table>';
                    system.debug('strEmailBody1-->'+strEmailBody1);
                    system.debug('In strEmailBody---->'+strEmailBody);
                    strEmailBody = strEmailBody + strEmailBody1 ; 
                    strEmailBody = strEmailBody +  ' </body>';
                    strEmailBody = strEmailBody +  ' </html>';
                     mail.setHTMLBody(strEmailBody );   
                    if(!lstOWE.isEmpty())              
                        mail.setOrgWideEmailAddressId(lstOWE[0].Id) ;    
                    mail.setSaveAsActivity(false);
                    mail.setSubject(label.Transfer_WeeklyMsg1);
                    lstSendMails.add(mail);
                    counter1++;
                }
            }   
            
            try{
                if(!lstSendMails.IsEmpty()) {
                Messaging.SendEmailResult[] mailResults = Messaging.sendEmail(lstSendMails);
                 } 
                if(Test.isRunningTest())
                { Integer a=1/0;
                }
            }           
            catch(Exception ex){
                System.Debug('Exception in Sending Email : ' + ex.getMessage());
            } 
   }
      
}