/*
===============================================================================
 Class Name   : TAG_DeleteAREntriesSchedular
===============================================================================
PURPOSE:    This schedular class runs the TAG_DeleteAREntriesBatch class 
            with a batch of 200 records. 

Developer: Deepika Rawat
Date: 01/25/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/25/2015          Deepika                     Original Version
===============================================================================
*/
global class TAG_DeleteAREntriesSchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        TAG_DeleteAREntriesBatch sc = new TAG_DeleteAREntriesBatch ();
        
        ID batchprocessid = Database.executeBatch(sc,200);
        system.debug('sc*************' +sc);
    }

}