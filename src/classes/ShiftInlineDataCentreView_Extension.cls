/*
@Name:  ShiftInlineOLIView_Extension 
@Description: Visualforce extension for the ShiftInlineOLIView page to display Data Centres
@Dependancies: 
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-29-2014  | 1.0.0          | Jocsan | Initial
04-30-2014  | 1.0.1          | Edward | Add try/catch in DML opperation in theSave method.
*/
public class ShiftInlineDataCentreView_Extension {
    
    private Opportunity theOpportunity;
    private map<string, string> thePicklistEntryMap = new map<string, string>();
    public list<Opportunity_Data_Centre__c> theODCList {get; set;}
    
    public ShiftInlineDataCentreView_Extension(ApexPages.StandardController controller) {
        // get opportunity records
        theOpportunity = (Opportunity) controller.getRecord();
        
        Schema.DescribeFieldResult F = Quote_Line_Item__c.Data_Center__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();
        for (Schema.PicklistEntry ple : P) {
            thePicklistEntryMap.put(ple.getValue(), ple.getLabel());
        }
        
        // get data centre records
        theGetODC();
    }

    private void theGetODC() {
    system.debug('***********theOpportunity.Id**'+theOpportunity.Id);
    
        // query opportunity line items
        theODCList = new list<Opportunity_Data_Centre__c>([select Id, Name, of_Cabinets__c from Opportunity_Data_Centre__c where Opportunity__c = : theOpportunity.Id]);
    system.debug('***********theODCLIST 2**'+theODCList);
        
        // check for duplicate data centres
        theCheckDuplicateDataCentres();
    }
    
    public PageReference theSave() {
        
        list<Opportunity_Data_Centre__c> theODCList2Upsert = new list<Opportunity_Data_Centre__c>();
        list<Opportunity_Data_Centre__c> theODCList2Delete = new list<Opportunity_Data_Centre__c>();
        
        // remove if # of cabinets is 0
        for (Opportunity_Data_Centre__c odc : theODCList) {       
            if (odc.of_Cabinets__c > 0) {
                theODCList2Upsert.add(odc);
            } else {
                if (odc.Id != null) theODCList2Delete.add(odc);
            }
        }
        
        try{
            // upsert odcs
            upsert theODCList2Upsert;
            
            // delete odcs
            delete theODCList2Delete;
        } catch (DMLException ex) {
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR, System.Label.Saving_Data_Centre_Error);
            ApexPages.addMessage(msg);
        }
        return null;
    }

    private void theCheckDuplicateDataCentres() {
        
        set<string> theDupeCheck = new set<string>();    
    
        for (Opportunity_Data_Centre__c odc : theODCList) {
            // check for duplicate data centres
            if (theDupeCheck.contains(odc.Name)) {
                ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.WARNING, System.Label.Duplicate_Data_Centres_Error);
                ApexPages.addMessage(msg);
            } else {
                theDupeCheck.add(odc.Name);
            }
        }    
    }

    public PageReference theNew() {
        // create new odc
        Opportunity_Data_Centre__c odc = new Opportunity_Data_Centre__c(Opportunity__c = theOpportunity.Id);
        //odc.Name = 'test';
        odc.of_Cabinets__c = 0;
        theODCList.add(odc);
        
        system.debug('***********theODCLIST 3**'+theODCList);
        // check for duplicate data centres
        theCheckDuplicateDataCentres();
        
        return null;
    }
    
    public PageReference theCancel() {
        // query olis again as we are not saving anything
        theGetODC();
        
        return null;
    }   
    
    public List<SelectOption> getDataCentres() {
        List<SelectOption> options = new List<SelectOption>();
        
        for (string s : thePicklistEntryMap.keySet()) {
            options.add(new SelectOption(s, thePicklistEntryMap.get(s)));
        }
        
        // sort options alphabetically
        options.sort();
        
        return options;
    }     
}