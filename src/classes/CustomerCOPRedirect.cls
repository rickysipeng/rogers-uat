public with sharing class CustomerCOPRedirect {

    

    private EncryptionKey__c ekey;  
    private final ApexPages.StandardController controller;
    private String tempCopId;
    private final Boolean isFrench;
    public String encryptedId {get; set;}
    
    public CustomerCOPRedirect() {
        encryptedId = ApexPages.currentPage().getParameters().get('copId');
    }
    
    public CustomerCOPRedirect(ApexPages.StandardController controller) {
        String encryptedCOPId = ApexPages.currentPage().getParameters().get('copId');
        String language = ApexPages.currentPage().getParameters().get('lang');
        tempCopId = encryptedCOPId;
        this.controller = controller;
        isFrench = language!= null && language.equals('FR');
        
        // Decrypt the COP Id
        encryptedCOPId = EncodingUtil.urlDecode(encryptedCOPId, 'UTF-8');
        encryptedCOPId = encryptedCOPId.replaceAll(' ', '+');
        try{      
            ekey = [SELECT Name, key__c FROM EncryptionKey__c WHERE Name = :encryptedCOPId LIMIT 1];
        }catch(Exception ex){
            
        }
        
    }
    
    public PageReference redirect(){

    PageReference returnURL;
    
    Datetime myDateTime = Datetime.now();
    Long temp = myDateTime.getTime();
    
    // Redirect if Record Type corresponds to custom VisualForce page
    if(ekey == null) {
        returnURL = new PageReference('/apex/COPCompleted');
    }else{
        if (!isFrench)
            returnURL = new PageReference('/apex/CustomerCOPForm');
        else
            returnURL = new PageReference('/apex/CustomerCOPFormFrench');
            
         returnURL.getParameters().put('copId', tempCOPId);
         returnURL.getParameters().put('timestamp', '' + temp);
    }
     
    return returnURL.setRedirect(true);
    }
}