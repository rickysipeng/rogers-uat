@isTest(SeeAllData = true)
public class Test_addAttendee_test {
    private static List<Contact> theAttendeeList;
    private static List<Account> theAccountList;
    
     static testmethod void TestaddRow(){
          dataGenerator(1);
          ApexPages.Standardcontroller sc = New ApexPages.StandardController(theAccountList[0]);
          Test_addAttendee cont = new Test_addAttendee(sc);  
          cont.addRow();
        }

     static testmethod void TestSave(){
          dataGenerator(1);
          ApexPages.Standardcontroller sc = New ApexPages.StandardController(theAccountList[0]);
          Test_addAttendee cont = new Test_addAttendee(sc);  
          cont.Save();
        }    

     static testmethod void TestdeleteRow(){

          integer index=0;
          PageReference reference = Page.Test_AddorRemove;
          reference.getParameters().put('rowIndex', String.valueOf(index));
          Test.setCurrentPage(reference);
          dataGenerator(2);
          ApexPages.Standardcontroller sc = New ApexPages.StandardController(theAccountList[0]);
          Test_addAttendee cont = new Test_addAttendee(sc); 
          cont.addRow(); 
          cont.deleteRow();
        } 
    
     static void dataGenerator (Integer numberOfRecords) {
        
        theAccountList = new List<Account>();       
        for (integer i=0; i<numberOfRecords; i++) {
            Account theAccount = new Account(Name = 'AccountTest'+i,BillingPostalCode= 'A1A 1A1');    
            theAccountList.add(theAccount);

        }
        insert theAccountList;
        
        theAttendeeList = new List <Contact>(); 
        
        for (integer i=0; i<numberOfRecords; i++) {            
            Contact theAttendee = new Contact (firstName = 'theAttendee '+i, 
                                                           lastName = 'test'+i,                                                           
                                                           Email = 'xyz@xyz.com',
                                                            MailingCity = 'Toronto',
                                                            MailingCountry ='CA',
                                                            MailingState='AB',
                                                            Contact_Type__c = 'Executive',
                                                           Phone = '000000000', Account=theAccountList[i]);
            theAttendeeList.add(theAttendee);
            Contact theAttendee2 = new Contact (firstName = 'theAttendee2'+i, 
                                                           lastName = 'test2'+i,                                                           
                                                           Email = 'xyz@xyz.com',
                                                            MailingCity = 'Toronto',
                                                            MailingCountry ='CA',
                                                            MailingState='AB',
                                                            Contact_Type__c = 'Executive',
                                                           Phone = '000000000', Account=theAccountList[i]);
            theAttendeeList.add(theAttendee2);            
        }
         insert theAttendeeList;
        
}
}