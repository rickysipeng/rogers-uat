/*
===============================================================================
 Class Name   : TAG_giveAccessToNewTeam_batch
===============================================================================
PURPOSE:    This batchgives acces to new AccountTeam after MSD and Shared MSD Owners 
            and Members have been updated.
  

Developer: Deepika Rawat
Date: 05/22/2015 

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
05/22/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class TAG_giveAccessToNewTeam_batch implements Database.Batchable<SObject>, Database.stateful{
    global String query;
    
    Global Set<Id> setAccountUpdate = new set<Id>();
    
    global TAG_giveAccessToNewTeam_batch(Set<Id> setAccountUpdate)
    {
        this.setAccountUpdate = setAccountUpdate;  // constructor copies in arg to local instance vbl
    }
    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
        return Database.getQueryLocator('select id,Cascade_Team_to_Child__c,  Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId, Owner.isActive FROM Account where Recalculate_Team__c=true and ParentID=null');
        }
        else{
           return Database.getQueryLocator('select id, Recalculate_Team__c,Account_Team_to_MAL__c, ParentID,OwnerId,Cascade_Team_to_Child__c,  Owner.isActive FROM Account where Recalculate_Team__c=true limit 10');
        }     
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        List<String> lstMSDRoles = TAGsettings.MSD_Recalcuate_Account_Team_Job__c.split(',');
        List<AccountTeamMember> parentAccMSDMembers = [Select Id, UserId, TeamMemberRole, User.isActive, Account.Id from AccountTeamMember where Account.Id in: scope and TeamMemberRole in :lstMSDRoles and User.isActive = true ];
        List<AccountTeamMember> team;
        Set<Id> teamMemIds;
        List<Account> accListToUpdate = new List<Account>();
        Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>(); 
        for(AccountTeamMember teamMem : parentAccMSDMembers){
            teamMemIds = mapAccUserIdsToAdd.get(teamMem.Account.Id);
            if(teamMemIds==null)
                teamMemIds = new Set<Id>();
            teamMemIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.Account.Id,teamMemIds);
        }
        updateAccountTeamAccess accessCls = new updateAccountTeamAccess();
        accessCls.giveAccesstoTeam(mapAccUserIdsToAdd);
        //Update Recalculate_Team__c flag to false
        for(Account acc:scope){
            acc.Recalculate_Team__c = false;
            acc.Cascade_Team_to_Child__c = true;
            acc.Account_Team_to_MAL__c = true;
            accListToUpdate.add(acc);
        }
        Set<Account> setAcctoUpdate = new Set<Account>();
        if(accListToUpdate.size()>0){
            setAcctoUpdate.addAll(accListToUpdate);
            accListToUpdate.clear();
            accListToUpdate.addAll(setAcctoUpdate);
            update accListToUpdate;
        }
    }

    global void finish(Database.BatchableContext BC){
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        Integer batchSize = Integer.ValueOf(TAGsettings.ReCalAccTeamSchChild1BatchSize__c);
        system.debug('*****batchSize-->>'+batchSize);
        TAG_ReCalAccTeamOnChild_new_batch p = new TAG_ReCalAccTeamOnChild_new_batch(setAccountUpdate);
        Database.executeBatch(p,batchSize);
    }
}