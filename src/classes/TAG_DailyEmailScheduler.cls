global class TAG_DailyEmailScheduler implements Schedulable {
   
   global void execute(SchedulableContext ctx) {
        
     // MySchedulableClass c = new MySchedulableClass();
      TAG_DailyDigestEmailController c = new TAG_DailyDigestEmailController ();      //Batch Class Name
      Database.executeBatch(c);
   }   
}