@isTest (seeAllData=true) 
private class TestCRController {
      private static user userRec;
      private static Change_Request__c cr;
      private static CR_Components_Join__c crcomjoin;
      private static Component__c comp;
      private static void setUpData(){
          Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
          userRec = new User(LastName = 'Raj', FirstName='Jeev',Alias = 'aRoger4', Email='test999@Rogertest.com', Username='test999@Rogertest4.com', CommunityNickname = 'nickRog4', ProfileId = profile.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
          insert userRec;
          
          cr= new Change_Request__c ();
          cr.Assigned_Developer__c=userRec.Id;
          insert cr;  
          
          comp= new Component__c();
          comp.Name='Test Component';
          comp.Description__c='Test Description';
          comp.Last_Migrated_On__c=System.Today();
          comp.Last_Migrated_To__c='Test01';
          insert comp;
          
          crcomjoin= new CR_Components_Join__c();
          crcomjoin.Change_Request__c=cr.Id;
          crcomjoin.Component__c=comp.Id;
          insert crcomjoin;
      
      
      }
      
       static testmethod void TestMethod1(){
         test.startTest();
         setUpData();
         //PageReference pageRef = Page.OpportunityProductTemplate;
         //Test.setCurrentPage(pageRef);
         ApexPages.currentPage().getParameters().put('id', cr.id);
         ApexPages.StandardController crcon= new ApexPages.StandardController(cr);
         CRController crCtrl = new CRController (crcon);
         crCtrl.newCompList.add(comp);
         crCtrl.attach();
         crCtrl.createCompNJoin();
         crCtrl.addRows();
         test.stopTest();
         
         }
}