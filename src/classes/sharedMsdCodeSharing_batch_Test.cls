/*
Class: sharedMsdCodeSharing_batch_Test
Test class for sharedMsdCodeSharing_batch.cls

Written by: Jacob Klay, Rogers Communications
Date: 27-Feb-2014 09:00 EST

***Revisions***
Date        |Name               |Description

*/
@isTest (SeeAllData = true)
private class sharedMsdCodeSharing_batch_Test{
    static Account acc;
    static Shared_MSD_Accounts__c sharedMsdAcc;
    static Shared_MSD_Code__c msd; 
    static AccountTeamMember atm;
    static AccountTeamMember atm2;
    static AccountTeamMember atm3;
    static Shared_MSD_Code__share msdShare;
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
 
    static testmethod void testMethod1(){
       
        User thisUser = [ select Id from User where Id = :UserInfo.getUserId() ];
        User u;
        System.runAs ( thisUser ) {
        Profile p = [select id from profile where name='System Administrator' ];
        u = new User(alias = 'TestUser', email='standarduser@testorg.com.rogers', 
            emailencodingkey='UTF-8', lastname='TestUser', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, 
            timezonesidkey='America/Los_Angeles', 
            username='standarduser@testorg.com.Rogers');
        }  
         
        System.runAs(u) {
            acc = new Account();
            acc.Name='TestAcc';
            acc.OwnerId=listUser[0].id;
            acc.Account_Status__c='Assigned';
            acc.BillingCountry= 'CA';
            acc.BillingPostalCode = 'A9A 9A9';
            acc.BillingState = 'MA';
            acc.BillingCity='City';
            acc.BillingStreet='Street';
            insert acc;
            
            msd= new Shared_MSD_Code__c();
            msd.ownerId=listUser[1].id;
            msd.Name = '987654367';
            msd.Description__c = 'TC Shared MSD';
            insert msd;
            
            sharedMsdAcc = new Shared_MSD_Accounts__c();
            sharedMsdAcc.Account__c = acc.id;
            sharedMsdAcc.Shared_MSD_Code__c = msd.Id;
            insert sharedMsdAcc;

            atm= new AccountTeamMember();
            atm.accountId= acc.id;
            atm.userId= listUser[1].id;
            atm.TeamMemberRole='Member';
            insert atm;
            
            atm2= new AccountTeamMember();
            atm2.accountId= acc.id;
            atm2.userId= listUser [0].id;
            atm2.TeamMemberRole='Member';
            insert atm2;
            
            msdShare= new Shared_MSD_Code__share();
            msdShare.UserOrGroupId= listUser[1].id;
            msdShare.ParentID =msd.id;
            msdShare.RowCause='AccountTeamMember__c';
            msdShare.AccessLevel ='Read';  
            insert msdShare;
        }

        System.runAs(u) {
        Database.BatchableContext scc;
        sharedMsdCodeSharing_batch sc = new sharedMsdCodeSharing_batch();           
        Test.StartTest();
        sc.Query ='SELECT Id, Shared_MSD_Code__c, Account__c, Account__r.OwnerId, Shared_MSD_Code__r.OwnerId ' +
                            'FROM Shared_MSD_Accounts__c ' +
                            'WHERE Shared_MSD_Code__c =\''+ msd.id+ '\' ';
        sc.start(scc);
        List<Shared_MSD_Accounts__c> scope = [SELECT Id, Shared_MSD_Code__c, Account__c, Account__r.OwnerId, Shared_MSD_Code__r.OwnerId FROM Shared_MSD_Accounts__c WHERE Shared_MSD_Code__c =: msd.id];
        sc.execute(scc,scope);
        sc.finish(scc);
        //ID batchprocessid = Database.executeBatch(sc);
        Test.StopTest();
        }
    }
}