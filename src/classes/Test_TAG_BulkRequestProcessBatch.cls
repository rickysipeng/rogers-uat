/*
===============================================================================
 Class Name   : Test_TAG_BulkRequestProcessBatch
===============================================================================
PURPOSE:    This class a controller class for TAG_BulkRequestProcessBatch.
              
Developer: Deepika Rawat
Date: 4/23/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
4/23/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_BulkRequestProcessBatch{
    private static List<Assignment_Request__c> lstAssignReq = new List<Assignment_Request__c>();
    private static List<Assignment_Request_Entry__c> lstAREntries = new List<Assignment_Request_Entry__c>();
    private static List<Assignment_Request_Item__c> lstARItems= new List<Assignment_Request_Item__c>();
    private static Assignment_Request_Item__c reqItem;
    private static List<Account> lstaccount = new List<Account>();
    private static List<MSD_Code__c> lstMSDCode = new List<MSD_Code__c>(); 
    private static List<Shared_MSD_Code__c> lstSharedMSD = new List<Shared_MSD_Code__c>();
    private static List<Shared_MSD_Accounts__c> lstAccSharedMSD = new List<Shared_MSD_Accounts__c>();
    private static District__c newDist;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    
    private static void setUpData(){
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Purge_AREntry__c = '0';
        insert TAG_CS;
        User user1 = new User(LastName = 'Mark O’Roger1', Alias = 'er112', Email='test@Rogertest.com', Username='test@Rogertest12.com', CommunityNickname = 'nick23Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Employee_Id__c='Te1Te2Te3', Owner_Type__c='Account', Assignment_Approver__c = UserInfo.getUserId(), Channel__c = 'ABS');
        insert user1;
         User userAccountOwner = new User(LastName = 'Mark O’Rogerdsfd1', ManagerId = user1.id, Alias = 'ty112', Email='test@Rogertest.com', Username='test@Trseegertest12.com', CommunityNickname = 'nicw3Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Assignment_Approver__c = user1.id,Employee_Id__c='Te1Te2Te4', Owner_Type__c='Account',Channel__c = 'ABS' );
        insert userAccountOwner;

        User userDistrictOwner = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1',ManagerId = user1.id, Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c=user1.id, Employee_Id__c='Te1Te2Te5', Owner_Type__c='District', Channel__c = 'ABS');
        insert userDistrictOwner;

        newDist = new District__c();
        newDist.Name='New Distrct';
        newDist.District_External_Id__c ='di2di3di4';
        newDist.District_Owner__c=userDistrictOwner.id;
        insert newDist;

        Account ParentAcc = new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= user1.id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        lstaccount.add(ParentAcc);

        Account ParentAccDistrict= new Account();
        ParentAccDistrict.Name = 'ParentAccDistrict';
        ParentAccDistrict.Account_Status__c = 'Assigned';
        ParentAccDistrict.ParentID= null;
        ParentAccDistrict.District__c =newDist.id;
        ParentAccDistrict.Initial_District__c =newDist.id;
        ParentAccDistrict.OwnerId= user1.id;
        ParentAccDistrict.BillingPostalCode= 'A1A 1A1';
        lstaccount.add(ParentAccDistrict);

        insert lstaccount;

        MSD_Code__c msdCode = new MSD_Code__c();
        msdCode.OwnerId = user1.id ;
        msdCode.Account__c = lstaccount[0].id ;
        msdCode.MSD_Code_External__c = '100101';
        lstMSDCode.add(msdCode);

        MSD_Code__c msdCodeDistrict = new MSD_Code__c();
        msdCodeDistrict.OwnerId = user1.id ;
        msdCodeDistrict.Account__c = lstaccount[1].id ;
        msdCodeDistrict.District__c = newDist.id;
        msdCodeDistrict.MSD_Code_External__c = '100100100';
        lstMSDCode.add(msdCodeDistrict);

        insert lstMSDCode;

        Shared_MSD_Code__c sharedMSD = new Shared_MSD_Code__c();
        sharedMSD.OwnerId = user1.id ;
        sharedMSD.Name = 'SharedMSD1';
        sharedMSD.MSD_Code_External__c = '100100';
        lstSharedMSD.add(sharedMSD);

        Shared_MSD_Code__c sharedMSDDistrict = new Shared_MSD_Code__c();
        sharedMSDDistrict.OwnerId = user1.id ;
        sharedMSDDistrict.Name = 'sharedMSDDistrict';
        sharedMSDDistrict.District__c = newDist.id;
        sharedMSDDistrict.MSD_Code_External__c = '100100101';
        lstSharedMSD.add(sharedMSDDistrict);

        insert lstSharedMSD;

        Shared_MSD_Accounts__c sharedAccMSD = new Shared_MSD_Accounts__c();
        sharedAccMSD.Account__c=lstaccount[0].id;
        sharedAccMSD.Shared_MSD_Code__c=lstSharedMSD[0].id;
        lstAccSharedMSD.add(sharedAccMSD);

        Shared_MSD_Accounts__c sharedAccMSDDistrict = new Shared_MSD_Accounts__c();
        sharedAccMSDDistrict.Account__c=lstaccount[1].id;
        sharedAccMSDDistrict.Shared_MSD_Code__c=lstSharedMSD[1].id;
        lstAccSharedMSD.add(sharedAccMSDDistrict);

        insert lstAccSharedMSD;

        Assignment_Request__c assignOwner = new Assignment_Request__c();
        assignOwner.Approval_Override__c = false;
        assignOwner.Upload_via_Data_Loader__c= true;
        assignOwner.Request_Type__c='Owner';
        assignOwner.Status__c='In-Progress';
        lstAssignReq.add(assignOwner);

        Assignment_Request__c assignDistrict = new Assignment_Request__c();
        assignDistrict.Approval_Override__c = false;
        assignDistrict.Upload_via_Data_Loader__c= true;
        assignDistrict.Request_Type__c='District';
        assignDistrict.Status__c='In-Progress';
        lstAssignReq.add(assignDistrict);

        Assignment_Request__c assignAccTeam = new Assignment_Request__c();
        assignAccTeam.Approval_Override__c = false;
        assignAccTeam.Upload_via_Data_Loader__c= true;
        assignAccTeam.Request_Type__c='Account Team';
        assignAccTeam.Status__c='In-Progress';
        lstAssignReq.add(assignAccTeam);

        Assignment_Request__c assignMSDMember= new Assignment_Request__c();
        assignMSDMember.Approval_Override__c = false;
        assignMSDMember.Upload_via_Data_Loader__c= true;
        assignMSDMember.Request_Type__c='MSD Member';
        assignMSDMember.Status__c='In-Progress';
        lstAssignReq.add(assignMSDMember);

        Assignment_Request__c assignSMSDMember= new Assignment_Request__c();
        assignSMSDMember.Approval_Override__c = false;
        assignSMSDMember.Upload_via_Data_Loader__c= true;
        assignSMSDMember.Request_Type__c='Shared MSD Member';
        assignSMSDMember.Status__c='In-Progress';
        lstAssignReq.add(assignSMSDMember);

        insert lstAssignReq;

        //Account Owner Entry
        Assignment_Request_Entry__c arEntry = new Assignment_Request_Entry__c();
        arEntry.Account__c = lstaccount[0].id;
        arEntry.User__c = userAccountOwner.id;
        arEntry.Assignment_Request__c = lstAssignReq[0].id;
        arEntry.Business_Case__c = 'Test Business Case';
        arEntry.Reason_Code__c = '1';
        lstAREntries.add(arEntry);

        //MSD Owner Entry
        Assignment_Request_Entry__c arEntry1 = new Assignment_Request_Entry__c();
        arEntry1.MSD__c = lstMSDCode[0].id;
        arEntry1.User__c = userAccountOwner.id;
        arEntry1.Assignment_Request__c = lstAssignReq[0].id;
        arEntry1.Business_Case__c = 'Test Business Case';
        arEntry1.Reason_Code__c = '1';
        lstAREntries.add(arEntry1);

        //Shared MSD Owner Entry
        Assignment_Request_Entry__c arEntry2 = new Assignment_Request_Entry__c();
        arEntry2.Shared_MSD__c = lstSharedMSD[0].id;
        arEntry2.User__c = userAccountOwner.id;
        arEntry2.Assignment_Request__c = lstAssignReq[0].id;
        arEntry2.Business_Case__c = 'Test Business Case';
        arEntry2.Reason_Code__c = '1';
        lstAREntries.add(arEntry2);

        //Account District Entry
        Assignment_Request_Entry__c arEntry3 = new Assignment_Request_Entry__c();
        arEntry3.Account__c = lstaccount[0].id;
        arEntry3.New_District__c = newDist.id;
        arEntry3.Assignment_Request__c = lstAssignReq[1].id;
        arEntry3.Business_Case__c = 'Test Business Case';
        arEntry3.Reason_Code__c = '1';
        lstAREntries.add(arEntry3);

        //MSD District Entry
        Assignment_Request_Entry__c arEntry4 = new Assignment_Request_Entry__c();
        arEntry4.MSD__c = lstMSDCode[0].id;
        arEntry4.New_District__c = newDist.id;
        arEntry4.Assignment_Request__c = lstAssignReq[1].id;
        arEntry4.Business_Case__c = 'Test Business Case';
        arEntry4.Reason_Code__c = '1';
        lstAREntries.add(arEntry4);

        //Shared MSD District Entry
        Assignment_Request_Entry__c arEntry5 = new Assignment_Request_Entry__c();
        arEntry5.Shared_MSD__c = lstSharedMSD[0].id;
        arEntry5.New_District__c = newDist.id;
        arEntry5.Assignment_Request__c = lstAssignReq[1].id;
        arEntry5.Business_Case__c = 'Test Business Case';
        arEntry5.Reason_Code__c = '1';
        lstAREntries.add(arEntry5);
        
        //Account Team Entry
        Assignment_Request_Entry__c arEntry6 = new Assignment_Request_Entry__c();
        arEntry6.Account__c = lstaccount[0].id;
        arEntry6.User__c = userDistrictOwner.id;
        arEntry6.Assignment_Request__c = lstAssignReq[2].id;
        arEntry6.Action__c='Add';
        arEntry6.Role__c = 'VAR Account Manager';
        arEntry6.Business_Case__c = 'Test Business Case';
        arEntry6.Reason_Code__c = '1';
        lstAREntries.add(arEntry6);

        //MSD Member Entry
        Assignment_Request_Entry__c arEntry7 = new Assignment_Request_Entry__c();
        arEntry7.MSD__c = lstMSDCode[0].id;
        arEntry7.User__c = userDistrictOwner.id;
        arEntry7.Assignment_Request__c = lstAssignReq[3].id;
        arEntry7.Action__c='Add';
        arEntry7.Role__c = 'Account Development Rep';
        arEntry7.Business_Case__c = 'Test Business Case';
        arEntry7.Reason_Code__c = '1';
        arEntry7.Territory_NPA__c = '1';
        lstAREntries.add(arEntry7);

        //MSD Member Entry
        Assignment_Request_Entry__c arEntry8 = new Assignment_Request_Entry__c();
        arEntry8.Shared_MSD__c = lstSharedMSD[0].id;
        arEntry8.User__c = userDistrictOwner.id;
        arEntry8.Assignment_Request__c = lstAssignReq[4].id;
        arEntry8.Action__c='Add';
        arEntry8.Role__c = 'Account Development Rep';
        arEntry8.Business_Case__c = 'Test Business Case';
        arEntry8.Reason_Code__c = '1';
        arEntry8.Territory_NPA__c = '1';
        lstAREntries.add(arEntry8);

        insert lstAREntries;
    }
     /*******************************************************************************************************
     * @description: This is a positive test method
    *******************************************************************************************************/
    static testmethod void testBatch(){
        setUpData(); 
        test.startTest();
        TAG_BulkRequestProcessBatch sc = new TAG_BulkRequestProcessBatch();
        ID batchprocessid = Database.executeBatch(sc,200);
        TAG_BulkRequestProcessBatch sc2 = new TAG_BulkRequestProcessBatch();
        ID batchprocessid2 = Database.executeBatch(sc2,200);
        test.stopTest();
    }
     static testmethod void testBatch2(){
        setUpData(); 
        test.startTest();
        TAG_BulkRequestProcessBatch sc = new TAG_BulkRequestProcessBatch();
        ID batchprocessid = Database.executeBatch(sc,200);
        lstAssignReq[0].Approval_Override__c = true;
        lstAssignReq[1].Approval_Override__c = true;
        lstAssignReq[2].Approval_Override__c = true;
        lstAssignReq[3].Approval_Override__c = true;
        lstAssignReq[4].Approval_Override__c = true;
        update lstAssignReq;
        test.stopTest();
    }
}