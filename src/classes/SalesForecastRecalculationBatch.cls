/*Class Name :SalesForecastRecalculationBatch.
 *Description : .
 *Created By : Deepika Rawat.
 *Created Date :5/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class SalesForecastRecalculationBatch implements Database.Batchable<SObject>{
     // Start:Declarations---------------------------------------------------------------------------------------
       list<OpportunityStage> oStageList= [select id,ForecastCategoryName,MasterLabel from OpportunityStage];
       public set<string> pipelinelst= new set<string>();
       public set<string> bestCaselst= new set<string>();
       public set<string> commitlst= new set<string>();
       public set<string> closedlst= new set<string>();
        public integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
       public date dT1=system.Today().addDays(-SFCheckPoint);
       global String Query;
       global Database.UpsertResult[] listSFUpsertResult;
       List<SalesForecast_Errors__c> errorList = new List<SalesForecast_Errors__c>();
        
    // End:Declarations----------------------------------------------------------------------------------------
   
     global Database.QueryLocator start(Database.BatchableContext BC){
        try{
            return database.getQueryLocator(query);
        }catch(exception ex){
            return null;
        }
        }

     global void execute(Database.BatchableContext BC,List<SObject> Scope) {
        list<string> lstsaleRepDepMonthQuota= new list<string>();
        list<Sales_Forecast__c> lstSalesForcastupdateQuota= new list<Sales_Forecast__c>();
        list<Sales_Forecast__c> lstSalesForcastupdateMeasurement= new list<Sales_Forecast__c>();
        list<Sales_Quota__c> sQuota1= new list<Sales_Quota__c>();
        list<Sales_Quota__c> sQuota= new list<Sales_Quota__c>();
        list<Sales_Measurement__c> sMeasurement= new list<Sales_Measurement__c>();
        list<Id> lstRltdUsers= new list<Id>();

        for(OpportunityStage oStage :oStageList){
                   if(oStage.ForecastCategoryName=='Pipeline'){
                       pipelinelst.add(oStage.MasterLabel);
                   }
                   if(oStage.ForecastCategoryName=='Best Case'){
                       bestCaselst.add(oStage.MasterLabel);
                   }
                   if(oStage.ForecastCategoryName=='Commit'){
                       commitlst.add(oStage.MasterLabel);
                   }
                   if(oStage.ForecastCategoryName=='Closed'){
                       closedlst.add(oStage.MasterLabel);
                   }
        }
       for(SObject sO:Scope){
            Schema.Sobjecttype inspecttype=sO.Id.getSObjectType();
           // check the Sobject Name and add to the List required
            if(String.Valueof(inspecttype) =='Sales_Quota__c'){
                sQuota1.add((Sales_Quota__c)sO);
            }
            if(String.Valueof(inspecttype) =='Sales_Measurement__c'){
                sMeasurement.add((Sales_Measurement__c)sO);
            }
        }
        if(sQuota1 !=null && sQuota1.size()!=0){
            for(Sales_Quota__c sq : sQuota1){
                String d =String.valueof(sq.Forecast_Month__c);
                Integer dyear = 0;
                Integer dmonth = 0;
                system.debug('=========>d'+d);
                if(d!=null){
                    dyear = Integer.Valueof(d.substring(0, 4));
                    dmonth = Integer.Valueof(d.substring(4, 6));
                }
                Date dt = Date.newInstance(dyear, dmonth, 1);
                if(dt >=dT1){
                    sQuota.add(sq);
                }
            }
        }
        if(sQuota !=null && sQuota.size()!=0){
        for(Sales_Quota__c sQ:sQuota){  
                Sales_Forecast__c sftocreate= new Sales_Forecast__c();                
                String a=string.valueof(sQ.Forecast_Month__c);
                sftocreate.Deployment_Month__c = a.substring(0,6);
                sftocreate.Quota_Family__c=sQ.Quota_Family__c;
                sftocreate.OwnerID=sQ.OwnerID; 
                sftocreate.Quantity_Quota__c=sQ.Quantity__c;
                sftocreate.Revenue_Quota__c=sQ.Revenue__c;
                sftocreate.Start_of_Deployment_Month__c=Date.Valueof(string.valueof(sQ.Forecast_Month__c).SubString(0,4)+'-'+string.valueof(sQ.Forecast_Month__c).SubString(4,6)+'-01 00:00:00');
                lstSalesForcastupdateQuota.add(sftocreate);   
            }
        }
        try{
             if(lstSalesForcastupdateQuota.size()>0){
                 listSFUpsertResult = Database.upsert(lstSalesForcastupdateQuota, false);
                 for(integer i=0; i<lstSalesForcastupdateQuota.size(); i++){
                     if(!(listSFUpsertResult[i].isSuccess())){
                        List<Database.Error> err = listSFUpsertResult[i].getErrors();
                        // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                        //These sharing records are not required and thus an insert exception is acceptable. 
                        if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                            SalesForecast_Errors__c rec = new SalesForecast_Errors__c();
                            rec.Record_Owner__c = lstSalesForcastupdateQuota[i].OwnerId;
                            rec.Job_Name__c = 'Upsert Salesforecast from Sales Quota' ;
                            rec.Error_Message__c = err[0].getMessage();
                            rec.Sales_Measurement_Info__c = 'Sales Quota Owner ID : '+ lstSalesForcastupdateQuota[i].OwnerId + ' Deployment Month : ' +lstSalesForcastupdateQuota[i].Deployment_Month__c+ + ' Quota Family : ' +String.valueof(lstSalesForcastupdateQuota[i].Quota_Family__c);
                            rec.Sales_Forecast__c = lstSalesForcastupdateQuota[i].id;
                            rec.Status_Code__c = String.Valueof(err[0].getStatusCode());
                            errorList.add(rec);                
                        }
                     }
                 }
             }
              if(errorList.size()>0){
                insert errorList;
            }
        }catch(exception ex){
        }

            if(sMeasurement !=null && sMeasurement.size()!=0){
            map<string,decimal> mapSalesQuotaQuantity = new map<string,decimal>();
            map<string,decimal> mapSalesQuotaRevenue = new map<string,decimal>();
            // As per new requirements---------
            map<string,decimal> closedQuantity = new map<string,decimal>();
            map<string,decimal> closedRevenue = new map<string,decimal>();
            map<string,decimal> commitQuantity = new map<string,decimal>();
            map<string,decimal> commitRevenue = new map<string,decimal>();
            map<string,decimal> bestCaseQuantity = new map<string,decimal>();
            map<string,decimal> bestCaseRevenue = new map<string,decimal>();
            map<string,decimal> pipelineQuantity = new map<string,decimal>();
            map<string,decimal> pipelineRevenue = new map<string,decimal>();
            map<string,decimal> totalQuantity = new map<string,decimal>();
            map<string,decimal> totalRevenue = new map<string,decimal>();
            set<string> uniqueKey= new set<string>();
            
            //***Step 6.    In the result records from step 5, get a list of Sales Rep (i.e. Sales Measurement Owner)
            for(Sales_Measurement__c sM:sMeasurement){
                if(sM.OwnerID !=null){
                    lstRltdUsers.add(sM.OwnerID);
                }
            }
  //***Step 7. For each of the Sales Rep in the list above

           // c)    Rollup the Weighted Quantity and Weighted Revenue values in the records of this dat set by Sales Rep + Deployment Month + Quota Family 
                for(Sales_Measurement__c sM:[select id,LastModifiedDate,Quota_Family__c,Quantity__c,Revenue__c,OwnerMonthFamily__c,Deployment_Month__c,OwnerID,Opportunity__r.StageName,Opportunity_Type__c, Opportunity__r.LastModifiedDate,Opportunity__c,Weighted_Revenue__c,Weighted_Quantity__c FROM Sales_Measurement__c where OwnerID IN: lstRltdUsers  AND Opportunity_Type__c !='Renewal']){                   
                    // Get Quantity for each unique value and add the same to total Map
                        if(totalQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=totalQuantity.get(sM.OwnerMonthFamily__c);
                            totalQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            totalQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to Pipeline Map
                        if(totalRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=totalRevenue.get(sM.OwnerMonthFamily__c);
                            totalRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            totalRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                     
                    // closed --------------------------------------------------------------------------------------------
                    if(closedlst.contains(sM.Opportunity__r.StageName)){
                        // Get Quantity for each unique value and add the same to closed Map
                        if(closedQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=closedQuantity.get(sM.OwnerMonthFamily__c);
                            closedQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            closedQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to closed Map
                        if(closedRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=closedRevenue.get(sM.OwnerMonthFamily__c);
                            closedRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            closedRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                    }
                    
                    // commit --------------------------------------------------------------------------------------------
                    if(commitlst.contains(sM.Opportunity__r.StageName)){
                        // Get Quantity for each unique value and add the same to commit Map
                        if(commitQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=commitQuantity.get(sM.OwnerMonthFamily__c);
                            commitQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            commitQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to commit Map
                        if(commitRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=commitRevenue.get(sM.OwnerMonthFamily__c);
                            commitRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            commitRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                    }
                    // BestCase----------------------------------------------------------------------------------------
                    if(bestCaselst.contains(sM.Opportunity__r.StageName)){
                        // Get Quantity for each unique value and add the same to bestCase Map
                            if(bestCaseQuantity.get(sM.OwnerMonthFamily__c) !=null){
                                decimal i=bestCaseQuantity.get(sM.OwnerMonthFamily__c);
                                bestCaseQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                            }else{
                                bestCaseQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                            }
                            // Get Revenue for each unique value and add the same to bextcase Map
                            if(bestCaseRevenue.get(sM.OwnerMonthFamily__c) !=null){
                                decimal i=bestCaseRevenue.get(sM.OwnerMonthFamily__c);
                                bestCaseRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                            }else{
                                bestCaseRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                            }
                    }
                    // Pipeline Map---------------------------------------------------------------------------
                    if(pipelinelst.contains(sM.Opportunity__r.StageName)){
                        if(pipelineQuantity.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=pipelineQuantity.get(sM.OwnerMonthFamily__c);
                            pipelineQuantity.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Quantity__c));
                        }else{
                            pipelineQuantity.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Quantity__c));
                        }
                        // Get Revenue for each unique value and add the same to Pipeline Map

                        if(pipelineRevenue.get(sM.OwnerMonthFamily__c) !=null){
                            decimal i=pipelineRevenue.get(sM.OwnerMonthFamily__c);
                            pipelineRevenue.put(sM.OwnerMonthFamily__c,i+decimal.valueOf(sM.Weighted_Revenue__c));
                        }else{
                            pipelineRevenue.put(sM.OwnerMonthFamily__c,decimal.valueOf(sM.Weighted_Revenue__c));
                        }
                    }
                    
                    uniqueKey.add(sM.OwnerMonthFamily__c);
                }
            for(string s:uniqueKey){
                                    // Addng up Quanity ---
                    decimal j=0;
                    if(bestCaseQuantity.get(s)!=null){
                        j=bestCaseQuantity.get(s);
                    }
                    
                    decimal k=0;
                    if(commitQuantity.get(s) !=null){
                        k=commitQuantity.get(s);
                    }
                    
                    decimal l=0;
                    if(closedQuantity.get(s) !=null){
                        l=closedQuantity.get(s);
                    }
                    decimal m=0;
                    if(pipelineQuantity.get(s) !=null){
                        m=pipelineQuantity.get(s);
                    }
                    pipelineQuantity.put(s,m+j+k);
                    bestCaseQuantity.put(s,j+k+l);
                    commitQuantity.put(s,k+l);
                    // Adding up Revenue ---
                    decimal jR=0;
                    if(bestCaseRevenue.get(s)!=null){
                        jR=bestCaseRevenue.get(s);
                    }       
                    decimal kR=0;
                    if(commitRevenue.get(s)!=null){
                       kR=commitRevenue.get(s);
                    }
                    decimal lR=0;
                    if(closedRevenue.get(s)!=null){
                        lR=closedRevenue.get(s);
                    } 
                    decimal mR=0;
                    if(pipelineRevenue.get(s)!=null){
                        mR=pipelineRevenue.get(s);
                    }
                    pipelineRevenue.put(s,mR+jR+kR);
                    bestCaseRevenue.put(s,jR+kR+lR);
                    commitRevenue.put(s,kR+lR);
            }
 
            Map<string,Sales_Forecast__c> mapSF= new Map<string,Sales_Forecast__c>();
            for(Sales_Forecast__c sF:[select id,Deployment_Month__c,Quota_Family__c,OwnerMonthFamily__c,Best_Case_Quantity__c, Best_Case_Revenue__c,Closed_Quantity__c,Closed_Revenue__c,Commit_Quantity__c,Commit_Revenue__c,Pipeline_Quantity__c, Pipeline_Revenue__c,Total_Quantity__c,Total_Revenue__c,Quantity_Quota__c,Revenue_Quota__c from Sales_Forecast__c Where OwnerMonthFamily__c IN:uniqueKey]){
                mapSF.put(sF.OwnerMonthFamily__c,sF);    
            }
            
            // Use Sales Rep + Deployment Month + Quota Family as the key to query Sales Quota object, get the quota for quantity and revenue, and update the Quantity Quota and Revenue Quota field in the Sales Forecast record 
            for(Sales_Quota__c sQ:[select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c where OwnerMonthFamily__c IN:uniqueKey]){
                mapSalesQuotaQuantity.put(sQ.OwnerMonthFamily__c,integer.valueOf(sQ.Quantity__c));
                mapSalesQuotaRevenue.put(sQ.OwnerMonthFamily__c,integer.valueOf(sQ.Revenue__c));
            }
            //o If find matching record, then update the 10 rollup values
            set<string> setUniqueSalesMeasurements= new set<string>();
            map<string,Sales_Measurement__c> mapSalesM= new map<string,Sales_Measurement__c>();
            for(Sales_Measurement__c sM:sMeasurement){
                setUniqueSalesMeasurements.add(sM.OwnerMonthFamily__c);
                mapSalesM.put(sM.OwnerMonthFamily__c,sM);
            }
            for(string s:setUniqueSalesMeasurements){
               if(mapSF.get(s)!=null){
                    Sales_Forecast__c sftoUpdate=mapSF.get(s);
                    sftoUpdate.Best_Case_Quantity__c=bestCaseQuantity.get(s);
                    sftoUpdate.Best_Case_Revenue__c=bestCaseRevenue.get(s);
                    sftoUpdate.Closed_Quantity__c=closedQuantity.get(s);
                    sftoUpdate.Closed_Revenue__c=closedRevenue.get(s);
                    sftoUpdate.Commit_Quantity__c=commitQuantity.get(s);
                    sftoUpdate.Commit_Revenue__c=commitRevenue.get(s);
                    sftoUpdate.Pipeline_Quantity__c=pipelineQuantity.get(s);
                    sftoUpdate.Pipeline_Revenue__c=pipelineRevenue.get(s);
                    sftoUpdate.Total_Quantity__c=totalQuantity.get(s);
                    sftoUpdate.Total_Revenue__c=totalRevenue.get(s);
                    //7.c.3------------------------------------------------------------------------
                    sftoUpdate.Quantity_Quota__c=mapSalesQuotaQuantity.get(s);
                    sftoUpdate.Revenue_Quota__c=mapSalesQuotaRevenue.get(s);
                    lstSalesForcastupdateMeasurement.add(sftoUpdate);    
                }else{
           //o  If can’t find matching record, then insert one record with     
                    Sales_Forecast__c sftocreate= new Sales_Forecast__c();
                    sftocreate.OwnerID= mapSalesM.get(s).OwnerID;
                    sftocreate.Deployment_Month__c=mapSalesM.get(s).Deployment_Month__c;
                    sftocreate.Start_of_Deployment_Month__c=Date.Valueof(string.valueof(mapSalesM.get(s).Deployment_Month__c).SubString(0,4)+'-'+string.valueof(mapSalesM.get(s).Deployment_Month__c).SubString(4,6)+'-01 00:00:00');
                    sftocreate.Quota_Family__c=mapSalesM.get(s).Quota_Family__c;
                    sftocreate.Best_Case_Quantity__c=bestCaseQuantity.get(s);
                    sftocreate.Best_Case_Revenue__c=bestCaseRevenue.get(s);
                    sftocreate.Closed_Quantity__c=closedQuantity.get(s);
                    sftocreate.Closed_Revenue__c=closedRevenue.get(s);
                    sftocreate.Commit_Quantity__c=commitQuantity.get(s);
                    sftocreate.Commit_Revenue__c=commitRevenue.get(s);
                    sftocreate.Pipeline_Quantity__c=pipelineQuantity.get(s);
                    sftocreate.Pipeline_Revenue__c=pipelineRevenue.get(s);
                    sftocreate.Total_Quantity__c=totalQuantity.get(s);
                    sftocreate.Total_Revenue__c=totalRevenue.get(s);
                    //7.c.3------------------------------------------------------------------------
                    sftocreate.Quantity_Quota__c=mapSalesQuotaQuantity.get(s);
                    sftocreate.Revenue_Quota__c=mapSalesQuotaRevenue.get(s);
                    lstSalesForcastupdateMeasurement.add(sftocreate);
                } 
            }                                              
        }

        try{
                 if(lstSalesForcastupdateMeasurement.size()>0){
                 listSFUpsertResult = Database.upsert(lstSalesForcastupdateMeasurement, false);
                 for(integer i=0; i<lstSalesForcastupdateMeasurement.size(); i++){
                     if(!(listSFUpsertResult[i].isSuccess())){
                        List<Database.Error> err = listSFUpsertResult[i].getErrors();
                        // Check if the error is related to trivial access level. Access levels equal or more permissive than the object's default access level are not allowed. 
                        //These sharing records are not required and thus an insert exception is acceptable. 
                        if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                            SalesForecast_Errors__c rec = new SalesForecast_Errors__c();
                            rec.Record_Owner__c = lstSalesForcastupdateMeasurement[i].OwnerId;
                            rec.Job_Name__c = 'Upsert Salesforecast from Sales Measurement' ;
                            rec.Error_Message__c = err[0].getMessage();
                            rec.Sales_Measurement_Info__c = 'Sales Measurement Owner Id : '+ lstSalesForcastupdateMeasurement[i].OwnerId+' Deployment Month : '+lstSalesForcastupdateMeasurement[i].Deployment_Month__c+ ' Quota Family : '+String.valueof(lstSalesForcastupdateMeasurement[i].Quota_Family__c);
                            rec.Sales_Forecast__c = lstSalesForcastupdateMeasurement[i].id;
                            rec.Status_Code__c = String.Valueof(err[0].getStatusCode());
                            errorList.add(rec);                
                        }
                     }
                 }
             }
              if(errorList.size()>0){
                insert errorList;
            }
            }catch(exception ex){
                }
                
            }
       
        global void finish(Database.BatchableContext BC){
            

            boolean flagvalue = Boolean.valueof(Static_Data_Utilities__c.getInstance('BatchFire').Value__c);
            
            if(flagvalue){
                 BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];
                 OrgWideEmailAddress orgEmailId=[select id from OrgWideEmailAddress limit 1] ;
                List<SalesForecast_Errors__c> errorRecordlist = [Select id , Error_Message__c,Sales_Measurement_Info__c, Job_Name__c, Status_Code__c from SalesForecast_Errors__c];
                string header = 'Job Name,  Sales Forecast Related Info, Error Message, Status Code \n';
                string finalstr = header ;
                for(SalesForecast_Errors__c  erObj: errorRecordlist )
                {
                       string recordString = '"'+erObj.Job_Name__c+'","'+erObj.Sales_Measurement_Info__c+'","'+erObj.Error_Message__c+'","'+erObj.Status_Code__c+'"\n';
                       finalstr = finalstr +recordString;
                }
                Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
                blob csvBlob = Blob.valueOf(finalstr);
                string csvname= 'SalesForecastRecalculation.csv';
                csvAttc.setFileName(csvname);
                csvAttc.setBody(csvBlob);
                Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
                String[] toAddresses = new String[] {admin.email__c}; 
                String body = 'Hi,<br/><br/>Sales Forecast Recalculation Batch Job has been completed. <br/><br/>'; 
                body += 'Please find the attached csv for error records that failed to get inserted in the database.';
                body += '<br/> <br/> Regards, <br/> Salesforce Team'; 
                email.setToAddresses(toAddresses); 
                email.setSubject('Sales Forecast Recalculation Job Status : Completed');        
                email.setSaveAsActivity(false);  
                email.setHtmlBody(body); 
                email.setOrgWideEmailAddressId(orgEmailId.id);
                email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
                Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
            }
            if(!flagvalue){
            SalesForecastRecalculationBatch salesMeasurementObj = new SalesForecastRecalculationBatch();
            if(!Test.isRunningTest()){
            salesMeasurementObj.Query = 'select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c where Deployment_Date__c >=:dT1 AND Opportunity_Type__c !='+'\'Renewal\'';
            }
            else{
            salesMeasurementObj.Query = 'select id, LastModifiedDate, Quota_Family__c, Quantity__c, Revenue__c, OwnerMonthFamily__c, Deployment_Month__c, OwnerID, Deployment_Date__c, Opportunity__r.StageName, Opportunity__r.LastModifiedDate, Opportunity__c, Weighted_Revenue__c, Weighted_Quantity__c FROM Sales_Measurement__c where Deployment_Date__c >=:dT1 limit 2';
            }
            ID salesMeasurementBatchProcessId = Database.executeBatch(salesMeasurementObj,200);
            Static_Data_Utilities__c flagset = Static_Data_Utilities__c.getInstance('BatchFire');
            flagset.Value__c= 'true';
            update flagset;
            }
        }
}