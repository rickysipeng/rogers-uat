public class accountRedirect {

    private ApexPages.StandardController controller;
    public String retURL {get; set;}
    public String saveNewURL {get; set;}
    public String rType {get; set;}
    public String cancelURL {get; set;}
    public String ent {get; set;}
    public String confirmationToken {get; set;}
    public String accountID {get; set;}
    public String contactID {get; set;}

    public accountRedirect(ApexPages.StandardController controller) {
         this.controller = controller;

         retURL = ApexPages.currentPage().getParameters().get('retURL');
         rType = ApexPages.currentPage().getParameters().get('RecordType');
         cancelURL = ApexPages.currentPage().getParameters().get('cancelURL');
         ent = ApexPages.currentPage().getParameters().get('ent');
         confirmationToken = ApexPages.currentPage().getParameters().get('_CONFIRMATIONTOKEN');
         saveNewURL = ApexPages.currentPage().getParameters().get('save_new_url');
         accountID = ApexPages.currentPage().getParameters().get('def_account_id');
         contactID = ApexPages.currentPage().getParameters().get('def_contact_id');
    }

    public PageReference redirect(){

        PageReference returnURL;

       /* if (Utils.isEmpty(rType))
            returnURL = new PageReference('/001/e');
         */   
        // Redirect if Record Type corresponds to custom VisualForce page
        // Channel Prospect Account appends 3D at the start of the record type here but not in the Edit
        if(!Utils.isEmpty(rType) && rType.contains('01230000000wDYF') || rType == '01230000000wDXR') {
              returnURL = new PageReference('/apex/addNewChannelAccount');
        } else {
              returnURL = new PageReference('/001/e');
        }

        returnURL.getParameters().put('retURL', retURL);
        returnURL.getParameters().put('RecordType', rType);
        returnURL.getParameters().put('cancelURL', cancelURL);
        returnURL.getParameters().put('ent', ent);
        returnURL.getParameters().put('_CONFIRMATIONTOKEN', confirmationToken);
        returnURL.getParameters().put('save_new_url', saveNewURL);
        returnURL.getParameters().put('nooverride', '1');

        if(accountID != null){
            returnURL.getParameters().put('def_account_id', accountID);
        }

        returnURL.setRedirect(true);
        return returnURL;

    }
}