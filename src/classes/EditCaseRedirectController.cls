public class EditCaseRedirectController {
    public String caseRecordId{get;set;}
    public Case cObj{get;set;}
    private Account aObj;
    private String RecordTypeName = '';
    public boolean redirect{get; set;}
    public boolean showMessage{get;set;}
    private boolean error = false;
    public boolean closedCaseMsg {get;set;}//Error message when case is closed
    public boolean inProgressCaseMsg {get;set;}//Error message when case is In Progess
    public boolean newCaseMsg {get;set;}//Error message when case is New
    public String profileId = UserInfo.getProfileId();    
    public Profile profile = [select name from profile where id= :profileId];
    public EditCaseRedirectController(ApexPages.StandardController controller){
        caseRecordId= ApexPages.currentPage().getParameters().get('id');
        if(caseRecordId!='' || caseRecordId!=null){
            try {
                cObj=[select RecordTypeId, status, Account.ParentId,New_Parent_Company__c, OwnerId, CaseNumber, Mass_Update_Parent_Account__c from case where id=:caseRecordId];
            }
            catch (System.QueryException e) {
                error = true;
            }
        }
        if(!error) {
            for(RecordType rt:[select id,name from recordtype where id = :cObj.RecordTypeId])
                RecordTypeName = rt.name;
            system.debug('cObj***'+cObj); 
            system.debug('RecordTypeName***'+RecordTypeName); 
            system.debug('cObj.Account.ParentId***'+cObj.Account.ParentId);  
        }
    }

     public pagereference doEditRedirect(){
        PageReference page; 
        if((RecordTypeName=='Account Delink' || RecordTypeName=='Account Link') && (cObj.status =='Approved'|| cObj.status =='Rejected') ){
            closedCaseMsg= true;
            return null;
        }
        //Restrict all users except Sys admin and CRM admin to edit a case with status "in progress"
        else if((RecordTypeName=='Account Delink' || RecordTypeName=='Account Link') && (cObj.status =='In Progress' && (profile.name!='System Administrator' && profile.name!='Rogers CRM Admin'))){
            inProgressCaseMsg= true;
            return null;
        }
        //Restrict all users except Sys admin and CRM admin and Case Owner to edit a case with status "New"
        else if((RecordTypeName=='Account Delink' || RecordTypeName=='Account Link') && (cObj.status =='New' && (profile.name!='System Administrator' && profile.name!='Rogers CRM Admin' && cObj.OwnerId != UserInfo.getUserId()))){
            newCaseMsg= true;
            return null;
        }
        else if(RecordTypeName=='Account Delink' && (cObj.status !='Approved' ||cObj.status !='Rejected')){//for Account Delink record type go to AccountDelinking vf page
            page = new PageReference('/apex/AccountDeLinkingTemplate?loc=500&parentAcc='+cObj.Mass_Update_Parent_Account__c+'&OnEdit=true&OnView=false'); 
            return page;
        }
        else if(RecordTypeName=='Account Link' && (cObj.status !='Approved' ||cObj.status !='Rejected')){//for Account Link record type go to AccountLinking vf page
            page = new PageReference('/apex/AccountLinkingTemplate?loc=500&parentAcc='+cObj.Mass_Update_Parent_Account__c+'&Type=Edit&ID='+cObj.id); 
            return page;
        }

        else{//for all the other record types redirect to the standard new page
            page = new PageReference('/'+caseRecordId+'/e?retURL='+caseRecordId+'&nooverride=1'); 
            return page;
        }
    }
     public pagereference doViewRedirect(){
        PageReference page; 
        if(RecordTypeName=='Account Delink' ){//for Account Delink record type go to AccountDelinking vf page
            page = new PageReference('/apex/AccountDeLinkingTemplate?id='+caseRecordId+'&loc=500&parentAcc='+cObj.Mass_Update_Parent_Account__c+'&OnEdit=false&OnView=True'); 
            return page;
        }
        else if(RecordTypeName=='Account Link' ){//for Account Link record type go to AccountLinking vf page
             page = new PageReference('/apex/AccountLinkingTemplate?loc=500&parentAcc='+cObj.Mass_Update_Parent_Account__c+'&Type=detail&ID='+cObj.id); 
             //page = new PageReference('/'+caseRecordId);
            return page;
        }
        else{//for all the other record types redirect to the standard new page
            page = new PageReference('/'+caseRecordId+'?nooverride=1'); 
            return page;
        }
        
    }

}