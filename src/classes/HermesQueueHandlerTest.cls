@isTest
private class HermesQueueHandlerTest {
    @isTest
    public static void testInsertHermesQueueFromCurrentOrg() {
    	buildHermesQueue();
        Lead[] leads = [SELECT Id FROM Lead];
        System.assertEquals(0, leads.size());    	
    }

    @isTest
    public static void testInsertHermesQueueFromOtherOrg() {
    	HermesQueueHandler.isTestFromOtherOrg = true;
    	buildHermesQueue();
        Lead[] leads = [SELECT Id FROM Lead];
        System.assertEquals(1, leads.size());
    }

    private static Hermes_Queue__c buildHermesQueue() {
        Hermes_Queue__c queue = new Hermes_Queue__c();
        queue.Class_Name__c = HermesLead.CLASS_NAME;
        queue.Source_Name__c = 'Source_Name__c';
        queue.Target_Connection_Name__c = 'TEST';
        queue.Target_Name__c = 'Target_Name__c';

        HermesLead payload = new HermesLead();
        payload.Company = 'test Company';
        payload.LastName = 'test LastName' ;
        queue.Payload__c = payload.toJson();

        insert queue;
        return queue;    	
    }
}