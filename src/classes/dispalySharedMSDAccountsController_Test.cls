/**************************************************************************************
Apex Class Name     : dispalySharedMSDAccountsController_Test
Version             : 1.0 
Created Date        : 2 Feb 2015
Function            : This is the Test Class for dispalySharedMSDAccountsController.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             02/02/2015              Original Version
*************************************************************************************/
@isTest
private class dispalySharedMSDAccountsController_Test
{  
    private static  Account AccountParent;
    private static  Account AccountParent2;
    private static  Shared_MSD_Code__c SharedMsdCode;
    private static  Shared_MSD_Code__c SharedMsdCode2;
    private static  List<Shared_MSD_Accounts__c> listChild;
    private static  Shared_MSD_Accounts__c ParentSharedMSDAccountChild;
       
    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
   
    private static void setUpData()
    {       
        
        AccountParent = new Account();
        AccountParent.Name = 'Test_account_with_SharedMSDchild';
        insert AccountParent; 
        
        AccountParent2 = new Account();
        AccountParent2.Name = 'Test_account_with_SharedMSDchild2';
        insert AccountParent2;
        
        SharedMsdCode = new Shared_MSD_Code__c();
        SharedMsdCode.Name='TestMSD';
        insert SharedMsdCode;
        
        SharedMsdCode2 = new Shared_MSD_Code__c();
        SharedMsdCode2.Name='TestMSD2';
        insert SharedMsdCode2;
        
        listChild = new List<Shared_MSD_Accounts__c>{};
        
        for(Integer i = 0; i < 11; i++){
        Shared_MSD_Accounts__c a = new Shared_MSD_Accounts__c();
        a.Account__c = AccountParent.Id;
        a.Shared_MSD_Code__c = SharedMsdCode.Id;
        listChild.add(a);
        }
        insert listChild;
        
        ParentSharedMSDAccountChild = new Shared_MSD_Accounts__c();
        ParentSharedMSDAccountChild.Account__c = AccountParent2.Id;
        ParentSharedMSDAccountChild.Shared_MSD_Code__c = SharedMsdCode2.Id;
        insert ParentSharedMSDAccountChild;
                         

                         
     }
        
    /*
     Description : This is is a method to test the Search account
     Parameters  : None
     Return Type : void
   
    private static testMethod void DisplayAccount()
    {
        Test.startTest();
        setUpData();
        {                
                ApexPages.CurrentPage().getparameters().put('smsdid', SharedMsdCode.id);
                dispalySharedMSDAccountsController obj = new dispalySharedMSDAccountsController();
                system.debug(' HEY...'+SharedMsdCode.name);
                    system.debug('HEY..From Controller....'+obj.sharedMSDCode.name);
                system.assertEquals(obj.sharedMSDCode.name,SharedMsdCode.name);       
        
         }
        Test.stopTest();     
    }
    
    /*
     Description : This is is a method to test SharedMSDAccounts without children. 
     Parameters  : None
     Return Type : void
     
    private static testMethod void SharedMSDAccountWithoutChildren() {
        Test.startTest();
        setUpData();
        {       
                dispalySharedMSDAccountsController obj = new dispalySharedMSDAccountsController();
                system.assert(obj.lstSharedMSDAcc.size() == 0);
        }
        Test.stopTest();     
    }
   
    /*
     Description : This is is a method to test SharedMSDAccounts with children. 
     Parameters  : None
     Return Type : void
    
    private static testMethod void SharedMSDAccountWithChildren() {
        Test.startTest();
            setUpData();
            {
                    ApexPages.CurrentPage().getparameters().put('smsdid', SharedMsdCode2.id);
                    dispalySharedMSDAccountsController obj = new dispalySharedMSDAccountsController();
                    system.debug('2nd HEY...'+SharedMsdCode2.name);
                    system.debug('HEY..From Controller....'+obj.sharedMSDCode.name);
                    system.assertEquals(obj.sharedMSDCode.name,SharedMsdCode2.name);
            }
        Test.stopTest();     
    } */
 
}