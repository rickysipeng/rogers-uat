/*
===============================================================================
 Class Name   : errorRecordsCleanUp_schedular_Test
===============================================================================
PURPOSE:    Test class for errorRecordsCleanUp_schedular class

Author   : Jacob Klay, Rogers Communications
Date     : 24-June-2o14

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/

@isTest(SeeAllData = true)
private class errorRecordsCleanUp_schedular_Test{
 
 static testmethod void testMethod1(){               
        Test.StartTest();
       if ([SELECT count() FROM AsyncApexJob WHERE JobType='BatchApex' AND (Status = 'Processing' OR Status = 'Preparing')] < 4){ 
         
        errorRecordsCleanUp_schedular sched = new errorRecordsCleanUp_schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('Error Records Deletion', sch, sched);
        System.assert(true);
        }
        Test.stopTest();    
    }
}