/*Class Name  : TestClass_activityLogOnTask .
 *Description : This is test class for trigger activityLogOnTask .
 *Created By  : Deepika Rawat.
 *Created Date :28/01/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = true)
private class TestClass_activityLogOnTask {
    static testmethod void taskmethod(){
        Task t = new Task();
        t.Subject_Type__c='Face to Face meeting';
        
        Task t2 = new Task();
        t2.Subject_Type__c='Prospecting Call - RPC';
        
        Task t3 = new Task();
        t3.Subject_Type__c='Call';
        
        Task t4 = new Task();
        t4.Subject_Type__c='Appointment';
        
         User uSystemAdmin = new User(alias = 'sysad1', email='systemadmin@testorg.com',
         emailencodingkey='UTF-8', lastname='systemadmin123', languagelocalekey='en_US',
         localesidkey='en_US', profileid = [select id from profile where name='System Administrator'].Id, 
         timezonesidkey='America/Los_Angeles', username='systemadmin@testorg.com',Channel__c = 'BIS');
         insert uSystemAdmin;
         
         BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
            bisActNew.OwnerID = uSystemAdmin.Id;
            bisActNew.Subject__c = t2.Subject_Type__c;
            bisActNew.Activity_Date__c = Date.Today();
            bisActNew.Activity_Type__c = 'Appointment';
            bisActNew.RPC_Call__c = FALSE;

        
        Test.starttest();       
            
            system.runAs(uSystemAdmin){
                insert t;
                
                t.Subject_Type__c='Prospecting Call - RPC';        
                update t;
                
                delete t;
                
                
                t2.OwnerId = uSystemAdmin.Id;
                insert t2;
                //bisActNew.Related_Record__c = t2.ID;
                insert bisActNew;
         
                t2.Subject_Type__c='Face to Face meeting';
                T2.Status = 'Completed';
                update t2;
                
                insert t3;
                
                insert t4;
            }
        Test.stoptest();
    
    }
    
    static testmethod void taskmethod2(){
        Task t = new Task();
        t.Subject_Type__c='Face to Face meeting';
        
        Task t2 = new Task();
        t2.Subject_Type__c='Prospecting Call - RPC';
        
        Task t3 = new Task();
        t3.Subject_Type__c='Call';
        
        Task t4 = new Task();
        t4.Subject_Type__c='Appointment';
        
         User uSystemAdmin = new User(alias = 'sysad1', email='systemadmin@testorg.com',
         emailencodingkey='UTF-8', lastname='systemadmin123', languagelocalekey='en_US',
         localesidkey='en_US', profileid = [select id from profile where name='System Administrator'].Id, 
         timezonesidkey='America/Los_Angeles', username='systemadmin@testorg.com',Channel__c = 'BIS');
         insert uSystemAdmin;
         
         BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
            bisActNew.OwnerID = uSystemAdmin.Id;
            bisActNew.Subject__c = t2.Subject_Type__c;
            bisActNew.Activity_Date__c = Date.Today();
            bisActNew.Activity_Type__c = 'Appointment';
            bisActNew.RPC_Call__c = FALSE;

        
        Test.starttest();       
            t2.OwnerId = uSystemAdmin.Id;
            List<Task> listTaask = new list<Task>();
            listTaask.add(t2);
            listTaask.add(t3);
            listTaask.add(t4);
            insert listTaask;
                
            system.runAs(uSystemAdmin){
                insert t;
                
                t.Subject_Type__c='Prospecting Call - RPC';        
                update t;
                
                delete t;

                //bisActNew.Related_Record__c = t2.ID;
                insert bisActNew;
                
                for(task tas: listTaask ){
                tas.Subject_Type__c='Face to Face meeting';
                tas.Status = 'Completed';
                }
                update listTaask;
                
                //insert t3;
                
                //insert t4;
            }
        Test.stoptest();
    
    }
    static testmethod void taskmethod3(){
        List<Task> listTaask = new list<Task>();
        listTaask.add(new task(Subject_Type__c='Face to Face meeting', Status = 'Completed'));
        listTaask.add(new task(Subject_Type__c='Prospecting Call - RPC', Status = 'Completed'));
        listTaask.add(new task(Subject_Type__c='Call', Status = 'Completed'));
        listTaask.add(new task(Subject_Type__c='Appointment', Status = 'Completed'));
        insert listTaask;
        List<BIS_Activity_Tracking__c> listBISTask = new List<BIS_Activity_Tracking__c>();
        for(Task tas: listTaask){
            BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
            //bisActNew.OwnerID = uSystemAdmin.Id;
            bisActNew.Subject__c = tas.Subject_Type__c;
            bisActNew.Activity_Date__c = Date.Today();
            bisActNew.Activity_Type__c = 'Appointment';
            bisActNew.RPC_Call__c = FALSE;
            bisActNew.Related_Record__c = tas.OwnerID;
            listBISTask.add(bisActNew);
        }
        insert listBISTask;
        
        

         Test.starttest();
             User uSystemAdmin = new User(alias = 'sysad1', email='systemadmin@testorg.com',
             emailencodingkey='UTF-8', lastname='systemadmin123', languagelocalekey='en_US',
             localesidkey='en_US', profileid = [select id from profile where name='System Administrator'].Id, 
             timezonesidkey='America/Los_Angeles', username='systemadmin@testorg.com',Channel__c = 'BIS');
             insert uSystemAdmin;
             
             system.runAs(uSystemAdmin){
                 update listTaask;
             }
         Test.stoptest();
    }
    static testmethod void taskmethod4(){
        List<Task> listTaask = new list<Task>();
        listTaask.add(new task(Subject_Type__c='Face to Face meeting', Status = 'Completed'));
        listTaask.add(new task(Subject_Type__c='Prospecting Call - RPC', Status = 'Completed'));
        listTaask.add(new task(Subject_Type__c='Call', Status = 'Completed'));
        listTaask.add(new task(Subject_Type__c='Appointment', Status = 'Completed'));
        insert listTaask;
        List<BIS_Activity_Tracking__c> listBISTask = new List<BIS_Activity_Tracking__c>();
        for(Task tas: listTaask){
            BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
            //bisActNew.OwnerID = uSystemAdmin.Id;
            bisActNew.Subject__c = tas.Subject_Type__c;
            bisActNew.Activity_Date__c = Date.Today();
            bisActNew.Activity_Type__c = 'Appointment';
            bisActNew.RPC_Call__c = FALSE;
            bisActNew.Related_Record__c = tas.OwnerID;
            bisActNew.Deleted__c = true;
            listBISTask.add(bisActNew);
        }
        insert listBISTask;
        
        

         Test.starttest();
             User uSystemAdmin = new User(alias = 'sysad1', email='systemadmin@testorg.com',
             emailencodingkey='UTF-8', lastname='systemadmin123', languagelocalekey='en_US',
             localesidkey='en_US', profileid = [select id from profile where name='System Administrator'].Id, 
             timezonesidkey='America/Los_Angeles', username='systemadmin@testorg.com',Channel__c = 'BIS');
             insert uSystemAdmin;
             
             system.runAs(uSystemAdmin){
                 update listTaask;
             }
         Test.stoptest();
    }
     static testmethod void taskmethod5(){
          Test.starttest();
         ActionPlansUtilities objTest = new ActionPlansUtilities();
         ActionPlan__c action=null;
         ActionPlansUtilities objTest1 = new ActionPlansUtilities(action);
         objTest1.getRelatedObjName();
         objTest1.getActionPlans();
        // objTest1.checkCycleDependent();
       /*  objTest1.saveAp();
         objTest1.checkPendingDeletes();
         objTest1.save();
         objTest1.saveHandler();
         objTest1.saveMultiple();
         objTest1.saveAndNew();
         objTest1.cancel();
         objTest1.getTasks();
         objTest1.addTask();
         objTest1.removeTask();
         objTest1.getTaskSize();
         objTest1.getSubjectItems();
         objTest1.getCompleted();
         objTest1.getVersion();
         objTest1.getCustomSetting();
         objTest1.getObjectKeyPrefix('abc');
         objTest1.getPostObjectBody('abc');
         objTest1.generateObjectFeed('abc');
         objTest1.getRecordOwnerId(action);
         objTest1.isObjectEnabledFeed(action);
         objTest1.isFeedEnabled('abc');
         objTest1.getHoursOption();
         objTest1.retrieveTaskFromExistingActionPlan('abc');
         objTest1.getRelatedObjectOptions();
         objTest1.getDefaultRelatedObjectOption();
         objTest1.relatedObjectId();
         objTest1.truncateId(null);
         objTest1.retrieveOwnersDataRelatedObject(new List<String>(),'abc');
         objTest1.validateChangeStatus('abc');
         objTest1.getDisplayReminder();
         objTest1.getReminderDefaultTime();
         objTest1.deleteActionPlans();
         objTest1.retrieveAPNameFromTemplate(null);
         */
         
         Test.stoptest();
     }
}