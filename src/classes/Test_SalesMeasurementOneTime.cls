@isTest (seeAllData=true) 
private class Test_SalesMeasurementOneTime{
    private static Account accTestObj;
    private static Opportunity oppTestObj;
    private static OpportunityLineItem oli;
    private static OpportunityLineItem oli1;
    private static String standardPriceBookId = '';
     private static user objUser;
    static testmethod void testMethod1(){
            Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        String randomUserName = String.valueOf(System.now().getTime()) + '@test.com';
        objUser = new User(alias = 'test', email='test@test.com',
        emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
        localesidkey='en_US', profileid = profile.Id,
        timezonesidkey='America/Denver', username=randomUserName, isActive=true);
        insert objUser;
       
       /* custSetting= new Product_Type_List__c();
        custSetting.Name='ABS Connectivity';
        custSetting.Form_Type__c='Deployment Form';
        custSetting.Order__c=1;
        custSetting.Product_Category__c='ABS';
        custSetting.Product_Family__c='ABS Connectivity';
        custSetting.Schedule_Type__c='Quality & Revenue';
        insert custSetting;*/
        
       accTestObj = new Account(Name='testAcc');
       accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        accTestObj.Account_Status__c= 'Assigned';
        accTestObj.ParentId = null;
       insert accTestObj;
       
       oppTestObj = new Opportunity(Name='testAcc');
       oppTestObj.StageName='Cloased Won';
       oppTestObj.CloseDate=system.today();
       oppTestObj.AccountID=accTestObj.id;
       insert oppTestObj;
          
        PriceBook2 pb2Standard = [select Id from Pricebook2 where isStandard=true];
        standardPriceBookId = pb2Standard.Id;
                
        // set up product2 and Verify that the results are as expected.
        Product2 p2 = new Product2(Name='Test Product',isActive=true,CanUseQuantitySchedule=true,CanUseRevenueSchedule=true,QuantityScheduleType='Divide',RevenueScheduleType='Repeat', NumberOfRevenueInstallments=5,NumberOfQuantityInstallments=5,QuantityInstallmentPeriod='Daily',RevenueInstallmentPeriod='Daily');
        insert p2;
        Product2 p2ex = [SELECT Name FROM Product2 WHERE Id = :p2.Id];
        System.assertEquals('Test Product', p2ex.Name);
                
        // set up PricebookEntry and Verify that the results are as expected.
        PricebookEntry pbe = new PricebookEntry(Pricebook2Id=standardPriceBookId, Product2Id=p2.Id, UnitPrice=99, isActive=true);
        insert pbe;
        PricebookEntry pbeex = [SELECT Pricebook2Id FROM PricebookEntry WHERE Id = :pbe.Id];
        System.assertEquals(standardPriceBookId, pbeex.Pricebook2Id);
        
        // set up OpportunityLineItem and Verify that the results are as expected.
        oli1 = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=oppTestObj.Id, Quantity=1, TotalPrice=99,Product_Family__c='Data Other',Data_of_Total_Revenue__c=0.5);
        insert oli1;
                
        // set up OpportunityLineItem and Verify that the results are as expected.
        oli = new OpportunityLineItem(PriceBookEntryId=pbe.Id, OpportunityId=oppTestObj.Id, Quantity=1, TotalPrice=99,Product_Family__c='ABS Connectivity Test');
        insert oli;
        OpportunityLineItem oliex = [SELECT PriceBookEntryId FROM OpportunityLineItem WHERE Id = :oli.Id];
        System.assertEquals(pbe.Id, oliex.PriceBookEntryId); 
       
        Date dt = System.today();
        OpportunityLineItemSchedule olisc = new OpportunityLineItemSchedule(OpportunityLineItemId=oli.id,Quantity=2,Revenue=10,type='both',scheduleDate=dt,description='test');
        insert olisc;
        OpportunityLineItemSchedule olisc1 = new OpportunityLineItemSchedule(OpportunityLineItemId=oli1.id,Quantity=2,Revenue=10,type='both',scheduleDate=dt,description='test');
        insert olisc1; 

    
        Test.StartTest();
        SalesMeasurementOneTime  sc = new SalesMeasurementOneTime();                
        sc.Query ='select id,OpportunityLineItemId,ScheduleDate,description,Quantity,Revenue from OpportunityLineItemSchedule  WHERE id =\''+ olisc.id+ '\'  or id =\''+ olisc1.id+'\'';
        ID SalesMeasurementOneTime = Database.executeBatch(sc,200);
        Test.StopTest();

        
    }
}