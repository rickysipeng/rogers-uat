/*
===============================================================================
 Class Name   : Test_TAG_AccountTeamMemberUpdateCont
===============================================================================
PURPOSE:    This class a controller class for TAG_AccountTeamMemberUpdateControllerpage.
              
Developer: Deepika Rawat 
Date: 02/05/2015


CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
02/05/2015           Deepika Rawat               Original Versio
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_AccountTeamMemberUpdateCont{
     private static Account ParentAcc;
     private static Account ParentAcc2;
     private static Account ParentAcc3;
     private static Account ChildAcc1;
     private static List<AccountTeamMember> parentAccTeam;
     private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
     private static User userRec;
     private static User userRec2;
     private static User userRec3;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
     private static Static_Data_Utilities__c staticData_PageSize = new Static_Data_Utilities__c ();
     private static Static_Data_Utilities__c staticData_Limit = new Static_Data_Utilities__c ();
     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Team_Roles_Available_2__c='ECM2,Member2,Account Manager2,Sales Team2,BIS Rep2,Solution Consultant2,Sales Support2,Supporting Executive2,SDC2,SPM2,ISR - Large2,ISR - Medium2,ISR - Small2,Specialist - Medium2,Specialist - Small2,ISR - Public2,Specialist - Large2,Specialist - Public2';
        TAG_CS.User_Location__c = 'Central,Eastern,National,Western';
        TAG_CS.User_Region__c = 'Alberta,Atlantic,British Columbia,National,Ontario,Prairies,Quebec';
        TAG_CS.User_Channel_1__c = 'ABS,BIS,Business Customer Enablement,Business Customer Loyalty,Business Customer Relations,Business National Support,Business Segment,Commercial Service Delivery,Dealer,Enterprise,Enterprise Service Assurance';
        TAG_CS.User_Channel_2__c = 'Enterprise,Service Delivery,Federal,Field Sales Cable,Large,M2M Reseller,Medium,,MSA,NIS,Public Sector,RBS,RCI,Sales Operations,Small,VAR';  
        insert TAG_CS;
        staticData_PageSize.Name = 'AccountTransfer_PageSize';
        staticData_PageSize.Value__c ='1';
        insert staticData_PageSize;
        staticData_Limit.Name='AccountTransfer_RecordsLimit';
        staticData_Limit.Value__c='2';
        insert staticData_Limit;
        User testu = new User(LastName = 'Mark O’Roger1', Alias = 'er112', Email='test@Rogertest.com', Username='test@Rogertest12.com', CommunityNickname = 'nick23Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert testu;

        User testu1 = new User(LastName = 'Mark O’Rogerdsfd1', ManagerId = testu.id, Alias = 'ty112', Email='test@Rogertest.com', Username='test@Trseegertest12.com', CommunityNickname = 'nicw3Rog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', Assignment_Approver__c = testu.id );
        insert testu1;
        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US', ManagerID=testu1.id,  Assignment_Approver__c = testu1.id);
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2',ManagerId = testu1.id, Assignment_Approver__c=userRec.Id, Email='test@Rogertest.com',  Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;

        userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3',Assignment_Approver__c=userRec2.Id,ManagerId = testu1.id, Email='test@Rogertest.com',  Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec3;
        system.runAs(userRec){
        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.BillingPostalCode= 'A1A 1A1';
        insert ChildAcc1;
        //Account 2
        ParentAcc2= new Account();
        ParentAcc2.Name = 'ParentAcc2';
        ParentAcc2.Account_Status__c = 'Assigned';
        ParentAcc2.ParentID= null;
        ParentAcc2.OwnerId= userRec3.id;
        ParentAcc2.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc2;
        
        //Account 3
        ParentAcc3= new Account();
        ParentAcc3.Name = 'ParentAcc3';
        ParentAcc3.Account_Status__c = 'Assigned';
        ParentAcc3.ParentID= null;
        ParentAcc3.OwnerId= userRec3.id;
        ParentAcc3.BillingPostalCode= 'A1A 1A1';
        insert ParentAcc3;
        //instantiate AccountTeamMember List
        parentAccTeam = new List<AccountTeamMember>();
        
        //Add team members for ParentAcc1
        AccountTeamMember newMem = new AccountTeamMember();
            newMem.TeamMemberRole = 'Owner';
            newMem.AccountId = ParentAcc.Id;
            newmem.UserId = ParentAcc.OwnerId; 
        parentAccTeam.add(newMem); 
        AccountTeamMember newMem2 = new AccountTeamMember();
            newMem2 .TeamMemberRole = 'SDC';
            newMem2 .AccountId = ParentAcc.Id;
            newMem2 .UserId = userRec2.Id; 
        parentAccTeam.add(newMem2 );
        //Add team members for ParentAcc2
        AccountTeamMember newMem3 = new AccountTeamMember();
            newMem3.TeamMemberRole = 'Owner';
            newMem3.AccountId = ParentAcc2.Id;
            newMem3.UserId = ParentAcc2.OwnerId; 
        parentAccTeam.add(newMem3);
         //Add team members for ParentAcc3
        AccountTeamMember newMem4 = new AccountTeamMember();
            newMem4.TeamMemberRole = 'Owner';
            newMem4.AccountId = ParentAcc3.Id;
            newMem4.UserId = ParentAcc3.OwnerId; 
        parentAccTeam.add(newMem4);
        //insert AccountTeamMember List
        insert parentAccTeam;    
}
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testSearchAccount(){
        setUpData(); 
        test.startTest();
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='Acc';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            obj.next();
            obj.previous();
            //Add new Member
            obj.addRow();
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with search the as Child account
    *******************************************************************************************************/
    static testmethod void testSearchAccountNegative(){
        setUpData(); 
        test.startTest();
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='Child';
            obj.search();
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with all the search criteria filled, with no record 
                    satisfying the filter criteria
    *******************************************************************************************************/
    static testmethod void testSearchAccountAllFilter(){
        setUpData(); 
        test.startTest();
            system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='Acc';
            obj.searchFilterData.AccExecutive='ABC';
            obj.searchFilterData.Segment='Large';
            obj.searchFilterData.DUNS='12345';
            obj.searchFilterData.accTeamMemRole.TeamMemberRole='SDC';
            obj.searchFilterData.Channel='Business Segment';
            obj.searchFilterData.Region='National';
            obj.searchFilterData.Location='National'; 
            obj.searchFilterData.AccMember='Rog';
            obj.search();
            obj.backtosearch();
            obj.search();
            obj.close();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap ==null); 
            }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name and Add members
    *******************************************************************************************************/
    static testmethod void testAddNewMember(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].member.UserId = userRec2.id;
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].member.UserId = userRec2.id;
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec3.id;
            obj.finalpage();
            obj.createRequest();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
        }
        test.stopTest(); 
    }
     /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as Account name and Add members
    *******************************************************************************************************/
    static testmethod void testAddNewMemberNegative(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].reasonCode =''; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].BusinessCase= '';
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec3.id;
            obj.finalpage();
        }
        test.stopTest(); 
    }
        /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name and Add members and
                    remove a row from confirmation page
    *******************************************************************************************************/
    static testmethod void testAddNewMember3(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].member.UserId = userRec2.id;
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].member.UserId = userRec2.id;
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            obj.finalpage();
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            Apexpages.currentPage().getParameters().put('index','1');
            obj.removeRow();
            obj.removeRowFromConfirmationPage();
            obj.createRequest();

        }  
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name, Add members and
                    remove the newly added member
    *******************************************************************************************************/
    static testmethod void testAddNewMember2(){
        setUpData(); 
        test.startTest();
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRegion();
            obj.getLocation();
            obj.getChannel();
            obj.search();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Add 2 new Members
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].member.TeamMemberRole= 'SDC';
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].member.UserId = userRec2.id;
            obj.listAccountMemberWrap[0].listAddmemberWrap[0].newMemLookup.TAG_New_Owner__c = userRec2.id;
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.addRow();
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].member.TeamMemberRole= 'SDC';
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].member.UserId = userRec2.id;
            obj.listAccountMemberWrap[0].listAddmemberWrap[1].newMemLookup.TAG_New_Owner__c = userRec.id;
            //Remove one of the added members
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            Apexpages.currentPage().getParameters().put('index','1');
            obj.removeRow();
            obj.finalpage();
            obj.backtoStep2();
        
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMember(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.removeExistingMem();
            obj.listAccountMemberWrap[0].listmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
            obj.createRequest();
            List<Assignment_Request_Item__c> aReqItems = [Select Id from Assignment_Request_Item__c where Account__c =:ParentAcc2.id];
            system.assert(aReqItems.size()>0);
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name, remove the 
                    existing member and remove a selected row from confirmation page
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMember2(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.removeExistingMem();
            obj.listAccountMemberWrap[0].listmemberWrap[0].reasonCode ='Test Reason'; 
            obj.listAccountMemberWrap[0].listmemberWrap[0].BusinessCase= 'Test BusinessCode';
            obj.listAccountMemberWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
            obj.listAccountMemberWrap[0].listmemberWrap[0].removed =true;
            obj.undoRemoveItems();
            obj.removeItems();
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as Account name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMemberNegative(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='ParentAcc2';
            obj.search();
            system.assert(obj.showSection2=True);
            system.assert(obj.listAccountMemberWrap.size()>0);
            //Remove Exixting Member
            Apexpages.currentPage().getParameters().put('accId',ParentAcc2.id);
            obj.removeExistingMem();
            obj.listAccountMemberWrap[0].listmemberWrap[0].reasonCode =''; 
            obj.listAccountMemberWrap[0].listmemberWrap[0].BusinessCase='';
            obj.listAccountMemberWrap[0].listmemberWrap[0].del = true;
            obj.finalpage();
        }
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a negative test method with search criteria as Account name, and remove the 
                    existing member.
    *******************************************************************************************************/
    static testmethod void testRemoveExistingMembe2r(){
        setUpData(); 
        test.startTest();
        system.runAs(userRec){
            TAG_AccountTeamMemberUpdateController obj = new TAG_AccountTeamMemberUpdateController();
            obj.getreasonCodevalue();
            obj.getRoles();
            obj.searchFilterData.AccName='Parent';
            obj.search();
        }
        test.stopTest(); 
    }
}