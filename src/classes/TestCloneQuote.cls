/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest
private class TestCloneQuote {

    static Id oppId, sl1Id, sl2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, site7Id; 
    static Id priceBookEntryAccessId1, priceBookEntryInstallId1, quote1Id;
    static Opportunity o, o_carrier;
    static Product2 pAccess, pInstall;
    static Boolean withAssertions = true;
    static Quote q;
    
static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
       
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 carrier_sp = new Pricebook2();
        carrier_sp = [select id from Pricebook2 where Name = :'Carrier PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quote1Id = q.Id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        sl2Id = sl2.Id;
       
        
        Site__c s1 = new Site__c();
        s1.Suite_Floor__c = '11a';
        s1.Street_Name__c = 'Somewhere1';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Suite_Floor__c = '11a';
        s2.Street_Name__c = 'Somewhere2';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
        
        // This site is Serviceable
        Site__c s3 = new Site__c();
        s3.Suite_Floor__c = '11a';
        s3.Street_Name__c = 'Somewhere3';
        s3.Street_Number__c  = '5';
        s3.City__c = 'Coty';
        s3.Postal_Code__c = 'A3A1A1';
        s3.Province_Code__c = 'ON';
        s3.CLLI_SWC__c = '123113';
        s3.ServiceableLocation__c = sl.Id;
        s3.Opportunity__c = oppId;
        insert s3;
        site3Id = s3.Id;
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site2Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site3Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        /* Products for Original Term */
        // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '12-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Access Product
        pAccess  = new Product2();
        pAccess.IsActive = true;
        pAccess.Name = 'Fibre';
        pAccess.Charge_Type__c = 'MRC';
        pAccess.Access_Type__c = 'Ethernet EON';
        pAccess.Access_Type_Group__c = 'Fibre';
        pAccess.Category__c = 'Access';
        pAccess.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pAccess.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccess.Service_Term__c = '12-Months';
        pAccess.Product_Install_Link__c = pInstall.Id;
        pAccess.Mark_Up_Factor__c = 1.0;
        pAccess.Start_Date__c = Date.today();
        pAccess.End_Date__c = Date.today()+1;
        insert pAccess;
        
        
        PricebookEntry pbeAccessStandard = new PricebookEntry();
        pbeAccessStandard.Pricebook2Id = sp1.id;
        pbeAccessStandard.UnitPrice = 60;
        pbeAccessStandard.Product2Id = pAccess.id;
        pbeAccessStandard.IsActive = true;
        pbeAccessStandard.UseStandardPrice = false;
        insert pbeAccessStandard;
        
        PricebookEntry pbeNonStandardAccess = new PricebookEntry();
        pbeNonStandardAccess.Pricebook2Id = sp.id;
        pbeNonStandardAccess.UnitPrice = 60;
        pbeNonStandardAccess.Product2Id = pAccess.id;
        pbeNonStandardAccess.IsActive = true;
        pbeNonStandardAccess.UseStandardPrice = false;
        insert pbeNonStandardAccess;
        priceBookEntryAccessId1 = pbeNonStandardAccess.Id;

        PricebookEntry pbeInstallStandard = new PricebookEntry();
        pbeInstallStandard.Pricebook2Id = sp1.id;
        pbeInstallStandard.UnitPrice = 50;
        pbeInstallStandard.Product2Id = pInstall.id;
        pbeInstallStandard.IsActive = true;
        pbeInstallStandard.UseStandardPrice = false;
        insert pbeInstallStandard;
        
        PricebookEntry pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 50;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
        priceBookEntryInstallId1 = pbeNonStandardInstall.Id;
                       
       QuoteLineItem qliInstall1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
       insert qliInstall1;
       QuoteLineItem qliAccess1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryAccessId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Install__c = qliInstall1.Id);      
       insert qliAccess1;
       
       
        /* Products for a Second Term */
        // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '36-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Access Product
        pAccess  = new Product2();
        pAccess.IsActive = true;
        pAccess.Name = 'Fibre';
        pAccess.Charge_Type__c = 'MRC';
        pAccess.Access_Type__c = 'Ethernet EON';
        pAccess.Access_Type_Group__c = 'Fibre';
        pAccess.Category__c = 'Access';
        pAccess.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pAccess.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccess.Service_Term__c = '36-Months';
        pAccess.Product_Install_Link__c = pInstall.Id;
        pAccess.Mark_Up_Factor__c = 1.0;
        pAccess.Start_Date__c = Date.today();
        pAccess.End_Date__c = Date.today()+1;
        insert pAccess;
        
        pbeAccessStandard = new PricebookEntry();
        pbeAccessStandard.Pricebook2Id = sp1.id;
        pbeAccessStandard.UnitPrice = 160;
        pbeAccessStandard.Product2Id = pAccess.id;
        pbeAccessStandard.IsActive = true;
        pbeAccessStandard.UseStandardPrice = false;
        insert pbeAccessStandard;
        
        pbeNonStandardAccess = new PricebookEntry();
        pbeNonStandardAccess.Pricebook2Id = sp.id;
        pbeNonStandardAccess.UnitPrice = 160;
        pbeNonStandardAccess.Product2Id = pAccess.id;
        pbeNonStandardAccess.IsActive = true;
        pbeNonStandardAccess.UseStandardPrice = false;
        insert pbeNonStandardAccess;

        pbeInstallStandard = new PricebookEntry();
        pbeInstallStandard.Pricebook2Id = sp1.id;
        pbeInstallStandard.UnitPrice = 150;
        pbeInstallStandard.Product2Id = pInstall.id;
        pbeInstallStandard.IsActive = true;
        pbeInstallStandard.UseStandardPrice = false;
        insert pbeInstallStandard;
        
        pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 150;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
    }

    

    static testMethod void testCloneQuote() {
        PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        Test.startTest();
        controller.returnToQuote();
        String test1 = controller.ExpirationDateAsString;
        String test2 = controller.currentDateAsString;
        
        // Test One - It will use the existing term 13 -> uses 12
        controller.actualterm = '13';
        controller.quoteName = 'Quote Test 1';
        controller.cloneQuote();
        
        
        Quote testQuote = [SELECT Id, Name, TotalPrice FROM Quote WHERE Id = :controller.clonedQuote.id];
        List<QuoteLineItem> lineItems = [SELECT Id, TotalPrice FROM QuoteLineItem WHERE quoteId = :testQuote.id];
       
        
        
        // Test Two - It will use the existing term 12 -> uses 12
        controller = new CloneQuoteExtension(sc);
        
        controller.actualterm = '12';
        controller.quoteName = 'Quote Test 2';
        controller.cloneQuote();
        
        
        testQuote = [SELECT Id, Name, TotalPrice FROM Quote WHERE Id = :controller.clonedQuote.id];
        lineItems = [SELECT Id, TotalPrice FROM QuoteLineItem WHERE quoteId = :testQuote.id];
       // System.assertEquals(testQuote.TotalPrice, 110);
      //  System.assertEquals(lineItems.size(), 2);
        
        
        // Test Three - It will use the a new term 37 -> uses 36
        controller = new CloneQuoteExtension(sc);
        
        controller.actualterm = '37';
        controller.quoteName = 'Quote Test 3';
        controller.cloneQuote();
        Test.stopTest();
        
    }
    
    static testMethod void testCloneMinMaxInstallQuote() {
            // Access Product
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        Test.startTest();
        Product2 pAccessMin  = new Product2();
        pAccessMin.IsActive = true;
        pAccessMin.Name = 'Fibre';
        pAccessMin.Charge_Type__c = 'MRC';
        pAccessMin.Access_Type__c = 'Ethernet EON';
        pAccessMin.Access_Type_Group__c = 'Fibre';
        pAccessMin.Category__c = 'Access';
        pAccessMin.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Min';
        pAccessMin.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessMin.Service_Term__c = '36-Months';
        pAccessMin.Product_Install_Link__c = pInstall.Id;
        pAccessMin.Mark_Up_Factor__c = 1.0;
        pAccessMin.Start_Date__c = Date.today();
        pAccessMin.End_Date__c = Date.today()+1;
        pAccessMin.isMinSpeed__c = true;
        insert pAccessMin;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        PricebookEntry pbeAccessMinStandard = new PricebookEntry();
        pbeAccessMinStandard.Pricebook2Id = sp1.id;
        pbeAccessMinStandard.UnitPrice = 160;
        pbeAccessMinStandard.Product2Id = pAccessMin.id;
        pbeAccessMinStandard.IsActive = true;
        pbeAccessMinStandard.UseStandardPrice = false;
        insert pbeAccessMinStandard;
        
        PricebookEntry pbeNonStandardMinAccess = new PricebookEntry();
        pbeNonStandardMinAccess.Pricebook2Id = sp.id;
        pbeNonStandardMinAccess.UnitPrice = 160;
        pbeNonStandardMinAccess.Product2Id = pAccessMin.id;
        pbeNonStandardMinAccess.IsActive = true;
        pbeNonStandardMinAccess.UseStandardPrice = false;
        insert pbeNonStandardMinAccess;
        
        Product2 pAccessMax  = new Product2();
        pAccessMax.IsActive = true;
        pAccessMax.Name = 'Fibre';
        pAccessMax.Charge_Type__c = 'MRC';
        pAccessMax.Access_Type__c = 'Ethernet EON';
        pAccessMax.Access_Type_Group__c = 'Fibre';
        pAccessMax.Category__c = 'Access';
        pAccessMax.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Max';
        pAccessMax.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessMax.Service_Term__c = '36-Months';
        pAccessMax.Product_Install_Link__c = pInstall.Id;
        pAccessMax.Mark_Up_Factor__c = 1.0;
        pAccessMax.Start_Date__c = Date.today();
        pAccessMax.End_Date__c = Date.today()+1;
        pAccessMax.isMaxSpeed__c = true;
        pAccessMax.Parent__c = pAccessMin.Id;
        insert pAccessMax;
        
        PricebookEntry pbeAccessMaxStandard = new PricebookEntry();
        pbeAccessMaxStandard.Pricebook2Id = sp1.id;
        pbeAccessMaxStandard.UnitPrice = 160;
        pbeAccessMaxStandard.Product2Id = pAccessMax.id;
        pbeAccessMaxStandard.IsActive = true;
        pbeAccessMaxStandard.UseStandardPrice = false;
        insert pbeAccessMaxStandard;
        
        PricebookEntry pbeNonStandardMaxAccess = new PricebookEntry();
        pbeNonStandardMaxAccess.Pricebook2Id = sp.id;
        pbeNonStandardMaxAccess.UnitPrice = 160;
        pbeNonStandardMaxAccess.Product2Id = pAccessMax.id;
        pbeNonStandardMaxAccess.IsActive = true;
        pbeNonStandardMaxAccess.UseStandardPrice = false;
        insert pbeNonStandardMaxAccess;
        
    
        QuoteLineItem qliInstall2 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false, index__c = 3, Product__c = pAccessMin.Id);                       
       insert qliInstall2;
       
       QuoteLineItem qliMax = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardMaxAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false, Product__c = pAccessMin.Id, index__c = 2);      
       insert qliMax;
       
       QuoteLineItem qliMin = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardMinAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Max_Speed__c = qliMax.Id, index__c = 1, Quote_Line_Item_Install__c = qliInstall2.Id );                       
       insert qliMin;
       
    
       
       PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        controller.actualterm = '36';
        controller.quoteName = 'Quote Test Min and Max';
        controller.cloneQuote();
        
        Test.stopTest();
    
    }
    
    static testMethod void testCloneQuoteWithMinandMax() {
        // Access Product
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        Test.startTest();
        Product2 pAccessMin  = new Product2();
        pAccessMin.IsActive = true;
        pAccessMin.Name = 'Fibre';
        pAccessMin.Charge_Type__c = 'MRC';
        pAccessMin.Access_Type__c = 'Ethernet EON';
        pAccessMin.Access_Type_Group__c = 'Fibre';
        pAccessMin.Category__c = 'Access';
        pAccessMin.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Min';
        pAccessMin.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessMin.Service_Term__c = '36-Months';
        pAccessMin.Product_Install_Link__c = pInstall.Id;
        pAccessMin.Mark_Up_Factor__c = 1.0;
        pAccessMin.Start_Date__c = Date.today();
        pAccessMin.End_Date__c = Date.today()+1;
        pAccessMin.isMinSpeed__c = true;
        insert pAccessMin;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        PricebookEntry pbeAccessMinStandard = new PricebookEntry();
        pbeAccessMinStandard.Pricebook2Id = sp1.id;
        pbeAccessMinStandard.UnitPrice = 160;
        pbeAccessMinStandard.Product2Id = pAccessMin.id;
        pbeAccessMinStandard.IsActive = true;
        pbeAccessMinStandard.UseStandardPrice = false;
        insert pbeAccessMinStandard;
        
        PricebookEntry pbeNonStandardMinAccess = new PricebookEntry();
        pbeNonStandardMinAccess.Pricebook2Id = sp.id;
        pbeNonStandardMinAccess.UnitPrice = 160;
        pbeNonStandardMinAccess.Product2Id = pAccessMin.id;
        pbeNonStandardMinAccess.IsActive = true;
        pbeNonStandardMinAccess.UseStandardPrice = false;
        insert pbeNonStandardMinAccess;
        
        Product2 pAccessMax  = new Product2();
        pAccessMax.IsActive = true;
        pAccessMax.Name = 'Fibre';
        pAccessMax.Charge_Type__c = 'MRC';
        pAccessMax.Access_Type__c = 'Ethernet EON';
        pAccessMax.Access_Type_Group__c = 'Fibre';
        pAccessMax.Category__c = 'Access';
        pAccessMax.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Max';
        pAccessMax.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessMax.Service_Term__c = '36-Months';
        pAccessMax.Product_Install_Link__c = pInstall.Id;
        pAccessMax.Mark_Up_Factor__c = 1.0;
        pAccessMax.Start_Date__c = Date.today();
        pAccessMax.End_Date__c = Date.today()+1;
        pAccessMax.isMaxSpeed__c = true;
        pAccessMax.Parent__c = pAccessMin.Id;
        insert pAccessMax;
        
        PricebookEntry pbeAccessMaxStandard = new PricebookEntry();
        pbeAccessMaxStandard.Pricebook2Id = sp1.id;
        pbeAccessMaxStandard.UnitPrice = 160;
        pbeAccessMaxStandard.Product2Id = pAccessMax.id;
        pbeAccessMaxStandard.IsActive = true;
        pbeAccessMaxStandard.UseStandardPrice = false;
        insert pbeAccessMaxStandard;
        
        PricebookEntry pbeNonStandardMaxAccess = new PricebookEntry();
        pbeNonStandardMaxAccess.Pricebook2Id = sp.id;
        pbeNonStandardMaxAccess.UnitPrice = 160;
        pbeNonStandardMaxAccess.Product2Id = pAccessMax.id;
        pbeNonStandardMaxAccess.IsActive = true;
        pbeNonStandardMaxAccess.UseStandardPrice = false;
        insert pbeNonStandardMaxAccess;
        
       
       QuoteLineItem qliMax = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardMaxAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false, Product__c = pAccessMin.Id);      
       insert qliMax;
       
       QuoteLineItem qliMin = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardMinAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Max_Speed__c = qliMax.Id);                       
       insert qliMin;
       
       PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        controller.actualterm = '36';
        controller.quoteName = 'Quote Test Min and Max';
        controller.cloneQuote();
        
        Test.stopTest();
        
    }
    
    static testMethod void testCloneQuoteWithOverage() {
        // Access Product
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        Test.startTest();
        
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        
        
        Product2 pAccessOverage  = new Product2();
        pAccessOverage.IsActive = true;
        pAccessOverage.Name = 'Fibre';
        pAccessOverage.Charge_Type__c = 'MRC';
        pAccessOverage.Access_Type__c = 'Ethernet EON';
        pAccessOverage.Access_Type_Group__c = 'Fibre';
        pAccessOverage.Category__c = 'Access';
        pAccessOverage.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Overage';
        pAccessOverage.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessOverage.Service_Term__c = '36-Months';
        pAccessOverage.Mark_Up_Factor__c = 1.0;
        pAccessOverage.Start_Date__c = Date.today();
        pAccessOverage.End_Date__c = Date.today()+1;
        insert pAccessOverage;
        
        PricebookEntry pbeAccessOverageStandard = new PricebookEntry();
        pbeAccessOverageStandard.Pricebook2Id = sp1.id;
        pbeAccessOverageStandard.UnitPrice = 160;
        pbeAccessOverageStandard.Product2Id = pAccessOverage.id;
        pbeAccessOverageStandard.IsActive = true;
        pbeAccessOverageStandard.UseStandardPrice = false;
        insert pbeAccessOverageStandard;
        
        PricebookEntry pbeNonStandardOverageAccess = new PricebookEntry();
        pbeNonStandardOverageAccess.Pricebook2Id = sp.id;
        pbeNonStandardOverageAccess.UnitPrice = 160;
        pbeNonStandardOverageAccess.Product2Id = pAccessOverage.id;
        pbeNonStandardOverageAccess.IsActive = true;
        pbeNonStandardOverageAccess.UseStandardPrice = false;
        insert pbeNonStandardOverageAccess;
        
       Product2 pAccessMin  = new Product2();
        pAccessMin.IsActive = true;
        pAccessMin.Name = 'Fibre';
        pAccessMin.Charge_Type__c = 'MRC';
        pAccessMin.Access_Type__c = 'Ethernet EON';
        pAccessMin.Access_Type_Group__c = 'Fibre';
        pAccessMin.Category__c = 'Access';
        pAccessMin.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Min';
        pAccessMin.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessMin.Service_Term__c = '36-Months';
        pAccessMin.Mark_Up_Factor__c = 1.0;
        pAccessMin.Start_Date__c = Date.today();
        pAccessMin.Product_Overage_Link__c = pAccessOverage.Id;
        pAccessMin.End_Date__c = Date.today()+1;
        insert pAccessMin;
        
        PricebookEntry pbeAccessMinStandard = new PricebookEntry();
        pbeAccessMinStandard.Pricebook2Id = sp1.id;
        pbeAccessMinStandard.UnitPrice = 160;
        pbeAccessMinStandard.Product2Id = pAccessMin.id;
        pbeAccessMinStandard.IsActive = true;
        pbeAccessMinStandard.UseStandardPrice = false;
        insert pbeAccessMinStandard;
        
        PricebookEntry pbeNonStandardMinAccess = new PricebookEntry();
        pbeNonStandardMinAccess.Pricebook2Id = sp.id;
        pbeNonStandardMinAccess.UnitPrice = 160;
        pbeNonStandardMinAccess.Product2Id = pAccessMin.id;
        pbeNonStandardMinAccess.IsActive = true;
        pbeNonStandardMinAccess.UseStandardPrice = false;
        insert pbeNonStandardMinAccess;
       
       QuoteLineItem qliOverage = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardOverageAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);      
       insert qliOverage;

       
       QuoteLineItem qliMin = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardMinAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Overage__c = qliOverage.Id);                       
       insert qliMin;

       
       PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        controller.actualterm = '36';
        controller.quoteName = 'Quote Test Min and Overage';
        controller.cloneQuote();
        
        Test.stopTest();
        
    }
    
        static testMethod void testCloneQuoteWithBundle() {
        // Access Product
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
        Test.startTest();
        
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        
        
        Product2 pAccessBundleParent  = new Product2();
        pAccessBundleParent.IsActive = true;
        pAccessBundleParent.Name = 'Fibre';
        pAccessBundleParent.Charge_Type__c = 'MRC';
        pAccessBundleParent.Access_Type__c = 'Ethernet EON';
        pAccessBundleParent.Access_Type_Group__c = 'Fibre';
        pAccessBundleParent.Category__c = 'Access';
        pAccessBundleParent.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Overage';
        pAccessBundleParent.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessBundleParent.Service_Term__c = '36-Months';
        pAccessBundleParent.Mark_Up_Factor__c = 1.0;
        pAccessBundleParent.Start_Date__c = Date.today();
        pAccessBundleParent.End_Date__c = Date.today()+1;
        pAccessBundleParent.Is_a_Bundle__c = true;
        insert pAccessBundleParent;
        
        PricebookEntry pbeAccessOverageStandard = new PricebookEntry();
        pbeAccessOverageStandard.Pricebook2Id = sp1.id;
        pbeAccessOverageStandard.UnitPrice = 160;
        pbeAccessOverageStandard.Product2Id = pAccessBundleParent.id;
        pbeAccessOverageStandard.IsActive = true;
        pbeAccessOverageStandard.UseStandardPrice = false;
        insert pbeAccessOverageStandard;
        
        PricebookEntry pbeNonStandardOverageAccess = new PricebookEntry();
        pbeNonStandardOverageAccess.Pricebook2Id = sp.id;
        pbeNonStandardOverageAccess.UnitPrice = 160;
        pbeNonStandardOverageAccess.Product2Id = pAccessBundleParent.id;
        pbeNonStandardOverageAccess.IsActive = true;
        pbeNonStandardOverageAccess.UseStandardPrice = false;
        insert pbeNonStandardOverageAccess;
        
       Product2 pAccessMin  = new Product2();
        pAccessMin.IsActive = true;
        pAccessMin.Name = 'Fibre';
        pAccessMin.Charge_Type__c = 'MRC';
        pAccessMin.Access_Type__c = 'Ethernet EON';
        pAccessMin.Access_Type_Group__c = 'Fibre';
        pAccessMin.Category__c = 'Access';
        pAccessMin.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test Min';
        pAccessMin.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccessMin.Service_Term__c = '36-Months';
        pAccessMin.Mark_Up_Factor__c = 1.0;
        pAccessMin.Parent_Promo__c = pAccessBundleParent.Id;
        pAccessMin.Start_Date__c = Date.today();
        pAccessMin.End_Date__c = Date.today()+1;
        pAccessMin.Is_a_Bundle__c = true;
        insert pAccessMin;
        
        PricebookEntry pbeAccessMinStandard = new PricebookEntry();
        pbeAccessMinStandard.Pricebook2Id = sp1.id;
        pbeAccessMinStandard.UnitPrice = 160;
        pbeAccessMinStandard.Product2Id = pAccessMin.id;
        pbeAccessMinStandard.IsActive = true;
        pbeAccessMinStandard.UseStandardPrice = false;
        insert pbeAccessMinStandard;
        
        PricebookEntry pbeNonStandardMinAccess = new PricebookEntry();
        pbeNonStandardMinAccess.Pricebook2Id = sp.id;
        pbeNonStandardMinAccess.UnitPrice = 160;
        pbeNonStandardMinAccess.Product2Id = pAccessMin.id;
        pbeNonStandardMinAccess.IsActive = true;
        pbeNonStandardMinAccess.UseStandardPrice = false;
        insert pbeNonStandardMinAccess;
       
       QuoteLineItem qliBundleParent = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardOverageAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false, Quote_Line_Item_Promo_Parent__c = true);      
       insert qliBundleParent;

       
       QuoteLineItem qliMin = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = pbeNonStandardMinAccess.Id, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item__c = qliBundleParent.Id);                       
       insert qliMin;

       
       PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        
        controller.actualterm = '36';
        controller.quoteName = 'Quote Test Min and Overage';
        controller.cloneQuote();
        
        Test.stopTest();
        
    }
    static testMethod void testCloneCoverCode1() {
        Test.startTest();
        ApexPages.StandardController sc = new ApexPages.standardController(q);
        CloneQuoteExtension controller = new CloneQuoteExtension(sc);
        controller.q=null;
        
        Test.stopTest();
    }
     static testMethod void testCloneCoverCode2() {
        Test.startTest();
        PageReference pageRef = New PageReference('/apex/CloneQuote?oppId=' + String.valueOf(oppId) + '&quoteId=' + quote1Id + '&quoteName=' + q.Name); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc1 = new ApexPages.standardController(q);
        CloneQuoteExtension controller1 = new CloneQuoteExtension(sc1);
        controller1.q=null;
        Test.stopTest();
    }
}