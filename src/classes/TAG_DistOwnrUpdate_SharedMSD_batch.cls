/*
===============================================================================
 Class Name   : TAG_DistOwnrUpdate_SharedMSD_batch 
===============================================================================
PURPOSE:  (This Class will support the logic for District Owner before Update Trigger.)  
            Get list of all Shared MSD Codes for a District Owner and 
            update them with owner and set a flag which is used for MAL integration.
  

Developer: Aakanksha Patel
Date: 06/29/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
06/29/2015           Aakanksha Patel             Original Version
===============================================================================
*/
global class TAG_DistOwnrUpdate_SharedMSD_batch implements Database.Batchable<SObject>, Database.stateful{
    
    Global String query;
    Global Map<ID,District__c> mapIdDistrict;
    Global set<ID> accountIds = new set<ID>();
        
    global TAG_DistOwnrUpdate_SharedMSD_batch(Map<ID,District__c> mapIdDistrict,set<ID> accountIds)
    {
        this.mapIdDistrict = mapIdDistrict; 
        this.accountIds = accountIds;           // constructor copies in arg to local instance vbl
    }
      
    global Database.QueryLocator start(Database.BatchableContext BC)
    {
        set<Id>  tempSet = new Set<Id>();
        tempSet = mapIdDistrict.keyset();
       
       if(!Test.isRunningTest()){
        return Database.getQueryLocator('Select Shared_MSD_to_MAL__c, OwnerId, Id, District__r.District_Owner__c, District__c From Shared_MSD_Code__c where District__c IN: tempSet');
        }
        else{
           return Database.getQueryLocator('Select Shared_MSD_to_MAL__c, OwnerId, Id, District__r.District_Owner__c, District__c From Shared_MSD_Code__c where District__c IN: tempset limit 10');
        }     
    }
    
    global void execute(Database.BatchableContext BC, List<Shared_MSD_Code__c> scope)
    {
        Account acc1 = new Account();
        Set<ID> setSmsdIds = new Set<ID>(); 
        
        Set<Account> setaccountUpdate = new Set<Account>();
        Set<Shared_MSD_Code__c> setSMsdOwnerUpdate = new Set<Shared_MSD_Code__c>();
        map<id, Account> mapsmsdAccount = new map<id,Account>();
        
        
        for(Shared_MSD_Code__c smsd : scope ) 
        {
            setSmsdIds.add(smsd.id);
        }
        list<Shared_MSD_Accounts__c> newListSMA = new list<Shared_MSD_Accounts__c>([select id,Account__c,Account__r.Id,Account__r.Recalculate_Team__c,Shared_MSD_Code__c from Shared_MSD_Accounts__c where Shared_MSD_Code__c IN:setSmsdIds]);
       
        for(Shared_MSD_Code__c sharedmsdRec: scope) 
        {
            if(sharedmsdRec.District__r.District_Owner__c!=null)
            {
                sharedmsdRec.OwnerId = sharedmsdRec.District__r.District_Owner__c;
            }
            sharedmsdRec.Shared_MSD_to_MAL__c = TRUE;
            for(Shared_MSD_Accounts__c lstSMSDAccount :newListSMA)
            {
                if(lstSMSDAccount.Account__r!=null)
                {
                    acc1 = lstSMSDAccount.Account__r;
                    if(!accountIds.contains(acc1.id))
                    {
                        acc1.Recalculate_Team__c = TRUE;                
                        setaccountUpdate.add(acc1);
                    }
                }
            }
            setSMsdOwnerUpdate.add(sharedmsdRec);
            
        }
        update scope;
        
        if(setaccountUpdate.size()!=NULL)
        {
            List<Account> listAccountUpdate = new List<Account>();
            listAccountUpdate.addAll(setaccountUpdate);
            set<Account> temp = new set<Account>(listAccountUpdate);

            listAccountUpdate.clear();
            listAccountUpdate.addAll(temp);
            update listAccountUpdate;
        }
        if(setSMsdOwnerUpdate.size()!=NULL)
        {
            List<Shared_MSD_Code__c> listSMSDUpdate = new List<Shared_MSD_Code__c>();
            listSMSDUpdate.addAll(setSMsdOwnerUpdate);
            set<Shared_MSD_Code__c> temp = new set<Shared_MSD_Code__c>(listSMSDUpdate);

            listSMSDUpdate.clear();
            listSMSDUpdate.addAll(temp);
            update listSMSDUpdate;
        }
    
    }
    
    global void finish(Database.BatchableContext BC)
    {       
       
    }
}