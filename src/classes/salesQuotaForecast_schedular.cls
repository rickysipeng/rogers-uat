/*Class Name :salesQuotaForecast_schedular.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :30/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class salesQuotaForecast_schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        // Step 1.  Retrieve [Quota Update Checking Period],  let’s use the example of [Quota Update Checking Period] = 2
        integer quCheckPoint= Integer.ValueOf(Static_Data_Utilities__c.getInstance('Quota Update Checking Period').Value__c);
        dateTime dTquCheckPoint = system.now().addDays(-quCheckPoint);
        //***Step 2. Query the Sales Quota object to get the all records that are updated between (Today-2days) and Today, inclusively.
        salesQuotaForecast_batch salesQuotaObj = new salesQuotaForecast_batch();
        if(!Test.isRunningTest()){
        salesQuotaObj.Query = 'select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c where LastModifiedDate >=:dTquCheckPoint';
        }else{
        salesQuotaObj.Query = 'select id,LastModifiedDate,Owner.FirstName,Owner.LastName,Quota_Family__c,Forecast_Month__c,OwnerID,Quantity__c,Revenue__c,OwnerMonthFamily__c FROM Sales_Quota__c where LastModifiedDate >=:dTquCheckPoint LIMIT 1';
        }
        ID salesQuotaBatchProcessId = Database.executeBatch(salesQuotaObj,25);
        
    }

}