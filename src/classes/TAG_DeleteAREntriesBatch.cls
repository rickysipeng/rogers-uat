/*
===============================================================================
 Class Name   : TAG_DeleteAREntriesBatch 
===============================================================================
PURPOSE:    This batch class deletes all AR Enties whose Age__c is greater than
            custom setting value Purge_AREntry__c.
  
Developer: Deepika Rawat
Date: 04/22/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/22/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class TAG_DeleteAREntriesBatch implements Database.Batchable<SObject>{
    public Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
    public String ageVal = TAGsettings.Purge_AREntry__c;
    global String query = 'Select Id, Age__c from Assignment_Request_Entry__c where Status__c=\'Processed\' and Age__c >='+ageVal;
    global Database.QueryLocator start(Database.BatchableContext BC){
        return Database.getQueryLocator(query);
    }
    global void execute(Database.BatchableContext BC, List<Assignment_Request_Entry__c> scope){
        system.debug('scope*****************'+scope);
        delete scope;       
    }
    global void finish(Database.BatchableContext BC){
        TAG_DeleteARErrorsBatch sc = new TAG_DeleteARErrorsBatch();
        ID batchprocessid = Database.executeBatch(sc,200);
    }
}