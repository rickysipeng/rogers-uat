/*Class Name :salesMeasurementForecast_Test.
 *Description : .
 *Created By : Rajiv Gangra.
 *Created Date :14/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(SeeAllData = true)
private class salesMeasurementForecast_Test{
    private static Account accTestObj;
    private static OpportunityLineItem  oliTestObj;
    private static OpportunityLineItem  oliTestObj1;
    private static Opportunity oppTestObj;
    private static Sales_Measurement__c smTest;
    private static Sales_Measurement__c smTest2;
    private static List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
    private static Product2 p2;
    private static Pricebook2 stdpb;
    private static PricebookEntry pbe;
    static testmethod void vanillaTest(){
        
              //insert AccountaccTestObj
        accTestObj = new Account(Name='testAcc');
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        insert accTestObj;
        p2 = new Product2(Name='Test Product', Family = 'Cable', isActive=true,CanUseQuantitySchedule=true,CanUseRevenueSchedule=true,QuantityScheduleType='Divide',RevenueScheduleType='Repeat', NumberOfRevenueInstallments=5,NumberOfQuantityInstallments=5,QuantityInstallmentPeriod='Daily',RevenueInstallmentPeriod='Daily');
        insert p2;
        stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        pbe = new PricebookEntry(Pricebook2Id = stdpb.Id, Product2Id = p2.Id, UnitPrice = 1, isActive = true);
        insert pbe;
        List<Opportunity> oppList = new List<Opportunity>();
        //insert Opportunity oppTestObj
        oppTestObj = new Opportunity(Name='testAcc');
        oppTestObj.CloseDate=system.today();
        oppTestObj.StageName='Prospecting';
        oppTestObj.AccountID=accTestObj.id;
        oppTestObj.Pricebook2ID= stdpb.id;
        oppTestObj.ownerid =listUser[0].id;
        //oppTestObj.Business_Unit__c = 'Wireless';
        //insert oppTestObj;
        
        Opportunity oppTestObj1 = new Opportunity(Name='testAcc');
        oppTestObj1.CloseDate=system.today();
        oppTestObj1.StageName='Identified';
        oppTestObj1.AccountID=accTestObj.id;
        oppTestObj1.Pricebook2ID= stdpb.id;
        oppTestObj1.ownerid =listUser[0].id;
        //insert oppTestObj1;
        
        Opportunity oppTestObj2 = new Opportunity(Name='testAcc');
        oppTestObj2.CloseDate=system.today();
        oppTestObj2.StageName='Identified';
        oppTestObj2.AccountID=accTestObj.id;
        oppTestObj2.Pricebook2ID= stdpb.id;
        oppTestObj2.ownerid =listUser[0].id;
        //oppTestObj2.Business_Unit__c = 'Wireless';
        //insert oppTestObj2;
        
         List<Opportunity> listOpp =  new List<Opportunity>{oppTestObj,oppTestObj1,oppTestObj2};        
        insert listOpp;

        //insert Opportunity line item oliTestObj1
        oliTestObj1= new OpportunityLineItem();
        oliTestObj1.ARPU__c=10;
        oliTestObj1.Start_Date__c=system.today();
        oliTestObj1.Quantity=10;
        oliTestObj1.Installment_Period__c='Monthly';
        oliTestObj1.of_Installments__c=100;
        oliTestObj1.Monthly_Value__c=100;
        oliTestObj1.Minimum_Contract_Value__c=100;
        oliTestObj1.PricebookEntryId=pbe.id;
        oliTestObj1.OpportunityId=listOpp[0].id;
        oliTestObj1.TotalPrice=0;
        oliTestObj1.Product_Family__c='Cable - TV';
        //insert oliTestObj1;
        
         //insert Opportunity line item oliTestObj
        oliTestObj= new OpportunityLineItem();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        oliTestObj.PricebookEntryId=pbe.id;
        oliTestObj.OpportunityId=listOpp[1].id;
        oliTestObj.TotalPrice=0;
        oliTestObj.Product_Family__c='Wireless - All Data';
        //insert oliTestObj;
        
         //insert Opportunity line item oliTestObj
        OpportunityLineItem oliTestObj3= new OpportunityLineItem();
        oliTestObj3.ARPU__c=10;
        oliTestObj3.Start_Date__c=system.today();
        oliTestObj3.Quantity=10;
        oliTestObj3.Installment_Period__c='Monthly';
        oliTestObj3.of_Installments__c=10;
        oliTestObj3.Monthly_Value__c=100;
        oliTestObj3.Minimum_Contract_Value__c=100;
        oliTestObj3.PricebookEntryId=pbe.id;
        oliTestObj3.OpportunityId=listOpp[2].id;
        oliTestObj3.TotalPrice=0;
        oliTestObj3.Product_Family__c='Wireless - All Data';
        //insert oliTestObj3;
        
        List<OpportunityLineItem> listOLi =  new List<OpportunityLineItem>{oliTestObj,oliTestObj1,oliTestObj3};        
        insert listOLi;

        //insert salesmeasurement for oliTestObj
        smTest2= new Sales_Measurement__c ();
        smTest2.Opportunity__c = listOpp[0].id;
        smTest2.Opportunity_Line_Item_ID__c = listOLi[1].id;
        smTest2.Deployment_Date__c=system.today();
        smTest2.OwnerID=listOpp[0].OwnerID;
        smTest2.Comments__c='Test';
        smTest2.product__c=p2.ID;
        smTest2.Quota_Family__c=listOLi[1].Product_Family__c;
        smTest2.Quantity__c=listOLi[1].Quantity;
        insert smTest2;

        Sales_Forecast__c sf = new Sales_Forecast__c();
        sf.Quota_Family__c=listOLi[1].Product_Family__c;
        sf.Deployment_Month__c='201504';
        sf.Start_of_Deployment_Month__c =system.today();
        insert sf;

         //insert salesmeasurement for oliTestObj
        smTest= new Sales_Measurement__c ();
        smTest.Opportunity__c = oppTestObj1.id;
        smTest.Opportunity_Line_Item_ID__c = listOLi[0].id;
        smTest.Deployment_Date__c=system.today();
        smTest.OwnerID=oppTestObj1.OwnerID;
        smTest.Comments__c='Test';
        smTest.product__c=p2.ID;
        smTest.Quota_Family__c='Wireless - All Data';
        smTest.Quantity__c=listOLi[0].Quantity;
        insert smTest;

        
        Test.StartTest();
        listOpp[2].StageName='Closed Won';
        listOpp[2].Win_Reason__c='test';
        update listOpp[2];

        Sales_Measurement__c smTest23= new Sales_Measurement__c ();
        smTest23.Opportunity__c = listOpp[2].id;
        smTest23.Opportunity_Line_Item_ID__c = listOLi[2].id;
        smTest23.Deployment_Date__c=system.today();
        smTest23.OwnerID=listOpp[2].OwnerID;
        smTest23.Comments__c='Test';
        smTest23.product__c=p2.ID;
        smTest23.Quota_Family__c=listOLi[2].Product_Family__c;
        smTest23.Quantity__c=listOLi[2].Quantity;        insert smTest23;

        
        salesMeasurementForecast_schedular obj= new salesMeasurementForecast_schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('salesMeasurementForecast', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}