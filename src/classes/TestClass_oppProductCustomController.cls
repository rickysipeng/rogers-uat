/*Class Name  : TestClass_oppProductCustomController.
 *Description : This is test class for opportunityProductCustomController.
 *Created By  : Rajiv Gangra.
 *Created Date :11/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest(seeAllData=true)
private class TestClass_oppProductCustomController {
    private static Account accTestObj;
    private static Opportunity oppTestObj;
    private static OpportunityLineItem  oliTestObj;
    private static OpportunityLineItem  oliTestObjNeg;
    private static Case caseTestObj;
    private static user objUser;
    private static Product2 p2;
    private static Pricebook2 stdpb;
    private static PricebookEntry pbe;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static Product_Type_List__c custSetting = [SELECT id, Name, Form_Type__c, Order__c, Product_Category__c, Product_Family__c, Schedule_Type__c from Product_Type_List__c limit 1];
    private static void setUpData(){
        
       /* TAG_CS = [SELECT id, Unassigned_User__c, Team_roles__c, Team_Roles_Available__c from Team_Assignment_Governance_Settings__c Limit 1 ];
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        update TAG_CS;
        
       //custSetting= new Product_Type_List__c();
        custSetting.Name='ABS Connectivity';
        custSetting.Form_Type__c='Deployment Form';
        custSetting.Order__c=1;
        custSetting.Product_Category__c='ABS';
        custSetting.Product_Family__c='ABS Connectivity';
        custSetting.Schedule_Type__c='Quality & Revenue';
        update custSetting;*/
       
       Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        objUser = [SELECT Id,IsActive,ProfileId FROM User WHERE ProfileId =: profile.id AND IsActive = True limit 1];
        accTestObj = new Account(Name='testAcc');
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        accTestObj.Restricted__c = 'Yes';
        insert accTestObj;
        
        p2 = new Product2(Name='Test Product', Family = 'Cable', isActive=true,CanUseQuantitySchedule=true,CanUseRevenueSchedule=true,QuantityScheduleType='Divide',RevenueScheduleType='Repeat', NumberOfRevenueInstallments=5,NumberOfQuantityInstallments=5,QuantityInstallmentPeriod='Daily',RevenueInstallmentPeriod='Daily');
        insert p2;
        stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        pbe = new PricebookEntry(Pricebook2Id = stdpb.Id, Product2Id = p2.Id, UnitPrice = 1, isActive = true);
        insert pbe;
        
        oppTestObj = new Opportunity();
        oppTestObj.AccountId=accTestObj.id;
        oppTestObj.Name='test';
        oppTestObj.StageName='New';
        oppTestObj.CloseDate=system.today();
        oppTestObj.Pricebook2Id = stdpb.Id;
        Insert oppTestObj;        
              
        oliTestObj= new OpportunityLineItem();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        //oliTestObj.PricebookEntryId=null;
        oliTestObj.Product_Family__c = 'ABS Connectivity';
        oliTestObj.Line_Grouping__c = 'TV';
        oliTestObj.PricebookEntryId=pbe.id;
        oliTestObjNeg= new OpportunityLineItem();
       
        caseTestObj= new case(accountID=accTestObj.id);
    }
    static testmethod void myUnitWithCaseTestDeployment(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            ApexPages.currentPage().getParameters().put('oID', oppTestObj.id);
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl); 
            oProdCtrl.contractTerm='10';
            oProdCtrl.lineGrouping='TV';
            oProdCtrl.productSelected=[Select Id from Product2 Limit 1].id;
            oProdCtrl.formType='Deployment Form';
            oProdCtrl.saveRecord();
            oProdCtrl.selectRecord();
            oProdCtrl.pickLineGroupChange();
            
            oProdCtrl.productType='ABS';
            oProdCtrl.formType='Monthly/One Time Form';
            oProdCtrl.saveRecord();
            oProdCtrl.formType='Reseller Form';
            oProdCtrl.saveRecord();
            
            oProdCtrl.productSelected='testPro';
            oProdCtrl.saveRecord();
            
            oProdCtrl.cancelChange();
            oProdCtrl.updateInstallment();
            oProdCtrl.pickProductTypeChange();
            
            oProdCtrl.productType='ABS';
            oProdCtrl.lineGrouping='ABS Connectivity';
            oProdCtrl.pickLineGroupChange();
                   
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithCaseTestReseller(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            ApexPages.currentPage().getParameters().put('oID', oppTestObj.id);
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl); 
            oProdCtrl.contractTerm='10';
            oProdCtrl.lineGrouping='TV';
            oProdCtrl.productSelected=[Select Id from Product2 Limit 1].id;
            oProdCtrl.formType='Reseller Form';
            oProdCtrl.saveRecord();
                  
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithCaseTestMonthly(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            ApexPages.currentPage().getParameters().put('oID', oppTestObj.id);
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl); 
            oProdCtrl.contractTerm='10';
            oProdCtrl.lineGrouping='TV';
            oProdCtrl.prodFamily='Wireless - Voice & Data';
            oProdCtrl.productSelected=[Select Id from Product2 Limit 1].id;
            oProdCtrl.formType='Monthly/One Time Form';
            oProdCtrl.saveRecord();
                  
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithCaseTestExistingOLI(){
        setUpData();
        System.runAs(objUser) {
            
            Test.starttest();
            oliTestObj.opportunityid=oppTestObj.id;
            oliTestObj.Product_Category__c='TV';
            oliTestObj.Product_Family__c='Cable';
            oliTestObj.Contract_Term__c=1;
            oliTestObj.TotalPrice=0;
            oliTestObj.PricebookEntryId=pbe.id;
            insert oliTestObj;
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl); 
            oProdCtrl.contractTerm='10';
            oProdCtrl.lineGrouping='TV';
            oProdCtrl.productSelected=[Select Id from Product2 Limit 1].id;
            oProdCtrl.formType='Monthly/One Time Form';
            oProdCtrl.saveRecord();
                  
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithCaseTestExistingOLIResAcc(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            accTestObj.Restricted__c='Yes';
            update accTestObj;
            oliTestObj.opportunityid=oppTestObj.id;
            oliTestObj.Product_Category__c='TV';
            oliTestObj.Product_Family__c='Cable';
            oliTestObj.Contract_Term__c=1;
            oliTestObj.TotalPrice=0;
            oliTestObj.ARPU__c=0;
            oliTestObj.Start_Date__c=system.today();
            oliTestObj.Quantity=10;
            oliTestObj.Installment_Period__c='Monthly';
            oliTestObj.of_Installments__c=0;
            oliTestObj.Monthly_Value__c=0;
            oliTestObj.Minimum_Contract_Value__c=0;
            oliTestObj.PricebookEntryId=pbe.id;
            insert oliTestObj;
            ApexPages.currentPage().getParameters().put('oID', oppTestObj.id);
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObj);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl); 
            oProdCtrl.contractTerm='10';
            oProdCtrl.lineGrouping='TV';
            oProdCtrl.productSelected=[Select Id from Product2 Limit 1].id;
            oProdCtrl.formType='Monthly/One Time Form';
            oProdCtrl.saveRecord();
                  
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithCaseTestNeg(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            ApexPages.currentPage().getParameters().put('oID', oppTestObj.id);
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObjNeg);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl); 
            oProdCtrl.formType='Deployment Form';
            oProdCtrl.saveRecord();
            oProdCtrl.formType='Monthly/One Time Form';
            oProdCtrl.saveRecord();
            oProdCtrl.formType='Reseller Form';
            oProdCtrl.saveRecord();
            oProdCtrl.selectRecord();
     
            Test.stoptest();
        }
    }
    static testmethod void testmethod1(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            ApexPages.currentPage().getParameters().put('oID', oppTestObj.id);
            ApexPages.StandardController sCrtl= new ApexPages.StandardController(oliTestObjNeg);
            opportunityProductCustomController oProdCtrl = new opportunityProductCustomController(sCrtl);
            oProdCtrl.prodFamily = 'Cable';
            oProdCtrl.lineGrouping = null; 
            oProdCtrl.closePopup();
            oProdCtrl.selectItem();
            oProdCtrl.showPopup();
            oProdCtrl.clearProductId();
            oProdCtrl.search();
            oProdCtrl.cancelAll();
            oProdCtrl.selectRecord();
     
            Test.stoptest();
        }
    }  
      
}