/*
===============================================================================
 Class Name   : InitialLoadAccountTeamOneParent_batch
===============================================================================
PURPOSE:    This batch class will run after initial Account load. This is to 
            create AccountTeamMembers on Child accounts as in Parent Account.
  

Developer: Deepika Rawat
Date: 02/24/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/10/2015           Deepika Rawat               Original Version
===============================================================================
*/
global class InitialLoadAccountTeamOneParent_batch implements Database.Batchable<SObject>{
   global Database.QueryLocator start(Database.BatchableContext BC){
        Id accId =ID.valueof(Label.Batch_AccountID);
        system.debug('*accId**'+accId);
        Set<Id> accIds = new Set<Id>();
        for(Account acc: [select a.id, a.ParentId from Account a where ParentId!=null and ParentId=:accId Limit 500]){                    
            accIds.add(acc.id);
        }
        system.debug('accIds****'+accIds);
        if(!Test.isRunningTest()){
            String query = 'Select id, ParentId,OwnerId,District__c,Parent.District__c,Account_Owner_to_MAL__c, Account_Team_to_MAL__c, Parent.OwnerId from Account where Id In:accIds';
            return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('Select id, ParentId,OwnerId,District__c,Parent.District__c,Account_Owner_to_MAL__c, Account_Team_to_MAL__c, Parent.OwnerId from Account where Id In:accIds limit 1');
        }     
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        Set<Id> childAccIds = new Set<Id>();
        List<Account> childAccToUpdate = new List<Account>();
        for(Account child :scope){
           childAccIds.add(child.Id);
        }
        system.debug('childAccIds****'+childAccIds);
        for(Account childAcc :scope){
            childAcc.Account_Team_to_MAL__c=true;
            if(childAcc.Parent.District__c!=null)
               childAcc.District__c = childAcc.Parent.District__c;
            if(childAcc.Parent.OwnerId!=null){
               childAcc.OwnerId= childAcc.Parent.OwnerId; 
               childAcc.Account_Owner_to_MAL__c=true;
            }
            childAccToUpdate.add(childAcc);
        }
        system.debug('childAccToUpdate****'+childAccToUpdate);
        
        /*Call syncAccountTeamMembers to create Team Members on Child accounts. 
        Note* This method will first delete AccountTeamMembers from child, if any and then create new team as in Parent Account*/
        TAG_SyncAccountTeamMembers obj = new TAG_SyncAccountTeamMembers();
        obj.addTeamMembersTraversingFromChild(childAccIds);
        
        if(childAccToUpdate.size()>0){
            update childAccToUpdate;
        }
        
    }
    global void finish(Database.BatchableContext BC){
    }
}