@isTest
private class shift_get_spr_owner_emailTest{
    static testMethod void testshift_get_spr_owner_email() {
/*
        // create user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest1@test.com');
        insert u;

        // create dummy account
        Account a = new Account(Name='shift test account');
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='shift test opportunity', AccountId=a.Id, CloseDate=Date.today(), Amount = 500, StageName = 'Dormant');
        insert o;
        
        // create test quote
        Quote__c q = new Quote__c(Opportunity_Name__c=o.id);
        insert q;
        
        // create SPR
        SPR__c s = new SPR__c(Account__c = a.Id, Opportunity__c = o.Id, Requested_By__c = u.Id, Global_Recipient_1__c = u.Id, Global_Recipient_2__c = u.Id, Global_Recipient_3__c = u.Id, 
                                Global_Recipient_4__c = u.Id, Global_Recipient_5__c = u.Id);
        insert s;
        
        // get SPR owner's email. current user is the same SPR owner.
        string theOwnerEmail = [select Email from User where Id =: UserInfo.getUserId()].Email;
        
        // create SPR note       
        SPR_Note__c sn = new SPR_Note__c(SPR__c = s.Id);
        insert sn;  
        
        // refresh SPR Note
        sn = [select Id, SPR_Owner_Email__c from SPR_Note__c where Id =: sn.Id];
 */       
        // validate expected result
        //system.assertEquals(sn.SPR_Owner_Email__c, theOwnerEmail);      
    }
    
    static testMethod void testshift_share_spr_with_requestor () {
 /*       // create dummy account
        Account a = new Account(Name='shift test account');
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='shift test opportunity', AccountId=a.Id, CloseDate=Date.today(), Amount = 500, StageName = 'Dormant');
        insert o;
        
        // create test quote
        Quote__c q = new Quote__c(Opportunity_Name__c=o.id);
        insert q;

        // create user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest2@test.com');
        insert u;

        // get number of share records for current user
        Integer theCount = [select count() from SPR__Share where UserOrGroupId =: u.Id];
        system.assertEquals(theCount, 0);
        
        // create SPR
        SPR__c s = new SPR__c(Account__c = a.Id, Opportunity__c = o.Id, Requested_By__c = u.Id, Request_Detail__c = '123');
        insert s;
               
        // validate expected result
        theCount = [select count() from SPR__Share where UserOrGroupId =: u.Id];
 */       //system.assertEquals(theCount, 0);      
    }    
}