/*Class Name  : TestClass_customButtonUtilities.
 *Description : This is test class for customButtonUtilities.
 *Created By  : Rajiv Gangra.
 *Created Date :29/08/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest
private class TestClass_customButtonUtilities {
    private static Account accTestObj;
    private static user objUser;
    private static Opportunity oppTestObj;
    private static Case caseTest;
    static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    // Create data for testing purpose
    private static void setUpData(){
      TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        
        Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        String randomUserName = String.valueOf(System.now().getTime()) + '@test.com';
        objUser = new User(alias = 'test', email='test@test.com',
        emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
        localesidkey='en_US', profileid = profile.Id,
        timezonesidkey='America/Denver', username=randomUserName, isActive=true);
        insert objUser;
       
       accTestObj = new Account(Name='testAcc');
       accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        accTestObj.Account_Status__c='Pending';
       insert accTestObj;
       
       oppTestObj = new Opportunity();
         oppTestObj.AccountId=accTestObj.id;
         oppTestObj.Name='test';
         oppTestObj.StageName='New';
         oppTestObj.CloseDate=system.today();
         oppTestObj.OwnerId = objUser.id;
         Insert oppTestObj;
         
         caseTest = new Case();
         caseTest.Opportunity__c = oppTestObj.id;
         insert caseTest;
       
    }
    static testmethod void myUnitTest(){
        setUpData();
        System.runAs(objUser) {
            customButtonUtilities.accountSendForApproval(accTestObj.id);
            List<case> createdTestCase=[select id,AccountId,recordtypeId from Case where AccountId=:accTestObj.id];
            id testRecordTypeId=[Select r.SystemModstamp, r.SobjectType, r.NamespacePrefix, r.Name, 
                            r.LastModifiedDate, r.LastModifiedById, r.IsActive, r.Id, r.DeveloperName, 
                            r.Description, r.CreatedDate, r.CreatedById, r.BusinessProcessId 
                            From RecordType r where r.SobjectType=:'Case' AND r.DeveloperName=:'New_Account' limit 1].id;
            
            
           
           customButtonUtilities.caseSaveandSubmit(caseTest.Id);
           customButtonUtilities.accountUpdate(accTestObj.id);
            
        }
    }
}