/*  Jan 23, 2015. Paul Saini. Removed Contact.Ext__c as per requirements.
    As per comments from MP: The data in this field is planned to be concatenated with the standard Phone field. 
    The code should no longer reference the Ext__c field. Pre-populate SOI Date from Serviceable location's region (timeframe define in custom setting)
*/
global class CreateCIFExtension {

  private final Boolean isFrench;

    public CIF__c customerCIF {get; set;}
    public Quote copQuote {get; set;}
    public Opportunity copOpportunity {get; set;}
    private List<Contact> contacts {get; set;}
    public Map<String, Contact> contactMap {get; set;}
    public String selectedContact {get; set;}
    private Set<String> contactKeys{private get; private set;} 
    public Map<String, CIF_Site__c> cifSiteMap {get; set;}
    public List<Quote_Site__c> qSites {get; set;}
    public Map<String, CIF_Site__c> siteInfoMap {get; set;} // This is what we will use when we refresh the Confirm Site Address
    public List<String> siteIds {get; set;}
    public String contactFields {get; set;}
    public List<String> quoteSiteIds {get; set;}  // used for the ids in the page.
    public List<String> copSiteIds {get; set;}  // used for the ids in the page.
    public Map<String, List<String>> contactLocationMap {get; set;} // Used to obtain the contact Location (contactMap key from the contactKey -> fName + lName + phone + mPhone) 
    public CIF__c existingCOP {get; set;}
    private Map<String, Id> contactKeyToIdMap = new Map<String, Id>();
    private Map<Id, Quote_Site__c> qSiteMap;
    private EncryptionKey__c ekey;
    public String testThis {get; set;}
    private List<Contact> initialContacts;
    private String copId; 
    
    public Set<String> unsavedContacts {get; set;}
    
    public CreateCIFExtension (ApexPages.StandardController stdController) { 
        String encryptedCOPId = ApexPages.currentPage().getParameters().get('copId');
        unsavedContacts = new Set<String>();
      isFrench = ApexPages.currentPage().getUrl().toLowerCase().contains('french');
        
        // Decrypt the COP Id
        encryptedCOPId = EncodingUtil.urlDecode(encryptedCOPId, 'UTF-8');
        encryptedCOPId = encryptedCOPId.replaceAll(' ', '+');
        ekey = [SELECT Name, key__c FROM EncryptionKey__c WHERE Name = :encryptedCOPId LIMIT 1];
        
        Blob key = EncodingUtil.base64Decode(ekey.key__c);      
        
        Blob decryptedData = Crypto.decryptWithManagedIV('AES128', key, EncodingUtil.base64Decode(encryptedCOPId));
        String b64Decrypted = EncodingUtil.base64Encode(decryptedData);
        Blob decoded = EncodingUtil.base64Decode(b64Decrypted);
        copId = decoded.toString(); 
        
    initCOP();
    }
    
    private void initCOP(){
      cifSiteMap = new Map<String, CIF_Site__c>();
        siteInfoMap = new Map<String, CIF_Site__c>();
        contactLocationMap = new Map<String, List<String>>();
        customerCIF = new CIF__c();
        //PS: removed Ext__c from query below
        existingCOP = [SELECT Id, Account_Name1__c,Name, Comments__c, Contact_Email__c, Contact_Mobile__c, Contact_Name__c, Contact_Phone__c, Opp_Owner__c, Rogers_Account_Optional__c, Customer_Started__c, Customer_Completed__c,
                      Contact_Title__c, Billing_Contact_Email__c, Billing_Contact_Mobile__c, Billing_Contact_Name__c, Billing_Contact_Phone__c,
                      Billing_Contact_Title__c, Credit_Approval__c, Credit_Approval_Date__c, Customer_Address__c, Opportunity_Name__c, Opportunity_Name__r.SyncedQuoteId,
                      Opportunity_Name__r.AccountId, Opportunity_Name1__c, Opportunity_Number__c, Opportunity__c, Provisioning_ID__c, Quote_Name__c, Quote_Number__c, 
                     Service_Date_Implementation__c, TSC_Assigned__c, TSC_Name__c,
                      Technical_Contact_Name__c, Technical_Contact_Name1__c, Technical_Contact_Email__c, Technical_Contact_Title__c, Technical_Contact_Mobile__c, Order_Tracker__c,
                      Technical_Contact_Phone__c, OSS__c, Oracle_CA__c, Billing_Contact_Name1__c, Contact_Name1__c, Contact_Email_Alternate__c, Contact_Email_Interconnection__c, Contact_Mobile_Alternate__c, Contact_Mobile_Interconnection__c,
                      Contact_Name_Alternate1_c__c, Contact_Name1_Interconnection__c, Contact_Alternate_Phone__c, Contact_Phone_Interconnection__c, Contact_Title_Alternate__c, Contact_Title_Interconnection__c,
                      Account_Owner__r.Name, AC_ID__c, AC_Name__c, AM_ID__c, Billing_Address__c, 
                      
                      Billing_Contact_Name__r.FirstName, Billing_Contact_Name__r.LastName, Billing_Contact_Name__r.Phone, Billing_Contact_Name__r.MobilePhone, Billing_Contact_Name__r.Email,
                      Billing_Contact_Name__r.Title, Billing_Contact_Name__r.MailingCity, Billing_Contact_Name__r.MailingCountry, Billing_Contact_Name__r.MailingState, Billing_Contact_Name__r.MailingStreet, Billing_Contact_Name__r.MailingPostalCode, 
                      
                      eBilling_Admin_Contact__r.FirstName, eBilling_Admin_Contact__r.LastName, eBilling_Admin_Contact__r.Phone, eBilling_Admin_Contact__r.MobilePhone, eBilling_Admin_Contact__r.Email,
                      eBilling_Admin_Contact__r.Title, eBilling_Admin_Contact__r.MailingCity, eBilling_Admin_Contact__r.MailingCountry, eBilling_Admin_Contact__r.MailingState, eBilling_Admin_Contact__r.MailingStreet, eBilling_Admin_Contact__r.MailingPostalCode,
                      
                      Technical_Contact_Name__r.FirstName, Technical_Contact_Name__r.LastName, Technical_Contact_Name__r.Phone, Technical_Contact_Name__r.MobilePhone, Technical_Contact_Name__r.Email,
                      Technical_Contact_Name__r.Title, Technical_Contact_Name__r.MailingCity, Technical_Contact_Name__r.MailingCountry, Technical_Contact_Name__r.MailingState, Technical_Contact_Name__r.MailingStreet, Technical_Contact_Name__r.MailingPostalCode,
                      
                      Contact_Name_Alternate__r.FirstName, Contact_Name_Alternate__r.LastName, Contact_Name_Alternate__r.Phone, Contact_Name_Alternate__r.MobilePhone, Contact_Name_Alternate__r.Email,
                      Contact_Name_Alternate__r.Title, Contact_Name_Alternate__r.MailingCity, Contact_Name_Alternate__r.MailingCountry, Contact_Name_Alternate__r.MailingState, Contact_Name_Alternate__r.MailingStreet, Contact_Name_Alternate__r.MailingPostalCode,
                      
                      Contact_Name_Interconnection__r.FirstName, Contact_Name_Interconnection__r.LastName, Contact_Name_Interconnection__r.Phone, Contact_Name_Interconnection__r.MobilePhone, Contact_Name_Interconnection__r.Email,
                      Contact_Name_Interconnection__r.Title, Contact_Name_Interconnection__r.MailingCity, Contact_Name_Interconnection__r.MailingCountry, Contact_Name_Interconnection__r.MailingState, Contact_Name_Interconnection__r.MailingStreet, Contact_Name_Interconnection__r.MailingPostalCode,
                      
                      Contact_Name__r.FirstName, Contact_Name__r.LastName, Contact_Name__r.Phone, Contact_Name__r.MobilePhone, Contact_Name__r.Email,
                      Contact_Name__r.Title, Contact_Name__r.MailingCity, Contact_Name__r.MailingCountry, Contact_Name__r.MailingState, Contact_Name__r.MailingStreet, Contact_Name__r.MailingPostalCode,
                      
                      (SELECT Id, Building_Entry_Address_FreeText__c, Building_Entry_Contact_FreeText__c, Building_Entry_Contact_Email_FreeText__c, Building_Entry_Contact_JobTitle_FreeText__c,
                      Building_Entry_Contact_Mobile_FreeText__c, Building_Entry_Contact_Phone_FreeText__c, CIF_Number__c, Name, Circuit_Type__c, Floor__c, Interface_Type__c,
                      Property_Mgmt_Address_FreeText__c, Property_Mgmt_Contact_FreeText__c, Property_Mgmt_Email_FreeText__c, Property_Mgmt_Job_Title_FreeText__c,
                      Property_Mgmt_Mobile_FreeText__c, Property_Mgmt_Phone_FreeText__c, Quote__c, Rack__c, Room__c, Router__c, Service_Type__c, Site__c, Site__r.Display_Name__c, Quote__r.Id, Notes__c, Order_Type__c, Schedule_A_List__c, Oracle_CA__c,
                      Target_Turn_Up__c, Hours_of_Operation__c, Confirmed_Display_Name__c, EPOM__c, INSIS__c, 
                      
                      Property_Mgmt_Contact__r.FirstName, Property_Mgmt_Contact__r.LastName, Property_Mgmt_Contact__r.Phone, Property_Mgmt_Contact__r.MobilePhone, Property_Mgmt_Contact__r.Email,
                      Property_Mgmt_Contact__r.Title, Property_Mgmt_Contact__r.MailingCity, Property_Mgmt_Contact__r.MailingCountry, Property_Mgmt_Contact__r.MailingState, Property_Mgmt_Contact__r.MailingStreet, Property_Mgmt_Contact__r.MailingPostalCode,
                      
                      Building_Entry_Contact__r.FirstName, Building_Entry_Contact__r.LastName, Building_Entry_Contact__r.Phone, Building_Entry_Contact__r.MobilePhone, Building_Entry_Contact__r.Email,
                      Building_Entry_Contact__r.Title, Building_Entry_Contact__r.MailingCity, Building_Entry_Contact__r.MailingCountry, Building_Entry_Contact__r.MailingState, Building_Entry_Contact__r.MailingStreet, Building_Entry_Contact__r.MailingPostalCode,
                      Incorrect_Site_Details__c, Confirmed_Suite_Floor__c, Confirmed_Street_Number__c, Confirmed_Street_Name__c, Confirmed_Street_Type__c,
                      Confirmed_Street_Direction__c, Confirmed_City__c, Confirmed_Province_State__c, Confirmed_Postal_Code__c, Confirmed_Country__c
                      
                      FROM CIF_Sites__r)
                      
                      FROM CIF__c where id = :copId];
                      
        existingCOP.Customer_Started__c = true;
        customerCIF = existingCOP.clone(false, true); // PS: removed Ext__c from query below
        initialContacts = [SELECT Id, Name, FirstName, LastName, Phone, MobilePhone, Email, MailingCity, MailingCountry, Title, MailingPostalCode, MailingState, MailingStreet FROM Contact WHERE AccountId = :existingCOP.Opportunity_Name__r.AccountId];
        
        Quote q = [SELECT Name, Opportunity_Id__c, Opportunity.Owner.Name, Opportunity.Account.Name, Opportunity.Account.BillingCity, Opportunity.Account.BillingCountry, Opportunity.Account.BillingState, Opportunity.Account.BillingStreet, Opportunity.Account.BillingPostalCode, QuoteNumber__c, Opportunity.Contact__r.Name FROM Quote WHERE id = :existingCOP.Opportunity_Name__r.SyncedQuoteId LIMIT 1]; 
        
        if (q!=null)
            copQuote = q;
          
        copOpportunity = existingCOP.Opportunity_Name__r;
      
        quoteSiteIds = new List<String>();
        copSiteIds = new List<String>();        
        Map <Id, Quote_Site__c> tempSiteMap = new Map <Id, Quote_Site__c>([SELECT Site__c, Site__r.Display_Name__c, Site__r.Suite_Floor__c, Site__r.Street_Number__c, Site__r.Street_Name__c, Site__r.Street_Type__c, Site__r.Street_Direction__c, 
            Site__r.City__c, Site__r.Province_Code__c, Site__r.Postal_Code__c, Site__r.Country__c FROM Quote_Site__c WHERE Quote__r.Id = :q.Id]);

        qSiteMap = new Map<Id, Quote_Site__c>();
        
        for (Id qsid : tempSiteMap.keySet()){
            qSiteMap.put(tempSiteMap.get(qsid).Site__c,tempSiteMap.get(qsid));
        }

        
        siteIds = new List<String>();
        
        // Create map containing the Site Display Name as the Key - we can use this as the Heading of the accordian view
        // The value is a COF Site.
        for (CIF_Site__c cSite : existingCOP.CIF_Sites__r){
             Quote_Site__c qSite = qSiteMap.get(cSite.Site__c);
            // If there is already a confirmedSiteAddress then leave it be - if there is not then set it to the existing address
            if (!cSite.Incorrect_Site_Details__c){
            cSite.Confirmed_Suite_Floor__c = qSite.Site__r.Suite_Floor__c;
              cSite.Confirmed_Street_Number__c = qSite.Site__r.Street_Number__c;
              cSite.Confirmed_Street_Name__c = qSite.Site__r.Street_Name__c;
              cSite.Confirmed_Street_Type__c = qSite.Site__r.Street_Type__c;
              cSite.Confirmed_Street_Direction__c = qSite.Site__r.Street_Direction__c;
              cSite.Confirmed_City__c = qSite.Site__r.City__c;
              cSite.Confirmed_Province_State__c = qSite.Site__r.Province_Code__c;
              cSite.Confirmed_Postal_Code__c = qSite.Site__r.Postal_Code__c;
              cSite.Confirmed_Country__c = qSite.Site__r.Country__c;
              //cSite.Site__r = new Site__c();
              cSite.Site__r.Display_Name__c = qSite.Site__r.Display_Name__c;
            }
            
            cifSiteMap.put(qSite.Site__r.Display_Name__c, cSite.clone());
            siteInfoMap.put('confirm_' + qSite.Site__r.Id, cSite.clone());
            copSiteIds.add(cSite.id);
            quoteSiteIds.add(qSite.id);
            siteIds.add(qSite.Site__r.Id);
        }
    
        contacts = initialContacts;
        contactKeys = new Set<String>();        
        initializeContactMap(siteIds, initialContacts);
        contactFields = 'decisionMaker, billingContact, eBillingAdminContact, technicalContact, alternateTechnicalContact, interconnect, accordionpb_bec, accordionpb_pm';
    }
    
    
      public PageReference initCheck(){
        System.debug('\n\n\nInit Check: ');
        if(eKey == null){
            System.debug('time to move');
            PageReference pageRef = Page.COPCompleted;
            //PageReference pageRef = new PageReference('/apex/COPCompleted');
            return pageRef.setRedirect(true);
            
        }
        
        return null;
    }   
       
       
    public List<SelectOption> getExistingContacts(){

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for(Contact c : contacts){
            options.add(new SelectOption(getContactKey(c), c.FirstName+ ' ' +c.LastName));
        }       

        return options;
    }
    
    public PageReference refreshSiteAddress(){
        String siteLocation = Apexpages.currentPage().getParameters().get('siteLocation');
        System.debug('\n\n\n\n' + siteLocation );
        
        if (Utils.isEmpty(siteLocation)){
            return null;
        }
        
        // get the CIFSite from the siteInfoMap
        CIF_Site__c cifSite = siteInfoMap.get(siteLocation);
        if (cifSite!=null){
            if (!Utils.isEmpty(cifSite.Site__r.Display_Name__c)){
                cifSiteMap.put(cifSite.Site__r.Display_Name__c, cifSite.clone());
            }
        }
        
        return null;
        
    }
    
    public PageReference addNewContact(){
        String contactLocation = Apexpages.currentPage().getParameters().get('contactLocation');
        
        if (Utils.isEmpty(contactLocation)){
            return null;
        }
        
        // The VF Page has a binding to the contactMap so let's obtain the contact 
        Contact c = contactMap.get(contactLocation).clone(false, true);
        c.AccountId = existingCOP.Opportunity_Name__r.AccountId;
        try{
          INSERT c;
          
          // Since we are cloning without the id - we need to store in somewhere for later.
          contactKeyToIdMap.put(contactLocation, c.Id);  // We know that this contact location is using a particular contact
          System.debug('\n\nAdd: ' + contactKeyToIdMap);
          
          if (c!=null){
              //contacts.add(c.clone(false, true));
              contacts.add(c);
              contactKeys.add(getContactKey(c));
          }
          
          // When we add a new contact to the list we also need to add it to the map in case it is updated later.
          List<String> locations = new List<String>();
          locations.add(contactLocation);         
          contactLocationMap.put(getContactKey(c), locations);            // We will need this when we update the contact
                                                                          // so that we can obtain the key (contactLocation)
                                                                          // from the contactKey
          
          
          saveCOP();
        }catch(Exception ex){
          System.debug('\n\n\n\n\nException for add new contact: ' + ex.getMessage());
        }
        return null;
    }
    
    public PageReference updateContact(){
        String contactKey = Apexpages.currentPage().getParameters().get('contactkey');
        String contactLocation = Apexpages.currentPage().getParameters().get('contactLocation');           
        /*
        if (Utils.isEmpty(contactKey)){
            return null;
        }
        */
        System.debug('\n\nkey: ' + contactKey);
        System.debug('\n\nlocation: ' + contactLocation);
        System.debug(contactKeyToIdMap);
        
        Contact cTemp = contactMap.get(contactLocation);
    System.debug('\n\ncTemp: ' + cTemp);

    System.debug('Contact Id to Update: ' + contactKeyToIdMap.get(contactLocation));

    if (contactKeyToIdMap.get(contactLocation)==null)
      return null;
    
    System.debug('Contact Id to Update: ' + contactKeyToIdMap.get(contactLocation));
        //PS: removed Ext__c from query below
        Contact tempContact = [SELECT Id, Name, FirstName, LastName, Phone, MobilePhone, Email, MailingCity, MailingCountry, Title, MailingPostalCode, MailingState, MailingStreet FROM Contact WHERE Id = :contactKeyToIdMap.get(contactLocation) LIMIT 1];
        updateCurrentContact(cTemp, tempContact);
        
       try{
          UPDATE tempContact;
       }catch(Exception ex){
         
       }
         saveCOP(); 
         initCOP();
        // The VF Page has a binding to the contactMap so let's obtain the contact so we can update the rest of the locations
      /*
        Contact c = cTemp.clone(true, true);       
               
        
        if (tempContact!=null){
            Integer index = -1;
            for (Integer i=0; i<contacts.size(); ++i){
                if ((getContactKey(contacts[i])).equals(contactKey)){
                    index = i;
                }
            }
            System.debug('\n\n\nIndex to remove: '+ index);
            if (index != -1)
                contacts.remove(index);
            contacts.add(tempContact);
            
            contactKeys.remove(contactKey);
            
            contactKeys.add(getContactKey(tempContact));
        }
        
        List<String> locations = contactLocationMap.get(contactKey);
        
        if (locations!=null){
          for (String location : locations){
              contactMap.put(location, c);
          }
          
          contactLocationMap.put(getContactKey(c), locations);
        }
        contactLocationMap.remove(contactKey);
        */   
        
        return null;
    }
    
    public PageReference useExistingContact(){
        String contact = Apexpages.currentPage().getParameters().get('contactkey');
        String contactLocation = Apexpages.currentPage().getParameters().get('contactLocation');
                
        if (Utils.isEmpty(contact) || Utils.isEmpty(contactLocation)){
            return null;
        }
        
        for (Contact c : contacts){
            System.debug(getContactKey(c));
            if (contact.equals(getContactKey(c))){
                contactMap.put(contactLocation, c.clone(false, true));
                // Since we are cloning without the id - we need to store in somewhere for later.
                contactKeyToIdMap.put(contactLocation, c.Id);  // We know that this contact location is using a particular contact
                System.debug('\n\n\ncontactKeyToIdMap: ' + contactKeyToIdMap);
                List<String> locations = contactLocationMap.get(getContactKey(c));
                if (locations==null)
                    locations = new List<String>();
                locations.add(contactLocation);         
                contactLocationMap.put(getContactKey(c), locations);    // We will need this when we update the contact
                                                                                                    // from the contactKey
            }
        }
        
        
        saveCOP();
        return null;
    }
    
    private void initializeContactMap(List<String> siteIds, List<Contact> currentContacts){
        contactMap = new Map<String, Contact>();
        
        contactMap.put('decisionMaker', (existingCOP.Contact_Name__r!=null)?existingCOP.Contact_Name__r.clone(false, true): new Contact());
        contactMap.put('billing', (existingCOP.Billing_Contact_Name__r!=null)?existingCOP.Billing_Contact_Name__r.clone(false, true): new Contact());
        contactMap.put('eBillingAdmin', (existingCOP.eBilling_Admin_Contact__r!=null)?existingCOP.eBilling_Admin_Contact__r.clone(false, true): new Contact());
        contactMap.put('technical', (existingCOP.Technical_Contact_Name__r!=null)?existingCOP.Technical_Contact_Name__r.clone(false, true): new Contact());
        contactMap.put('alternateTechnical', (existingCOP.Contact_Name_Alternate__r!=null)?existingCOP.Contact_Name_Alternate__r.clone(false, true): new Contact());
        contactMap.put('interconnect', (existingCOP.Contact_Name_Interconnection__r!=null)?existingCOP.Contact_Name_Interconnection__r.clone(false, true): new Contact());    
        
        // Since we are cloning without the id - we need to store in somewhere for later.
        if (existingCOP.Contact_Name__c!=null){
            contactKeyToIdMap.put('decisionMaker', existingCOP.Contact_Name__c);
            contactKeys.add(getContactKey(existingCOP.Contact_Name__r));
            List<String> locations = contactLocationMap.get(getContactKey(existingCOP.Contact_Name__r));
            if (locations==null)
                locations = new List<String>();
            locations.add('decisionMaker');         
            contactLocationMap.put(getContactKey(existingCOP.Contact_Name__r), locations);      // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
        }
        if (existingCOP.Billing_Contact_Name__c!=null){
            contactKeyToIdMap.put('billing', existingCOP.Billing_Contact_Name__c);
            contactKeys.add(getContactKey(existingCOP.Billing_Contact_Name__r));
            List<String> locations = contactLocationMap.get(getContactKey(existingCOP.Billing_Contact_Name__r));
            if (locations==null)
                locations = new List<String>();
            locations.add('billing');           
            contactLocationMap.put(getContactKey(existingCOP.Billing_Contact_Name__r), locations);      // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
        } 
        /* Check this to ensure we are getting the Contact */
         if (existingCOP.eBilling_Admin_Contact__c!=null){
            contactKeyToIdMap.put('eBillingAdmin', existingCOP.eBilling_Admin_Contact__c);
            contactKeys.add(getContactKey(existingCOP.eBilling_Admin_Contact__r));
            List<String> locations = contactLocationMap.get(getContactKey(existingCOP.eBilling_Admin_Contact__r));
            if (locations==null)
                locations = new List<String>();
            locations.add('eBillingAdmin');           
            contactLocationMap.put(getContactKey(existingCOP.eBilling_Admin_Contact__r), locations);      // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
        } 
        
        if (existingCOP.Technical_Contact_Name__c!=null){
            contactKeyToIdMap.put('technical', existingCOP.Technical_Contact_Name__c);
            contactKeys.add(getContactKey(existingCOP.Technical_Contact_Name__r));
            List<String> locations = contactLocationMap.get(getContactKey(existingCOP.Technical_Contact_Name__r));
            if (locations==null)
                locations = new List<String>();
            locations.add('technical');         
            contactLocationMap.put(getContactKey(existingCOP.Technical_Contact_Name__r), locations);    // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
        }
        
        if (existingCOP.Contact_Name_Alternate__c!=null){ 
            contactKeyToIdMap.put('alternateTechnical', existingCOP.Contact_Name_Alternate__c);
            contactKeys.add(getContactKey(existingCOP.Contact_Name_Alternate__r));
            List<String> locations = contactLocationMap.get(getContactKey(existingCOP.Contact_Name_Alternate__r));
            if (locations==null)
                locations = new List<String>();
            locations.add('alternateTechnical');            
            contactLocationMap.put(getContactKey(existingCOP.Contact_Name_Alternate__r), locations);    // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
        } 
        
        
        
        if (existingCOP.Contact_Name_Interconnection__c!=null){ 
            contactKeyToIdMap.put('interconnect', existingCOP.Contact_Name_Interconnection__c);
            contactKeys.add(getContactKey(existingCOP.Contact_Name_Interconnection__r));
            List<String> locations = contactLocationMap.get(getContactKey(existingCOP.Contact_Name_Interconnection__r));
            if (locations==null)
                locations = new List<String>();
            locations.add('interconnect');          
            contactLocationMap.put(getContactKey(existingCOP.Contact_Name_Interconnection__r), locations);      // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
        }  



        // The quote Sites also have two contacts and need each placed in the map
        // We will append the qSite Id to the name of the contact type to make it unique
        // in the map
        for (String siteId : siteIds){
            
            CIF_Site__c cSite = getCOPSite(siteId);
            
            contactMap.put('bec_' + cSite.Id, (cSite.Building_Entry_Contact__r!=null)?cSite.Building_Entry_Contact__r.clone(false, true): new Contact());

            if (cSite.Building_Entry_Contact__c!=null){ 
                contactKeyToIdMap.put('bec_' + cSite.Id, cSite.Building_Entry_Contact__c);
                contactKeys.add(getContactKey(cSite.Building_Entry_Contact__r));                
                List<String> locations = contactLocationMap.get(getContactKey(cSite.Building_Entry_Contact__r));
                if (locations==null)
                    locations = new List<String>();
                locations.add('bec_' + cSite.Id);         
                contactLocationMap.put(getContactKey(cSite.Building_Entry_Contact__r), locations);      // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
            }
            
            contactMap.put('pm_' + cSite.Id, (cSite.Property_Mgmt_Contact__r!=null)?cSite.Property_Mgmt_Contact__r.clone(false, true): new Contact());
            
            if (cSite.Property_Mgmt_Contact__c!=null){ 
                contactKeyToIdMap.put('pm_' + cSite.Id, cSite.Property_Mgmt_Contact__c);
                contactKeys.add(getContactKey(cSite.Property_Mgmt_Contact__r));
                List<String> locations = contactLocationMap.get(getContactKey(cSite.Property_Mgmt_Contact__r));
                if (locations==null)
                    locations = new List<String>();
                locations.add('pm_' + cSite.Id);          
                contactLocationMap.put(getContactKey(cSite.Property_Mgmt_Contact__r), locations);       // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
            }
        }               
    }
    
    private CIF_Site__c getCOPSite(String siteId){
        CIF_Site__c cSite = null;
        
        for (CIF_Site__c cifSite : existingCOP.CIF_Sites__r){
            if (siteId.equals(cifSite.Site__c))
                return cifSite;
        }
        
        return cSite;
    }
    
    public PageReference saveCOP(){
        existingCOP.Rogers_Account_Optional__c = customerCIF.Rogers_Account_Optional__c;
        
        
        
        // Save the Contacts
        for (String key : contactKeyToIdMap.keySet()){
            Id contactId = contactKeyToIdMap.get(key);          
            System.debug('key: ' + key + ' contactId: ' + contactId);
            // The contact does not have the id - only one of the contacts does!  Use the contact that contains the id.
            if (contactId!=null){
                if (key.equals('decisionMaker')){
                    existingCOP.Contact_Name__c = contactId;    
                }else if (key.equals('technical')){
                    existingCOP.Technical_Contact_Name__c = contactId;  
                }else if (key.equals('interconnect')){
                    existingCOP.Contact_Name_Interconnection__c = contactId;    
                }else if (key.equals('alternateTechnical')){
                    existingCOP.Contact_Name_Alternate__c = contactId;  
                }else if (key.equals('billing')){
                    existingCOP.Billing_Contact_Name__c = contactId;    
                }else if (key.equals('eBillingAdmin')){
                    existingCOP.eBilling_Admin_Contact__c = contactId;    
                }
            }
        }
        
        List<CIF_Site__c> cifSites = new List<CIF_Site__c>();
        // I can go through all the sites and update using getCIFSite
        for (String siteId : siteIds){
            CIF_Site__c cSite = getCOPSite(siteId);
            
            Quote_Site__c qSite = qSiteMap.get(cSite.Site__c);
            CIF_Site__c tempCOPSite = cifSiteMap.get(qSite.Site__r.Display_Name__c);
            
            cSite.Floor__c = tempCOPSite.Floor__c;
            cSite.Room__c = tempCOPSite.Room__c;
            cSite.Rack__c = tempCOPSite.Rack__c;
            cSite.Hours_of_Operation__c = tempCOPSite.Hours_of_Operation__c;
            cSite.Target_Turn_Up__c = tempCOPSite.Target_Turn_Up__c;
            cSite.Incorrect_Site_Details__c = tempCOPSite.Incorrect_Site_Details__c;
            
            // Now the contacts
            for (String key : contactKeyToIdMap.keySet()){
                Id contactId = contactKeyToIdMap.get(key);          
            
                // The contact does not have the id - only one of the contacts does!  Use the contact that contains the id.
                if (contactId!=null){
                    if (key.equals('bec_' + cSite.Id)){
                        cSite.Building_Entry_Contact__c = contactId;    
                    }else if (key.equals('pm_' + cSite.Id)){
                        cSite.Property_Mgmt_Contact__c = contactId; 
                    }
                }
            }
            
            
            cSite.Confirmed_City__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_City__c:qSite.Site__r.City__c;
            cSite.Confirmed_Country__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Country__c:qSite.Site__r.Country__c;
            cSite.Confirmed_Postal_Code__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Postal_Code__c:qSite.Site__r.Postal_Code__c;
            cSite.Confirmed_Province_State__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Province_State__c:qSite.Site__r.Province_Code__c;
            cSite.Confirmed_Street_Direction__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Street_Direction__c:qSite.Site__r.Street_Direction__c;
            cSite.Confirmed_Street_Name__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Street_Name__c:qSite.Site__r.Street_Name__c;
            cSite.Confirmed_Street_Number__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Street_Number__c:qSite.Site__r.Street_Number__c;
            cSite.Confirmed_Street_Type__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Street_Type__c:qSite.Site__r.Street_Type__c;
            cSite.Confirmed_Suite_Floor__c = tempCOPSite.Incorrect_Site_Details__c?tempCOPSite.Confirmed_Suite_Floor__c:qSite.Site__r.Suite_Floor__c;
            
            cifSites.add(cSite);
        }
        
        
        UPDATE existingCOP;
        UPDATE cifSites;
        
        return null;
    }
    
    public PageReference submitCOP(){
        saveCOP();
        existingCOP.Customer_Completed__c = true; 
        existingCOP.COP_Submitted_Date__c = Datetime.now();
        UPDATE existingCOP;
        
        DELETE eKey;
        
        PageReference p;
        
        p = (isFrench)?Page.CustomerCOPConfirmationFrench:Page.CustomerCOPConfirmation;
        
        p.setRedirect(true);
        return p;   
    }
    
    private String getContactKey(Contact c){
        
        if (c == null)
            return null;
        //PS: removed Ext__c from string below
        String key = c.FirstName+c.LastName+c.Phone+c.MobilePhone+c.Email+c.Title+c.MailingCity+c.MailingCountry+c.MailingState+c.MailingStreet+c.MailingPostalCode;
        
        if (key.equals('nullnullnullnullnullnullnullnullnullnullnullnull')){
            return null;
        }
        
            
        return key;

    }
    
    @RemoteAction
    global static Set<String> getUnsavedContacts(String contactLocation, List<String> unsaved){
        System.debug(contactLocation);
        System.debug(unsaved);
        Set<String> unsavedContacts = new Set<String>();
            if (unsaved!=null && !unsaved.isEmpty()){
                for (String c : unsaved){
                    if (!c.equals(contactLocation))
                        unsavedContacts.add(c);
                }
            }
        
        return unsavedContacts;
    }
    
    
    private void updateCurrentContact(Contact src, Contact dest){
        dest.FirstName = src.FirstName;
        dest.LastName = src.LastName;
        dest.Phone = src.Phone;
        dest.MobilePhone = src.MobilePhone;
        dest.Email = src.Email;
        dest.Title = src.Title;
        dest.MailingCity = src.MailingCity;
        //PS: dest.Ext__c = src.Ext__c;
        dest.MailingCountry = src.MailingCountry;
        dest.MailingPostalCode = src.MailingPostalCode;
        dest.MailingStreet = src.MailingStreet;
        dest.MailingState = src.MailingState;
        
    }
    
}