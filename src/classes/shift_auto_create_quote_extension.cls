public class shift_auto_create_quote_extension {

    public Quote__c theQuote ;    
    private Id theOpportunityId = null;
    
    public shift_auto_create_quote_extension(ApexPages.StandardController controller) {
    	if (Config__c.getInstance('Config') == null) return;	
    	string theOpportunityFieldId = Config__c.getInstance('Config').Opportunity_Field_Id__c; 	
        theOpportunityId = ApexPages.currentPage().getParameters().get('CF' + theOpportunityFieldId + '_lkid');
        theQuote = new Quote__c(Name = 'nonanme', Opportunity_Name__c = theOpportunityId);
    }

    public PageReference theCreateQuote() {
        
        // if the no opportunity then show an error message
        if (theOpportunityId == null) {
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, 'Quotes can only be created from an Opportunity.');
            ApexPages.addMessage(myMsg);
            return null;
        }
		System.debug('Inserting Quote'+theQuote);
        insert theQuote;
        
        PageReference theQuotePageReference = new PageReference('/' + theQuote.Id);
        return theQuotePageReference;

    }
}