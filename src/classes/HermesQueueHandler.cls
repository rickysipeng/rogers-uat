public class HermesQueueHandler {

    public static Boolean isTestFromOtherOrg = false;

    public void handleBeforeInsert(List<Hermes_Queue__c> queues) {
        List<Hermes_Queue__c> receivedQueues = new List<Hermes_Queue__c>();        
        for (Hermes_Queue__c queue : queues) {
            if (queue.ConnectionReceivedId != null || isTestFromOtherOrg) {
                // from other org
                receivedQueues.add(queue);
            }
        }
        if (receivedQueues.size() > 0 ) { 
            new HermesQueueReceivedHandler().handleFromOtherOrg(receivedQueues);
        }                  
    }
}