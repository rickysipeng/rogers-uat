/*
 * http://code.google.com/p/salesforce-super-clone/source/browse/trunk/%20salesforce-super-clone/Google%20code%20Super%20Clone/src/classes/Clone.cls
 * r3 by jinghaili on Jul 10, 2011 
 * 
 */
//The top class needs to be global as this needs to be accessible for API call
global abstract class AbstractClone {
    
    //New Parent Record ID for API call
    global string newParentRecordID {get;private set;}
    
    public class MyException extends Exception {}
    
    public Map<String,String> chidlObjAPIName_FieldAPIName = new Map<String,String>{};
        
        //standard constructor
        global AbstractClone (){
            
        }
    
    //quick clone constructor
    global AbstractClone (String parentRecordID,List<String> childObjAPINames){
        newParentRecordID=startsClone(parentRecordID,childObjAPINames);
    }
    
    //excute clone and returns new parent record id
    public string startsClone(String parentRecordID,List<String> childObjAPINames){
        string fieldsNames='';
        
        List<SObject> objList = new List<SObject>();
        String queryString;

        //Loop through all child object names
        for(string objAPIName: childObjAPINames) {
            //Check if current child object has records linked with the parent record
            //Handle Note and Attachement in different way as they use "ParentID" in the query
            if(objAPIName == 'Note' || objAPIName == 'Attachment') {
                queryString = 'select Id from ' + String.escapeSingleQuotes(objAPIName) + ' where ParentId =\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
                system.debug('startsClone(): NoteAttach queryString \'' + queryString + '\'');
                
                objList = Database.query(queryString);
                system.debug('startsClone(): NoteAttach ' + objAPIName + ' list size = ' + objList.size());

                if(objList.size()>0){
                    fieldsNames = fieldsNames + objAPIName + ',';
                }
                


            }
            else {
                queryString = 'select Id from ' + String.escapeSingleQuotes(objAPIName) + ' where ' + String.escapeSingleQuotes(chidlObjAPIName_FieldAPIName.get(objAPIName)) + '=\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
                system.debug('startsClone(): Else queryString \'' + queryString + '\'');
                
                objList = Database.query(queryString);
                system.debug('startsClone(): Else ' + objAPIName + ' list size = ' + objList.size());
                
                if(objList.size()>0){
                    fieldsNames = fieldsNames + objAPIName + ',';
                }


            }
        }
        
        //Create Child Object API Name and creatable fields map
        Map<String, list<String>> objFields = New Map<String, list<String>>{};
            if(fieldsNames.length()>0) {
                //getCreatableFields(objAPIName) returns all creatable fields name for a given child obj API name
                for(String objAPIName:fieldsNames.split(','))
                    objFields.put(objAPIName,getCreatableFields(objAPIName));
                
            }
        
        //Clone parent record
        String newParentRecordID = cloneParentRecord(returnAPIObjectName(parentRecordID),getCreatableFields(returnAPIObjectName(parentRecordID)),parentRecordID);
        
        //Clone all selected child records
        for(String childObjAPIName:objFields.keySet())
            cloneChildRecords(childObjAPIName, objFields.get(childObjAPIName), returnAPIObjectName(parentRecordID), newParentRecordID,parentRecordID);
        
        return newParentRecordID;
    }
    
    //Get all child object (which have records linked with the parent record) API name
    private void getFieldAPINames (String[] objects,String parentObjAPIName,String parentRecordID){
        string fieldsNames = '';

        List<SObject> objList = new List<SObject>();
        String objAPIName;
        String queryString;

        for(string s: objects) {
            objAPIName=getAllChildObjNames(parentObjAPIName,parentRecordID).get(s);
            
            if(objAPIName == 'Note' || objAPIName == 'Attachment') {
                queryString = 'select Id from ' + String.escapeSingleQuotes(objAPIName) + ' where ParentId =\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
                system.debug('getFieldAPINames (): NoteAttach queryString \'' + queryString + '\'');
                
                objList = Database.query(queryString);
                system.debug('getFieldAPINames(): NoteAttach ' + objAPIName + ' list size = ' + objList.size());
                
                if(objList.size()>0){
                    fieldsNames = fieldsNames + objAPIName + ',';
                }
                


            }
            else {
                queryString = 'select Id from ' + String.escapeSingleQuotes(objAPIName) + ' where ' + String.escapeSingleQuotes(chidlObjAPIName_FieldAPIName.get(objAPIName)) + '=\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
                system.debug('getFieldAPINames (): Else queryString \'' + queryString + '\'');
                
                objList = Database.query(queryString);
                system.debug('getFieldAPINames(): Else ' + objAPIName + ' list size = ' + objList.size());

                if(objList.size()>0){
                    fieldsNames = fieldsNames + objAPIName + ',';
                }
                

                    
            }
        }
    }
    
    //Clone all child records
    public void cloneChildRecords(String objAPIName, list<String> createableFields, String parentObjAPIName, string newParentRecordID, string parentRecordID){

        String fields = '';
        for(string s:createableFields)
            fields = fields + s + ',';
        system.debug('cloneChildRecords(): fields = \'' + fields + '\'');
        
        fields=fields.substring(0, fields.lastIndexOf(','));
        
        String queryString = 'select ' + String.escapeSingleQuotes(fields) + ' from ' + String.escapeSingleQuotes(objAPIName) + ' where ' + String.escapeSingleQuotes(chidlObjAPIName_FieldAPIName.get(objAPIName)) + '=\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
        system.debug('cloneChildRecords(): queryString = \'' + queryString + '\'');

        list<SObject> result = Database.query(queryString);
        if(result!=null){
            system.debug('cloneChildRecords(): result list size = \'' + result.size() + '\'');
        }
        
        //list<SObject> copy = result.deepclone(false);
        /*
        *  From http://www.salesforce.com/us/developer/docs/apexcode/Content/apex_methods_system_list.htm?SearchType=Stem&Highlight=deepClone#apex_System_List_deepClone
        * 
        * public List<Object> deepClone(Boolean opt_preserve_id, Boolean opt_preserve_readonly_timestamps, Boolean opt_preserve_autonumber) 
        */
        list<SObject> copy = result.deepclone(false, true, true);
        if(copy!=null){
            system.debug('cloneChildRecords(): copy list size = \'' + copy.size() + '\'');
        }
        
        for(SObject obj :copy){
            
            system.debug('cloneChildRecords(): adding ' + objAPIName + ' object to be copied');
            obj.put(chidlObjAPIName_FieldAPIName.get(objAPIName), newParentRecordID);
            
        }            
        try{
            insert copy;
        }
        catch(System.Exception e){
            system.debug('cloneChildRecords(): adding ' + objAPIName + ' threw \n' + e.getStackTraceString() );
        }
    }
    
    //Clone parent record and return new parent record ID
    public String cloneParentRecord(String objAPIName, List<String> createableFields,string parentRecordID) {
        String fields = '';
        for(string s:createableFields) {
            //Remove all contact mirror fields from person account
            if(s.contains('__pc') == false)
                fields = fields + s + ',';
        }
        fields = fields.substring(0, fields.lastIndexOf(','));
        
        //Customization:  Extract process to query for and create a copy of parent record
        Sobject copy = processCopy(fields,objAPIName, parentRecordID);
       
        try{
            insert copy;
        }
        catch(System.Exception e){
            system.debug('cloneParentRecord(): adding ' + objAPIName + ' threw \n' + e.getStackTraceString() );
            return null;
        }

        system.debug('cloneParentRecord(): insert copy from processCopy with new id = ' + copy.id );
        return copy.id ;
    }
    
    /**
    * 
    * Return an Sobject using the query result for the given fields on a given parent record.
    */
    public virtual Sobject processCopy(String fields,String objAPIName, string parentRecordID){
   
        //http://code.google.com/p/salesforce-super-clone/source/browse/trunk/%20salesforce-super-clone/Google%20code%20Super%20Clone/src/classes/Clone.cls
        //r3 by jinghaili on Jul 10, 2011 
        String queryString = 'select ' + String.escapeSingleQuotes(fields) + ' from ' + String.escapeSingleQuotes(objAPIName) + ' where id=\'' + String.escapeSingleQuotes(parentRecordID) + '\'';
//        SObject result = Database.query(queryString);
        List<SObject> result = Database.query(queryString);
        
        if(result!=null && result.size()>0){
            return result.get(0).clone(false, true);
        }
        
        return null;
    }
    
    //Return the object API name for a given record
    public String returnAPIObjectName (string myRecordID){
        String objectName = '';
        
        String prefix = myRecordID.substring(0,3);
        
        Map<String, Schema.SObjectType> gd = Schema.getglobalDescribe();

        DescribeSObjectResult r;
        
        for(SObjectType s :gd.values()) {
            r = s.getDescribe();
            if(r.getKeyPrefix()!=null) {
                if(r.getLocalName()!=null && r.getKeyPrefix().equals(prefix)) {
                    objectName=r.getLocalName();
                    break;
                }
            }
        }
        return objectName;
    }
    
    
    //Get all creatable fields for a given object
    public list<String> getCreatableFields(String objAPIName){
        Map<string,string> childFieldsName = new Map<string,string>{};
            
            Map<String, Schema.SObjectType> gd = Schema.getglobalDescribe();
        SObjectType sot = gd.get(objAPIName);
        
        //Get all non-creatable fields name except
        //Get the field tokens map
        Map<String, SObjectField> fields = new Map<String, SObjectField>{};
            if(sot.getDescribe().fields.getMap().keyset().size()>0)
            fields = sot.getDescribe().fields.getMap();
        
        //And drop those tokens in a List
        List<SObjectField> fieldtokens = fields.values();
        
        List<string> objectFields = new List<String>();
        
        DescribeFieldResult dfr;

        for(SObjectField fieldtoken:fieldtokens) {
            dfr = fieldtoken.getDescribe();
            
            if(dfr.isCreateable())
                objectFields.add(dfr.getLocalName());
        }
        
        return objectFields;
    }
    
    //Get all child object API names for a given parent object API name
    //return Map<object label,object local name>,
    public Map<string,string> getAllChildObjNames(String parentObj,String parentRecordID) {
        Map<string,string> childFieldsName = new Map<string,string>{};
            Map<String, Schema.SObjectType> gd = Schema.getglobalDescribe();
        SObjectType sot = gd.get(ParentObj);
        
        //Get all child fields
        Schema.DescribeSObjectResult fieldResult2 = sot.getDescribe();
        List<Schema.ChildRelationship> children = fieldResult2.getChildRelationships();
        
        String descLocalName;
        Schema.DescribeSObjectResult describe;
        
        for(Schema.ChildRelationship child:children) {
            //Exclude following objects
            describe = child.getChildSObject().getDescribe();
            descLocalName = describe.getLocalName();

            if(descLocalName <> 'ProcessInstance'
               && descLocalName <> 'ProcessInstanceHistory'
               && descLocalName <> 'ContentVersion'
               && descLocalName <> 'ContentDocument'
               && descLocalName <> 'ActivityHistory'
               && descLocalName <> 'OpenActivity'
               && descLocalName <> 'Event'
               && descLocalName <> 'Task'
               && descLocalName <> 'User'
               && descLocalName <> 'FeedComment'
               && descLocalName <> 'FeedPost'
               && descLocalName <> 'EntitySubscription'
               && descLocalName <> 'NoteAndAttachment'
               && descLocalName <> 'UserRole'
               && descLocalName <> 'Partner'
               && descLocalName <> 'CampaignMemberStatus'
               
               //Do not take parent record which is the same object
               && descLocalName <> returnAPIObjectName(parentRecordID)
               
               //exclude obj created for sharing purpose
               && descLocalName <> returnAPIObjectName(parentRecordID) + 'share'
               && descLocalName.endsWith('__Share')==false

               //has to be creatable
               && describe.isCreateable() == true) {

                   string a=string.valueof(child.getChildSObject().getdescribe().getLocalName());
                   string b=string.valueof(child.getField());
                   chidlObjAPIName_FieldAPIName.put(string.valueof(child.getChildSObject().getdescribe().getLocalName()), string.valueof(child.getField()));
                   childFieldsName.put(child.getChildSObject().getDescribe().getLabel(), child.getChildSObject().getDescribe().getLocalName());
               }
        }
        
        return childFieldsName;
    }
}