public without sharing class ContractRenewalExtension {
	public String accId {get; set;}
	public String contractId {get; set;}
	public List<Wireline_Contract__c> existingContractsList { get; private set;}
	public List<ContractWrapper> contractWrapperList{ get; set; }
	public List<Selected_Contract__c> existingSelectedContractsList { get; private set;}
	public Wireline_Contract__c parentContract { get; private set; }
	public Integer numWrapped { get; private set; }
	public Boolean isNew { get; private set; }
	public String recordType {get; set;}
	public String owner {get; set;}
	public Integer numExisting {get; private set;}
	ApexPages.StandardController cStdController;
		
	public ContractRenewalExtension(ApexPages.StandardController stdController) {
		cStdController = stdController;
		contractId = stdController.getId();
		
		recordType = 'Enterprise Contract Renewal';
		owner = UserInfo.getName();
		
		if (Utils.isEmpty(contractId)){
			isNew = true;
		}else
			isNew = false;
		
		contractWrapperList = new List<ContractWrapper>();
		numWrapped = 0;
		
		accId = ApexPages.currentPage().getParameters().get('accId');
		String recType = ApexPages.currentPage().getParameters().get('RecordType');
		
		SObject con = stdController.getRecord();
		
		if (accId != null){
			con.put('Account_Name__c', accId);
		}
		
		if (recType != null){
			con.put('RecordTypeId', recType);
		}
		
		try{
			parentContract = [SELECT id, Account_Name__c, No_Existing_Contract__c, Status__c FROM Wireline_Contract__c WHERE id = :contractId LIMIT 1];
		}catch(Exception ex){
			
		}
		if (parentContract != null){
			accId = parentContract.Account_Name__c;
		}
		
		if (accId == null){
			numExisting = 0;
			return;
		}

		
		
		existingContractsList = [ select Id, Name
					, Contract_Number__c
					, Contract_Start_Date__c
					, Contract_End_Date__c
					, Status__c
					, Current_Contract_Flag__c 
				from Wireline_Contract__c
				where Account_Name__c = : accId
				AND Id <> :contractId
				];
		numExisting = existingContractsList != null ? existingContractsList.size() : 0;
				
		existingSelectedContractsList = [select Id, Name, Wireline_Contract__c 
				from Selected_Contract__c 
				where Parent_Wireline_Contract__c = : contractId];
				
		
		Set<Id> existingContracts = new Set<Id>();
		
		if (existingSelectedContractsList != null){
			for (Selected_Contract__c sc : existingSelectedContractsList){
				existingContracts.add(sc.Wireline_Contract__c);
			}			
		}
		
		for (Wireline_Contract__c c : existingContractsList){
			ContractWrapper cw = new ContractWrapper(c);
			if (existingContracts.contains(c.Id))
				cw.isSelected = true;
			
			contractWrapperList.add(cw);
		}
		
		numWrapped = contractWrapperList.size();

	}
	
		public PageReference getRelatedContracts(){
			accId =	ApexPages.currentPage().getParameters().get('acctId');
			if (parentContract != null)
				parentContract.Account_Name__c = accId;
				
			contractWrapperList = new List<ContractWrapper>();
			numWrapped = 0;
			
			existingContractsList = [ select Id, Name
					, Contract_Number__c
					, Contract_Start_Date__c
					, Contract_End_Date__c
					, Status__c
					, Current_Contract_Flag__c 
				from Wireline_Contract__c
				where Account_Name__c = : accId
				];
				
			numExisting = existingContractsList != null ? existingContractsList.size() : 0;
		
	
		Set<Id> existingContracts = new Set<Id>();
		
		for (Wireline_Contract__c c : existingContractsList){
			ContractWrapper cw = new ContractWrapper(c);
			if (existingContracts.contains(c.Id))
				cw.isSelected = true;
			
			contractWrapperList.add(cw);
		}
		numWrapped = contractWrapperList.size();
		
		return null;
	}
	
	public PageReference resetAccount(){
		String accountId = ApexPages.currentPage().getParameters().get('acctId');
		parentContract.Account_Name__c = accountId;
		
		return null;
	}
	
	public List<SelectOption> getStatus() {
 	 	List<SelectOption> options = new List<SelectOption>();
 	 	if (isNew || (parentContract != NULL && parentContract.Status__c == 'Draft'))
 	 		options.add(new SelectOption('Draft','Draft'));
 	 	else if (parentContract != NULL && (parentContract.Status__c == 'Activated' || parentContract.Status__c == 'Signed')){
 	 		options.add(new SelectOption('Signed','Signed'));
 	 		options.add(new SelectOption('Activated','Activated'));
 	 	}else{
 	 		options.add(new SelectOption('In Approval Process','In Approval Process'));
 	 	}
 	 	return options;
  	}
		
	
	public PageReference updateRelatedContracts(){
		PageReference pRef = null;
		
		
		pRef = cStdController.save();
			
		if (isNew){
			System.debug('The opportunity Id: ' + cStdController.getId());
			parentContract = [SELECT id, account_name__c, No_Existing_Contract__c FROM Wireline_Contract__c WHERE id = :cStdController.getId() LIMIT 1];
			if (contractId == null)
				contractId = parentContract.id;
			
		}
		
		List<Wireline_Contract__c> selectedContracts= new List<Wireline_Contract__c>();
        List<Wireline_Contract__c> contractsToUpdate = new List<Wireline_Contract__c>();    

        for(ContractWrapper cw : contractWrapperList) {
        	if(cw.isSelected) {
            	selectedContracts.add(cw.selContract);
            	cw.getContract().Current_Contract_Flag__c = false;
            }
            
            contractsToUpdate.add(cw.getContract());
            
       	}
 		
 		try{
 			UPDATE contractsToUpdate;
 		}catch(Exception ex){
 			System.debug('Unable to update contracts. ' + ex.getMessage());
 		}
 		
 		Set<Id> originalContractIds = new Set<Id>();
        Set<Id> currentContractIds = new Set<Id>();
        
        if (this.existingSelectedContractsList!=null){
             for (Selected_Contract__c sc : this.existingSelectedContractsList){
                originalContractIds.add(sc.Wireline_Contract__c);
             }
         }
                
                
        existingSelectedContractsList = new List<Selected_Contract__c>();        
         
        for(Wireline_Contract__c c : selectedContracts){
            Selected_Contract__c sc = new Selected_Contract__c();
            sc.Parent_Wireline_Contract__c = contractId;
            sc.Wireline_Contract__c = c.Id;
        
            if (!originalContractIds.contains(c.Id)){                    
                existingSelectedContractsList.add(sc);
            }
        
            currentContractIds.add(c.Id);
        }
                   
        INSERT existingSelectedContractsList;
        
        // Delete the old ones that are not needed any more
        List<Selected_Contract__c> expiredContracts = [SELECT Id FROM Selected_Contract__c WHERE Parent_Wireline_Contract__c = :contractId AND Wireline_Contract__c NOT IN :currentContractIds];
        DELETE expiredContracts;
        
        if (null != currentContractIds && currentContractIds.size() > 0){
        	try{
	        	contractsToUpdate = [SELECT id, Current_Contract_Flag__c FROM Wireline_Contract__c WHERE id IN :currentContractIds];
	        	
	        	if (null != contractsToUpdate && contractsToUpdate.size() > 0){
	        		for (Wireline_Contract__c con : contractsToUpdate){
	        			con.Current_Contract_Flag__c = false;
	        		}	
	        		
	        		UPDATE contractsToUpdate;
	        	}
        	}catch(Exception ex){
        		System.debug('Unable to remove the current contract flag. ' + ex.getMessage());
        	}
        	
        }
        
        return pRef;
   	
	}
	
	public PageReference updateContracts(){
		PageReference pRef = null;
		
		List<Wireline_Contract__c> selectedContracts= new List<Wireline_Contract__c>();
        List<Wireline_Contract__c> contractsToUpdate = new List<Wireline_Contract__c>();    

        for(ContractWrapper cw : contractWrapperList) {
        	if(cw.isSelected) {
            	selectedContracts.add(cw.selContract);
            	cw.getContract().Current_Contract_Flag__c = false;
            }
            
            contractsToUpdate.add(cw.getContract());
            
       	}
 		
 		try{
 			UPDATE contractsToUpdate;
 		}catch(Exception ex){
 			System.debug('Unable to update contracts. ' + ex.getMessage());
 		}
 		
 		Set<Id> originalContractIds = new Set<Id>();
        Set<Id> currentContractIds = new Set<Id>();
        
        if (this.existingSelectedContractsList!=null){
             for (Selected_Contract__c sc : this.existingSelectedContractsList){
                originalContractIds.add(sc.Wireline_Contract__c);
             }
         }
                
                
        existingSelectedContractsList = new List<Selected_Contract__c>();        
         
        for(Wireline_Contract__c c : selectedContracts){
            Selected_Contract__c sc = new Selected_Contract__c();
            sc.Parent_Wireline_Contract__c = contractId;
            sc.Wireline_Contract__c = c.Id;
        
            if (!originalContractIds.contains(c.Id)){                    
                existingSelectedContractsList.add(sc);
            }
        
            currentContractIds.add(c.Id);
        }
                   
        INSERT existingSelectedContractsList;
        
        // Delete the old ones that are not needed any more
        List<Selected_Contract__c> expiredContracts = [SELECT Id FROM Selected_Contract__c WHERE Parent_Wireline_Contract__c = :contractId AND Wireline_Contract__c NOT IN :currentContractIds];
        DELETE expiredContracts;
        
        if (null != currentContractIds && currentContractIds.size() > 0){
        	try{
	        	contractsToUpdate = [SELECT id, Current_Contract_Flag__c FROM Wireline_Contract__c WHERE id IN :currentContractIds];
	        	
	        	if (null != contractsToUpdate && contractsToUpdate.size() > 0){
	        		for (Wireline_Contract__c con : contractsToUpdate){
	        			con.Current_Contract_Flag__c = false;
	        		}	
	        		
	        		UPDATE contractsToUpdate;
	        	}
        	}catch(Exception ex){
        		System.debug('Unable to remove the current contract flag. ' + ex.getMessage());
        	}
        	
        }
        
        return pRef;
   	
	}
	
	public class ContractWrapper
	{
		Wireline_Contract__c selContract;
		Boolean isSelected;
		
		public void setContract(Wireline_Contract__c b) { this.selContract = b; }
    	public Wireline_Contract__c getContract() {return selContract; }
    	
    	public void setIsSelected(Boolean b) { this.isSelected = b; }
    	public Boolean getIsSelected() { return isSelected; }
		
		public ContractWrapper( Wireline_Contract__c contract)
		{
			this.selContract = contract;
			isSelected = false;
		}
	}
}