public class NewPricingRequestCont {
    public boolean StartFlow{get;set;}
    public String OppId{get;set;}
    public Flow.Interview.New_Pricing_Enablement_Request my_interview {get; set;}
    
    public NewPricingRequestCont (){
        StartFlow = false;
        OppId = ApexPages.currentPage().getParameters().get('id');
        Map<String, Object> myMap = new Map<String, Object>();
        myMap.put('OpportunityID', OppId);
        OppId = ApexPages.currentPage().getParameters().get('id');
        my_interview = new Flow.Interview.New_Pricing_Enablement_Request(myMap);
        
    }
    public pagereference returnToOpp(){
        PageReference pageRef = new PageReference('/'+OppId);
        return pageRef;
    }
    public pagereference returnToNewReq(){
        if (my_interview!=null && my_interview.Existing_Request_ID!=null){
            PageReference pageRef = new PageReference('/'+my_interview.Existing_Request_ID);
            return pageRef;
            }
        else
            return null;

    }
}