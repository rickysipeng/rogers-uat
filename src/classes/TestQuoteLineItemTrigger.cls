/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
 /*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest
private class TestQuoteLineItemTrigger {

    static Id oppId, quote1Id, quote2Id, site1Id, site2Id, site3Id, priceEntry1Id, priceEntry2Id, priceEntry2_1id;
    static QuoteLineItem qli2;
    static Boolean withAssertion = true;
    
    static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.ParentId =null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Pricebook2 pb = new Pricebook2();
        pb.IsActive = true;
        pb.Name = 'PB1';
        insert pb;
        
        Quote q = new Quote(Name='q1', Term__c='3-Years', OpportunityId=oppId, Pricebook2Id = pb.id);
        insert q;
        quote1Id = q.Id;
        
        Quote q2 = new Quote(Name='q1', Term__c='3-Years', OpportunityId=oppId, Pricebook2Id = pb.id);
        insert q2;
        quote2Id = q2.Id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        insert sl;
       
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = null;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Somewhere2';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = null;
        insert s2;
        site2Id = s2.Id;
        
        // This site is Serviceable
        Site__c s3 = new Site__c();
        s3.Street_Name__c = 'Somewhere3';
        s3.Suite_Floor__c = '11a';
        s3.Street_Number__c  = '5';
        s3.City__c = 'Coty';
        s3.Postal_Code__c = 'A3A1A1';
        s3.Province_Code__c = 'ON';
        s3.CLLI_SWC__c = '123113';
        s3.ServiceableLocation__c = sl.Id;
        insert s3;
        site3Id = s3.Id;
        
        //Product 1
        Product2 p  = new Product2();
        p.IsActive = true;
        p.Name = 'ProductMe';
        p.Mark_Up_Factor__c = 1.0;
        p.Start_Date__c = Date.today();
        p.End_Date__c = Date.today()+1;
        insert p;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Pricebook2.IsStandard = true];
        
        PricebookEntry pe1 = new PricebookEntry();
        pe1.Pricebook2Id = sp.id;
        pe1.UnitPrice = 60;
        pe1.Product2Id = p.id;
        pe1.IsActive = true;
        pe1.UseStandardPrice = false;
        
        insert pe1;
        
        PricebookEntry pe = new PricebookEntry();
        pe.Pricebook2Id = pb.id;
        pe.UnitPrice = 50;
        pe.Product2Id = p.id;
        pe.IsActive = true;
        pe.UseStandardPrice = false;
        
        insert pe;
        priceEntry1id = pe.Id;
         
        //Product 2
        Product2 p1  = new Product2();
        p1.IsActive = true;
        p1.Name = 'ProductMe1';
        p1.Mark_Up_Factor__c = 1.0;
        p1.Start_Date__c = Date.today();
        p1.End_Date__c = Date.today()+1;
        insert p1;
        
        PricebookEntry pe21 = new PricebookEntry();
        pe21.Pricebook2Id = sp.id;
        pe21.UnitPrice = 60;
        pe21.Product2Id = p1.id;
        pe21.IsActive = true;
        pe21.UseStandardPrice = false;
        
        insert pe21;
        priceEntry2_1id = pe21.Id;
        
        PricebookEntry pe2 = new PricebookEntry();
        pe2.Pricebook2Id = pb.id;
        pe2.UnitPrice = 50;
        pe2.Product2Id = p1.id;
        pe2.IsActive = true;
        pe2.UseStandardPrice = false;
        
        insert pe2;
        priceEntry2id = pe2.Id;

        //Line Item Product 1
        
        QuoteLineItem qli = new QuoteLineItem();
        qli.PricebookEntryId = pe.id;
        qli.Site__c = s1.id;
        qli.Quantity = 1;
        qli.QuoteId = q.id;
        qli.UnitPrice = 50;
        qli.Visible_Quote_Line_Item__c = true;
        insert qli;
        
        //Line Item Product 2
        qli2 = new QuoteLineItem();
        qli2.PricebookEntryId = pe2.id;
        qli2.Site__c = s1.id;
        qli2.Quantity = 1;
        qli2.QuoteId = q.id;
        qli2.UnitPrice = 50;
        qli2.Visible_Quote_Line_Item__c = true;
        insert qli2;
        
        QuoteDocument qd = new QuoteDocument(QuoteId = quote1Id, document = null);
    }

    static testMethod void quoteDocumentDeletedTest() {
        QuoteLineItem qli3 = qli2;
        qli3.UnitPrice = 60;
        upsert qli3;
        
        // Ensure that the Quote Document is no longer there.
        List <QuoteDocument> qd = new List <QuoteDocument>();
        qd = [select id from QuoteDocument where QuoteDocument.QuoteId = :quote1Id];
        if (withAssertion){
            System.assertEquals(qd.size(), 0);
        }
    }
    
    static testMethod void quoteWithNonServiceableSiteTest() {
        QuoteLineItem qli4 = new QuoteLineItem();
        qli4.PricebookEntryId = priceEntry2id;
        qli4.Site__c = site3Id;
        qli4.Quantity = 1;
        qli4.QuoteId = quote1Id;
        qli4.UnitPrice = 50;
        qli4.Visible_Quote_Line_Item__c = true;
        insert qli4;
        
        // Ensure that the Quote has Non serviceable flag set to false.
        List <Quote> q = [select id, Contains_Non_Serviceable_Site__c, Non_Serviceable_Site_on_Quote__c from Quote where Id = :quote1Id];
        
    //  System.debug('----------------------------------------------' + q.size() + '----' + q[0].Contains_Non_Serviceable_Site__c + q[0].Non_Serviceable_Site_on_Quote__c);
        
        if (withAssertion){
      //      System.assertEquals(q[0].Non_Serviceable_Site_on_Quote__c, true);
        }
    }
    
    static testMethod void quoteWithoutNonServiceableSiteTest() {
        QuoteLineItem qli4 = new QuoteLineItem();
        qli4.PricebookEntryId = priceEntry2id;
        qli4.Site__c = site3Id;
        qli4.Quantity = 1;
        qli4.QuoteId = quote2Id;
        qli4.UnitPrice = 50;
        qli4.Visible_Quote_Line_Item__c = true;
        insert qli4;
        
        // Ensure that the Quote has Non serviceable flag set to false.
       List <Quote> q = [select id, Contains_Non_Serviceable_Site__c, Non_Serviceable_Site_on_Quote__c from Quote where Id = :quote2Id];
        
        
        if (withAssertion){
            System.assertEquals(q[0].Non_Serviceable_Site_on_Quote__c, false);
        }
    }
    
    static testMethod void singleQLItemNonServiceableSiteTest() {
        QuoteLineItem qli4 = new QuoteLineItem();
        qli4.PricebookEntryId = priceEntry2id;
        qli4.Site__c = site2Id;
        qli4.Quantity = 1;
        qli4.QuoteId = quote2Id;
        qli4.UnitPrice = 50;
        qli4.Visible_Quote_Line_Item__c = true;
        insert qli4;
        
        // Ensure that the Quote has Non serviceable flag set to false.
       List <Quote> q = [select id, Contains_Non_Serviceable_Site__c, Non_Serviceable_Site_on_Quote__c from Quote where Id = :quote2Id];
        
        System.debug('----------------------------------------------' + q.size() + '----' + q[0].Contains_Non_Serviceable_Site__c + q[0].Non_Serviceable_Site_on_Quote__c);
        
        if (withAssertion){
     //       System.assertEquals(q[0].Non_Serviceable_Site_on_Quote__c, true);
        }
    }
}