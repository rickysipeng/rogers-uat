/*
Apex Class Name     : Test_TAG_ProcessApprovalRequestsBatch
Version             : 1.0 
Created Date        : 3 Mar 2015
Function            : This is the Test Class for TAG_ProcessApprovalRequestsBatch.
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Aakanksha Patel             01/30/2015              Original Version
*************************************************************************************/
@isTest
private class Test_TAG_ProcessApprovalRequestsBatch
{  
    private static  Account MSDOwnerAccount;  //MSD Owner
    private static  MSD_Code__c MSDOwnerMSDCode; 
    private static  Assignment_Request__c MSDOwnerAReq;  
    private static  Assignment_Request_Item__c MSDOwnerARI;  
    
    private static  Account SharedMSDOwnerAccount;  //Shared MSD Owner
    private static  Shared_MSD_Code__c SharedMSDOwnerMSDCode; 
    private static  Assignment_Request__c SharedMSDOwnerAReq; 
    private static  Shared_MSD_Accounts__c SharedMSDOwnerSMA; 
    private static  Assignment_Request_Item__c SharedMSDOwnerARI; 
    
    private static  Assignment_Request_Item__c MSDDistrictARI;   //MSD District
    private static  MSD_Code__c MSDDistrictMSDCode; 
    private static  District__c MSDDistrictDist;
    
    private static  Assignment_Request_Item__c SharedMSDDistrictARI;  //Shared MSD District
    private static  Shared_MSD_Accounts__c SharedMSDDistrictSMA; 
    private static  Shared_MSD_Code__c SharedMSDDistrictMSDCode;
    
    private static  Account MSDMemberAccount;  //MSD Member
    private static  MSD_Code__c MSDMemberMSDCode; 
    private static  Assignment_Request__c MSDMemberAReq;  
    private static  Assignment_Request_Item__c MSDMemberARI;
    private static  Assignment_Request_Item__c MSDMemberRARI;  
    private static  MSD_Member__c MSDMemberMSDCodeMM;
    
    private static  Account SharedMSDMemberAccount;  //Shared MSD Member
    private static  Shared_MSD_Code__c SharedMSDMemberMSDCode; 
    private static  Assignment_Request__c SharedMSDMemberAReq; 
    private static  Shared_MSD_Accounts__c SharedMSDMemberSMA; 
    private static  Assignment_Request_Item__c SharedMSDMemberARI;
    private static  Assignment_Request_Item__c SharedMSDMemberRARI;
    private static  Shared_MSD_Member__c SharedMSDMemberMSDCodeSM;
         
    private static  Account AccountOwnerPAccount;  //Account Owner
    private static  Account AccountOwnerCAccount;
    private static  Assignment_Request__c AccountOwnerAReq;  
    private static  Assignment_Request_Item__c AccountOwnerARI;  
    private static  AccountTeamMember AccountTeamMemberAO;
    private static  AccountTeamMember AccountTeamMemberAO1;
    
    private static  Account AccountDistrictPAccount;  //Account District
    private static  Account AccountDistrictCAccount;
    private static  Assignment_Request__c AccountDistrictAReq;  
    private static  Assignment_Request_Item__c AccountDistrictARI;
    private static  AccountTeamMember AccountTeamMemberAD;
    private static  AccountTeamMember AccountTeamMemberAD1; 
    
    private static  Account AccountTeamPAccount;  //Account Team
    private static  Account AccountTeamCAccount;
    private static  Assignment_Request__c AccountTeamAReq;  
    private static  Assignment_Request_Item__c AccountTeamARI;     
    private static  Assignment_Request_Item__c AccountTeamRARI;
    private static  AccountTeamMember AccountTeamMemberAT;
    private static  AccountTeamMember AccountTeamMemberAT1;     
         
    private static  User UserRec1;
    private static  User UserRec2;
    private Static Profile pEmp = [Select Id from Profile where name like '%Rogers%' limit 1];
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();         
   
    /*
    Description : This method is used to create Test Data. 
    Parameters  : None
    Return Type : void
    */
    private static void setUpData()
    {       
        
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Default_Account_District__c = 'Account District';
        TAG_CS.Default_Account_Owner__c = 'Account Owner';
        TAG_CS.Default_Account_Team__c = 'Account Team';
        TAG_CS.Default_MSD_District__c = 'MSD District'; 
        TAG_CS.Default_MSD_Member_Role__c = 'MSD Member';
        TAG_CS.Default_MSD_Owner_Role__c = 'MSD Owner';
        TAG_CS.Default_Shared_MSD_District__c = 'Shared MSD District';
        TAG_CS.Default_Shared_MSD_Member__c = 'Shared MSD Member';
        TAG_CS.Default_Shared_MSD_Owner__c = 'Shared MSD Owner';
        TAG_CS.ProAppReqSchBatchSize__c = 10; 
        TAG_CS.GiveAccToNewTeamBatchSize__c = 10;
        TAG_CS.ReCalAccTeamSchChild1BatchSize__c = 10;
        TAG_CS.UpdAccCasFlagBatchSize__c = 10;
        TAG_CS.ProAppReqSchChildBatchSize__c = 10;
        TAG_CS.ReCalAccTeamSchBatchSize__c = 10;
        insert TAG_CS;

        
        userRec1 = new User(LastName = 'Mark O’BrienRoger',Owner_Type__c = 'District', Alias = 'alRoger1', Email='test1@Rogertest.com', Username='test1@Rogertest.com', CommunityNickname = 'nickRoger2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec1;
        
        userRec2 = new User(LastName = 'Mark ',Owner_Type__c = 'Owner2', Alias = 'alRoger2', Email='test2@Rogertest.com', Username='test2@Rogertest.com', CommunityNickname = 'nickRoger3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec2;
                               
    }
    
   
     /*
     Description : This is is a method to test the Batch for MSD District and MSD Owner Type
     Parameters  : None
     Return Type : void
     */
   
    private static testMethod void Test_MSDDistrictOwner()
    {
        setUpData();
        
        // Set Up data for MSD Owner
        MSDOwnerAccount = new Account();
        MSDOwnerAccount.Name ='Account_MSDOwner';
        MSDOwnerAccount.Account_Status__c = 'Assigned';
        MSDOwnerAccount.ParentID= null;
        MSDOwnerAccount.Recalculate_Team__c = TRUE;
        MSDOwnerAccount.BillingPostalCode = 'A1A 1A1';
        insert MSDOwnerAccount; 
        
        MSDOwnerMSDCode = new MSD_Code__c();
        MSDOwnerMSDCode.OwnerId = userRec1.id ;
        MSDOwnerMSDCode.Account__c = MSDOwnerAccount.Id;
        MSDOwnerMSDCode.District__c = NULL;
        MSDOwnerMSDCode.MSD_to_MAL__c = TRUE;
        insert MSDOwnerMSDCode;
        
        MSDOwnerAReq = new Assignment_Request__c();
        MSDOwnerAReq.OwnerID = userRec2.id;
        insert MSDOwnerAReq;
        
        MSDOwnerARI = new Assignment_Request_Item__c();
        MSDOwnerARI.Account__c = MSDOwnerAccount.Id;
        MSDOwnerARI.MSD__c = MSDOwnerMSDCode.Id;
        MSDOwnerARI.Requested_Item_Type__c = 'MSD Owner';
        MSDOwnerARI.Status__c = 'Approved';
        MSDOwnerARI.New_Owner__c = userRec2.id;
        MSDOwnerARI.Business_Case__c = 'Business_MSDOwner';
        MSDOwnerARI.Reason_code__c = '4 - New Account Creation';
        MSDOwnerARI.Assignment_Request__c = MSDOwnerAReq.Id;
        insert MSDOwnerARI;
       
       //Set Up data For MSD District  
    /*    MSDDistrictDist = new District__c();
        insert MSDDistrictDist;
      */ 
        MSDDistrictMSDCode = new MSD_Code__c();
        MSDDistrictMSDCode.OwnerId = userRec1.id ;
        MSDDistrictMSDCode.Account__c = MSDOwnerAccount.Id;
    //    MSDDistrictMSDCode.District__c = MSDDistrictDist.id;
        MSDDistrictMSDCode.MSD_to_MAL__c = TRUE;
        insert MSDDistrictMSDCode;
       
        MSDDistrictARI= new Assignment_Request_Item__c();
        MSDDistrictARI.Account__c = MSDOwnerAccount.Id;
        MSDDistrictARI.MSD__c = MSDDistrictMSDCode.Id;
        MSDDistrictARI.Requested_Item_Type__c = 'MSD District';
        MSDDistrictARI.Status__c = 'Approved';
        MSDDistrictARI.New_Owner__c = userRec1.id;
        MSDDistrictARI.Business_Case__c = 'Business_MSDDistrict';
        MSDDistrictARI.Reason_code__c = '4 - New Account Creation';
        MSDDistrictARI.Assignment_Request__c = MSDOwnerAReq.Id;
        insert MSDDistrictARI;
        
            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
            
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Assignment_Request__c,Territory_City__c,Territory_Region__c,NPA__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ MSDDistrictARI.id+ '\' OR id = \''+MSDOwnerARI.id+ '\' ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,Assignment_Request__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Territory_City__c,Territory_Region__c,NPA__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
         
        Test.stopTest();     
            
    }
    
    /*
     Description : This is is a method to test the Batch for Shared MSD District and Shared MSD Owner Type
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_SharedMSDDistrictOwner()
    {
        setUpData();
        
         // Set Up data For Shared MSD Owner
        SharedMSDOwnerAccount = new Account();
        SharedMSDOwnerAccount.Name ='Account_SharedMSDOwner';
        SharedMSDOwnerAccount.Account_Status__c = 'Assigned';
        SharedMSDOwnerAccount.ParentID= null;
        SharedMSDOwnerAccount.Recalculate_Team__c = TRUE;
        SharedMSDOwnerAccount.BillingPostalCode = 'A1A 1A1';
        insert SharedMSDOwnerAccount; 
        
        SharedMSDOwnerMSDCode = new Shared_MSD_Code__c();
        SharedMSDOwnerMSDCode.OwnerId = userRec1.id ;
    //    SharedMSDOwnerMSDCode.Account__c = SharedMSDOwnerAccount.Id;
        SharedMSDOwnerMSDCode.District__c = NULL;
        SharedMSDOwnerMSDCode.Shared_MSD_to_MAL__c = TRUE;
        insert SharedMSDOwnerMSDCode;
        
        SharedMSDOwnerSMA = new Shared_MSD_Accounts__c();
        SharedMSDOwnerSMA.Account__c = SharedMSDOwnerAccount.Id;
        SharedMSDOwnerSMA.Shared_MSD_Code__c = SharedMSDOwnerMSDCode.id;
        insert SharedMSDOwnerSMA;
        
        SharedMSDOwnerAReq = new Assignment_Request__c();
        SharedMSDOwnerAReq.OwnerID = userRec2.id;
        insert SharedMSDOwnerAReq;
        
        SharedMSDOwnerARI = new Assignment_Request_Item__c();
        SharedMSDOwnerARI.Account__c = SharedMSDOwnerAccount.Id;
        SharedMSDOwnerARI.Shared_MSD__c = SharedMSDOwnerMSDCode.Id;
        SharedMSDOwnerARI.Requested_Item_Type__c = 'Shared MSD Owner';
        SharedMSDOwnerARI.Status__c = 'Approved';
        SharedMSDOwnerARI.New_Owner__c = userRec2.id;
        SharedMSDOwnerARI.Business_Case__c = 'Business_SharedMSDOwner';
        SharedMSDOwnerARI.Reason_code__c = '4 - New Account Creation';
        SharedMSDOwnerARI.Assignment_Request__c = SharedMSDOwnerAReq.Id;
        insert SharedMSDOwnerARI;
       
        
        //Set Up data For Shared MSD District  
        SharedMSDDistrictMSDCode = new Shared_MSD_Code__c();
        SharedMSDDistrictMSDCode.OwnerId = userRec1.id ;
    //    SharedMSDDistrictMSDCode.Account__c = SharedMSDDistrictAccount.Id;
        SharedMSDDistrictMSDCode.District__c = NULL;
        SharedMSDDistrictMSDCode.Shared_MSD_to_MAL__c = TRUE;
        insert SharedMSDDistrictMSDCode;
        
        SharedMSDDistrictSMA = new Shared_MSD_Accounts__c();
        SharedMSDDistrictSMA.Account__c = SharedMSDOwnerAccount.Id;
        SharedMSDDistrictSMA.Shared_MSD_Code__c = SharedMSDDistrictMSDCode.id;
        insert SharedMSDDistrictSMA;
        
        SharedMSDDistrictARI= new Assignment_Request_Item__c();
        SharedMSDDistrictARI.Account__c = SharedMSDOwnerAccount.Id;
        SharedMSDDistrictARI.Shared_MSD__c = SharedMSDDistrictMSDCode.Id;
        SharedMSDDistrictARI.Requested_Item_Type__c = 'Shared MSD District';
        SharedMSDDistrictARI.Status__c = 'Approved';
        SharedMSDDistrictARI.New_Owner__c = userRec2.id;
        SharedMSDDistrictARI.Business_Case__c = 'Business_SharedMSDDistrict';
        SharedMSDDistrictARI.Reason_code__c = '4 - New Account Creation';
        SharedMSDDistrictARI.Assignment_Request__c = SharedMSDOwnerAReq.Id;
        insert SharedMSDDistrictARI;
        
            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
        
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Assignment_Request__c,Member__r.Name,MSD__c,Territory_City__c,Territory_Region__c,NPA__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ SharedMSDDistrictARI.id+ '\' OR  id = \''+ SharedMSDOwnerARI.id+ '\' ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Assignment_Request__c,Role__c,Territory_City__c,Territory_Region__c,NPA__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
         
            Test.stopTest();     
            
    }
    
    /*
     Description : This is is a method to test the Batch for MSD Member Type(Add, Remove)
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_MSDMemberAddRemove()
    {
        
        setUpData();
        //Set Up data For MSD Member (Add)
        MSDMemberAccount = new Account();
        MSDMemberAccount.Name ='Account_MSDMember';
        MSDMemberAccount.Account_Status__c = 'Assigned';
        MSDMemberAccount.ParentID= null;
        MSDMemberAccount.Recalculate_Team__c = TRUE;
        MSDMemberAccount.BillingPostalCode = 'A1A 1A1';
        insert MSDMemberAccount ; 
        
        MSDMemberMSDCode = new MSD_Code__c();
        MSDMemberMSDCode.Account__c = MSDMemberAccount.Id;
        MSDMemberMSDCode.MSD_Members_to_MAL__c = TRUE;
        insert MSDMemberMSDCode ;
        
        MSDMemberAReq = new Assignment_Request__c();
        MSDMemberAReq.OwnerID = userRec2.id;
        insert MSDMemberAReq;
        
        MSDMemberARI = new Assignment_Request_Item__c();
        MSDMemberARI.Account__c = MSDMemberAccount.Id;
        MSDMemberARI.MSD__c = MSDMemberMSDCode.Id;
        MSDMemberARI.Requested_Item_Type__c = 'MSD Member';
        MSDMemberARI.Status__c = 'Approved';
        MSDMemberARI.Action__c = 'Add';
        MSDMemberARI.Business_Case__c = 'Business_MSDMember';
        MSDMemberARI.Reason_code__c = '4 - New Account Creation';
        MSDMemberARI.Assignment_Request__c = MSDMemberAReq.Id;
        MSDMemberARI.Territory_City__c = 'LONDON';
        MSDMemberARI.Member__c = userRec2.id;
        MSDMemberARI.Role__c = 'ECM';
        insert MSDMemberARI;
        
        //Add MSD members for MSDMemberMSDCode 
        MSDMemberMSDCodeMM = new MSD_Member__c();
          //  MSDMemberMSDCodeMM.Role__c = 'Owner';
            MSDMemberMSDCodeMM.MSD_Code__c = MSDMemberMSDCode.Id;
            MSDMemberMSDCodeMM.User__c = userRec2.id;
            MSDMemberMSDCodeMM.Territory_City__c = 'LONDON'; 
        insert MSDMemberMSDCodeMM; 
        
        //to Remove(MSD Member)
        MSDMemberRARI = new Assignment_Request_Item__c();
        MSDMemberRARI.Account__c = MSDMemberAccount.Id;
        MSDMemberRARI.MSD__c = MSDMemberMSDCode.Id;
        MSDMemberRARI.Requested_Item_Type__c = 'MSD Member';
        MSDMemberRARI.Status__c = 'Approved';
        MSDMemberRARI.Action__c = 'Remove';
        MSDMemberRARI.Business_Case__c = 'Business_MSDMember';
        MSDMemberRARI.Reason_code__c = '4 - New Account Creation';
        MSDMemberRARI.Assignment_Request__c = MSDMemberAReq.Id;
        MSDMemberRARI.Territory_City__c = 'LONDON';
        MSDMemberRARI.Member__c = userRec2.id;
        MSDMemberRARI.Role__c = 'ECM';
        insert MSDMemberRARI;
                
        
            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
            
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Assignment_Request__c,Member__r.Name,MSD__c,Territory_City__c,Territory_Region__c,NPA__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ MSDMemberARI.id+ '\' OR id = \''+ MSDMemberRARI.id+ '\'  ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Assignment_Request__c,Role__c,Member__c,Territory_City__c,Territory_Region__c,NPA__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
         
        Test.stopTest();     
            
    }
    
    /*
     Description : This is is a method to test the Batch for Shared MSD Member Type (Add,Remove)
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_SharedMSDMemberAddRemove()
    {
        setUpData();
        
        //Set Up data For Shared MSD Member
        SharedMSDMemberAccount= new Account();
        SharedMSDMemberAccount.Name ='Account_SharedMSDMember';
        SharedMSDMemberAccount.Account_Status__c = 'Assigned';
        SharedMSDMemberAccount.ParentID= null;
        SharedMSDMemberAccount.Recalculate_Team__c = TRUE;
        SharedMSDMemberAccount.BillingPostalCode = 'A1A 1A1';
        insert SharedMSDMemberAccount; 
        
        SharedMSDMemberMSDCode = new Shared_MSD_Code__c();
    //    SharedMSDMemberMSDCode .Account__c = SharedMSDOwnerAccount.Id;
        SharedMSDMemberMSDCode.Shared_MSD_Members_to_MAL__c = TRUE;
        insert SharedMSDMemberMSDCode ;
        
        SharedMSDMemberSMA = new Shared_MSD_Accounts__c();
        SharedMSDMemberSMA .Account__c = SharedMSDMemberAccount.Id;
        SharedMSDMemberSMA .Shared_MSD_Code__c = SharedMSDMemberMSDCode.id;
        insert SharedMSDMemberSMA ;
        
        SharedMSDMemberAReq = new Assignment_Request__c();
        SharedMSDMemberAReq.OwnerID = userRec2.id;
        insert SharedMSDMemberAReq;
        
        SharedMSDMemberARI= new Assignment_Request_Item__c();
        SharedMSDMemberARI.Account__c = SharedMSDMemberAccount.Id;
        SharedMSDMemberARI.Shared_MSD__c = SharedMSDMemberMSDCode.Id;
        SharedMSDMemberARI.Requested_Item_Type__c = 'Shared MSD Member';
        SharedMSDMemberARI.Status__c = 'Approved';
        SharedMSDMemberARI.Action__c = 'Add';
        SharedMSDMemberARI.Business_Case__c = 'Business_SharedMSDMember';
        SharedMSDMemberARI.Reason_code__c = '4 - New Account Creation';
        SharedMSDMemberARI.Assignment_Request__c = SharedMSDMemberAReq.Id;
        SharedMSDMemberARI.Territory_Region__c = 'AB';
        SharedMSDMemberARI.Member__c = userRec2.id;
        SharedMSDMemberARI.Role__c = 'ECM';
        insert SharedMSDMemberARI;
        
        //Add Shared MSD members for SharedMSDMemberMSDCode 
        SharedMSDMemberMSDCodeSM = new Shared_MSD_Member__c();
          //  MSDMemberMSDCodeMM.Role__c = 'Owner';
            SharedMSDMemberMSDCodeSM.Shared_MSD_Code__c = SharedMSDMemberMSDCode.Id;
            SharedMSDMemberMSDCodeSM.User__c = userRec2.id; 
            SharedMSDMemberMSDCodeSM.Territory_Region__c = 'AB';
        insert SharedMSDMemberMSDCodeSM; 
                            
        //to Remove(Shared Member)
        SharedMSDMemberRARI= new Assignment_Request_Item__c();
        SharedMSDMemberRARI.Account__c = SharedMSDMemberAccount.Id;
        SharedMSDMemberRARI.Shared_MSD__c = SharedMSDMemberMSDCode.Id;
        SharedMSDMemberRARI.Requested_Item_Type__c = 'Shared MSD Member';
        SharedMSDMemberRARI.Status__c = 'Approved';
        SharedMSDMemberRARI.Action__c = 'Remove';
        SharedMSDMemberRARI.Business_Case__c = 'Business_SharedMSDMember';
        SharedMSDMemberRARI.Reason_code__c = '4 - New Account Creation';
        SharedMSDMemberRARI.Assignment_Request__c = SharedMSDMemberAReq.Id;
        SharedMSDMemberRARI.Territory_Region__c = 'AB';
        SharedMSDMemberARI.Member__c = userRec2.id;
        SharedMSDMemberARI.Role__c = 'ECM';
        insert SharedMSDMemberRARI;
        

            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
            
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,NPA__c,Assignment_Request__c,Territory_City__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ SharedMSDMemberARI.id+ '\' OR id =\''+ SharedMSDMemberRARI.id+ '\' ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,Assignment_Request__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,NPA__c,Territory_City__c,Territory_Region__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
         
            Test.stopTest();     
            
    }
    
    /*
     Description : This is is a method to test the Batch for Account Owner
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_AccountOwner()
    {
        setUpData();
        
        //Set Up data For Account Owner
        AccountOwnerPAccount = new Account();
        AccountOwnerPAccount.Name ='Account_AccountOwnerParent';
        AccountOwnerPAccount.Account_Status__c = 'Assigned';
        AccountOwnerPAccount.ParentID= null;
        AccountOwnerPAccount.Account_Owner_to_MAL__c = TRUE;
        AccountOwnerPAccount.Recalculate_Team__c = TRUE;
        AccountOwnerPAccount.OwnerID = userRec1.id;
        AccountOwnerPAccount.BillingPostalCode = 'A1A 1A1';
        insert AccountOwnerPAccount ; 
        
        AccountOwnerCAccount = new Account();
        AccountOwnerCAccount.Name ='Account_AccountOwnerChild';
        AccountOwnerCAccount.Account_Owner_to_MAL__c = TRUE;
        AccountOwnerCAccount.ParentID = AccountOwnerPAccount.Id;
        AccountOwnerCAccount.OwnerID = userRec2.id;
        AccountOwnerCAccount.Recalculate_Team__c = TRUE;
        AccountOwnerCAccount.BillingPostalCode = 'A1A 1A1';
        insert AccountOwnerCAccount ; 
               
        AccountOwnerAReq = new Assignment_Request__c();
        AccountOwnerAReq.OwnerID = userRec2.id;
        insert AccountOwnerAReq;
        
        AccountOwnerARI = new Assignment_Request_Item__c();
        AccountOwnerARI.Account__c = AccountOwnerPAccount.Id;
        AccountOwnerARI.Requested_Item_Type__c = 'Account Owner';
        AccountOwnerARI.Status__c = 'Approved';
        AccountOwnerARI.New_Owner__c = userRec2.id;
        AccountOwnerARI.Business_Case__c = 'Business_AccountOwner';
        AccountOwnerARI.Reason_code__c = '4 - New Account Creation';
        AccountOwnerARI.Assignment_Request__c = AccountOwnerAReq .Id;
        AccountOwnerARI.New_Owner__c = userRec2.id;
        insert AccountOwnerARI;
        
        //Add team members for AccountOwnerPAccount 
        AccountTeamMemberAO = new AccountTeamMember();
            AccountTeamMemberAO.TeamMemberRole = 'Owner';
            AccountTeamMemberAO.AccountId = AccountOwnerPAccount.Id;
            AccountTeamMemberAO.UserId = userRec2.id; 
        insert AccountTeamMemberAO;   
        
        //Add team members for AccountOwnerCAccount 
        AccountTeamMemberAO1 = new AccountTeamMember();
            AccountTeamMemberAO1.TeamMemberRole = 'Owner';
            AccountTeamMemberAO1.AccountId = AccountOwnerCAccount.Id;
            AccountTeamMemberAO1.UserId = userRec2.id; 
        insert AccountTeamMemberAO1;   
        
        
            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
                       
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Assignment_Request__c,Territory_Region__c,Territory_City__c,NPA__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ AccountOwnerARI.id+ '\'  ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,Assignment_Request__c,New_Owner__r.isActive ,Member__r.isActive ,Territory_Region__c,Territory_City__c,NPA__c,Role__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
            
            Test.stopTest();     
            
    }   
   
   
        
    /*
     Description : This is is a method to test the Batch for Account District
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_AccountDistrict()
    {
       
       setUpData();
        
            //Set Up data For Account District
        AccountDistrictPAccount = new Account();
        AccountDistrictPAccount.Name ='Account_AccountDistrictParent';
        AccountDistrictPAccount.Account_Status__c = 'Assigned';
        AccountDistrictPAccount.ParentID= null;
        AccountDistrictPAccount.Account_Owner_to_MAL__c = TRUE;
        AccountDistrictPAccount.Recalculate_Team__c = TRUE;
         AccountDistrictPAccount.BillingPostalCode = 'A1A 1A1';
        insert AccountDistrictPAccount; 
        
        AccountDistrictCAccount = new Account();
        AccountDistrictCAccount.Name ='Account_AccountDistrictChild';
        AccountDistrictCAccount.Account_Owner_to_MAL__c = TRUE;
        AccountDistrictCAccount.ParentID = AccountDistrictPAccount.Id;
        AccountDistrictCAccount.OwnerID = userRec2.id;
        AccountDistrictCAccount.Recalculate_Team__c = TRUE;
        AccountDistrictCAccount.BillingPostalCode = 'A1A 1A1';
        insert AccountDistrictCAccount; 
               
        AccountDistrictAReq = new Assignment_Request__c();
        AccountDistrictAReq.OwnerID = userRec2.id;
        insert AccountDistrictAReq;
        
        AccountDistrictARI = new Assignment_Request_Item__c();
        AccountDistrictARI.Account__c = AccountDistrictPAccount.Id;
        AccountDistrictARI.Requested_Item_Type__c = 'Account District';
        AccountDistrictARI.Status__c = 'Approved';
        AccountDistrictARI.New_Owner__c = userRec2.id;
        AccountDistrictARI.Business_Case__c = 'Business_AccountDistrict';
        AccountDistrictARI.Reason_code__c = '4 - New Account Creation';
        AccountDistrictARI.Assignment_Request__c = AccountDistrictAReq .Id;
        AccountDistrictARI.New_Owner__c = userRec2.id;
        insert AccountDistrictARI;
        
        //Add team members for AccountOwnerPAccount 
        AccountTeamMemberAD = new AccountTeamMember();
            AccountTeamMemberAD.TeamMemberRole = 'Owner';
            AccountTeamMemberAD.AccountId = AccountDistrictPAccount.Id;
            AccountTeamMemberAD.UserId = userRec2.id; 
        insert AccountTeamMemberAD;   
        
        //Add team members for AccountOwnerCAccount 
        AccountTeamMemberAD1 = new AccountTeamMember();
            AccountTeamMemberAD1.TeamMemberRole = 'Owner';
            AccountTeamMemberAD1.AccountId = AccountDistrictCAccount.Id;
            AccountTeamMemberAD1.UserId = userRec2.id; 
        insert AccountTeamMemberAD1; 
        
        
            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
        
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Assignment_Request__c,Member__r.Name,MSD__c,Territory_City__c,Territory_Region__c,NPA__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ AccountDistrictARI.id+ '\'  ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Assignment_Request__c,Role__c,Territory_City__c,Territory_Region__c,NPA__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  Status__c= 'Approved']);
            sc.execute(scc,scope);
            sc.finish(scc);
        
            Test.stopTest();     
            
    }
    
    
    /*
     Description : This is is a method to test the Batch for Account Team Type (Add, Remove)
     Parameters  : None
     Return Type : void
     */
    private static testMethod void Test_AccountTeamAddRemove()
    {
        setUpData();
          //Set Up data For Account Team (Add)
        AccountTeamPAccount = new Account();
        AccountTeamPAccount.Name ='Account_AccountTeamParent';
        AccountTeamPAccount.Account_Status__c = 'Assigned';
        AccountTeamPAccount.ParentID= null;
        AccountTeamPAccount.OwnerID = userRec2.id;
        AccountTeamPAccount.Account_Owner_to_MAL__c = TRUE;
        AccountTeamPAccount.Recalculate_Team__c = TRUE;
        AccountTeamPAccount.BillingPostalCode = 'A1A 1A1';
        insert AccountTeamPAccount; 
        
        AccountTeamCAccount = new Account();
        AccountTeamCAccount.Name ='Account_AccountTeamChild';
        AccountTeamCAccount.Account_Team_to_MAL__c = TRUE;
        AccountTeamCAccount.ParentID = AccountTeamPAccount.Id;
        AccountTeamCAccount.OwnerID = userRec1.id;
        AccountTeamCAccount.Recalculate_Team__c = TRUE;
        AccountTeamCAccount.BillingPostalCode = 'A1A 1A1';
        insert AccountTeamCAccount; 
               
        AccountTeamAReq= new Assignment_Request__c();
        AccountTeamAReq.OwnerID = userRec2.id;
        insert AccountTeamAReq;
        
        AccountTeamARI = new Assignment_Request_Item__c();
        AccountTeamARI.Account__c = AccountTeamPAccount.Id;
        AccountTeamARI.Requested_Item_Type__c = 'Account Team';
        AccountTeamARI.Status__c = 'Approved';
        AccountTeamARI.Action__c = 'Add';
        AccountTeamARI.Business_Case__c = 'Business_AccTeamAdd';
        AccountTeamARI.Reason_code__c = '4 - New Account Creation';
        AccountTeamARI.Assignment_Request__c = AccountTeamAReq.Id;
        AccountTeamARI.Member__c = userRec2.id;
        AccountTeamARI.Role__c = 'ECM';
        insert AccountTeamARI;
        
        //Add team members for AccountTeamPAccount 
        AccountTeamMemberAT = new AccountTeamMember();
            AccountTeamMemberAT.TeamMemberRole = 'Owner';
            AccountTeamMemberAT.AccountId = AccountTeamPAccount.Id;
            AccountTeamMemberAT.UserId = userRec2.id; 
        insert AccountTeamMemberAT; 
        
        //Add team members for AccountTeamCAccount 
        AccountTeamMemberAT1 = new AccountTeamMember();
            AccountTeamMemberAT1.TeamMemberRole = 'Owner';
            AccountTeamMemberAT1.AccountId = AccountTeamCAccount.Id;
            AccountTeamMemberAT1.UserId = userRec2.id; 
        insert AccountTeamMemberAT1; 
        
        //to Remove(Account Team)
        AccountTeamRARI = new Assignment_Request_Item__c();
        AccountTeamRARI.Account__c = AccountTeamPAccount.Id;
        AccountTeamRARI.Requested_Item_Type__c = 'Account Team';
        AccountTeamRARI.Status__c = 'Approved';
        AccountTeamRARI.Action__c = 'Remove';
        AccountTeamRARI.Business_Case__c = 'Business_AccTeamAdd';
        AccountTeamRARI.Reason_code__c = '4 - New Account Creation';
        AccountTeamRARI.Assignment_Request__c = AccountTeamAReq.Id;
        AccountTeamRARI.Member__c = userRec2.id;
        AccountTeamARI.Role__c = 'ECM';
        insert AccountTeamRARI;
     
            Test.startTest();
            Database.BatchableContext scc;
            TAG_ProcessApprovalRequestsBatch sc = new TAG_ProcessApprovalRequestsBatch();
            
            sc.query ='SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Role__c,Member__c,Assignment_Request__c,Territory_City__c,Member__r.Name,MSD__c,Territory_Region__c,NPA__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c where id =\''+ AccountTeamARI.id+ '\' OR id =\''+ AccountTeamRARI.id+ '\'   ';
            sc.start(scc);
            List<Assignment_Request_Item__c> scope = ([SELECT Id,Account__c,New_Owner__r.isActive ,Member__r.isActive ,Assignment_Request__c,Role__c,Territory_City__c,Territory_Region__c,NPA__c,Member__c,Member__r.Name,MSD__c,Shared_MSD__c,New_Owner__c,Name,New_District__c,Status__c,Requested_Item_Type__c,Action__c FROM Assignment_Request_Item__c WHERE  id =: AccountTeamARI.id OR id =: AccountTeamRARI.id ]);
            sc.execute(scc,scope);
            sc.finish(scc);
         
            Test.stopTest();     
            
    }
    
   
 
}