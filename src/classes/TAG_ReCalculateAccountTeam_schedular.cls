/*
===============================================================================
 Class Name   : TAG_ReCalculateAccountTeam_schedular
===============================================================================
PURPOSE:    This schedular class runs the TAG_ReCalculateAccountTeam_batch class 
            with a batch of 200 records. 

Developer: Deepika Rawat
Date: 01/25/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/25/2015          Deepika                     Original Version
===============================================================================
*/
global class TAG_ReCalculateAccountTeam_schedular implements schedulable{

 //private static  Set<Id> setAccountUpdate = new set<ID>();
    global void execute(SchedulableContext ctx) {
      Set<Id> setAccountUpdate = new set<ID>();
        Team_Assignment_Governance_Settings__c TAGsettings = Team_Assignment_Governance_Settings__c.getInstance();
        Integer batchSize = Integer.ValueOf(TAGsettings.ReCalAccTeamSchBatchSize__c);
        TAG_ReCalculateAccountTeam_batch sc = new TAG_ReCalculateAccountTeam_batch(setAccountUpdate);
        
        ID batchprocessid = Database.executeBatch(sc,batchSize);
        system.debug('sc*************' +sc);
    }

}