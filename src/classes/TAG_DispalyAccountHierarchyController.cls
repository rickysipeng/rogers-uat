/*
===============================================================================
 Class Name   : TAG_DispalyAccountHierarchyController
===============================================================================
PURPOSE:    This class a controller class for TAG_DispalyAccountHierarchy page.
            It displays child Accounts for the Account whode ID is in the URL.
  
Developer: Deepika Rawat
Date: 01/28/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/28/2015           Deepika Rawat               Original Version
===============================================================================
*/
public class TAG_DispalyAccountHierarchyController {
    public List<Account> lstAllAccount{get;set;}
    public Account parentAccount{get;set;} 
    public String accID;
    public String noOfChilds{get;set;}
    //This is used to set limit on the number of child Accounts displayed on the Account Hierarchy pop up
    static final Integer childAccountlimit=10;
    
    /*******************************************************************************************************
     * @description: This method is controller method. It reads the 'accid' parameter from URL and fetches its 
                     it's related child accounts
     * @param: none
     * @return type: none.
     ********************************************************************************************************/
    public TAG_DispalyAccountHierarchyController(){
        lstAllAccount = new List<Account>();
        parentAccount = new account();
        String accid = ApexPages.currentPage().getParameters().get('accid');
        if(accid !=null && accid !=''){
            lstAllAccount = [select Id, Name , Internal_DUNS__c from Account where ParentID =:accid ORDER BY Internal_DUNS__c limit: childAccountlimit];
            parentAccount = [select Id, Name , Internal_DUNS__c from Account where ID =:accid ];
        }
        if(lstAllAccount!=null && lstAllAccount.size()>0){
            if(lstAllAccount.size() >10){
                noOfChilds = String.valueof(lstAllAccount.size()) + '+ ';
            }
            else
                 noOfChilds = String.valueof(lstAllAccount.size());
        }
        else{
            noOfChilds ='0';
        }
    }
}