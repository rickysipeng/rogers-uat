/*
===============================================================================
Class Name : Test_TAG_ProcessApprovalRequestsSchedular 
===============================================================================
PURPOSE: This is a test class for TAG_ProcessApprovalRequestsSchedular class

COMMENTS: 

Developer:Aakanksha Patel
Date: 12/3/2015


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
12/3/2015               Aakanksha               Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_TAG_ProcessApprovalReqSchedular{
 static testmethod void testMethod1(){
                
                
        Test.StartTest();
        list<AsyncApexJob> templist = new list<AsyncApexJob>([SELECT ApexClassId FROM AsyncApexJob WHERE JobType='BatchApex' AND ApexClassId IN (SELECT Id FROM ApexClass WHERE NamespacePrefix = null AND (Name = 'TAG_ProcessApprovalRequestsChildBatch' OR Name='TAG_ReCalculateAccountTeamOnChild_batch' OR Name = 'TAG_ReCalculateAccountTeam_batch')) AND (Status = 'Processing' OR Status = 'Preparing') ]);
        
        TAG_ProcessApprovalRequestsSchedular obj= new TAG_ProcessApprovalRequestsSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('TAG_ProcessApprovalRequestsTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}