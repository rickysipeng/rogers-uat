@isTest(SeeAllData = true)
public class shift_auto_create_quote_extension_test {
        private static List<Quote__c> theQuoteList;
        private static List<Opportunity> theOpportunityList;
        private static List<Account> theAccountList;
        static testmethod void TesttheCreateQuote(){
            dataGenerator(1);
            ApexPages.Standardcontroller sc = New ApexPages.StandardController(theQuoteList[0]);
            shift_auto_create_quote_extension ext = new shift_auto_create_quote_extension(sc);  
            ext.theCreateQuote();
        }
        static void dataGenerator (Integer numberOfRecords) {
        
        theAccountList = new List<Account>();       
        for (integer i=0; i<numberOfRecords; i++) {
            Account theAccount = new Account(Name = 'AccountTest'+i,BillingPostalCode = 'A1A 1A1');    
            theAccountList.add(theAccount);

        }
        insert theAccountList; 
        
        Schema.DescribeSObjectResult opportunitySchema = Schema.SObjectType.Opportunity; 
 
        Id oppRt = Schema.SObjectType.Opportunity.getRecordTypeInfosByName().get('Data Centre - RFP').getRecordTypeId(); 
        theOpportunityList = new List <Opportunity>();   
        for (integer i=0; i<numberOfRecords; i++) {            
            Opportunity theOpportunity1 = new Opportunity (Name = 'theOpportunity '+i, 
                                                           RecordTypeId = oppRt,
                                                           AccountId = theAccountList[i].id,
                                                           CloseDate = date.Today(),
                                                           StageName = 'Identify',
                                                           Amount = 50 );
            theOpportunityList.add(theOpportunity1);
        }         
        insert theOpportunityList;          
            theQuoteList = new List<Quote__c>();
            
            for (integer i=0; i<numberOfRecords; i++) {
                Quote__c theQuote = new Quote__c(Term__c=9000,Name='QuoteTest'+i, Opportunity_Name__c= theOpportunityList[i].id);    
                theQuoteList.add(theQuote);
    
            }
            insert theQuoteList; 

        }
}