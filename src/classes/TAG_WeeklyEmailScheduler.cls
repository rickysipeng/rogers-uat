global class TAG_WeeklyEmailScheduler implements Schedulable {
   
   global void execute(SchedulableContext ctx) {
        
     // MySchedulableClass c = new MySchedulableClass();
      TAG_WeeklyDigestEmailController c = new TAG_WeeklyDigestEmailController();      //Batch Class Name
      Database.executeBatch(c);
   }   
}