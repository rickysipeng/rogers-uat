/*
===============================================================================
Class Name : OpportunityUpdate_Test
===============================================================================
PURPOSE: This is a test class for OpportunityUpdate Trigger
COMMENTS: 

Developer: Deepika Rawat
Date: 9/11/2013

CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
9/11/2013               Deepika               Created
===============================================================================
*/
@isTest (SeeAllData = true)
private class OpportunityUpdate_Test{
    private static Account accTestObj;
    private static OpportunityLineItem  oliTestObj;
    private static OpportunityLineItem  oliTestObj1;
    private static Opportunity oppTestObj;
    private static Sales_Measurement__c smTest;
    private static Sales_Measurement__c smTest1;
    private static List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
     private static Product2 p2;
    private static Pricebook2 stdpb;
    private static PricebookEntry pbe;
    
    private static testmethod void testdata(){
        //insert AccountaccTestObj
        accTestObj = new Account(Name='testAcc');
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9';
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        insert accTestObj;
        p2 = new Product2(Name='Test Product', Family = 'Cable', isActive=true,CanUseQuantitySchedule=true,CanUseRevenueSchedule=true,QuantityScheduleType='Divide',RevenueScheduleType='Repeat', NumberOfRevenueInstallments=5,NumberOfQuantityInstallments=5,QuantityInstallmentPeriod='Daily',RevenueInstallmentPeriod='Daily');
        insert p2;
        stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
        pbe = new PricebookEntry(Pricebook2Id = stdpb.Id, Product2Id = p2.Id, UnitPrice = 1, isActive = true);
        insert pbe;
        //insert Opportunity oppTestObj
        oppTestObj = new Opportunity(Name='testAcc');
        oppTestObj.StageName='Cloased Won';
        oppTestObj.CloseDate=system.today();
        oppTestObj.AccountID=accTestObj.id;
        oppTestObj.Pricebook2ID= stdpb.id;
        oppTestObj.ownerid =listUser[0].id;
        //oppTestObj.Business_Unit__c = 'Wireless';
        insert oppTestObj;
        //insert Opportunity line item oliTestObj
        oliTestObj= new OpportunityLineItem();
        oliTestObj.ARPU__c=10;
        oliTestObj.Start_Date__c=system.today();
        oliTestObj.Quantity=10;
        oliTestObj.Installment_Period__c='Monthly';
        oliTestObj.of_Installments__c=10;
        oliTestObj.Monthly_Value__c=100;
        oliTestObj.Minimum_Contract_Value__c=100;
        oliTestObj.PricebookEntryId=pbe.id;
        oliTestObj.OpportunityId=oppTestObj.id;
        oliTestObj.TotalPrice=0;
        oliTestObj.Product_Family__c='Cable - TV';
        insert oliTestObj;
        //insert Opportunity line item oliTestObj1
        oliTestObj1= new OpportunityLineItem();
        oliTestObj1.ARPU__c=10;
        oliTestObj1.Start_Date__c=system.today();
        oliTestObj1.Quantity=10;
        oliTestObj1.Installment_Period__c='Monthly';
        oliTestObj1.of_Installments__c=100;
        oliTestObj1.Monthly_Value__c=100;
        oliTestObj1.Minimum_Contract_Value__c=100;
        oliTestObj1.PricebookEntryId=pbe.id;
        oliTestObj1.OpportunityId=oppTestObj.id;
        oliTestObj1.TotalPrice=0;
        oliTestObj1.Product_Family__c='Cable - TV';
        insert oliTestObj1;
        //insert salesmeasurement for oliTestObj
        smTest= new Sales_Measurement__c ();
        smTest.Opportunity__c = oppTestObj.id;
        smTest.Opportunity_Line_Item_ID__c = oliTestObj.id;
        insert smTest;
        //insert salesmeasurement for oliTestObj1
        smTest1= new Sales_Measurement__c ();
        smTest1.Opportunity__c = oppTestObj.id;
        smTest1.Opportunity_Line_Item_ID__c = oliTestObj1.id;
        insert smTest1;
    }
    static testmethod void testMethod1(){
       testdata();
       test.startTest();       
       delete oppTestObj;
       test.stopTest();       
     }
}