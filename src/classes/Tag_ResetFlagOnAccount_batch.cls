/*
===============================================================================
 Class Name   : Tag_ResetFlagOnAccount_batch
===============================================================================
PURPOSE:    This batch class will reset flag Account_Team_to_MAL__c = false 
            on child Accounts.
  
Developer: Deepika Rawat
Date: 06/17/2015

CHANGE HISTORY
===============================================================================
DATE                     NAME                         DESC
06/17/2015          Deepika Rawat               Original Version
===============================================================================
*/
global class Tag_ResetFlagOnAccount_batch implements Database.Batchable<SObject>{
    global Database.QueryLocator start(Database.BatchableContext BC){
        String query = 'Select id, ParentId,OwnerId,District__c,Parent.District__c,Account_Owner_to_MAL__c, Account_Team_to_MAL__c, Parent.OwnerId from Account where ParentId!=null and Account_Team_to_MAL__c=true';
        return Database.getQueryLocator(query);  
    }
    global void execute(Database.BatchableContext BC, List<Account> scope){
        List<Account> childAccToUpdate = new List<Account>();
        for(Account childAcc :scope){
            childAcc.Account_Team_to_MAL__c=false;
            childAccToUpdate.add(childAcc);
        }
        if(childAccToUpdate.size()>0){
            update childAccToUpdate;
        }
    }
    global void finish(Database.BatchableContext BC){
    
    }
}