/*
===============================================================================
Class Name : Test_TAG_ReCalculateAccTeam_schedular
===============================================================================
PURPOSE: This is a test class for TAG_ReCalculateAccountTeam_schedular class

COMMENTS: 

Developer: Deepika Rawat
Date: 18/9/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
18/9/2013               Deepika               Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class Test_TAG_ReCalculateAccTeam_schedular{

 private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
 private static  Set<Id> setAccountUpdate = new set<ID>();
 
 private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.ProAppReqSchBatchSize__c=200;       
        TAG_CS.ReCalAccTeamSchBatchSize__c = 200;   
        TAG_CS.GiveAccToNewTeamBatchSize__c = 10;
        TAG_CS.ReCalAccTeamSchChild1BatchSize__c = 10;
        TAG_CS.UpdAccCasFlagBatchSize__c = 10;     
        insert TAG_CS;
        
       
        
 }   
 static testmethod void testMethod1(){
                
        Test.StartTest();
        setUpData();
        TAG_ReCalculateAccountTeam_schedular obj= new TAG_ReCalculateAccountTeam_schedular();
        TAG_ReCalculateAccountTeam_batch sc = new TAG_ReCalculateAccountTeam_batch(setAccountUpdate);
        
        String sch = '0 0 23 * * ?';
        system.schedule('TAG_ReCalculateAccountTeamTesting', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}