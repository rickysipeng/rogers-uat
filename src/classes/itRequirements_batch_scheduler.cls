/*
===============================================================================
 Class Name   : itRequirements_batch_scheduler
===============================================================================

Jacob Klay, 11/11/2014

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/
global class itRequirements_batch_scheduler implements schedulable{

	global void execute(SchedulableContext ctx) {
		itRequirements_batch sc = new itRequirements_batch();
		ID batchprocessid = Database.executeBatch(sc,200);
	}
}