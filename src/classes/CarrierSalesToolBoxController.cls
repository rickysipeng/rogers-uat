public with sharing class CarrierSalesToolBoxController {

    public String baseURL { get; set; }
    
    public CarrierSalesToolBoxController() {
		baseURL = ApexPages.currentPage().getHeaders().get('Host');
		baseURL = baseURL.replace('c.cs', 'cs').replace('visual.force', 'salesforce');
		baseURL = baseURL.replace('c.n', 'n').replace('visual.force', 'salesforce');
	} 
}