/*Class Name :opportunityProductDisplayController.
 *Description : Controller Class for opportunityProductDisplayTemplate, Contains all the Methods used in tha Page.
 *Created By : Rajiv Gangra.
 *Created Date :12/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
  JACOB KLAY         11/4/2014         Seats revenue support
------------------------------------------------------------------------------------------------------------------------------
*
*/
public with sharing class opportunityProductDisplayController {
    Public string formType{get;set;}
    public String lineGrouping{get;set;}
    Public map<string,Product_Type_List__c> prodTypelst = Product_Type_List__c.getAll();
    Public OpportunityLineItem oliRecord{get;set;}
    Public List<OpportunityLineItemSchedule> relatedschedulelst{get;set;}
    Public boolean isDeploymentForm{get;set;}
    public String warningMessage{get;set;}
    
    public opportunityProductDisplayController(ApexPages.StandardController controller) {
        
        try{
    
            oliRecord=[select id,OpportunityID,Contract_Term__c,One_Time_Value__c,Product_Category__c,Start_Date__c,Product_Family__c,
                        Quantity,ARPU__c,Data_of_Total_Revenue__c,Revenue_Deployment_Renewal__c,Revenue_Monthly_or_One_Time__c,
                        Revenue_Resellers__c,Schedule_Type__c,Monthly_Value__c,Minimum_Contract_Value__c,UnitPrice, Installment_Period__c,
                        of_Installments__c,TotalPrice,PricebookEntry.Product2Id,Opportunity.Account.Account_Status__c,Line_Grouping__c,Opportunity.OwnerId,
                        Seats__c, Price_Per_Seat__c from OpportunityLineItem WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
        }
        catch (System.QueryException qe){
            System.debug (Label.Error_Opportunity_Product_Not_Found  + '\n\t'+ qe.getStackTraceString());
            ApexPages.Message msg = new ApexPages.Message(ApexPages.Severity.ERROR
                                                        ,Label.Error_Opportunity_Product_Not_Found);
            ApexPages.addMessage(msg);
            return;            
/*******            
            throw new GenericException(Label.Error_Opportunity_Product_Not_Found);
NB:  Decided against throwing Exception for the following reasons:
1.  Limits our ability to customize the displayed message.  Most of the message text cannot be set
2.  The line reference is not the line which caused the exception, but rather the location where we throw our exception.  This could be confusing for debugging efforts.  
********/
        }
        
        if(oliRecord.Product_Family__c !=null){
              for(string keyValue:prodTypelst.keySet()){
                     Product_Type_List__c pT=prodTypelst.get(keyValue);
                     if(pT.Product_Family__c==oliRecord.Product_Family__c){
                         formType=pT.Form_Type__c;
                         lineGrouping=pT.Name;
                     }
                 }
        }
        system.debug('*******oliRecord*********'+oliRecord);
        relatedschedulelst=[select id,ScheduleDate,Revenue,Quantity,OpportunityLineItemId,Description from OpportunityLineItemSchedule WHERE OpportunityLineItemId = :ApexPages.currentPage().getParameters().get('id') ORDER BY ScheduleDate ASC];
        if(lineGrouping!=null){
        if(prodTypelst.get(lineGrouping).Form_Type__c=='Deployment Form'){
            isDeploymentForm=true;
        }
        }
        
        if(formType =='' ||(formType !='Monthly/One Time Form' && formType !='Reseller Form' && formType !='Deployment Form') ){
        warningMessage= Label.Edit_Opportunity_Product;
       // ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, warningMessage);
       //ApexPages.addMessage(myMsg); 
        }
 
    }
    
    public boolean getHasErrorMessages(){
        return ApexPages.hasMessages(ApexPages.Severity.ERROR);
    }
    
    /*
    @ Method Name: DeleteAll
      Description: delete all the schedules related and redirect to Oppotunity product edit page.
      Return Type: NA.
      Input Param: NA.   
    */
    public PageReference DeleteAll() {
        PageReference oppProdPage = new PageReference('/apex/OpportunityProductTemplate?id='+oliRecord.id);
        oppProdPage.setRedirect(true);
        return oppProdPage;
     }
     public PageReference DeleteOppProduct() {
        OpportunityLineItem oppProduct=[select id,Quantity,OpportunityId from OpportunityLineItem WHERE Id =:oliRecord.id];
        string OppId=oppProduct.OpportunityId;
        delete oppProduct;
        PageReference oppProdPage = new PageReference('/'+OppId);
        oppProdPage.setRedirect(true);
        return oppProdPage;
     }
     public PageReference editOLI() {
        PageReference oppProdPage = new PageReference('/apex/opportunityProductScheduleEditTemplate?Id='+oliRecord.id);
        oppProdPage.setRedirect(true);
        return oppProdPage;
     }
     public PageReference resetAll() {
        Decimal seatsRevenue = 0;
        Decimal seatsMonthly = 0;
        // Delete the list of records created 
        list<OpportunityLineItemSchedule> lstOLIScheduleDelete=[select id,OpportunityLineItemId from OpportunityLineItemSchedule where OpportunityLineItemId=:oliRecord.id];
        list<Sales_Measurement__c> lstSalesMeasurementDelete=[select id,Opportunity_Line_Item_ID__c from Sales_Measurement__c where Opportunity_Line_Item_ID__c=:oliRecord.id ];
        delete lstOLIScheduleDelete;
        delete lstSalesMeasurementDelete;
        //recreate the schedule and measurement 
        system.debug('=============================== OLI quantity======================>'+oliRecord.Quantity+'\\\\'+oliRecord.Line_Grouping__c+'\\\\'+oliRecord.Product_Family__c);
        // Creating schedule for the Opportunity Product in case its a new Opporrunity Product
        list<OpportunityLineItemSchedule> lstOppSchedule= new list<OpportunityLineItemSchedule>();
        list<Sales_Measurement__c> lstSalesMeasurement= new list<Sales_Measurement__c>();
        Product_Type_List__c pT;
        for(Product_Type_List__c pTSearch:Product_Type_List__c.getall().values()){
            if(pTSearch.Product_Family__c==oliRecord.Product_Family__c){
                pT=pTSearch;
            }
        }
        string formType=pT.Form_Type__c;
        string prodFamily=pT.Product_Family__c;
        string quotaFamily=pT.Quota_Family__c;
        system.debug('=============================== formType======================>'+formType);
        
        if(formType=='Monthly/One Time Form') {
            if(oliRecord.Seats__c != null && oliRecord.Price_Per_Seat__c != null) {
                seatsRevenue = oliRecord.Seats__c * oliRecord.Price_Per_Seat__c;
                //seatsRemainder = math.mod(seatsRevenue, oliRecord.of_Installments__c);
                //seatsMonthly = (seatsRevenue) / oliRecord.of_Installments__c;
        seatsMonthly = seatsRevenue;
            }
        }
        for(integer i=0;i<Integer.Valueof(oliRecord.of_Installments__c);i++){
                                     OpportunityLineItemSchedule oliSchedule= new OpportunityLineItemSchedule();
                                     oliSchedule.OpportunityLineItemId=oliRecord.id;
            
            //Deployment Form-----------------------------------------------                         
                                     if(formType=='Deployment Form'){
                                         Integer remainder = math.mod(integer.Valueof(oliRecord.Quantity), Integer.Valueof(oliRecord.of_Installments__c));
                                         system.debug('=============================== Remainder======================>'+remainder);
                                         Integer quantity=integer.Valueof(oliRecord.Quantity)-remainder;
                                         system.debug('=============================== quantity======================>'+quantity);
                                         oliSchedule.Type='Both';
                                       
                                         if(oliRecord.Installment_Period__c==label.Monthly){
                                             oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addmonths(i);
                                         }else if(oliRecord.Installment_Period__c==label.Yearly){
                                             oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addyears(i);
                                         }else if(oliRecord.Installment_Period__c==label.Quarterly){
                                             oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addmonths(i*3);
                                         }
                                         if(i==0){
                                             oliSchedule.Quantity=(quantity/Integer.Valueof(oliRecord.of_Installments__c))+remainder;
                                         }else{
                                             oliSchedule.Quantity=quantity/Integer.Valueof(oliRecord.of_Installments__c);
                                         }
                                         if(oliRecord.ARPU__c !=null){
                                             oliSchedule.Revenue=(oliSchedule.Quantity*Integer.Valueof(oliRecord.ARPU__c)*Integer.Valueof(oliRecord.Contract_term__c));
                                         }else{
                                             oliSchedule.Revenue=0;
                                         }
            //Monthly/One Time Form------------------------------------------
                                     }else if(formType=='Monthly/One Time Form'){
                                         oliSchedule.Type='Revenue';
                                         oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addmonths(i);
                                         if(i==0){
                                             if(oliRecord.Monthly_Value__c !=null && oliRecord.One_Time_Value__c!=null){
                                                 oliSchedule.Revenue=oliRecord.Monthly_Value__c + oliRecord.One_Time_Value__c;
                                             }else if(oliRecord.Monthly_Value__c !=null){
                                                 oliSchedule.Revenue=oliRecord.Monthly_Value__c;
                                             }else if(oliRecord.One_Time_Value__c!=null){
                                                 oliSchedule.Revenue=oliRecord.One_Time_Value__c;
                                             }else{
                                                 oliSchedule.Revenue=0;
                                             }
                                             if(oliRecord.Seats__c != null && oliRecord.Price_Per_Seat__c != null) {
                                                oliSchedule.Revenue += seatsMonthly;
                                             }
                                             
                                         }else{
                                             if(oliRecord.Monthly_Value__c !=null){
                                                 oliSchedule.Revenue=oliRecord.Monthly_Value__c;
                                             }else{
                                                 oliSchedule.Revenue=0;
                                             }
                                             if(oliRecord.Seats__c != null && oliRecord.Price_Per_Seat__c != null) {
                                                oliSchedule.Revenue += seatsMonthly;
                                             }
                                         }
            //Reseller form--------------------------------------------------
                                     }else if(formType=='Reseller Form'){
                                         Integer remainder = math.mod(integer.Valueof(oliRecord.TotalPrice), Integer.Valueof(oliRecord.of_Installments__c));
                                         Integer totalPrice=integer.Valueof(oliRecord.TotalPrice)-remainder;
                                         oliSchedule.Type='Revenue';
                                         if(oliRecord.Installment_Period__c==label.Monthly){
                                             oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addmonths(i);
                                         }else if(oliRecord.Installment_Period__c==label.Yearly){
                                             oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addyears(i);
                                         }else if(oliRecord.Installment_Period__c==label.Quarterly){
                                             oliSchedule.ScheduleDate=oliRecord.Start_Date__c.addmonths(i*3);
                                         }
                                         oliSchedule.Revenue=oliRecord.Minimum_Contract_Value__c ;
                                        
                                      }
            
            // create measurement Object related to the schedule----------------------------------------------------------------------------
                                    Sales_Measurement__c salesMeasureSchedule= new Sales_Measurement__c();
                                    salesMeasureSchedule.Opportunity__c=oliRecord.OpportunityId;
                                    salesMeasureSchedule.Opportunity_Line_Item_ID__c=oliRecord.id;
                                    salesMeasureSchedule.Deployment_Date__c=oliSchedule.ScheduleDate;
                                    salesMeasureSchedule.OwnerID=oliRecord.Opportunity.ownerID;
                                    salesMeasureSchedule.Comments__c=oliSchedule.description;
                                    salesMeasureSchedule.product__c=oliRecord.PricebookEntry.Product2ID;
                                    salesMeasureSchedule.Quantity__c=oliSchedule.Quantity;
                                    if( prodFamily !='Wireless - Voice & Data'){
                                        salesMeasureSchedule.Revenue__c=oliSchedule.Revenue;
                                        salesMeasureSchedule.Quota_Family__c=quotaFamily;
                                    }else{
                                        Sales_Measurement__c salesMeasureSchedule1= new Sales_Measurement__c();
                                       // salesMeasureSchedule1.Quota_Family__c=prodFamily;
                                        salesMeasureSchedule1.Opportunity__c=oliRecord.OpportunityId;
                                        salesMeasureSchedule1.Opportunity_Line_Item_ID__c=oliRecord.id;
                                        salesMeasureSchedule1.Deployment_Date__c=oliSchedule.ScheduleDate;
                                        salesMeasureSchedule1.OwnerID=oliRecord.Opportunity.ownerID;
                                        salesMeasureSchedule1.Comments__c=oliSchedule.description;
                                        salesMeasureSchedule1.product__c=oliRecord.PricebookEntry.Product2ID;
                                        salesMeasureSchedule1.Quota_Family__c='Wireless - All Data';
                                        salesMeasureSchedule1.Quantity__c=oliSchedule.Quantity;
                                        salesMeasureSchedule.Quota_Family__c='Wireless - Voice';
                                        if(oliSchedule.Revenue !=null && oliRecord.Data_of_Total_Revenue__c!=null){
                                            salesMeasureSchedule1.Revenue__c=oliSchedule.Revenue *(oliRecord.Data_of_Total_Revenue__c/100);
                                        }
                                        // Adding the same Product 2 times for once for Voice and 2nd time for Data
                                        lstSalesMeasurement.add(salesMeasureSchedule1);
                                        if(oliSchedule.Revenue !=null && oliRecord.Data_of_Total_Revenue__c!=null){
                                            salesMeasureSchedule.Revenue__c=oliSchedule.Revenue *(1-(oliRecord.Data_of_Total_Revenue__c/100));
                                        }
                                        
                                    }
                                    system.debug('salesMeasureSchedule************'+salesMeasureSchedule);
                                    lstOppSchedule.add(oliSchedule);
                                    lstSalesMeasurement.add(salesMeasureSchedule);
                                 }
                             insert lstOppSchedule;
                             insert lstSalesMeasurement;
                         PageReference oppPage = new PageReference('/'+oliRecord.id);
                         oppPage.setRedirect(true);
                         return oppPage;

     }
}