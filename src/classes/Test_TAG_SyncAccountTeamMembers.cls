/*
===============================================================================
 Class Name   : Test_TAG_SyncAccountTeamMembers
===============================================================================
PURPOSE:    This class a controller class for TAG_SyncAccountTeamMembers.
              
Developer: Deepika Rawat
Date: 03/11/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
03/11/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=False) 
private class Test_TAG_SyncAccountTeamMembers{
     private static Account ParentAcc;
    private static Account ParentAcc2;
    private static Account ParentAcc3;
    private static Account ChildAcc1;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static User userRec2;
    private static User userRec3;
    private static District__c newDist;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();

     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Team_Account_Access__c='Read';
        TAG_CS.Team_Custom_Object_Access__c='Read';
        insert TAG_CS;

        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',  Assignment_Approver__c = userRec.id,Channel__c='Business Segment',Owner_Type__c='District');
        insert userRec2;

        userRec3 = new User(LastName = 'Mark O’Roger3', Alias = 'aRoger3', Email='test@Rogertest.com', Username='test@Rogertest3.com', CommunityNickname = 'nickRog3', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Assignment_Approver__c = userRec2.id,Channel__c='Business Segment');
        insert userRec3;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        ParentAcc.BillingPostalCode = 'A1A 1A1';
        insert ParentAcc;
        //child of ParentAcc
        ChildAcc1= new Account();
        ChildAcc1.Name = 'ChildAcc1';
        ChildAcc1.Account_Status__c = 'Assigned';
        ChildAcc1.ParentID= ParentAcc.Id;
        ChildAcc1.OwnerId= userRec.id;
        ChildAcc1.BillingPostalCode = 'A1A 1A1';
        insert ChildAcc1;
        
        AccountTeamMember newMem = new AccountTeamMember();
        newMem.TeamMemberRole = 'Owner';
        newMem.AccountId = ParentAcc.Id;
        newmem.UserId = ParentAcc.OwnerId; 
        insert newmem;
        
        AccountTeamMember newMem2 = new AccountTeamMember();
        newMem2.TeamMemberRole = 'SDC';
        newMem2.AccountId = ParentAcc.Id;
        newMem2.UserId = userRec2.Id; 
        insert newMem2 ;

        AccountTeamMember newMem3 = new AccountTeamMember();
        newMem3.TeamMemberRole = 'SDC';
        newMem3.AccountId = ChildAcc1.Id;
        newMem3.UserId = userRec2.Id; 
        insert newMem3 ;



    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testTraversingFromChild(){
        setUpData(); 
        Set<Id> childIds =new Set<Id>();
        childIds.add(ChildAcc1.id);
        test.startTest();
            TAG_SyncAccountTeamMembers obj = new TAG_SyncAccountTeamMembers();
            obj.addTeamMembersTraversingFromChild(childIds );
        test.stopTest(); 
        List<AccountTeamMember > atmList = [select id,  Account.Id from AccountTeamMember  where  Account.Id=:ChildAcc1.id];
        system.assert( atmList.size()>1);
    }
     /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testTraversingFromParent(){
        setUpData(); 
        Set<Id> ParentIds =new Set<Id>();
        ParentIds .add(ParentAcc.id);
        test.startTest();
            TAG_SyncAccountTeamMembers obj = new TAG_SyncAccountTeamMembers();
            obj.addTeamMembersTraversingFromParent(ParentIds);
        test.stopTest(); 
        List<AccountTeamMember > atmList = [select id,  Account.Id from AccountTeamMember  where  Account.Id=:ChildAcc1.id];
        system.assert( atmList.size()>1);
    }
}