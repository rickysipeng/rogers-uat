/*
===============================================================================
Class Name : errorRecordsCleanUp_batch_Test
===============================================================================
PURPOSE: Test class for errorRecordsCleanUp_batch class

Author   : Jacob Klay, Rogers Communications
Date     : 24-June-2o14

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
===============================================================================
*/

@isTest (SeeAllData = true)
private class errorRecordsCleanUp_batch_Test{
    //static ErrorRecords__c rec;
    //static Account acc;
    //static MSD_Code__c msd; 
    //static AccountTeamMember atm;
    //static AccountTeamMember atm2;
    //static AccountTeamMember atm3;
    //static MSD_Code__share msdShare;
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
 
    static testMethod void verifyErrorRecordsDeletion(){
    List<ErrorRecords__c> errorList = new List<ErrorRecords__c>();
        for(Integer i = 0; i < 200; i++) {
            ErrorRecords__c rec = new ErrorRecords__c(Name = 'Unit Test ' + i);
            errorList.add(rec);
        }
        //test.startTest();
        insert errorList;
        //test.stopTest();
        System.debug('####errorList size: ' + errorList.size());
        
        User thisUser = [select Id from User where Id = :UserInfo.getUserId()];
        //User u;
        /*
        System.runAs (thisUser) {
        Profile p = [select id from profile where name='System Administrator' ];
        UserRole r = [Select id from userrole where name='Enterprise Executive'];
        u = new User(alias = 'TestUser', email='standarduser@testorg.com.rogers', 
            emailencodingkey='UTF-8', lastname='TestUser', 
            languagelocalekey='en_US', 
            localesidkey='en_US', profileid = p.Id, userroleid = r.Id,
            timezonesidkey='America/Los_Angeles', 
            username='standarduser@testorg.com.Rogers');
        } 
        */
        System.runAs(thisUser) {
                
        test.StartTest();
        Database.BatchableContext scc;
        errorRecordsCleanUp_batch sc = new errorRecordsCleanUp_batch();   
        //sc.Query ='SELECT id, account_name__c, userId__c, user_name__c, Error_Message__c, statusCode__c FROM ErrorRecords__c WHERE isDeleted = false and Name like \'Unit Test%\' ' + //'LIMIT 9999';
        
        sc.start(scc);
        List<ErrorRecords__c> scope = [SELECT id, account_name__c, userId__c, user_name__c, Error_Message__c, statusCode__c FROM ErrorRecords__c WHERE isDeleted = false and Name like 'Unit Test%' LIMIT 10];
        System.debug('####Scope size: ' + scope.size());
        sc.execute(scc,scope);
        sc.finish(scc);
        
        test.StopTest();
        }
    }
}