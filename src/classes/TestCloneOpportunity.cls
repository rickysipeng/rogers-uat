/**
 * 
 * Target to test: 
 *      CloneOpportunityController.cls
 *      CloneOpportunity.page
 *
 */
@isTest(seeAllData=true)
private class TestCloneOpportunity {
    
    // ====================================================
    // test 01: for unwanted call
    static testMethod void myTest01() {
        Opportunity baseOpp = new Opportunity();
        
        ApexPages.StandardController sc = new ApexPages.standardController(baseOpp);
        CloneOpportunityController cloneController = new CloneOpportunityController(sc);
        
        PageReference pageRef = Page.CloneOpportunity_RDC;
        Test.setCurrentPage(pageRef);
        System.assertEquals(false, cloneController.sourceOppIsValid);
    }
    
    // ====================================================
    // test 02: basic normal interaction
    //          with prospect sites and z-sites
    static testMethod void myTest02() {
        Opportunity baseOpp = createOpportunity();
        createProspectSites(baseOpp);
        // 
        // scenario 1: all sites are prospect sites
        ApexPages.StandardController sc = new ApexPages.standardController(baseOpp);
        CloneOpportunityController cloneController = new CloneOpportunityController(sc);
        
        PageReference pageRef = Page.CloneOpportunity_RDC;
        Test.setCurrentPage(pageRef);
        System.assertEquals(true, cloneController.sourceOppIsValid);
        
        // test the cancel button
        pageRef = cloneController.returnToSourceOpportunity();
        Test.setCurrentPage(pageRef);
        
        // test the clone functionality
        pageRef = Page.CloneOpportunity_RDC;
        pageRef = cloneController.CloneOpportunity();
        
        // verify cloned sites
    }
        
        
        
    // ====================================================
    // test 03: normal case
    //      with prospect and active
    static testMethod void myTest03() {
        Opportunity baseOpp = createOpportunity();
        createProspectSites(baseOpp);
        createActiveSites(baseOpp);
        
        // scenario 1: all sites are prospect sites
        ApexPages.StandardController sc = new ApexPages.standardController(baseOpp);
        CloneOpportunityController cloneController = new CloneOpportunityController(sc);
        
        PageReference pageRef = Page.CloneOpportunity_RDC;
        Test.setCurrentPage(pageRef);
        System.assertEquals(true, cloneController.sourceOppIsValid);
        
        // test the clone functionality
        cloneController.newOpp.Probability = 25;
        pageRef = Page.CloneOpportunity_RDC;
        pageRef = cloneController.CloneOpportunity();
        
        // verify cloned sites
        
        
    }
    
    
    
    // ====================================================
    // supporting functions
    // ====================================================
    
    static Opportunity createOpportunity()
    {
        List<RecordType > recTypeAccounts = new List <RecordType> ([Select id, recordtype.Name
                , SobjectType, DeveloperName 
            from RecordType 
            where recordtype.SobjectType in ('Account', 'Opportunity')
            order by SobjectType, DeveloperName]);
        Map <String,Id> rtDevNameToIdMap = new Map <String,Id> ();
        
        for (RecordType rtp : recTypeAccounts){
            rtDevNameToIdMap.put(rtp.SobjectType+'-'+rtp.DeveloperName, rtp.id);  
        }
        

        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = rtDevNameToIdMap.get('Account'+'-New_Account');
        a.BillingStreet = 'Street'; a.Account_Status__c = 'Assigned';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        insert a;   
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = rtDevNameToIdMap.get('Opportunity'+'-New_Opportunity');
        o.CloseDate = date.today();
        insert o;
        return o;
    }
    
    static void createProspectSites(Opportunity opp)
    {
        Site__c site11 = new Site__c(
            Street_Name__c = 'Somewhere1'
            , Suite_Floor__c = '11a'
            , Street_Number__c  = '4'
            , City__c = 'City'
            , Postal_Code__c = 'A1A1A1'
            , Province_Code__c = 'ON'
            //, ServiceableLocation__c = sl1Id
            , Opportunity__c = opp.Id
            , Is_a_Z_Site__c = false
            );
            
        Site__c site12 = new Site__c(
            Street_Name__c = 'Somewhere12'
            , Suite_Floor__c = '11a'
            , Street_Number__c  = '40'
            , City__c = 'City'
            , Postal_Code__c = 'A1A1A1'
            , Province_Code__c = 'ON'
            //, ServiceableLocation__c = sl1Id
            , Opportunity__c = opp.Id
            , Is_a_Z_Site__c = true
            );
        
        Site__c site21 = new Site__c(
            Street_Name__c = 'Somewhere02'
            , Suite_Floor__c = '11a'
            , Street_Number__c  = '224'
            , City__c = 'City'
            , Postal_Code__c = 'A1A1A1'
            , Province_Code__c = 'ON'
            //, ServiceableLocation__c = sl1Id
            , Opportunity__c = opp.Id
            , Is_a_Z_Site__c = false
            );
            
            
        List<Site__c> siteList1 = new List<Site__c> {site11, site12, site21};
        insert siteList1;
        
        A_Z_Site__c azSitePair01 = new A_Z_Site__c( Site_A__c = site11.Id, Site_Z__c = site12.Id );
        
        List<A_Z_Site__c> azSiteList = new List<A_Z_Site__c>{ azSitePair01};
        insert azSiteList;
    }
    
    // create active sites
    static void createActiveSites(Opportunity opp)
    {
        Site__c siteActive31 = new Site__c(
            Street_Name__c = 'Somewhere31'
            , Suite_Floor__c = '11a'
            , Street_Number__c  = '224'
            , City__c = 'City'
            , Postal_Code__c = 'A1A1A1'
            , Province_Code__c = 'ON'
            //, ServiceableLocation__c = sl1Id
            , Opportunity__c = opp.Id
            , Is_a_Z_Site__c = false
            );
        insert siteActive31;
        // 
        Active_Site__c as1 = new Active_Site__c(
            // ActiveSiteKey__c
            Site__c = siteActive31.Id
            // , Site__r.LocationKey_del__c, Site__r.Account__c,
            , Account__c = opp.AccountId
            //, Access_Type_Group__c
            //, Access_Type__c
            //, Account_ID__c
            //, Account_Status__c
            //, ActiveSiteKey__c
            //, Additional_Notes__c
            //, AS400_Account_Number__c
            //, Billing_Stop_Date__c
            //, Business_Segment__c
            //, Cancellation_Penalty__c
            //, Circuit_ID__c
            //, City_Address__c
            //, Contract_End_Date__c
            //, Contract_Number__c
            //, Contract_Start_Date__c
            //, Contract_Status__c
            //, Country__c
            //, CreatedById
            //, CreatedDate
            //, Existing_MRR__c
            //, Id
            //, IsDeleted
            //, LastActivityDate
            //, LastModifiedById
            //, LastModifiedDate
            //, Lost_Contract_Value__c
            //, MRC__c
            //, MRR__c
            //, Name
            //, NRC__c
            //, Opportunity_Number__c
            //, Opportunity_Record_Type__c
            //, Opportunity_Type__c
            //, Order_Tracker_Number__c
            //, Postal_Zip_Code__c
            //, Product_Code__c
            //, Provider__c
            //, Quote_Number__c
            //, Quote_Signed_Date__c
            //, Reason_for_Cancelling__c
            //, RecordTypeId
            //, Region__c
            //, Serviceability__c
            //, Service_City_2__c
            //, Service_City__c
            //, Service_Country_2__c
            //, Service_Country__c
            //, Service_End_Date__c
            //, Service_Postal_Code_2__c
            //, Service_Postal_Code__c
            //, Service_Province_2__c
            //, Service_Province__c
            //, Service_Request_Date__c
            //, Service_Start_Date__c
            //, Service_Status__c
            //, Service_Street_Direction__c
            //, Service_Street_Dir_2__c
            //, Service_Street_Name_2__c
            //, Service_Street_Name__c
            //, Service_Street_Number_2__c
            //, Service_Street_Number__c
            //, Service_Street_Type_2__c
            //, Service_Street_Type__c
            //, Service_Type__c
            //, Site__c
            //, State_Province_Code__c
            //, Street_Address__c
            //, SystemModstamp
            //, Termination_Date__c
            //, Term__c
            //, Total_Contract_Value__c
            //, Total_Remining_Contract_Value__c
            //, Z_Site__c 
            );
        insert as1;
    }
}