/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 
 Jan 23, 2015. Paul Saini. Removed Contact.Ext__c as per requirements.
                                               
 */
@isTest
private class TestMandatoryFields {

    static Id oppId, accountId, quoteId, qsId1, contactId, sl1Id, sl2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, site7Id;
    

static{
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
       
        Account a = new Account();
        a.name = 'Test Act - Online CIF';
        a.Business_Segment__c = 'Alternate';
        //a.RecordTypeId = mapRTa.get('New Account');
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street Wizard';
        a.BillingCity = 'MyCity Wizard';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L2L2L2';
        a.BillingState = 'ON';
        a.parentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        accountId = a.Id;


        Contact c = new Contact();
        c.AccountId = accountId;
        c.Email = 'testContact@test.com';
        c.FirstName = 'CustomerTest';
        c.LastName = 'CustomerLastName';
        c.Phone = '416-555-1212';
        c.MailingCountry ='CA';
        c.MailingState='AB';
        c.MailingPostalCode='A9A 9A9';
        c.Contact_Type__c = 'Executive';
        INSERT c;
        contactId = c.Id;
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp for Online CIF';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        sl2Id = sl2.Id;
       
        
        Site__c s1 = new Site__c();
        s1.Suite_Floor__c = '11a';
        s1.Street_Name__c = 'Somewhere1';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Suite_Floor__c = '11a';
        s2.Street_Name__c = 'Somewhere2';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId;
        insert s2;
        site2Id = s2.Id;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        
        Quote q = new Quote(Name='Online CIF Quote', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quoteId = q.Id;
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quoteId;
        insert qs;
        
        qsId1 = qs.Id;
        
        qs = new Quote_Site__c();
        qs.Site__c = site2Id;
        qs.Quote__c = quoteId;
        insert qs;
    
    /* Products for Original Term */
        // Install Product
        Product2 pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '12-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Access Product
        Product2 pAccess  = new Product2();
        pAccess.IsActive = true;
        pAccess.Name = 'Fibre';
        pAccess.Charge_Type__c = 'MRC';
        pAccess.Access_Type__c = 'Ethernet EON';
        pAccess.Access_Type_Group__c = 'Fibre';
        pAccess.Category__c = 'Access';
        pAccess.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pAccess.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccess.Service_Term__c = '12-Months';
        pAccess.Product_Install_Link__c = pInstall.Id;
        pAccess.Mark_Up_Factor__c = 1.0;
        pAccess.Start_Date__c = Date.today();
        pAccess.End_Date__c = Date.today()+1;
        insert pAccess;
        
        
        
        PricebookEntry pbeAccessStandard = new PricebookEntry();
        pbeAccessStandard.Pricebook2Id = sp1.id;
        pbeAccessStandard.UnitPrice = 60;
        pbeAccessStandard.Product2Id = pAccess.id;
        pbeAccessStandard.IsActive = true;
        pbeAccessStandard.UseStandardPrice = false;
        insert pbeAccessStandard;
        
        PricebookEntry pbeNonStandardAccess = new PricebookEntry();
        pbeNonStandardAccess.Pricebook2Id = sp.id;
        pbeNonStandardAccess.UnitPrice = 60;
        pbeNonStandardAccess.Product2Id = pAccess.id;
        pbeNonStandardAccess.IsActive = true;
        pbeNonStandardAccess.UseStandardPrice = false;
        insert pbeNonStandardAccess;
        Id priceBookEntryAccessId1 = pbeNonStandardAccess.Id;

        PricebookEntry pbeInstallStandard = new PricebookEntry();
        pbeInstallStandard.Pricebook2Id = sp1.id;
        pbeInstallStandard.UnitPrice = 50;
        pbeInstallStandard.Product2Id = pInstall.id;
        pbeInstallStandard.IsActive = true;
        pbeInstallStandard.UseStandardPrice = false;
        insert pbeInstallStandard;
        
        PricebookEntry pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 50;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
        Id priceBookEntryInstallId1 = pbeNonStandardInstall.Id;
                       
       
       QuoteLineItem qliInstall1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
       insert qliInstall1;
       QuoteLineItem qliAccess1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryAccessId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Install__c = qliInstall1.Id);      
       insert qliAccess1;
        
       
    }


    static testMethod void mandatoryFieldsTest1() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
        m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
        m1.Demarc__c = 'there';
        insert m1;
        
       PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        
        controller.getExistingContacts();
        controller.saveMandatoryFields();
        controller.createPDF();
        
        
        
    }
    
    /* Checks some of the bad things that may happen */
    static testMethod void mandatoryFields2(){
        /* no synced quote */
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        
        /* No Opportunity */
        pageRef = New PageReference('/apex/CollectMandatoryFields');
        Test.setCurrentPage(pageRef);
        sc = new ApexPages.standardController(opp);
        controller = new CollectMandatoryFieldsController(sc);
        
        /* Invalid Opp Id */
        pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(accountId));
        Test.setCurrentPage(pageRef);
        sc = new ApexPages.standardController(opp);
        controller = new CollectMandatoryFieldsController(sc);
        
        /* No Quote Sites */
        List<Quote_Site__c> qSites = [SELECT Id FROM Quote_Site__c WHERE Quote__c =:quoteId];
        DELETE qSites;

        pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(accountId));
        Test.setCurrentPage(pageRef);
        sc = new ApexPages.standardController(opp);
        controller = new CollectMandatoryFieldsController(sc);
    }
    
       /* Checks some more bad things that may happen */
        static testMethod void mandatoryFields3(){
             // Sync the quote to the opp
            Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
            opp.SyncedQuoteId = quoteId;
            UPDATE opp;
            
            /* No Quote Sites */
            List<Quote_Site__c> qSites = [SELECT Id FROM Quote_Site__c WHERE Quote__c =:quoteId];
            DELETE qSites;
    
            PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
            Test.setCurrentPage(pageRef);
            ApexPages.StandardController sc = new ApexPages.standardController(opp);
            CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        }
    
    static testMethod void mandatoryFieldsTest4() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
        m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
        m1.Demarc__c = 'there';
        insert m1;
        
        
       PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        
        pageRef.getParameters().put('contactLocation', 'bec_'+m1.Id);
        
        Contact c1 = new Contact();
        c1.Email = 'test@test.com';
        c1.FirstName  = 'Steve';
        c1.LastName = 'Test';
        c1.Phone = '4165551212';
        c1.HomePhone = '4162229393';
        c1.MobilePhone = '4168882282';
        c1.MailingCity = 'Toronto';
        c1.MailingCountry ='CA';
        c1.MailingState='AB';
        c1.MailingPostalCode='A9A 9A9';
        c1.Contact_Type__c = 'Executive';
        //PS: c1.Ext__c = '123';
        
        
        
        controller.contactMap.put('bec_'+m1.Id, c1);
        controller.addNewContact();
        controller.updateContact();
        // PS: removeed Ext__c 
        String key = c1.FirstName+c1.LastName+c1.Phone+c1.MobilePhone+c1.Email+c1.Title+c1.MailingCity+c1.MailingCountry+c1.MailingState+c1.MailingStreet+c1.MailingPostalCode;
        pageRef.getParameters().put('contactkey', key);
        controller.useExistingContact();
        
        
        CollectMandatoryFieldsController.getUnsavedContacts('bec_'+m1.Id, new List<String>{'bec_'+m1.Id});
        controller.submitMandatoryFields();
    }
    
    static testMethod void mandatoryFieldsTest5() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
        m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
    //    m1.Demarc__c = 'there';
        insert m1;
        
        
       PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        
        pageRef.getParameters().put('contactLocation', 'bec_'+m1.Id);
        
        Contact c1 = new Contact();
        c1.Email = 'test@test.com';
        c1.FirstName  = 'Steve';
        c1.LastName = 'Test';
        c1.Phone = '4165551212';
        c1.HomePhone = '4162229393';
        c1.MobilePhone = '4168882282';
        c1.MailingCity = 'Toronto';
        c1.MailingCountry ='CA';
        c1.MailingState='AB';
        c1.MailingPostalCode='A9A 9A9';
        c1.Contact_Type__c = 'Executive';
        //PS: c1.Ext__c = '123';
        
        
        
        controller.contactMap.put('bec_'+m1.Id, c1);
        controller.addNewContact();
      //  controller.updateContact();
        //String key = c1.FirstName+c1.LastName+c1.Phone+c1.MobilePhone+c1.Email+c1.Ext__c+c1.Title+c1.MailingCity+c1.MailingCountry+c1.MailingState+c1.MailingStreet+c1.MailingPostalCode;
       // pageRef.getParameters().put('contactkey', key);
       // controller.useExistingContact();
        controller.submitMandatoryFields();
        
    }
    
    static testMethod void mandatoryFieldsTest6() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
    //    m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
        m1.Demarc__c = 'there';
        insert m1;
        
        
       PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        
        pageRef.getParameters().put('contactLocation', 'bec_'+m1.Id);
        
        Contact c1 = new Contact();
        c1.Email = 'test@test.com';
        c1.FirstName  = 'Steve';
        c1.LastName = 'Test';
        c1.Phone = '4165551212';
        c1.HomePhone = '4162229393';
        c1.MobilePhone = '4168882282';
        c1.MailingCity = 'Toronto';
        c1.MailingCountry ='CA';
        c1.MailingState='AB';
        c1.MailingPostalCode='A9A 9A9';
        c1.Contact_Type__c = 'Executive';
        //PS: c1.Ext__c = '123';
        
        
        
        controller.contactMap.put('bec_'+m1.Id, c1);
        controller.addNewContact();
      //  controller.updateContact();
        //String key = c1.FirstName+c1.LastName+c1.Phone+c1.MobilePhone+c1.Email+c1.Ext__c+c1.Title+c1.MailingCity+c1.MailingCountry+c1.MailingState+c1.MailingStreet+c1.MailingPostalCode;
       // pageRef.getParameters().put('contactkey', key);
       // controller.useExistingContact();
        controller.submitMandatoryFields();
        
    }
    
        static testMethod void mandatoryFieldsTest7() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
        m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
        m1.Demarc__c = 'there';
        insert m1;
        
        
       PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        
        pageRef.getParameters().put('contactLocation', 'bec_'+m1.Id);
        
        Contact c1 = new Contact();
        c1.Email = 'test@test.com';
     //   c1.FirstName  = 'Steve';
        c1.LastName = 'Test';
        c1.Phone = '4165551212';
        c1.HomePhone = '4162229393';
        c1.MobilePhone = '4168882282';
        c1.MailingCity = 'Toronto';
        c1.MailingCountry ='CA';
        c1.MailingState='AB';
        c1.MailingPostalCode='A9A 9A9';
        c1.Contact_Type__c = 'Executive';
        //c1.Ext__c = '123';
        
        
        
        controller.contactMap.put('bec_'+m1.Id, c1);
        controller.addNewContact();
      //  controller.updateContact();
        //String key = c1.FirstName+c1.LastName+c1.Phone+c1.MobilePhone+c1.Email+c1.Ext__c+c1.Title+c1.MailingCity+c1.MailingCountry+c1.MailingState+c1.MailingStreet+c1.MailingPostalCode;
       // pageRef.getParameters().put('contactkey', key);
       // controller.useExistingContact();
        controller.submitMandatoryFields();
        
    }
    
    static testMethod void mandatoryFieldsTest8() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        /*
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
        m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
        m1.Demarc__c = 'there';
        insert m1;
        
       PageReference pageRef = New PageReference('/apex/CollectMandatoryFields?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        CollectMandatoryFieldsController controller = new CollectMandatoryFieldsController(sc);
        */
        CIFWebServices.createMandatoryFields(opp.Id);
        
        
        
    }
    
    static testMethod void mandatoryFieldsPDFTest1() {
        // Sync the quote to the opp
        Opportunity opp = [SELECT Id, SyncedQuoteId FROM Opportunity WHERE Id = :oppId];
        opp.SyncedQuoteId = quoteId;
        UPDATE opp;
        
        Mandatory_COP_Fields__c m1 = new Mandatory_COP_Fields__c();
        m1.Quote_Site__c = qsId1;
        m1.Suite_Floor__c = '12';
        m1.Roger_Account_Optional__c = '111';
        m1.Demarc__c = 'there';
        insert m1;
        
       PageReference pageRef = New PageReference('/apex/MandatoryFieldsPDF?oppId=' + String.valueOf(oppId));
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(opp);
        MandatoryFieldsPDFController controller = new MandatoryFieldsPDFController(sc);
        
        controller.previewPDF();
        
    }
}