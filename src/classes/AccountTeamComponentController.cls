public without sharing class AccountTeamComponentController {
     public Id accountId {get; set;}
         
     public String getAccountTeam(){
         String aTeamHTML = '';
         try{
            List<AccountTeamMember> accountTeam = [SELECT AccountId, Id, UserId, User.Name FROM AccountTeamMember WHERE accountId = :accountId];    

            for (AccountTeamMember aTeamMember : accountTeam){
                aTeamHTML += aTeamMember.User.Name + '<br/>';
            }
        }catch(Exception ex){
            System.debug('Exception: Unable to obtain the Team members for the account');
        }

        return aTeamHTML;
     }
     @isTest(SeeAllData=true)
     static  void testAccountTeamMemberComponent1() {
       
       List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act 23';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Street'; a_carrier.Account_Status__c = 'Assigned';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        insert a_carrier;   
        
        AccountTeamComponentController aTeam = new AccountTeamComponentController();
        aTeam.accountId = a_carrier.Id;
        aTeam.getAccountTeam();
       
     }
     @isTest(SeeAllData=true)
     static  void testAccountTeamMemberComponent2() {
       
       List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act 23';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Street'; a_carrier.Account_Status__c = 'Assigned';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        insert a_carrier;   
        
        Profile pEnterprise = [select id from profile where name='EBU - Rogers Account Executive'];
        Profile pNSA = [select id from profile where name='EBU - Rogers NSA'];
        
         User uNSAManager = new User(alias = 'stand3', email='standarduserEnterprise3@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise3', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pNSA.Id, 
         timezonesidkey='America/Los_Angeles', username='rbsEnterprise3User@testorg.com');
        insert uNSAManager;
        
         User uNSA = new User(alias = 'stand2', email='standarduserEnterprise2@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise2', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pNSA.Id, ManagerId = uNSAManager.Id,
         timezonesidkey='America/Los_Angeles', username='rbsEnterprise2User@testorg.com');
         insert uNSA;
        
        User uSales = new User(alias = 'stand1', email='standarduserEnterprise@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pEnterprise.Id, Network_Solutions_Architect__c = uNSA.Id,
         timezonesidkey='America/Los_Angeles', username='rbsEnterpriseUser@testorg.com');
         insert uSales;
        
        AccountTeamMember tMember = new AccountTeamMember();
        tMember.AccountId = a_carrier.Id;
        tMember.UserId = uSales.Id;
        INSERT tMember;
        
        AccountTeamComponentController aTeam = new AccountTeamComponentController();
        aTeam.accountId = a_carrier.Id;
        aTeam.getAccountTeam();
        
     }
     @isTest(SeeAllData=true)
     static  void testAccountTeamMemberComponent3() {
       
       List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        
        Account a_carrier = new Account();
        a_carrier.name = 'Test Act 23';
        a_carrier.Business_Segment__c = 'Alternate';
        a_carrier.RecordTypeId = mapRTa.get('Carrier Account');
        a_carrier.BillingStreet = 'Street'; a_carrier.Account_Status__c = 'Assigned';
        a_carrier.BillingCity = 'Ontario';
        a_carrier.BillingCountry = 'CA';
        a_carrier.BillingPostalCode = 'A9A 9A9';
        a_carrier.BillingState = 'ON';
        insert a_carrier;   
        
        AccountTeamComponentController aTeam = new AccountTeamComponentController();
        aTeam.getAccountTeam();
       
     }
}