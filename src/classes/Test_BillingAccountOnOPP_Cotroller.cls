/*
===============================================================================
 Class Name   : Test_BillingAccountOnOPP_Cotroller 
===============================================================================
PURPOSE:    This class a controller class for BillingAccountOnOPP_Cotroller .
              
Developer: Deepika Rawat
Date: 04/01/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/01/2015           Deepika Rawat               Original Version
===============================================================================*/
@isTest (seeAllData=True) 
private class Test_BillingAccountOnOPP_Cotroller{
    private static Account ParentAcc;
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
    private static User userRec;
    private static Billing_Account__c bAccount;
    private static Opportunity theOpportunity1;

     //create Test Data
     private static void setUpData(){
         userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US');
        insert userRec;

        //Account 1
        ParentAcc= new Account();
        ParentAcc.Name = 'ParentAcc';
        ParentAcc.Account_Status__c = 'Assigned';
        ParentAcc.ParentID= null;
        ParentAcc.OwnerId= userRec.id;
        insert ParentAcc;

        bAccount = new Billing_Account__c();
        bAccount.Account__c=ParentAcc.id;
        bAccount.Name='Testing Only';
        insert bAccount;

        Schema.DescribeSObjectResult opportunitySchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> opportunityRecordTypeInfo = opportunitySchema.getRecordTypeInfosByName();     
        Id oppRt = opportunityRecordTypeInfo.get('Data Centre - RFP').getRecordTypeId();             
                
        theOpportunity1 = new Opportunity (Name = 'theOpportunity ', 
                           RecordTypeId = oppRt,
                           AccountId = ParentAcc.id,
                           CloseDate = date.Today(),
                           StageName = 'Identify',
                           of_Cabinets__c = 0,
                           Amount = 50,
                         Billing_Account__c =bAccount.id);
        insert theOpportunity1;

    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testBANoSelect(){
        setUpData(); 
        test.startTest();
        Apexpages.currentPage().getParameters().put('accid',ParentAcc.Id);
        Apexpages.currentPage().getParameters().put('oppid',theOpportunity1.id);
        BillingAccountOnOPP_Cotroller  cls = new BillingAccountOnOPP_Cotroller (); 
        cls.selectBA();
        test.stopTest(); 
    }
    /*******************************************************************************************************
    * @description: This is a positive test method with search criteria as Account name.
    *******************************************************************************************************/
    static testmethod void testBASelect(){
        setUpData(); 
        test.startTest();
        Apexpages.currentPage().getParameters().put('accid',ParentAcc.Id);
        Apexpages.currentPage().getParameters().put('oppid',theOpportunity1.id);
        BillingAccountOnOPP_Cotroller  cls = new BillingAccountOnOPP_Cotroller (); 
        cls.lstBAWrapper[0].isSelected =true;
        cls.selectBA();
        test.stopTest(); 
    }
}