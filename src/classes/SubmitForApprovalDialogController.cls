public without sharing class SubmitForApprovalDialogController {
  public id quoteId {get; set;}
  public Quote q {get; set;}
  public String redirectUrl {get; set;}
  
  public SubmitForApprovalDialogController(ApexPages.StandardController controller) {
    quoteId = controller.getId();
    q = [SELECT id, OpportunityId, Discount_Reasons__c, Competitor__c, Competitor_Match__c FROM Quote WHERE id = :quoteId]; 
    this.redirectUrl = '/' + quoteId; 
  }
  
  public void executeLogic() {
      system.debug('**->>q--'+q);
      system.debug('**->>q.Competitor_Match__c--'+q.Competitor_Match__c);
       system.debug('**->>q.Competitor__c--'+q.Competitor__c);
      if(q.Competitor_Match__c==true && (q.Competitor__c==null || q.Competitor__c=='' || q.Competitor__c == '--None--'))
      {
           ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,label.Transfer_Competitor_Match));
            this.redirectUrl = ''; 
      }else if (q.Competitor_Match__c==false && q.Competitor__c!=null && q.Competitor__c != '--None--'){
        
        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,  label.Transfer_Competitor_NotRequired ));
         this.redirectUrl = ''; 
      }
      else 
      {
        UPDATE q;
        
        String errMessage = OpportunityWebServices.submitForApproval(quoteId);
        
        if (''.equals(errMessage))
          this.redirectUrl = '/' + quoteId; 
        else
          this.redirectUrl = ''; 
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.WARNING,errMessage ));
      }  
      
  }
}