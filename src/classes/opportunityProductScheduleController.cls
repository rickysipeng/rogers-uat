/*Class Name :opportunityProductScheduleController.
 *Description : Controller Class for opportunityProductScheduleTemplate, Contains all the Methods used in tha Page.
 *Created By : Rajiv Gangra.
 *Created Date :12/09/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
public class opportunityProductScheduleController {

    
    Public List<OpportunityLineItemSchedule> relatedschedulelst{get;set;}
    Public List<OpportunityLineItemSchedule> relatedschedulelstUpdate= new List<OpportunityLineItemSchedule>();
    Public List<OpportunityLineItemSchedule> relatedschedulelstDelete= new List<OpportunityLineItemSchedule>();
    Public List<Sales_Measurement__c> lstSalesSchedule= new List<Sales_Measurement__c>();
    Public OpportunityLineItem oppLineItem{get;set;}
    Public string lineGrouping{get;set;}
    Public string prodFamily{get;set;}
    Public boolean isDeploymentForm{get;set;}
    Public boolean isEditable{get;set;}
    Public map<string,Product_Type_List__c> prodTypelst = Product_Type_List__c.getAll();
    public decimal totalRevenue{get;set;}
    public decimal actualTotalRevenue{get;set;}
    public decimal totalQuantity{get;set;}
    public String type;
    public opportunityProductScheduleController(ApexPages.StandardController controller) {
    }
    /*
    @ Method Name: Relatedschedulelst
      Description: This method will get the List of Related Schedule Based on Opportunity Passed and set Required Values for Further Procss.
      Return Type: NA.
      Input Param: NA.   
    */
    public Void Relatedschedulelst() {
        IF(ApexPages.currentPage().getParameters().get('id') !=null && ApexPages.currentPage().getParameters().get('id')!=''){
            // set Revenue and Quantity to 0 so as to recalculate based on New data
            totalRevenue=0;
            totalQuantity=0;
            try{
                // Get related Opportunity product to assign Required Values
                oppLineItem=[select id,Product_Family__c,TotalPrice, Data_of_Total_Revenue__c, One_Time_Value__c, Monthly_Value__c, Contract_term__c, OpportunityId,Quantity,of_Installments__c,ARPU__c,Opportunity.OwnerID,Opportunity.Account.Restricted__c,PricebookEntry.Product2ID from OpportunityLineItem WHERE Id = :ApexPages.currentPage().getParameters().get('id')];
                // Assigning Value for line grouping to add in the New Schedules Created
                for(Product_Type_List__c pT:Product_Type_List__c.getall().values()){
                    if(pT.Product_Family__c==oppLineItem.Product_Family__c){
                        lineGrouping=pT.Name;
                    }
                }
                actualTotalRevenue=oppLineItem.TotalPrice;
                // Get list of all the Related Schedule based on the Opportunity Product
                relatedschedulelst=[select id,Type,ScheduleDate,Revenue,Quantity,OpportunityLineItemId,Description from OpportunityLineItemSchedule WHERE OpportunityLineItemId = :ApexPages.currentPage().getParameters().get('id') ORDER BY ScheduleDate ASC];
                // Calculation for Total Quantity and Revenue based on the Information recived from Related Schedule List
                for(OpportunityLineItemSchedule oLIS:relatedschedulelst){
                   totalRevenue=totalRevenue+oLIS.Revenue;
                   if(oLIS.Quantity !=null){
                       totalQuantity=totalQuantity+oLIS.Quantity;
                   }
                }
                totalRevenue= totalRevenue.setScale(2);
                // Verify the form Type and Check isEdiatble based on Account Restricted field
                if(prodTypelst.get(lineGrouping).Form_Type__c=='Deployment Form'){
                    isDeploymentForm=true;
                    isEditable= false;
                    type='Both';
                }else{
                    type='Revenue';
        
                    if(oppLineItem.Opportunity.Account.Restricted__c =='yes'){
                        isEditable= False;
                    }else{
                        isEditable= true;
                    }
                }
                // Get the Product Family Based on the Line Grouping form the Custom Setting
                prodFamily=prodTypelst.get(lineGrouping).Product_Family__c;
                for(integer i=0;i<=5;i++){
                    OpportunityLineItemSchedule oliScheduleNew= new OpportunityLineItemSchedule();
                    oliScheduleNew.ScheduleDate=System.Today();
                    relatedschedulelst.add(oliScheduleNew);
            }
            }catch(exception ex){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Improper_Data_Received_Kindly_Verify_the_Data_sent));
            }
        }
    }
    /*
    @ Method Name: SaveSchedule
      Description: This Method will Update the List of schedules Related to the Current Opportunity update Related Items.
      Return Type: NA.
      Input Param: NA.   
    */
    public PageReference SaveSchedule() {
        relatedschedulelstUpdate.clear();
        relatedschedulelstDelete.clear();
        if(oppLineItem !=null){
            system.debug('getting into code');
            try{
                Decimal totalModifiedRevenue = 0;
                for(OpportunityLineItemSchedule oliSchedule:relatedschedulelst){
                    if(oliSchedule.revenue !=null) {
                        totalModifiedRevenue += oliSchedule.revenue;
                    }
                    if(oliSchedule.Quantity <1){
                        oliSchedule.Quantity=null;
                    }
                    if((type=='both' && oliSchedule.Quantity !=null) || (type=='revenue' && oliSchedule.revenue !=null)){
                        if(oliSchedule.id ==null){
                            oliSchedule.Type=type;
                            oliSchedule.OpportunityLineItemId=oppLineItem.id;
                        }
                        if(type=='both'){
                               oliSchedule.revenue= oliSchedule.Quantity*Integer.Valueof(oppLineItem.ARPU__c) *Integer.Valueof(oppLineItem.Contract_term__c);
                        }
                        relatedschedulelstUpdate.add(oliSchedule);
                    }
                    if((type=='both' && (oliSchedule.Quantity ==null || oliSchedule.Quantity==0)) || (type=='revenue' && (oliSchedule.revenue ==null || oliSchedule.revenue ==0))){
                        if(oliSchedule.id !=null){
                            relatedschedulelstDelete.add(oliSchedule);
                        }
                    }
                }
                totalRevenue=totalModifiedRevenue;
                if(totalModifiedRevenue != actualTotalRevenue && type=='revenue') {
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Total_value_mismatch));
                    return null;
                }
                if(relatedschedulelstUpdate.size() !=0){
                    Upsert relatedschedulelstUpdate;
                }
                if(relatedschedulelstDelete.size() !=0){
                    delete relatedschedulelstDelete;
                }
        // update Opportunity Product in case the quantity is 0 or null 
                OpportunityLineItem oliUpdated=[select id,Quantity,of_Installments__c from OpportunityLineItem WHERE Id =:oppLineItem.id];
                if(oliUpdated.Quantity == 0){
                    oliUpdated.Quantity=1;
                }
                oliUpdated.of_Installments__c=relatedschedulelstUpdate.size();
                Update oliUpdated;
        // Get the List of related Sales Measurement records
                List<Sales_Measurement__c> lstRelatedSchedule=[Select Id,Name from Sales_Measurement__c where Opportunity_Line_Item_ID__c=:oppLineItem.id];
                delete lstRelatedSchedule;
                for(OpportunityLineItemSchedule oli:relatedschedulelstUpdate){
                    
                    Sales_Measurement__c salesMeasureSchedule= new Sales_Measurement__c();
                    salesMeasureSchedule.Opportunity__c=oppLineItem.OpportunityId;
                    salesMeasureSchedule.Opportunity_Line_Item_ID__c=oli.OpportunityLineItemId;
                    salesMeasureSchedule.Deployment_Date__c=oli.ScheduleDate;
                    salesMeasureSchedule.OwnerID=oppLineItem.Opportunity.OwnerID;
                    salesMeasureSchedule.Comments__c=oli.description;
                    salesMeasureSchedule.product__c=oppLineItem.PricebookEntry.Product2ID;
                    salesMeasureSchedule.Quota_Family__c=prodFamily;
                    salesMeasureSchedule.Quantity__c=oli.Quantity;
                    if( salesMeasureSchedule.Quota_Family__c !='Wireless - Voice & Data'){
                        salesMeasureSchedule.Revenue__c=oli.Revenue;
                    }else{
                       Sales_Measurement__c salesMeasureSchedule1= new Sales_Measurement__c();
                       salesMeasureSchedule1.Quota_Family__c=prodFamily;
                       salesMeasureSchedule1.Opportunity__c=oppLineItem.OpportunityId;
                       salesMeasureSchedule1.Opportunity_Line_Item_ID__c=oppLineItem.id;
                       salesMeasureSchedule1.Deployment_Date__c=oli.ScheduleDate;
                       salesMeasureSchedule1.OwnerID=oppLineItem.Opportunity.OwnerID;
                       salesMeasureSchedule1.Comments__c=oli.description;
                       salesMeasureSchedule1.product__c=oppLineItem.PricebookEntry.Product2ID;
                       salesMeasureSchedule1.Quota_Family__c='Wireless - All Data';
                       salesMeasureSchedule1.Quantity__c=oli.Quantity;
                       salesMeasureSchedule.Quota_Family__c='Wireless - Voice';
                       if(oli.Revenue !=null && oppLineItem.Data_of_Total_Revenue__c!=null){
                           salesMeasureSchedule1.Revenue__c=oli.Revenue *(oppLineItem.Data_of_Total_Revenue__c/100);
                       }
                       // Adding the same Product 2 times for once for Voice and 2nd time for Data
                       lstSalesSchedule.add(salesMeasureSchedule1);
                       if(oli.Revenue !=null && oppLineItem.Data_of_Total_Revenue__c!=null){
                        salesMeasureSchedule.Revenue__c=oli.Revenue *(1-(oppLineItem.Data_of_Total_Revenue__c/100));
                       }
                                        
                    }
                    lstSalesSchedule.add(salesMeasureSchedule);
                }
                insert lstSalesSchedule;
                PageReference redirect = new PageReference('/'+oliUpdated.id); 
                redirect.setRedirect(true); 
                return redirect;
            }catch(exception ex){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Please_Verify_the_data_Inserted));
                return null;
            }
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Please_Verify_the_data_Inserted));
            return null;
         }
        
    }
    /*
    @ Method Name: recalulateSchedule
      Description: This Method will recalculate the Value of total Revenue and total Quantity  Based on the Quantity passes and Update the same.
      Return Type: NA.
      Input Param: NA.   
    */
    public void recalulateSchedule() {
        totalRevenue=0;
        totalQuantity=0;
        if(oppLineItem !=null && relatedschedulelst.size()!=0){
            try{
                for(OpportunityLineItemSchedule oliSchedule:relatedschedulelst){
                    if((type=='both' && oliSchedule.Quantity !=null) || (type=='revenue' && oliSchedule.revenue !=null)){
                        if(type=='both'){
                               oliSchedule.revenue= oliSchedule.Quantity*Integer.Valueof(oppLineItem.ARPU__c)*Integer.Valueof(oppLineItem.Contract_term__c);
                        }
                    }
                    if(oliSchedule.Revenue !=null){
                       totalRevenue=totalRevenue+oliSchedule.Revenue;
                    }
                   if(oliSchedule.Quantity !=null){
                       totalQuantity=totalQuantity+oliSchedule.Quantity;
                    }
                }
            }catch(exception ex){
                ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Please_Verify_the_data_Inserted));
            }
        }
    }
    /*
    @ Method Name: AddScheduleRow
      Description: Adds New row for Schedule.
      Return Type: NA.
      Input Param: NA.   
    */
    public PageReference AddScheduleRow() {
        if(oppLineItem !=null){
            try{
                OpportunityLineItemSchedule oliScheduleNew= new OpportunityLineItemSchedule();
                oliScheduleNew.ScheduleDate=System.Today();
                relatedschedulelst.add(oliScheduleNew);
            }catch(exception ex){
                    ApexPages.addmessage(new ApexPages.message(ApexPages.Severity.ERROR,label.Please_Verify_the_data_Inserted));
            }
        }
        return null;
    }
}