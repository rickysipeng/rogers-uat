public class AccountEditRedirect {
 	
 	public String newType {get; set;}
 
    public AccountEditRedirect(ApexPages.StandardController controller) {
        this.controller = controller;
      	Account acc = (Account)controller.getRecord();
      	rType = acc.RecordTypeId; 
      	newType = ApexPages.currentPage().getParameters().get('RecordType');
    }
 
    public PageReference redirect(){

    PageReference returnURL;
    Account a = null;
    try{
    	a = [Select id, recordtypeid From Account Where Id = :ApexPages.currentPage().getParameters().get('id') LIMIT 1];
    }catch (Exception ex){
    	System.debug('AccountEditRedirect.redirect(): Account does not exist!');
    	return null;
    }
    
    // Redirect if Record Type corresponds to custom VisualForce page
    if(rType.startsWith('01230000000wDYF') || rType.startsWith('01230000000wDXR')) {
        returnURL = new PageReference('/apex/addNewChannelAccount');
    }else{
         returnURL = new PageReference('/' + a.id + '/e');
         returnURL.getParameters().put('nooverride', '1');
         returnURL.getParameters().put('RecordType', newType); 
    }
 
    returnURL.getParameters().put('id', a.id);
     
    return returnURL.setRedirect(true);
    }
 
    private final ApexPages.StandardController controller;
    public String rType {get; set;}

}