/*Class Name :Test_TAG_OpportunityDMWarningController.
 *Description :Test Class for showing 'This Account is not Marshalled' Error Message  
                on OpportunityDetailPage
 *Created By : Jayavardhan Raju.
 *Created Date :03/16/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
@isTest (seeAllData=False) 
private class Test_TAG_OpportunityDMWarningController {

    private static Account parentAccount;    
    private static Opportunity testOpp;
    private static User userRec;
    private static User userRec2;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static Profile pEmp = [Select Id from Profile where Name Like 'System Administrator' limit 1];
   // private ID AccountRecordTypeId = [select Id,sObjecttype from recordType where name='New Account' and sObjecttype = 'Account'].id;
     //create Test Data
     private static void setUpData(){
        //Data for Custom Settings
        TAG_CS.Unassigned_User__c ='Unassigned User';        
        insert TAG_CS;
        
    }
    
      /*******************************************************************************************************
    * @description: This is a positive test method with New Account record type.
    *******************************************************************************************************/
    private static testMethod void OpportunityRecord()
    {
        setUpData();
        //ID AccountRecordTypeId = [select Id,sObjecttype from recordType where name='New Account' and sObjecttype = 'Account'].id;     
        userRec = new User(LastName = 'Mark O’Roger', Alias = 'alRoger1', Email='test@Rogertest.com', Username='test@Rogertest1.com', CommunityNickname = 'nickRog1', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Channel__c = 'Business Segment',Region__c = 'National',Sub_Region__c = 'National');
        insert userRec;
        userRec2 = new User(LastName = 'Mark O’Roger2', Alias = 'aRoger2', Email='test@Rogertest.com', Username='test@Rogertest2.com', CommunityNickname = 'nickRog2', ProfileId = pEmp.Id, TimeZoneSidKey = 'America/Los_Angeles', LocaleSidKey = 'en_US', EmailEncodingKey = 'ISO-8859-1', LanguageLocaleKey = 'en_US',Channel__c = 'Business Segment',Region__c = 'National',Sub_Region__c = 'National');
        insert userRec2;

        //ParentAcc
        parentAccount= new Account();
        parentAccount.Name = 'ParentAcc1';
        parentAccount.Account_Status__c = 'Assigned';        
        parentAccount.OwnerId= userRec.id; 
        parentAccount.ParentId = null;       
        RecordType rt = [select Id,Name from RecordType where Name = 'Carrier Account' and SobjectType = 'Account' limit 1];
        parentAccount.BillingStreet = 'Street';
        parentAccount.BillingCity = 'Ontario';
        parentAccount.BillingCountry = 'CA';
        parentAccount.BillingPostalCode = 'A9A 9A9';
        parentAccount.BillingState = 'ON';
        parentAccount.RecordTypeId = rt.Id;
        insert parentAccount;
        
        system.debug('============>parentAccount.RecordType'+parentAccount.RecordType.Name);
          testOpp = new Opportunity();
            testOpp.Name ='Op1';
            testOpp.StageName = 'Won';
            testOpp.AccountId = parentAccount.Id;
            testOpp.CloseDate = Date.today();
            insert testOpp;
        ApexPages.StandardController sc = new ApexPages.standardController(testOpp);
        TAG_OpportunityDMWarningController cloneController = new TAG_OpportunityDMWarningController(sc);
        
        PageReference pageRef = Page.TAG_OpportunityDMWarning;
        Test.setCurrentPage(pageRef);        
    }
}