public with sharing class BrowserUsageController {
	
	public Browser_Usage_Statistics__c totalUsage {get; set;}

	public BrowserUsageController(ApexPages.StandardController controller){
		totalUsage = [SELECT iPad__c, Chrome__c, iPhone__c, Internet_Explorer__c, Safari__c, Other__c, Blackberry__c FROM Browser_Usage_Statistics__c LIMIT 1];
	}
}