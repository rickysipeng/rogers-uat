@isTest(SeeAllData=true)

public class InactiveServiceGroupTest {
    static testMethod void inactivateServiceGroup() {
    
    // create new Service Group
    Service_Group__c newSG = new Service_Group__c();
    
    // populate required fields for Service Group
    newSG.Group_Number__c = '12345';
    newSG.Group_Name__c = 'Test InactiveServiceGroup';
    
    // insert new Service Group
    insert newSG;
    
    // create new Service Group Member
    Service_Group_Member__c newSGM = new Service_Group_Member__c();
    
    // populate required fields for Service Group Member
    newSGM.Service_Group__c = newSG.Id;
    
    // insert new Service Group Member
    insert newSGM;
    
    // create new Account
    Account newAccount = new Account();
    
    // populate required fields for Account
    newAccount.Name = 'Test InactiveServiceGroup';
    newAccount.NumberOfEmployees = 10;
    RecordType rt = [select Id from RecordType where Name = 'Carrier Account' and SobjectType = 'Account' limit 1];
        newAccount.BillingStreet = 'Street';
        newAccount.BillingCity = 'Ontario';
        newAccount.BillingCountry = 'CA';
        newAccount.BillingPostalCode = 'A9A 9A9';
        newAccount.BillingState = 'ON';
    newAccount.RecordTypeId = rt.Id;
    newAccount.Account_Status__c = 'New';
    newAccount.Phone = '4164121234';
    
    // insert new Account
    insert newAccount;
    
    // create new Account Assignment
    Account_Assignment__c newAA = new Account_Assignment__c();
    
    // populate required fields for Account Assignment
    newAA.Account__c = newAccount.Id;
    newAA.Service_Group__c = newSG.Id;
    
    // insert new Account Assignment
    insert newAA;
        
    // deactivate Account Assignment
    newAA.Inactive__c = true;
    
    // update Account Assignment
    update newAA;
    
    // deactivate Service Group
    newSG.Inactive__c = true;
    
    // update Service Group
    update newSG;
    
    // verify Service Group becomes inactive
    Service_Group__c newSG_3 = [SELECT Inactive__c FROM Service_Group__c WHERE Id = :newSG.Id];
    System.assertEquals( true, newSG_3.Inactive__c);
    }
}