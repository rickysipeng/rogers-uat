/*Class Name :SalesForecastDeletionSchedular.
 *Description : .
 *Created By : Deepika Rawat.
 *Created Date :05/11/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class uploadMSDSnapshotSchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        ////integer SFCheckPoint = Integer.ValueOf(Static_Data_Utilities__c.getInstance('Recalculation Period').Value__c);
       // date dT1=system.Today().addDays(-SFCheckPoint);
        uploadMSDSnapshotbatch uploadsnapshotObj = new uploadMSDSnapshotbatch ();
        uploadsnapshotObj.Query = 'select id , Name, Account__c, Churn_MTD__c, Churn_QTD__c,Account__r.Name, Churn_YTD__c, Data_Revenue__c, MSD_Active_Data_Only_Lines__c, MSD_Active_Voice_Data_Lines__c, MSD_Active_Voice_Only_Lines__c, MSD_ARPU__c, MSD_Code_Status__c, MSD_Type__c, Voice_Revenue__c from MSD_Code__c';
        ID BatchProcessId = Database.executeBatch(uploadsnapshotObj,200);
    }
}