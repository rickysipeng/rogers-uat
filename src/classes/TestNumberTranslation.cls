@IsTest
private class TestNumberTranslation {
	static numberTranslation nTranslation;
	static Boolean withAssertion = true;
	 
	static void testForOneLanguage( String langCode )
	{
		nTranslation.strLanguage = langCode; 
		nTranslation.dtValue = null;
		String currentDate = nTranslation.getdtDate(); 
		if(withAssertion) System.assertEquals(null, currentDate);

		nTranslation.dtValue = date.today();
		currentDate = nTranslation.getdtDate(); 
		
		// test formatDouble with a number of cases
		String myNumber= nTranslation.formatDouble( 99.8,True, langCode) ;
		
		myNumber= nTranslation.formatDouble( 12345678901.0, True, langCode) ;

		myNumber= nTranslation.formatDouble( 123456, True, langCode) ;
		// assertion needs to consider the language 
		if (withAssertion)
		{
			if (langCode=='fr'){
				System.assertEquals( '123 456,00 $', myNumber);
			} else if (langCode=='en')
			{
				System.assertEquals( '$ 123,456.00', myNumber);
			}
		}
		
		myNumber= nTranslation.formatDouble( 1234567.12348, False, langCode) ;
		// assert for non currency will fail
		
		myNumber= nTranslation.formatDouble( 12345, True, langCode) ;
		
		myNumber= nTranslation.formatDouble( 999, True, langCode) ;
		if (withAssertion)
		{
			if (langCode=='fr'){
				System.assertEquals( '999,00 $', myNumber);
			} else if (langCode=='en')
			{
				System.assertEquals( '$ 999.00', myNumber);
			}
		}
		
		nTranslation.decNumber = null;
		String strNumber = nTranslation.getValue();
		if(withAssertion) System.assertEquals(null, strNumber);
		
		nTranslation.decNumber = 123456789.1234;
		strNumber = nTranslation.getValue();
	}
	
	static testMethod void testNumberTranslation()
	{
		nTranslation = new numberTranslation();
		
		testForOneLanguage('fr');
		
		testForOneLanguage('en');
		
	}

}