/*
* Specific CloneAll implementation for Case objects.
*
*/
public class CloneAllCaseController extends AbstractCloneAllController {

    public CloneAllCaseController (ApexPages.StandardController controller){
        super(controller); 
        
        hasSufficientPrivileges = getHasSufficientPrivileges(((Case)controller.getRecord()).Id);        
        system.debug('CloneAllCaseController: hasSufficientPrivileges = ' + hasSufficientPrivileges ); 
    }
    
    public override AbstractClone getClone(){
        return new CloneCase();
    }
   
}