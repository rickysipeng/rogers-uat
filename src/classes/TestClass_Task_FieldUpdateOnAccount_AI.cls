/*Class Name  : TestClass_Task_FieldUpdateOnAccount_AI.
 *Description : This is test class for task_FieldUpdateOnAccount_AI.
 *Created By  : Rajiv Gangra.
 *Created Date :29/08/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
* Modified : Aakanksha Patel(March 2015) : For TAG
*/
@isTest
private class TestClass_Task_FieldUpdateOnAccount_AI {
    private static Account accTestObj;
    private static Opportunity oppTestObj;
    private static Task taskTestObj;
    private static user objUser;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  
   // Create data for testing purpose
    private static void setUpData(){
        Profile profile = [Select Id From Profile Where name = 'System Administrator' Limit 1];
        String randomUserName = String.valueOf(System.now().getTime()) + '@test.com';
        objUser = new User(alias = 'test', email='test@test.com',
        emailencodingkey='UTF-8', lastname='TestUser1', languagelocalekey='en_US',
        localesidkey='en_US', profileid = profile.Id,
        timezonesidkey='America/Denver', username=randomUserName, isActive=true);
        insert objUser;
        
        
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

       accTestObj = new Account(Name='testAcc');
       accTestObj.BillingCountry= 'CA';
       accTestObj.BillingPostalCode = 'A9A 9A9';
       accTestObj.BillingState = 'MA';
       accTestObj.BillingCity='City';
       accTestObj.BillingStreet='Street';
       accTestObj.Account_Status__c= 'Assigned';
       accTestObj.ParentId = null;
       insert accTestObj;
       
       oppTestObj = new Opportunity(Name='testAcc');
       oppTestObj.StageName='Cloased Won';
       oppTestObj.CloseDate=system.today();
       oppTestObj.AccountID=accTestObj.id;
       insert oppTestObj;
       
       taskTestObj = new Task();
             
    }
    static testmethod void myUnitWithEventAccount(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            taskTestObj.WhatID=accTestObj.id;
            insert taskTestObj;
            Datetime DT=[select id,createdDate from Task Where Id=:taskTestObj.id].createdDate;
            Date createdDate = date.newinstance(dT.year(), dT.month(), dT.day());
            Account updateAcc=[select Id,Last_Activity_Owner__c,Last_Activity_Date__c from Account where Id=:accTestObj.id];
   //         system.assertequals(updateAcc.Last_Activity_Owner__c,objUser.id);
    //        system.assertequals(updateAcc.Last_Activity_Date__c,createdDate);
            Test.stoptest();
        }
    }
    static testmethod void myUnitWithEventOpp(){
        setUpData();
        System.runAs(objUser) {
            Test.starttest();
            taskTestObj.WhatID=oppTestObj.id;
            insert taskTestObj;
            Datetime DT=[select id,createdDate from Task Where Id=:taskTestObj.id].createdDate;
            Date createdDate = date.newinstance(dT.year(), dT.month(), dT.day());
            Account updateAcc=[select Id,Last_Activity_Owner__c,Last_Activity_Date__c from Account where Id=:accTestObj.id];
   //         system.assertequals(updateAcc.Last_Activity_Owner__c,objUser.id);
  //          system.assertequals(updateAcc.Last_Activity_Date__c,createdDate);
            Test.stoptest();
        }
    }
    
}