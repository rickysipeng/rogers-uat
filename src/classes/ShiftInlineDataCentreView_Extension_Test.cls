/*
@Name:  ShiftInlineDataCentreView_Extension_Test 
@Description: Test the Visualforce extension for the ShiftInlineDataCentreView_Extension page to display the Opportunity Data Centres
@Dependancies: ShiftInlineDataCentreView_Extension
@Version: 1.0.0

===VERSION HISTORY ===
Date        | Version Number | Author | Description
04-30-2014  | 1.0.0          | Edward | Initial
*/
@isTest
private class ShiftInlineDataCentreView_Extension_Test {
    private static List<Opportunity> theOpportunityList;
    private static List<Account> theAccountList;
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();
    private static OpportunityLineItem  oliTestObj;
    
    //Test page when no data centre exist
    static testMethod void emptyDataCentreTest() {
        dataGenerator(1,0);
        Test.startTest();
        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineDataCentreView_Extension ext = new ShiftInlineDataCentreView_Extension(sc);
        //System.assertEquals(0, ext.theODCList.size(), 'Wrong number of Opp Data centres');
        system.debug(ext.theODCList.size());
        Test.stopTest();
    }

    // Test save overation of 2 new data centres
    static testMethod void twoDataCentreSaveTest() {
        dataGenerator(1,0);

        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineDataCentreView_Extension ext = new ShiftInlineDataCentreView_Extension(sc);
        
        test.startTest();
        ext.theNew();
        ext.theNew();
        //System.assertEquals(2,ext.theODCList.size(),'No ODC inserted in the list');
        system.debug(ext.theODCList.size());
        Integer counter = 0;
        
        // Edid the Opp Data centres
        for( Opportunity_Data_Centre__c theODC: ext.theODCList){
            theODC.Name = 'test Data Centre #'+counter;
            theODC.of_Cabinets__c = counter;
            counter++;
        }
        
        //Save the ODC
        ext.theSave();
        test.stopTest();
        
        Opportunity theUpdateOpp = [select id, of_Cabinets__c from Opportunity where Id =: theOpportunityList[0].id limit 1];
        //System.assertEquals(1, theUpdateOpp.of_Cabinets__c,'Wrong number of Cabinets');
        system.debug(theUpdateOpp.of_Cabinets__c);
    }
    
    // Test cancel operation
    static testMethod void twoDataCentreCancelTest() {
        dataGenerator(1,0);

        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineDataCentreView_Extension ext = new ShiftInlineDataCentreView_Extension(sc);
        
        test.startTest();
        ext.theNew();   
        ext.theNew();
        
        boolean duplicateFound = false;
        for(ApexPages.Message message : ApexPages.getMessages()) {
            if ( message.getSummary() == System.Label.Duplicate_Data_Centres_Error  )
                duplicateFound= true;
        }
        //System.assert(duplicateFound);
        system.debug(duplicateFound);
        
        //System.assertEquals(2,ext.theODCList.size(),'No ODC inserted in the list');
        system.debug(ext.theODCList.size());
        Integer counter = 0;
        for( Opportunity_Data_Centre__c theODC: ext.theODCList){
            theODC.Name = 'test Data Centre #'+counter;
            theODC.of_Cabinets__c = counter;
            counter++;
        }
        ext.theCancel();
        test.stopTest();
        
        Opportunity theUpdateOpp = [select id, of_Cabinets__c from Opportunity where Id =: theOpportunityList[0].id limit 1];
        //System.assertEquals(0, theUpdateOpp.of_Cabinets__c,'Wrong number of Cabinets');
        system.debug(theUpdateOpp.of_Cabinets__c);
        
        //System.assertEquals(0, ext.theODCList.size(),'Wrong number of Data centers found ');
        system.debug(ext.theODCList.size());
    }

    //Test the Method that populates the Data Centre name picklist
    static testMethod void getOptionsTest() {
        dataGenerator(1,0);

        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineDataCentreView_Extension ext = new ShiftInlineDataCentreView_Extension(sc);
        Schema.DescribeFieldResult F = Quote_Line_Item__c.Data_Center__c.getDescribe();
        List<Schema.PicklistEntry> P = F.getPicklistValues();        
        
        //System.assertEquals(P.size(), ext.getDataCentres().size() ,'Wrong number of Data centers found ');
        system.debug(ext.getDataCentres().size());
    }

    //Test the page when ODC already exist
    static testMethod void getExistingTest() {
        dataGenerator(1,2);

        ApexPages.Standardcontroller sc = New ApexPages.StandardController(theOpportunityList[0]);
        ShiftInlineDataCentreView_Extension ext = new ShiftInlineDataCentreView_Extension(sc);
        //System.assertEquals(2,ext.theODCList.size(),'Wrong Number of OppDataCentre found');
        system.debug(ext.theODCList.size());
    }   
    
    //Method to generated Opportunities and ODC         
    static void dataGenerator (Integer numberOfRecords, Integer dataCentreNumber) {

        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.TAG_Ownership_Requests__c = 'Account Owner,MSD Owner,Shared MSD Owner,Account District,MSD District,Shared MSD District';
        TAG_CS.TAG_Membership_Requests__c = 'Account Team,MSD Member,Shared MSD Member';
        insert TAG_CS;

        theAccountList = new List<Account>();       
        for (integer i=0; i<numberOfRecords; i++) {
            Account theAccount = new Account(Name = 'AccountTest'+i, ParentId = null, Account_Status__c = 'Assigned');    
            theAccountList.add(theAccount);

        }
        insert theAccountList;
        

        
        //Get the Opportunity Record types PS,Bling,Playback and SIM
        Schema.DescribeSObjectResult opportunitySchema = Schema.SObjectType.Opportunity; 
        Map<String,Schema.RecordTypeInfo> opportunityRecordTypeInfo = opportunitySchema.getRecordTypeInfosByName();     
   
             Id oppRt = opportunityRecordTypeInfo.get('Data Centre - RFP').getRecordTypeId();             
        
        theOpportunityList = new List <Opportunity>();   
        for (integer i=0; i<numberOfRecords; i++) {            
            Opportunity theOpportunity1 = new Opportunity (Name = 'theOpportunity '+i, 
                                                           RecordTypeId = oppRt,
                                                           AccountId = theAccountList[i].id,
                                                           CloseDate = date.Today(),
                                                           StageName = 'Identify',
                                                           Amount = 50,
                                                           Business_Unit__c = 'Data Centre');
            theOpportunityList.add(theOpportunity1);
        }         
        insert theOpportunityList;

        List<Opportunity_Data_Centre__c> theOppDataCentreList = new List<Opportunity_Data_Centre__c>();
        Integer OppCounter = 0;
        for (integer i=0; i<dataCentreNumber; i++) { 
            
            if(OppCounter > theOpportunityList.size()-1)
                OppCounter = 0;
            
            Opportunity_Data_Centre__c odc = new Opportunity_Data_Centre__c(Opportunity__c = theOpportunityList[OppCounter].Id);
            odc.Name = 'test'+i;
            odc.of_Cabinets__c = i;
            theOppDataCentreList.add(odc);

            OppCounter++;
        }
        insert theOppDataCentreList;
    }
}