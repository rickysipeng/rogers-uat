/*
 * http://code.google.com/p/salesforce-super-clone/source/browse/trunk/+salesforce-super-clone/Google+code+Super+Clone/src/classes/CloneTest.cls
 *
 * r3 by jinghaili on Jul 10, 2011 
 * 
 */
@istest
private class TestClone {
 private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();         
   
    private static Account myAccount;
    
    private static Contact myContact1;
    private static Contact myContact2;

    static testMethod void testDefaultClone() {
    
        testEngine(getCloneDefault());
        
    }
 
    static testMethod void testCaseClone() {
        
        testEngine(getCloneCase());
        
    }

    private static void setup(){
        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

    
    
        //Create account test data
        myAccount = new Account(name='CloneToolTestAccount',ParentId = null, Account_Status__c = 'Assigned');
        
        Insert myAccount;
        //Create contact test data
        myContact1 = new Contact(Lastname = 'Lee', AccountId = myAccount.Id,Contact_Type__c = 'Other',MailingCountry = 'CA',MailingState = 'AB',MailingPostalCode = 'A9A 9A9');
        myContact2 = new Contact(Lastname = 'Lee', AccountId = myAccount.Id,Contact_Type__c = 'Other',MailingCountry = 'CA',MailingState = 'AB',MailingPostalCode = 'A9A 9A9');
        
        //Customize: Add missing steps
        myContact1.Email = 'test@test.com';
        myContact2.Email = 'test@test.com';
        
        Insert myContact1;
        Insert myContact2;    
    }
    
    private static AbstractClone getCloneDefault(){
        return new Clone();
    }

    private static AbstractClone getCloneCase(){
        return new CloneCase();
    }

    
    static void testEngine(AbstractClone cc) {
        if(myAccount==null){
            setup();
        }
    
        string parentObjId=myAccount.Id;
        
        string parentObjAPIName=cc.returnAPIObjectName(parentObjId);
        Map<string,string> objLabelobjAPI=cc.getAllChildObjNames(parentObjAPIName,parentObjId);
        string clondedParentRecordID=cc.startsClone(parentObjId, objLabelobjAPI.values());
        
        system.debug('clondedParentRecordID:'+clondedParentRecordID);
        system.debug('newParentRecordID:'+cc.newParentRecordID);
               
        
        cc = null;
        parentObjId=null;
        parentObjAPIName=null;
        objLabelobjAPI=null;
        clondedParentRecordID=null;
    }
    
 
}