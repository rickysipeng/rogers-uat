/*
===============================================================================
Class Name : SaleQuotaRecalculationSchedular_Test
===============================================================================
PURPOSE: This is a test class for SaleQuotaRecalculationSchedular class

COMMENTS: 

Developer: Deepika Rawat
Date: 9/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
9/11/2013               Deepika               Created
===============================================================================

*/
@isTest(SeeAllData = false)
private class SaleQuotaRecalculationSchedular_Test{
    private static Static_Data_Utilities__c CS= new Static_Data_Utilities__c(); 
    private static Static_Data_Utilities__c CS4= new Static_Data_Utilities__c();   
    private static Static_Data_Utilities__c CS3= new Static_Data_Utilities__c();   
    private static BatchProcess_Admin__c CS1= new BatchProcess_Admin__c();   
    private static Static_Data_Utilities__c CS2 = new Static_Data_Utilities__c(); 
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
 static testmethod void testMethod1(){
        Sales_Quota__c quota = new Sales_Quota__c();
        quota.Forecast_Month__c =201310;
        quota.Ownerid =listUser[1].id;
        quota.Quantity__c= 2;
        quota.Quota_Family__c= 'ABS - Software';
        quota.Revenue__c = 100.00;
        
        insert quota; 
 
        CS.Name = 'Quota Update Checking Period';
        CS.Value__c = '1'; 
        insert CS;
        CS3.Name = 'Recalculation Period';
        CS3.Value__c= '120';
        insert CS3;
         
        
        CS2.Name = 'Opportunity Update Checking Period';
        CS2.Value__c = '1'; 
        insert CS2;
        
        CS1.Name = 'SysAdm1';
        CS1.Email__c = 'sfdc.support@rci.rogers.com'; 
        insert CS1;
        
        CS4.Name = 'BatchFire';
        CS4.Value__c = 'true'; 
        insert CS4;
        
        
                
        Test.StartTest();
        SaleQuotaRecalculationSchedular obj= new SaleQuotaRecalculationSchedular();
        String sch = '0 0 23 * * ?';
        system.schedule('TestForecastDeletion1', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}