/*
===============================================================================
 Class Name   : uploadMSDSnapshotbatch
===============================================================================
PURPOSE:    This batch class deltets all the MSD__share records with 
            rowcause = AccountTeamMember__c and inserts new MSD_Code share records
            for related Account's Owner and AccountTeamMembers
  

Developer: Deepika Rawat
Date: 09/03/2013

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
09/03/2013          Deepika                     Created
===============================================================================
*/
global class uploadMSDSnapshotbatch implements Database.Batchable<SObject>{
    global String query;
    global Database.SaveResult[] listSaveResult;
    static BatchProcess_Admin__c admin = [Select email__c from BatchProcess_Admin__c limit 1];
    List<SalesForecast_Errors__c> errorList = new List<SalesForecast_Errors__c>();

     // End:Declarations----------------------------------------------------------------------------------------

    global Database.QueryLocator start(Database.BatchableContext BC){
        if(!Test.isRunningTest()){
            List<SalesForecast_Errors__c> errorRecordlist = [Select id , Error_Message__c, Job_Name__c, Status_Code__c, Record_Owner__c, MSD_Code__c from SalesForecast_Errors__c limit 10000];
            if(errorRecordlist!= null && errorRecordlist.size()>0){
            delete errorRecordlist;
            }
            List<SalesForecast_Errors__c> errorRecordlist1 = [Select id , Error_Message__c, Job_Name__c, Status_Code__c, Record_Owner__c, MSD_Code__c from SalesForecast_Errors__c limit 10000];
            if(errorRecordlist1 != null && errorRecordlist1.size()>0){
            delete errorRecordlist1;
            }
        return Database.getQueryLocator(query);
        }
        else{
           return Database.getQueryLocator('select id , Name, Account__c, Churn_MTD__c, Churn_QTD__c,Account__r.Name, Churn_YTD__c, Data_Revenue__c, MSD_Active_Data_Only_Lines__c, MSD_Active_Voice_Data_Lines__c, MSD_Active_Voice_Only_Lines__c, MSD_ARPU__c, MSD_Code_Status__c, MSD_Type__c, Voice_Revenue__c from MSD_Code__c limit 10');
        }             
    }
    
    global void execute(Database.BatchableContext BC, List<MSD_Code__c > scope) {
        List<MSD__c> listMSDSnapshot = new List<MSD__c>();
        system.debug('**scope*****'+scope.size());
        for(MSD_Code__c msdCode:scope){
            MSD__c mObj = new MSD__c();
            mObj.AccountId__c = msdCode.Account__c;
            mObj.Account__c = msdCode.Account__r.Name;
            mObj.Churn_MTD__c = msdCode.Churn_MTD__c;
            mObj.Churn_QTD__c = msdCode.Churn_QTD__c;
            mObj.Churn_YTD__c = msdCode.Churn_YTD__c;
            mObj.MSD_Active_Data_Only_Lines__c = msdCode.MSD_Active_Data_Only_Lines__c;
            mObj.MSD_Active_Voice_Data_Lines__c = msdCode.MSD_Active_Voice_Data_Lines__c;
            mObj.MSD_Active_Voice_Only_Lines__c = msdCode.MSD_Active_Voice_Only_Lines__c;
            mObj.MSD_ARPU__c = msdCode.MSD_ARPU__c;
            mObj.MSD_Code_Status__c = msdCode.MSD_Code_Status__c;
            mObj.MSD_Type__c = msdCode.MSD_Type__c;
            mObj.Voice_Revenue__c = msdCode.Voice_Revenue__c;
            mObj.Data_Revenue__c = msdCode.Data_Revenue__c;
            mObj.MSD_Code__c = msdCode.Name;
            listMSDSnapshot.add(mObj);
        }

        If(listMSDSnapshot.size()>0){
            listSaveResult = Database.insert(listMSDSnapshot, false);
             for(integer i=0; i<listMSDSnapshot.size(); i++){
                 if(!(listSaveResult[i].isSuccess())){
                    List<Database.Error> err = listSaveResult[i].getErrors();
                    if(!(err[0].getStatusCode() == StatusCode.FIELD_FILTER_VALIDATION_EXCEPTION &&  err[0].getMessage().contains('AccessLevel'))){
                        SalesForecast_Errors__c rec = new SalesForecast_Errors__c();
                        rec.Record_Owner__c = listMSDSnapshot[i].OwnerId;
                        rec.Job_Name__c = 'Create MSD Snapshot from MSD Code' ;
                        rec.Error_Message__c = err[0].getMessage();
                        rec.MSD_Code__c = listMSDSnapshot[i].id;
                        rec.Status_Code__c = String.Valueof(err[0].getStatusCode());
                        errorList.add(rec);                
                    }
                 }
             }
             
             if(errorList.size()>0){
             insert errorList;
             }
        }
    }
    global void finish(Database.BatchableContext BC){
         List<SalesForecast_Errors__c> errorRecordlist;
        if(!Test.isRunningTest()){
             errorRecordlist = [Select id , Error_Message__c,Sales_Measurement_Info__c, MSD_Code__c, Job_Name__c, Status_Code__c, Record_Owner__c from SalesForecast_Errors__c where createddate =:system.today() limit 10000];
        }
        else{
           errorRecordlist = [Select id , Error_Message__c,Sales_Measurement_Info__c, MSD_Code__c, Job_Name__c, Status_Code__c, Record_Owner__c from SalesForecast_Errors__c limit 10];
        }       
        string header = 'Job Name,  MSD Code, MSD Code Owner, Error Message, Status Code \n';
        string finalstr = header ;
        for(SalesForecast_Errors__c  erObj: errorRecordlist ){
               string recordString = '"'+erObj.Job_Name__c+'","'+erObj.MSD_Code__c+'","'+'","'+erObj.Record_Owner__c+'","'+erObj.Error_Message__c+'","'+erObj.Status_Code__c+'"\n';
               finalstr = finalstr +recordString;
        }
        
        Messaging.EmailFileAttachment csvAttc = new Messaging.EmailFileAttachment();
        blob csvBlob = Blob.valueOf(finalstr);
        string csvname= 'MSDSnapshot.csv';
        csvAttc.setFileName(csvname);
        csvAttc.setBody(csvBlob);
        Messaging.SingleEmailMessage email =new Messaging.SingleEmailMessage();
        String[] toAddresses = new String[] {admin.email__c}; 
        String body = 'Hi,<br/><br/>Upload MSD Snapshot Batch Job has been completed. <br/><br/>'; 
        body += 'Please find the attached csv for error records that failed to get inserted in the database.';
        body += '<br/> <br/> Regards, <br/> Salesforce Team'; 
        email.setToAddresses(toAddresses); 
        email.setSubject('Upload MSD Snapshot Job Status : Completed');        
        email.setSaveAsActivity(false);  
        email.setHtmlBody(body); 
        email.setFileAttachments(new Messaging.EmailFileAttachment[]{csvAttc});
        Messaging.SendEmailResult [] r = Messaging.sendEmail(new Messaging.SingleEmailMessage[] {email});
    }
}