/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest
private class TestCIFSiteTrigger {

    static testMethod void myUnitTest() {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta)
        {
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto)
        {
            mapRTo.put(rto.Name,rto.id);  
        }
        
        Account a = new Account();
        a.name = 'Test Act';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned'; 
        insert a;
        
        
        Opportunity o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        
        User user1 = [SELECT Id, Name From User LIMIT 1];
        
        CIF__c cif = new CIF__c();
        cif.Opportunity__c = user1.Id; 
        cif.Opportunity_Name__c = o.Id;
        insert cif;
        
        Contact ContactCif = new Contact();
        ContactCif.Phone='9898989898';
        ContactCif.Contact_Type__c = 'Other'; 
        ContactCif.LastName = 'New Contact'; 
        ContactCif.MobilePhone ='9898989890';
        ContactCif.Title ='BTA';
        ContactCif.Email = 'aaa@aa.com';
        ContactCif.MailingCity = 'Toronto';
        ContactCif.MailingCountry ='CA';
        ContactCif.MailingState='AB';
        ContactCif.MailingPostalCode='A9A 9A9';
        insert ContactCif;
        
        CIF__c cif1 = new CIF__c();
        cif1.Opportunity__c = user1.Id; 
        cif1.Opportunity_Name__c = o.Id; 
        cif1.eBilling_Admin_Contact__c = contactCif.id;
        insert cif1; 
        
        Id cif1Id = cif1.id;
        CIF_Site__c cSiteNew = new CIF_Site__c(CIF_Number__c = cif1Id);
        insert cSiteNew;
        
        Id cifId = cif.id;
        CIF_Site__c cSite = new CIF_Site__c(CIF_Number__c = cifId);
        insert cSite;
        Test.startTest();
        CIF_Site__c cSite1 = [SELECT Id, Name, New_Schedule_A__c, Delete_Schedule_A__c, Schedule_A_List__c FROM CIF_Site__c where Id = :cSite.Id LIMIT 1];
        
        cSite1.New_Schedule_A__c = 'test1, test, test3';
        update cSite1;
        
        cSite1 = [SELECT Id, Name, New_Schedule_A__c, Delete_Schedule_A__c, Schedule_A_List__c FROM CIF_Site__c where Id = :cSite.Id LIMIT 1];
    //    System.assertEquals(Utils.parseList(cSite1.Schedule_A_List__c).size(), 3);
        
        cSite1.New_Schedule_A__c = 'test4';
        update cSite1;
        
        cSite1 = [SELECT Id, Name, New_Schedule_A__c, Delete_Schedule_A__c, Schedule_A_List__c FROM CIF_Site__c where Id = :cSite.Id LIMIT 1];
        //System.assertEquals(Utils.parseList(cSite1.Schedule_A_List__c).size(), 4);
        
        cSite1.New_Schedule_A__c = 'test4';
        cSite1.Delete_Schedule_A__c = '1,3';
        update cSite1;
        
        cSite1 = [SELECT Id, Name, New_Schedule_A__c, Delete_Schedule_A__c, Schedule_A_List__c FROM CIF_Site__c where Id = :cSite.Id LIMIT 1];
        //System.assertEquals(Utils.parseList(cSite1.Schedule_A_List__c).size(), 3);
        
        cSiteNew = [SELECT Id, Name, New_Schedule_A__c, Delete_Schedule_A__c, Schedule_A_List__c FROM CIF_Site__c where Id = :cSite.Id LIMIT 1];
        
        Test.stopTest();

    }
}