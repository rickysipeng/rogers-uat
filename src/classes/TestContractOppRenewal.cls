/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
@isTest(SeeAllData = true)
private class TestContractOppRenewal {


    static Id oppId, oppRenewalId, cId, cRenewalId;
    static Account account1;
    static Opportunity o, oRenewal;
    static Wireline_Contract__c c, cRenewal;
    static Map <string,Id> mapRTo, mapRTc;
     private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c(); 

    static { 
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtc = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Wireline_Contract__c']);
        mapRTc = new Map <string,id> ();
        
        for (RecordType rtc : lRtc){
            mapRTc.put(rtc.Name,rtc.id);  
        }
        
    /*    TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        TAG_CS.Wireline_Contract_URL__c='/a0h/e';
        insert TAG_CS;
     */   
        account1 = new Account(); 
        account1.name = 'Test Act Two';
        account1.Business_Segment__c = 'RBS GTA Region';
        account1.RecordTypeId = mapRTa.get('New Account');
        account1.Phone = '416-555-1212';
        account1.Website = 'www.abc123.com';
        account1.BillingStreet = 'Street1';
        account1.BillingCity = 'MyCity';
        account1.BillingCountry = 'Canada';
        account1.BillingPostalCode = 'L2L2L2';
        account1.BillingState = 'ON';
        insert account1;
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Existing_MRR__c=500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = account1.id;
        o.RecordTypeId = mapRTo.get('Wireline - Enterprise Contract Renewal');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        oRenewal = new Opportunity();
        oRenewal.Estimated_MRR__c = 500;
        oRenewal.Existing_MRR__c=500;
        oRenewal.Name = 'Test Opp';
        oRenewal.StageName = 'Suspect - Qualified';
        oRenewal.Product_Category__c = 'Local';
        oRenewal.Network__c = 'Cable';
        oRenewal.Estimated_One_Time_Charge__c = 500;
        oRenewal.New_Term_Months__c = 5;
        oRenewal.AccountId = account1.id;
        oRenewal.RecordTypeId = mapRTo.get('Wireline - Enterprise Contract Renewal');
        oRenewal.CloseDate = date.today();
        insert oRenewal;
        oppRenewalId = oRenewal.id;
        
        c = new Wireline_Contract__c();
        c.Account_Name__c = account1.Id;
        c.Name = 'Wireline Contract Renewal Test';
        c.Status__c = 'Draft';
        c.Contract_Start_Date__c = date.today();
        c.Contract_Term_months__c = 24;
        c.RecordTypeId = mapRTc.get('Enterprise Contract Layout');
        insert c;
        cId = c.Id;
        
        cRenewal = new Wireline_Contract__c();
        cRenewal.Account_Name__c = account1.Id;
        cRenewal.Name = 'Wireline Contract Renewal Test';
        cRenewal.Status__c = 'Draft';
        cRenewal.Contract_Start_Date__c = date.today();
        cRenewal.Contract_Term_months__c = 24;
        cRenewal.RecordTypeId  = mapRTc.get('Enterprise Contract Renewal');
        insert cRenewal;
        cRenewalId = cRenewal.Id;
        
        Selected_Contract__c sc1 = new Selected_Contract__c();
        sc1.Wireline_Contract__c = cRenewalId;
        sc1.Parent_Wireline_Contract__c = cId;
        INSERT sc1;
        
        Selected_Contract_Opp__c sc2 = new Selected_Contract_Opp__c();
        sc2.Wireline_Contract__c = cRenewalId;
        sc2.Parent_Opportunity__c = oppId;
        INSERT sc2;
       
    }

    static testMethod void oppEditRedirectTest() {
       Test.startTest();
        PageReference pageRef = New PageReference('/apex/OpportunityEditRedirect?Id=' + String.valueOf(oppId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(o);
        OpportunityEditRedirect controller = new OpportunityEditRedirect(sc);
        controller.redirect();
       Test.stopTest();
    }
    
    static testMethod void oppEditRedirectTest2() {
    Test.startTest();
        PageReference pageRef = New PageReference('/apex/OpportunityEditRedirect?Id=' + String.valueOf(oppRenewalId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(oRenewal);
        OpportunityEditRedirect controller = new OpportunityEditRedirect(sc);
        controller.redirect();
     Test.stopTest();
    }
    static testMethod void oppEditRedirectTest3() {
    Test.startTest();
        PageReference pageRef = New PageReference('/apex/OpportunityEditRedirect?Id=' + String.valueOf(oppRenewalId) + '&RecordType=Wireline - Enterprise Contract Renewal'+'&accid='+account1+'&def_Account_id='+account1); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(oRenewal);
        OpportunityEditRedirect controller = new OpportunityEditRedirect(sc);
        controller.redirect();
     Test.stopTest();
    }
    
    static testMethod void contractEditRedirectTest() {
    Test.startTest();
        PageReference pageRef = New PageReference('/apex/ContractEditRedirect?Id=' + String.valueOf(cId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        ContractEditRedirect controller = new ContractEditRedirect(sc);
        controller.redirect();
    Test.stopTest();
    }
    
    static testMethod void contractEditRedirectTest2() {
       Test.startTest();
        PageReference pageRef = New PageReference('/apex/ContractEditRedirect?Id=' + String.valueOf(cRenewalId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(cRenewal);
        ContractEditRedirect controller = new ContractEditRedirect(sc);
        controller.redirect();
        ContractOppRenewalExtension sc1 = new ContractOppRenewalExtension(sc);
        sc1.getRelatedContracts();
  //      sc1.resetAccount();
       Test.stopTest();
    }
    
    static testMethod void oppRedirectTest1() {
       Test.startTest();
       PageReference pageRef = New PageReference('/apex/opportunityRedirect'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Opportunity());
        OpportunityRedirect controller = new OpportunityRedirect(sc);
        controller.redirect();
        Test.stopTest();
    }
    
    static testMethod void oppRedirectTest2() {
       Test.startTest();
       PageReference pageRef = New PageReference('/apex/opportunityRedirect?RecordType='+mapRTo.get('Enterprise Contract Renewal')); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Opportunity());
        OpportunityRedirect controller = new OpportunityRedirect(sc);
        controller.redirect();
       Test.stopTest();
    }
    
    static testMethod void conRedirectTest1() {
      Test.startTest();
       PageReference pageRef = New PageReference('/apex/ContractRedirect'); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Contract());
        ContractRedirect controller = new ContractRedirect(sc);
        controller.redirect();
       Test.stopTest();
    }
    
    static testMethod void conRedirectTest2() {
      Test.startTest();
       PageReference pageRef = New PageReference('/apex/ContractRedirect?RecordType=' + mapRTc.get('Enterprise Contract Layout')); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(new Contract());
        ContractRedirect controller = new ContractRedirect(sc);
        controller.redirect();
      Test.stopTest();
    }
    
    static testMethod void oppRenewalTestA() {
    Test.startTest();
       PageReference pageRef = New PageReference('/apex/ContractOppRenewalList?Id=' + String.valueOf(oppRenewalId) +'&RecordType='+mapRTo.get('Enterprise Contract Renewal')+'&accId=' + String.valueOf(account1.Id)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(oRenewal);
        ContractOppRenewalExtension controller = new ContractOppRenewalExtension(sc);
        controller.updateRelatedContracts();
        controller.updateContracts();
    Test.stopTest();
        
        
    }
    
    static testMethod void conRenewalTestA() {
    Test.startTest();
       PageReference pageRef = New PageReference('/apex/ContractRenewalList?Id=' + String.valueOf(cRenewalId) +'&RecordType='+mapRTc.get('Enterprise Contract Renewal')+'&accId=' + String.valueOf(account1.Id)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(cRenewal);
        ContractRenewalExtension controller = new ContractRenewalExtension(sc);
        controller.updateRelatedContracts();
        controller.updateContracts();
        controller.getStatus();
     Test.stopTest();
    } 
    
    static testMethod void conRenewalTestB() { 
      /*
       c.Activated_Date__c = date.today();
        c.Status__c = 'Activated';
        UPDATE c;
     */
     Test.startTest();
       PageReference pageRef = New PageReference('/apex/ContractRenewalList?Id=' + String.valueOf(c) +'&RecordType='+mapRTc.get('Enterprise Contract Renewal')+'&accId=' + String.valueOf(account1.Id)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        ContractRenewalExtension controller = new ContractRenewalExtension(sc);
        controller.updateRelatedContracts();
        controller.updateContracts();

        controller.getStatus();
        Test.stopTest();
    } 
    
    static testMethod void conRenewalTestC() { 
      /*
       c.Activated_Date__c = date.today();
        c.Status__c = 'In Approval Process';
        UPDATE c;
        */
        
       PageReference pageRef = New PageReference('/apex/ContractRenewalList?Id=' + String.valueOf(c) +'&RecordType='+mapRTc.get('Enterprise Contract Renewal')+'&accId=' + String.valueOf(account1.Id)); 
        Test.startTest();
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(c);
        ContractRenewalExtension controller = new ContractRenewalExtension(sc);
        controller.getRelatedContracts();       
        controller.updateRelatedContracts();
        controller.updateContracts();
        controller.getStatus();
        controller.resetAccount();
        Test.stopTest();
    }
}