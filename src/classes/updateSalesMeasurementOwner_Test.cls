/*
===============================================================================
Class Name : updateSalesMeasurementOwner_Test
===============================================================================
PURPOSE: This is a test class for updateSalesMeasurementOwner Trigger

COMMENTS: 

Developer: Deepika Rawat
Date: 1/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
1/11/2013               Deepika               Created

March 2015              Aakanksha Patel        Modified(For TAG)
===============================================================================
*/
@isTest (SeeAllData = true)
private class updateSalesMeasurementOwner_Test{
    private static Account accTestObj;
    private static Opportunity oppTestObj;
    private static Opportunity oppTestObj1;
    private static Sales_Measurement__c smTest;
    private static Sales_Measurement__c smTest1;
    
    private static List<User> listUser =[Select id from User where isActive= True and profile.name='System Administrator'  limit 2];
     static testmethod void testMethod1(){
       
        accTestObj = new Account();
        accTestObj.Name='TestAcc';
        accTestObj.OwnerId=listUser[0].id;
        accTestObj.Account_Status__c='Assigned';
        accTestObj.BillingCountry= 'CA';
        accTestObj.BillingPostalCode = 'A9A 9A9'; //41 014
        accTestObj.BillingState = 'MA';
        accTestObj.BillingCity='City';
        accTestObj.BillingStreet='Street';
        accTestObj.ParentId= null;
        insert accTestObj;    
       
   /*   Product2 p2 = new Product2(Name='pro01',ProductCode='Fulfillment');
      insert p2;
      
       Pricebook2 stdpb = [SELECT Id FROM Pricebook2 WHERE IsStandard = true];
       PricebookEntry pbe = new PricebookEntry(Pricebook2Id = stdpb.Id, Product2Id = p2.Id, UnitPrice = 1);
        insert pbe;
     //   Pricebook2 pb = new Pricebook2(Name = 'Standard Price Book 2009', Description = 'Price Book 2009 Products', IsActive = true);
     //   insert pb;
        
     //   PricebookEntry pbe = new PricebookEntry(Pricebook2Id = pb.Id, Product2Id = prod.Id, UnitPrice = 10000, IsActive = true, UseStandardPrice = false);
      //  insert pbe;
       
     */  
       oppTestObj = new Opportunity(Name='testAcc');
       oppTestObj.StageName='Identify';
       oppTestObj.CloseDate=system.today();
       oppTestObj.AccountID=accTestObj.id;
       oppTestObj.ownerid =listUser[0].id;
    //   oppTestObj.Pricebook2ID = stdpb.id;
       insert oppTestObj; 
       
   /*   OpportunityLineItem oli = new OpportunityLineItem(opportunityId = oppTestObj.Id, pricebookentryId = pbe.Id, Quantity = 1, UnitPrice = 7500, Description = '2007 CMR #4 - Anti-Infectives');
        insert oli;
     
      oppTestObj.StageName='Closed Won';
      update oppTestObj;
     */  
       
    /*   oppTestObj1 = new Opportunity(Name='testAcc1');
       oppTestObj1.StageName='Identify';
       oppTestObj1.CloseDate=system.today();
       oppTestObj1.AccountID=accTestObj.id;
       oppTestObj1.ownerid =listUser[0].id;
       insert oppTestObj1;   
      */ 
       smTest= new Sales_Measurement__c ();
       smTest.Opportunity__c = oppTestObj.id;
       insert smTest;     
      /* smTest1= new Sales_Measurement__c ();
       smTest1.Opportunity__c = oppTestObj1.id;
       insert smTest1; 
       */
       test.startTest();
       oppTestObj.ownerid =listUser[1].id;
       update oppTestObj;
   //    update oppTestObj1;
       test.stopTest();
     
       System.Assert(oppTestObj.ownerid ==listUser[1].id);
     }
}