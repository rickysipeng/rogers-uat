/*
===============================================================================
 Class Name   : TAG_BulkRequestProcessSchedular
===============================================================================
PURPOSE:    This schedular class runs the TAG_BulkRequestProcessBatch class 
            with a batch of 150 records. 

Developer: Deepika Rawat
Date: 01/25/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
01/25/2015          Deepika                     Original Version
===============================================================================
*/
global class TAG_BulkRequestProcessSchedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        TAG_BulkRequestProcessBatch sc = new TAG_BulkRequestProcessBatch();
        ID batchprocessid = Database.executeBatch(sc,150);
        system.debug('sc*************' +sc);
    }

}