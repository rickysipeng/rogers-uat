public with sharing class BillingAccountOnOPP_Cotroller {
    public List<Billing_Account__c> lstBillingAccount = new List<Billing_Account__c>();
    public List<billingAccWrapper > lstBAWrapper{get;set;}
    public Opportunity oppRec;
    public Boolean shouldRedirect{get;set;}
    public String url{get;set;}
    public String oppid;
    
    public BillingAccountOnOPP_Cotroller(){
        String accid = ApexPages.currentPage().getParameters().get('accid');
        oppid = ApexPages.currentPage().getParameters().get('oppid');
        if(accid !=null && accid !='')
            lstBillingAccount = [select Name from Billing_Account__c where Account__c =:accid ];
        oppRec = [Select id, Billing_Account__c from Opportunity where id=:oppid];
        lstBAWrapper= new List<billingAccWrapper >();
        if(lstBillingAccount!=null && lstBillingAccount.size()>0){
            for(Billing_Account__c ba :lstBillingAccount ){
                billingAccWrapper baWrapper = new billingAccWrapper();
                baWrapper.baRec = ba;
                baWrapper.isSelected = false; 
                lstBAWrapper.add(baWrapper);
            }
        }
    }
    
    
    public class billingAccWrapper{
        public Billing_Account__c baRec{get;set;}
        public boolean isSelected {get;set;}        
    }
    
    public pagereference selectBA(){
        if (Config__c.getInstance('Config') == null) return null;  
            string theOpportunityFieldId = Config__c.getInstance('Config').Opportunity_Field_Id__c;   
        url = '/apex/shift_auto_create_quote?CF' + theOpportunityFieldId + '_lkid='+oppid;
        for(billingAccWrapper wrap :lstBAWrapper){
            if(wrap.isSelected == true){
                oppRec.Billing_Account__c = wrap.baRec.id;
            }
        }
        update oppRec;
        shouldRedirect=true;
        return null;
        
    }
    
}