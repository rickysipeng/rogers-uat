/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/

public class shift_share_spr_note_support {

    /* 
    A future annotation is required so Salesforce can calculate the sharing rules for ew records.
    This class does not cater for bulk trigger.
    Developed by Jocsan Diaz on November 8th, 2012
    */

    static SPR_Note__c theSPRNote;
    static list<SPR_Note__Share> theNewSPRNoteShareList = new list<SPR_Note__Share>();
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c();  

    @future
    static public void theShareSPRNote(Id theId) {
        // get SPR Note from Id
        theSPRNote = [select Id, Recipient_1__c, Request_ACK__c, Private__c, Recipient_1_Shared__c from SPR_Note__c where Id =: theId];

        // share records when there is recipient and ACK is requested
        if (theSPRNote.Recipient_1__c != null && theSPRNote.Request_ACK__c == true) {
            // get user access for the record
            list<UserRecordAccess> theUserRecordAccessList = new list<UserRecordAccess>([select RecordId , HasReadAccess, MaxAccessLevel, HasEditAccess  
                                                                                        from UserRecordAccess 
                                                                                            where RecordId =: theSPRNote.Id and UserId =: theSPRNote.Recipient_1__c]);
            system.debug(theUserRecordAccessList);
                                                                                            
            // if spr note is private, check whether the user has access to the record. If it does, give Edit access and flag the spr note record
            if (theSPRNote.Private__c == true) {
                // check based on user
                if (!theUserRecordAccessList.isEmpty()) {
                    if (theUserRecordAccessList.get(0).HasReadAccess) {
                        theCreateShare();
                    }
                } 
            } else { // if spr note is not private then share it
                 theCreateShare();
            }

            // insert share records
            try {
                insert theNewSPRNoteShareList;
            } catch (Exception e) {
                system.debug(e);
            } finally {
                // update srp note
                try {
                    update theSPRNote;
                } catch (Exception e) {
                    system.debug(e);
                }               
            }                                                                       
        }
    }
    
    static private void theCreateShare() {
        // create new share record
        SPR_Note__Share theSRPNoteShare = new SPR_Note__Share();
        theSRPNoteShare.ParentId = theSPRNote.Id;
        theSRPNoteShare.AccessLevel = 'Edit';
        theSRPNoteShare.UserOrGroupId = theSPRNote.Recipient_1__c;
        
        theNewSPRNoteShareList.add(theSRPNoteShare);    
        
        // flag the record to send notifications. do this for before trigger.
        theSPRNote.Recipient_1_Shared__c = !theSPRNote.Recipient_1_Shared__c;    
    }
    
    static testMethod void testshift_share_spr_note_with_recipient1() {

        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;

        // create user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest1@test.com');
        insert u;

        // create recipient 1
        User r1 = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest2@test.com');
        insert r1;

        // create recipient 2
        User r2 = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest3@test.com');
        insert r2;

        // create dummy account
        Account a = new Account(Name='shift test account', ParentId = Null, Account_Status__c = 'Assigned', BillingPostalCode = 'A1A 1A1');
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='shift test opportunity', AccountId=a.Id, CloseDate=Date.today(), Amount = 500, StageName = 'Dormant');
        insert o;
        
        // create test quote
        Quote__c q = new Quote__c(Opportunity_Name__c=o.id);
        insert q;
        
        // create SPR
        SPR__c s = new SPR__c(Account__c = a.Id, Opportunity__c = o.Id, Requested_By__c = u.Id, Global_Recipient_1__c = u.Id, Global_Recipient_2__c = u.Id, Global_Recipient_3__c = u.Id, 
                                Global_Recipient_4__c = u.Id, Global_Recipient_5__c = u.Id);
        insert s;
        
        Test.StartTest();
        // create SPR notes       
        SPR_Note__c sn1 = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123', Recipient_1__c = r1.Id, Request_ACK__c = true);
        insert sn1;  
        Test.StopTest(); 


        // verify the SPR note record is shared with the recipient 1
        list<SPR_Note__Share> theSPRNoteShareList = new list<SPR_Note__Share>([select s.UserOrGroupId, s.ParentId, s.AccessLevel  
                                                                                    from SPR_Note__Share s
                                                                                        where s.UserOrGroupId =: r1.Id and s.ParentId =: sn1.Id]);
     //   system.assertEquals(theSPRNoteShareList.size(), 1);
        //system.assertEquals(theSPRNoteShareList.get(0).AccessLevel, 'Edit');                
    }
    
    static testMethod void testshift_share_spr_note_with_recipient2() {

        TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
        
        // create user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest1@test.com');
        insert u;

        // create recipient 1
        User r1 = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest2@test.com');
        insert r1;

        // create recipient 2
        User r2 = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest3@test.com');
        insert r2;

        // create dummy account
        Account a = new Account(Name='shift test account', ParentId = Null, Account_Status__c = 'Assigned', BillingPostalCode = 'A1A 1A1');
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='shift test opportunity', AccountId=a.Id, CloseDate=Date.today(), Amount = 500, StageName = 'Dormant');
        insert o;
        
        // create test quote
        Quote__c q = new Quote__c(Opportunity_Name__c=o.id);
        insert q;
        
        // create SPR
        SPR__c s = new SPR__c(Account__c = a.Id, Opportunity__c = o.Id, Requested_By__c = u.Id, Global_Recipient_1__c = u.Id, Global_Recipient_2__c = u.Id, Global_Recipient_3__c = u.Id, 
                                Global_Recipient_4__c = u.Id, Global_Recipient_5__c = u.Id);
        insert s;
        
        Test.StartTest();
        // create SPR notes       
        SPR_Note__c sn1 = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123', Recipient_1__c = r1.Id, Request_ACK__c = true);
        insert sn1;  
        
       
        // update note with new recipient
        sn1.Recipient_1__c = r2.Id;                                                                             
        update sn1;
        Test.StopTest(); 
        
        // verify the SPR note record is shared with the recipient 2
        list<SPR_Note__Share> theSPRNoteShareList = new list<SPR_Note__Share>([select s.UserOrGroupId, s.ParentId, s.AccessLevel  
                                                                                    from SPR_Note__Share s
                                                                                        where s.UserOrGroupId =: r2.Id and s.ParentId =: sn1.Id]);
     //   system.assertEquals(theSPRNoteShareList.size(), 1);
        //system.assertEquals(theSPRNoteShareList.get(0).AccessLevel, 'Edit');    


/*        
        // update note back to recipient 1. record was already shared.
        sn1.Recipient_1__c = r1.Id;     
        sn1.Private__c = true;                                                                      
        update sn1; 

        // create another spr note to test bulk the trigger
        SPR_Note__c sn2 = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123', Recipient_1__c = r1.Id, Request_ACK__c = true);
        insert sn2;
        
        // verify the SPR note record is shared with the recipient 2
        theSPRNoteShareList = new list<SPR_Note__Share>([select s.UserOrGroupId, s.ParentId, s.AccessLevel  
                                                                                    from SPR_Note__Share s
                                                                                        where s.UserOrGroupId =: r1.Id]);
        system.assertEquals(theSPRNoteShareList.size(), 2);
        system.assertEquals(theSPRNoteShareList.get(0).AccessLevel, 'Edit');        
        system.assertEquals(theSPRNoteShareList.get(1).AccessLevel, 'Edit');    
        
        // create another spr note to test exception with private note
        SPR_Note__c sn3 = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123', Recipient_1__c = r1.Id, Request_ACK__c = true, Private__c = true);
        insert sn3; 
        
        // verify the SPR note record is not shared with the recipient 1
        theSPRNoteShareList = new list<SPR_Note__Share>([select s.UserOrGroupId, s.ParentId, s.AccessLevel  
                                                                                    from SPR_Note__Share s
                                                                                        where s.UserOrGroupId =: r1.Id and s.ParentId =: sn3.Id]);
        system.assertEquals(theSPRNoteShareList.size(), 0);    */
                
    }  
    
    static testMethod void testshift_share_spr_note_with_recipient3() {

       TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
       
        // create user
        Profile p = [select id from profile where name='Standard User'];
        User u = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest1@test.com');
        insert u;

        // create recipient 1
        User r1 = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest2@test.com');
        insert r1;

        // create recipient 2
        User r2 = new User(alias = 'standt', email='standarduser@testorg.com',
          emailencodingkey='UTF-8', lastname='Testing', languagelocalekey='en_US',
          localesidkey='en_US', profileid = p.Id,
          timezonesidkey='America/Los_Angeles', username='shifttest3@test.com');
        insert r2;

        // create dummy account
        Account a = new Account(Name='shift test account', ParentId = Null, Account_Status__c = 'Assigned', BillingPostalCode = 'A1A 1A1');
        insert a;
        
        // create dummy opp
        Opportunity o = new Opportunity(Name='shift test opportunity', AccountId=a.Id, CloseDate=Date.today(), Amount = 500, StageName = 'Dormant');
        insert o;
        
        // create test quote
        Quote__c q = new Quote__c(Opportunity_Name__c=o.id);
        insert q;
        
        // create SPR
        SPR__c s = new SPR__c(Account__c = a.Id, Opportunity__c = o.Id, Requested_By__c = u.Id, Global_Recipient_1__c = u.Id, Global_Recipient_2__c = u.Id, Global_Recipient_3__c = u.Id, 
                                Global_Recipient_4__c = u.Id, Global_Recipient_5__c = u.Id);
        insert s;
        
        Test.StartTest();
        // create SPR notes       
        SPR_Note__c sn1 = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123', Recipient_1__c = r1.Id, Request_ACK__c = true);
        insert sn1;  
        
       
        // update note with new recipient
        sn1.Recipient_1__c = r2.Id;                                                                             
        update sn1;

        // update note back to recipient 1. record was already shared.
        sn1.Recipient_1__c = r1.Id;     
        sn1.Private__c = true;                                                                      
        update sn1; 

        // create another spr note to test bulk the trigger
        SPR_Note__c sn2 = new SPR_Note__c(SPR__c = s.Id, Body__c = 'shift 123', Recipient_1__c = r1.Id, Request_ACK__c = true);
        insert sn2;
        Test.StopTest(); 
        
        // verify the SPR note record is shared with the recipient 2
        list<SPR_Note__Share> theSPRNoteShareList = new list<SPR_Note__Share>([select s.UserOrGroupId, s.ParentId, s.AccessLevel  
                                                                                    from SPR_Note__Share s
                                                                                        where s.UserOrGroupId =: r1.Id]);
   //     system.assertEquals(theSPRNoteShareList.size(), 1);
        //system.assertEquals(theSPRNoteShareList.get(0).AccessLevel, 'Edit'); 
        
        sn1.Request_ACK__c = false;
        update sn1;
        
        sn1.Request_ACK__c = true;
        update sn1;              
    }         
}