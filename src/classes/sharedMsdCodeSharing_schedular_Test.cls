/*
This is the test class for SharedMsdCodeSharing_schedular.
            
Written by: Jacob Klay, Rogers Communications
Date: 10-Mar-2014 09:00 EST

***Revisions***
Date        |Name               |Description

*/
@isTest(SeeAllData = true)
private class sharedMsdCodeSharing_schedular_Test{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        sharedMsdCodeSharing_schedular obj= new sharedMsdCodeSharing_schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('SharedMsdSharing', sch, obj);
        System.assert(true);
        Test.stopTest();    
    }
}