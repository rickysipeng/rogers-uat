/*
===============================================================================
Class Name : msdSnapshotSharing_schedular_Test
===============================================================================
PURPOSE: This is a test class for msdSnapshotSharing_schedular class

COMMENTS: 

Developer: Deepika Rawat
Date: 18/9/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
18/9/2013               Deepika               Created
===============================================================================

*/
@isTest(SeeAllData = true)
private class msdSnapshotSharing_schedular_Test{
 static testmethod void testMethod1(){
                
        Test.StartTest();
        msdSnapshotSharing_schedular obj= new msdSnapshotSharing_schedular();
        String sch = '0 0 23 * * ?';
        system.schedule('msdSharing', sch, obj);
        System.assert(true);
        Test.stopTest();    

    }
}