/* ********************************************************************
 * Name : MassApplyZSitesController
 * Description : Controller. Allows user to link Z Sites to Site A
 * Modification Log
  =====================================================================
 * Ver    Date          Author               Modification
 ----------------------------------------------------------------------
 * 1.0    -/--/2011     Kevin DesLauriers    Initial Version  
 * 1.1    5/29/2012     Kevin DesLauriers    Allow Site A to have multiple Z Sites
 * 1.2    6/25/2012		Kevin DesLauriers	 Reuse for Quote Request Process
 **********************************************************************/

public with sharing class MassApplyZSitesController {
    public Opportunity opp {get; set;}
	    
    
    private Id oppId;
    
    public Boolean renderSiteTable {get; set;}
    public Boolean renderSiteMessage {get; set;}
    
    public List<SiteWrapper> zSites {get; set;}
    public Boolean renderZSiteMessage {get; set;}
    public Boolean renderZSiteTable {get; set;}
        
    public Map<String, Site__c> prospectSitesMap {get; set;}

	public Quote_Request__c qRequest {get; set;}
	public Boolean fromQuoteRequest {get; set;}
	private Id returnURL;
	private Id qRequestId;

	
	// These maps have the site id as the key and the QRLocation as the value.
	private Map<Id, Location_A__c> locationAMap = new Map<Id, Location_A__c>();
	private Map<Id, Location_Z__c> locationZMap = new Map<Id, Location_Z__c>();
	private Set<Id> QRLocationSet = new Set<Id>();
    
    // the soql without the order and limit
    private String soql {get;set;}
    
      // the collection of serviceablity to display
	public List<SiteWrapper> prospectSites = new List<SiteWrapper>();
	  
	public List<SiteWrapper> getProspectSites(){
    	return this.prospectSites;
    }
    
    public MassApplyZSitesController() {
        
        try{
            returnURL = ApexPages.currentPage().getParameters().get('retUrl');
        } catch (Exception ex){  // In the case that the user uses the application incorrectly and selects a site before hitting process Sites
            returnURL = ApexPages.currentPage().getParameters().get('retUrl').substring(1);
        }
       
        
        try{
        	qRequestId = ApexPages.currentPage().getParameters().get('qRequestId');
        }catch(Exception ex){
    	}
        fromQuoteRequest = !Utils.isEmpty(qRequestId);
        if (fromQuoteRequest){
        	qRequest = [SELECT Id From Quote_Request__c WHERE Id = :qRequestId];
        	loadQRequestsLocations();
        }
        
        try{
            oppId = ApexPages.currentPage().getParameters().get('oppId');
        } catch (Exception ex){  // In the case that the user uses the application incorrectly and selects a site before hitting process Sites
            oppId = ApexPages.currentPage().getParameters().get('oppId').substring(1);
        }
		try{        
        	opp = [SELECT Id From Opportunity WHERE Id = :oppId];
		}catch(Exception ex){
        	
        }
                
        soql = 'select s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c, s.Street_Name__c, s.Street_Type__c, s.Street_Direction__c, s.City__c, s.Postal_Code__c, s.Display_Name__c, s.Province_Code__c, s.ServiceableLocation__r.Id, s.ServiceableLocation__r.Name, s.Type__c, s.Z_Sites__c from Site__c s where Opportunity__c = : oppId AND Is_a_Z_Site__c = false order by CreatedDate';
        
        // Obtain any prospective sites
        loadProspectSites();  
         // Obtain any prospective sites
        loadZSites(oppId);      
              
        if (prospectSites.size() == 0)
            renderSiteTable = false;
        else
            renderSiteTable = true;
        
        renderSiteMessage = !renderSiteTable;
        
        if (zSites.size() == 0)
            renderZSiteTable = false;
        else
            renderZSiteTable = true;
        
        renderZSiteMessage = !renderZSiteTable;
        
    }
     
   
    private void loadZSites(Id opportunityId) {
        try{
            if(zSites == null) {
                zSites= new List<SiteWrapper>();
                for(Site__c s : [select s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c
                                        , s.Street_Name__c
                                        , s.Street_Type__c
                                        , s.Street_Direction__c
                                        , s.City__c
                                        , s.Postal_Code__c
                                        , s.Province_Code__c
                                        , s.ServiceableLocation__r.Id
                                        , s.ServiceableLocation__r.Name, s.Type__c
                                        , s.Display_Name__c,  s.Z_Sites__c
                                         from Site__c s
                                         where Opportunity__c = : opportunityId
                                         AND s.Is_a_Z_Site__c = true 
                                         order by CreatedDate]) {
                    
                                       
                    zSites.add(new SiteWrapper(s, false, QRLocationSet.contains(s.Id)));
                  
                }
            }
        }
        catch(DMLException e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
    }
  

    
     public void loadProspectSites() {
        Set<Id> currentlySelected = new Set<Id>();
     
      for (SiteWrapper site : prospectSites){
                    if (site.getIsSelected()){
                        currentlySelected.add(site.Site.Id);
                    }
         }
         
         prospectSites.clear();
      try{
                            
                for(Site__c s : Database.query(soql)) {
                    SiteWrapper temp = new SiteWrapper(s, false, QRLocationSet.contains(s.Id));
                    if (currentlySelected.contains(temp.Site.Id))
                        temp.setIsSelected(true);
                        
                    prospectSites.add(temp);
                }
            
        }
        catch(DMLException e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
      

  }
    
     public void loadQRequestsLocations() {
		List<Location_Z__c> zQRLocations = new List<Location_Z__c>();
		List<Location_A__c> aQRLocations = new List<Location_A__c>();
      
      try{
          zQRLocations = [SELECT Id, Prospect_Site__c FROM Location_Z__c WHERE Quote_Request__c = :qRequestId];                  
          aQRLocations = [SELECT Id, Prospect_Site__c FROM Location_A__c WHERE Quote_Request__c = :qRequestId];
                           
        }catch(DMLException e){
            ApexPages.Message myMsg = new ApexPages.Message(ApexPages.Severity.ERROR, e.getMessage());
            ApexPages.addMessage(myMsg);
        }
      
      	for (Location_Z__c z : zQRLocations){
      		QRLocationSet.add(z.Prospect_Site__c);
      		locationZMap.put(z.Prospect_Site__c, z);
      	}

		for (Location_A__c a : aQRLocations){
      		QRLocationSet.add(a.Prospect_Site__c);
      		locationAMap.put(a.Prospect_Site__c, a);
      	}
  }
    
    
    public PageReference runSearch() {
 
        String streetNumber = Apexpages.currentPage().getParameters().get('streetNumber');
        String streetName = Apexpages.currentPage().getParameters().get('streetName');
        String city = Apexpages.currentPage().getParameters().get('city');
        String provinceCode = Apexpages.currentPage().getParameters().get('provinceCode');
        
        
        soql = 'select s.Id , s.Name, s.Suite_Floor__C, s.Street_Number__c, s.Street_Name__c, s.Street_Type__c, s.Street_Direction__c, s.City__c, s.Postal_Code__c, s.Display_Name__c, s.Province_Code__c, s.ServiceableLocation__r.Id, s.ServiceableLocation__r.Name, s.Type__c, s.Z_Site__r.Display_Name__c, s.Z_Sites__c from Site__c s where Opportunity__c = : oppId AND Is_a_Z_site__c = false';
        
         if (!streetNumber.equals(''))
      soql += ' and s.Street_Number__c LIKE \''+String.escapeSingleQuotes(streetNumber)+'%\'';
    if (!streetName.equals(''))
      soql += ' and s.Street_Name__c LIKE \''+String.escapeSingleQuotes(streetName)+'%\'';    
  
    if (!provinceCode.equals(''))
      soql += ' and s.Province_Code__c LIKE \''+String.escapeSingleQuotes(provinceCode)+'%\'';
    if (!city.equals(''))
      soql += ' and s.City__c LIKE \''+String.escapeSingleQuotes(city)+'%\'';  
        
        soql += ' order by CreatedDate';
        
         // run the query again
        loadProspectSites();
 
        return null;
      }
    
   
    /*
         Refactored: May 30/2012 - KD - Allow for selection of many ZSites 
         This method now creates AZ SIte Junction Objects that did not previously 
         exist.
    */   
    public PageReference updateZSites(){
        List<Id> zSiteIds = new List<Id>();
        List<Id> aSiteIds = new List<Id>();
        List<A_Z_Site__c> azSites = new List<A_Z_Site__c>();
        Map<Id, Set<Id>> azSiteMap = new Map<Id, Set<Id>>();
        List<A_Z_Site__c> newAZConnections = new List<A_Z_Site__c>();
        
        
        for (SiteWrapper s : zSites){
            if (s.getIsSelected()){
                zSiteIds.add(s.Site.Id);            
                s.setIsSelected(false);
            }
        }

        for (SiteWrapper s : prospectSites){
            if (s.getIsSelected()){
                aSiteIds.add(s.Site.Id);
                s.setIsSelected(false);
            }
        }
        

        try{
            azSites = [SELECT Site_A__c, Site_Z__c FROM A_Z_Site__c WHERE Site_A__c IN :aSiteIds];
            
            for (A_Z_Site__c az : azSites){
                Set<Id> zSiteList = azSiteMap.get(az.Site_A__c);
                if (zSiteList == null){
                    zSiteList = new Set<Id>();
                }
                zSiteList.add(az.Site_Z__c);
                azSiteMap.put(az.Site_A__c, zSiteList);
            }
       
        }catch(Exception ex){
            // There are no site AZ junction objects for this site A
        }
        
        for (Id a : aSiteIds){
            Set<Id> zSiteList = azSiteMap.get(a);
            for (Id z : zSiteIds){
                if (zSiteList == null || !zSiteList.contains(z))
                newAZConnections.add(new A_Z_Site__c(Site_A__c = a, Site_Z__C = z));
            }
        }
        
        INSERT newAZConnections;
        loadProspectSites();        
        
        return null;
    }
    
   /*
         Refactored: May 30/2012 - KD - Allow for removal of all ZSites. 
         This method now removes the AZ SIte Junction Objects that exist for the A Site.
    */ 
    public PageReference removeZSites(){
        
        String removeAll = Apexpages.currentPage().getParameters().get('allSites');
        Boolean clearAll = !Utils.isEmpty(removeAll) && removeAll == 'true';
        
        List<Site__c> updateSites = new List<Site__c>();
        List<Id> aSiteIds = new List<Id>();
        for (SiteWrapper s : prospectSites){
            if (s.getIsSelected()){
                s.Site.Z_Site__c = null;
                updateSites.add(s.Site);
                s.setIsSelected(false);
                aSiteIds.add(s.Site.Id);
            }
        }
        
        List<A_Z_Site__c> azSites; 
        try{
            azSites = [SELECT Site_A__c, Site_Z__c FROM A_Z_Site__c WHERE Site_A__c IN :aSiteIds];
            if (clearAll){
                delete azSites;
            }else{
                List<A_Z_Site__c> selectedAZSites = new List<A_Z_Site__c>();
                List<Id> zSiteIds = new List<Id>();
                for (SiteWrapper s : zSites){
                    if (s.getIsSelected()){
                        zSiteIds.add(s.Site.Id);            
                        s.setIsSelected(false);
                    }
                }
                
                /* n^3 but we should not have a long list of these so it should be relatively quick */
                for (Id a : aSiteIds){
                    for (Id z : zSiteIds){
                        for (A_Z_Site__c az : azSites){
                            if ((az.Site_A__c == a) && (az.Site_Z__c == z)){
                                selectedAZSites.add(az);
                            }
                        }
                    }
                }
                
                delete selectedAZSites;
            }
        }catch(Exception ex){
            // There are no site AZ junction objects for this site A
        }
       
        loadProspectSites();
        clearZSites();
                      
        return null;
    }
    
    public PageReference returnToOpportunity(){
        PageReference oppPage = new ApexPages.StandardController(opp).view();
        oppPage.setRedirect(true);
        return oppPage;
    }
    
    public PageReference returnToQuoteRequest(){
        PageReference qRequestPage = new ApexPages.StandardController(qRequest).view();
        qRequestPage.setRedirect(true);
        return qRequestPage;
    }
    
    public PageReference addSites(){
        List<Location_A__c> aQRSites = new List<Location_A__c>();
        List<Location_Z__c> zQRSites = new List<Location_Z__c>();
        
        for (SiteWrapper s : zSites){
            if (s.getIsSelected()){
            	if (!QRLocationSet.contains(s.Site.Id)){
                	Location_Z__c locationZ = new Location_Z__c(Name = s.Site.Display_Name__c, Prospect_Site__c = s.Site.Id, Quote_Request__c = qRequestId);
                	zQRSites.add(locationZ);
            		QRLocationSet.add(s.Site.Id);
            		locationZMap.put(s.Site.Id, locationZ);
            	}
                s.setOnQuoteRequest(true);         
                s.setIsSelected(false);
                
            }
        }

        for (SiteWrapper s : prospectSites){
            if (s.getIsSelected()){
            	if (!QRLocationSet.contains(s.Site.Id)){
                	Location_A__c locationA = new Location_A__c(Name = s.Site.Display_Name__c, Prospect_Site__c = s.Site.Id, Quote_Request__c = qRequestId);
                	aQRSites.add(locationA);
                	QRLocationSet.add(s.Site.Id);
                	locationAMap.put(s.Site.Id, locationA);
            	}
                s.setOnQuoteRequest(true);
                s.setIsSelected(false);
            }
        }
        
        INSERT zQRSites;
        INSERT aQRSites;
        
        return null;
    }
   
    public PageReference removeSites(){
       
        List<Location_A__c> aQRSites = new List<Location_A__c>();
        List<Location_Z__c> zQRSites = new List<Location_Z__c>();
        
        for (SiteWrapper s : zSites){
            if (s.getIsSelected()){
            	if (QRLocationSet.contains(s.Site.Id)){
                	zQRSites.add(locationZMap.get(s.Site.Id));
            		QRLocationSet.remove(s.Site.Id);
            		locationZMap.remove(s.Site.Id);
            	}
                s.setOnQuoteRequest(false);         
                s.setIsSelected(false);
                
            }
        }

        for (SiteWrapper s : prospectSites){
            if (s.getIsSelected()){
            	if (QRLocationSet.contains(s.Site.Id)){
                	aQRSites.add(locationAMap.get(s.Site.Id));
                	QRLocationSet.remove(s.Site.Id);
                	locationAMap.remove(s.Site.Id);
            	}
                s.setOnQuoteRequest(false);
                s.setIsSelected(false);
            }
        }
       
        if (!zQRSites.isEmpty())
        	DELETE zQRSites;
        if (!aQRSites.isEmpty())
        	DELETE aQRSites;
        
        return null;
    }
    
    private void clearZSites(){
        for (SiteWrapper s : zSites){
            s.setIsSelected(false);
        }
    }    
}