/*Class Name :customButtonUtilities.
 *Description : This Utilities Class will support the Custom Button Finctionality.
 *Created By : Rajiv Gangra.
 *Created Date :21/08/2013.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
global class customButtonUtilities{

    /**
        * Gets all available Action Plans
        * @return List<ActionPlan__c>
    */
    WebService static String accountSendForApproval(string accId) {
        try{
            Account relatedAcc=[select id,Name,Account_Status__c,RecordTypeId from Account where Id=:accId];
            id caseRecordType=[Select  r.SobjectType, r.Name,r.IsActive, r.Id,r.DeveloperName
                               From RecordType r where r.SobjectType=:'Case' AND r.DeveloperName=:'New_Account' AND r.IsActive=:true ].id;
            id caseSupportQueueID=[Select g.Type, g.Name, g.Id, g.DeveloperName From Group g where g.Type=:'queue' AND g.DeveloperName=:Static_Data_Utilities__c.getInstance('CRM Queue Name').Value__c].id;
            relatedAcc.Account_Status__c='Pending';
            Update relatedAcc;
            Case relatedAccCase= new Case(recordtypeId=caseRecordType,Subject='New Account to Approve:'+relatedAcc.name,AccountId=accId,OwnerID=caseSupportQueueID);
            Insert relatedAccCase;
            return label.Record_successfully_Submitted_for_Approval;
         }catch(exception ex){
            return label.Error_While_Processing_Record;
         }
                  
    }
    
    /**
        * Gets all available Action Plans
        * @return List<ActionPlan__c>
    */
    WebService static string caseSaveandSubmit(string caseId) {
        try{
            case c=[select id,status,AccountID,OwnerID,Account_Owner_Manager__c,Account_Owner__c from case where ID=:caseId];
            if(c.status=='New'){
                Approval.ProcessSubmitRequest req1 = 
                new Approval.ProcessSubmitRequest();
                req1.setComments('Submitting request for approval.');
                req1.setObjectId(c.id);
                // Submit the approval request for the account
                Approval.ProcessResult result = Approval.process(req1);
                
                id caseSupportQueueID=[Select g.Type, g.Name, g.Id, g.DeveloperName From Group g where g.Type=:'queue' AND g.DeveloperName=:Static_Data_Utilities__c.getInstance('CRM Queue Name').Value__c].id;
                c.status='In Progress';
                c.OwnerID=caseSupportQueueID;
                update c;
                return label.Record_successfully_Submitted_for_Approval;
            }else{
                return label.Record_Already_Submitted_for_Approval;
            }
            
        }catch(exception ex){
            return label.Error_While_Processing_Record;
        }
    }
    
     /**
        * Gets all available Action Plans
        * @return List<ActionPlan__c>
    */
    WebService static user accountUpdate(string accId) {
        try{
            system.debug('getting in the button code');
            Account relatedAcc=[select id,Name,Account_Status__c,RecordTypeId,OwnerId from Account where Id=:accId];
            user u=[Select Name,Id,manager.Name,Manager.Id from User Where id =:relatedAcc.OwnerId];
            return u;
         }catch(exception ex){
            return null;
         }
                  
    }
       
}