//
// This is the payload class for lead sharing.
//
public class HermesLead {

    public static final String CLASS_NAME = 'HermesLead';

    public Datetime createdDate;
    public String createdBy;
    public Id createdById;

    public String company;
    public String firstname;
    public String lastname;
    public String email;     
    public String phone;
    public String mobilePhone;
    public String altPhone;                
    public String street; 
    public String city;    
    public String state; 
    public String country; 
    public String postalcode;  
    public String industry; 
    public Integer numberOfEmployees; 

    public String hasInternet; 
    public String internetProvider;

    public String productMobileInternet;
    public String productInternet;
    public String productWireless;
    public String productPhone;
    public String productTV;
    public String productSecurity;

    public String comment;
    public DateTime demoDateTime;  
    public Date followUpDate;
    public String followUpTime;
    public String isRogersCustomer;  
    public String priority;      

    public String toJson() {
        return System.JSON.serialize(this);
    }

    public static HermesLead parseJson(String json) {
        return (HermesLead) System.JSON.deserialize(json, HermesLead.class);
    }
}