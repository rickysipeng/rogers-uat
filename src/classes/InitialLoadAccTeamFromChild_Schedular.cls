/*
===============================================================================
 Class Name   :  InitialLoadAccTeamFromChild_Schedular
===============================================================================
PURPOSE:    This schedular class runs the  InitialLoadAccountTeamFromChild_batch class 
            with a batch of 100 records. 

Developer: Deepika Rawat
Date: 04/10/2015

CHANGE HISTORY
===============================================================================
DATE                 NAME                        DESC
04/2102015          Deepika                     Original Version
===============================================================================
*/
global class InitialLoadAccTeamFromChild_Schedular implements schedulable{

    global void execute(SchedulableContext ctx) {
        InitialLoadAccountTeamFromChild_batch sc = new InitialLoadAccountTeamFromChild_batch();
        ID batchprocessid = Database.executeBatch(sc,100);
        system.debug('sc*************' +sc);
    }

}