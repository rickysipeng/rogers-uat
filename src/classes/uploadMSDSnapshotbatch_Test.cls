/*
===============================================================================
Class Name :uploadMSDSnapshotbatch_Test
===============================================================================
PURPOSE: This is a test class for uploadMSDSnapshotbatch class

COMMENTS: 

Developer: Deepika Rawat
Date: 11/11/2013


CHANGE HISTORY
===============================================================================
DATE                     NAME                  DESC
11/11/2013               Deepika               Created

March 2015              Aakanksha Patel        Modified(For TAG)
===============================================================================

*/
@isTest (SeeAllData = true)
private class uploadMSDSnapshotbatch_Test{
    static Account acc;
    static MSD_Code__c msd; 
    static List<User> listUser = [select id, user.profile.name from User where user.profile.name= 'System Administrator' and isActive = true limit 2];
    private static Team_Assignment_Governance_Settings__c TAG_CS= new Team_Assignment_Governance_Settings__c(); 
 
    static testmethod void testMethod1(){
     /*   TAG_CS.Unassigned_User__c ='Unassigned User';
        TAG_CS.Team_roles__c ='Owner,MSD Owner,MSD Member';
        TAG_CS.Team_Roles_Available__c='ECM,Member,Account Manager,Sales Team,BIS Rep,Solution Consultant,Sales Support,Supporting Executive,SDC,SPM,ISR - Large,ISR - Medium,ISR - Small,Specialist - Medium,Specialist - Small,ISR - Public,Specialist - Large,Specialist - Public';
        insert TAG_CS;
       */ acc = new Account();
        acc.Name='TestAcc';
        acc.OwnerId=listUser[0].id;
        acc.Account_Status__c='Assigned';
        acc.BillingCountry= 'CA';
        acc.BillingPostalCode = 'A9A 9A9';//41 014
        acc.BillingState = 'MA';
        acc.BillingCity='City';
        acc.BillingStreet='Street';
        acc.ParentId= null;
        insert acc;  
        msd= new MSD_Code__c();
        msd.ownerId=listUser[1].id;
        msd.Account__c= acc.id;
        insert msd;
        Database.BatchableContext scc;
        uploadMSDSnapshotbatch sc = new uploadMSDSnapshotbatch();
                   
        Test.StartTest();
        sc.Query ='select id , Name, Account__c, Churn_MTD__c, Churn_QTD__c,Account__r.Name, Churn_YTD__c, Data_Revenue__c, MSD_Active_Data_Only_Lines__c, MSD_Active_Voice_Data_Lines__c, MSD_Active_Voice_Only_Lines__c, MSD_ARPU__c, MSD_Code_Status__c, MSD_Type__c, Voice_Revenue__c from MSD_Code__c WHERE id =\''+ msd.id+ '\' ';
        sc.start(scc);
        List<MSD_Code__c> scope = [select id , Name, Account__c, Churn_MTD__c, Churn_QTD__c,Account__r.Name, Churn_YTD__c, Data_Revenue__c, MSD_Active_Data_Only_Lines__c, MSD_Active_Voice_Data_Lines__c, MSD_Active_Voice_Only_Lines__c, MSD_ARPU__c, MSD_Code_Status__c, MSD_Type__c, Voice_Revenue__c from MSD_Code__c WHERE id =: msd.id];
        sc.execute(scc,scope);
        sc.finish(scc);
        Test.StopTest();
    }
}