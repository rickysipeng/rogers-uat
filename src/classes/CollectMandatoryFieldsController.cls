/*
    @Name: CollectMandatoryFieldsController
    @Description: 
    @Version: 1.1.0 
    
    ===VERSION HISTORY === 
    | Version Number | Author               | Description
    | 1.0.0          | Unknown              |  Initial
    | 1.1.0          | ShiftCRM - RM        |  Add SOI and Requested SOI Date fields to controller 
    
    Jan 23, 2015. Paul Saini. Removed Contact.Ext__c as per requirements.
    As per comments from MP: The data in this field is planned to be concatenated with the standard Phone field. 
    The code should no longer reference the Ext__c field.                                             Pre-populate SOI Date from Serviceable location's region (timeframe define in custom setting)
    Feb 24, 2015 Replace Opportunity.Actual_Sign_Date with Opportunity.CloseDate
*/

global without sharing class CollectMandatoryFieldsController {
    
    public Opportunity opp {get; set;}
    public Quote q {get; set;}
    public List<Mandatory_COP_Fields__c> mandatoryCOPFieldsList {get; set;}
    public Map<String, Mandatory_COP_Fields__c> cifSiteMap {get; set;}
    public Map<String, Contact> contactMap {get; set;}                              // These are bound in the VF Page one per site.
    private List<Contact> initialContacts;
    public String selectedContact {get; set;}
    private List<Contact> contacts {get; set;}
    public String contactFields {get; set;}                                         // contact fields for rerendering
    private Map<String, Id> contactKeyToIdMap = new Map<String, Id>();
    public Map<String, List<String>> contactLocationMap {get; set;} // Used to obtain the contact Location (contactMap key from the contactKey -> fName + lName + phone + mPhone)
    private Set<String> contactKeys{private get; private set;} 
    public Boolean isCompleted {get; private set;}
    public Boolean isSystemAdminSOC {get; private set;}
    public Boolean isDemarcMandatory {get; private set;}
    
        
    public CollectMandatoryFieldsController (ApexPages.StandardController stdController) {
        String oppId = ApexPages.currentPage().getParameters().get('oppId');
        
        if (Utils.isEmpty(oppId)){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Missing Opportunity Id in URL. Please ensure to enter this page via the button on the Opportunity Detail Page after you have WON an opportunity and the quote is synced.'));
            return;
        }
        
        
        try{
            // Get the Opportunity
            opp = [SELECT Id, Mandatory_COP_Fields_Status__c,  CloseDate, StageName, Is_Verbal_Commit__c, SyncedQuoteId, Account.BillingCountry, Account.BillingPostalCode, Account.BillingState, Account.BillingCity, Account.BillingStreet, Contact__r.Name, Account.Name FROM Opportunity WHERE Id = :oppId];
            
            // Get the Quote
            if (opp != null && opp.SyncedQuoteId != null){
                q = [SELECT id, Name, QuoteNumber__c FROM Quote WHERE Id = :opp.SyncedQuoteId];
            }
            
            if (q != null){
                 initMandatoryFields();
            }
        }catch(Exception ex){
            if (opp == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the Opportunity from the given Id.  Please ensure that the Opportunity Id is valid.'));
                return;
            } else if (q == null){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the synced quote from the opportunity.  Please ensure there is a quote synced to this opportunity before proceeding.'));
                return;
            }
        }
        
        if (opp == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the Opportunity from the given Id.  Please ensure that the Opportunity Id is valid.'));
            return;
        } else if (q == null){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the synced quote from the opportunity.  Please ensure there is a quote synced to this opportunity before proceeding.'));
            return;
        } else if (mandatoryCOPFieldsList == null || mandatoryCOPFieldsList.isEmpty()){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the quote sites for this opportunity.  Please ensure there is at least one site on this quote before proceeding.'));
            mandatoryCOPFieldsList = new List<Mandatory_COP_Fields__c>();
            mandatoryCOPFieldsList.add(new Mandatory_COP_Fields__c());
                            
            return;
        } 
        
        initMandatoryFields();
        
        
        isCompleted = !Utils.isEmpty(opp.Mandatory_COP_Fields_Status__c) && opp.Mandatory_COP_Fields_Status__c.equals('Submitted/Completed');
        
        User currentUser = [SELECT Id, Profile.Name FROM User WHERE Id = :UserInfo.getUserId()];
        isSystemAdminSOC = (currentUser.Profile.Name != 'Sales Order Coordinator') || currentUser.Profile.Name != 'System Administrator';
        isDemarcMandatory = opp.Is_Verbal_Commit__c;
        
    }
    
    private void initMandatoryFields(){
        try{
            //V1.1 RM - added Customer_Requested_SOIDate__c, SOI_Requested_Date__c, SOI_Date__c, Quote_Site__r.Site__r.ServiceableLocation__r.Region__c to query
            // removed Ext__c from query below.
            mandatoryCOPFieldsList = [SELECT Id, CreatedDate, 
                        Building_Entry_Contact__r.FirstName, Building_Entry_Contact__r.LastName, Building_Entry_Contact__r.Phone, Building_Entry_Contact__r.MobilePhone, Building_Entry_Contact__r.Email,
                        Building_Entry_Contact__r.Title, Building_Entry_Contact__r.Phone_Extension__c, Building_Entry_Contact__r.MailingCity, Building_Entry_Contact__r.MailingCountry, Building_Entry_Contact__r.MailingState, Building_Entry_Contact__r.MailingStreet, Building_Entry_Contact__r.MailingPostalCode,
                        Building_Entry_Contact__c, Not_Applicable__c, Quote_Site__c, Roger_Account_Optional__c, Suite_Floor__c, Quote_Site__r.Site__r.Display_Name__c, Demarc__c, 
                        Customer_Requested_SOIDate__c, SOI_Requested_Date__c, SOI_Date__c,
                        Quote_Site__r.Site__r.ServiceableLocation__r.Region_Access_Type__c 
                        FROM Mandatory_COP_Fields__c WHERE Quote_Site__r.Quote__c = :opp.SyncedQuoteId Order By CreatedDate asc];
        }catch(Exception ex1){
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to obtain the quote sites for this opportunity.  Please ensure there is at least one site on this quote before proceeding.'));
                mandatoryCOPFieldsList = new List<Mandatory_COP_Fields__c>();
                mandatoryCOPFieldsList.add(new Mandatory_COP_Fields__c());
                return;
        }
        
        cifSiteMap = new Map<String, Mandatory_COP_Fields__c>();
        
        // iterate through the Mandatory Field List and init the Map.
        Map<String, Shift_Region_Timeframe_Mapping__c> SOITimeframeMappings = Shift_Region_Timeframe_Mapping__c.getAll(); //V1.1 RM - Get custom settings as map
        BusinessHours bh = [select Id from BusinessHours where IsDefault = true]; // get default business days
        for (Mandatory_COP_Fields__c m : mandatoryCOPFieldsList){
            
            //V1.1 RM - If a custom setting timeframe mapping exists for the region, add the timeframe to the todays date
            //If it doesn't exist force the to put in a custom date
            if (SOITimeframeMappings.containsKey(m.Quote_Site__r.Site__r.ServiceableLocation__r.Region_Access_Type__c)) {
                if (Opp.CloseDate != NULL) {
                    // get time frame from custom setting
                    integer theTimeframe = integer.valueOf(SOITimeframeMappings.get(m.Quote_Site__r.Site__r.ServiceableLocation__r.Region_Access_Type__c).Timeframe__c) + 1;
                    
                    // calculate SOI Date based on business hours
                    m.SOI_Date__c = BusinessHours.addGmt(bh.Id, opp.CloseDate, theTimeframe * 24 * 60 * 60 * 1000L).date();
                     
                    //m.SOI_Date__c = date.valueOf(opp.CloseDate) + integer.valueOf(SOITimeframeMappings.get(m.Quote_Site__r.Site__r.ServiceableLocation__r.Region_Access_Type__c).Timeframe__c);
                }   
            } else {
                m.Customer_Requested_SOIDate__c = true;
            }           
            
            cifSiteMap.put(m.Quote_Site__r.Site__r.Display_Name__c, m);
            
        }
        
        contactLocationMap = new Map<String, List<String>>();
        initialContacts = [SELECT Id, Name, FirstName, LastName, Phone, MobilePhone, Email, Phone_Extension__c, MailingCity, MailingCountry, Title, MailingPostalCode, MailingState, MailingStreet FROM Contact WHERE AccountId = :opp.AccountId];
        contacts = initialContacts;
        contactKeys = new Set<String>();   
        initializeContactMap(initialContacts);
        contactFields = 'accordionpb_bec';
        
    }
    
    public PageReference returnToOpportunity(){
        PageReference pageRef = new PageReference('/' + opp.Id);
        pageRef.setRedirect(true);
        
        return pageRef;
    }
    
     public List<SelectOption> getExistingContacts(){

        List<SelectOption> options = new List<SelectOption>();
        options.add(new SelectOption('', '--None--'));
        for(Contact c : contacts){
            options.add(new SelectOption(getContactKey(c), c.FirstName+ ' ' +c.LastName));
        }       

        return options;
    }
    
     private String getContactKey(Contact c){
        
        if (c == null)
            return null;
        // PS: removed Ext__c from string below
        String key = c.FirstName+c.LastName+c.Phone+c.MobilePhone+c.Email+c.Phone_Extension__c+c.Title+c.MailingCity+c.MailingCountry+c.MailingState+c.MailingStreet+c.MailingPostalCode;
        
        key = Utils.cleanSpaces(key);
        
        if (key.equals('nullnullnullnullnullnullnullnullnullnullnullnull')){
            return null;
        }
            
        return key;
    }
    
    public PageReference updateContact(){
        String contactKey = Apexpages.currentPage().getParameters().get('contactkey');
        String contactLocation = Apexpages.currentPage().getParameters().get('contactLocation');           
    
        
        Contact cTemp = contactMap.get(contactLocation);
    
        if (contactKeyToIdMap.get(contactLocation)==null)
            return null;
        
    
        // PS: removed Ext__c from query below.
        Contact tempContact = [SELECT Id, Name, FirstName, LastName, Phone, MobilePhone, Email, Phone_Extension__c, MailingCity, MailingCountry, Title, MailingPostalCode, MailingState, MailingStreet FROM Contact WHERE Id = :contactKeyToIdMap.get(contactLocation) LIMIT 1];
        updateCurrentContact(cTemp, tempContact);
        
        
       try{
            UPDATE tempContact;
       }catch(Exception ex){
            if (ex.getMessage().indexOf('SSDup') == -1){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to create the Contact.  Please ensure there is not a problem with the data you entered.  You may want to contact your administrator.'));
            }
       }
        saveMandatoryFields();
        initMandatoryFields();
       
        return null;
    }
    
    public PageReference useExistingContact(){
        String contact = Apexpages.currentPage().getParameters().get('contactkey');
        String contactLocation = Apexpages.currentPage().getParameters().get('contactLocation');
                
        if (Utils.isEmpty(contact) || Utils.isEmpty(contactLocation)){
            return null;
        }
        
        for (Contact c : contacts){
            System.debug(getContactKey(c));
            if (contact.equals(getContactKey(c))){
                contactMap.put(contactLocation, c.clone(false, true));
                // Since we are cloning without the id - we need to store in somewhere for later.
                contactKeyToIdMap.put(contactLocation, c.Id);  // We know that this contact location is using a particular contact
                
                List<String> locations = contactLocationMap.get(getContactKey(c));
                if (locations==null)
                    locations = new List<String>();
                locations.add(contactLocation);         
                contactLocationMap.put(getContactKey(c), locations);    // We will need this when we update the contact
                                                                                                    // from the contactKey
            }
        }
        
        saveMandatoryFields();
        return null;
    }
    
    public PageReference submitMandatoryFields(){
        Boolean isSubmittable = true;
        
        for (Mandatory_COP_Fields__c cSite : mandatoryCOPFieldsList){
            if (Utils.isEmpty(cSite.Suite_Floor__c) && !cSite.Not_Applicable__c){   // reads backwards  if this is checked it is true and means NA
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You are uable to submit the form and create the COP without completing all of the mandatory fields.  Please ensure to either add the Suite/Floor or check N/A.  Site: ' + cSite.Quote_Site__r.Site__r.Display_Name__c));
                
                isSubmittable = false;
                //break;
            }
            
            if (isDemarcMandatory && Utils.isEmpty(cSite.Demarc__c)){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You are uable to submit the form and create the COP without completing all of the mandatory fields.  Please ensure to complete the demarcation information.  Site: ' + cSite.Quote_Site__r.Site__r.Display_Name__c));
                
                isSubmittable = false;
                //break;
            }
            
            
            if (Utils.isEmpty(contactMap.get('bec_' + cSite.Id).FirstName) || Utils.isEmpty(contactMap.get('bec_' + cSite.Id).LastName) || Utils.isEmpty(contactMap.get('bec_' + cSite.Id).Email) || Utils.isEmpty(contactMap.get('bec_' + cSite.Id).Phone)){   
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You are uable to submit the form and create the COP without completing all of the mandatory fields.  Please ensure to fill in all of the mandatory contact fields.  Site: ' + cSite.Quote_Site__r.Site__r.Display_Name__c));
                isSubmittable = false;
//              break;
            }
        }
        
        if (!isSubmittable){
  //        ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'You are uable to submit the form and cretae the COP without completing all of the mandatory fields.'));
            return null;
        }
        
        
        PageReference  pdf  = createPDF();
        
        saveMandatoryFields();
        
        // Need to set the Mandatory Fields as Completed!
        opp.Mandatory_COP_Fields_Status__c = 'Submitted/Completed';
        UPDATE opp;
        
        // Create the COP and update with these fields.
        //Id cifId = CIFWebServices.createCIF_v2(opp.Id); 
        /*
        if (!Utils.isEmpty('' + cifId) && cifId != opp.Id){
            PageReference pageRef = new PageReference('/' + opp.Id);
            return pageRef.setRedirect(true);
        }else{
            ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to create the COP.  Please ensure there is not a problem with the data you entered.  You may want to contact your administrator.'));
            return null;
        }
        */
        
        // Redirect to the mandatory PDF Page
        return pdf;
    }
    
    public void saveMandatoryFields(){
        for (Mandatory_COP_Fields__c cSite : mandatoryCOPFieldsList){
            cSite.Roger_Account_Optional__c = mandatoryCOPFieldsList[0].Roger_Account_Optional__c;
            
            for (String key : contactKeyToIdMap.keySet()){
                Id contactId = contactKeyToIdMap.get(key);          
            
                // The contact does not have the id - only one of the contacts does!  Use the contact that contains the id.
                if (contactId!=null){
                    if (key.equals('bec_' + cSite.Id)){
                        cSite.Building_Entry_Contact__c = contactId;    
                    }
                }
            }
        }
        
        UPDATE mandatoryCOPFieldsList;
    }
    
    private void initializeContactMap(List<Contact> currentContacts){
        contactMap = new Map<String, Contact>();
   
        for (Mandatory_COP_Fields__c cSite : mandatoryCOPFieldsList){               
            contactMap.put('bec_' + cSite.Id, (cSite.Building_Entry_Contact__r!=null)?cSite.Building_Entry_Contact__r.clone(false, true): new Contact());
    
            if (cSite.Building_Entry_Contact__c!=null){ 
                contactKeyToIdMap.put('bec_' + cSite.Id, cSite.Building_Entry_Contact__c);
                contactKeys.add(getContactKey(cSite.Building_Entry_Contact__r));                
                List<String> locations = contactLocationMap.get(getContactKey(cSite.Building_Entry_Contact__r));
                if (locations==null)
                    locations = new List<String>();
                locations.add('bec_' + cSite.Id);         
                contactLocationMap.put(getContactKey(cSite.Building_Entry_Contact__r), locations);      // We will need this when we update the contact so that we can obtain the key (contactLocation) from the contactKey
            }
        }
    }
    
     private void updateCurrentContact(Contact src, Contact dest){
        dest.FirstName = src.FirstName;
        dest.LastName = src.LastName;
        dest.Phone = src.Phone;
        dest.MobilePhone = src.MobilePhone;
        dest.Email = src.Email;
        dest.Title = src.Title;
        dest.MailingCity = src.MailingCity;
        dest.Phone_Extension__c = src.Phone_Extension__c;
        // PS: dest.Ext__c = src.Ext__c;
        dest.MailingCountry = src.MailingCountry;
        dest.MailingPostalCode = src.MailingPostalCode;
        dest.MailingStreet = src.MailingStreet;
        dest.MailingState = src.MailingState;
        
    }
    
    public PageReference addNewContact(){
        String contactLocation = Apexpages.currentPage().getParameters().get('contactLocation');
        
        if (Utils.isEmpty(contactLocation)){
            return null;
        }
        
        // The VF Page has a binding to the contactMap so let's obtain the contact 
        Contact c = contactMap.get(contactLocation).clone(false, true);
        c.AccountId = opp.AccountId;
        try{
            INSERT c;
            
            // Since we are cloning without the id - we need to store in somewhere for later.
            contactKeyToIdMap.put(contactLocation, c.Id);  // We know that this contact location is using a particular contact
            
            if (c!=null){
                contacts.add(c);
                contactKeys.add(getContactKey(c)); 
            }
            
            // When we add a new contact to the list we also need to add it to the map in case it is updated later.
            List<String> locations = new List<String>();
            locations.add(contactLocation);         
            contactLocationMap.put(getContactKey(c), locations);            // We will need this when we update the contact
                                                                            // so that we can obtain the key (contactLocation)
                                                                            // from the contactKey
            
            
            saveMandatoryFields();
        }catch(Exception ex){
            if (ex.getMessage().indexOf('SSDup') == -1){
                ApexPages.addmessage(new ApexPages.message(ApexPages.severity.ERROR,'Unable to create the Contact.  Please ensure there is not a problem with the data you entered.  You may want to contact your administrator. ' + ex.getMessage() ));
            }
        }
        return null;
    }
    
    public PageReference createPDF(){
        
        // navigate to the generateMandatoryFieldsPDF -> it will call the MandatoryFieldsPDF and save it.  These pages both use the MandatoryFieldsPDFController
        //return new PageReference('/apex/GenerateMandatoryFieldsPDF?oppid=' + opp.Id);
        
        List<Attachment> listOPPAttachments = new List<Attachment>();
        Attachment oppAttachment = new Attachment();
        try{
            Opportunity o = [SELECT Id, Name FROM Opportunity WHERE Id = :opp.Id LIMIT 1];
        
            PageReference pdf = Page.MandatoryFieldsPDF;
          
            // Add parent id to the parameters for standardcontroller
            pdf.getParameters().put('oppid',o.Id);
              
            // The contents of the attachment from the pdf
            Blob body;
         
            try {
                // returns the output of the page as a PDF
                body = pdf.getContent();
         
            // need to pass unit test -- current bug    
            } catch (VisualforceException e) {
                body = Blob.valueOf('Some Text');
            }
         
         
            oppAttachment.Body = body;
            oppAttachment.ParentId = o.Id;
            oppAttachment.Name = o.Name + ' Mandatory Fields.pdf'; 
            
            try{
                listOPPAttachments = [SELECT id, ParentId, Name FROM Attachment WHERE ParentId = :o.Id AND Name = :oppAttachment.Name];
                if (listOPPAttachments != null && listOPPAttachments.size()>0){
                    DELETE listOPPAttachments;
                }
            }catch(Exception ex){
                
            }
            
            insert oppAttachment;
            
        }catch(Exception ex){
            return null;
        }
        return new PageReference('/'+opp.Id);
        
        
        
        // We need all of the Mandatory Field Objects.  There is one per site.  This should make it easy to display.
        
        
        
        
        //return null;
    }
    
    @RemoteAction
    global static Set<String> getUnsavedContacts(String contactLocation, List<String> unsaved){
        Set<String> unsavedContacts = new Set<String>();
            if (unsaved!=null && !unsaved.isEmpty()){
                for (String c : unsaved){
                    if (!c.equals(contactLocation))
                        unsavedContacts.add(c);
                }
            }
        
        return unsavedContacts;
    }
    
}