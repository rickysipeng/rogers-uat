public without sharing class ContractOppRenewalExtension {
	public String accId {get; set;}
	public String oppId {get; set;}
	public List<Wireline_Contract__c> existingContractsList { get; private set;}
	public List<ContractWrapper> contractWrapperList{ get; set; }
	public List<Selected_Contract_Opp__c> existingSelectedContractsList { get; private set;}
	public Opportunity parentOpp { get; private set; }
	public Integer numWrapped { get; private set; }
	public Boolean isNew { get; private set; }
	public String recordType {get; set;}
	public String owner {get; set;}
	public Integer numExisting {get; private set;}
	ApexPages.StandardController oStdController;
		
	public ContractOppRenewalExtension(ApexPages.StandardController stdController) {
		oStdController = stdController;
		oppId = stdController.getId();
		
		recordType = 'Enterprise Contract Renewal';
		owner = UserInfo.getName();
		
		if (Utils.isEmpty(oppId)){
			isNew = true;
		}else
			isNew = false;
			
		contractWrapperList = new List<ContractWrapper>();
		numWrapped = 0;
		
		accId = ApexPages.currentPage().getParameters().get('accId');
		String recType = ApexPages.currentPage().getParameters().get('RecordType');
		
		SObject opp = stdController.getRecord();
		
		if (accId != null){
			opp.put('AccountId', accId);
		}
		
		if (recType != null){
			opp.put('RecordTypeId', recType);
		}
		
		try{
			parentOpp = [SELECT id, AccountId, No_Existing_Contract__c FROM Opportunity WHERE id = :oppId LIMIT 1];
		}catch(Exception ex){
			
		}
		
		if (parentOpp != null){
			accId = parentOpp.AccountId;
		}

		if (accId == null){
			numExisting = 0;
			return;
		}		
		
		existingContractsList = [ select Id, Name
					, Contract_Number__c
					, Contract_Start_Date__c
					, Contract_End_Date__c
					, Status__c
					, Current_Contract_Flag__c 
				from Wireline_Contract__c
				where Account_Name__c = : accId
				];
		numExisting = existingContractsList != null ? existingContractsList.size() : 0;
				
		existingSelectedContractsList = [select Id, Name, Wireline_Contract__c 
				from Selected_Contract_Opp__c 
				where Parent_Opportunity__c = : oppId];
				
		
		Set<Id> existingContracts = new Set<Id>();
		
		if (existingSelectedContractsList != null){
			for (Selected_Contract_Opp__c sc : existingSelectedContractsList){
				existingContracts.add(sc.Wireline_Contract__c);
			}			
		}
		
		for (Wireline_Contract__c c : existingContractsList){
			ContractWrapper cw = new ContractWrapper(c);
			if (existingContracts.contains(c.Id))
				cw.isSelected = true;
			
			contractWrapperList.add(cw);
		}
		numWrapped = contractWrapperList.size();
		//getRelatedContracts();

	}
	
	public PageReference getRelatedContracts(){
		System.debug('This is the beginning 1: ');
		accId =	ApexPages.currentPage().getParameters().get('acctId');
		System.debug('This is the beginning 2: ' + accId);
		if (parentOpp != null)
			parentOpp.AccountId = accId;
		
		
		
		contractWrapperList = new List<ContractWrapper>();
		numWrapped = 0;
			
			existingContractsList = [ select Id, Name
					, Contract_Number__c
					, Contract_Start_Date__c
					, Contract_End_Date__c
					, Status__c
					, Current_Contract_Flag__c 
				from Wireline_Contract__c
				where Account_Name__c = : accId
				];
				
		numExisting = existingContractsList != null ? existingContractsList.size() : 0;
		
		System.debug('This is the beginning 4: ' + existingContractsList);
		
		Set<Id> existingContracts = new Set<Id>();
		
		for (Wireline_Contract__c c : existingContractsList){
			ContractWrapper cw = new ContractWrapper(c);
			if (existingContracts.contains(c.Id))
				cw.isSelected = true;
			
			contractWrapperList.add(cw);
		}
		numWrapped = contractWrapperList.size();
		
		return null;
	}
	
	public PageReference updateRelatedContracts(){
		PageReference pRef = null;
		
		
		pRef = oStdController.save();
			
		if (isNew){
			System.debug('The opportunity Id: ' + oStdController.getId());
			parentOpp = [SELECT id, AccountId, No_Existing_Contract__c FROM Opportunity WHERE id = :oStdController.getId() LIMIT 1];
			if (oppId == null)
				oppId = parentOpp.id;
			
		}
		
	//	pRef = new PageReference('/'+parentOpp.id);
		
		List<Wireline_Contract__c> selectedContracts= new List<Wireline_Contract__c>();
		List<Wireline_Contract__c> contractsToUpdate = new List<Wireline_Contract__c>();            
        
        for(ContractWrapper cw : contractWrapperList) {
        	if(cw.isSelected) {
            	selectedContracts.add(cw.selContract);
            }
            
            contractsToUpdate.add(cw.getContract());
            
       	}
       	
       	try{
 			UPDATE contractsToUpdate;
 		}catch(Exception ex){
 			System.debug('Unable to update contracts. ' + ex.getMessage());
 		}
 		
 		Set<Id> originalContractIds = new Set<Id>();
        Set<Id> currentContractIds = new Set<Id>();
        
        if (this.existingSelectedContractsList!=null){
             for (Selected_Contract_Opp__c sc : this.existingSelectedContractsList){
                originalContractIds.add(sc.Wireline_Contract__c);
             }
         }
                
                
        existingSelectedContractsList = new List<Selected_Contract_Opp__c>();        
         
        for(Wireline_Contract__c c : selectedContracts){
            Selected_Contract_Opp__c sc = new Selected_Contract_Opp__c();
            sc.Parent_Opportunity__c = oppId;
            sc.Wireline_Contract__c = c.Id;
        
            if (!originalContractIds.contains(c.Id)){                    
                existingSelectedContractsList.add(sc);
            }
        
            currentContractIds.add(c.Id);
        }
                   
        INSERT existingSelectedContractsList;
        
        List<Selected_Contract_Opp__c> expiredContracts = [SELECT Id FROM Selected_Contract_Opp__c WHERE Parent_Opportunity__c = :oppId AND Wireline_Contract__c NOT IN :currentContractIds];
        DELETE expiredContracts;
        
         	
        
        return pRef;
   	
	}
	
	public PageReference updateContracts(){
		PageReference pRef = null;
		
		
		
		List<Wireline_Contract__c> selectedContracts= new List<Wireline_Contract__c>();
		List<Wireline_Contract__c> contractsToUpdate = new List<Wireline_Contract__c>();            
        
        for(ContractWrapper cw : contractWrapperList) {
        	if(cw.isSelected) {
            	selectedContracts.add(cw.selContract);
            }
            
            contractsToUpdate.add(cw.getContract());
            
       	}
       	
       	try{
 			UPDATE contractsToUpdate;
 		}catch(Exception ex){
 			System.debug('Unable to update contracts. ' + ex.getMessage());
 		}
 		
 		Set<Id> originalContractIds = new Set<Id>();
        Set<Id> currentContractIds = new Set<Id>();
        
        if (this.existingSelectedContractsList!=null){
             for (Selected_Contract_Opp__c sc : this.existingSelectedContractsList){
                originalContractIds.add(sc.Wireline_Contract__c);
             }
         }
                
                
        existingSelectedContractsList = new List<Selected_Contract_Opp__c>();        
         
        for(Wireline_Contract__c c : selectedContracts){
            Selected_Contract_Opp__c sc = new Selected_Contract_Opp__c();
            sc.Parent_Opportunity__c = oppId;
            sc.Wireline_Contract__c = c.Id;
        
            if (!originalContractIds.contains(c.Id)){                    
                existingSelectedContractsList.add(sc);
            }
        
            currentContractIds.add(c.Id);
        }
                   
        INSERT existingSelectedContractsList;
        
        List<Selected_Contract_Opp__c> expiredContracts = [SELECT Id FROM Selected_Contract_Opp__c WHERE Parent_Opportunity__c = :oppId AND Wireline_Contract__c NOT IN :currentContractIds];
        DELETE expiredContracts;
        
         	
        
        return pRef;
   	
	}
	
	public PageReference resetAccount(){
		String accountId = ApexPages.currentPage().getParameters().get('acctId');
		parentOpp.AccountId = accountId;
		
		return null;
	}
	
	public class ContractWrapper
	{
		Wireline_Contract__c selContract;
		Boolean isSelected;
		
		public void setContract(Wireline_Contract__c b) { this.selContract = b; }
    	public Wireline_Contract__c getContract() {return selContract; }
    	
    	public void setIsSelected(Boolean b) { this.isSelected = b; }
    	public Boolean getIsSelected() { return isSelected; }
		
		public ContractWrapper( Wireline_Contract__c contract)
		{
			this.selContract = contract;
			isSelected = false;
		}
	}
}