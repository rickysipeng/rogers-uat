/**
 * This class contains unit tests for validating the behavior of Apex classes
 * and triggers.
 *
 * Unit tests are class methods that verify whether a particular piece
 * of code is working properly. Unit test methods take no arguments,
 * commit no data to the database, and are flagged with the testMethod
 * keyword in the method definition.
 *
 * All test methods in an organization are executed whenever Apex code is deployed
 * to a production organization to confirm correctness, ensure code
 * coverage, and prevent regressions. All Apex classes are
 * required to have at least 75% code coverage in order to be deployed
 * to a production organization. In addition, all triggers must have some code coverage.
 * 
 * The @isTest class annotation indicates this class only contains test
 * methods. Classes defined with the @isTest annotation do not count against
 * the organization size limit for all Apex scripts.
 *
 * See the Apex Language Reference for more information about Testing and Code Coverage.
 */
/*
========================================================================
Date                        Name                        Status
March 2015         Aakanksha Patel            Modified(For TAG)
========================================================================
*/
@isTest
private class TestSiteServiceSummary {

 static Id oppId, sl1Id, sl2Id, site1Id, site2Id, site3Id, site4Id, site5Id, site6Id, site7Id, qli1Id, qli2Id; 
    static QuoteLineItem qliTest;
    static Id priceBookEntryAccessId1, priceBookEntryInstallId1, quote1Id;
    static Opportunity o, o_carrier;
    static Product2 pAccess, pInstall;
    static Boolean withAssertions = true;
    static Quote q;
    
static {
        List <RecordType > lRta = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Account']);
        Map <string,Id> mapRTa = new Map <string,id> ();
        
        for (RecordType rta : lRta){
            mapRTa.put(rta.Name,rta.id);  
        }
        
        List <RecordType > lRto = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Opportunity']);
        Map <string,Id> mapRTo = new Map <string,id> ();
        
        for (RecordType rto : lRto){
            mapRTo.put(rto.Name,rto.id);  
        }
        
        List <RecordType > lRtp = new List <RecordType> ([Select id,recordtype.Name from RecordType where recordtype.SobjectType = 'Product2']);
        Map <string,Id> mapRTp = new Map <string,id> ();
        
        for (RecordType rtp : lRtp){
            mapRTp.put(rtp.Name,rtp.id);  
        }
       
        Account a = new Account();
        a.name = 'Test Act test test test 123';
        a.Business_Segment__c = 'Alternate';
        a.RecordTypeId = mapRTa.get('New Account');
        a.BillingStreet = 'Street';
        a.BillingCity = 'MyCity';
        a.BillingCountry = 'Canada';
        a.BillingPostalCode = 'L1L1L1';
        a.BillingState = 'ON';
        a.ParentId = null;
        a.Account_Status__c = 'Assigned';
        insert a;       
        
        o = new Opportunity();
        o.Estimated_MRR__c = 500;
        o.Name = 'Test Opp';
        o.StageName = 'Suspect - Qualified';
        o.Product_Category__c = 'Local';
        o.Network__c = 'Cable';
        o.Estimated_One_Time_Charge__c = 500;
        o.New_Term_Months__c = 5;
        o.AccountId = a.id;
        o.RecordTypeId = mapRTo.get('Wireless - New Opportunity');
        o.CloseDate = date.today();
        insert o;
        oppId = o.id;
        
        Pricebook2 sp = new Pricebook2();
        sp = [select id from Pricebook2 where Name = :'Enterprise PriceBook'];
        
        Pricebook2 carrier_sp = new Pricebook2();
        carrier_sp = [select id from Pricebook2 where Name = :'Carrier PriceBook'];
        
        Pricebook2 sp1 = new Pricebook2();
        sp1 = [select id from Pricebook2 where isStandard = true];
        
        
        ServiceableLocation__c sl = new ServiceableLocation__c();
        sl.Street_Name__c = 'Somewhere';
        sl.Street_Number__c  = '5';
        sl.Street_Type__c  = 'Ave';
        sl.City__c = 'Coty';
        sl.Postal_Code__c = 'A1A1A1';
        sl.Province_Code__c = 'ON';
        sl.CLLI_Code__c = '123113';
        sl.Access_Type_Group__c = 'NNI';
        sl.Access_Type__c = 'Ethernet Eon';
        insert sl;
        sl1Id = sl.Id;
        
        ServiceableLocation__c sl2 = new ServiceableLocation__c();
        sl2.Street_Name__c = 'Somewhere';
        sl2.Street_Number__c  = '5';
        sl2.Street_Type__c  = 'Ave';
        sl2.City__c = 'Coty';
        sl2.Postal_Code__c = 'A1A1A1';
        sl2.Province_Code__c = 'ON';
        sl2.CLLI_Code__c = '123113';
        sl2.Access_Type_Group__c = 'Fibre';
        sl2.Access_Type__c = 'ETHERNET EON;ETHERNET CAP';
        insert sl2;
        sl2Id = sl2.Id;
       
        
        Site__c s1 = new Site__c();
        s1.Street_Name__c = 'Somewhere1';
        s1.Suite_Floor__c = '11a';
        s1.Street_Number__c  = '5';
        s1.City__c = 'Coty';
        s1.Postal_Code__c = 'A1A1A1';
        s1.Province_Code__c = 'ON';
        s1.CLLI_SWC__c = '123113';
        s1.ServiceableLocation__c = sl1Id;
        s1.Opportunity__c = oppId;
        insert s1;
        site1Id = s1.Id;
        
        Site__c s2 = new Site__c();
        s2.Street_Name__c = 'Somewhere2';
        s2.Suite_Floor__c = '11a';
        s2.Street_Number__c  = '5';
        s2.City__c = 'Coty';
        s2.Postal_Code__c = 'A2A1A1';
        s2.Province_Code__c = 'ON';
        s2.CLLI_SWC__c = '123113';
        s2.ServiceableLocation__c = sl2Id;
        s2.Opportunity__c = oppId;
        s2.Is_a_Z_Site__c = true;
        insert s2;
        site2Id = s2.Id;
        
        // This site is Serviceable
        Site__c s3 = new Site__c();
        s3.Suite_Floor__c = '11a';
        s3.Street_Name__c = 'Somewhere3';
        s3.Street_Number__c  = '5';
        s3.City__c = 'Coty';
        s3.Postal_Code__c = 'A3A1A1';
        s3.Province_Code__c = 'ON';
        s3.CLLI_SWC__c = '123113';
        s3.ServiceableLocation__c = sl.Id;
        s3.Opportunity__c = oppId;
        insert s3;
        site3Id = s3.Id;
        
        q = new Quote(Name='q1', actualTerm__c = '12', Term__c='12-Months', OpportunityId=oppId, Pricebook2Id = sp.id);
        insert q;
        quote1Id = q.Id;
        
        
        Quote_Site__c qs = new Quote_Site__c();
        qs.Site__c = site1Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site2Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        qs = new Quote_Site__c();
        qs.Site__c = site3Id;
        qs.Quote__c = quote1Id;
        insert qs;
        
        /* Products for Original Term */
        // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '12-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Access Product
        pAccess  = new Product2();
        pAccess.IsActive = true;
        pAccess.Name = 'Fibre';
        pAccess.Charge_Type__c = 'MRC';
        pAccess.Access_Type__c = 'Ethernet EON';
        pAccess.Access_Type_Group__c = 'Fibre';
        pAccess.Category__c = 'Access';
        pAccess.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pAccess.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccess.Service_Term__c = '12-Months';
        pAccess.Product_Install_Link__c = pInstall.Id;
        pAccess.Mark_Up_Factor__c = 1.0;
        pAccess.Start_Date__c = Date.today();
        pAccess.End_Date__c = Date.today()+1;
        insert pAccess;
        
        
        PricebookEntry pbeAccessStandard = new PricebookEntry();
        pbeAccessStandard.Pricebook2Id = sp1.id;
        pbeAccessStandard.UnitPrice = 60;
        pbeAccessStandard.Product2Id = pAccess.id;
        pbeAccessStandard.IsActive = true;
        pbeAccessStandard.UseStandardPrice = false;
        insert pbeAccessStandard;
        
        PricebookEntry pbeNonStandardAccess = new PricebookEntry();
        pbeNonStandardAccess.Pricebook2Id = sp.id;
        pbeNonStandardAccess.UnitPrice = 60;
        pbeNonStandardAccess.Product2Id = pAccess.id;
        pbeNonStandardAccess.IsActive = true;
        pbeNonStandardAccess.UseStandardPrice = false;
        insert pbeNonStandardAccess;
        priceBookEntryAccessId1 = pbeNonStandardAccess.Id;

        PricebookEntry pbeInstallStandard = new PricebookEntry();
        pbeInstallStandard.Pricebook2Id = sp1.id;
        pbeInstallStandard.UnitPrice = 50;
        pbeInstallStandard.Product2Id = pInstall.id;
        pbeInstallStandard.IsActive = true;
        pbeInstallStandard.UseStandardPrice = false;
        insert pbeInstallStandard;
        
        PricebookEntry pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 50;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
        priceBookEntryInstallId1 = pbeNonStandardInstall.Id;
                       
       QuoteLineItem qliInstall1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
       insert qliInstall1;
       QuoteLineItem qliAccess1 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryAccessId1, Site__c=site1Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Install__c = qliInstall1.Id);      
       insert qliAccess1;
       qli1Id = qliAccess1.Id;
       qliTest = qliAccess1;
       
       QuoteLineItem qliInstall2 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryInstallId1, Site__c=site2Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = false);                       
       insert qliInstall2;
       QuoteLineItem qliAccess2 = new QuoteLineItem(QuoteId = q.Id, PricebookEntryId = priceBookEntryAccessId1, Site__c=site2Id, Quantity=1, UnitPrice = 10, Visible_Quote_Line_Item__c = true, Quote_Line_Item_Install__c = qliInstall1.Id);      
       insert qliAccess2;
       qli2Id = qliAccess2.Id;
       
       
        /* Products for a Second Term */
        // Install Product
        pInstall  = new Product2();
        pInstall.IsActive = true;
        pInstall.Name = 'Fibre';
        pInstall.Charge_Type__c = 'NRC';
        pInstall.Access_Type__c = 'Ethernet EON';
        pInstall.Access_Type_Group__c = 'Fibre';
        pInstall.Category__c = 'Install';
        pInstall.Service_Type__c = '10 Mbps - Ethernet Fibre Network Install - Test';
        pInstall.RecordTypeId = mapRTp.get('Enterprise Products');
        pInstall.Service_Term__c = '36-Months';
        pInstall.Mark_Up_Factor__c = 1.0;
        pInstall.Start_Date__c = Date.today();
        pInstall.End_Date__c = Date.today()+1;
        insert pInstall;
        
        // Access Product
        pAccess  = new Product2();
        pAccess.IsActive = true;
        pAccess.Name = 'Fibre';
        pAccess.Charge_Type__c = 'MRC';
        pAccess.Access_Type__c = 'Ethernet EON';
        pAccess.Access_Type_Group__c = 'Fibre';
        pAccess.Category__c = 'Access';
        pAccess.Service_Type__c = '10 Mbps - Ethernet Fibre Network Access - Test';
        pAccess.RecordTypeId = mapRTp.get('Enterprise Products');
        pAccess.Service_Term__c = '36-Months';
        pAccess.Product_Install_Link__c = pInstall.Id;
        pAccess.Mark_Up_Factor__c = 1.0;
        pAccess.Start_Date__c = Date.today();
        pAccess.End_Date__c = Date.today()+1;
        insert pAccess;
        
        pbeAccessStandard = new PricebookEntry();
        pbeAccessStandard.Pricebook2Id = sp1.id;
        pbeAccessStandard.UnitPrice = 160;
        pbeAccessStandard.Product2Id = pAccess.id;
        pbeAccessStandard.IsActive = true;
        pbeAccessStandard.UseStandardPrice = false;
        insert pbeAccessStandard;
        
        pbeNonStandardAccess = new PricebookEntry();
        pbeNonStandardAccess.Pricebook2Id = sp.id;
        pbeNonStandardAccess.UnitPrice = 160;
        pbeNonStandardAccess.Product2Id = pAccess.id;
        pbeNonStandardAccess.IsActive = true;
        pbeNonStandardAccess.UseStandardPrice = false;
        insert pbeNonStandardAccess;

        pbeInstallStandard = new PricebookEntry();
        pbeInstallStandard.Pricebook2Id = sp1.id;
        pbeInstallStandard.UnitPrice = 150;
        pbeInstallStandard.Product2Id = pInstall.id;
        pbeInstallStandard.IsActive = true;
        pbeInstallStandard.UseStandardPrice = false;
        insert pbeInstallStandard;
        
        pbeNonStandardInstall = new PricebookEntry();
        pbeNonStandardInstall.Pricebook2Id = sp.id;
        pbeNonStandardInstall.UnitPrice = 150;
        pbeNonStandardInstall.Product2Id = pInstall.id;
        pbeNonStandardInstall.IsActive = true;
        pbeNonStandardInstall.UseStandardPrice = false;
        insert pbeNonStandardInstall;
    }

static testMethod void testLocations() {
         Profile pEnterprise = [select id from profile where name='Rogers Enterprise Sales Manager'];
        Profile pNSA = [select id from profile where name='EBU - Rogers NSA'];
        
         User uNSAManager = new User(alias = 'stand3', email='standarduserEnterprise3@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise3', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pNSA.Id, 
         timezonesidkey='America/Los_Angeles', username='rbsEnterprise3User@testorg.com');
        insert uNSAManager;
        
         User uNSA = new User(alias = 'stand2', email='standarduserEnterprise2@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise2', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pNSA.Id, ManagerId = uNSAManager.Id,
         timezonesidkey='America/Los_Angeles', username='rbsEnterprise2User@testorg.com');
         insert uNSA;
        
        User uSales = new User(alias = 'stand1', email='standarduserEnterprise@testorg.com',
         emailencodingkey='UTF-8', lastname='Enterprise', languagelocalekey='en_US',
         localesidkey='en_US', profileid = pEnterprise.Id, Network_Solutions_Architect__c = uNSA.Id,
         timezonesidkey='America/Los_Angeles', username='rbsEnterpriseUser@testorg.com');
         insert uSales;
         
         
        Id qrId = OpportunityWebServices.createQuoteRequest('' + q.Id, '' + uNSA.Id);
         Quote_Request__c qr = [SELECT id FROM Quote_Request__c WHERE id = :qrId];
         
         
         Carrier_Quote__c carrierQuote = new Carrier_Quote__c();
            carrierQuote.Quote_Request__c = qrId;
            carrierQuote.Carrier_Name__c = 'Bell';
            INSERT carrierQuote;
            
         Carrier_Quote_Information__c cri = new Carrier_Quote_Information__c();
         cri.Carrier_Quote__c = carrierQuote.Id;
         cri.Build_Cost__c = 500;
         cri.Carrier_Name__c = 'Bell';
         cri.Site__c = site1Id;
         INSERT cri;   
        
        ServiceRequestInfo__c sInfo = new ServiceRequestInfo__c();
        sInfo.Diversity__c = 'test';
        insert sInfo;
        
        
        QuoteRequestService__c qs = new QuoteRequestService__c();
        qs.Quote_Request__c = qrId;
        qs.Service__c = qli1Id;
        qs.ServiceRequestInfo__c = sInfo.Id;
        insert qs;
        
        
        ServiceRequestInfo__c sInfoZ = new ServiceRequestInfo__c();
        sInfoZ.Diversity__c = 'test';
        insert sInfoZ;
        
        
        QuoteRequestService__c qsZ = new QuoteRequestService__c();
        qsZ.Quote_Request__c = qrId;
        qsZ.Service__c = qli2Id;
        qsZ.ServiceRequestInfo__c = sInfoZ.Id;
        insert qsZ;
        
        
        PageReference pageRef = New PageReference('/apex/SiteServiceSummaryController?quoteRequestId=' + String.valueOf(qrId)); 
        Test.setCurrentPage(pageRef);
        ApexPages.StandardController sc = new ApexPages.standardController(qr);
        SiteServiceSummaryController controller = new SiteServiceSummaryController(sc);
        
      
        
        Test.startTest();
        controller.previewPDF();
        SiteServiceSummaryController.ServiceWrapper temp = new SiteServiceSummaryController.ServiceWrapper();
        temp.gethasInfo();
        temp.getIsActive();
        temp.getIsDisabled();
        temp.sethasInfo(true);
        temp.setIsActive(true);
        temp.setIsDisabled(true);
        
        new SiteServiceSummaryController.ServiceWrapper(qliTest);
        new SiteServiceSummaryController.ServiceWrapper(qliTest, false, false);
        new SiteServiceSummaryController.ServiceWrapper(qliTest, false, false, false);
        Test.stopTest();
        
    }
}