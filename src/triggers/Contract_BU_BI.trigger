/*    Feb 6, 2015 Paul Saini
      Request: Delete all contract custom fields from OrgDv1.
               Removed reference to all contract custom fields.   
*/
trigger Contract_BU_BI on Contract (before insert, before update) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='Contract' and isActive=true];
        
    //Create a map between the Record Type Name and Id for easy retrieval
    Map<String,String> contractRecordTypes = new Map<String,String>{};
    for(RecordType rt: rtypes)
        contractRecordTypes.put(rt.Name,rt.Id);
    
    if (Trigger.isInsert){
        for (Contract c : Trigger.New){
            // if this is a renewal and an insert, it will become the new current Contract on the Account
            //if (c.RecordTypeId == contractRecordTypes.get('Enterprise Contract Renewal'))
                //c.Current_Contract_Flag__c = true;
        }
    }else{
        // It's an update and we want to ensure that if this is an Enterprise Contract Renewal and the
        // no existing contract flag is not selected and there are no selected contracts we will display an error.
        /*
        for (Contract c : Trigger.New){
            if (c.RecordTypeId == contractRecordTypes.get('Enterprise Contract Renewal')){
                if (!c.No_Existing_Contract__c && c.CreatedDate > date.newinstance(2013, 8, 28)){
                    Integer numOfContracts = [SELECT count() FROM Selected_Contract__c WHERE Parent_Contract__c = :c.Id];
                    
                    if (numOfContracts == 0){
                        c.No_Existing_Contract__c.addError('You must ensure that you select an existing contract or identify this contract as not having one in Salesforce.');
                    }
                }
            }
        }
        */
    }
   } 
}