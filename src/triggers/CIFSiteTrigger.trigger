trigger CIFSiteTrigger on CIF_Site__c (before update, before insert) { 
	 /* Create a map of Record Types for the CIFSite so we can use it to decide which Record type to assign the CIF */
if(Label.isTriggerActive.equalsIgnoreCase('true')){
	List <RecordType> cifSiteRecordTypes = new List <RecordType> ([Select Id, recordtype.Name from RecordType where recordtype.SobjectType = 'CIF_Site__c']);
    Map <Id, String> mapRTcifSite = new Map <Id, String> ();
        
   	for (RecordType rt : cifSiteRecordTypes){
    	mapRTcifSite.put(rt.id, rt.Name);  
    }
	
	
	
	for (CIF_Site__c cifSite : Trigger.new){
		if (Trigger.isUpdate){
			List<String> schedAs = new List<String>();
			List<String> tempSchedAs = new List<String>();
			List<String> delScheduleAs = new List<String>();
			List<String> newScheduleAs = new List<String>();
			
			schedAs = Utils.parseList(cifSite.Schedule_A_List__c);
			
			if (!Utils.isEmpty(cifSite.Delete_Schedule_A__c)){
				delScheduleAs = Utils.removeSpacesFromList(cifSite.Delete_Schedule_A__c.split(','));
				Boolean valid = Utils.validateNumericList(delScheduleAs, schedAs.size());
				if (!valid || schedAs.size() == 0)
					cifSite.addError('Deleted Schedule A values must be numeric and within the range specified by the list, separated only by spaces.'); 
										
				for (Integer i = 0; i<SchedAs.size(); ++i){	
					if (!Utils.listContains(delScheduleAs, '' + (i+1))){
						tempSchedAs.add(schedAs[i]);
					}
				}
			}else{
				tempSchedAs = schedAs;
			}
			
			// Add New Schedule As
			if (!Utils.isEmpty(cifSite.New_Schedule_A__c)){
				newScheduleAs = cifSite.New_Schedule_A__c.split(',');
				for(String schedA : newScheduleAs){
					tempSchedAs.add(schedA.trim());
				}
			}
			
			// Update the Schedule A List field on the CIF Site
			cifSite.Schedule_A_List__c = ''; 
			for (Integer i = 0; i<tempSchedAs.size(); ++i){
				cifSite.Schedule_A_List__c += (i+1) + '. ' + tempSchedAs[i] + '\n';
			}
			
			// Clear the new and delete Schedule A
			cifSite.New_Schedule_A__c = '';
			cifSite.Delete_Schedule_A__c = '';
		}
		System.debug('\n\n\n\n\nBefore');
		if (mapRTcifSite.get(cifSite.RecordTypeId).startsWith('Enterprise')){
			System.debug('\n\n\n\n\nInside');
			cifSite.Building_Entry_Address_FreeText__c = cifSite.Building_Entry_Address__c;
			cifSite.Building_Entry_Contact_Email_FreeText__c = cifSite.Building_Entry_Contact_Email__c;
			cifSite.Building_Entry_Contact_JobTitle_FreeText__c = cifSite.Building_Entry_Contact_Job_Title__c;
			cifSite.Building_Entry_Contact_Mobile_FreeText__c = cifSite.Building_Entry_Contact_Mobile__c;
			cifSite.Building_Entry_Contact_Phone_FreeText__c = cifSite.Building_Entry_Contact_Phone__c; 
			cifSite.Building_Entry_Contact_FreeText__c = cifSite.Building_Entry_Contact_Name__c;
			
			cifSite.Property_Mgmt_Address_FreeText__c = cifSite.Property_Mgmt_Address__c;
			cifSite.Property_Mgmt_Email_FreeText__c = cifSite.Property_Mgmt_Email__c;
			cifSite.Property_Mgmt_Job_Title_FreeText__c = cifSite.Property_Mgmt_Job_Title__c;
			cifSite.Property_Mgmt_Mobile_FreeText__c = cifSite.Property_Mgmt_Mobile__c;
			cifSite.Property_Mgmt_Phone_FreeText__c = cifSite.Property_Mgmt_Phone__c;
			cifSite.Property_Mgmt_Contact_FreeText__c = cifSite.Property_Mgmt_Contact_Name__c;
			
			System.debug('FreeText: ' + cifSite.Building_Entry_Contact_FreeText__c);
			System.debug('NonFreeText: ' + cifSite.Building_Entry_Contact__r.Name); 
		}
		
	}
  }
}