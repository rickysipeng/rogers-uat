/* Date Created : March 25, 2014
    Created By : Stuart Massey, Rogers Data Centres
    Description : Moves the Expected_Revenue_Start_Date of the edited Opportunity ahead by 60 days of the CloseDate. 
              Does not execute if the Close Date has not been changed, the new Close Date value is less then the new Expected Revenue Start Date value or
              the value has been deleted (set to null) by the user.
Jan/27/2015 Paul Saini    Edit trigger
    Function: Update trigegr such that it should only fire if the Opportunity.Business_Unit__c = 'Data Centre'
    Check for condition if  Opportunity.Business_Unit__c is not populated.
    Put a if block around processing and only process records when Opportunity.Business_Unit__c='Data Centre'.
*/
trigger updateCloseDate on Opportunity (before update) {
  if(Label.isTriggerActive.equalsIgnoreCase('true')){
    if (trigger.new[0].CloseDate == trigger.old[0].CloseDate) {
        return;
    }
    
    if (trigger.new[0].CloseDate == NULL) {
         return;
    }  

    if (trigger.new[0].CloseDate <= trigger.new[0].Expected_Revenue_Start_Date__c) {
        return;
    }
     /*  Only do processing when business unit='Data Centre'  
         Current processing looks for first record only. There must be a reason for this.
         Adding check for business unit based on first record check only.     
     */   
     if (trigger.new[0].Business_Unit__c !=null){
         if(trigger.new[0].Business_Unit__c =='Data Centre'){
             trigger.new[0].Expected_Revenue_Start_Date__c = trigger.new[0].CloseDate + 60;
         }        
     }
       
    //trigger.new[0].Expected_Revenue_Start_Date__c = trigger.new[0].CloseDate + 60;
	}
}