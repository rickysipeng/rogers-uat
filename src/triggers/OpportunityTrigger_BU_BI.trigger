/* Jan/27/2015 Paul Saini    Edit trigger
    Function: Update trigegr such that it should only fire if the Opportunity.Business_Unit__c = 'Wireline'
    Check for condition if  Opportunity.Business_Unit__c is not populated.
    Put a if block around processing and only process records when Opportunity.Business_Unit__c='Wireline'.
    
    Feb 17, 2015 Paul Saini, Comment trigger
    As per new requirement object [OpportunityStageHistory__c] is deleted from orgDv1.
    This trigger will need to be revised. Commentign whole trigger for now.
*/

trigger OpportunityTrigger_BU_BI on Opportunity (before insert, before update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){
    Map<Id,Opportunity> newOppMap = Trigger.newMap;
    Map<Id,Opportunity> oldOppMap = Trigger.oldMap;
    
    Set<Id> parentIds = new Set<Id>();
    
    Custom_Property__c property;
    Decimal probability;
    Set<id> carAccts = new Set<Id>();
    if(Recursion_blocker.flag){   
        Recursion_blocker.flag = false;   
    if (Trigger.isInsert){
        try{
                property = [SELECT Id, Value__c FROM Custom_Property__c WHERE Name = 'LEAD CONVERSION PROB' LIMIT 1];
                               
                if (property != null)
                    try{
                        probability = Double.valueOf(property.Value__c);
                    }catch(Exception ex){
                        probability = 10;
                    } 
                else
                     probability = 10;
            }catch(Exception ex){
                probability = 10;
            }
    }

    for (Opportunity o1 : Trigger.New){
        parentIds.add(o1.parent_opportunity__c);
    }

    Map<Id, Opportunity> oppMap = new Map<Id, Opportunity>([SELECT Id, Name, Contact__c, isAvailableAsParent__c FROM Opportunity WHERE Id IN :parentIds AND isAvailableAsParent__c = 1 AND Business_Unit__c='Wireline']);
    // PS: Added filter for Business_Unit__c='Wireline'

    Set<Id> oppsWithQuote = new Set<Id>(); 
    
    if (Trigger.newMap != null){
    list<Quote> newQuote = new list<Quote>([SELECT id, OpportunityId FROM Quote WHERE OpportunityId IN :Trigger.newMap.keySet() AND Opportunity.Business_Unit__c = 'Wireline']);
        for ( Quote q : newQuote ) // change made by Aakanksha (18 Mar 2015): created list newQuote to avoid soql query in for loop
        {
            oppsWithQuote.add(q.OpportunityId);
        }
    }
    Set<Id> accAllIds = new Set<Id>();
    Set<Id> accts = new Set<Id>();
    //List<OpportunityStageHistory__c> newStageHistory = new List<OpportunityStageHistory__c>();
    for (Opportunity opp : Trigger.New){
         // skip records where business unit is not populated
        if (opp.Business_Unit__c == null) continue;
        
        // skip records where business unit != 'Wireline'
        if (opp.Business_Unit__c != 'Wireline') continue;   
        accts.add(opp.AccountId);
        
        if (Trigger.isInsert){
            if (!Utils.isEmpty(opp.LeadSource))
                opp.Probability = probability ;

           // if (opp.Account_Record_Type__c != null && opp.Account_Record_Type__c.contains('Carrier')){
                carAccts.add(opp.AccountId);
         //   }
        
        }
        accAllIds.addAll(accts);
        accAllIds.addAll(carAccts);
        
        if (Trigger.isUpdate){
            Opportunity myNewOpp = newOppMap.get(opp.Id);
            Opportunity myOldOpp = oldOppMap.get(opp.Id);
                
            if (opp.Account_Record_Type__c != null && !opp.Account_Record_Type__c.contains('Carrier') && (myNewOpp.isWON != myOldOpp.isWON) && opp.isWon && opp.SyncedQuoteId == null && oppsWithQuote.contains(opp.Id)){
                opp.stageName.addError('An Opportunity with quotes must have at least one of its quotes synced before setting Stage as Contracted/Won.');
            }
            
            /* Opportunity Stage History Tracking 
            
            String stageHistoryPrev = myOldOpp.StageName;
            String stageHistoryCurrent = myNewOpp.StageName;
            
            if (stageHistoryPrev != stageHistoryCurrent){
                OpportunityStageHistory__c oppStageHistory = new OpportunityStageHistory__c();
                oppStageHistory.Current_Stage__c = stageHistoryCurrent;
                oppStageHistory.Previous_Stage__c = stageHistoryPrev;
                oppStageHistory.Opportunity__c = opp.Id;
                oppStageHistory.Opportunity_Owner__c = opp.OwnerId;
                
                if ('Suspect'.equalsIgnoreCase(stageHistoryPrev) && 'Prospecting'.equalsIgnoreCase(stageHistoryCurrent)){
                    oppStageHistory.Correct_Flow__c = true;
                } else if ('Prospecting'.equalsIgnoreCase(stageHistoryPrev) && 'Quoted'.equalsIgnoreCase(stageHistoryCurrent)){
                    oppStageHistory.Correct_Flow__c = true;
                } else if ('Quoted'.equalsIgnoreCase(stageHistoryPrev) && 'Negotiating'.equalsIgnoreCase(stageHistoryCurrent)){
                    oppStageHistory.Correct_Flow__c = true;
                } else if ('Negotiating'.equalsIgnoreCase(stageHistoryPrev) && 'Committed'.equalsIgnoreCase(stageHistoryCurrent)){
                    oppStageHistory.Correct_Flow__c = true;
                } else if ('Committed'.equalsIgnoreCase(stageHistoryPrev) && 'Contracted'.equalsIgnoreCase(stageHistoryCurrent)){
                    oppStageHistory.Correct_Flow__c = true;
                } else{
                    oppStageHistory.InCorrect_Flow__c = true;
                }
                newStageHistory.add(oppStageHistory);
            }*/
            
            
            
            
        }else{
            if (opp.Account_Record_Type__c != null && !opp.Account_Record_Type__c.contains('Carrier') && opp.isWon && opp.SyncedQuoteId == null && oppsWithQuote.contains(opp.Id)){
                opp.stageName.addError('An Opportunity with quotes must have at least one of its quotes synced before setting Stage as Contracted/Won.');
            }
        }
        
    }
    List<Account > acctListAll = new List<Account>([Select Id, Pricebook__c,Total_MRR__c From Account WHERE id = :accAllIds]);
    if (Trigger.isInsert){
         // Set the Default Carrier pricebook to use.
         //Map<Id, Account> acctMap = new Map<Id, Account>([Select Id, Pricebook__c From Account WHERE id = :carAccts]);
         Map<Id, Account> acctMap = new Map<Id, Account>();
         for(Account a :acctListAll){
            if(carAccts.contains(a.Id))
                acctMap.put(a.id,a);
         }
         system.debug('acctMap***'+acctMap);
         for (Opportunity oppNew : Trigger.New){
            Account relatedAcc = acctMap.get(oppNew.AccountId);
            if (oppNew.Business_Unit__c=='WireLine' && relatedAcc!= null && (relatedAcc.Pricebook__c!=null || relatedAcc.Pricebook__c!='')){
               
                oppNew.Pricebook__c = relatedAcc.Pricebook__c;
            }
         }
    } 
    
   /* if (newStageHistory.size()>0)
        INSERT newStageHistory;*/
      
    
    List<Id> acctList = new List<Id>(accts);
    
   Map<Id, Account> accountMap = new Map<Id, Account> ();//[SELECT Id, Total_MRR__c FROM Account where Id IN :acctList]
    
    List<RecordType> rtypes = [Select Name, Id From RecordType where sObjectType='Opportunity' and isActive=true];
     
    //Create a map between the Record Type Name and Id for easy retrieval
    Map<String,String> opportunityRecordTypes = new Map<String,String>{};
    for(RecordType rt: rtypes)
        opportunityRecordTypes.put(rt.Name,rt.Id);
    
    for (Opportunity o2 : Trigger.New){
         for(Account a :acctListAll){
            if(accts.contains(a.Id))
                accountMap.put(a.id,a);
         }
        if (o2.RecordTypeId == opportunityRecordTypes.get('Enterprise_Contract_Renewal')){
             if (Trigger.isInsert && (o2.Existing_MRR__c == NULL || o2.Existing_MRR__c == 0 ))
                o2.Existing_MRR__c = accountMap.get(o2.AccountId).Total_MRR__c;
        }
        
        if (o2.RecordTypeId == opportunityRecordTypes.get('Enterprise_Temp_Service') && !oppMap.containsKey(o2.parent_opportunity__c)){
            o2.parent_opportunity__c.addError('Temporary Service Opportunity must have a parent opportunity that is either a Move or a New Sale.');
        }
        
        if ((oppMap.get(o2.Parent_Opportunity__c)!=null) && o2.RecordTypeId == opportunityRecordTypes.get('Enterprise_Temp_Service')){
            if (Trigger.isInsert)
                    o2.Contact__c = oppMap.get(o2.Parent_Opportunity__c).Contact__c;
            o2.Name = oppMap.get(o2.Parent_Opportunity__c).Name;
        }
    }
    }
  }
}