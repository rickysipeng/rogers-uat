trigger AZ_Site_Trigger_AD on A_Z_Site__c (before delete) {
         if(Label.isTriggerActive.equalsIgnoreCase('true')){
    List<Id> aSiteIds = new List<Id>();
    List<Id> zSiteIds = new List<Id>();
    Map<Id, String> azSiteMap = new Map<Id, String>();
    List<Id> azSiteIds = new List<Id>();
    
    for (A_Z_Site__c az : Trigger.Old){
        aSiteIds.add(az.Site_A__c);
        azSiteIds.add(az.Id);
    }
    
    System.debug('AZ Site Ids: ' + azSiteIds);
    System.debug('A Site Ids: ' + aSiteIds);
    
    List<Site__c> aSites = [SELECT Id, Z_Sites__c FROM Site__c WHERE Id IN :aSiteIds];
    List<A_Z_Site__c> azSites = [SELECT Site_A__c, Site_Z__c, Site_Z__r.Display_Name__c FROM A_Z_Site__c WHERE Id NOT IN :azSiteIds AND Site_A__c IN :aSiteIds];
    
    System.debug('AZ Sites: ' + azSites);
    System.debug('A Sites: ' + aSites);
    
    for (A_Z_Site__c az : azSites){
        String zSiteList = azSiteMap.get(az.Site_A__c);
        if (Utils.isEmpty(zSiteList)){
            zSiteList = '';
        }
        
        zSiteList+= (az.Site_Z__r.Display_Name__c + '\n');
        azSiteMap.put(az.Site_A__c, zSiteList);
    }
    
    for (Site__c a : aSites){
        if (azSiteMap.get(a.Id) != null)
            a.Z_Sites__c = azSiteMap.get(a.Id);
        else
            a.Z_Sites__c = '';
    }
    System.debug('A Sites: ' + aSites);
    
    UPDATE aSites;
    
    System.debug('A Sites: ' + aSites);
    }
    
}