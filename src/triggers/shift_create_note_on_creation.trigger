trigger shift_create_note_on_creation on SPR__c (before insert, after insert) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /* Additon of Notes field on the SPR object which, on SPR record INSERT will create a new SPR Note containing content form the field. 
    Trigger will create SPR and add Notes content. Trigger should then change SPR RT to page layout that supresses Notes field and exposes Requested By, Owner.
    Create by: Jocsan Diaz on 2012-09-27
    */
    
    if (trigger.isBefore) { // change SPR record type
        // get SPR record types. the code expect 2 and only 2 active record types
        set<RecordType> theRecordTypeSet = new set<RecordType>([select r.Name, r.Id from RecordType r where r.SobjectType = 'SPR__c' and r.IsActive = true]);
        set<Id> theRecordTypeIdSet = new set<Id>();
        for (RecordType rt : theRecordTypeSet) {
            theRecordTypeIdSet.add(rt.Id);
        }
        system.debug(theRecordTypeIdSet);
        
        // all new SPR record should have one particular record type. getting the firt record record type should be ok.
        theRecordTypeIdSet.remove(trigger.new.get(0).RecordTypeId);
        system.debug(theRecordTypeIdSet);
        
        // change record type to the other record type, the one hiding the SPR note field.
        if (theRecordTypeIdSet.size() == 1) {
            list<Id> theRecordTypeList = new list<Id>();
            theRecordTypeList.addAll(theRecordTypeIdSet);
            system.debug(theRecordTypeList.get(0));
            
            for (SPR__c spr : trigger.new) {
                spr.RecordTypeId = theRecordTypeList.get(0);
            }
        }
    } else { // create SPR notes
        list<SPR_Note__c> SPRNoteList = new list<SPR_Note__c>();
        
        // loop through SPR list and create SPR notes
        for (SPR__c spr : trigger.new) {
            // don't create SPR note if SPR Notes field is blank
            if (spr.Request_Detail__c == null) continue;
        
            SPR_Note__c sprn = new SPR_Note__c();
            sprn.Body__c = spr.Request_Detail__c;
            sprn.SPR__c = spr.Id;
            
            // add new SPR note to the list
            SPRNoteList.add(sprn);
        }
        
        // insert list
        insert SPRNoteList;
    }
  }
}