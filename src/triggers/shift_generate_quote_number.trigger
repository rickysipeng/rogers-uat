trigger shift_generate_quote_number on Quote__c (before insert, before update) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /*
        Auto-generate quote number based on the opportunity number. The code append a number at the end of the opportunity number as quote number to better
        control quote versioning.
        June 14th, 2012
    */
      if(Recursion_blocker.flag){   
        Recursion_blocker.flag = false; 
    // get opportunity ids
    set<Id> theOpportunityIdList = new set<Id>();
    for (Quote__c q : trigger.new) {
        if (q.Opportunity_Name__c != null) theOpportunityIdList.add(q.Opportunity_Name__c);
    }

    // exit trigger if reaching limit
    if (Limits.getLimitQueries() - Limits.getQueries() < 50) return;

    // get number of quotes
    map<Id, Integer> theQuoteNumberMap = new map<Id, Integer>();
    list<Quote__c> theQuoteList = new list<Quote__c>([SELECT Opportunity_Name__c, Name FROM Quote__c where Opportunity_Name__c in: theOpportunityIdList]);
    system.debug(theQuoteList);
    for (Quote__c q : theQuoteList) {
        
        // get the integer position
        Integer temp = 0;
        try {
            temp = Integer.valueOf(q.Name.split('-')[2]);
        } catch (Exception e) {
            // if exception then the quote number doesn't follow standard format
            continue;
        }
        
        // get the latest quote version
        if (!theQuoteNumberMap.containsKey(q.Opportunity_Name__c)) {
            theQuoteNumberMap.put(q.Opportunity_Name__c, temp);
        } else {
            if (temp > theQuoteNumberMap.get(q.Opportunity_Name__c)) theQuoteNumberMap.put(q.Opportunity_Name__c, temp);
        }
    }   
    system.debug(theQuoteNumberMap);

    // get opportunity map
    map<Id, Opportunity> theOpportunityMap = new map<Id, Opportunity>([select Id, Opportunity_Number2__c from Opportunity where Id in: theOpportunityIdList]);

    // creating a new quote
    if (trigger.isInsert) {
        for (Quote__c q : trigger.new) {
            // check whether the opportunity has any quote associated to
            if (!theQuoteNumberMap.containsKey(q.Opportunity_Name__c)) theQuoteNumberMap.put(q.Opportunity_Name__c, 0);
            
            // calculate quote version
            Integer theVersion = theQuoteNumberMap.get(q.Opportunity_Name__c) + 1;
            system.debug(theVersion);   
                    
            q.Name = theOpportunityMap.get(q.Opportunity_Name__c).Opportunity_Number2__c + '-' + theVersion;
        }
    }
    
    // updating an existing quote
    if (trigger.isUpdate) {
        for (Quote__c q : trigger.new) {
            
            // if quote not associated to an opportunity then skip
            if (q.Opportunity_Name__c == null) continue;
            
            // if quote number is blank or quote has been moved to another opportunity recalculate the quote number
            if ((q.Opportunity_Name__c != trigger.oldMap.get(q.Id).Opportunity_Name__c) && (q.Opportunity_Name__c != null)) { // if opportunity lookup changes then update the quote number
                // check whether the opportunity has any quote associated to
                if (!theQuoteNumberMap.containsKey(q.Opportunity_Name__c)) theQuoteNumberMap.put(q.Opportunity_Name__c, 0);
                
                // calculate quote version
                Integer theVersion = theQuoteNumberMap.get(q.Opportunity_Name__c) + 1;
                system.debug(theVersion);
                
                q.Name = theOpportunityMap.get(q.Opportunity_Name__c).Opportunity_Number2__c + '-' + theVersion;              
            } else if (q.Name != trigger.oldMap.get(q.Id).Name) {
                q.Name = trigger.oldMap.get(q.Id).Name;
            }
        }
    }
      }
  }
}