trigger shift_set_primary_quote on Quote__c (after update) {
  if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /*** ensure only one quote is set as primary ***/
    
    // get all quote opportunity ids
    set<Id> theOpportunityIdSet = new set<Id>();
    map<Id, Id> theOpportunityIdToQuoteIdMapPrimary = new map<Id, Id>();
    map<Id, Id> theOpportunityIdToQuoteIdMapNotPrimary = new map<Id, Id>();
    for (Quote__c q : trigger.new) {
        // just set as primary
        if ((q.Syncing__c == true) && ((q.Syncing__c != trigger.oldMap.get(q.Id).Syncing__c))) {
            theOpportunityIdToQuoteIdMapPrimary.put(q.Opportunity_Name__c, q.Id);
        }
        
        // just set as not primary
        if ((q.Syncing__c == false) && ((q.Syncing__c != trigger.oldMap.get(q.Id).Syncing__c))) {
            theOpportunityIdToQuoteIdMapNotPrimary.put(q.Opportunity_Name__c, q.Id);
        }
    }
    system.debug(theOpportunityIdToQuoteIdMapPrimary);
    system.debug(theOpportunityIdToQuoteIdMapNotPrimary);
    
    if (!theOpportunityIdToQuoteIdMapPrimary.isEmpty() || !theOpportunityIdToQuoteIdMapNotPrimary.isEmpty()) {
        // exit trigger if reaching limit
        if (Limits.getLimitQueries() - Limits.getQueries() < 50) return;
    
        // select opportunity records
        list<Opportunity> theOpportunityList = new list<Opportunity>([
            select Id, Name from Opportunity 
                where Id in: theOpportunityIdToQuoteIdMapPrimary.keySet() or Id in: theOpportunityIdToQuoteIdMapNotPrimary.keySet()]);
        
        // update opportunity with primary quote
        for (Opportunity o : theOpportunityList) {
            // remove primary quote
            if (theOpportunityIdToQuoteIdMapNotPrimary.containsKey(o.Id)) o.Primary_Quote__c = null;            
            
            // flag opportunity with a primary quote
            if (theOpportunityIdToQuoteIdMapPrimary.containsKey(o.Id)) o.Primary_Quote__c = theOpportunityIdToQuoteIdMapPrimary.get(o.Id);
        }
        
        // get all quotes associated to the opportunity
        list<Quote__c> theQuoteList = new list<Quote__c>([select Id, Syncing__c, Unlock_Quote__c from Quote__c where Opportunity_Name__c in: theOpportunityIdToQuoteIdMapPrimary.keySet() and Syncing__c = true]);
        system.debug(theQuoteList);
        
        // set quote to false of true and not the triggering record
        list<Quote__c> theQuote2UpdateList = new list<Quote__c>();
        for (Quote__c q : theQuoteList) {
            // don't touch the trigger quote record
            if (trigger.newMap.containsKey(q.Id)) continue;
            
            // stop quote syncing as only one can be set as primary
            if (q.Syncing__c == true) { 
                q.Syncing__c = false;
                q.Unlock_Quote__c = !q.Unlock_Quote__c;
                theQuote2UpdateList.add(q);
            }    
        }
        system.debug(theQuote2UpdateList);
        
        // update quote list
        try {
            update theQuote2UpdateList;
        } catch (Exception e) {
            system.debug(e);
        }
        
        // update opportunity list
        try {
            update theOpportunityList;
        } catch (Exception e) {
            system.debug(e);
        }       
    }
  }  
}