/* ********************************************************************
 * Name : QuoteTrigger_AU_AI
 * Description : Trigger. After Insert and Before Update Trigger
 * Modification Log
  =====================================================================
 * Ver    Date          Author                  Modification
 ----------------------------------------------------------------------
 * 1.0    5/28/2012     Kevin DesLauriers       Initial Version  
 * 1.1    5/30/2012     Kevin DesLauriers       Update the COP
 * 1.2    10/5/2012     Kevin DesLauriers       send Email to NSA Manager
  
 **********************************************************************/
trigger QuoteTrigger_AU_AI on Quote (after insert, after update) {
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
    // Only run if this trigger hasn't run before
    system.debug('hasRun: ' + ShiftQuoteTrigger_AU_AISupport.hasRun); 
    if (ShiftQuoteTrigger_AU_AISupport.hasRun) return;
    ShiftQuoteTrigger_AU_AISupport.hasRun = true;
    
    List<Id> quoteids = new List<Id>();
    
    if (Trigger.isInsert){
        List<Id> originalQuoteIds = new List<Id>();
        List<Id> parentQuoteIds = new List<Id>();
        for (Quote q : Trigger.New){
            quoteids.add(q.id);
            if (q.Original_Quote__c!=null)
                originalQuoteIds.add(q.Original_Quote__c);
            if (q.parentQuote__c!=null)
                parentQuoteIds.add(q.parentQuote__c);
        }
        Map<Id, Quote> originalQuoteMap;
        Map<Id, Quote> parentQuoteMap;
        
        Set<Quote> originalQuotes = new Set<Quote>();
        if (originalQuoteIds.size()!=null){
            originalQuoteMap = new Map<Id, Quote>([SELECT id, Number_Of_Derived_Quotes__c FROM Quote WHERE Id IN :originalQuoteIds]);
            for (Quote q : Trigger.New){
                Quote originalQuote = originalQuoteMap.get(q.Original_Quote__c);
                if (originalQuote!=null){
                    if (originalQuote.Number_Of_Derived_Quotes__c!=null)
                        originalQuote.Number_Of_Derived_Quotes__c++;
                    else
                        originalQuote.Number_Of_Derived_Quotes__c = 1;
                    originalQuote.Version__c = 1;
                    originalQuotes.add(originalQuote);
                }
            }
        }
        
        Set<Quote> parentQuotes = new Set<Quote>();
        if (parentQuoteIds.size()!=null){
            parentQuoteMap = new Map<Id, Quote>([SELECT id, Number_Of_Derived_Quotes__c FROM Quote WHERE Id IN :parentQuoteIds]);
            for (Quote q : Trigger.New){
                Quote parentQuote = parentQuoteMap.get(q.parentQuote__c);
                Quote originalQuote = originalQuoteMap.get(q.Original_Quote__c);
                
                if (parentQuote!=null && originalQuote != null && (parentQuote.id != originalQuote.id)){
                    if (parentQuote.Number_Of_Derived_Quotes__c!=null)
                        parentQuote.Number_Of_Derived_Quotes__c++;
                    else
                        parentQuote.Number_Of_Derived_Quotes__c = 1;
                    parentQuotes.add(parentQuote);
                }
            }
        }
        if (originalQuotes.size()>0)
            UPDATE new List<Quote>(originalQuotes);
            
        if (parentQuotes.size()>0)
            UPDATE new List<Quote>(parentQuotes);
    }else{  // This is an UPDATE
        if (!Test.isRunningTest()){
            Map<id, Quote> qMap = Trigger.newMap;
            Set<Id> keys = qMap.keySet();   
            List<Quote> quotes = new List<quote>();
            String baseURL = 'https://rogersb2b.my.salesforce.com';
            try{
                quotes = [SELECT Id, Name, Opportunity.Name, Opportunity.Account.Name, Opportunity.Owner.Network_Solutions_Architect__c, Opportunity.Owner.Network_Solutions_Architect__r.DelegatedApproverId, Opportunity.Owner.Network_Solutions_Architect__r.ManagerId, Discount_Approval_Status__c FROM Quote WHERE Discount_Approval_Status__c = 'Submitted to NSA' AND Id IN :keys ];
                baseURL = [Select Id, value__c FROM Custom_Property__c WHERE Name = 'baseURL'].value__c;
            }catch(Exception ex){
            }
            for (Quote q1 : quotes){
               // baseURL = temp.value__c;
                
                Id NSAManager = q1.Opportunity.Owner.Network_Solutions_Architect__r.ManagerId;
                
                if (NSAManager != null){
                    Messaging.SingleEmailMessage mail = new Messaging.SingleEmailMessage();
                    mail.setReplyTo('jose.camacho@rci.rogers.com');
                    mail.setSenderDisplayName('Salesforce Administrator');
                    
                    mail.setSubject('Manager Notification for Quote Approval: ' + q1.Name);
                    String body = '';
                    body += '<p>The following quote needs approval by the NSA: <b>' + q1.Name + '</b></p>';
                    
                    body += '<p>Opportunity: ' + q1.Opportunity.Name + '<br/>';
                    body += 'Account: ' + q1.Opportunity.Account.Name + '<br/></p>';
                    body += '<p>The detail of that Quote can be access at: <a href="'+baseURL+'/'+q1.Id+'">' + q1.Name + '</a>.</p>';
                    mail.setHtmlBody(body);
                    mail.setSaveAsActivity(false);
                    
                    mail.setTargetObjectId(NSAManager);
                    Messaging.sendEmail(new Messaging.SingleEmailMessage[] { mail });
                }
            }
        }
        
        List<String> ids = new List<String>();

        for (Quote q : Trigger.New){
            if ((!q.Add_Line_Item_In_Progress__c) && (q.isSyncing))
                ids.add(q.Id);
        }
       
        if (!Test.isRunningTest())
            if (ids!=null && !ids.isEmpty())
                CIFWebServices.updateCOP(ids);
                
        
    }
    }
}