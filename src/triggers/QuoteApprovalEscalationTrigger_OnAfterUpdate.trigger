trigger QuoteApprovalEscalationTrigger_OnAfterUpdate on Quote (after update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){  
    // Only run if this trigger hasn't run before
    system.debug('hasRun: ' + ShiftQuoteApprovalTriggerSupport.hasRun); 
    if (ShiftQuoteApprovalTriggerSupport.hasRun) return;
    ShiftQuoteApprovalTriggerSupport.hasRun = true;

    list<Quote> quoteToEscalate = new List<Quote>();
    
    List<Pending_Approval_Submission__c> quotesToApprove = new List<Pending_Approval_Submission__c>();
    Set<Id> quoteIds = new Set<Id>();
    for(Quote q :trigger.new){
      quoteIds.add(q.Id);
    }
    
    if (!Test.isRunningTest()){
      List<Pending_Approval_Submission__c> existingApprovals = [SELECT id, Quote_to_Approve__c FROM Pending_Approval_Submission__c WHERE Quote_to_Approve__c IN :quoteIds];
      Set<Id> exitsingQuoteIds = new Set<Id>();
      for (Pending_Approval_Submission__c a : existingApprovals){
        exitsingQuoteIds.add(a.Quote_to_Approve__c);
      }
      
      for(Quote q :trigger.new){
          if(q.Approval_Escalation_Started__c) {    // but was not previously started
              Quote oldQuote = Trigger.oldMap.get(q.id); 
              if (q.Approval_Escalation_Started__c!= oldQuote.Approval_Escalation_Started__c) { 
                  quoteToEscalate.add(q);
              }
          }
      
          if (!exitsingQuoteIds.contains(q.Id) && q.In_Enterprise_Approval__c && !q.Approval_Process_Started__c && !q.Approval_Escalation_Started__c && q.NSA_Approved__c && q.AccountRecordType__c != null && !((q.AccountRecordType__c).toLowerCase().contains('carrier'))){
              if (!exitsingQuoteIds.contains(q.Id)){
                exitsingQuoteIds.add(q.Id);
                Pending_Approval_Submission__c quoteToApprove = new Pending_Approval_Submission__c ();
                quoteToApprove.Next_Approver__c = q.Manager_s_id__c;
                quoteToApprove.Quote_to_Approve__c= q.Id;
                quotesToApprove.add(quoteToApprove);
              }
          }
      
      }
      
      INSERT quotesToApprove;
    }
    
    /* if(quoteToEscalate != null) {
        for(Quote q :quoteToEscalate) {
            list<ProcessInstanceWorkitem> pendingApproval = new List<ProcessInstanceWorkitem>([select Id from ProcessInstanceWorkitem where ProcessInstance.Status = 'Pending' AND ProcessInstance.TargetObjectId = :q.Id limit 1]);
            
            for(ProcessInstanceWorkitem f :pendingApproval) {
                Approval.ProcessWorkItemRequest pwr = new Approval.ProcessWorkItemRequest();

                pwr.setWorkitemId(f.id);
                pwr.setComments('-- AUTOMATED PROCESS: Approval is being removed and sent for Escalation. --');
                pwr.setAction('Reject');    // only system admin users can Remove so we need to reject
             
                Approval.ProcessResult pr = Approval.process(pwr);
            }
            
            Approval.Processsubmitrequest newEscalation = new Approval.ProcessSubmitRequest();
            newEscalation.setComments('-- AUTOMATED PROCESS: Submitting Approval Escalation --');
            newEscalation.setObjectId(q.id);
            newEscalation.setNextApproverIds(new Id[] {});
            if (!Test.isRunningTest()){
                Approval.ProcessResult approvalResult = Approval.process(newEscalation);
            }
            //List<Id> newWorkItemIds = approvalResult.getNewWorkitemIds();
        }
    } */
}
}