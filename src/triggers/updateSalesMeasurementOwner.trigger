trigger updateSalesMeasurementOwner on Opportunity (after update, before insert){
 //   public set<Id> oppIDsNew = new Set<Id>();
    public set<Id> oppIDs = new Set<Id>();
    
    Set<Id> accIds =new Set<Id>();
    List<Case> newCases = new List<Case>();
    Id recordTypeId = [Select Id From RecordType Where SObjectType = 'Case' And DeveloperName='Wireline_Customer_Status_Update'].Id;
    system.debug('****'+Recursion_blocker.FlagOpp_BI_BU);
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    if(Recursion_blocker.FlagOpp_BI_BU!= FALSE){   
        Recursion_blocker.FlagOpp_BI_BU = false;
        system.debug(trigger.isUpdate+'trigger alert');
        if(trigger.isUpdate) {

            for(integer i=0; Trigger.new.size()>i; i++)
            {
                if(Trigger.new[i].OwnerId != Trigger.old[i].OwnerId && Trigger.new[i].Business_Unit__c == 'Wireless'){
                oppIDs.add(Trigger.new[i].id);
                } 
                
                if(Trigger.new[i].OwnerId != Trigger.old[i].OwnerId && Trigger.new[i].Business_Unit__c == 'Wireline' ){
                    accIds.add(Trigger.new[i].AccountId);
                }
                system.debug('****accIds*'+accIds);
               // accIds.add(Trigger.new[i].AccountId);
            } 
            
            //Added By Sagar
            //for(Opportunity o : Trigger.new){
             //   accIds.add(o.AccountId);
            //}
           
            for(integer i = 0; i<Trigger.new.size(); i++){
         //   system.debug('****listopp*'+listopp);
            
                //list<Opportunity> listopp= mapAccOpp.get(Trigger.new[i].AccountId);
                 system.debug('****Trigger.new[i].StageName***'+Trigger.new[i].StageName);
                  system.debug('****Trigger.old[i].StageName***'+Trigger.old[i].StageName);
                   system.debug('****Trigger.new[i].Business_Unit__c***'+Trigger.new[i].Business_Unit__c);
              
                if((Trigger.new[i].StageName !=Trigger.old[i].StageName  ) && Trigger.new[i].StageName =='Closed Won' && Trigger.new[i].Business_Unit__c == 'Wireline'  ){
                   system.debug('*****chk**'+Trigger.new[i].ownerid);
                 //  system.debug();
                    Case newCas= new case();
                    newCas.ownerid= Trigger.new[i].ownerid;
                    newcas.Opportunity__c= Trigger.new[i].id;
                    newCas.Accountid= Trigger.new[i].accountid;
                    newCas.subject= 'Wireline Customer Status Update';
                    newCas.recordTypeId = recordTypeId;
                    newCases.add(newCas);
                }
            
            }
            if(newCases.size()>0){
                Insert newCases;
                for(case caserec: newcases){
                    Approval.ProcessSubmitRequest app = new Approval.ProcessSubmitRequest();
                        app.setObjectId(caserec.id);
                        Approval.ProcessResult result = Approval.process(app);
                    }
            }//Added By Sagar        
        }
    }
    if(trigger.isUpdate && trigger.isAfter ){
   
    List<Sales_Measurement__c> listSalesMeasurement = [select Opportunity__r.ownerid, OwnerId, id from Sales_Measurement__c where Opportunity__c in :oppIDs];
    List<Sales_Measurement__c> listSMUpdate = new List<Sales_Measurement__c>() ;
        for(Sales_Measurement__c sm:listSalesMeasurement){
            if(sm.ownerid != sm.Opportunity__r.ownerid){
                sm.ownerid = sm.Opportunity__r.ownerid;
                listSMUpdate.add(sm);
            }
        }

        if(listSMUpdate.size()>0){
            update listSMUpdate;
        }
    }
    //before insert logic
    if(trigger.isInsert){
        Map<ID,RecordType> rt_Map = New Map<ID,RecordType>([Select ID, Name, DeveloperName From RecordType Where sObjectType = 'Opportunity']);
        map<String, ID> mapPBMapping = new map<String, ID>();
        List<Price_Book_Mapping__c> lstPBMapping = [select Opp_Record_Type_Name__c, Price_Book_Id__c, Price_Book_Name__c from Price_Book_Mapping__c];
        if(lstPBMapping !=null && lstPBMapping.size()>0){
            for(Price_Book_Mapping__c pb :lstPBMapping){
                mapPBMapping.put(pb.Opp_Record_Type_Name__c, pb.Price_Book_Id__c);
            }
        }
        for(Opportunity opp : Trigger.new){
            if(opp.Pricebook2ID == null){
                RecordType rt = rt_Map.get(opp.recordtypeID);
                opp.Pricebook2ID = mapPBMapping.get(rt.DeveloperName);
            }
        }
    }
    }
}