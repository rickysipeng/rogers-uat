/*Trigger Name :checkAccountUpdate
 *Description : This trigger restricts the user for creating an Account update request if there is already an open Account Update request.
 *Created By : Deepika Rawat.
 *Created Date :29/1/2014.
 * Modification Log :
------------------------------------------------------------------------------------------------------------------------------
    Developer                Date                Description
------------------------------------------------------------------------------------------------------------------------------
*
*/
trigger checkAccountUpdate on Case (before insert) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){
    List<Id> lstAccountIds = new List<Id>();
    ID AccUpdateRecordTypeId = [select Id from recordType where name='Account Update' and sObjecttype ='Case'].id;
    ID submittedRecordTypeId = [select Id from recordType where name='Account Update - Submitted' and sObjecttype ='Case'].id;
    for(Case c: Trigger.new){
       lstAccountIds.add(c.AccountId);
    }
    //List of open Account-Update Cases.
    List<Case> lstAccUpdateCase = [select MAL_Updated__c,AccountId, CaseNumber, status, RecordTypeId From Case Where AccountId in:lstAccountIds and (RecordTypeId=: AccUpdateRecordTypeId or RecordTypeId=:submittedRecordTypeId )and MAL_Updated__c = false and status!='Rejected' ];
    Map<Id, List<Case>> mapAccCase = new  Map<Id, List<Case>>();
    //Create Map of AccountId and List of AccountUpdate Case
    for(case c:lstAccUpdateCase){
        List<Case> cList  = mapAccCase.get(c.AccountId);
        if(cList == null) 
            cList = new List<Case>();
            cList.add(c);
            mapAccCase.put(c.AccountId, cList);
    }
    for(Case c: Trigger.new){
        if(c.RecordTypeId == AccUpdateRecordTypeId || c.RecordTypeId == submittedRecordTypeId){
        List<Case> pendingCase = mapAccCase.get(c.AccountId);
        if(pendingCase!=null && pendingCase.size()>0){
            if(!Test.isRunningTest()){
            c.addError(label.AccountUpdateError1+' '+pendingCase[0].CaseNumber+' '+label.AccountUpdateError2);
            }
        }
        else{
        }
        }
    }
  }	
}