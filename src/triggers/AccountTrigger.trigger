trigger AccountTrigger on Account (before delete, after undelete, before update) {
    
    set<ID>             aIds    = new set<ID>();
    set<ID>             OwnerIdsOld    = new set<ID>();
    set<ID>             IdsDistrict = new set<ID>();
    set<ID>             UserIds = new set<ID>();
    set<ID>             AccountwidParentIds = new Set<ID>();
    set<ID>             AccIdsForReplicateClass = new Set<ID>();
    List<String>        apIds   = new List<String>();
    List<ActionPlan__c> deletePermantently_apIds= new List<ActionPlan__c>();
    User UnassignedUser = new User();
    
    //Delete related action plans
    if ( trigger.isdelete ){
        for( Account a : trigger.old ){
            aIds.add( a.Id );
        }
    
        /* GET Action Plans to delete from recycle bin */
        deletePermantently_apIds = [ select Id, Name , LastModifiedDate from ActionPlan__c where Account__c in : aIds and isDeleted = true ALL ROWS ];
        
        if ( deletePermantently_apIds.size() >0 ){          
            Database.emptyRecycleBin(deletePermantently_apIds);
        }
    
        //Get all action plans associated with Accounts
        for( Account a : [Select (Select Id , isDeleted From Action_Plans__r) From Account a where Id in : aIds]){
            if (a.Action_Plans__r.size() >0 ){
                for(ActionPlan__c ap :a.Action_Plans__r ){                  
                    apIds.add(ap.Id);
                }
            }
        }
        if ( apIds.size() >0 ){     
            ActionPlansBatchDelete aPBatch = new ActionPlansBatchDelete(apIds, Userinfo.getUserId());
            Database.ExecuteBatch( aPBatch );
        }
    }
    
    //Undelete related action plans
    if ( trigger.isUnDelete ){
        Database.UndeleteResult[] unDel_errors;
        for( Account a : trigger.new ){
            aIds.add( a.Id );
        }
        list <ActionPlan__c> aPs = [ select Id, Name , LastModifiedDate from ActionPlan__c where Account__c in : aIds and isDeleted = true ALL ROWS ];
        
        try{
            if(ActionPlanObjectTriggerTest.isTest){
                //throw dmlException
                insert new Contact();   
            }
            
            unDel_errors =Database.undelete( aPs,false);
        } catch ( Dmlexception e ){             
            for (Account a: trigger.new){
                a.addError('You can not undelete an action plan whose related object is deleted.');
            }
        }
    }
  system.debug('*********'+trigger.isUpdate + trigger.isbefore);  
  //Update Account as per the Account Status
  if(trigger.isUpdate && trigger.isbefore)  
 {
    //String Initial_Acc_Status;
 //   list<user> userlist = [select id,Name,Channel__c from user where name='Unassigned User' AND IsActive=TRUE limit 1 ];
    list<Assignment_Request__c> listassign = new list<Assignment_Request__c>();
    list<Assignment_Request_Item__c> listassignItem = new list<Assignment_Request_Item__c>();
       
    Team_Assignment_Governance_Settings__c cs = Team_Assignment_Governance_Settings__c.getInstance();
       
       for(Account acc1 : Trigger.old)
       {
            OwnerIdsOld.add( acc1.OwnerId );
            OwnerIdsOld.add(acc1.Initial_Account_Executive__c);
            IdsDistrict.add(acc1.District__c);
            IdsDistrict.add( acc1.Initial_District__c );
       }
       for(Account acc2: Trigger.new)
       {
           UserIds.add(acc2.OwnerId );
           AccountwidParentIds.add(acc2.Id);
       }
       
     
     List<User> listUnassignedUser = [Select id from user where Name  =: cs.Unassigned_User__c limit 1];
     if(listUnassignedUser!=null ){
        UnassignedUser =   listUnassignedUser[0];   
     }   
     
     Map<id, District__c> mapDistrict = new Map<Id, District__c>([select id,District_Owner__c,District_Owner__r.Channel__c,District_Owner__r.Assignment_Approver__c from District__c where id in : IdsDistrict]);  
     Map<id,Account> mapAccwidParent = new Map<id,Account>([Select id, OwnerId, Parent.OwnerId, Parent.Account_Status__c, Account_Status__c from Account where id in:AccountwidParentIds]);
      system.debug('*****map**'+mapAccwidParent);
  
      for(District__c dis : mapDistrict.values())
      {
           OwnerIdsOld.add(dis.District_Owner__c);
      } 
       
     Map<id, User> mapUser = new Map<Id, User>([select id, Channel__c, Assignment_Approver__c from User where id in : ownerIdsOld]);
     Assignment_Request__c a = new Assignment_Request__c();
     Assignment_Request_Item__c aItem = new Assignment_Request_Item__c();
    
    for( integer i=0; i<Trigger.new.size();i++)
    {
      /*  system.debug(Trigger.old[i]);
        system.debug('******oldStatus**'+Trigger.old[i].Account_Status__c);
        system.debug('******newStatus(Unass)***'+Trigger.new[i].Account_Status__c);
        system.debug('******parent(null)**' +Trigger.old[i].ParentID);
        system.debug('******parent(null)**' +Trigger.new[i].ParentID);
        system.debug('******chk value**' +Trigger.old[i]!=null && Trigger.old[i].Account_Status__c!=null && Trigger.old[i].Account_Status__c == 'Pending');
        system.debug('******chk value2(old parent null)**' +Trigger.old[i].ParentID == null);
        system.debug('******chk value2(new acc status not null)' +Trigger.new[i].Account_Status__c!=null);
        system.debug('*****new status(not null)**'+ Trigger.new[i].Account_Status__c == cs.Unassigned_User__c );
      */  
        if(Trigger.old[i]!=null && Trigger.old[i].Account_Status__c!=null && Trigger.old[i].Account_Status__c == 'Pending' &&Trigger.new[i].Account_Status__c!=null && Trigger.new[i].Account_Status__c == cs.Unassigned_Status__c && Trigger.old[i].ParentID == null)
        {
                  a.Request_Type__c='Mixed';
                  a.Approval_Override__c = FALSE;
                  a.Status__c = 'Processed';
                  
                  system.debug('******listassign(null)**' +listassign);
                  listassign.add(a);
                   system.debug('******listassign(a)**' +listassign);
                              
                  aItem.Account__c = Trigger.old[i].ID;
                  aItem.Current_Owner__c=  Trigger.new[i].OwnerId;
                  aItem.Business_Case__c = cs.Business_case__c;
                  aItem.Reason_Code__c= cs.New_Acct_Reason_Code__c;
              
                  Trigger.new[i].OwnerId= UnassignedUser.Id;
                           
                  if( Trigger.old[i].Initial_Account_Executive__c!=null)
                  {
                      if(Trigger.old[i].Initial_Account_Executive__c!=null)
                          aItem.New_Owner__c = Trigger.old[i].Initial_Account_Executive__c;
                      if(mapUser.get(Trigger.old[i].OwnerId)!=null)
                      {                      
                          aItem.New_Approver__c = mapUser.get(Trigger.old[i].Initial_Account_Executive__c).Assignment_Approver__c;
                          aItem.New_Channel__c = mapUser.get(Trigger.old[i].Initial_Account_Executive__c).Channel__c;
                          aItem.Requested_Item_Type__c='Account Owner';
                      }
                  }
                  else
                  {
                    
                      if(Trigger.old[i].Initial_District__c!=null)
                          aItem.New_District__c =Trigger.old[i].Initial_District__c;
                                                             
                      if(mapDistrict.get(Trigger.old[i].Initial_District__c)!=null)
                      {
                          aItem.New_Owner__c = mapDistrict.get(Trigger.old[i].Initial_District__c).District_Owner__c;
                          aItem.New_Approver__c = mapDistrict.get(Trigger.old[i].Initial_District__c).District_Owner__r.Assignment_Approver__c;
                          aItem.New_Channel__c  = mapDistrict.get(Trigger.old[i].Initial_District__c).District_Owner__r.Channel__c;
                         
                          aItem.Requested_Item_Type__c='Account District';
                      }
                      
                   }
                  listassignItem.add(aItem); 
        } 
        else if(Trigger.old[i]!=null && Trigger.new[i]!=null && Trigger.old[i].Account_Status__c!=null &&  Trigger.old[i].Account_Status__c != 'Unassigned' && Trigger.new[i].Account_Status__c == cs.Unassigned_Status__c && Trigger.old[i].ParentID != null)
        {
                Trigger.new[i].OwnerId = mapAccwidParent.get(Trigger.old[i].Id).Parent.OwnerId ;
                Trigger.new[i].Account_Status__c = mapAccwidParent.get(Trigger.old[i].Id).Parent.Account_Status__c;
                AccIdsForReplicateClass.add(Trigger.new[i].id);
        }
              
    }
    
   try
   {
        if(listassign.size()!=NULL)
        insert listassign;
        
   }
   catch (system.Dmlexception e)
   {
        system.debug (e);
   }
    
   for(integer j=0; j<listassign.size(); j++)
   {
       listassignItem[j].Assignment_Request__c=listassign[j].id;
   }
 
   try
   {
        if(listassignItem.size()!=NULL)
        insert listassignItem;
        List<Approval.ProcessSubmitRequest> approvalReqList = new List<Approval.ProcessSubmitRequest>();
       Approval.ProcessSubmitRequest req;
       for(Assignment_Request_Item__c rec:listassignItem){
           req = new Approval.ProcessSubmitRequest();
           req.setComments('Submitted for approval. Please approve.');
           req.setObjectId(rec.Id);
           req.setSubmitterId(userinfo.getuserid());
           approvalReqList.add(req);
       }
       List<Approval.ProcessResult> resultList = Approval.process(approvalReqList); 
   }
   catch (system.Dmlexception e)
   {
        system.debug (e);
   }
   
   //Call updateAccountTeamAccess class to duplicate account team from parent to child
  if(AccIdsForReplicateClass.size()>0 && AccIdsForReplicateClass!=null)
  {
      TAG_SyncAccountTeamMembers accessCls = new TAG_SyncAccountTeamMembers();
      accessCls.addTeamMembersTraversingFromChild(AccIdsForReplicateClass);  
   }
 }
   
     
    
}