trigger ContactTrigger_AU on Contact (after update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){
    Map<Id, Contact> contactMap = Trigger.newMap;
    List<Quote> quoteList = new List<Quote>();
    for(List<Quote> quotes : [SELECT Id, Email, ContactId, Phone, Fax FROM Quote Where ContactId IN :Trigger.newMap.keySet()]){
        for(Quote q : quotes){
            q.Email = contactMap.get(q.ContactId).Email;
            q.Phone = contactMap.get(q.ContactId).Phone;
            q.Fax = contactMap.get(q.ContactId).Fax;
            quoteList.add(q);
        }
    }   
    
    UPDATE quoteList ;
     
     
	}
}