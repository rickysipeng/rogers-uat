/*Trigger Name  : TAG_WireLineCon_ATM_Access
*Description : This Trigger will support the logic for the 
            existing Account Team Members need to be granted access to this new record
*Created By  : Jayavardhan.
*Created Date :02/03/2015.
* Modification Log :
--------------------------------------------------------------------------------------------------------------------------------------------------
                Developer         Version       Date        Description
                Jayavardhan         1.0       02/03/2015    Get list of all Account team members for an account of this new  Wireline_Contract__c
                                                            record and update Access Level based on the Custom setting value Read/Edit
--------------------------------------------------------------------------------------------------------------------------------------------------
*
*/  
Trigger TAG_WireLineCon_ATM_Access on Wireline_Contract__c (after insert) {

    Public Map<ID,Wireline_Contract__c> mapIdContract {get;set;} 
    Set<Id> setAllAccounts = new Set<Id>();
    Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> mapConObjUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Id> mapAccContId = new Map<Id,Id>();
    
    mapIdContract  = new Map<ID,Wireline_Contract__c>();
    // We only execute the trigger after a Job record has been inserted 
    // because we need the Id of the Job record to already exist.
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    if(Trigger.isAfter &&  Trigger.isInsert){
        
        for(integer i=0; Trigger.new.size()>i; i++){
            mapIdContract.put(Trigger.new[i].id,Trigger.new[i]);
            if(Trigger.new[i].Account_Name__c!=null)
            setAllAccounts.add(Trigger.new[i].Account_Name__c);
            mapAccContId.put(Trigger.new[i].Account_Name__c,Trigger.new[i].Id);
        }  
        
        //Cascade Team member Changes to Child Accounts
        Map<Id, List<AccountTeamMember>> mapParentAccTeamMembers = new Map<Id, List<AccountTeamMember>>(); 
        
        List<AccountTeamMember> parentAccMSDMembers = [Select Id, UserId,TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in:setAllAccounts];      
        for(AccountTeamMember teamMem : parentAccMSDMembers){
            List<AccountTeamMember> team = mapParentAccTeamMembers.get(teamMem.Account.Id);
            if(team==null)
            team= new List<AccountTeamMember>();
            team.add(teamMem);
            mapParentAccTeamMembers.put(teamMem.Account.Id,team); 
            Set<Id> userIds = mapAccUserIdsToAdd.get(teamMem.Account.Id);
            If(userIds==null)
            userIds = new Set<Id>();
            userIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.Account.Id,userIds);
            if(mapAccContId.containskey(teamMem.Account.Id))
            mapConObjUserIdsToAdd.put(mapAccContId.get(teamMem.Account.Id),userIds);
        }       
        
        //Call TAG_updateAccountTeamAccessHelper class to Give access from Users after updating AccountTeam
        TAG_updateAccountTeamAccessHelper accessCls = new TAG_updateAccountTeamAccessHelper();                
        accessCls.giveAccesstoContractTeam(mapConObjUserIdsToAdd);
    }
    }
}