trigger activityLogOnEvent on Event (after insert, after update, before delete) {
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
    List<Activity_Log__c> lstActlogFinal = new List<Activity_Log__c>();
    List<Activity_Log__c> lstActlogDelete = new List<Activity_Log__c>();
    List<BIS_Activity_Tracking__c> lstBISupdate= new List<BIS_Activity_Tracking__c>();
    Map<String,Activity_Log__c> mapActivityLog = new Map<String,Activity_Log__c>();
    //List<User> eventUser = new List<User>();
    set<String> newLstBU= new set<String>();
    //get the list of channels coming under BIS users
    for(BIS_Users__c bU:BIS_Users__c.getAll().values()){
        newLstBU.add(bU.Name);  
    }
    
    //map the user with there corresponding channel
    Map<id,String> mapUserChannel= new Map<id,String>();
    
    //EBU Consolidation deployment error fix JK
    List<User> relUsers = new List<User>();
    if(!Test.isRunningTest()){
        relUsers = [select id,Name,Channel__c from User];
    } else {
        relUsers = [select id,Name,Channel__c from User limit 1];
     }

    for(User u:relUsers){
        mapUserChannel.put(u.id,u.channel__c);
    }
    if(trigger.isUpdate || trigger.isInsert){
        List<String> eventIds = new List<String>();
        List<String> eventBISIds = new List<String>();
        for(Event e: Trigger.new){
            //if this is a public calendar then the OwnerId will not point to a User
           // eventUser = [SELECT Id, Name FROM user WHERE id = :e.OwnerId];
            //if a user was returned then we know it's not a public calendar
            if(mapUserChannel.containsKey(e.OwnerId)) {
                if(mapUserChannel.get(e.CreatedById) !=null && newLstBU.Contains(mapUserChannel.get(e.CreatedById))){
                    eventBISIds.add(e.id);    
                }else{
                    eventIds.add(e.id);
                }
            }
        }
            if(eventIds.size() > 0) {
                List<Activity_Log__c> lstActLog = new List<Activity_Log__c>([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where isdeleted=false AND Related_Record__c in: eventIds ]);
                for(Activity_Log__c act:lstActLog){
                    mapActivityLog.put(act.Related_Record__c,act);
                }
                for(integer i=0; i<trigger.new.size();i++){
                   Activity_Log__c act = mapActivityLog.get(trigger.new[i].id);
                   if(act!=null){
                       if(trigger.new[i].OwnerID != trigger.old[i].OwnerID || trigger.new[i].Subject != trigger.old[i].Subject || trigger.new[i].ActivityDate != trigger.old[i].ActivityDate){
                         act.Sales_Rep__c = trigger.new[i].OwnerID; 
                         if(trigger.new[i].Subject != null && trigger.new[i].Subject!='' && trigger.new[i].Subject.length()>=200){
                            act.Subject__c = label.Appointment_1+' '+ trigger.new[i].Subject.substring(0,200);
                         }else{
                             act.Subject__c = label.Appointment_1+' '+ trigger.new[i].Subject;
                         }
                         act.Activity_Date__c = trigger.new[i].ActivityDate;               
                       }
                       lstActlogFinal.add(act);
                   }
                   else{
                      act = new Activity_Log__c();
                      act.Sales_Rep__c = trigger.new[i].OwnerID;
                      if(trigger.new[i].Subject != null && trigger.new[i].Subject!='' && trigger.new[i].Subject.length()>=200){
                            act.Subject__c = label.Appointment_1+' '+ trigger.new[i].Subject.substring(0,200);
                      }else{
                             act.Subject__c = label.Appointment_1+' ' + trigger.new[i].Subject;
                      }
                      act.Activity_Date__c = trigger.new[i].ActivityDate;
                      act.Activity_Type__c = label.Appointment;
                      act.Related_Record__c = trigger.new[i].id;
                      act.RPC_Call__c = false;
                      lstActlogFinal.add(act);
                   }
                }
        }
        // In Case Of BIS Events only In case of Insert
        if(eventBISIds.size() > 0 && trigger.isInsert) {
                
                for(integer i=0; i<trigger.new.size();i++){
                        BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
                        bisActNew.OwnerID = trigger.new[i].OwnerID;
                        bisActNew.Subject__c = label.Appointment_1+' ' + trigger.new[i].Subject;
                        bisActNew.Activity_Date__c =Date.ValueOf(trigger.new[i].ActivityDate);
                        bisActNew.Activity_Type__c = label.Appointment;
                        bisActNew.RPC_Call__c = FALSE;
                        bisActNew.Related_Record__c = trigger.new[i].ID;
                        bisActNew.Sales_Rep_ID_Activity_ID__c =String.ValueOf(trigger.new[i].OwnerID)+String.ValueOf(trigger.new[i].ID);
                        lstBISupdate.add(bisActNew);
                }
        }
        
    }
    if( trigger.isdelete ){
        List<String> eventIds = new List<String>();
        for(Event e: Trigger.old){
            eventIds.add(e.id);
        }
        lstActlogDelete.addAll([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where isdeleted=false AND Related_Record__c in: eventIds ]);
        //List<Activity_Log__c> lstActLog = new List<Activity_Log__c>([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where Related_Record__c in: eventIds ]);
        for(BIS_Activity_Tracking__c bAT:[select Activity_Date__c, Activity_Type__c, RPC_Call__c,Subject__c,Related_Record__c,Note__c from BIS_Activity_Tracking__c where isdeleted=false AND Related_Record__c in: eventIds]){
            bAT.Deleted__c=true;
            bAT.Note__c=label.Event_deleted_by+' '+UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' ' +label.on+' ' + system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
            lstBISupdate.add(bAT);
        }
     }
    if(lstActlogFinal.size()>0){
        upsert lstActlogFinal;
    }  
    if(lstActlogDelete.size()>0){
        delete lstActlogDelete;
    }  
    if(lstBISupdate.size()>0){
        upsert lstBISupdate;
    }       
    }
}