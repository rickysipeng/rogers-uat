trigger Task_FieldUpdateOnAccount_AI_AU on Task (After Insert, After Update) {
if(Label.isTriggerActive.equalsIgnoreCase('true')){
    if (trigger.isAfter && (trigger.isInsert || trigger.isUpdate)){
            task_FieldUpdateOnAccount_AI_AU obj = new task_FieldUpdateOnAccount_AI_AU();
            obj.updateAccountInfo(Trigger.new);
    }
 }
}