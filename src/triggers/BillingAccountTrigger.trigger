trigger BillingAccountTrigger on Billing_Account__c (after insert) {
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
  Set<Id> accountIds = new Set<Id>();
  List<Billing_Account__share> listBAShare = new List<Billing_Account__share>();
  for(Billing_Account__c ba : Trigger.new){
      accountIds.add(ba.Account__c);
  }
  system.debug('accountIds****'+accountIds);
  Map<Id, List<AccountTeamMember>> mapAccountTeam = new Map<Id, List<AccountTeamMember>>();
  
  for(AccountTeamMember mem : [Select AccountId, UserId from AccountTeamMember where AccountId in:accountIds ]){
      List<AccountTeamMember> lstAccTeam = mapAccountTeam.get(mem.AccountId);
      if(lstAccTeam==null)
          lstAccTeam = new List<AccountTeamMember>();
     lstAccTeam.add(mem);  
     mapAccountTeam.put(mem.AccountId, lstAccTeam );   
  }
  system.debug('mapAccountTeam****'+mapAccountTeam);
  if(Trigger.isInsert && Trigger.isAfter){
  for(Billing_Account__c ba : Trigger.new){
    if(ba.Source__c =='Admin 3'){
        List<AccountTeamMember> lstAccTeam = mapAccountTeam.get(ba.Account__c);
        system.debug('lstAccTeam****'+lstAccTeam);
        if(lstAccTeam != null && lstAccTeam.size()>0){
            for(AccountTeamMember mem : lstAccTeam){
                Billing_Account__share baShare = new Billing_Account__share();
                baShare.ParentID = ba.Id;
                baShare.UserOrGroupId = mem.UserId;
                baShare.AccessLevel = 'Read';
                listBAShare.add(baShare);
            }
        }
    }
  }
  
/*
*Description : This Trigger will support the logic for the 
            existing Account Team Members need to be granted access to this new record
*Created By  : Jayavardhan.
*Created Date :02/03/2015.
* Modification Log :
--------------------------------------------------------------------------------------------------------------------------------------------------
                Developer         Version       Date        Description
                Jayavardhan         1.0       02/03/2015    Get list of all Account team members for an account of this new  Billing_Account__c
                                                            record and update Access Level based on the Custom setting value Read/Edit
--------------------------------------------------------------------------------------------------------------------------------------------------
*
*/ 
    Set<Id> setAllAccounts = new Set<Id>();
    Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> mapBillACCUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Id> mapAccContId = new Map<Id,Id>();
    
    
    // We only execute the trigger after a Job record has been inserted 
    // because we need the Id of the Job record to already exist.
    if(Trigger.isAfter &&  Trigger.isInsert){
        
        for(integer i=0; Trigger.new.size()>i; i++){            
            if(Trigger.new[i].Account__c!=null)
            setAllAccounts.add(Trigger.new[i].Account__c);
            mapAccContId.put(Trigger.new[i].Account__c,Trigger.new[i].Id);
        }   
        
        List<AccountTeamMember> parentAccMSDMembers = [Select Id, UserId,TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in:setAllAccounts];      
        for(AccountTeamMember teamMem : parentAccMSDMembers){            
            Set<Id> userIds = mapAccUserIdsToAdd.get(teamMem.Account.Id);
            If(userIds==null)
            userIds = new Set<Id>();
            userIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.Account.Id,userIds);
            if(mapAccContId.containskey(teamMem.Account.Id))
            mapBillACCUserIdsToAdd.put(mapAccContId.get(teamMem.Account.Id),userIds);
        }       
        
        //Call TAG_updateAccountTeamAccessHelper class to Give access from Users after updating AccountTeam
        TAG_updateAccountTeamAccessHelper accessCls = new TAG_updateAccountTeamAccessHelper();                
        accessCls.giveAccesstoBillingAccTeam(mapBillACCUserIdsToAdd);
    }

  
  }
  system.debug('listBAShare****'+listBAShare);
  if(listBAShare.size()> 0){
      insert listBAShare;
  }
  }
}