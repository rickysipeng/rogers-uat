/*Trigger Name  : TAG_ShrMSDACC_ATM_Access
*Description : This Trigger will support the logic for the 
            existing Account Team Members need to be granted access to this new record
*Created By  : Jayavardhan.
*Created Date :02/03/2015.
* Modification Log :
--------------------------------------------------------------------------------------------------------------------------------------------------
                Developer         Version       Date        Description
                Jayavardhan         1.0       02/03/2015    Get list of all Account team members for an account of this new  Shared_MSD_Accounts__c
                                                            record and update Access Level based on the Custom setting value Read/Edit
--------------------------------------------------------------------------------------------------------------------------------------------------
*
*/  
Trigger TAG_ShrMSDACC_ATM_Access on Shared_MSD_Accounts__c (after insert) {
  if(Label.isTriggerActive.equalsIgnoreCase('true')){
    //Set and Map Variables for accounts,SharedMSDAccounts
    Set<Id> setAllAccounts = new Set<Id>();
    Map<Id, Set<Id>> mapAccUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> mapShrMSDAccUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Set<Id>> mapShrMSDCodeUserIdsToAdd = new Map<Id, Set<Id>>();
    Map<Id, Shared_MSD_Accounts__c> mapAccShrMSDCodeObj = new Map<Id,Shared_MSD_Accounts__c>();
    
    
    // We only execute the trigger after a Job record has been inserted 
    // because we need the Id of the Job record to already exist.
    if(Trigger.isAfter &&  Trigger.isInsert){
        
        for(integer i=0; Trigger.new.size()>i; i++){            
            if(Trigger.new[i].Account__c!=null)
            setAllAccounts.add(Trigger.new[i].Account__c);
            mapAccShrMSDCodeObj.put(Trigger.new[i].Account__c,Trigger.new[i]);    
                    system.debug('===============>Trigger.new[i]'+Trigger.new[i]);      
        }  
        
        system.debug('===============>setAllAccounts'+setAllAccounts);
        //To get the Account Team Members for each Shared MSD Accounts record
        List<AccountTeamMember> parentAccMSDMembers = [Select Id, UserId,TeamMemberRole,Account.Id from AccountTeamMember where Account.Id in:setAllAccounts];      
        for(AccountTeamMember teamMem : parentAccMSDMembers){            
            Set<Id> userIds = mapAccUserIdsToAdd.get(teamMem.Account.Id);
            If(userIds==null)
            userIds = new Set<Id>();
            userIds.add(teamMem.UserId);
            mapAccUserIdsToAdd.put(teamMem.Account.Id,userIds);
            if(mapAccShrMSDCodeObj.containskey(teamMem.Account.Id))
            mapShrMSDAccUserIdsToAdd.put(mapAccShrMSDCodeObj.get(teamMem.Account.Id).Id,userIds);
            mapShrMSDCodeUserIdsToAdd.put(mapAccShrMSDCodeObj.get(teamMem.Account.Id).Shared_MSD_Code__c,userIds);
        }       
        
        //Call TAG_updateAccountTeamAccessHelper class to Give access from Users after updating AccountTeam
        TAG_updateAccountTeamAccessHelper accessCls = new TAG_updateAccountTeamAccessHelper();    
        //To give access to Shared MSD Accounts records     
        accessCls.giveAccesstoShrMSDAccTeam(mapShrMSDAccUserIdsToAdd);
        //To give access to Shared MSD Code records     
        accessCls.giveAccesstoShrMSDCodeTeam(mapShrMSDCodeUserIdsToAdd);
    }
  }
}