trigger AccountTrigger_RBS on Account (before insert, before update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){
  Set<id> acctTransferIds = new Set<id>();
  Set<id> acctTransferNoteIds = new Set<id>();
  
  
  Map<Id,Account> newAccountMap = Trigger.newMap;
    Map<Id,Account> oldAccountMap = Trigger.oldMap;
  Map<Id, User> acctOwners = new Map<Id, User>();
  Set<Id> ownerIds = new Set<Id>();
  for (Account acct : Trigger.New){
    if (Utils.isEmpty(acct.Business_Segment__c)){
      acct.Business_Segment__c = 'From Lead Conversion';
    }
    
    ownerIds.add(acct.OwnerId);
    
    /*
    if (Trigger.isUpdate){
    
    if (acct.Account_Transfer_in_Progress__c && 'Approved by Existing Owner\'s Manager'.equals(acct.Account_Transfer_Status__c)){
       acctTransferIds.add(acct.id);  
      
       acct.Previous_Owner__c = acct.OwnerId;
    }else if('Account Transfer Approval Complete'.equals(acct.Account_Transfer_Status__c)){
      Account myNewAccount = newAccountMap.get(acct.id);
            Account myOldAccount = oldAccountMap.get(acct.id);  
      if (myNewAccount.Account_Transfer_Status__c != myOldAccount.Account_Transfer_Status__c){
        acct.OwnerId = acct.Requested_Owner__c;
      }
      
    }
    acctTransferNoteIds.add(acct.id);
    }
    */
    
    
  }
  
  acctOwners = new Map<Id, User>([SELECT id, ManagerId FROM User WHERE id IN :ownerIds]);
  
  for (Account acct : Trigger.New){
    try{
      Id uId = acctOwners.get(acct.OwnerId).ManagerId;
      acct.Existing_Owner_s_Manager__c = uId;
    }catch(Exception ex){
      
    }
  }
  
  /*
    if (Trigger.isUpdate){
      Map<id, Account> acctTransferMap = new Map<id, Account>([SELECT id, Owner.ManagerId, Requested_Owner__r.ManagerId FROM Account WHERE id in :acctTransferIds]);
       for (Account acct : Trigger.New){
         Account a = acctTransferMap.get(acct.id);
         if (a != null){
           if ('Approved by Existing Owner\'s Manager'.equals(acct.Account_Transfer_Status__c) && a.Owner.ManagerId != a.Requested_Owner__r.ManagerId){
             acct.Account_Transfer_Status__c = 'Approved by Existing Owner\'s Manager - Pending Approval';
           }
         }
       }
       
       List<ProcessInstance> approvals = [SELECT Id,  
                                            Status, 
                                            TargetObjectId, 
                                                (SELECT Id, 
                                                    StepStatus, 
                                                    ActorId,
                                                    Actor.Name, 
                                                    OriginalActor.Name, 
                                                    CreatedDate, Comments 
                                                    FROM StepsAndWorkitems Order By CreatedDate DESC) 
                                            FROM ProcessInstance 
                                            WHERE TargetObjectId IN :acctTransferNoteIds Order By LastModifiedDate DESC LIMIT 1];
                
        for (ProcessInstance approval : approvals){
            for (Account a : Trigger.New){
                if (approval != null && a.Id == approval.TargetObjectId && approval.StepsAndWorkitems != null && approval.StepsAndWorkitems.size()>0){
                    a.Recent_Approval_Notes__c  = approval.StepsAndWorkitems[0].Comments;
                }
            }
        }
       
       
    }
   */
    
  
  User currentUser = [SELECT Id, ContactId, Profile.UserType FROM User where Id = :(UserInfo.getUserId()) LIMIT 1];
    Boolean isPartnerUser = false;
    
    if (currentUser!=null){
      isPartnerUser = !Utils.isEmpty(currentUser.ContactId);
    }
  
  if (isPartnerUser){
    Map<Id, UserRole> roles = new Map<Id, UserRole>([SELECT Id, Name FROM UserRole WHERE Name IN ('Strategic Account Manager','Business Sales Executive')]);
    
    Set<String> accountNames = new Set<String>();
    
    for (Account a : Trigger.New){
      accountNames.add(a.Name);
    }
        
    List<Account> accounts = [SELECT Id, Name, Phone, Owner.UserRoleId FROM Account WHERE Owner.UserRoleId IN :roles.keySet() AND Name IN :accountNames];
    
    Set<String> duplicateAccounts = new Set<String>();
    for (Account a : accounts){
      if (accountNames.contains(a.Name)){
        duplicateAccounts.add(a.Name);
      }
    }
    
    for (Account tempAccount : Trigger.New){
      
      if (duplicateAccounts.contains(tempAccount.Name)){
        tempAccount.addError('Account ' + tempAccount.Name + ' currently exists and a duplicate account cannot be recreated.');
      }
    }
  }
  
 } 
}