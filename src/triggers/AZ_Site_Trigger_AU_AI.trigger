trigger AZ_Site_Trigger_AU_AI on A_Z_Site__c (after insert, after update) {
     if(Label.isTriggerActive.equalsIgnoreCase('true')){
    List<Id> aSiteIds = new List<Id>();
    List<Id> zSiteIds = new List<Id>();
    Map<Id, String> azSiteMap = new Map<Id, String>();
    
    for (A_Z_Site__c az : Trigger.New){
        aSiteIds.add(az.Site_A__c);
        zSiteIds.add(az.Site_Z__c);
    }
    
    Map<Id, Site__c> aSiteMap = new Map<Id, Site__c>([SELECT Id, Z_Sites__c FROM Site__c WHERE Id IN :aSiteIds]);
    Map<Id, Site__c> zSiteMap = new Map<Id, Site__c>([SELECT Id, Display_Name__c FROM Site__c WHERE Id IN :zSiteIds]);
    
    
    for (A_Z_Site__c az : Trigger.New){
        String zSiteList = azSiteMap.get(az.Site_A__c);
        if (Utils.isEmpty(zSiteList)){
            if (aSiteMap == null || aSiteMap.get(az.Site_A__c) == null || aSiteMap.get(az.Site_A__c).Z_Sites__c == null)
                zSiteList = '';
            else
                zSiteList = aSiteMap.get(az.Site_A__c).Z_Sites__c  + '\n';
        }
        
        if (zSiteMap != null  && zSiteMap.get(az.Site_Z__c) != null){
            zSiteList += (zSiteMap.get(az.Site_Z__c).Display_Name__c + '\n');
            azSiteMap.put(az.Site_A__c, zSiteList);
        }
    }
    
    Set<Site__c> aSites = new Set<Site__c>();
    for (Site__c a : aSiteMap.values()){
        a.Z_Sites__c = azSiteMap.get(a.Id);
        a.tempField__c = true;
        aSites.add(a);
    }
    
    List<Site__c> siteList = new List<Site__c>(aSites);
    
    UPSERT siteList;
    }
}