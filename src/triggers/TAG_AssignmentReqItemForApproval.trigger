/**************************************************************************************
Apex Trigger Name     : TAG_AssignmentReqItemForApproval
Version             :  
Created Date        : 2/12/2015
Function            : This is a ApexTrigger for Submitting an approval process for 
                    Assignment Request Item Records if status is Submitted                
Modification Log    :
* Developer                   Date                   Description
* ----------------------------------------------------------------------------                 
* Jayavardhan                  2/12/2015               Original Version  
*************************************************************************************/
Trigger TAG_AssignmentReqItemForApproval on Assignment_Request_Item__c (after insert) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
    //Variables for custom setting and Approval Process
    Team_Assignment_Governance_Settings__c myCS2;  
    Approval.ProcessSubmitRequest req;
    Approval.ProcessResult result;    
    
    Profile p = [SELECT Id FROM Profile WHERE Name='System Administrator']; 
    User user1 = [SELECT Id FROM User WHERE ProfileId =:p.Id and isActive = true limit 1];
    myCS2 = Team_Assignment_Governance_Settings__c.getInstance();    
    for (Assignment_Request_Item__c reqItem: Trigger.New)
    {   
        if(reqitem.Reason_Code__c.equalsIgnoreCase(myCS2.New_Acct_Reason_Code__c)){     
            if (reqItem.Status__c!=null && reqItem.Status__c=='Submitted')
            {        
                // create the new approval request to submit    
                req = new Approval.ProcessSubmitRequest();   
                req.setComments('Submitted for approval. Please approve.');
                req.setObjectId(reqItem.Id);                
                // submit the approval request for processing        
                result = Approval.process(req);                
            }  
            
        }
    }  
  }
}