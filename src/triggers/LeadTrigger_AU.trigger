trigger LeadTrigger_AU on Lead (after update) {
   if(Label.isTriggerActive.equalsIgnoreCase('true')){	
	List<Id> accountIds = new List<Id>();
	
	for (Lead l : Trigger.New){
		if (l.ConvertedAccountId != null){
			accountIds.add(l.ConvertedAccountId);
		}
	}
	
	String businessRegion;
	try{
    	Custom_Property__c property = [SELECT Id, Value__c FROM Custom_Property__c WHERE Name = 'Default Business Region' LIMIT 1]; 
        businessRegion = property.Value__c;

    }catch(Exception ex){
        businessRegion = 'From Lead Conversion';
    }
	
	List<Account> accts = [SELECT Id, Business_Segment__c FROm Account WHERE Id IN :accountIds];
	
	for (Account acct : accts){
		acct.Business_Segment__c = businessRegion;
	}
	
	UPDATE accts;
  }
}