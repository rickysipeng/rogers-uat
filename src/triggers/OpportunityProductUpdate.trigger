trigger OpportunityProductUpdate on OpportunityLineItem (after undelete, before delete) {
  if(Label.isTriggerActive.equalsIgnoreCase('true')){
    set<ID> oppProdIds  = new set<ID>();
    set<ID> oppIds  = new set<ID>();
    List<Sales_Measurement__c> listSMUpdate = new List<Sales_Measurement__c>();
    
    if ( trigger.isdelete ){
        for( OpportunityLineItem o : trigger.old ){
            oppProdIds.add(o.Id);
            oppIds.add(o.OpportunityId);
        } 
    }
    List<Opportunity> lstOpp = [select Id, Business_Unit__c from Opportunity where Business_Unit__c ='Wireless' and Id in :oppIds];
    List<Sales_Measurement__c> listSalesMeasurement = [select Opportunity_Line_Item_ID__c, Opportunity__c, id from Sales_Measurement__c where Opportunity_Line_Item_ID__c in :oppProdIds and Opportunity__c in:lstOpp];

    if(listSalesMeasurement.size()>0){
        delete listSalesMeasurement;
    }
  }  
}