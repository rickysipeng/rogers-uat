trigger OpportunityUpdate on Opportunity (after undelete, before delete) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
        set<ID> oppIds = new set<ID>();
        List<Sales_Measurement__c> listSalesMeasurement = new List<Sales_Measurement__c>();
        if ( trigger.isdelete ){
            for( Opportunity opp : trigger.old ){
                if(opp.Business_Unit__c =='Wireless')
                    oppIds.add( opp.Id );
            }    
            listSalesMeasurement = [select Opportunity_Line_Item_ID__c, id, Opportunity__c from Sales_Measurement__c where Opportunity__c in :oppIds ];   
        }
    
        if(listSalesMeasurement.size()>0){
            Database.delete(listSalesMeasurement, false);
        }
    }
}