/* ********************************************************************
 * Name : Site_Trigger_BD
 * Description : Trigger. Before Prospect Site Delete Trigger
 * Modification Log
  =====================================================================
 * Ver    Date        	Author          	 Modification
 ----------------------------------------------------------------------
 * 1.0    5/29/2012    	Kevin DesLauriers    Initial Version  
 * 1.1	  6/18/2012		Kevin DesLauriers	 Ensure to not allow user to delete Site if there is an Active Site
 **********************************************************************/
trigger Site_Trigger_BD on Site__c (before delete) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
	if (Trigger.oldMap != null){
		Set<Id> keys = new Set<Id>(Trigger.oldMap.keySet());
		List<Id> ids = new List<Id>(keys);
		List<Active_Site__c> activeSites = new List<Active_Site__c>();
		
		// Let's grab all of the Active Sites with Site ids and inform the user that they cannot delete these.
		try{
			activeSites = [SELECT Id, Site__c FROM Active_Site__c WHERE Site__c IN :ids];
		}catch (Exception ex){
		}
				
		Set<Id> siteIds = new Set<Id>();
		for (Active_Site__c aSite : activeSites){
			siteIds.add(aSite.Site__c);
			keys.remove(aSite.Site__c);
		}
		
		// We need to do this again because we may have decided to not delete some of the AZSites.
		ids = new List<Id>(keys);
		for (Site__c s : Trigger.old){
			if (siteIds.contains(s.Id)){
				s.addError('Prospect Site ' + s.Display_Name__c + ' cannot be deleted as it is tied to a Active Site.  Contact your administrator to delete the Active Site first.' );
			}
		}
		
		// Remove any sites that we are not allowed to delete so we do not delete the AZ Site	
		List<A_Z_Site__c> azSites = [SELECT Id FROM A_Z_Site__c WHERE Site_A__c IN :ids OR Site_Z__c IN :ids];		
			
		try{
			DELETE azSites;
		}catch(Exception ex){
			ApexPages.addMessage(new ApexPages.Message(ApexPages.Severity.WARNING, 'There was a problem deleting the Junction Objects. You should inform your system administrator'));
		}
	}
   }
}