trigger activityLogOnTask on Task (after insert, after update, before delete){
 if(Label.isTriggerActive.equalsIgnoreCase('true')){
    List<Activity_Log__c> lstActlogFinal = new List<Activity_Log__c>();
    List<Activity_Log__c> lstActlogDelete = new List<Activity_Log__c>();
    Map<String,Activity_Log__c> mapActivityLog = new Map<String,Activity_Log__c>();
    List<BIS_Activity_Tracking__c> lstBISupdate= new List<BIS_Activity_Tracking__c>();
    set<String> newLstBU= new set<String>();
    //get the list of channels coming under BIS users
    for(BIS_Users__c bU:BIS_Users__c.getAll().values()){
        newLstBU.add(bU.Name);  
    }
    
    set<String> newLstBISSubject= new set<String>();
    //get the list of subjects that needs to be worked on 
    for(BIS_Task_Subjects__c bU:BIS_Task_Subjects__c.getAll().values()){
        newLstBISSubject.add(bU.Name);  
    }
    
    
    //map the user with there corresponding channel
    Map<id,String> mapUserChannel= new Map<id,String>();
    
    //EBU Consolidation deployment error fix JK
    List<User> relUsers = new List<User>();
    if(!Test.isRunningTest()){
        relUsers = [select id,Name,Channel__c from User];
    } else {
        relUsers = [select id,Name,Channel__c from User limit 1];
     }

    for(User u:relUsers){
        mapUserChannel.put(u.id,u.channel__c);
    }
    
    if(trigger.isUpdate || trigger.isInsert){
        Map<String,String> mapSubject = new Map<String,String>();
        for(Task_Subjects__c t: [select Name,type__c from Task_Subjects__c]){
            mapSubject.put(t.name,t.Type__c);
        }
        set<String> tskIds = new set<String>();
        set<String> tskBISIds = new set<String>();
        for(Task tsk: Trigger.new){
            if(mapUserChannel.get(tsk.createdById) !=null && newLstBU.Contains(mapUserChannel.get(tsk.createdById))){
                tskBISIds.add(tsk.id);
            }else{
                tskIds.add(tsk.id);
            }
        }
        if(tskIds.size()>0){
            map<id,BIS_Activity_Tracking__c> mapTskBIS= new map<id,BIS_Activity_Tracking__c>();
            List<Activity_Log__c> lstActLog = new List<Activity_Log__c>([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where isdeleted=false AND Related_Record__c in: tskIds]);
            for(Activity_Log__c act: lstActLog){
                mapActivityLog.put(act.Related_Record__c,act);
            }
            
            List<BIS_Activity_Tracking__c> lstBISActTrak = new List<BIS_Activity_Tracking__c>([select Activity_Date__c,Note__c, Activity_Type__c, RPC_Call__c,Subject__c,Related_Record__c,Deleted__c from BIS_Activity_Tracking__c where isdeleted=false AND Related_Record__c in: tskIds]);
            for(BIS_Activity_Tracking__c bisAct:lstBISActTrak){
                mapTskBIS.put(bisAct.Related_Record__c,bisAct);
            }
            
            for(integer i=0; i<trigger.new.size();i++){
            // Start Logic for BIS creation when assigned to is BIS user
                IF(trigger.new[i].status==label.Completed && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerId)) && mapTskBIS.get(trigger.new[i].id)==null){
                       BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
                       bisActNew.OwnerID = trigger.new[i].OwnerId;
                       bisActNew.Subject__c = label.Task_1 + trigger.new[i].Subject;
                       bisActNew.Activity_Date__c =Date.ValueOf(trigger.new[i].LastModifiedDate);
                       bisActNew.Activity_Type__c = label.Task;
                       bisActNew.Related_Record__c = trigger.new[i].ID;
                       IF(trigger.new[i].Subject_Type__c == label.Prospecting_Call_RPC){
                           bisActNew.RPC_Call__c  = TRUE;
                       }else{
                           bisActNew.RPC_Call__c = FALSE;
                       }
                       bisActNew.Note__c = UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' ' + label.created_Task_on+' '+ system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
                       bisActNew.Sales_Rep_ID_Activity_ID__c =String.ValueOf(trigger.new[i].OwnerID)+String.ValueOf(trigger.new[i].ID);  
                       lstBISupdate.add(bisActNew);    
                } 
            // End logic
            
                Activity_Log__c act = mapActivityLog.get(trigger.new[i].id);
                if(act!=null){
                    if(trigger.new[i].OwnerID != trigger.old[i].OwnerID || trigger.new[i].Subject != trigger.old[i].Subject || trigger.new[i].LastModifiedDate != trigger.old[i].LastModifiedDate || trigger.new[i].Subject_Type__c != trigger.old[i].Subject_Type__c){
                        act.Sales_Rep__c = trigger.new[i].OwnerID; 
                        act.Activity_Date__c = Date.valueof(trigger.new[i].LastModifiedDate);   
                        String subjType = mapSubject.get(trigger.new[i].Subject_Type__c);
                        if(subjType==label.Call || subjType==label.RPC_Call){
                            if(trigger.new[i].Subject!=null && trigger.new[i].Subject.length()>=200){
                                act.Subject__c = label.Call_1 + trigger.new[i].Subject.substring(0,200);
                            }else{ 
                                act.Subject__c = label.Call_1+ trigger.new[i].Subject;}
                            act.Activity_Type__c = label.Call;
                            if(subjType==label.Call){
                                act.RPC_Call__c = false;
                            }
                            if(subjType==label.RPC_Call){
                                act.RPC_Call__c = true;
                            }
                        }
                        else if(subjType==label.Appointment){
                            if(trigger.new[i].Subject!=null && trigger.new[i].Subject.length()>=200){
                                act.Subject__c = label.Appointment_1 + trigger.new[i].Subject.substring(0,200);
                            }else{
                            act.Subject__c = label.Appointment_1 + trigger.new[i].Subject;}
                            act.Activity_Type__c = label.Appointment;
                            act.RPC_Call__c = false;
                        }
                        lstActlogFinal.add(act);
                    } 
                }
                else{
                    act = new Activity_Log__c();
                    act.Sales_Rep__c = trigger.new[i].OwnerID;
                    act.Activity_Date__c = Date.valueof(trigger.new[i].LastModifiedDate);
                    act.Related_Record__c = trigger.new[i].id;
                    String subjType = mapSubject.get(trigger.new[i].Subject_Type__c);
                    if(subjType==label.Call || subjType==label.RPC_Call){
                         if(trigger.new[i].Subject!=null && trigger.new[i].Subject.length()>=200){
                            act.Subject__c = label.Call_1+ trigger.new[i].Subject.substring(0,200);
                        }else{
                            act.Subject__c = label.Call_1 + trigger.new[i].Subject;}
                        act.Activity_Type__c = label.Call;
                        if(subjType==label.Call){
                            act.RPC_Call__c = false;
                        }
                        if(subjType==label.RPC_Call){
                            act.RPC_Call__c = true;
                        }
                    }
                    else if(subjType==label.Appointment){
                       if(trigger.new[i].Subject!=null && trigger.new[i].Subject.length()>=200){
                                act.Subject__c = label.Appointment_1 + trigger.new[i].Subject.substring(0,200);
                        }else{
                         act.Subject__c = label.Appointment_1 + trigger.new[i].Subject;}
                        act.Activity_Type__c = label.Appointment;
                        act.RPC_Call__c = false;
                    }
                    lstActlogFinal.add(act);
                }
            }
        }
        
        // tskBISIds
         
        if(tskBISIds.size()>0){
            map<id,BIS_Activity_Tracking__c> mapTskBIS= new map<id,BIS_Activity_Tracking__c>();
            List<BIS_Activity_Tracking__c> lstBISActTrak = new List<BIS_Activity_Tracking__c>([select Activity_Date__c,Note__c, Activity_Type__c, RPC_Call__c,Subject__c,Related_Record__c,Deleted__c from BIS_Activity_Tracking__c where isdeleted=false AND Related_Record__c in: tskBISIds]);
            for(BIS_Activity_Tracking__c bisAct:lstBISActTrak){
                mapTskBIS.put(bisAct.Related_Record__c,bisAct);
            }
            for(integer i=0; i<trigger.new.size();i++){
                BIS_Activity_Tracking__c act = mapTskBIS.get(trigger.new[i].id);
                if(act!=null){
                    if(!act.Deleted__c){
                    //Case 1: IF Task. Status is changed to "Completed" AND Created By = BIS user AND Assigned To = BIS user AND Created By = Assigned To
                     if(trigger.new[i].status==label.Completed && newLstBU.Contains(mapUserChannel.get(trigger.new[i].createdById)) && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerId)) && (trigger.new[i].createdById ==trigger.new[i].OwnerId)){
                        act.Activity_Date__c =Date.ValueOf(trigger.new[i].LastModifiedDate);
                        string s='';
                        if(act.Note__c!=null){
                            s = act.Note__c;
                        }
                        s=s+'; '+ label.Marked_Completed_By+' ' + UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' '+ label.on+' ' + system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
                        if(s.length() >255){
                        act.Note__c = s.SubString(s.length()-255,s.length());
                        }else{
                        act.Note__c = s;
                        }  
                        lstBISupdate.add(act);
                    }else if(trigger.new[i].status=='Completed' && newLstBU.Contains(mapUserChannel.get(trigger.new[i].createdById)) && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerId)) && (trigger.new[i].createdById !=trigger.new[i].OwnerId)){
                        act.OwnerId = trigger.new[i].OwnerId;
                        act.Activity_Date__c =Date.ValueOf(trigger.new[i].lastModifiedDate);
                        string s='';
                        if(act.Note__c!=null){
                            s = act.Note__c;
                        }
                        s = s+'; ' + label.Marked_Completed_By+' '+ UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' '+ label.on +' '+ system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York') ;
                        if(s.length() >255){
                        act.Note__c = s.SubString(s.length()-255,s.length());
                        }else{
                        act.Note__c = s;
                        }  
                        lstBISupdate.add(act);
                    }else if(!newLstBU.Contains(mapUserChannel.get(trigger.new[i].createdById))&& trigger.new[i].status=='Completed' && newLstBU.Contains(mapUserChannel.get(trigger.new[i].OwnerId))){
                       act.OwnerID = trigger.new[i].LastModifiedById;
                       act.Subject__c = label.Task_1 + trigger.new[i].Subject;
                       act.Activity_Date__c =Date.ValueOf(trigger.new[i].LastModifiedDate);
                       act.Activity_Type__c = label.Task;
                       act.Related_Record__c = trigger.new[i].ID;
                       IF(trigger.new[i].Subject_Type__c == label.Prospecting_Call_RPC){
                           act.RPC_Call__c  = TRUE;
                       }else{
                           act.RPC_Call__c = FALSE;
                       }
                       
                       act.Note__c = UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' ' + label.created_Task_on +' ' + system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
                       act.Sales_Rep_ID_Activity_ID__c =String.ValueOf(trigger.new[i].OwnerID)+String.ValueOf(trigger.new[i].ID);  
                       lstBISupdate.add(act);
                    }
                    }
                }
                else{
                // Upon Creation
                  if(trigger.new[i].Subject_Type__c != null && (newLstBISSubject.Contains(trigger.new[i].Subject_Type__c))){
                       BIS_Activity_Tracking__c bisActNew= new BIS_Activity_Tracking__c();
                       bisActNew.OwnerID = trigger.new[i].LastModifiedById;
                       bisActNew.Subject__c = label.Task_1 + trigger.new[i].Subject;
                       bisActNew.Activity_Date__c =Date.ValueOf(trigger.new[i].LastModifiedDate);
                       bisActNew.Activity_Type__c = label.Task;
                       bisActNew.Related_Record__c = trigger.new[i].ID;
                       IF(trigger.new[i].Subject_Type__c == label.Prospecting_Call_RPC){
                           bisActNew.RPC_Call__c  = TRUE;
                       }else{
                           bisActNew.RPC_Call__c = FALSE;
                       }
                       bisActNew.Note__c = UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' ' + label.created_Task_on+' ' + system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
                       bisActNew.Sales_Rep_ID_Activity_ID__c =String.ValueOf(trigger.new[i].OwnerID)+String.ValueOf(trigger.new[i].ID);  
                      lstBISupdate.add(bisActNew);
                   }
                }
            }
        }
    }
    if( trigger.isdelete ){
        List<String> tskIds = new List<String>();
        for(Task tsk: Trigger.old){
            tskIds.add(tsk.id);
        }
        lstActlogDelete.addAll([select Activity_Date__c, Activity_Type__c, Related_Record__c, RPC_Call__c,Sales_Rep__c, Subject__c from Activity_Log__c where Related_Record__c in: tskIds]);
        
        for(BIS_Activity_Tracking__c bAT:[select Note__c,Activity_Date__c, Activity_Type__c, RPC_Call__c,Subject__c,Related_Record__c from BIS_Activity_Tracking__c where isdeleted=false AND Deleted__c=:False AND Related_Record__c in: tskIds]){
            string s='';
                        if(bAT.Note__c!=null){
                            s = bAT.Note__c;
                        }
            s=s+'; '+label.This_Task_has_been_deleted_by+' '+UserInfo.GetFirstName()+' '+UserInfo.GetLastName()+' '+ label.on+' ' + system.Now().format('MM/dd/yyyy HH:mm:ss','America/New_York');
            if(s.length() >255){
            bAT.Note__c = s.SubString(s.length()-255,s.length());
            }else{
            bAT.Note__c = s;
            }
            bAT.Deleted__c=true;
            lstBISupdate.add(bAT);
        }
    }
    if(lstActlogFinal.size()>0){
        upsert lstActlogFinal;
    } 
    if(lstBISupdate.size() >0){
        upsert lstBISupdate;
    } 
    if(lstActlogDelete.size()>0){
        delete lstActlogDelete;
    } 
    }
       
}