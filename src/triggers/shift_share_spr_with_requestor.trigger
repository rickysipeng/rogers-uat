trigger shift_share_spr_with_requestor on SPR__c (before insert, after insert, after update) {
	if(Label.isTriggerActive.equalsIgnoreCase('true')){
    /*
    Trigger to add the original requestor to the Sharing Detail screen automatically when an SPR is created. Whenever a new SPR 
    is created, or cloned from an existing SPR, the requestor will be granted access. For simplicity, if the requestor changes (not sure if 
    this ever happens), the new requestor would also be granted access, but the previous requestor would NOT be removed automatically. 

    Developed by Jocsan Diaz on July 30th, 2012
    */
    /*
    if (trigger.isAfter) {
        // list to store share records to insert
        list<SPR__Share> theSPRShareList = new list<SPR__Share>();
        
        // loop throu records and create new shares
        for (SPR__c s : trigger.new) {
            // if no requestor set then continue
            if (s.Requested_By__c == null) continue;
            
            // create new share record
            SPR__Share theSRPShare = new SPR__Share();
            theSRPShare.ParentId = s.Id;
            theSRPShare.AccessLevel = 'Read';
            theSRPShare.UserOrGroupId = s.Requested_By__c;
            
            theSPRShareList.add(theSRPShare);
        }
        
        // insert share records
        try {
            insert theSPRShareList;
        } catch (Exception e) {
            system.debug(e);
        }    
    } else {
        for (SPR__c s : trigger.new) {
            if (s.Requested_By__c == null) s.Requested_By__c = UserInfo.getUserId();
        }
    }
    */
	}
}