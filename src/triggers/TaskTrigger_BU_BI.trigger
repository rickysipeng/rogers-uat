/* ********************************************************************
 * Name : TaskTrigger_BU_BI.trigger
 * Description : Trigger. Before Insert and Before Update Trigger
 * Modification Log
  =====================================================================
 * Ver    Date          Author               Modification
 ----------------------------------------------------------------------
 * 1.x    -/--/2012     Kevin DesLauriers    Initial Version 
 * 1.2   10/28/2013     Kevin DesLauriers    NSA Tasks are routed to backup - Vacation Logic
 * 1.3   02/07.2014     Kevin DesLauriers    Allow for the reporting of the activity date (Merged from Jose's Trigger)
 **********************************************************************/


trigger TaskTrigger_BU_BI on Task (before update, before insert) {
    if(Label.isTriggerActive.equalsIgnoreCase('true')){
      List<String> usernames = new List<String>();
      for (Task t : Trigger.New){
        t.UsableDueDate__c = t.ActivityDate;
          if (!Utils.isEmpty(t.user__c))
              usernames.add(t.user__c);       
      }
      
      List<User> users = [SELECT Id, Name FROM User WHERE Name IN :usernames AND isActive = true];
      
      Map<String, Id> userMap = new Map<String, Id>();
      
      for (User u : users){
          userMap.put(u.Name.toLowerCase(), u.Id);
      }
      
      
      
      for (Task t : Trigger.New){
          if (!Utils.isEmpty(t.user__c)){
              t.OwnerId = userMap.get(t.user__c.toLowerCase());
              t.User__c = null;
              t.Group__c = null;
          }
      }

    /* We need to check the current owner and see if they are on vacation.  If they are and they have a back-up
       then you need to send an email notification to them as well.       
    */
   if (Trigger.isBefore){
       /* The users that are currently owners of the tasks */
      Set<Id> taskOwners = new Set<Id>();
       for (Task t : Trigger.New){
          taskOwners.add(t.OwnerId);
      }
      
    try{
      Map<Id, User> vacationUsers = new Map<Id, User>([SELECT id, Name, On_Vacation__c, Backup_User__c, Backup_User__r.On_Vacation__c, Backup_User__r.Backup_User__c FROM User WHERE On_Vacation__c = TRUE AND Backup_User__c <> '' AND id IN :taskOwners]);
     
      for (Task t : Trigger.New){
        if (vacationUsers.get(t.OwnerId) != null){
          if (!vacationUsers.get(t.OwnerId).Backup_User__r.On_Vacation__c)
            t.OwnerId = vacationUsers.get(t.OwnerId).Backup_User__c;
          else if (vacationUsers.get(t.OwnerId).Backup_User__r.Backup_User__c != null)
            t.OwnerId = vacationUsers.get(t.OwnerId).Backup_User__r.Backup_User__c;
          
        }
      }
      
      
    }catch(Exception ex){
      
    }
   }
  }
}