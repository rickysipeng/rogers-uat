trigger shift_flag_spr_from_note on SPR_Note__c (after insert) {
  if(Label.isTriggerActive.equalsIgnoreCase('true')){
	/*
	Everytime an SPR Note is created, the corresponding SPR should have field Note_Just_Added__c changed.
	Version 1.0 Developed by JD on 2013-01-15.
	*/
    
    // get spr records id
    set<Id> theIdSet = new set<Id>();
    for (SPR_Note__c sprn : trigger.new) {
    	theIdSet.add(sprn.SPR__c);    	
    }
    
    // get asssociated spr records
    list<SPR__c> theSPRList = new list<SPR__c>([select Id, Note_Just_Added__c from SPR__c where Id in: theIdSet]);
    
    // change spr flag
    for (SPR__c spr : theSPRList) {
        spr.Note_Just_Added__c = !spr.Note_Just_Added__c;
    }
    
    // update spr records
    try {
    	update theSPRList;
    } catch (Exception e) {
        system.debug(e);
    }
   }
}